<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

function css_url($file_name) {
    return base_url() . CSS_URL_PATH . $file_name;
}

function js_url($file_name) {
    return base_url() . JS_URL_PATH . $file_name;
}

function images_url_global($file_name) {
    return base_url() . IMAGES_URL_PATH_GLOBAL . $file_name;
}

function img_url_global($file_name) {
    return base_url() . IMG_URL_PATH_GLOBAL . $file_name;
}

function asset_url($file_name) {
    return base_url() . ASSET_URL_PATH . $file_name;
}

function p($value) {
    echo '<pre>';
    print_r($value);
    echo'</pre>';
}

function p_d($value) {
    echo '<pre>';
    print_r($value);
    echo'</pre>';
    die();
}

function p_v($value) {
    echo '<pre>';
    var_dump($value);
    echo'</pre>';
}

function p_vd($value) {
    echo '<pre>';
    var_dump($value);
    echo'</pre>';
    die();
}

function messages($var) {
    $CI = &get_instance();
    $message_array = $CI->config->item('wesbite_messages');
    $message = null;
    //p($var);
    //die();


    if (isset($var) && !empty($var) && is_array($var)) {
        //p($var);
        //die();
        $message_var = each($var);
        $key = $message_var['key'];
        $value = $message_var['value'];
        //echo $message_var['key'];
        //echo $message_var['value'];
        $message = str_replace("{{var}}", $value, $message_array[$key]);
        //echo $message;
        //die();
    }
    return $message;
}

function current_date($format = false) {
    $date = new DateTime();
    if ($format != false) {
        $current_date = $date->format($format);
    } else {
        $current_date = $date->format('Y-m-d H:i:s');
    }
    return $current_date;
}

function getDateByFormat($date_is,$format=false){
    if($format == false){
        $format = 'Y-m-d';
    }
    if($date_is == 0){
        $date_is = '1990-11-11';
    }
    $date = date_create($date_is);
    return date_format($date,$format);
}

function send_email($to, $message, $subject, $from = null, $from_name = null, $cc = null, $bcc = null, $attachments = null) {
    $CI = & get_instance();
    $CI->load->library('email');
    $CI->load->helper('email');
    $config['word_wrap'] = true;
    $config['mailtype'] = 'html';
    $CI->email->initialize($config);

    $CI->email->clear();
    if (valid_email($to)) {
        $CI->email->to($to);
    } else {
        return;
    }
    $CI->email->message($message);
    $CI->email->subject($subject);
    if (empty($from)) {
        $from = 'noreply@xicom.biz';
    }
    if (empty($from_name)) {
        $from_name = 'xicom';
    }
    $CI->email->from($from, $from_name);
    if (!empty($cc)) {
        $CI->email->cc($cc);
    }
    if (!empty($bcc)) {
        $CI->email->bcc($bcc);
    }
    if (!empty($attachments)) {
        if (is_array($attachments)) {
            foreach ($attachments as $attachment) {
                $CI->email->attach($attachment);
            }
        } else {
            $CI->email->attach($attachments);
        }
    }
    if ($CI->email->send()) {
        return true;
    }
}

function slug_clean($val) {
    $clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $val);
    $clean = strtolower(trim($clean, '-'));
    $clean = preg_replace("/[\/_|+ -]+/", '-', $clean);
    return $clean;
    //echo $clean;
    //die();
}

function make_date($format, $date, $time_zone = null) {
    $default_time = date_default_timezone_get();
    if (!empty($time_zone)) {
        //$default_time = date_default_timezone_get();
        $old_date = $date;
        $date = new DateTime($date, new DateTimeZone("{$default_time}"));
        $date->setTimezone(new DateTimeZone($time_zone));
    } else {
        $date = new DateTime($date, new DateTimeZone("{$default_time}"));
    }
    $result = $date->format($format);
    return $result;
}

//function upload_file($path = NULL, $allowed_types = NULL, $min_width = NULL, $min_height = NULL, $max_width = NULL, $max_height = NULL, $file_name = NULL)

function upload_file($path = NULL, $allowed_types = NULL, $file_name = NULL) {
    //echo $file_name;
    //die();
    $CI = & get_instance();
    $config = array(
        'upload_path' => !empty($path) ? $path : './assets/bar_images/',
        'max_size' => '1048',
        'remove_spaces' => TRUE,
    );
    if (!is_dir($config['upload_path'])) {
        mkdir($config['upload_path'], 0777);
    }
    if (!empty($allowed_types)) {
        $config['allowed_types'] = $allowed_types;
    }

    $CI->load->library('upload');
    $CI->upload->initialize($config);

    if (!empty($file_name)) {
        $file = $CI->upload->do_upload($file_name);
    } else {
        $file = $CI->upload->do_upload();
    }

    if (!$file) {
        return $CI->upload->display_errors();
    } else {
        //$data = $CI->upload->data();
        return $CI->upload->data();
        /*
          $data = $CI->upload->data();
          $img_details = getimagesize($data['full_path']);

          if ((!empty($min_width) && $img_details[0] < $min_width) || (!empty($min_height) && $img_details[1] < $min_height))
          {
          unlink($data['full_path']);
          return "Image Should Be atleast $min_width x $min_height";
          }

          elseif((!empty($max_width) && $img_details[0] > $max_width) || (!empty($max_height) && $img_details[1] > $max_height))
          {
          resize_image($data['full_path'], $max_width, $max_height, '', false, false);
          $aspect_ratio = $img_details[0]/$img_details[1];
          if($img_details[0] > $img_details[1])
          {
          $height_new = floor($max_width/$aspect_ratio);
          if(isset($height_new) && $height_new >= $min_height && $height_new <= $max_height)
          {
          resize_image($data['full_path'], $max_width, $height_new);
          }
          else
          {
          if($img_details[1] > $max_height)
          {
          crop_image($data['full_path'], '', $max_width, $max_height);
          }
          else
          {
          crop_image($data['full_path'], '', $max_width, $img_details[1]);
          }
          }
          //unlink($data['full_path']);
          //return "Image new height $height_new {$data['full_path']}";
          //if($height
          }
          else
          {
          $width_new = floor($max_height * $aspect_ratio);
          if(isset($width_new) && $width_new >= $min_width && $width_new <= $max_width)
          {
          resize_image($data['full_path'], $width_new, $max_height);
          }
          else
          {
          if($img_details[0] > $max_width)
          {
          crop_image($data['full_path'], '', $max_width, $max_height);
          }
          else
          {
          crop_image($data['full_path'], '', $img_details[0], $max_height);
          }
          }
          }
          }
          return $data;
         */
        //p($data);
        //die();
        //return $data;
    }
}

function resize_image($s, $w, $h, $d = NULL, $new_image = FALSE, $maintain_ratio = TRUE) {
    if (file_exists($d) || file_exists($s) == FALSE) {
        return;
    }

    $img_details = getimagesize($s);
    $master_dim = 'height';

    if ($img_details[1] > $img_details[0]) {
        $master_dim = 'width';
    }
    $CI = & get_instance();
    $config = array();
    $config['image_library'] = 'gd2';
    $config['source_image'] = $s;
    if ($new_image == TRUE) {
        $config['new_image'] = $d;
    }
    $config['height'] = $h;
    $config['width'] = $w;
    $config['maintain_ratio'] = $maintain_ratio;
    $config['master_dim'] = $master_dim;
    $CI->load->library('image_lib', $config);

    if (!$CI->image_lib->resize()) {
        //echo $CI->image_lib->display_errors();
    }
    $CI->image_lib->clear();
    return $d;
}

function crop_image($s, $d, $w, $h, $x = NULL, $y = NULL) {
    if ((file_exists($s) == FALSE)) {
        return;
    }
    $img_details = getimagesize($s);
    $CI = & get_instance();
    $config = array();
    $config['image_library'] = 'gd2';
    $config['source_image'] = $s;
    $config['new_image'] = $d;
    $config['x_axis'] = $x;
    $config['y_axis'] = $y;
    $config['height'] = $h;
    $config['width'] = $w;
    $config['maintain_ratio'] = FALSE;
    $CI->load->library('image_lib', $config);

    if (!$CI->image_lib->crop()) {
        //echo $CI->image_lib->display_errors();
    }
}

function nicetime($date) {
    if (empty($date)) {
        return "No date provided";
    }

    $periods = array("second", "minute", "hour", "day", "week", "month", "year", "decade");
    $lengths = array("60", "60", "24", "7", "4.35", "12", "10");

    $now = time();
    $unix_date = strtotime($date);

    // check validity of date
    if (empty($unix_date)) {
        return "Bad date";
    }

    // is it future date or past date
    if ($now > $unix_date) {
        $difference = $now - $unix_date;
        $tense = "ago";
    } else {
        $difference = $unix_date - $now;
        $tense = "from now";
    }

    for ($j = 0; $difference >= $lengths[$j] && $j < count($lengths) - 1; $j++) {
        $difference /= $lengths[$j];
    }

    $difference = round($difference);

    if ($difference != 1) {
        $periods[$j].= "s";
    }

    return "$difference $periods[$j] {$tense}";
}

function delete_directory($path) {
    if (is_dir($path) === true) {
        $files = array_diff(scandir($path), array('.', '..'));

        foreach ($files as $file) {
            delete_directory(realpath($path) . '/' . $file);
        }

        return rmdir($path);
    } else if (is_file($path) === true) {
        return unlink($path);
    }

    return false;
}

if (!function_exists('get_prefix_code')) {

    function get_prefix_code() {
        $CI = & get_instance();
        $CI->load->model('country_model');
        $codes = $CI->country_model->getCodes();
        return $codes;
    }

}


if (!function_exists('get_countries')) {

    function get_countries() {
        $CI = & get_instance();
        $CI->load->model('country_model');
        $countries = $CI->country_model->getAllCountries();
        return $countries;
    }

}


if (!function_exists('get_states')) {

    function get_states($country) {
        $CI = & get_instance();
        $CI->load->model('region_model');
        $states = $CI->region_model->getAllRegionsByCountryId($country);
        return $states;
    }

}

if (!function_exists('get_state_name')) {

    function get_state_name($state_id) {
        $CI = & get_instance();
        $CI->load->model('region_model');
        $state = $CI->region_model->getStateName($state_id);
        return $state->region_name;
    }

}


if (!function_exists('lease_docs')) {

    function lease_docs($prop_id, $tenant_id, $type) {
        $CI = & get_instance();
        $CI->load->model('properties_model');
        $states = $CI->properties_model->getLeaseDocs($prop_id, $tenant_id, $type);
        return $states;
    }

}

if (!function_exists('get_services')) {

    function get_services() {
        $CI = & get_instance();
        $CI->load->model('services_model');
        $services = $CI->services_model->frontGetAllServices();
        return $services;
    }

}

if (!function_exists('get_vendor_services')) {

    function get_vendor_services() {
        $CI = & get_instance();
        $CI->load->model('tservices_model');
        $services = $CI->tservices_model->frontGetVendorServices();
        return $services;
    }

}

function ordinal($number) {
    $ends = array('th', 'st', 'nd', 'rd', 'th', 'th', 'th', 'th', 'th', 'th');
    if ((($number % 100) >= 11) && (($number % 100) <= 13))
        return $number . 'th';
    else
        return $number . $ends[$number % 10];
}

function getLnt($loc) {
    $url = "http://maps.googleapis.com/maps/api/geocode/json?address=
" . urlencode($loc) . "&sensor=false";
    $result_string = file_get_contents($url);
    $result = json_decode($result_string, true);
    $result1[] = $result['results'][0];
    $result2[] = $result1[0]['geometry'];
    $result3[] = $result2[0]['location'];
    return $result3[0];
}

function getSocialUrls() {
    $CI = & get_instance();
    $CI->load->model('web_config_model');
    $recordDetail = $CI->web_config_model->getRecordById(1);
    return $recordDetail;
}

function getFooterLinks() {
    $CI = & get_instance();
    $CI->load->model('pages_model');
    $recordDetail = $CI->pages_model->getFooterLinks();
    return $recordDetail;
}

function getFeatureLinks() {
    $CI = & get_instance();
    $CI->load->model('features_model');
    $recordDetail = $CI->features_model->getFeatureLinks();
    return $recordDetail;
}

function getFeatureBySlug($slug){
    $CI = & get_instance();
    $CI->load->model('common_model');
    $params['table'] = 'tbl_pages';
    $params['single_row'] = true;
    $conditions['slug'] = $slug;
    $content = $CI->common_model->get_data($conditions, $params);
    if(!empty($content)){
        $text=str_ireplace('<p>','',$content->description);
        $text=str_ireplace('</p>','',$content->description); 
        $text=str_ireplace('<strong>','',$content->description); 
        $text=str_ireplace('</strong>','',$content->description); 
        return '<div class="area_body_popup" style="padding:10px;">'.$text."</div>";
    }else{
        return '';
    }
}

function amount_formated($amount){
   return number_format((float)$amount, 2, '.', '');
}

$date = "2009-03-04 17:45";
$result = nicetime($date); // 2 days ago

function amount_format($foo){
   return number_format((float)$foo, 2, '.', '');
}

function amount_format_view($foo){
   return '$'. number_format((float)$foo, 2, '.', '');
}


/**
 * Method Name : getUserName
 * Author Name : Lakhvinder Singh
 * Date : 21 Sept 2016
 * Description : return all states of given country 
 */
if (!function_exists('getUserName')) {
    function getUserName($user_id) {
        $CI = & get_instance();
        $CI->load->model('common_model');
        $conditions = $params = array();
        $params['table'] = 'tbl_members';
        $conditions['mem_id'] = $user_id;
        $params['single_row'] = TRUE;
        $params['fields'] = array('first_name', 'last_name');
        $info = $CI->common_model->get_data($conditions, $params);
        return $info->first_name.' '.$info->last_name;
    }
}

/**
 * Method Name : getSlugById
 * Author Name : Lakhvinder Singh
 * Date : 21 Sept 2016
 * Description : return all states of given country 
 */
if (!function_exists('getSlugById')) {
    function getSlugById($user_id) {
        $CI = & get_instance();
        $CI->load->model('common_model');
        $conditions = $params = array();
        $params['table'] = 'tbl_members';
        $conditions['mem_id'] = $user_id;
        $params['single_row'] = TRUE;
        $params['fields'] = array('slug');
        $info = $CI->common_model->get_data($conditions, $params);
        return $info->slug;
    }
}

function getPropertyAddress($prop_id) {
        $CI = & get_instance();
        $CI->load->model('common_model');
        $conditions = $params = array();
        $params['table'] = 'tbl_properties';
        $conditions['prop_id'] = $prop_id;
        $params['single_row'] = TRUE;
        $params['fields'] = array('city', 'address1', 'address2');
        $info = $CI->common_model->get_data($conditions, $params);
        return $info->city.', '.$info->address1.', '.$info->address2;
    }


function count_checkout_property($user_id){
    $CI = & get_instance();
    $CI->load->model('common_model');
    $conditions = $params = array();
    $params['complex'] = true;

    $conditions['tables'] = array(
        'property' => 'tbl_properties',
        'state' => 'tbl_region'
    );
    $conditions['table'] = 'property';
    $conditions['on']['state'] = array(
        'sign' => '=',
        'column' => 'region_id',
        'alias_column' => 'state_id',
        'alias_other' => 'property',
    );

    $conditions['where']['status']['alias'] = 'property';
    $conditions['where']['status']['value'] = 'Active';
    
    $conditions['where']['step_completed']['alias'] = 'property';
    $conditions['where']['step_completed']['value'] = '3';
    $conditions['where']['owner_id']['alias'] = 'property';
    $conditions['where']['owner_id']['value'] = $user_id;
    $params['fields'] = array('property.prop_id');
    $info = $CI->common_model->get_data($conditions, $params);
    //echo $this->db->last_query(); exit;
    return count($info);
}


function getDuePaymentInfo($tenant_id){
    $CI = & get_instance();
    $CI->load->model('common_model');
    $conditions = $params = array();
    $params['table'] = 'tbl_properties_lease_detail';
    $conditions['tenant_id'] = $tenant_id;
    $params['single_row'] = true;
    $info = $CI->common_model->get_data($conditions, $params);
    if(!empty($info)){
        $all_tenants = getAllTenantIdByPropertyId($info->property_id);
    }else{
        $all_tenants = array();
    }
    //echo "<pre>"; print_r($info); exit;

    $current_date        = date('Y-m-d');
    $current_day         = date('d');
    $lease_start_date   = $info->lease_start_date;
    $lease_end_date     = $info->lease_end_date;
    $rent_amount        = $info->rent_amount;
    $due_date           = ($info->due_date <= 0) ? 27 : $info->due_date;
    $late_fee           = $info->late_fee;           // 1
    $late_fee_type      = $info->late_fee_type;         // Daily charge
    $late_fee_start     = $info->late_fee_start;
    $due_date_is        = date('Y-m-d');
    $date_due           = explode('-', $due_date_is);
    $date_due[2]        = $due_date;   // DD
    $due_date_formated  = implode('-', $date_due); 
    $current_month      = date('n');
    //$PaymentByMonth     = $CI->common_model->getPaymentByMonth($all_tenants,$lease_start_date,$lease_end_date);
    $PaymentByMonth     = $CI->common_model->getPaymentByMonth($all_tenants,$current_month);
    //print_R($PaymentByMonth); exit;
    //echo $current_day .'>'. $late_fee_start .'&&'. $late_fee_type .'==Daily charge';
    //echo "(". $current_date. ">". $lease_start_date ."&&". $lease_end_date .">=". $current_date;
    
    if($current_date >= $lease_start_date && $lease_end_date >= $current_date){
        if($current_day >= $late_fee_start){
            if($late_fee_type == 'Daily charge'){
                $late_day = $current_day - $late_fee_start; 
                $late_day = $late_day + 1; 
                $late_fee = $late_fee * $late_day;
                $due_amount = $rent_amount + $late_fee;       
                //echo $due_amount;
            }else{ 
                $due_amount = $rent_amount + $late_fee;
            }
        }else{
                $due_amount = $rent_amount;
        }   
        $due_amount = $rent_amount;
        //exit;
    }else{
       $due_amount =0; 
    }
    //echo $PaymentByMonth .'>='. $due_amount; exit;
    if($PaymentByMonth >= $due_amount){
        $due_amount = 0;
    }else{
        $due_amount = $due_amount - $PaymentByMonth;
    }


    $CI = & get_instance();
    $CI->db->select('sum(amount) as amount');
    $CI->db->where('issue_memo_year',date('Y'));
    $CI->db->where('issue_memo_month',date('m'));
    $CI->db->where('issue_memo','credit_memo');
    $CI->db->where_in('tenant_id',$all_tenants);
    $CI->db->where('status',1);
    $q = $CI->db->get('tbl_issue_memo');
    $credit_memo_info = $q->row();

    if(!empty($credit_memo_info)){
        $due_amount = $due_amount - $credit_memo_info->amount;
    }

    $CI = & get_instance();
    $CI->db->select('sum(amount) as amount');
    $CI->db->where('issue_memo_year',date('Y'));
    $CI->db->where('issue_memo_month',date('m'));
    $CI->db->where('issue_memo','debit_memo');
    $CI->db->where_in('tenant_id',$all_tenants);
    $CI->db->where('status',1);
    $q = $CI->db->get('tbl_issue_memo');
    $debit_memo_info = $q->row();
    if(!empty($debit_memo_info)){
        $due_amount = $due_amount + $debit_memo_info->amount;
    }

    /*if($test != false){
       // $PaymentByMonth     = $CI->common_model->getPaymentByMonth1($tenant_id,$current_month);
        echo "<pre>"; print_R($due_amount);  exit;
    }*/
    $due_result = array('due_amount'=>$due_amount, 'due_date' => $due_date_formated);
    return $due_result;
}

function getDuePaymentByTenantId_bkup($tenant_id,$test=false){
    $CI = & get_instance();
    $CI->load->model('common_model');
    $conditions = $params = array();
    $params['table'] = 'tbl_properties_lease_detail';
    $conditions['tenant_id'] = $tenant_id;
    $params['single_row'] = true;
    $info = $CI->common_model->get_data($conditions, $params);
    if(!empty($info)){
        $all_tenants = getAllTenantIdByPropertyId($info->property_id);
    }else{
        $all_tenants = array();
    }
    //echo "<pre>"; print_r($info); exit;

    $current_date        = date('Y-m-d');
    $current_day         = date('d');
    $lease_start_date   = $info->lease_start_date;
    $lease_end_date     = $info->lease_end_date;
    $rent_amount        = $info->rent_amount;
    $due_date           = $info->due_date;
    $late_fee           = $info->late_fee;           // 1
    $late_fee_type      = $info->late_fee_type;         // Daily charge
    $late_fee_start     = $info->late_fee_start;
    $due_date_is        = date('Y-m-d');
    $date_due           = explode('-', $due_date_is);
    $date_due[2]        = $due_date;   // DD
    $due_date_formated  = implode('-', $date_due); 
    $current_month      = date('m');
    $PaymentByMonth     = $CI->common_model->getPaymentByMonth($all_tenants,$current_month);
    //print_R($PaymentByMonth); exit;
    //echo "(". $current_date. ">". $lease_start_date ."&&". $lease_end_date .">=". $current_date;
    //echo $current_day .'>'. $late_fee_start .'&&'. $late_fee_type .'==Daily charge'; 
    //exit;
    if($current_date > $lease_start_date && $lease_end_date >= $current_date){
        if($current_day >= $late_fee_start){
            if($late_fee_type == 'Daily charge'){
                $late_day = $current_day - $late_fee_start; 
                $late_fee = $late_fee * $late_day;
                $due_amount = $rent_amount + $late_fee;       

            }else{ // One time
                $due_amount = $rent_amount + $late_fee;
            }
        }else{
                $due_amount = $rent_amount;
        }   
    }else{
       $due_amount ='0'; 
       $due_date_formated ='0000-00-00'; 
    }

    if($PaymentByMonth >= $due_amount){
        $due_amount = '0';
        $due_date_formated ='0000-00-00'; 
    }else{
        $due_amount = $due_amount - $PaymentByMonth;
    }

    /*$conditions = $params = array();
    $params['table'] = 'tbl_issue_memo';
    $params['fields'] = array('sum(amount) as amount');
    $conditions['issue_memo_year'] = date('Y');
    $conditions['issue_memo_month'] = date('m');
    $conditions['issue_memo'] = 'credit_memo';
    $conditions['tenant_id'] = $tenant_id;
    $conditions['status'] = 1;
    $params['single_row'] = true;
    $credit_memo_info = $CI->common_model->get_data($conditions, $params);*/

    $CI = & get_instance();
    $CI->db->select('sum(amount) as amount');
    $CI->db->where('issue_memo_year',date('Y'));
    $CI->db->where('issue_memo_month',date('m'));
    $CI->db->where('issue_memo','credit_memo');
    $CI->db->where_in('tenant_id',$all_tenants);
    $CI->db->where('status',1);
    $q = $CI->db->get('tbl_issue_memo');
    $credit_memo_info = $q->row();

    if(!empty($credit_memo_info)){
        $due_amount = $due_amount - $credit_memo_info->amount;
    }


    /*$conditions = $params = array();
    $params['table'] = 'tbl_issue_memo';
    $params['fields'] = array('sum(amount) as amount');
    $conditions['issue_memo_year'] = date('Y');
    $conditions['issue_memo_month'] = date('m');
    $conditions['issue_memo'] = 'debit_memo';
    $conditions['tenant_id'] = $tenant_id;
    $conditions['status'] = 1;
    $params['single_row'] = true;
    $debit_memo_info = $CI->common_model->get_data($conditions, $params);*/

    $CI = & get_instance();
    $CI->db->select('sum(amount) as amount');
    $CI->db->where('issue_memo_year',date('Y'));
    $CI->db->where('issue_memo_month',date('m'));
    $CI->db->where('issue_memo','debit_memo');
    $CI->db->where_in('tenant_id',$all_tenants);
    $CI->db->where('status',1);
    $q = $CI->db->get('tbl_issue_memo');
    $debit_memo_info = $q->row();
    if(!empty($debit_memo_info)){
        $due_amount = $due_amount + $debit_memo_info->amount;
    }

    /*if($test != false){
       // $PaymentByMonth     = $CI->common_model->getPaymentByMonth1($tenant_id,$current_month);
        echo "<pre>"; print_R($due_amount);  exit;
    }*/

    return $due_amount;
}
function getAllTenantIdByPropertyId($property_id){
    $CI = & get_instance();
    //$CI->db->where_in('id', array('20','15','22','42','86'));
    $CI->db->select('tenant_id');
    $CI->db->where('property_id',$property_id);
    $q = $CI->db->get('tbl_properties_lease_detail');
    $result = $q->result_array();
    $tenants = array();
    foreach ($result as $key => $value) {
        $tenants[] = $value['tenant_id'];
    }
    //print_R($tenants); exit;
    return $tenants;
}

if (!function_exists('getSynapseKycStatus')) {
    function getSynapseKycStatus($user_id) {
        $CI = & get_instance();
        $CI->load->model('common_model');
        $conditions = $params = array();
        $conditions['user_id'] = $user_id;
        $params['table'] = 'tbl_linked_account_info';
        $params['single_row'] = true;
        $data = $CI->common_model->get_data($conditions,$params);
        // print_r($data); exit;
        if(empty($data)){
            return false;
        }else{
            if($data->kyc_permission == 'UNVERIFIED' || $data->kyc_permission == ''){
                return false;
            }else{
                return true;
            }
        }
    }
}


    function getSynapseSetUpStatus($user_id) {
        if(getSynapseKycStatus($user_id) == true){
            $CI = & get_instance();
            $CI->load->model('common_model');
            $conditions = $params = array();
            $conditions['user_id'] = $user_id;
            $conditions['status'] = 1;
            $conditions['is_deleted'] = 0;
            $params['single_row'] = TRUE;
            $params['table'] = 'tbl_linked_account_numbers';
            $info = $CI->common_model->get_data($conditions, $params);
            //print_r($info); exit;
            if(!empty($info)){
                //if($info->bank_code == 'other'){
                    //if($info->synapse_micro_permission == 'CREDIT-AND-DEBIT' || $info->synapse_micro_permission == 'CREDIT'){
                    if($info->synapse_micro_permission == 'CREDIT-AND-DEBIT'){
                        return true;
                    }else{
                       return  false;
                       // return false;
                    }
                //}else{
                    //return  true;
                //}
            }else{
                return false;
               // return false;
            }
        }else{
            return false;
        }
    }


    function get_single_survey_question(){       
        $CI = & get_instance();
        $user_id = $CI->session->userdata('MEM_ID');
        //echo $user_id; exit;
        $question_info = $CI->db->query("SELECT * FROM tbl_questions WHERE id NOT IN (SELECT question_id FROM tbl_survey_answers where user_id=$user_id) AND type = 2"); 
        $question_info = $question_info->row();
        if(empty($question_info)){
            $html = '';
            $question_html = '';
            $question_exists = 'no';
        }else{
            $question_exists = 'yes';
            //print_r($question_info); exit;
            $html = 'Question not found';
            $html = '<form class="survey_question_form" action="/update_survey_question" method="post">';
            $html .= '<div class="ajax_report alert display-hide" role="alert" style="float: left; width: 85%; padding: 5px; margin-top: 4px;">
                                            <span class="close close_custom_popup"></span>
                                            <span class="ajax_message" style="line-height:normal;margin: 0;padding: 10px; 5px 10px 5px"></span>
                                        </div>';
                    $i=1;
                    $answer_type = $question_info->answer_type;
                    $question_html = $question_info->question;
                    $html .= '<div class="cq_content">';
                        $html .= "<b>".$i.'. '.$question_info->question. "</b>";
                            $html .= '<div class="cm_row">';
                                if($answer_type == 1){
                                        $html .= '<p class="my-radio"><input type="radio"  id="1_'.$question_info->id.'" value="yes" name="'.$question_info->id.'[answer]"> <label for="1_'.$question_info->id.'">Yes</label></p>';          
                                        $html .= '<p class="my-radio"><input type="radio"  id="2_'.$question_info->id.'" value="no"  name="'.$question_info->id.'[answer]"> <label for="2_'.$question_info->id.'">No</label></p><br>';          
                                
                                }else{
                                    if(!empty($question_info->answer1)){
                                        $html .= '<p class="my-radio"><input type="checkbox" id="1_'.$question_info->id.'" value="'.$question_info->answer1 .'" name="'.$question_info->id.'[answer1]"> <label for="1_'.$question_info->id.'">'.$question_info->answer1.'</label></p>';          
                                    }
                                    if(!empty($question_info->answer2)){
                                        $html .= '<p class="my-radio"><input type="checkbox" id="2_'.$question_info->id.'" value="'.$question_info->answer2 .'" name="'.$question_info->id.'[answer2]"> <label for="2_'.$question_info->id.'">'.$question_info->answer2.'</label></p>';          
                                    }
                                    if(!empty($question_info->answer3)){
                                        $html .= '<p class="my-radio"><input type="checkbox" id="3_'.$question_info->id.'" value="'.$question_info->answer3 .'" name="'.$question_info->id.'[answer3]"> <label for="3_'.$question_info->id.'">'.$question_info->answer3.'</label></p>';          
                                    }
                                    if(!empty($question_info->answer4)){
                                        $html .= '<p class="my-radio"><input type="checkbox" id="4_'.$question_info->id.'" value="'.$question_info->answer4 .'" name="'.$question_info->id.'[answer4]"> <label for="4_'.$question_info->id.'">'.$question_info->answer4.'</label></p>';          
                                    }
                                    if(!empty($question_info->answer5)){
                                        $html .= '<p class="my-radio"><input type="checkbox" id="5_'.$question_info->id.'" value="'.$question_info->answer5 .'" name="'.$question_info->id.'[answer5]"> <label for="5_'.$question_info->id.'">'.$question_info->answer5.'</label></p>';          
                                    }

                                }
                            $html .= '</div>';
                    $html .= '</div><br>';
                
                $html .= '<input type="button"  name="survey_submit" class="btn-tenant" id="survey_submit" value="Submit" style="text-align:center"> ';          
                    $html .= '</br></br>';
            $html .= '</form>';
        }
        $data['success'] = true; 
        $data['html'] = $html; 
        $data['question_html'] = $question_html; 
        $data['question_exists'] = $question_exists; 
        return $data;
       
    }

    function checkTrialTimePeriod($user_id){
        $CI = & get_instance();
        $CI->load->model('common_model');
        $params['table'] = 'tbl_members';
        $params['single_row'] = true;
        $conditions['mem_id'] = $user_id;
        $content = $CI->common_model->get_data($conditions, $params);
        if(empty($content)){
            return false;
        }else{
            $trial_end_date  = $content->trial_end_date;
            $current_date = date('Y-m-d');
            //echo $trial_end_date .' > '. $current_date;
            //exit;
            if($trial_end_date < $current_date){
                return false;
            }else{
                return $trial_end_date;
            }
        }
    }

    function getAnyTenantInfoByPropertyId($prop_id = false){
        if($prop_id == false){
            $CI = & get_instance();
            $prop_id = $CI->session->userdata('PROPERTY_ID');
        }
        $CI = & get_instance();
        $CI->load->model('common_model');
        $params['table'] = 'tbl_properties_lease_detail';
        $params['single_row'] = true;
        $conditions['property_id'] = $prop_id;
        $content = $CI->common_model->get_data($conditions, $params);
        return $content;
    }
