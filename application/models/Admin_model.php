<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Admin_model extends CI_Model {

	var $tableName	=	'tbl_admin';
	var $tableNameLog ='tbl_admin_log';
    function __construct()
    {
        parent::__construct();
    }
    
    function doLogin()
	{
		$this->db->where('username',$this->input->post('username'));
		$this->db->where('password',md5($this->input->post('password')));
		$query	=	$this->db->get($this->tableName);
		return $query->row();
		
	}	

	function logout()
	{
		$this->session->unset_userdata('ADM_ID');
		if($this->input->cookie($cookie))
			delete_cookie($cookie);
		redirect('admin');
	}

	function getRecordByIdOrEmail($value)
	{
		$this->db->where('adm_id',$value);
		$this->db->or_where('email',$value);
		$query	=	$this->db->get($this->tableName);
		return $query->row();	
	}
	
	function forgetPassword()
	{
		$str		=	'asdfqwerzxcvGHJKL1234567890bnmTYuiOp';
		$shuffle	=	str_shuffle($str);
		$password	=	substr($shuffle,0,8);
		$this->session->set_userdata('NEW_PASSWORD',$password);
		
		$this->db->set('password',md5($password));
		$this->db->where('email',$this->input->post('email'));
		$this->db->update($this->tableName);
	}
	
	#================================== Insert Log In Detail When Admin Login=============================================#
	function create_log($browser,$version,$platform,$mobile)
	{
		$operating_system=($mobile!="")?$platform.'-'.$mobile:$platform;
		$data = array(
		   'admin_id' => '1',
		   'admin_name' => $this->input->post('username'),
		   'login_date' => date("Y-m-d H:i:s"),
		   'login_ip' => $_SERVER['REMOTE_ADDR'],
		   'operating_system' => $operating_system,
		   'browser' => $browser."-Version-".$version
		);
		$this->db->insert($this->tableNameLog, $data); 
	}
	
	//================= Admin profile functions starts======================================================//

	function updateAdminPersonalProfile($id)
	{
		$this->db->set('name', $this->input->post('name'));
		$this->db->set('email', $this->input->post('email'));
		$this->db->set('mobile_no', $this->input->post('mobile_no'));
		$this->db->set('address', $this->input->post('address'));
		
		$this->db->where('adm_id',$id);
		$this->db->update($this->tableName);
	}
	
	function updateAdminAvatar($id,$image)
	{
		$rec = $this->getRecordByIdOrEmail($id);		
		unlink('./assets/uploads/admin_image/'.$rec->profile_pic);
		$this->db->set('profile_pic',$image);
		$this->db->where('adm_id',$id);
		$this->db->update($this->tableName);
	}
	
	function updateAdminPasswords($id)
	{
		$this->db->set('password',md5($this->input->post('new_password')));
		$this->db->where('adm_id',$id);
		$this->db->update($this->tableName);
	}
	
	function getAdminLogsData($id)
	{
		$this->db->order_by("id","desc");
		$this->db->where("admin_id",$id);
	 	$query=$this->db->get($this->tableNameLog);
		$record	=	$query->result();	
		return $record;		
	}	
	
	function getAdminSomeDetail($id)
	{
	 	$this->db->select('profile_pic,name');
		$this->db->where('adm_id',$id);
	 	$query = $this->db->get($this->tableName);
	 	return $record	=	 $query->row();
	}
	
	//================= Admin profile functions End======================================================//
}
