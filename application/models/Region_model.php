<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Region_model extends CI_Model {

    var $tableName = 'tbl_region';

    function __construct() {
        parent::__construct();
    }

    /*     * ***************** Front functions Starts here ************************* */

    function getAllRegions() {
        $this->db->where('status', 'Active');
        $this->db->order_by('region_name', 'ASC');
        $query = $this->db->get($this->tableName);
        return $query->result();
    }

    function getAllRegionsByCountryId($con_id) {
        $this->db->where('country_id', $con_id);
        $this->db->where('status', 'Active');
        $this->db->order_by('region_name', 'ASC');
        $query = $this->db->get($this->tableName);
        return $query->result();
    }
    
     function getStateName($id) {
        $this->db->where('region_id', $id);
        $query = $this->db->get($this->tableName);
        return $query->row();
    }

    /*     * ***************** Front functions Ends here ************************* */
}
