<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Country_model extends CI_Model {

    var $tableName = 'tbl_country';

    function __construct() {
        parent::__construct();
    }

    /*     * ***************** Front functions Starts here ************************* */

    function getAllCountries() {
        $this->db->where('status', 'Active');
        $this->db->order_by('country_name', 'ASC');
        $query = $this->db->get($this->tableName);
        return $query->result();
    }

    function getCodes(){
        $this->db->select('DISTINCT(prefix_code)');
        $this->db->from($this->tableName);
        $this->db->order_by('prefix_code', 'ASC');
        $query = $this->db->get();
        return $query->result();
    }
    /*     * ***************** Front functions Ends here ************************* */
}
