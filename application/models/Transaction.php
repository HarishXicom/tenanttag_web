<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Transaction extends CI_Model{


    function getRows($params = array()){
        $user_arr = array();
        if($params['slug'] == 'all'){
            $user_arr[] = $params['user_id'];
            $tenantList = $this->get_all_tenant($params['user_id']);
            //echo "<pre>"; print_r($tenantList); exit;
            if($tenantList){
                foreach ($tenantList as $key => $value) {
                    $user_arr[] = $value['tenant_id'];
                }
            }
        }else{
            $user_arr[] = $this->getTenantIdBySlug($params['slug']);
        }
        if(empty($user_arr)){
            return FALSE;
        }else{
            //$user_arr = implode(',', $user_arr);
            $this->db->select('payment.*, properties.city,properties.address1,properties.address2');
            $this->db->from('tbl_payment as payment');
            $this->db->join('tbl_properties as properties', 'properties.prop_id = payment.property_id');
            $this->db->where_in('payment.mem_id',$user_arr);
            $this->db->order_by('payment.id','desc');        
            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }        
            $query = $this->db->get();        
            return ($query->num_rows() > 0)?$query->result_array():FALSE;
        }
    }

    function get_all_tenant($owner_id){
        $this->db->select('*');
        $this->db->from('tbl_properties_lease_detail');
        $this->db->where('owner_id',$owner_id);
        $query = $this->db->get();
        return ($query->num_rows() > 0)?$query->result_array():FALSE;
        
    }
    function getTenantIdBySlug($slug){
        $this->db->select('*');
        $this->db->from('tbl_members');
        $this->db->where('slug',$slug);
        $mem_id = $this->db->get()->row()->mem_id;
        return $mem_id;
        
    }
    
}