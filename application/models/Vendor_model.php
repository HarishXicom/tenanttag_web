<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Vendor_model extends CI_Model {

    var $tableName = 'tbl_vendors';

    function __construct() {
        parent::__construct();
    }

    /*     * ***************** Front functions Starts here ************************* */

    //******************* Get Member Info by any field **************************//

    function getMemberInfo($field_name, $field_value) {
        $this->db->where($field_name, $field_value);
        $query = $this->db->get($this->tableName);
        $record = $query->row();
        return $record;
    }

    /*     * *************** Get all vendors by owner ID *********************************** */

    function countVendors($owner_id,$prop_id) {
        $this->db->where('status', 'Active');
        $this->db->where('vendor_owner_id', $owner_id);
        $this->db->where('property_id', $prop_id);
        $this->db->order_by('v_id', 'desc');
        $query = $this->db->get($this->tableName);
        return $query->result();
    }

    /*     * ************** Add Vendor by landlord ***************************************** */

    function addVendor() {
        $this->db->set('company_name', $this->input->post('company_name'));
        $this->db->set('mobile_no', $this->input->post('company_contact'));
        $this->db->set('email', $this->input->post('email'));
        $this->db->set('service', $this->input->post('service'));
        $this->db->set('vendor_owner_id', $this->session->userdata('MEM_ID'));
        $this->db->set('property_id', $this->session->userdata('PROPERTY_ID'));
        $this->db->set('status', 'Active');
        $this->db->set('add_date', time());
        $this->db->set('ip', $_SERVER['REMOTE_ADDR']);
        $this->db->insert($this->tableName);
        $mem_id = $this->db->insert_id();
        return $mem_id;
    }

    /*     * ************** Update Vendor by landlord ***************************************** */

    function updateVendor($memId) {
        $this->db->set('company_name', $this->input->post('company_name'));
        $this->db->set('mobile_no', $this->input->post('company_contact'));
        $this->db->set('service', $this->input->post('service'));
        $this->db->set('email', $this->input->post('email'));
        $this->db->where('v_id', $memId);
        $this->db->update($this->tableName);
    }

    function deleteRecordById($memId) {
        $this->db->where('v_id', $memId);
        $this->db->delete($this->tableName);
    }
    
    function deleteRecordByEmail($email,$landlord) {
        $this->db->where('email', $email);
        $this->db->where('vendor_owner_id',$landlord);
        $this->db->delete($this->tableName);
    }
    
    function getAllVendors($owner_id) {
        $this->db->where('status', 'Active');
        $this->db->where('vendor_owner_id', $owner_id);
        $this->db->order_by('v_id', 'desc');
        $query = $this->db->get($this->tableName);
        return $query->result();
    }
    
    function getDistinctVendors($owner_id) {
        $this->db->select('*');
        $this->db->where('status', 'Active');
        $this->db->where('vendor_owner_id', $owner_id);
        $this->db->group_by('email');
        $query = $this->db->get($this->tableName);
        return $query->result();
    }
    function getDistinctVendorsOfParticularProperty($property_id) {
        $this->db->select('*');
        $this->db->where('status', 'Active');
        $this->db->where('property_id', $property_id);
        $this->db->group_by('email');
        $query = $this->db->get($this->tableName);
        return $query->result();
    }

    function deleteVendorsOfProperty($property_id){
        $this->db->where('property_id',$property_id);
        $this->db->delete($this->tableName);
    }
    function deleteVendorsOfPropertyByVendorIds($ids){
        if(!empty($ids)){
            $this->db->where_in('v_id',$ids);
            $this->db->delete($this->tableName);
        }
    }

}
