<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Member_model extends CI_Model {

    var $tableName = 'tbl_members';
    var $ownedPropertiestableName = 'tbl_properties_lease_detail';
    var $ownedPropertieshistorytableName = 'tbl_properties_lease_history';

    function __construct() {
        parent::__construct();
    }

    /*     * ***************** Admin functions Starts here ************************* */

    function getCountAllRecordsForAdminByType($type, $status = NULL) {
        if($status ==   'Deleted'){
            $this->db->where('account_closed_by_member', 'Yes');
              if ($type)
                $this->db->where('user_type', $type);
        }else{
            if ($type)
                $this->db->where('user_type', $type);

            if ($status != NULL && $status != 'Deleted'){
                $this->db->where('status', $status);
            }
            $this->db->where('account_closed_by_member', 'No');
        }
        $query = $this->db->get($this->tableName);
        //echo $this->db->last_query(); exit;
        return $query->num_rows();
    }

    function getDeletedCountAllRecordsForAdminByType() {
        //$this->db->where('account_closed_by_member', 'Yes');
        $this->db->where('user_type', 'tenant');
        $query = $this->db->get($this->tableName);
        //echo $this->db->last_query(); exit;
        return $query->num_rows();
    }

    function getAllRecordsForAdminByType($type, $status = NULL, $num, $offset) {
      
        if ($type)
            $this->db->where('user_type', $type);
        if ($status)
            $this->db->where('status', $status);
        $this->db->where('account_closed_by_member', 'No');
        $this->db->order_by('first_name', 'ASC');
        if ($offset >=0) {
            $query = $this->db->get($this->tableName, $num, $offset);
        } else {
            $query = $this->db->get($this->tableName);
        }
        return $query->result();
    }

    function getMemberRecordById($memId) {
        $this->db->where('mem_id', $memId);
        $query = $this->db->get($this->tableName);
        return $query->row();
    }

    function getMemberRecordByAnyField($field_value, $field_name) {
        $this->db->where($field_name, $field_value);
        $query = $this->db->get($this->tableName);
        return $query->row();
    }

    function getMemberRecordByIdAndType($memId, $type) {
        $this->db->where('mem_id', $memId);
        $this->db->where('user_type', $type);
        $query = $this->db->get($this->tableName);
        return $query->row();
    }

    function add($profile_pic) {
        $strnew2 = 'abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $shuff2 = str_shuffle($strnew2);
        $shuff = str_shuffle($strnew2);
        $verification_code = substr($shuff2, 0, 8);
        $newPassword = substr($shuff, 0, 8);
        $this->db->set('first_name', $this->input->post('first_name'));
        $this->db->set('last_name', $this->input->post('last_name'));
        $slug = $this->common_model->create_unique_slug_for_common($this->input->post('first_name'), $this->tableName);
        $this->db->set('slug', $slug);
        if ($this->input->post('company_name'))
            $this->db->set('company_name', $this->input->post('company_name'));
        $this->db->set('email', $this->input->post('email'));
        if ($this->input->post('password')) {
            $this->db->set('password', md5($this->input->post('password')));
            $this->db->set('tpass', $this->input->post('password'));
            $this->session->set_userdata('NEW_PASSWORD', $this->input->post('password'));
        } else {
            $this->db->set('password', md5($newPassword));
            $this->db->set('tpass', $newPassword);
        }
        if ($this->input->post('dob')) {
            $this->db->set('dob', strtotime($this->input->post('dob')));
        }
       /* if ($this->input->post('prefix_code'))
            $this->db->set('prefix_code', $this->input->post('prefix_code'));*/
        if ($this->input->post('mobile'))
            $this->db->set('mobile_no', $this->input->post('mobile'));
        if ($this->input->post('country'))
            $this->db->set('country_id', $this->input->post('country'));
        if ($this->input->post('state'))
            $this->db->set('state_id', $this->input->post('state'));
        if ($this->input->post('city'))
            $this->db->set('city_id', $this->input->post('city'));
        if ($this->input->post('address')) {
            $this->db->set('address', $this->input->post('address'));
        }
        $this->db->set('user_type', $this->input->post('user_type'));
        if ($this->input->post('preferred_language'))
            $this->db->set('preferred_language', $this->input->post('preferred_language'));
        if ($profile_pic != ''){
            $this->db->set('profile_image', $profile_pic);
        }
        $this->db->set('add_date', time());
        $this->db->set('status', 'Active');
        $this->db->set('verification_status', 'Yes'); 
        $this->db->set('trial_end_date', date('Y-m-d', strtotime("+60 days")));
        $this->db->set('ip', $_SERVER['REMOTE_ADDR']);
        $this->db->insert($this->tableName);
        $mem_id = $this->db->insert_id();

        /*         * ***function if tenant is going to be added by admin then insert property detail also in other table ****** */

        if ($this->input->post('user_type') == 'tenant') {
            $this->db->set('property_id', $this->input->post('property'));
            $this->db->set('owner_id', $this->input->post('landlord'));
            $this->db->set('tenant_id', $mem_id);
//            $this->db->set('lease_start_date', strtotime($this->input->post('lease_start_date')));
//            $this->db->set('lease_end_date', strtotime($this->input->post('lease_end_date')));
            $this->db->set('lease_start_date', date('Y-m-d',strtotime($this->input->post('lease_start_date'))));
            $this->db->set('lease_end_date', date('Y-m-d',strtotime($this->input->post('lease_end_date'))));
            $this->db->set('due_date', $this->input->post('due_date'));
            $this->db->set('rent_amount', $this->input->post('rent_amount'));
            $this->db->set('late_fee', $this->input->post('late_fee'));
            $this->db->set('late_fee_type', $this->input->post('late_fee_type'));
            $this->db->set('occupie_status', 'running');
            $this->db->set('add_date', time());
            $this->db->insert('tbl_properties_lease_detail');

            /*             * ***function if tenant is going to be added by admin then insert record in lease_history table also ****** */
            $this->db->set('property_id', $this->input->post('property'));
            $this->db->set('owner_id', $this->input->post('landlord'));
            $this->db->set('tenant_id', $mem_id);
            //            $this->db->set('lease_start_date', strtotime($this->input->post('lease_start_date')));
            //            $this->db->set('lease_end_date', strtotime($this->input->post('lease_end_date')));
            $this->db->set('lease_start_date', date('Y-m-d',strtotime($this->input->post('lease_start_date'))));
            $this->db->set('lease_end_date', date('Y-m-d',strtotime($this->input->post('lease_end_date'))));
            $this->db->set('due_date', $this->input->post('due_date'));
            $this->db->set('rent_amount', $this->input->post('rent_amount'));
            $this->db->set('late_fee', $this->input->post('late_fee'));
            $this->db->set('add_date', time());
            $this->db->insert('tbl_properties_lease_history');
        }
        return $mem_id;
    }

    function update($memId, $profile_pic) {
        $record = $this->getMemberRecordById($memId);
        if ($this->input->post('first_name'))
            $this->db->set('first_name', $this->input->post('first_name'));
        if ($this->input->post('last_name'))
            $this->db->set('last_name', $this->input->post('last_name'));
        if ($this->input->post('company_name'))
            $this->db->set('company_name', $this->input->post('company_name'));
        if ($this->input->post('email'))
            $this->db->set('email', $this->input->post('email'));
        if ($this->input->post('password')) {
            $this->db->set('password', md5($this->input->post('password')));
            $this->db->set('tpass', $this->input->post('password'));
        }
        if ($this->input->post('dob'))
            $this->db->set('dob', strtotime($this->input->post('dob')));
        if ($this->input->post('user_type'))
            $this->db->set('user_type', $this->input->post('user_type'));
        if ($this->input->post('mobile'))
            $this->db->set('mobile_no', $this->input->post('mobile'));
        if ($this->input->post('country'))
            $this->db->set('country_id', $this->input->post('country'));
        if ($this->input->post('state'))
            $this->db->set('state_id', $this->input->post('state'));
        if ($this->input->post('city'))
            $this->db->set('city_id', $this->input->post('city'));
        if ($this->input->post('address'))
            $this->db->set('address', $this->input->post('address'));
        if ($this->input->post('mailing_address1'))
            $this->db->set('mailing_address1', $this->input->post('mailing_address1'));
        if ($this->input->post('address'))
            $this->db->set('mailing_address2', $this->input->post('mailing_address2'));
        if ($this->input->post('mailing_unit'))
            $this->db->set('mailing_unit', $this->input->post('mailing_unit'));
        if ($this->input->post('mailing_city'))
            $this->db->set('mailing_city', $this->input->post('mailing_city'));
        if ($this->input->post('mailing_country_id'))
            $this->db->set('mailing_country_id', $this->input->post('mailing_country_id'));
        if ($this->input->post('mailing_state_id'))
            $this->db->set('mailing_state_id', $this->input->post('mailing_state_id'));
        if ($profile_pic) {
            @unlink('./assets/uploads/member/' . $record->profile_image);
            $this->db->set('profile_image', $profile_pic);
        }
        $this->db->where('mem_id', $memId);
        $this->db->update($this->tableName);

        /*         * ***function if tenant is going to be updated by admin then update property detail also in other table ****** */
        if ($this->input->post('user_type') == 'tenant') {
            $this->db->set('property_id', $this->input->post('property'));
            $this->db->set('owner_id', $this->input->post('landlord'));
            $this->db->set('tenant_id', $mem_id);
//            $this->db->set('lease_start_date', strtotime($this->input->post('lease_start_date')));
//            $this->db->set('lease_end_date', strtotime($this->input->post('lease_end_date')));
            $this->db->set('lease_start_date', date('Y-m-d',strtotime($this->input->post('lease_start_date'))));
         $this->db->set('lease_end_date', date('Y-m-d',strtotime($this->input->post('lease_end_date'))));
            $this->db->set('due_date', $this->input->post('due_date'));
            $this->db->set('rent_amount', $this->input->post('rent_amount'));
            $this->db->set('late_fee', $this->input->post('late_fee'));
            $this->db->where('add_date', time());
            $this->db->update('tbl_properties_lease_detail');
        }
    }

    function getCountFilterAllRecords($type = NULL, $filterKey = NULL, $status = NULL, $sort_by = NULL) {
        if ($type != 'NULL') {
            $this->db->where('user_type', $type);
        }
        if ($filterKey != '' && $filterKey != 'NULL') {
            $where = "( first_name like '%" . $filterKey . "%' OR last_name like '%" . $filterKey . "%' OR email like '%" . $filterKey . "%' )";
            $this->db->where($where);
        }
        if ($status != '' && $status != 'NULL') {
            $this->db->where('status', $status);
        }
        if ($sort_by != 'NULL') {
            if ($sort_by == 'New')
                $this->db->order_by("mem_id", "desc");
            if ($sort_by == 'Old')
                $this->db->order_by("mem_id", "asc");
            if ($sort_by == 'Asc')
                $this->db->order_by("mem_id", "asc");
            if ($sort_by == 'Desc')
                $this->db->order_by("mem_id", "desc");
        }
        $query = $this->db->get($this->tableName);
        return $query->num_rows();
    }

    function getFilterAllRecords($type = NULL, $filterKey = NULL, $status = NULL, $sort_by = NULL, $num, $offset) {
        if ($type != 'NULL') {
            $this->db->where('user_type', $type);
        }
        if ($filterKey != '' && $filterKey != 'NULL') {
            $where = "( first_name like '%" . $filterKey . "%' OR last_name like '%" . $filterKey . "%' OR email like '%" . $filterKey . "%' )";
            $this->db->where($where);
        }
        if ($status != '' && $status != 'NULL') {
            $this->db->where('status', $status);
        }
        if ($sort_by != 'NULL') {
            if ($sort_by == 'New')
                $this->db->order_by("mem_id", "desc");
            if ($sort_by == 'Old')
                $this->db->order_by("mem_id", "asc");
            if ($sort_by == 'Asc')
                $this->db->order_by("mem_id", "asc");
            if ($sort_by == 'Desc')
                $this->db->order_by("mem_id", "desc");
        }
        $this->db->order_by('mem_id', 'desc');
        $query = $this->db->get($this->tableName, $num, $offset);
        return $query->result();
    }

    function getAllMembers() {
        $query = $this->db->get($this->tableName);
        return $query->result();
    }

    function performMultipleTasks($task, $iDs) {
        if ($task == 'delete') {
            foreach ($iDs as $value) {
                $record = $this->getMemberRecordById($value);
                if ($record->user_type == 'tenant') {
                    $this->db->set('account_closed_by_member', 'Yes');
                    $this->db->where('mem_id', $value);
                    $this->db->update($this->tableName);
                } else {
                    /*if(file_exists('assets/uploads/member/' . $record->profile_image)){
                        unlink('assets/uploads/member/' . $record->profile_image);                        
                    }*/
                    $this->db->where('mem_id', $value);
                    $this->db->delete($this->tableName);
                }
            }
            $message = 'Selected records has been deleted successfully';
            return $message;
        }
        if ($task == 'Active' || $task == 'Inactive') {
            foreach ($iDs as $value) {
                $this->db->set('status', $task);
                $this->db->where('mem_id', $value);
                $this->db->update($this->tableName);
            }
            $message = 'Selected records has been ' . $task . ' successfully';
            return $message;
        }
    }

    function performTask($task, $iD) {
        if ($task == 'delete') {
            $this->db->where('mem_id', $iD);
            $this->db->delete($this->tableName);
            $message = 'Selected record has been deleted successfully';
            return $message;
        }
        if ($task == 'Active' || $task == 'Inactive') {
            $this->db->set('status', $task);
            $this->db->where('mem_id', $iD);
            $this->db->update($this->tableName);
            $message = 'Selected record has been ' . $task . ' successfully';
            return $message;
        }
    }

    function deleteTenant($task, $iD) {
        if ($task == 'delete') {
            $this->db->set('account_closed_by_member', 'Yes');
            $this->db->where('mem_id', $iD);
            $this->db->update($this->tableName);
            $message = 'Selected record has been deleted successfully';
            return $message;
        }
    }

    /*     * ***************** Admin functions Ends here ************************* */

    /*     * ***************** Front functions Starts here ************************* */

    function checkLogin() {
        $this->db->where('email', $this->input->post('email'));
        $this->db->where('password', md5($this->input->post('password')));
        //$this->db->where('user_type','landlord');
        $query = $this->db->get($this->tableName);
        $record = $query->row();
        if (isset($record->mem_id) && $record->mem_id != '') {
            //~ if($record->verification_status == 'No')
            //~ {
            //~ $error_message= '<p>Your Account is not verified, Please verify your account first by clicking on the link in you mail.</p>';
            //~ }
            if ($record->user_type != 'landlord') {
                $error_message = '<p>Please check email/password.</p>';
            } else if ($record->status == 'Inactive') {
                $error_message = '<p>Your Login Status Is currently Inactive, Please Contact Admin For More Info.</p>';
            } else if ($record->account_closed_by_member == 'Yes') {
                $error_message = '<p>Your Account is inactive, Please contact to Admin to Activate your account again.</p>';
            } else {
                $this->session->set_userdata('MEM_ID', $record->mem_id);
                $error_message = '';
                //$this->last_login_model->doEntry();
            }
        } else {
            $error_message = '<p>Please check email/password.</p>';
        }
        return $error_message;
    }

    function register() {
        $strnew2 = 'abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $shuff2 = str_shuffle($strnew2);
        $verification_code = substr($shuff2, 0, 8);

        $this->db->set('first_name', $this->input->post('first_name'));
        $this->db->set('last_name', $this->input->post('last_name'));
        $slug = $this->common_model->create_unique_slug_for_common($this->input->post('first_name'), $this->tableName);
        $this->db->set('slug', $slug);
        if ($this->input->post('company_name'))
            $this->db->set('company_name', $this->input->post('company_name'));
        $this->db->set('email', $this->input->post('email'));
        if ($this->input->post('password')) {
            $this->db->set('password', md5($this->input->post('password')));
            $this->db->set('tpass', $this->input->post('password'));
        }
        if ($this->input->post('country_code'))
            $this->db->set('country_code', $this->input->post('country_code'));
        if ($this->input->post('mobile')) {
            $mob = $this->input->post('mobile');
            $this->db->set('mobile_no', $mob);
        }
        if ($this->input->post('country'))
            $this->db->set('country_id', $this->input->post('country'));
        if ($this->input->post('state'))
            $this->db->set('state_id', $this->input->post('state'));
        if ($this->input->post('city'))
            $this->db->set('city_id', $this->input->post('city'));
        $this->db->set('user_type', 'landlord');
        $this->db->set('verification_code', $verification_code);
        $this->db->set('add_date', time());
        $this->db->set('trial_end_date', date('Y-m-d', strtotime("+60 days")));
        $this->db->set('status', 'Active');
        $this->db->set('ip', $_SERVER['REMOTE_ADDR']);
        $this->db->insert($this->tableName);
        $mem_id = $this->db->insert_id();
        return $mem_id;
    }

    #====================Email verification code================================================================================#

    function doValidateEmail($code) {
        $current_time = time();
        $strnew2 = 'abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $shuff2 = str_shuffle($strnew2);
        $verification_code = substr($shuff2, 0, 8);

        $this->db->select('mem_id,status,verification_status');
        $this->db->where('verification_code', $code);
        $query = $this->db->get($this->tableName);
        $record = $query->row();
        if ($record->mem_id != '') {
            $this->db->set('status', 'Active');
            $this->db->set('verification_status', 'Yes');
            $this->db->set('verification_code', $verification_code);
            $this->db->where('mem_id', $record->mem_id);
            $this->db->update($this->tableName);
            return true;
        } else {
            return false;
        }
    }

    //########################### do logout ################################//

    function doLogout() {
        $this->session->unset_userdata('MEM_ID');
        $this->session->sess_destroy();
        if ($this->input->cookie('remember_me')) {
            $cookie = array('name' => 'remember_me');
            delete_cookie($cookie);
        }
    }

    //******************* Get Member Info by any field **************************//

    function getMemberInfo($field_name, $field_value) {
        $this->db->where($field_name, $field_value);
        $query = $this->db->get($this->tableName);
        $record = $query->row();
        return $record;
    }

    #==============Check Member Email Exist or Not =============================================================#

    public function checkEmail($email) {
        $email = strtolower($email);
        $this->db->select('mem_id,email,first_name,last_name,verification_status,verification_code,mobile_no');
        $this->db->where('email', $email);
        $this->db->where('status', 'Active');
        //$this->db->where('verification_status','Yes');
        $query = $this->db->get($this->tableName);
        $row = $query->row();
        return $row;
    }

    public function checkUsername($mem) {
        //$email = strtolower($email);
        $this->db->select('mem_id,email,first_name,last_name,verification_status,verification_code,mobile_no');
        $this->db->where('mem_id', $mem);
        $this->db->where('status', 'Active');
        //$this->db->where('verification_status','Yes');
        $query = $this->db->get($this->tableName);
        $row = $query->row();
        return $row;
    }

    public function customCheckUsername($mem) {
        //$email = strtolower($email);
        $this->db->select('mem_id,email,first_name,last_name,verification_status,verification_code,mobile_no');
        $this->db->where('mem_id', $mem);
        //$this->db->where('status', 'Active');
        //$this->db->where('verification_status','Yes');
        $query = $this->db->get($this->tableName);
        $row = $query->row();
        return $row;
    }
    #===============================================================================================#
    #==============Forgot Password ========================================================#

    public function forgotPassword() {
        $strnew = 'abcXdefgRhijklmnoYpqrstGuvwxJyz012345A6789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $shuff = str_shuffle($strnew);
        $newPassword = substr($shuff, 0, 8);
        $this->session->set_userdata('NEW_PASSWORD', $newPassword);

        $this->db->set('password', md5($newPassword));
        $this->db->set('tpass', $newPassword);
        $this->db->where('email', $this->input->post('email'));
        $this->db->update($this->tableName);
    }

    #==============Reset Password ========================================================#

    function resetPassword($verification_code) {
        $strnew2 = 'abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $shuff2 = str_shuffle($strnew2);
        $code = substr($shuff2, 0, 8);
        $this->db->select('mem_id,status,verification_status');
        $this->db->where('verification_code', $verification_code);
        $query = $this->db->get($this->tableName);
        $record = $query->row();
        if ($record->mem_id != '') {
            $this->db->set('password', md5($this->input->post('password')));
            $this->db->set('tpass', $this->input->post('password'));
            $this->db->set('verification_code', $code);
            $this->db->set('status', 'Active');
            $this->db->set('verification_status', 'Yes');
            $this->db->where('verification_code', $verification_code);
            $this->db->update($this->tableName);
            return $record->mem_id;
        } else {
            return false;
        }
    }

    /*     * *************** Get all vendors by owner ID *********************************** */

    function getAllVendors($owner_id) {
        $this->db->where('status', 'Active');
        $this->db->where('vendor_owner_id', $owner_id);
        $this->db->order_by('mem_id', 'desc');
        $query = $this->db->get($this->tableName);
        return $query->result();
    }

    /*     * ************** Add Vendor by landlord ***************************************** */

    function addVendor() {
        $this->db->set('company_name', $this->input->post('company_name'));
        $this->db->set('mobile_no', $this->input->post('company_contact'));
        $this->db->set('email', $this->input->post('email'));
        $this->db->set('service', $this->input->post('service'));
        $this->db->set('vendor_owner_id', $this->session->userdata('MEM_ID'));
        $this->db->set('user_type', 'vendor');
        $this->db->set('status', 'Active');
        $this->db->set('verification_status', 'Yes');
        $this->db->set('add_date', time());
        $this->db->set('ip', $_SERVER['REMOTE_ADDR']);
        $this->db->insert($this->tableName);
        $mem_id = $this->db->insert_id();
        return $mem_id;
    }

    /*     * ************** Update Vendor by landlord ***************************************** */

    function updateVendor($memId) {
        $this->db->set('company_name', $this->input->post('company_name'));
        $this->db->set('mobile_no', $this->input->post('company_contact'));
        $this->db->set('service', $this->input->post('service'));
        $this->db->set('email', $this->input->post('email'));
        $this->db->where('mem_id', $memId);
        $this->db->update($this->tableName);
    }

    function deleteRecordById($memId) {
        $this->db->where('mem_id', $memId);
        $this->db->delete($this->tableName);
    }

    function uploadProfilePic($memId, $profile_pic) {
        if ($profile_pic) {
            $record = $this->getMemberRecordById($memId);
            @unlink('./assets/uploads/member/' . $record->profile_image);
            $this->db->set('profile_image', $profile_pic);
        }
        $this->db->where('mem_id', $memId);
        $this->db->update($this->tableName);
    }

    function updateTenantRecord($memId, $lease_doc) {

        $this->db->set('lease_start_date', date('Y-m-d',strtotime($this->input->post('lease_start_date_'.$memId))));
        $this->db->set('lease_end_date', date('Y-m-d',strtotime($this->input->post('lease_end_date_'.$memId))));
        $this->db->set('rent_amount', $this->input->post('rent_amount_'.$memId));
        if ($this->input->post('late_fee_type_'.$memId) != '') {
            $this->db->set('late_fee_type', $this->input->post('late_fee_type_'.$memId));
            $late_fee = $this->input->post('late_fee_'.$memId);
        } else {
            $late_fee = 0;
        }
        $this->db->set('late_fee', $late_fee);
        $this->db->set('late_fee_start', $this->input->post('late_fee_start_'.$memId));
        $this->db->set('due_date', $this->input->post('due_date_'.$memId));
        $this->db->where('property_id', $this->input->post('property_id_'.$memId));
        $this->db->update($this->ownedPropertiestableName);


        $this->db->set('lease_start_date', date('Y-m-d',strtotime($this->input->post('lease_start_date_'.$memId))));
        $this->db->set('lease_end_date', date('Y-m-d',strtotime($this->input->post('lease_end_date_'.$memId))));
        $this->db->set('rent_amount', $this->input->post('rent_amount_'.$memId));
        $this->db->set('late_fee', $this->input->post('late_fee_'.$memId));
        $this->db->set('due_date', $this->input->post('due_date_'.$memId));
        $this->db->where('property_id', $this->input->post('property_id_'.$memId));
        $this->db->update($this->ownedPropertieshistorytableName);



        //***********update Entry in table members for tenant *********//
        $this->db->set('first_name', $this->input->post('first_name_'.$memId));
        $this->db->set('middle_name', $this->input->post('middle_name_'.$memId));
        $this->db->set('last_name', $this->input->post('last_name_'.$memId));
        $this->db->set('email', $this->input->post('email_'.$memId));
        $this->db->set('country_code', $this->input->post('ccode_'.$memId));
        $this->db->set('mobile_no', $this->input->post('mobile_no_'.$memId));
        $this->db->set('dob', strtotime($this->input->post('dob_'.$memId)));
        $this->db->set('preferred_language', $this->input->post('preferred_lang_'.$memId));
        $this->db->where('mem_id', $memId);
        $this->db->update('tbl_members');

        //***********update Entry in table property lease detail *********//
        $this->db->where('tenant_id', $memId);
        $this->db->where('property_id', $this->input->post('property_id_'.$memId));
//        $this->db->set('lease_start_date', strtotime($this->input->post('lease_start_date')));
//        $this->db->set('lease_end_date', strtotime($this->input->post('lease_end_date')));
        $this->db->set('lease_start_date', date('Y-m-d',strtotime($this->input->post('lease_start_date_'.$memId))));
         $this->db->set('lease_end_date', date('Y-m-d',strtotime($this->input->post('lease_end_date_'.$memId))));
        $this->db->set('rent_amount', $this->input->post('rent_amount_'.$memId));
        if ($this->input->post('late_fee_type_'.$memId) == '') {
            $lt_fee = 0;
        } else {
            $lt_fee = $this->input->post('late_fee_'.$memId);
        }
        $this->db->set('late_fee', $lt_fee);
        $this->db->set('late_fee_type', $this->input->post('late_fee_type_'.$memId));
        $this->db->set('late_fee_start', $this->input->post('late_fee_start_'.$memId));
        if ($lease_doc != '') {
            $this->db->set('lease_document', $lease_doc);
        }
        $this->db->set('due_date', $this->input->post('due_date_'.$memId));

        // Start Pets details        
        $this->db->set('pets', implode(",",$this->input->post('pets[]')));
        $this->db->set('pets_type',implode(",",$this->input->post('pets_type[]')));
        $this->db->set('pets_name',implode(",",$this->input->post('pets_name[]')));
        $this->db->set('pets_fee',implode(",",$this->input->post('pets_fee[]')));
        $this->db->set('pets_breed', implode(",",$this->input->post('pets_breed[]')));
        
        // END Pets details
        // Start Emergency Contact
        $this->db->set('emergency_name', $this->input->post('emergency_name_'.$memId));
        $this->db->set('emergency_phone', $this->input->post('emergency_phone_'.$memId));
        $this->db->set('emergency_email', $this->input->post('emergency_email_'.$memId));
        // END Emergency Contact
        $this->db->update($this->ownedPropertiestableName);

        //~ //***********update Entry in table property lease history *********//
        //~ $this->db->where('tenant_id',$memId);
        //~ $this->db->where('property_id',$this->input->post('property_id'));
        //~ $this->db->set('lease_start_date',strtotime($this->input->post('lease_start_date')));
        //~ $this->db->set('lease_end_date',strtotime($this->input->post('lease_end_date')));
        //~ $this->db->set('rent_amount',$this->input->post('rent_amount'));
        //~ $this->db->set('late_fee',$this->input->post('late_fee'));
        //~ $this->db->set('due_date',strtotime($this->input->post('due_date')));
        //~ $this->db->set('lease_document',$lease_doc);
        //~ $this->db->set('updated_by_owner_date',time());
        //~ $this->db->update($this->ownedPropertieshistorytableName);
    }

    function updateAllTenantRentAndLeaseInfo(){
        $this->db->set('lease_start_date', date('Y-m-d',strtotime($this->input->post('lease_start_date'))));
        $this->db->set('lease_end_date', date('Y-m-d',strtotime($this->input->post('lease_end_date'))));
        $this->db->set('rent_amount', $this->input->post('rent_amount'));
        if ($this->input->post('late_fee_type') != '') {
            $this->db->set('late_fee_type', $this->input->post('late_fee_type'));
            $late_fee = $this->input->post('late_fee');
        } else {
            $late_fee = 0;
        }
        $this->db->set('late_fee', $late_fee);
        $this->db->set('late_fee_start', $this->input->post('late_fee_start'));
        $this->db->set('due_date', $this->input->post('due_date'));
        $this->db->where('property_id', $this->input->post('property_id'));
        $this->db->update($this->ownedPropertiestableName);


        $this->db->set('lease_start_date', date('Y-m-d',strtotime($this->input->post('lease_start_date'))));
        $this->db->set('lease_end_date', date('Y-m-d',strtotime($this->input->post('lease_end_date'))));
        $this->db->set('rent_amount', $this->input->post('rent_amount'));
        $this->db->set('late_fee', $this->input->post('late_fee'));
        $this->db->set('due_date', $this->input->post('due_date'));
        $this->db->where('property_id', $this->input->post('property_id'));
        $this->db->update($this->ownedPropertieshistorytableName);

    }

    function addTenant($file_name,$verification_code=false,$newPassword=false,$slug=false) {
        if($newPassword == false){
            $strnew2 = 'abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $shuff2 = str_shuffle($strnew2);
            $shuff = str_shuffle($strnew2);
            $verification_code = substr($shuff2, 0, 8);
            $newPassword = substr($shuff, 0, 8);            
        }
        $this->db->set('occupied_status', 'occupied');
        $this->db->where('prop_id', $this->input->post('property_id'));
        $this->db->update('tbl_properties');

        //*********** Entry in table members for new tenant *********//
        $this->db->set('first_name', $this->input->post('first_name'));
        $this->db->set('middle_name', $this->input->post('middle_name'));
        $this->db->set('last_name', $this->input->post('last_name'));
        if($slug == false){
            $slug = $this->common_model->create_unique_slug_for_common($this->input->post('first_name'), 'tbl_members');
        }
        $this->db->set('slug', $slug);
        $this->db->set('email', $this->input->post('email'));
        $this->db->set('password', md5($newPassword));
        $this->db->set('tpass', $newPassword);
        $this->db->set('country_code', '+1');
        $this->db->set('mobile_no', $this->input->post('mobile_no'));
        $this->db->set('dob', strtotime($this->input->post('dob')));
        $this->db->set('preferred_language', $this->input->post('preferred_language'));
        $this->db->set('user_type', 'tenant');
        $this->db->set('add_date', time());
        $this->db->set('status', 'Active');
        $this->db->set('verification_status', 'Yes');
        $this->db->set('verification_code', $verification_code);
        $this->db->set('ip', $_SERVER['REMOTE_ADDR']);
        $this->db->insert('tbl_members');
        $tenant_id = $this->db->insert_id();
        //echo $this->db->last_query();die;
        //*********** Entry in table property lease detail *********//
        $this->db->set('property_id', $this->input->post('property_id'));
        $this->db->set('property_unique_id', $this->common_model->getSingleFieldFromAnyTable('property_code', 'prop_id', $this->input->post('property_id'), 'tbl_properties'));
        $this->db->set('owner_id', $this->session->userdata('MEM_ID'));
        $this->db->set('tenant_id', $tenant_id);
//        $this->db->set('lease_start_date', strtotime($this->input->post('lease_start_date')));
//        $this->db->set('lease_end_date', strtotime($this->input->post('lease_end_date')));
        $this->db->set('lease_start_date', date('Y-m-d',strtotime($this->input->post('lease_start_date'))));
         $this->db->set('lease_end_date', date('Y-m-d',strtotime($this->input->post('lease_end_date'))));
        $this->db->set('rent_amount', $this->input->post('rent_amount'));
        if ($this->input->post('late_fee_type') != '') {
            $this->db->set('late_fee_type', $this->input->post('late_fee_type'));
            $late_fee = $this->input->post('late_fee');
        } else {
            $late_fee = 0;
        }
        $this->db->set('late_fee', $late_fee);
        $this->db->set('late_fee_start', $this->input->post('late_fee_start'));
        $this->db->set('due_date', $this->input->post('due_date'));
        $this->db->set('occupie_status', 'running');

        // Start Pets details        
        $this->db->set('pets', implode(",",$this->input->post('pets[]')));
        $this->db->set('pets_type',implode(",",$this->input->post('pets_type[]')));
        $this->db->set('pets_breed',implode(",",$this->input->post('pets_breed[]')));
        $this->db->set('pets_name',implode(",",$this->input->post('pets_name[]')));
        $this->db->set('pets_fee', implode(",",$this->input->post('pets_fee[]')));
        
        // END Pets details
        // Start Emergency Contact
        $this->db->set('emergency_name', $this->input->post('emergency_name'));
        $this->db->set('emergency_phone', $this->input->post('emergency_phone'));
        $this->db->set('emergency_email', $this->input->post('emergency_email'));
        // END Emergency Contact

        $this->db->set('add_date', time());
        $this->db->insert($this->ownedPropertiestableName);

        //entry in lease doc 
        if ($file_name != '') {
            $this->db->set('lease_doc', $file_name);
            $this->db->set('property_id', $this->input->post('property_id'));
            $this->db->set('tenant_id', $tenant_id);
            $this->db->set('type', 1);
            $this->db->set('add_date', date('Y-m-d'));
            $this->db->insert('tbl_lease_docs');
        }

        //*********** Entry in table property lease history *********//
        $this->db->set('property_id', $this->input->post('property_id'));
        $this->db->set('property_unique_id', $this->common_model->getSingleFieldFromAnyTable('property_code', 'prop_id', $this->input->post('property_id'), 'tbl_properties'));
        $this->db->set('owner_id', $this->session->userdata('MEM_ID'));
        $this->db->set('tenant_id', $tenant_id);
//        $this->db->set('lease_start_date', strtotime($this->input->post('lease_start_date')));
//        $this->db->set('lease_end_date', strtotime($this->input->post('lease_end_date')));
        $this->db->set('lease_start_date', date('Y-m-d',strtotime($this->input->post('lease_start_date'))));
         $this->db->set('lease_end_date', date('Y-m-d',strtotime($this->input->post('lease_end_date'))));
        $this->db->set('rent_amount', $this->input->post('rent_amount'));
        $this->db->set('late_fee', $this->input->post('late_fee'));
        $this->db->set('due_date', $this->input->post('due_date'));
        if ($file_name != '') {
            //$this->db->set('lease_document', $data['upload_data']['file_name']);
        }
        $this->db->set('updated_by_owner_date', time());



        $this->db->set('add_date', time());
        $this->db->insert($this->ownedPropertieshistorytableName);
        return $tenant_id;
    }

    function frontAddMAilingAddress() {
        if ($this->input->post('address_1')) {
            $this->db->set('mailing_address1', $this->input->post('address_1'));
            if ($this->input->post('address_2')) {
                $this->db->set('mailing_address2', $this->input->post('address_2'));
            }
            $this->db->set('mailing_unit', $this->input->post('unit'));
            $this->db->set('mailing_city', $this->input->post('city'));
            //$this->db->set('mailing_country_id', $this->input->post('country'));
            $this->db->set('mailing_state_id', $this->input->post('state'));
            $this->db->set('mailing_zip', $this->input->post('zip'));
            $this->db->where('mem_id', $this->session->userdata('MEM_ID'));
            $this->db->update('tbl_members');
        }return true;
    }

}
