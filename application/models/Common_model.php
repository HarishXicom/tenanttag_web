<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Common_model extends CI_Model {

    function __construct() {
        parent::__construct();
        //~ $url	=	$_SERVER['REQUEST_URI'];
        //~ $urlData	=	parse_url($url);
        //~ if(substr_count($urlData['path'],'admin'))
        //~ $this->checkAdminLogin();
        $ci = get_instance();
        $ci->load->config('config');
        $this->load->library('user_agent');

        $this->db->select('site_name,site_logo,page_size_front,page_size_admin,facebook_url,google_url,twitter_url,youtube_url,contact_address,contact_number,contact_email,email_goes_from_name,email_goes_from_email,copyright_text');
        $this->db->where('id', 1);
        $query = $this->db->get('tbl_wesite_config');
        $row = $query->row();

        $ci->config->set_item('site_name', $row->site_name);
        $ci->config->set_item('site_logo', $row->site_logo);
        $ci->config->set_item('page_size_admin', $row->page_size_admin);
        $ci->config->set_item('page_size_front', $row->page_size_front);
        $ci->config->set_item('facebook_url', $row->facebook_url);
        $ci->config->set_item('google_url', $row->google_url);
        $ci->config->set_item('twitter_url', $row->twitter_url);
        $ci->config->set_item('youtube_url', $row->youtube_url);
        $ci->config->set_item('contact_address', $row->contact_address);
        $ci->config->set_item('contact_number', $row->contact_number);
        $ci->config->set_item('contact_email', $row->contact_number);
        $ci->config->set_item('email_goes_from_name', $row->email_goes_from_name);
        $ci->config->set_item('email_goes_from_email', $row->email_goes_from_email);
        $ci->config->set_item('copyright_text', $row->copyright_text);
    }

    function checkAdminLogin() {
        if (!$this->session->userdata('ADM_ID'))
            redirect('admin/login');
    }

    function checkMemberLogin() {
        if ($this->session->userdata('MEM_ID') == '') {
            //~ $this->session->set_userdata('REDIRECT_URL',current_url());
            //~ $this->session->set_flashdata('PAGE_ERROR_MESSAGE', "Please login to access this page");
            redirect(site_url(''));
        }
    }

    function getSingleFieldFromAnyTable($field_name, $condition_column, $condition_value, $table) {
        $this->db->where($condition_column, $condition_value);
        $query = $this->db->get($table);
        $data = $query->row();
        if(isset($data->$field_name)){
            return $data->$field_name;
        }
        return '';
    }

    #=============Function Create Unique Slug===========================================================#

    function create_unique_slug_for_common($app_title, $table) {
        $slug = url_title($app_title);
        $slug = strtolower($slug);
        $i = 0;
        $params = array();
        $params['slug'] = $slug;
        while ($this->db->where($params)->get($table)->num_rows()) {
            if (!preg_match('/-{1}[0-9]+$/', $slug)) {
                $slug .= '-' . ++$i;
            } else {
                $slug = preg_replace('/[0-9]+$/', ++$i, $slug);
            }
            $params ['slug'] = $slug;
        }
        $app_title = $slug;
        return $app_title;
    }

    function showLimitedText($string, $len) {

        $string = strip_tags($string);
        if (strlen($string) > $len)
            $string = substr($string, 0, $len - 3) . "...";
        return $string;
    }

    #======================================================================================================#

    function getAllAdminNotificationsById($memberId, $read_status) {
        $this->db->where('notification_for', 'Admin');
        $this->db->where('notification_to', $memberId);
        $this->db->where('read_status', $read_status);
        $this->db->order_by('id', 'desc');
        $query = $this->db->get('tbl_notifications');
        $data = $query->result();
        return $data;
    }

    #==============For converting time in string like (12 hours ago)=============================================#

    function Myago($tm, $rcs = 0) {
        $cur_tm = time();
        $dif = $cur_tm - $tm;
        $agoText = '';
        $agoProcess = 0;

        //$lngh = array(1,60,3600,86400,604800,2630880,31570560);
        $difYear = floor($dif / 31570560);
        $difYearRemaining = $dif % 31570560;
        if ($difYear > 0) {
            $agoText.=$difYear . ' Years ';
            $agoProcess++;
        }

        $difMonth = floor($difYearRemaining / 2630880);
        $difMonthRemaining = $difYearRemaining % 2630880;
        if ($difMonth > 0) {
            $agoText.=$difMonth . ' Months ';
            $agoProcess++;
            if ($agoProcess == 2)
                return $agoText;
        }

        $difWeek = floor($difMonthRemaining / 604800);
        $difWeekRemaining = $difMonthRemaining % 604800;
        if ($difWeek > 0) {
            $agoText.=$difWeek . ' Weeks ';
            $agoProcess++;
            if ($agoProcess == 2)
                return $agoText;
        }

        $difDay = floor($difWeekRemaining / 86400);
        $difDayRemaining = $difWeekRemaining % 86400;
        if ($difDay > 0) {
            $agoText.=$difDay . ' Days ';
            $agoProcess++;
            if ($agoProcess == 2)
                return $agoText;
        }

        $difHour = floor($difDayRemaining / 3600);
        $difHourRemaining = $difDayRemaining % 3600;
        if ($difHour > 0) {
            $agoText.=$difHour . ' Hours ';
            $agoProcess++;
            if ($agoProcess == 2)
                return $agoText;
        }

        $difMinute = floor($difHourRemaining / 60);
        $difSecondRemaining = $difHourRemaining % 60;
        if ($difMinute > 0) {
            $agoText.=$difMinute . ' Minutes ';
            $agoProcess++;
            if ($agoProcess == 2)
                return $agoText;
        }

        $agoText.=$difSecondRemaining . ' Second';
        return $agoText;
    }

    function getLastProeprty() {
        $this->db->select_max('prop_id');
        $this->db->where('owner_id', $this->session->userdata('MEM_ID'));
        $Q = $this->db->get('tbl_properties');
        $row = $Q->row_array();
        if (isset($row['prop_id']) && !empty($row['prop_id'])) {
            $prop_id = $row['prop_id'];
            $step_completed = $this->getSingleFieldFromAnyTable('step_completed', 'prop_id', $prop_id, 'tbl_properties');
        } else {
            $step_completed = 0;
        }
        //print_R($step_completed); exit;
        // $redirctUrl = '';
        /* if ($step_completed != 0)
          $this->session->set_userdata('PROPERTY_ID', $prop_id); */
        return $step_completed;
    }


    function getHigestCompletedStepProeprty() {
        $this->db->select_max('step_completed');
        $this->db->where('owner_id', $this->session->userdata('MEM_ID'));
        $Q = $this->db->get('tbl_properties');
        $row = $Q->row_array();
        if(isset($row['step_completed']) && !empty($row['step_completed'])){
            $step_completed = $row['step_completed'];
        }else{
            $step_completed = 0;
        }
        return $step_completed;
    }

    public function insert_data($conditions, $params) {
        if (empty($params['table']) && empty($conditions)) {
            return;
        }
        if (!empty($params['batch_mode']) && $params['batch_mode'] == true) {
            $this->db->insert_batch($params['table'], $conditions);
        } else {
            $this->db->insert($params['table'], $conditions);
        }
        if ($this->db->insert_id()) {
            return $this->db->insert_id();
        } else {
            return false;
        }
    }

    public function get_tenants_list_for_notification() {


        $curtimestamp = time();
        $after_one_day = date('j',strtotime("+1 day"));
        $after_two_days = date('j',strtotime("+2 day"));
        $after_three_days = date('j',strtotime("+3 day"));

        $after_sixty_days = date('Y-m-d',strtotime("+60 day"));
        $after_thirty_days = date('Y-m-d',strtotime("+30 day"));
        $after_two_weeks = date('Y-m-d',strtotime("+14 day"));
//echo "2 days ".$after_two_days."<br/>3days ".$after_three_days."<br/>60-".$after_sixty_days."<br/>30-".$after_thirty_days."<br/>14-".$after_two_weeks;
        //$where = array('due_date >= ' => $curtimestamp,'due_date <=' => $after_three_days);
        //$this->db->where($where);

       // $this->db->or_where(array('due_date' => $after_two_days, 'due_date' => $after_three_days));
/*        $this->db->or_where(array('due_date' => $after_one_day));
        $this->db->or_where(array('due_date' => $after_two_days));
        $this->db->or_where(array('due_date' => $after_three_days));
        $q = $this->db->get('tbl_properties_lease_detail');
        $rentdata = $q->result_array();
        $data['rentusers'] = $rentdata;*/


        $query_due = "SELECT `tbl_properties_lease_detail`.* FROM `tbl_properties_lease_detail` JOIN `tbl_members` ON `tbl_members`.`mem_id` = `tbl_properties_lease_detail`.`tenant_id` WHERE (`tbl_properties_lease_detail`.`due_date` = $after_one_day OR `tbl_properties_lease_detail`.`due_date` = $after_two_days OR `tbl_properties_lease_detail`.`due_date` = $after_three_days) AND `tbl_members`.`status` = 'Active'";
        $q = $this->db->query($query_due);
        $rentdata = $q->result_array();
        $data['rentusers'] = $rentdata;


        //$where = array('lease_end_date >= ' => $curtimestamp,'lease_end_date <=' => $after_sixty_days);
        //$this->db->where($where);

        //$this->db->or_where(array('lease_end_date' => $after_sixty_days, 'lease_end_date' => $after_thirty_days, 'lease_end_date' => $after_two_weeks));
        /* $this->db->or_where(array('lease_end_date' => $after_sixty_days));
        $this->db->or_where(array('lease_end_date' => $after_thirty_days));
        $this->db->or_where(array('lease_end_date' => $after_two_weeks));
        $q = $this->db->get('tbl_properties_lease_detail');*/
        $query_lease_end = "SELECT `tbl_properties_lease_detail`.* FROM `tbl_properties_lease_detail` JOIN `tbl_members` ON `tbl_members`.`mem_id` = `tbl_properties_lease_detail`.`tenant_id` WHERE (`tbl_properties_lease_detail`.`lease_end_date` = $after_sixty_days OR `tbl_properties_lease_detail`.`lease_end_date` = $after_thirty_days OR `tbl_properties_lease_detail`.`lease_end_date` = $after_two_weeks) AND `tbl_members`.`status` = 'Active'";
        $q = $this->db->query($query_lease_end);
        $leasedata = $q->result_array();
        $data['leaseusers'] = $leasedata;
        return $data;
    }


    public function get_tenants_birthday_list() {
        $date_current = date('Y-m-d');
        $m = date("m");
        $d1 = date("d");
        $query_lease_end = "SELECT `tbl_linked_account_info`.* FROM  `tbl_linked_account_info` where dob like '%-$m-$d1' AND `is_deleted` = 0";

        $q = $this->db->query($query_lease_end);
        $users_info = $q->result_array();
        //echo $this->db->last_query(); print_r($users_info); exit;
        return $users_info;
    }

    public function get_tenants_pet_list() {
        $date_current = date('Y-m-d');
        $m = date("m");
        $d1 = date("d");
        $query_lease_end = "SELECT `tbl_members`.`first_name`,`tbl_members`.`last_name`,`tbl_members`.`email`,`tbl_members`.`mobile_no`,`tbl_properties_lease_detail`.property_id,`tbl_properties_lease_detail`.`tenant_id`,`tbl_properties_lease_detail`.`owner_id`,`tbl_properties_lease_detail`.`pets_type` FROM `tbl_properties_lease_detail` inner join tbl_properties on tbl_properties.prop_id=tbl_properties_lease_detail.property_id INNER join tbl_members on tbl_members.mem_id=tbl_properties_lease_detail.tenant_id where tbl_properties_lease_detail.pets_type <> 'None' AND tbl_properties_lease_detail.pets_type is not null and tbl_properties_lease_detail.pets_type <> '' And tbl_properties.step_completed=4 and tbl_members.move_status <> 0";

        $q = $this->db->query($query_lease_end);
        $users_info = $q->result_array();
        //echo $this->db->last_query(); print_r($users_info); exit;
        return $users_info;
    }
    public function get_data($conditions, $params = null) {

        if (!empty($params['complex']) && $params['complex'] == true) {
            if (!empty($conditions['on']) && !empty($conditions['tables'])) {

                foreach ($conditions['on'] as $key => $value) {
                    if (!empty($value['join']) && $value['join'] == 'left') {
                        $this->db->join("{$conditions['tables'][$key]} AS $key", "$key.{$value['column']} {$value['sign']} {$value['alias_other']}.{$value['alias_column']}", 'left');
                    } else if (!empty($value['join']) && $value['join'] == 'right') {
                        $this->db->join("{$conditions['tables'][$key]} AS $key", "$key.{$value['column']} {$value['sign']} {$value['alias_other']}.{$value['alias_column']}", 'right');
                    } else {
                        $this->db->join("{$conditions['tables'][$key]} AS $key", "$key.{$value['column']} {$value['sign']} {$value['alias_other']}.{$value['alias_column']}");
                    }
                }
                if (!empty($conditions['where'])) {
                    foreach ($conditions['where'] as $key => $value) {
                        $val = $value['value'];
                        $join_key = "{$value['alias']}.$key";
                        if (!empty($val) && is_array($val)) {
                            $val['operator'] = !empty($val['operator']) ? $val['operator'] : 'AND';
                            if ($val['operator'] == 'AND') {
                                if ($val['sign'] == 'LIKE') {
                                    $this->db->like($join_key, $val['value']);
                                } else if ($val['sign'] == 'NULL') {
                                    $this->db->where("{$join_key} IS NULL");
                                } else if ($val['sign'] == 'NOT NULL') {
                                    $this->db->where("{$join_key} IS NOT NULL");
                                } else if ($val['sign'] == 'spl_where') {
                                    $this->db->where($val['value'], NULL, FALSE);
                                } else {
                                    $this->db->where($join_key . ' ' . $val['sign'], $val['value']);
                                }
                            } else if ($val['operator'] == 'OR') {
                                if ($val['sign'] == 'LIKE') {
                                    $this->db->or_like($join_key, $val['value']);
                                } else if ($val['sign'] == 'NULL') {
                                    $this->db->or_where("{$join_key} IS NULL");
                                } else if ($val['sign'] == 'spl_where') {
                                    $this->db->where($val['value'], NULL, FALSE);
                                } else {
                                    $this->db->or_where($join_key . ' ' . $val['sign'], $val['value']);
                                }
                            } else if ($val['operator'] == 'IN') {
                                $this->db->where_in($join_key, $val['value']);
                            }
                        } else {
                            $this->db->where($join_key, $val);
                        }
                    }
                }
                if (!empty($conditions['having'])) {
                    foreach ($conditions['having'] as $key => $value) {
                        //$val = $value['having'];
                        $val = $value;
                        $join_key = $key;
                        if (!empty($val) && is_array($val)) {
                            $val['operator'] = !empty($val['operator']) ? $val['operator'] : 'AND';
                            if ($val['operator'] == 'AND') {

                                if ($val['sign'] == 'NULL') {
                                    $this->db->having("{$join_key} IS NULL");
                                } else if ($val['sign'] == 'NOT NULL') {
                                    $this->db->having("{$join_key} IS NOT NULL");
                                } else if ($val['sign'] == 'spl_having') {
                                    $this->db->having($val['value'], NULL, FALSE);
                                } else {
                                    $this->db->having($join_key . ' ' . $val['sign'], $val['value']);
                                }
                            } else if ($val['operator'] == 'OR') {
                                if ($val['sign'] == 'NULL') {
                                    $this->db->or_having("{$join_key} IS NULL");
                                } else if ($val['sign'] == 'spl_where') {
                                    $this->db->or_having($val['value'], NULL, FALSE);
                                } else {
                                    $this->db->or_having($join_key . ' ' . $val['sign'], $val['value']);
                                }
                            }
                        } else {
                            $this->db->having($join_key, $val);
                        }
                    }
                }

                if (isset($params['limit']) && isset($params['offset'])) {
                    $this->db->limit($params['limit'], $params['offset']);
                }
                if (!empty($params['fields'])) {
                    $this->db->select($params['fields']);
                }
                if (!empty($params['order_by'])) {
                    $this->db->order_by($params['order_by']);
                }
                if (!empty($params['group_by'])) {
                    $this->db->group_by($params['group_by']);
                }
                $table_key = $conditions['table'];
                $table_name = $conditions['tables'][$table_key];
                if (isset($params['print_query']) && $params['print_query'] == true) {
                    $query = $this->db->get("{$table_name} AS {$table_key}");
                    echo $this->db->last_query();
                    die();
                }
                if (isset($params['cnt']) && $params['cnt'] == 1) {
                    return $this->db->count_all_results("{$table_name} AS {$table_key}");
                }
                $query = $this->db->get("{$table_name} AS {$table_key}");

                if (!empty($params['single_row']) && $params['single_row'] == true) {
                    $result = $query->row();
                    $query->free_result();
                    return $result;
                }
                $result = $query->result();
                $query->free_result();
                return $result;
            }
        } else {
            if (empty($params) && empty($params['table'])) {
                return;
            }
            if (!empty($conditions)) {
                foreach ($conditions as $key => $val) {
                    if (!empty($val) && is_array($val)) {
                        $val['operator'] = !empty($val['operator']) ? $val['operator'] : 'AND';
                        if ($val['operator'] == 'AND') {
                            if ($val['sign'] == 'LIKE') {
                                $this->db->like($val['key'], $val['value']);
                            } else {
                                $this->db->where($val['key'] . ' ' . $val['sign'], $val['value']);
                            }
                        } else if ($val['operator'] == 'OR') {
                            if ($val['sign'] == 'LIKE') {
                                $this->db->or_like($val['key'], $val['value']);
                            } else {
                                $this->db->or_where($val['key'] . ' ' . $val['sign'], $val['value']);
                            }
                        }else if ($val['operator'] == 'IN') {
                                //$valIN = $val['value'];
                                $this->db->where_in($val['key'], $val['value']);
                        }
                    } else if (isset($params['word']) && !empty($params['word'])) {
                        $this->db->where($conditions['orwhere']);
                    } else {
                        $this->db->where($key, $val);
                    }
                }
            }
            if (isset($params['cnt']) && $params['cnt'] == true) {
                return $this->db->count_all_results($params['table']);
            }
            if (!empty($params['fields'])) {
                $this->db->select($params['fields']);
            }
            if (!empty($params['group_by'])) {
                $this->db->group_by($params['group_by']);
            }
            if (isset($params['limit']) && isset($params['offset'])) {
                $this->db->limit($params['limit'], $params['offset']);
            }
            if (!empty($params['order_by'])) {
                $this->db->order_by($params['order_by']);
            }
            if (isset($params['single_row']) && $params['single_row'] == true) {
                $query = $this->db->get($params['table']);
                $result = $query->row();
                $query->free_result();
                return $result;
            }
            $query = $this->db->get($params['table']);
            //echo $this->db->last_query();
            //die();
            $result = $query->result();
            $query->free_result();
            return $result;
        }
    }

    public function get_countries() {
        $query = $this->db->get("barf_countries");
        $result = $query->result();
        $query->free_result();
        return $result;
    }

    public function get_country($country_id) {
        if (empty($country_id)) {
            return false;
        }
        $this->db->where("id", $country_id);
        $query = $this->db->get("barf_bar_category");
        $result = $query->row();
        $query->free_result();
        return $result;
    }

    public function update($conditions, $params) {
        if (empty($conditions['value']) && empty($params['table'])) {
            return;
        }
        $params['batch_mode'] = isset($params['batch_mode']) ? $params['batch_mode'] : false;
        if (!empty($conditions['where'])) {
            if (is_array($conditions['where'])) {
                foreach ($conditions['where'] as $key => $value) {
                    if (is_array($value)) {
                        $this->db->where_in($key, $value);
                    } elseif (!empty($value)) {
                        $this->db->where($key, $value);
                    }
                }
            }
        }
        if ($params['batch_mode'] === false) {
            $this->db->update($params['table'], $conditions['value']);
        }
    }

    public function delete($conditions, $params) {
        if (empty($conditions) && empty($params['table'])) {
            return;
        }
        $this->db->delete($params['table'], $conditions);
    }

    public function getCellNumbers($ids) {
        $this->db->select("p.property_id,m.mobile_no,p.tenant_id");
        $this->db->from('tbl_members as m');
        $this->db->join('tbl_properties_lease_detail as p', 'p.tenant_id = m.mem_id');
        $this->db->where_in('m.mem_id', $ids);
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    public function getTenants($ids) {
        $this->db->select('DISTINCT(tenant_id)');
        $this->db->from("tbl_properties_lease_detail");
        $this->db->where_in('property_id', $ids);
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    public function getMobileNumbers($ids) {
        $this->db->select("m.mobile_no,m.mem_id");
        $this->db->from('tbl_members as m');
        $this->db->where_in('m.mem_id', $ids);
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    public function checkAmenity($prop, $amen) {
        $this->db->select('count(prop_id) as cnt');
        $this->db->from('tbl_properties');
        $this->db->where("FIND_IN_SET('$amen',amenties) !=", 0);
        $this->db->where('prop_id', $prop);
        $query = $this->db->get();
        $result = $query->row();
        return $result;
    }

    public function checkService($prop_id, $col, $match) {
        if ($col == 'heat_type') {
            $this->db->select('heat_filter_size as cnt');
        } elseif ($col == 'ac_type') {
            $this->db->select('ac_filter_size as cnt');
        } else {
            $this->db->select('count(prop_id) as cnt');
        }
        $this->db->from('tbl_properties');
        $this->db->where("$col !=", $match);
        $this->db->where('prop_id', $prop_id);
        $query = $this->db->get();
        $result = $query->row();
        return $result;
    }

    public function getSubscribersCSV() {
        $this->db->select('email');
        $this->db->from('tbl_subscribers');
        $query = $this->db->get();
        return $query;
    }
    function getPaymentByMonth($all_tenants,$month){
        $all_tenants = implode(',', $all_tenants);
        $query = $this->db->query("SELECT  sum(amount) as total_amount FROM `tbl_payment` WHERE status='PAID' AND mem_id IN ( $all_tenants ) AND MONTH(created_date) = $month");   
        //echo $this->db->last_query(); exit; 
        $info = $query->row();
        $PaymentByMonth = $info->total_amount;
        /************* canceled payment *******************/
        /*$query = $this->db->query("SELECT  sum(amount) as total_amount FROM `tbl_payment` WHERE status='Refund' AND mem_id IN ( $all_tenants ) AND MONTH(created_date) = $month");   
        $info = $query->row();
        $canceledPayment =  $info->total_amount;
        echo $PaymentByMonth .'-'. $canceledPayment; exit;
        $PaymentByMonth = $PaymentByMonth - $canceledPayment;*/
        return $PaymentByMonth;
    }

     function getPaymentByMonth1($user_id,$month){
        $query = $this->db->query("SELECT  tbl_payment.*, sum(amount) as total_amount FROM `tbl_payment` WHERE mem_id=$user_id AND MONTH(created_date) = $month");   
        //echo $this->db->last_query(); exit; 
        $info = $query->row();
        return $info;
    }
    public function getResources(){
        $this->db->select('*');
        $this->db->from('tbl_resources_docs');
        $query = $this->db->get();
        return $query->result_array();        
    }
    public function getQuestions($type = false){
        $this->db->select('tbl_questions.* ,tbl_question_category.category_name');
        if($type != false){
            $this->db->where('type', $type);
        }
        $this->db->from('tbl_questions'); 
        $this->db->join('tbl_question_category', 'tbl_question_category.id=tbl_questions.category_id', 'inner');
        $this->db->order_by('tbl_questions.id', 'desc');
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();        
    }

    public function getSurveyAnswer($id){
        $this->db->select('`tbl_survey_answers`.*,`tbl_questions`.question,`tbl_members`.first_name,`tbl_members`.last_name');
        $this->db->from('tbl_survey_answers');
        $this->db->join('tbl_questions', 'tbl_questions.id=tbl_survey_answers.question_id', 'inner');
        $this->db->join('tbl_members', 'tbl_members.mem_id=tbl_survey_answers.user_id', 'inner');
        $this->db->where('question_id', $id);
        $query = $this->db->get();
        return $query->result_array(); 
    }


    public function getPropertySubscriptionStatus($user_id,$property_id){
        //$query = $this->db->query("SELECT id,mem_id,FROM_UNIXTIME(`date`) timestamp FROM `tbl_payment` WHERE date >= UNIX_TIMESTAMP(LAST_DAY(CURDATE()) + INTERVAL 1 DAY - INTERVAL 1 MONTH) AND date < UNIX_TIMESTAMP(LAST_DAY(CURDATE()) + INTERVAL 1 DAY) AND property_id=$property_id AND mem_id=$user_id");    
        if(checkTrialTimePeriod($this->session->userdata('MEM_ID')) == false){
            $month  = date('m');
            $year   = date('Y');
            $current_timestamp = time();
            $query  = $this->db->query("SELECT * FROM `tbl_payment` WHERE FIND_IN_SET($property_id,property_id) AND mem_id=$user_id order by id desc limit 1");    
            $info   = $query->row(); 
            //print_r($info); exit; 
            if(!empty($info)){
                $last_payment_timestamp = $info->date;
                $next_payment_timestamp =  strtotime("+1 month", $last_payment_timestamp); 
                //echo date('Y-m-d H:i:s', $next_payment_timestamp) .'>='. date('Y-m-d H:i:s', $current_timestamp).'<br>'; 
                //echo $next_payment_timestamp .'>='. $current_timestamp; exit;
                if($next_payment_timestamp >= $current_timestamp){
                    $result = true;
                }else{
                    $result = false;
                }
            }else{
                $result = false;
            }
        }else{
            $result = true;
        }
        return $result;
    }



    public function getPropertySubscriptionDetail($user_id,$property_id){
        //$query = $this->db->query("SELECT id,mem_id,FROM_UNIXTIME(`date`) timestamp FROM `tbl_payment` WHERE date >= UNIX_TIMESTAMP(LAST_DAY(CURDATE()) + INTERVAL 1 DAY - INTERVAL 1 MONTH) AND date < UNIX_TIMESTAMP(LAST_DAY(CURDATE()) + INTERVAL 1 DAY) AND property_id=$property_id AND mem_id=$user_id");    
        $month  = date('m');
        $year   = date('Y');
        $current_timestamp = time();
        $query  = $this->db->query("SELECT `id`,`date` as last_subscription_date,`created_date`  FROM `tbl_payment` WHERE FIND_IN_SET($property_id,property_id) AND mem_id=$user_id order by id desc limit 1");    
        $info   = $query->row();    
        if(!empty($info)){
            $info->next_subscription_date = date('Y-m-d', strtotime("+1 month", $info->last_subscription_date));
            $info->last_subscription_date = date('Y-m-d', $info->last_subscription_date);
        }
        return $info;
    }


    function get_customer_saved_card($user_id,$prop_id){
        $conditions = $params = array();
        $params['complex'] = true;
        $conditions['tables'] = array(
            'stripe_card_info' => 'tbl_stripe_card_info',
            'properties' => 'tbl_properties'
        );
        $conditions['table'] = 'stripe_card_info';
        $conditions['on']['properties'] = array(
            'sign' => '=',
            'column' => 'prop_id',
            'alias_column' => 'property_id',
            'alias_other' => 'stripe_card_info',
        );

        $conditions['where']['user_id']['alias'] = 'stripe_card_info';
        $conditions['where']['user_id']['value'] = $user_id;

        $conditions['where']['status']['alias'] = 'properties';
        $conditions['where']['status']['value'] = 'Active';

        $conditions['where']['prop_id']['alias'] = 'properties';
        $conditions['where']['prop_id']['value'] = $prop_id;

        $conditions['where']['disable_card']['alias'] = 'stripe_card_info';
        $conditions['where']['disable_card']['value'] = 0;

        $conditions['where']['stripe_customer_id']['alias'] = 'stripe_card_info';
        $conditions['where']['stripe_customer_id']['value'] =  array(
                                                                    'operator' => 'AND',
                                                                    'key' => 'stripe_customer_id',
                                                                    'sign' => '!=',
                                                                    'value' => ''
                                                                );
        $params['fields'] = array('stripe_card_info.*','properties.city as property_city','properties.address1 as property_address1','properties.address2 as property_address2');
        $params['single_row'] = true;
        $stripe_cards = $this->common_model->get_data($conditions, $params);
        
        return $stripe_cards; 
    }


}
