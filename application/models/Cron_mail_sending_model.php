<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cron_mail_sending_model extends CI_Model {

    var $from = "";
    var $sendermail = "xicom.biz";
    var $sitelogo = '';
    var $site_title = '';
    var $contact_email = '';
    var $mailtype = 'html';

    function __construct() {
        $this->db->select('contact_email,email_goes_from_name,email_goes_from_email,site_name,site_logo,copyright_text');
        $this->db->where('id', '1');
        $query = $this->db->get('tbl_wesite_config');
        $row = $query->row();
        $this->sitelogo = '<img src="' . base_url() . 'assets/uploads/site_logo/' . $row->site_logo . '" height="100" width="800">';
        $this->from = $row->email_goes_from_name;
        $this->senderemail = $row->email_goes_from_email;
        $this->site_title = $row->site_name;
        $this->contact_email = $row->contact_email;
        $this->copyright = $row->copyright_text;
        $this->adminEmail = $row->contact_email;
    }



    function send_aminity_mail($msg) {
        
        $template = $this->templatechoose(8);
        if (!empty($template)) {            
            $str = 'Status: '.' '.$msg;
            $this->email->from($this->senderemail, $this->from);
            $this->email->to(array('lakhvinder.singh@xicom.biz','ankit.chhabra@xicom.biz'));
            $this->email->mailtype = $this->mailtype;
            $this->email->subject('CRON Job Testing: Amenities Messages');
            $this->email->message($str);
            return $this->email->send();
        } else {
            return TRUE;
        }
    }

  

    function templatechoose($id) {
        $this->db->where('et_id', $id);
        $this->db->where('status', 'Active');
        $query = $this->db->get('tbl_email_templates');
        $row = $query->row();
        return $row;
    }



}
