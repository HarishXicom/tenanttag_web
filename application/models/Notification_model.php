<?php
class Notification_model extends CI_Model
{
	
	var $tableName='tbl_notifications';
	
	function addNotification($notification,$notification_to,$notification_from)
	{
		$this->db->set('notification_for','Admin');
		$this->db->set('notification_to',$notification_to);
		$this->db->set('notification_from',$notification_from);
		$this->db->set('notification',$notification);
		$this->db->set('add_date',time());
		$this->db->set('read_status','Unread');
		$this->db->set('ip',$_SERVER['REMOTE_ADDR']);
		$this->db->insert($this->tableName);
     }
	 
	function changeReadStatus($notification_for)
	{
		$this->db->set('read_status','Read');
		$this->db->where('notification_for',$notification_for);
		$this->db->update($this->tableName);
     }
    
	function getAllAdminNotificationsById($memberId,$read_status)
	{
		$this->db->where('notification_for','Admin');
		$this->db->where('notification_to',$memberId);
		$this->db->where('read_status',$read_status);
		$this->db->order_by('id','desc');
		$query	=	$this->db->get($this->tableName);
		$data = $query->result();
		return $data;
	}
	
	
	
}

?>
