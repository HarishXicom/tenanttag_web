<?php

class Mailsending_Model extends CI_Model {

    var $from = "";
    var $sendermail = "xicom.biz";
    var $sitelogo = '';
    var $site_title = '';
    var $contact_email = '';
    var $mailtype = 'html';

    function __construct() {
        $this->db->select('contact_email,email_goes_from_name,email_goes_from_email,site_name,site_logo,copyright_text');
        $this->db->where('id', '1');
        $query = $this->db->get('tbl_wesite_config');
        $row = $query->row();
        $this->sitelogo = '<img src="' . base_url() . 'assets/uploads/site_logo/' . $row->site_logo . '" height="80" width="200">';
        $this->from = $row->email_goes_from_name;
        $this->senderemail = $row->email_goes_from_email;
        $this->site_title = $row->site_name;
        $this->contact_email = $row->contact_email;
        $this->copyright = $row->copyright_text;
        $this->adminEmail = $row->contact_email;
    }

    function sendTransactionMail($user_name, $user_email,$amount, $status,$bank_info,$transaction_id) {
        $template = $this->templatechoose(18);
        if (!empty($template)) {
            $str = str_replace("{#logo}", $this->sitelogo, $template->description);
            $str = str_replace("{#name}", ucfirst($user_name), $str);
            $str = str_replace("{#amount}", $amount, $str);
            $str = str_replace("{#status}", $status, $str);
            $str = str_replace("{#transaction_id}", $transaction_id, $str);
            $str = str_replace("{#bank_info}", $bank_info, $str);
            $str = str_replace("{#site_title}", $this->site_title, $str);
            $str = str_replace("{#copyright_text}", $this->copyright, $str);
            $this->email->from($this->senderemail, $this->from);
            $this->email->to($user_email);
            $this->email->mailtype = $this->mailtype;
            $this->email->subject($template->subject);
            $this->email->message($str);
            return $this->email->send();
        } else {
            return TRUE;
        }
    }


    function send_test_tmaintenance_email($email) {
        $memberRecord = $this->member_model->getMemberRecordByAnyField($email, 'email');
        $template = $this->templatechoose(11);
        if (!empty($template)) {
            $str = str_replace("{#logo}", $this->sitelogo, $template->description);
            $str = str_replace("{#name}", ucfirst($memberRecord->first_name) . ' ' . $memberRecord->last_name, $str);
            $str = str_replace("{#subject}", ucfirst($this->input->post('subject')), $str);
            //$str = str_replace("{#message}", $this->input->post('message'), $str);
            $str = str_replace("{#site_title}", $this->site_title, $str);
            $str = str_replace("{#copyright_text}", $this->copyright, $str);
            $this->email->from($this->senderemail, $this->from);
            $this->email->to($email);
            $this->email->mailtype = $this->mailtype;
            $this->email->subject($template->subject);
            $this->email->message($str);
            $this->email->send();
        }
        //echo $this->email->print_debugger();die;
        return TRUE;
        //echo"<pre>";print_r($str);die;
        //$this->email->clear();    
    }

    function sendMailToLandlord($data) {
        $template = $this->templatechoose(19);
        if (!empty($template)) {
            $str = str_replace("{#logo}", $this->sitelogo, $template->description);
            $str = str_replace("{#name}", $data['name'], $str);
            $str = str_replace("{#text}", ucfirst($data['text']), $str);
            //$str = str_replace("{#message}", $this->input->post('message'), $str);
            $str = str_replace("{#site_title}", $this->site_title, $str);
            $str = str_replace("{#copyright_text}", $this->copyright, $str);
            $this->email->from($this->senderemail, $this->from);
            $this->email->to($data['email']);
            $this->email->mailtype = $this->mailtype;
            $this->email->subject($template->subject);
            $this->email->message($str);
            $this->email->send();
        }
        //echo"<pre>";print_r($data); //die;
        //echo $this->email->print_debugger();die;
        $this->email->clear();    
        return TRUE;
    }

    function testmailsend($email,$message = false) {
        $memberRecord = $this->member_model->getMemberRecordByAnyField($email, 'email');
        $template = $this->templatechoose(11);
        if (!empty($template)) {
            $str = str_replace("{#logo}", $this->sitelogo, $template->description);
            $str = str_replace("{#name}", ucfirst($memberRecord->first_name) . ' ' . $memberRecord->last_name, $str) . ' = '. $message;
            $str = str_replace("{#subject}", ucfirst($this->input->post('subject')), $str);
            //$str = str_replace("{#message}", $this->input->post('message'), $str);
            $str = str_replace("{#site_title}", $this->site_title, $str);
            $str = str_replace("{#copyright_text}", $this->copyright, $str);
            $this->email->from($this->senderemail, $this->from);
            $this->email->to($email);
            $this->email->mailtype = $this->mailtype;
            $this->email->subject($template->subject);
            $this->email->message($str);
            $this->email->send();
        }
        echo $this->email->print_debugger();die;
        return TRUE;
        //echo"<pre>";print_r($str);die;
        //$this->email->clear();    
    }

    function sendSynapseUserStatusEmail($email,$message = false) {
        $memberRecord = $this->member_model->getMemberRecordByAnyField($email, 'email');
        $template = $this->templatechoose(20);
        if (!empty($template)) {
            $str = str_replace("{#logo}", $this->sitelogo, $template->description);
            $str = str_replace("{#name}", ucfirst($memberRecord->first_name) . ' ' . $memberRecord->last_name, $str) . ' = '. $message;
            $str = str_replace("{#subject}", ucfirst($this->input->post('subject')), $str);
            $str = str_replace("{#message}", $message, $str);
            $str = str_replace("{#site_title}", $this->site_title, $str);
            $str = str_replace("{#copyright_text}", $this->copyright, $str);
            $this->email->from($this->senderemail, $this->from);
            $this->email->to($email);
            $this->email->mailtype = $this->mailtype;
            $this->email->subject($template->subject);
            $this->email->message($str);
            $this->email->send();
        }
        return TRUE;
        //echo"<pre>";print_r($str);die;
        //echo $this->email->print_debugger();die;
        //$this->email->clear();    
    }

    function sendSynapseBankAccountStatusEmail($email,$message = false) {
        $memberRecord = $this->member_model->getMemberRecordByAnyField($email, 'email');
        $template = $this->templatechoose(21);
        if (!empty($template)) {
            $str = str_replace("{#logo}", $this->sitelogo, $template->description);
            $str = str_replace("{#name}", ucfirst($memberRecord->first_name) . ' ' . $memberRecord->last_name, $str) . ' = '. $message;
            $str = str_replace("{#subject}", ucfirst($this->input->post('subject')), $str);
            $str = str_replace("{#message}", $message, $str);
            $str = str_replace("{#site_title}", $this->site_title, $str);
            $str = str_replace("{#copyright_text}", $this->copyright, $str);
            $this->email->from($this->senderemail, $this->from);
            $this->email->to($email);
            $this->email->mailtype = $this->mailtype;
            $this->email->subject($template->subject);
            $this->email->message($str);
            $this->email->send();
        }
        return TRUE;
        //echo"<pre>";print_r($str);die;
        //echo $this->email->print_debugger();die;
        //$this->email->clear();    
    }
    function sendMailToRegistereduser($email) {
        $memberRecord = $this->member_model->getMemberRecordByAnyField($email, 'email');
        $template = $this->templatechoose(1);
        if (!empty($template)) {
            $str = str_replace("{#logo}", $this->sitelogo, $template->description);
            $str = str_replace("{#name}", ucfirst($memberRecord->first_name) . ' ' . $memberRecord->last_name, $str);
            $str = str_replace("{#subject}", ucfirst($this->input->post('subject')), $str);
            $str = str_replace("{#message}", $this->input->post('message'), $str);
            $str = str_replace("{#site_title}", $this->site_title, $str);
            $str = str_replace("{#copyright_text}", $this->copyright, $str);
            $this->email->from($this->senderemail, $this->from);
            $this->email->to($email);
            $this->email->mailtype = $this->mailtype;
            $this->email->subject($template->subject);
            $this->email->message($str);
            $this->email->send();
        }
        return TRUE;
        //echo"<pre>";print_r($str);die;
        //echo $this->email->print_debugger();die;
        //$this->email->clear();	
    }

    function adminforgotPasswordEmail($memberRec) {
        $template = $this->templatechoose(4);
        if (!empty($template)) {
            $newPassword = $this->session->userdata('NEW_PASSWORD');
            $this->session->unset_userdata('NEW_PASSWORD');
            $str = str_replace("{#logo}", $this->sitelogo, $template->description);
            $str = str_replace("{#name}", $memberRec->name, $str);
            $str = str_replace("{#username}", $memberRec->username, $str);
            $str = str_replace("{#email}", $memberRec->email, $str);
            $str = str_replace("{#password}", $newPassword, $str);
            $str = str_replace("{#site_title}", $this->site_title, $str);
            $str = str_replace("{#site_url}", site_url(), $str);
            $str = str_replace("{#copyright_text}", $this->copyright, $str);
            $this->email->from($this->senderemail, $this->from);
            $this->email->to($memberRec->email);
            $this->email->mailtype = $this->mailtype;
            $this->email->subject($template->subject);
            $this->email->message($str);
            return $this->email->send();
        } else {
            return TRUE;
        }
    }

    function userforgotPasswordEmail($memberRec) {
        $template = $this->templatechoose(7);
        //~ $newPassword=$this->session->userdata('NEW_PASSWORD');
        //~ $this->session->unset_userdata('NEW_PASSWORD');
        $password_reset_link = '<a href="' . site_url('reset-password/' . $memberRec->verification_code) . '"> Click here to reset your password.</a>';
        if (!empty($template)) {
            $str = str_replace("{#logo}", $this->sitelogo, $template->description);
            $str = str_replace("{#name}", $memberRec->first_name . ' ' . $memberRec->last_name, $str);
            $str = str_replace("{#email}", $memberRec->email, $str);
            $str = str_replace("{#password}", $password_reset_link, $str);
            $str = str_replace("{#site_title}", $this->site_title, $str);
            $str = str_replace("{#site_url}", site_url(), $str);
            $str = str_replace("{#copyright_text}", $this->copyright, $str);
            $this->email->from($this->senderemail, $this->from);
            $this->email->to($memberRec->email);
            $this->email->mailtype = $this->mailtype;
            $this->email->subject($template->subject);
            $this->email->message($str);
            $this->email->send();
        }
        $msg = 'Click ' . site_url('reset-password/' . $memberRec->verification_code) . ' to reset password for TenantTag';
        $number = "+1" . preg_replace("/[^0-9]/", "", $memberRec->mobile_no);
        $this->load->helper('twilio_helper');  // references library/twilio_helper.php
        $service = get_twilio_service();
        try {
            $service->account->sms_messages->create("+19046743077", "$number", "$msg", array());
            return TRUE;
        } catch (Exception $e) {
            $file = 'sms_error_logs.txt';
            if (file_exists($file)) {
                $current = file_get_contents($file);
                $current .= $e->getMessage() . "\n";
                file_put_contents($file, $current);
            }
            return TRUE;
        }
    }

    function contactUsReply() {
        $template = $this->templatechoose(3);
        if (!empty($template)) {
            $str = str_replace("{#logo}", $this->sitelogo, $template->description);
            $str = str_replace("{#name}", ucfirst($this->input->post('name')), $str);
            $str = str_replace("{#user_enquiry}", ucfirst($this->input->post('user_enquiry')), $str);
            $str = str_replace("{#message}", $this->input->post('message'), $str);
            $str = str_replace("{#site_title}", $this->site_title, $str);
            $str = str_replace("{#copyright_text}", $this->copyright, $str);
            $this->email->from($this->senderemail, $this->from);
            $this->email->to($this->input->post('email'));
            $this->email->mailtype = $this->mailtype;
            $this->email->subject($template->subject);
            $this->email->message($str);
            return $this->email->send();
        } else {
            return TRUE;
        }
    }

    function registrationEmail($member_id) {
        $this->db->where('mem_id', $member_id);
        $query = $this->db->get('tbl_members');
        $memberRec = $query->row();
        $template = $this->templatechoose(5);
        //$verificatin_link='<a href="'.site_url('login/validate/'.$memberRec->verification_code).'"> '.site_url('login/validate/'.$memberRec->verification_code).'</a>';

        if (!empty($template)) {
            $str = str_replace("{#logo}", $this->sitelogo, $template->description);
            $str = str_replace("{#name}", $memberRec->first_name . ' ' . $memberRec->last_name, $str);
            $str = str_replace("{#email}", $memberRec->email, $str);
            $str = str_replace("{#password}", $this->session->userdata('NEW_PASSWORD'), $str);
            $str = str_replace("{#site_title}", $this->site_title, $str);
            $str = str_replace("{#copyright_text}", $this->copyright, $str);
            $this->email->from($this->senderemail, $this->from);
            $this->email->to($memberRec->email);
            $this->email->mailtype = $this->mailtype;
            $this->email->subject($template->subject);
            $this->email->message($str);
            return $this->email->send();
        } else {
            return TRUE;
        }
    }

    function landlordRegistrationFromFrontEmail($member_id) {
        $this->db->where('mem_id', $member_id);
        $query = $this->db->get('tbl_members');
        $memberRec = $query->row();
        $template = $this->templatechoose(6);
        $verificatin_link = '<a href="' . site_url('register/validate/' . $memberRec->verification_code) . '"> ' . site_url('login/validate/' . $memberRec->verification_code) . '</a>';
        if (!empty($template)) {
            $str = str_replace("{#logo}", $this->sitelogo, $template->description);
            $str = str_replace("{#name}", $memberRec->first_name . ' ' . $memberRec->last_name, $str);
            $str = str_replace("{#email}", $memberRec->email, $str);
            $str = str_replace("{#password}", "******", $str);
            $str = str_replace("{#verificatin_link}", $verificatin_link, $str);
            $str = str_replace("{#site_title}", $this->site_title, $str);
            $str = str_replace("{#copyright_text}", $this->copyright, $str);
            $this->email->from($this->senderemail, $this->from);
            $this->email->to($memberRec->email);
            $this->email->mailtype = $this->mailtype;
            $this->email->subject($template->subject);
            $this->email->message($str);
            return $this->email->send();
        } else {
            return TRUE;
        }
    }

    function userResetSucessEmail($member_id) {
        $this->db->where('mem_id', $member_id);
        $query = $this->db->get('tbl_members');
        $memberRec = $query->row();
        $template = $this->templatechoose(8);
        if (!empty($template)) {
            $str = str_replace("{#logo}", $this->sitelogo, $template->description);
            $str = str_replace("{#name}", $memberRec->first_name . ' ' . $memberRec->last_name, $str);
            $str = str_replace("{#site_title}", $this->site_title, $str);
            $str = str_replace("{#copyright_text}", $this->copyright, $str);
            $this->email->from($this->senderemail, $this->from);
            $this->email->to($memberRec->email);
            $this->email->mailtype = $this->mailtype;
            $this->email->subject($template->subject);
            $this->email->message($str);
            return $this->email->send();
        } else {
            return TRUE;
        }
    }

    function sendMaintenanceMails($emails, $info, $msg, $service) {
        $address = $info->address1 . ' ' . $info->address2 . ' ' . $info->city . ', ' . $info->region_name . ' ' . $info->zip;
        $this->load->helper('email');
        $this->load->library('email');
        
        $template = $this->templatechoose(9);
        if (!empty($template)) {
            $str = str_replace("{#logo}", $this->sitelogo, $template->description);
            $str = str_replace("{#landlord_name}", ucfirst($info->lfname) . ' ' . $info->llname, $str);
            $str = str_replace("{#landlord_phone}", $info->lmobile, $str);
            $str = str_replace("{#property_address}", $address, $str);
            $str = str_replace("{#tenant_name}", ucfirst($info->tfname) . ' ' . $info->tlname, $str);
            $str = str_replace("{#tenant_phone}", $info->tmobile, $str);
            $str = str_replace("{#type}", $service, $str);
            $str = str_replace("{#message}", $msg, $str);
            $str = str_replace("{#site_title}", $this->site_title, $str);
            $str = str_replace("{#copyright_text}", $this->copyright, $str);
           // $this->email->initialize($config);
            $this->email->from($this->senderemail, $this->from);
            $this->email->to("");
            $this->email->bcc($emails);
            $this->email->subject($template->subject);
            $this->email->message($str);
            $this->email->send();
          //  echo $this->email->print_debugger();die;
            $this->email->clear();
            
        }
        return TRUE;
    }

    function templatechoose($id) {
        $this->db->where('et_id', $id);
        $this->db->where('status', 'Active');
        $query = $this->db->get('tbl_email_templates');
        $row = $query->row();
        return $row;
    }

    function contactNewLandlord($name, $email, $msg) {
        $template = $this->templatechoose(10);
        if (!empty($template)) {
            $str = str_replace("{#logo}", $this->sitelogo, $template->description);
            $str = str_replace("{#name}", ucfirst($name), $str);
            $str = str_replace("{#message}", $msg, $str);
            $str = str_replace("{#site_title}", $this->site_title, $str);
            $str = str_replace("{#copyright_text}", $this->copyright, $str);
            $this->email->from($this->senderemail, $this->from);
            $this->email->to($email);
            $this->email->mailtype = $this->mailtype;
            $this->email->subject($template->subject);
            $this->email->message($str);
            return $this->email->send();
        } else {
            return TRUE;
        }
    }

    function tenantEmailUpdate($name, $email, $msg) {
        $template = $this->templatechoose(10);
        if (!empty($template)) {
            $str = str_replace("{#logo}", $this->sitelogo, $template->description);
            $str = str_replace("{#name}", ucfirst($name), $str);
            $str = str_replace("{#message}", $msg, $str);
            $str = str_replace("{#site_title}", $this->site_title, $str);
            $str = str_replace("{#copyright_text}", $this->copyright, $str);
            $this->email->from($this->senderemail, $this->from);
            $this->email->to($email);
            $this->email->mailtype = $this->mailtype;
            $this->email->subject($template->subject);
            $this->email->message($str);
            return $this->email->send();
        } else {
            return TRUE;
        }
    }

    

    function sentPaymentInfo($name, $email, $msg) {
        $template = $this->templatechoose(17);
        if (!empty($template)) {
            $str = str_replace("{#logo}", $this->sitelogo, $template->description);
            $str = str_replace("{#name}", ucfirst($name), $str);
            $str = str_replace("{#message}", $msg, $str);
            $str = str_replace("{#site_title}", $this->site_title, $str);
            $str = str_replace("{#copyright_text}", $this->copyright, $str);
            $this->email->from($this->senderemail, $this->from);
            $this->email->to($email);
            $this->email->mailtype = $this->mailtype;
            $this->email->subject($template->subject);
            $this->email->message($str);
            return $this->email->send();
        } else {
            return TRUE;
        }
    }

}
