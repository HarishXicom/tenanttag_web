<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Properties_model extends CI_Model {

    var $tableName = 'tbl_properties';
    var $ownedPropertiestableName = 'tbl_properties_lease_detail';
    var $ownedPropertieshistorytableName = 'tbl_properties_lease_history';

    function __construct() {
        parent::__construct();
    }

    function getLnt($loc) {
        $url = "http://maps.googleapis.com/maps/api/geocode/json?address=
" . urlencode($loc) . "&sensor=false";
        $result_string = file_get_contents($url);
        $result = json_decode($result_string, true);
        $result1[] = isset($result['results'][0]) ? $result['results'][0] : '';
        $result2[] = isset($result1[0]['geometry']) ? $result1[0]['geometry'] : '';
        $result3[] = isset($result2[0]['location']) ? $result2[0]['location'] : '';
        return isset($result3[0]) ? $result3[0] : '';
    }

    /*     * ***************** Admin functions Starts here ************************* */

    function getDetailByTenantId($id) {
        $this->db->where('tenant_id', $id);
        $query = $this->db->get($this->ownedPropertiestableName);
        return $query->row();
    }

    function getAllPropertiesByLandlordId($id) {
        $this->db->where('owner_id', $id);
        $this->db->where('step_completed', '3');
        //$this->db->where('status','Active');
        //$this->db->where('occupied_status','vacant');
        $query = $this->db->get('tbl_properties');
        return $query->result();
    }

    /*     * ***************** Admin functions Ends here ************************* */

    /*     * ***************** Front functions Starts here ************************* */

    function occupiedProperties($memId) {
        $this->db->select('tbl_properties_lease_detail.property_id as prop_id');
        $this->db->join('tbl_members', 'tbl_members.mem_id=tbl_properties_lease_detail.tenant_id', 'inner');
        $this->db->join('tbl_properties', 'tbl_properties.prop_id=tbl_properties_lease_detail.property_id', 'inner');
        $this->db->where('tbl_properties_lease_detail.owner_id', $memId);
        $this->db->where('tbl_members.move_status != 2');
        $this->db->where('tbl_members.account_closed_by_member = "No"');
        $this->db->where('tbl_properties.status = "Active"');
        $this->db->group_by('tbl_properties_lease_detail.property_id');
        $query = $this->db->get($this->ownedPropertiestableName);
        $cnt = $query->result();
        return $cnt;
    }

    function frontGetCountAllPropertiesByLandlordId($memId, $occupied_status = NULL) {
        if ($occupied_status != 'NULL' && $occupied_status != '')
            $this->db->where('occupied_status', $occupied_status);
        $this->db->where('owner_id', $memId);
        $this->db->where('status', 'Active');
        //$this->db->where('step_completed','3');
        $query = $this->db->get('tbl_properties');
        return $query->num_rows();
    }

    function frontGetAllPropertiesByLandlordId($memId, $occupied_status = NULL) {
        if ($occupied_status != 'NULL' && $occupied_status != '')
            $this->db->where('occupied_status', $occupied_status);
        $this->db->where('owner_id', $memId);
        $this->db->where('status', 'Active');
        //$this->db->where('step_completed','3');
        $query = $this->db->get('tbl_properties');
        //echo $this->db->last_query();die;
        return $query->result();
    }
    function getPaymentInfoByPropertyId($property_id) {
        $this->db->where('property_id', $property_id);
        $query = $this->db->get('tbl_payment');
        return $query->row();
    }

    public function frontGetAllPropertiesByType($memId, $inids, $type) {
        //occupied
        if ($type == 'occupied' && !empty($inids)) {
            $this->db->where_in('prop_id', $inids);
        } elseif($type == 'vacant' && !empty($inids)) {
            $this->db->where_not_in('prop_id', $inids);
        }
        $this->db->where('owner_id', $memId);
        $this->db->where('status', "Active");
        $query = $this->db->get('tbl_properties');
        return $query->result();
    }

    function frontAddProperty($allAmenties, $community_doc) {
        $str = 'AGRTYSNBHJKLQUIONGGHJKL1234567890LPAZXWQRPLKGTFVMXE';
        $shuffle = str_shuffle($str);
        $unique_code = substr($shuffle, 0, 8);
        $loc = $latitude = $longitude = '';
        /* Geolocation */
        if ($this->input->post('address1')) {
            $loc .= $this->input->post('address1') . " ";
        }
        if ($this->input->post('address2')) {
            $loc .= $this->input->post('address2') . " ";
        }
        if ($this->input->post('city')) {
            $loc .= $this->input->post('city') . " ";
        }
        if ($this->input->post('state')) {
            $stname = get_state_name($this->input->post('state'));
            $loc .=$stname . " ";
        }
        if ($this->input->post('zip')) {
            $loc .= $this->input->post('zip');
        }
        $val = $this->getLnt($loc);
        if (!empty($val['lat']) && !empty($val['lng'])) {
            $latitude = $val['lat'];
            $longitude = $val['lng'];
        }
        /* end */

        $this->db->set('owner_id', $this->session->userdata('MEM_ID'));
        $this->db->set('property_code', '#' . $unique_code . '_' . $this->session->userdata('MEM_ID'));
        $this->db->set('country_id', $this->input->post('country'));
        $this->db->set('state_id', $this->input->post('state'));
        $this->db->set('city', $this->input->post('city'));
        $this->db->set('address1', $this->input->post('address1'));
        $this->db->set('address2', $this->input->post('address2'));
        $this->db->set('zip', $this->input->post('zip'));
        $this->db->set('unit_number', $this->input->post('unit'));
        $this->db->set('property_type', $this->input->post('property_type'));
        $this->db->set('latitude', $latitude);
        $this->db->set('longitude', $longitude);
        if ($this->input->post('parking'))
            $this->db->set('parking', $this->input->post('parking'));
        if ($this->input->post('area'))
            $this->db->set('size', $this->input->post('area'));
        if ($this->input->post('build_year'))
            $this->db->set('year_built', $this->input->post('build_year'));
        $this->db->set('community_status', $this->input->post('is_community'));
        if ($this->input->post('is_community') == 'Yes') {
            $this->db->set('community', $this->input->post('community'));
            $this->db->set('gate_code', $this->input->post('gate_code'));
            $this->db->set('unit_po_box', $this->input->post('po_box'));
            $this->db->set('community_managing_company', $this->input->post('community_company'));
            $this->db->set('community_contact_number', $this->input->post('company_number'));
        }
        $this->db->set('garbage_day', $this->input->post('garbage_pick_day'));
        $this->db->set('recycle_day', $this->input->post('recycle_pick_day'));
        $this->db->set('yard_waste_day', $this->input->post('yard_waste_pick_day'));
        $this->db->set('safety_equipment1', $this->input->post('safety_equipment1'));
        $this->db->set('safety_equipment2', $this->input->post('safety_equipment2'));
        $this->db->set('heat_type', $this->input->post('heat_type'));
        if ($this->input->post('heat_filter_size') == 'Other') {
            $this->db->set('heat_filter_size', $this->input->post('heat_filter_size_other'));
        } else {
            $this->db->set('heat_filter_size', $this->input->post('heat_filter_size'));
        }
        $this->db->set('ac_type', $this->input->post('ac_type'));
        if ($this->input->post('ac_filter_size_other') == 'Other') {
            $this->db->set('ac_filter_size', $this->input->post('ac_filter_size_other'));
        } else {
            $this->db->set('ac_filter_size', $this->input->post('ac_filter_size'));
        }
        $this->db->set('pest_control_home', $this->input->post('pest_control_home'));
        if ($this->input->post('pest_service_provider')) {
            $this->db->set('pest_service_provider', $this->input->post('pest_service_provider'));
            $this->db->set('pest_service_provider_number', $this->input->post('pest_service_provider_number'));
        }
        $this->db->set('yard', $this->input->post('yard'));
        $this->db->set('yard_service_provided', $this->input->post('yard_service_provided'));
        if ($this->input->post('yard_service_provider')) {
            $this->db->set('yard_service_provider', $this->input->post('yard_service_provider'));
            $this->db->set('yard_service_provider_number', $this->input->post('yard_service_provider_number'));
        }
        $selectedAmenties = array();
        foreach ($allAmenties as $value) {
            if ($this->input->post($value->name))
                $selectedAmenties[] = $this->input->post($value->name);
        }
        $amenties = implode(',', $selectedAmenties);
        $this->db->set('amenties', $amenties);
        $this->db->set('tmessaging', '');
        $this->db->set('tmaintenance', '');
        $this->db->set('occupied_status', 'vacant');
        $this->db->set('step_completed', 1);
        if ($community_doc != '') {
            $this->db->set('community_doc', $community_doc);
        }
        $this->db->set('add_date', time());
        $this->db->set('status', 'Active');
        $this->db->set('ip', $_SERVER['REMOTE_ADDR']);
        $this->db->insert($this->tableName);
        $prop_id = $this->db->insert_id();

        /*         * ******** Adding provided services in tbl_property_service ***************** */
        //~ $this->db->set('service_id',$this->input->post('service'));
        //~ $this->db->set('company',$this->input->post('service_provider'));
        //~ $this->db->set('contact',$this->input->post('service_provider_number'));
        //~ $this->db->set('property_id',$prop_id);
        //~ $this->db->insert('tbl_property_services');
        $oldServices = $this->input->post('service');
        $oldservice_provider = $this->input->post('service_provider');
        $oldservice_provider_number = $this->input->post('service_provider_number');
        if(!empty($oldServices)){
            foreach ($oldServices as $key => $val) {
                if ($oldServices[$key]) {
                    $this->db->set('service_id', $oldServices[$key]);
                    $this->db->set('company', $oldservice_provider[$key]);
                    $this->db->set('contact', $oldservice_provider_number[$key]);
                    $this->db->set('property_id', $prop_id);
                    $this->db->insert('tbl_property_services');
                }
            }            
        }


        /*         * ******** Adding more provided services in tbl_service and tbl_property_service ***************** */
        for ($i = 0; $i <= $this->input->post('new_created_services'); $i++) {
            if ($this->input->post('new_service_' . $i)) {
                $this->db->set('service', $this->input->post('new_service_' . $i));
                $slug = $this->common_model->create_unique_slug_for_common($this->input->post('new_service_' . $i), 'tbl_services');
                $this->db->set('slug', $slug);
                $this->db->set('added_by', 'landlord');
                $this->db->set('added_by_id', $this->session->userdata('MEM_ID'));
                $this->db->set('add_date', time());
                $this->db->set('status', 'Active');
                $this->db->set('ip', $_SERVER['REMOTE_ADDR']);
                $this->db->insert('tbl_services');
                $service_id = $this->db->insert_id();
                if ($service_id) {
                    $this->db->set('service_id', $service_id);
                    $this->db->set('company', $this->input->post('new_service_provider_' . $i));
                    $this->db->set('contact', $this->input->post('new_service_provider_number_' . $i));
                    $this->db->set('property_id', $prop_id);
                    $this->db->insert('tbl_property_services'); 
                }
            }
        }
        return $prop_id;
    }

    function frontAddTenantsToProperty($prop_id, $data) {
        //************ updating property detail ************//
        
        $this->db->set('occupied_status', 'vacant');
        $this->db->set('step_completed', 2);
        $this->db->where('prop_id', $prop_id);
        $this->db->update($this->tableName);
        if ($this->input->post('is_vacant') != 'Yes') {
            for ($i = 1; $i <= $this->input->post('total_tenant'); $i++) {
                $strnew = 'abcXdefgRhijklmnoYpqrstGuvwxJyz012345A6789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
                $shuff = str_shuffle($strnew);
                $newPassword = substr($shuff, 0, 8);

                $this->db->set('occupied_status', 'occupied');
                $this->db->where('prop_id', $prop_id);
                $this->db->update($this->tableName);

                //*********** Entry in table members for new tenant *********//
                if ($this->input->post('is_vacant') != 'Yes') {
                    /********************** updateAllTenantRentAndLeaseInfo*******************/
                    $this->db->set('lease_start_date', date('Y-m-d',strtotime($this->input->post('lease_start_date_' . $i))));
                    $this->db->set('lease_end_date', date('Y-m-d',strtotime($this->input->post('lease_end_date_' . $i))));
                    $this->db->set('rent_amount', $this->input->post('rent_amount_' . $i));
                    if ($this->input->post('late_fee_type_' . $i) != '') {
                        $this->db->set('late_fee_type', $this->input->post('late_fee_type_' . $i));
                        $late_fee = $this->input->post('late_fee_' . $i);
                    } else {
                        $late_fee = 0;
                    }
                    $this->db->set('late_fee', $late_fee);
                    $this->db->set('late_fee_start', $this->input->post('late_fee_start_' . $i));
                    $this->db->set('due_date', strtotime($this->input->post('due_date_' . $i)));
                    $this->db->where('property_id', $prop_id);
                    $this->db->update($this->ownedPropertiestableName);
                    /********************** updateAllTenantRentAndLeaseInfo*******************/

    
                    $this->db->set('first_name', $this->input->post('first_name_' . $i));
                    $this->db->set('middle_name', $this->input->post('middle_name_' . $i));
                    $this->db->set('last_name', $this->input->post('last_name_' . $i));
                    $slug = $this->common_model->create_unique_slug_for_common($this->input->post('first_name_' . $i), 'tbl_members');
                    $this->db->set('slug', $slug);
                    $this->db->set('email', $this->input->post('email_' . $i));
                    $this->db->set('password', md5($newPassword));
                    $this->db->set('tpass', $newPassword);
                    $this->db->set('country_code', $this->input->post('prefix_code_' . $i));
                    $this->db->set('mobile_no', $this->input->post('mobile_' . $i));
                    $this->db->set('dob', strtotime($this->input->post('dob_' . $i)));
                    $this->db->set('preferred_language', $this->input->post('preferred_language_' . $i));
                    $this->db->set('user_type', 'tenant');
                    $this->db->set('add_date', time());
                    $this->db->set('status', 'Active');
                    $this->db->set('verification_status', 'Yes');
                    $this->db->set('ip', $_SERVER['REMOTE_ADDR']);
                    $this->db->insert('tbl_members');
                    $tenant_id = $this->db->insert_id();
                    //*********** Entry in table property lease detail *********//
                    $this->db->set('property_id', $prop_id);
                    $this->db->set('property_unique_id', $this->common_model->getSingleFieldFromAnyTable('property_code', 'prop_id', $prop_id, 'tbl_properties'));
                    $this->db->set('owner_id', $this->session->userdata('MEM_ID'));
                    $this->db->set('tenant_id', $tenant_id);
//                    $this->db->set('lease_start_date', strtotime($this->input->post('lease_start_date_' . $i)));
//                    $this->db->set('lease_end_date', strtotime($this->input->post('lease_end_date_' . $i)));
                    $this->db->set('lease_start_date', date('Y-m-d', strtotime($this->input->post('lease_start_date_' . $i))));
                    $this->db->set('lease_end_date', date('Y-m-d', strtotime($this->input->post('lease_end_date_' . $i))));
                    $this->db->set('rent_amount', $this->input->post('rent_amount_' . $i));
                    $this->db->set('late_fee', $this->input->post('late_fee_' . $i));
                    $this->db->set('late_fee_type', $this->input->post('late_fee_type_' . $i));
                    $this->db->set('late_fee_start', $this->input->post('late_fee_start_' . $i));
                    $this->db->set('due_date', $this->input->post('due_date_' . $i));
                    $this->db->set('occupie_status', 'running');
                    /* if (!empty($data)) {
                      $this->db->set('lease_document', $data[$i - 1]['upload_data']['file_name']);
                      } */
                    // Start Pets details
                    $pets_arr = $this->input->post('pets['.$i.']');
                    $data_pets = $data_pets_type = $data_pets_breed = array();
                    foreach ($pets_arr as $pets_i => $pets_ar) {
                        if(!empty($this->input->post('pets[' . $i . '][' . $pets_i . ']'))){
                            $data_pets[] = $this->input->post('pets[' . $i . '][' . $pets_i . ']');
                            $data_pets_type[] = $this->input->post('pets_type[' . $i . '][' . $pets_i . ']');
                            $data_pets_breed[] = $this->input->post('pets_breed[' . $i . '][' . $pets_i . ']');
                            $data_pets_name[] = $this->input->post('pets_name[' . $i . '][' . $pets_i . ']');
                            $data_pets_fee[] = $this->input->post('pets_fee[' . $i . '][' . $pets_i . ']');
                        }
                    }
                    /*print_R($pets_arr);
                    echo implode(",",$data_pets); exit;*/
                    $this->db->set('pets', implode(",",$data_pets));
                    $this->db->set('pets_type',implode(",",$data_pets_type ));
                    $this->db->set('pets_breed', implode(",",$data_pets_breed));
                    $this->db->set('pets_name', implode(",",$data_pets_name));
                    $this->db->set('pets_fee', implode(",",$data_pets_fee));


                    // END Pets details
                    // Start Emergency Contact
                    $this->db->set('relationship', $this->input->post('relationship[' . $i . ']'));
                    $this->db->set('emergency_name', $this->input->post('emergency_name[' . $i . ']'));
                    $this->db->set('emergency_phone', $this->input->post('emergency_phone[' . $i . ']'));
                    $this->db->set('emergency_email', $this->input->post('emergency_email[' . $i . ']'));
                    // END Emergency Contact

                    $this->db->set('add_date', time());
                    $this->db->insert($this->ownedPropertiestableName);
                    //lease docs entry
                    if (isset($data[$i - 1]['upload_data']['file_name']) && !empty($data[$i - 1]['upload_data']['file_name'])) {
                        $this->db->set('tenant_id', $tenant_id);
                        $this->db->set('property_id', $prop_id);
                        $this->db->set('add_date', time());
                        $this->db->set('lease_doc', $data[$i - 1]['upload_data']['file_name']);
                        $this->db->insert('tbl_lease_docs');
                    }
                }
                //~ //*********** Entry in table property lease history *********//
                //~ $this->db->set('property_id',$prop_id);
                //~ $this->db->set('property_unique_id',$this->common_model->getSingleFieldFromAnyTable('property_code','prop_id',$prop_id,'tbl_properties'));
                //~ $this->db->set('owner_id',$this->session->userdata('MEM_ID'));
                //~ $this->db->set('tenant_id',$tenant_id);
                //~ $this->db->set('lease_start_date',strtotime($this->input->post('lease_start_date_'.$i)));
                //~ $this->db->set('lease_end_date',strtotime($this->input->post('lease_end_date_'.$i)));
                //~ $this->db->set('rent_amount',$this->input->post('rent_amount_'.$i));
                //~ $this->db->set('late_fee',$this->input->post('late_fee_'.$i));
                //~ $this->db->set('late_fee_type',$this->input->post('late_fee_type_'.$i));
                //~ $this->db->set('due_date',$this->input->post('due_date_'.$i));
                //~ $this->db->set('lease_document',$data[$i-1]['upload_data']['file_name']);
                //~ $this->db->set('updated_by_owner_date',time());
                //~ $this->db->set('add_date',time());
                //~ $this->db->insert($this->ownedPropertieshistorytableName);
            }
        }
        return TRUE;
    }

    function frontAddVendorsAndTServicesToPropertyStep3($prop_id,$step_completed =false) {
        /*         * ****** updating tServices status in properties table **************** */
        /* if ($this->input->post('add-to-property')) {
          $vendor_ids = implode(',', $this->input->post('add-to-property'));
          $this->db->set('vendor_ids', $vendor_ids);
          } */
        $tMaintenace = empty($this->input->post('tMaintenace')) ? 'No' : $this->input->post('tMaintenace');
        $tMessaging = empty($this->input->post('tMessaging')) ? 'No' : $this->input->post('tMessaging');
        $tPay = empty($this->input->post('tPay')) ? 'No' : $this->input->post('tPay');
        $this->db->set('tmaintenance', $tMaintenace);
        $this->db->set('tmessaging', $tMessaging);
        $this->db->set('tpay', $tPay);
        $this->db->set('step_completed', 3);
        /*if($step_completed != false){
            
            $this->db->set('step_completed', 3);
        }*/
        $this->db->where('prop_id', $prop_id);
        $this->db->update($this->tableName);
    }

    function frontPropertyOrderProcessComplete($prop_id ) {
        $this->db->set('step_completed', 4);
        $this->db->where('prop_id', $prop_id);
        $this->db->update($this->tableName);
    }

    function getPropertyById($id) {
        $this->db->where('prop_id', $id);
        $this->db->where('status', 'Active');
        $query = $this->db->get($this->tableName);
        return $query->row();
    }

    function getCountAllTenantsByProperty($prop_id) {
        $this->db->select('count(tbl_members.first_name) as count');
        $this->db->join('tbl_members', 'tbl_members.mem_id=tbl_properties_lease_detail.tenant_id', 'inner');
        $this->db->where('tbl_properties_lease_detail.property_id', $prop_id);
        $this->db->where('tbl_members.account_closed_by_member', 'No');
        $query = $this->db->get($this->ownedPropertiestableName);
        $cnt = $query->row();
        return $cnt->count;
    }

    function getAllTenantsByProperty($prop_id) {
        $this->db->select('tbl_properties_lease_detail.*,tbl_members.slug,tbl_members.first_name,tbl_members.middle_name,tbl_members.last_name,tbl_members.email,tbl_members.country_code,tbl_members.mobile_no,tbl_members.dob,tbl_members.preferred_language,tbl_members.move_status');
        $this->db->join('tbl_members', 'tbl_members.mem_id=tbl_properties_lease_detail.tenant_id', 'inner');
        $this->db->where('tbl_properties_lease_detail.property_id', $prop_id);
        $this->db->where('tbl_members.account_closed_by_member', 'No');
        $query = $this->db->get($this->ownedPropertiestableName);
        //print_R($query->result()); exit;
        return $query->result();
    }

    function getLeaseDetailByTenantAndProperty($tenantId, $propertyId) {
        $this->db->where('property_id', $propertyId);
        $this->db->where('tenant_id', $tenantId);
        $query = $this->db->get($this->ownedPropertiestableName);
        return $query->row();
    }

    function getServicesByPropertyId($prop_id) {
        $this->db->where('property_id', $prop_id);
        $query = $this->db->get('tbl_property_services');
        return $query->result();
    }

    function changeTservicesStatus($prop_id, $field, $value) {
        $this->db->where('prop_id', $prop_id);
        $this->db->set($field, $value);
        $this->db->update($this->tableName);
    }

    function changeShopingCartTservicesStatus($prop_id, $field, $value) {
        if($field == 'ttext'){
            $this->db->where('prop_id', $prop_id);
            $this->db->set('tmessaging', $value);
            $this->db->set('tmaintenance', $value);
            $this->db->update($this->tableName);            
        }else{
            $this->db->where('prop_id', $prop_id);
            $this->db->set($field, $value);
            $this->db->update($this->tableName);
        }
    }

    function updateProperty($allAmenties) {
        if ($this->input->post('garbage_pick_day'))
            $this->db->set('garbage_day', $this->input->post('garbage_pick_day'));
        if ($this->input->post('recycle_pick_day'))
            $this->db->set('recycle_day', $this->input->post('recycle_pick_day'));
        if ($this->input->post('yard_waste_pick_day'))
            $this->db->set('yard_waste_day', $this->input->post('yard_waste_pick_day'));
        if ($this->input->post('safety_equipment1'))
            $this->db->set('safety_equipment1', $this->input->post('safety_equipment1'));
        if ($this->input->post('safety_equipment2'))
            $this->db->set('safety_equipment2', $this->input->post('safety_equipment2'));
        if ($this->input->post('heat_type')) {
            $this->db->set('heat_type', $this->input->post('heat_type'));
        }
        if ($this->input->post('heat_filter_size')) {
            if ($this->input->post('heat_filter_size') == 'Other') {
                $this->db->set('heat_filter_size', $this->input->post('heat_filter_size_other'));
            } else {
                $this->db->set('heat_filter_size', $this->input->post('heat_filter_size'));
            }
        }
        if ($this->input->post('ac_filter_size')) {
            if ($this->input->post('ac_filter_size') == 'Other') {
                $this->db->set('ac_filter_size', $this->input->post('ac_filter_size_other'));
            } else {
                $this->db->set('ac_filter_size', $this->input->post('ac_filter_size'));
            }
        }
        if ($this->input->post('ac_type')) {
            $this->db->set('ac_type', $this->input->post('ac_type'));
        }

        if ($this->input->post('pest_control_home')) {
            $this->db->set('pest_control_home', $this->input->post('pest_control_home'));
        }
        if ($this->input->post('pest_service_provider')) {
            $this->db->set('pest_service_provider', $this->input->post('pest_service_provider'));
            $this->db->set('pest_service_provider_number', $this->input->post('pest_service_provider_number'));
        }
        if ($this->input->post('yard')) {
            $this->db->set('yard', $this->input->post('yard'));
        }
        $this->db->set('yard_service_provided', $this->input->post('yard_service_provided'));
        if ($this->input->post('pest_service_provider')) {
            $this->db->set('yard_service_provider', $this->input->post('yard_service_provider'));
            $this->db->set('yard_service_provider_number', $this->input->post('yard_service_provider_number'));
        }

        if (isset($allAmenties) && !empty($allAmenties)) {
            foreach ($allAmenties as $value) {
                if ($this->input->post($value->name))
                    $selectedAmenties[] = $this->input->post($value->name);
            }
            if (!empty($selectedAmenties)) {
                $amenties = implode(',', $selectedAmenties);
                $this->db->set('amenties', $amenties);
            } else {
                $this->db->set('amenties', '');
            }
        }

        $this->db->where('prop_id', $this->input->post('property_id'));
        $this->db->update($this->tableName);

        /*         * ******** Adding provided services in tbl_property_service ***************** */
        $oldServices = $this->input->post('service');
        $oldservice_provider = $this->input->post('service_provider');
        $oldservice_provider_number = $this->input->post('service_provider_number');

        $selectedServices = $this->input->post('new-service');
        $service_provider = $this->input->post('new-service_provider');
        $service_provider_number = $this->input->post('new-service_provider_number');
        //echo"<pre>";print_r($selectedServices);die;
        $this->deleteServices($this->input->post('property_id'));
        if (!empty($oldServices)) {
            foreach ($oldServices as $key => $val) {
                $this->db->set('service_id', $oldServices[$key]);
                $this->db->set('company', $oldservice_provider[$key]);
                $this->db->set('contact', $oldservice_provider_number[$key]);
                $this->db->set('property_id', $this->input->post('property_id'));
                $this->db->insert('tbl_property_services');
            }
        }
        /*         * ******** Adding more provided services in tbl_service and tbl_property_service ***************** */
        if (isset($selectedServices) && !empty($selectedServices)) {
            foreach ($selectedServices as $key => $val) {
                $this->db->set('service', $selectedServices[$key]);
                $slug = $this->common_model->create_unique_slug_for_common($selectedServices[$key], 'tbl_services');
                $this->db->set('slug', $slug);
                $this->db->set('added_by', 'landlord');
                $this->db->set('added_by_id', $this->session->userdata('MEM_ID'));
                $this->db->set('add_date', time());
                $this->db->set('status', 'Active');
                $this->db->set('ip', $_SERVER['REMOTE_ADDR']);
                $this->db->insert('tbl_services');
                $service_id = $this->db->insert_id();
                if ($service_id) {
                    $this->db->set('service_id', $service_id);
                    $this->db->set('company', $service_provider[$key]);
                    $this->db->set('contact', $service_provider_number[$key]);
                    $this->db->set('property_id', $this->input->post('property_id'));
                    $this->db->insert('tbl_property_services');
                }
            }
        }

        //~ foreach($selectedServices as $key=>$val)
        //~ {
        //~ $this->db->set('service_id',$selectedServices[$key]);
        //~ $this->db->set('company',$service_provider[$key]);
        //~ $this->db->set('contact',$service_provider_number[$key]);
        //~ $this->db->set('property_id',$this->input->post('property_id'));
        //~ $this->db->insert('tbl_property_services');
        //~ }
    }

    function frontUpdatePropertyStep1($allAmenties, $community_doc) {

        $loc = $latitude = $longitude = '';
        /* Geolocation */
        if ($this->input->post('address1')) {
            $loc .= $this->input->post('address1') . " ";
        }
        if ($this->input->post('address2')) {
            $loc .= $this->input->post('address2') . " ";
        }
        if ($this->input->post('city')) {
            $loc .= $this->input->post('city') . " ";
        }
        if ($this->input->post('state')) {
            $stname = get_state_name($this->input->post('state'));
            $loc .=$stname . " ";
        }
        if ($this->input->post('zip')) {
            $loc .= $this->input->post('zip');
        }
        $val = $this->getLnt($loc);
        if (!empty($val['lat']) && !empty($val['lng'])) {
            $latitude = $val['lat'];
            $longitude = $val['lng'];
        }
        /* end */

        $this->db->set('state_id', $this->input->post('state'));
        $this->db->set('city', $this->input->post('city'));
        $this->db->set('address1', $this->input->post('address1'));
        $this->db->set('address2', $this->input->post('address2'));
        $this->db->set('zip', $this->input->post('zip'));
        $this->db->set('latitude', $latitude);
        $this->db->set('longitude', $longitude);
        $this->db->set('unit_number', $this->input->post('unit'));
        $this->db->set('property_type', $this->input->post('property_type'));
        if ($this->input->post('bedrooms'))
            $this->db->set('no_of_bedrooms', $this->input->post('bedrooms'));
        if ($this->input->post('bathrooms'))
            $this->db->set('no_of_bathrooms', $this->input->post('bathrooms'));
        if ($this->input->post('parking'))
            $this->db->set('parking', $this->input->post('parking'));
        if ($this->input->post('area'))
            $this->db->set('size', $this->input->post('area'));
        if ($this->input->post('build_year'))
            $this->db->set('year_built', $this->input->post('build_year'));
        $this->db->set('community_status', $this->input->post('is_community'));
        if ($this->input->post('is_community') == 'Yes') {
            $this->db->set('community', $this->input->post('community'));
            $this->db->set('gate_code', $this->input->post('gate_code'));
            $this->db->set('unit_po_box', $this->input->post('po_box'));
            $this->db->set('community_managing_company', $this->input->post('community_company'));
            $this->db->set('community_contact_number', $this->input->post('company_number'));
        }
        $this->db->set('garbage_day', $this->input->post('garbage_pick_day'));
        $this->db->set('recycle_day', $this->input->post('recycle_pick_day'));
        $this->db->set('yard_waste_day', $this->input->post('yard_waste_pick_day'));
        $this->db->set('safety_equipment1', $this->input->post('safety_equipment1'));
        $this->db->set('safety_equipment2', $this->input->post('safety_equipment2'));


        $this->db->set('heat_type', $this->input->post('heat_type'));
        if ($this->input->post('heat_filter_size') == 'Other') {
            $this->db->set('heat_filter_size', $this->input->post('heat_filter_size_other'));
        } else {
            $this->db->set('heat_filter_size', $this->input->post('heat_filter_size'));
        }
        if ($this->input->post('ac_filter_size') == 'Other') {
            $this->db->set('ac_filter_size', $this->input->post('ac_filter_size_other'));
        } else {
            $this->db->set('ac_filter_size', $this->input->post('ac_filter_size'));
        }
        $this->db->set('ac_type', $this->input->post('ac_type'));

        $this->db->set('pest_control_home', $this->input->post('pest_control_home'));
        if ($this->input->post('pest_service_provider')) {
            $this->db->set('pest_service_provider', $this->input->post('pest_service_provider'));
            $this->db->set('pest_service_provider_number', $this->input->post('pest_service_provider_number'));
        }
        $this->db->set('yard', $this->input->post('yard'));
        $this->db->set('yard_service_provided', $this->input->post('yard_service_provided'));
        if ($this->input->post('pest_service_provider')) {
            $this->db->set('yard_service_provider', $this->input->post('yard_service_provider'));
            $this->db->set('yard_service_provider_number', $this->input->post('yard_service_provider_number'));
        }
        $selectedAmenties = array();
        foreach ($allAmenties as $value) {
            if ($this->input->post($value->name))
                $selectedAmenties[] = $this->input->post($value->name);
        }
        $amenties = implode(',', $selectedAmenties);
        $this->db->set('amenties', $amenties);
        $this->db->set('tmessaging', '');
        $this->db->set('tmaintenance', '');
        // $this->db->set('occupied_status', 'vacant');
        if ($community_doc != '') {
            $this->db->set('community_doc', $community_doc);
        }
        $this->db->where('prop_id', $this->session->userdata('PROPERTY_ID'));
        $this->db->update($this->tableName);
        //echo $this->db->last_query();die;
        /*         * ******** Adding provided services in tbl_property_service ***************** */
        $this->deleteServices($this->session->userdata('PROPERTY_ID'));

        $oldServices = $this->input->post('service');
        $oldservice_provider = $this->input->post('service_provider');
        $oldservice_provider_number = $this->input->post('service_provider_number');
        if (!empty($oldServices)) {
            foreach ($oldServices as $key => $val) {
                if ($oldServices[$key]) {
                    $this->db->set('service_id', $oldServices[$key]);
                    $this->db->set('company', $oldservice_provider[$key]);
                    $this->db->set('contact', $oldservice_provider_number[$key]);
                    $this->db->set('property_id', $this->session->userdata('PROPERTY_ID'));
                    $this->db->insert('tbl_property_services');
                }
            }
        }

        /*         * ******** Adding more provided services in tbl_service and tbl_property_service ***************** */
        for ($i = 0; $i <= $this->input->post('new_created_services'); $i++) {
            if ($this->input->post('new_service_' . $i)) {
                $this->db->set('service', $this->input->post('new_service_' . $i));
                $slug = $this->common_model->create_unique_slug_for_common($this->input->post('new_service_' . $i), 'tbl_services');
                $this->db->set('slug', $slug);
                $this->db->set('added_by', 'landlord');
                $this->db->set('added_by_id', $this->session->userdata('MEM_ID'));
                $this->db->set('add_date', time());
                $this->db->set('status', 'Active');
                $this->db->set('ip', $_SERVER['REMOTE_ADDR']);
                $this->db->insert('tbl_services');
                $service_id = $this->db->insert_id();
                if ($service_id) {
                    $this->db->set('service_id', $service_id);
                    $this->db->set('company', $this->input->post('new_service_provider_' . $i));
                    $this->db->set('contact', $this->input->post('new_service_provider_number_' . $i));
                    $this->db->set('property_id', $this->session->userdata('PROPERTY_ID'));
                    $this->db->insert('tbl_property_services');
                }
            }
        }
    }

    function frontUpdateTenantsToProperty($prop_id, $data) {
        //************ updating property detail ************//
        $this->db->set('occupied_status', 'vacant');
        $this->db->set('step_completed', 2);
        $this->db->where('prop_id', $prop_id);
        $this->db->update($this->tableName);

        $this->deleteAllTenant($prop_id);
        $tenant_id = '';
        if ($this->input->post('is_vacant') != 'Yes') {
            $this->db->set('occupied_status', 'occupied');
            $this->db->where('prop_id', $prop_id);
            $this->db->update($this->tableName);
            //$count = count($this->input->post('first_name'));
            $arr = $this->input->post('first_name');
            foreach ($arr as $i => $ar) {
                $strnew = 'abcXdefgRhijklmnoYpqrstGuvwxJyz012345A6789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
                $shuff = str_shuffle($strnew);
                $newPassword = substr($shuff, 0, 8);

                /********************** updateAllTenantRentAndLeaseInfo*******************/
                $this->db->set('lease_start_date', date('Y-m-d',strtotime($this->input->post('lease_start_date[' . $i . ']'))));
                $this->db->set('lease_end_date', date('Y-m-d',strtotime($this->input->post('lease_end_date[' . $i . ']'))));
                $this->db->set('rent_amount', $this->input->post('rent_amount[' . $i . ']'));
                if ($this->input->post('late_fee_type[' . $i . ']') != '') {
                    $this->db->set('late_fee_type', $this->input->post('late_fee_type[' . $i . ']'));
                    $late_fee = $this->input->post('late_fee[' . $i . ']');
                } else {
                    $late_fee = 0;
                }
                $this->db->set('late_fee', $late_fee);
                $this->db->set('late_fee_start', $this->input->post('late_fee_start[' . $i . ']'));
                $this->db->set('due_date', strtotime($this->input->post('due_date[' . $i . ']')));
                $this->db->where('property_id', $prop_id);
                $this->db->update($this->ownedPropertiestableName);
                /********************** updateAllTenantRentAndLeaseInfo*******************/

                //*********** Entry in table members for new tenant *********//
                $this->db->set('first_name', $this->input->post('first_name[' . $i . ']'));
                $this->db->set('middle_name', $this->input->post('middle_name[' . $i . ']'));
                $this->db->set('last_name', $this->input->post('last_name[' . $i . ']'));
                $slug = $this->common_model->create_unique_slug_for_common($this->input->post('first_name[' . $i . ']'), 'tbl_members');
                $this->db->set('slug', $slug);
                $this->db->set('email', $this->input->post('email[' . $i . ']'));
                $this->db->set('password', md5($newPassword));
                $this->db->set('tpass', $newPassword);
                $this->db->set('mobile_no', $this->input->post('mobile[' . $i . ']'));
                $this->db->set('dob', strtotime($this->input->post('dob[' . $i . ']')));
                $this->db->set('preferred_language', $this->input->post('preferred_language[' . $i . ']'));
                $this->db->set('user_type', 'tenant');
                $this->db->set('add_date', time());
                $this->db->set('status', 'Active');
                $this->db->set('verification_status', 'Yes');
                $this->db->set('ip', $_SERVER['REMOTE_ADDR']);
                $this->db->insert('tbl_members');
                $tenant_id = $this->db->insert_id();


                //*********** Entry in table property lease detail *********//
                $this->db->set('property_id', $prop_id);
                $this->db->set('property_unique_id', $this->common_model->getSingleFieldFromAnyTable('property_code', 'prop_id', $prop_id, 'tbl_properties'));
                $this->db->set('owner_id', $this->session->userdata('MEM_ID'));
                $this->db->set('tenant_id', $tenant_id);
//                $this->db->set('lease_start_date', strtotime($this->input->post('lease_start_date[' . $i . ']')));
//                $this->db->set('lease_end_date', strtotime($this->input->post('lease_end_date[' . $i . ']')));

                $this->db->set('lease_start_date', date('Y-m-d', strtotime($this->input->post('lease_start_date[' . $i . ']'))));
                $this->db->set('lease_end_date', date('Y-m-d', strtotime($this->input->post('lease_end_date[' . $i . ']'))));
                $this->db->set('rent_amount', $this->input->post('rent_amount[' . $i . ']'));
                $this->db->set('late_fee', $this->input->post('late_fee[' . $i . ']'));
                $this->db->set('late_fee_type', $this->input->post('late_fee_type[' . $i . ']'));
                $this->db->set('late_fee_start', $this->input->post('late_fee_start[' . $i . ']'));
                $this->db->set('due_date', $this->input->post('due_date[' . $i . ']'));
                $this->db->set('occupie_status', 'running');
                // Start Pets details
                $pets_arr = $this->input->post('pets['.$i.']');
                $data_pets = $data_pets_type = $data_pets_breed = $data_pets_name = $data_pets_fee = array();
                foreach ($pets_arr as $pets_i => $pets_ar) {
                    if(!empty($this->input->post('pets[' . $i . '][' . $pets_i . ']'))){
                        $data_pets[] = $this->input->post('pets[' . $i . '][' . $pets_i . ']');
                        $data_pets_type[] = $this->input->post('pets_type[' . $i . '][' . $pets_i . ']');
                        $data_pets_breed[] = $this->input->post('pets_breed[' . $i . '][' . $pets_i . ']');
                        $data_pets_name[] = $this->input->post('pets_name[' . $i . '][' . $pets_i . ']');
                        $data_pets_fee[] = $this->input->post('pets_fee[' . $i . '][' . $pets_i . ']');
                    }
                }
                /*print_R($pets_arr);
                echo implode(",",$data_pets); exit;*/
                $this->db->set('pets', implode(",",$data_pets));
                $this->db->set('pets_type',implode(",",$data_pets_type ));
                $this->db->set('pets_breed', implode(",",$data_pets_breed));
                $this->db->set('pets_name', implode(",",$data_pets_name));
                $this->db->set('pets_fee', implode(",",$data_pets_fee));
                // END Pets details
                // Start Emergency Contact 
                $this->db->set('relationship', $this->input->post('relationship[' . $i . ']'));
                $this->db->set('emergency_name', $this->input->post('emergency_name[' . $i . ']'));
                $this->db->set('emergency_phone', $this->input->post('emergency_phone[' . $i . ']'));
                $this->db->set('emergency_email', $this->input->post('emergency_email[' . $i . ']'));
                // END Emergency Contact
                $this->db->set('add_date', time());
                $this->db->insert($this->ownedPropertiestableName);
                //lease docs entry
                if (isset($data[$i]['upload_data']['file_name']) && !empty($data[$i]['upload_data']['file_name'])) {
                    $this->db->set('tenant_id', $tenant_id);
                    $this->db->set('property_id', $prop_id);
                    $this->db->set('add_date', time());
                    $this->db->set('lease_doc', $data[$i]['upload_data']['file_name']);
                    $this->db->insert('tbl_lease_docs');
                }
                if (isset($data[$i]['upload_data']['uploaded_file']) && !empty($data[$i]['upload_data']['uploaded_file'])) {
                    $this->db->set('tenant_id', $tenant_id);
                    $this->db->set('property_id', $prop_id);
                    $this->db->set('add_date', time());
                    $this->db->set('lease_doc', $data[$i]['upload_data']['uploaded_file']);
                    $this->db->insert('tbl_lease_docs');
                }
            }
            /*  for ($i = 0; $i < $count; $i++) {
              //  $this->deleteMember($this->input->post('tenant_id_' . $i));
              // echo $this->input->post('first_name['. $i.']');die;

              } */
        }
        return $tenant_id;
    }

    function deleteServices($prop_id) {
        $this->db->where('property_id', $prop_id);
        $this->db->delete('tbl_property_services');
    }

    function deleteAllTenant($prop_id) {
//delete member info        
        $names = $this->getTenantAray($prop_id);
        if (!empty($names)) {
            $this->db->where_in('mem_id', $names);
            $this->db->delete('tbl_members');
//delete lease docs
            $this->db->where_in('tenant_id', $names);
            $this->db->delete('tbl_lease_docs');
        }
//delete lease info
        $this->db->where('property_id', $prop_id);
        $this->db->delete($this->ownedPropertiestableName);
    }

    function getTenantAray($prop_id) {
        $ids = array();
        $this->db->where('property_id', $prop_id);
        $query = $this->db->get($this->ownedPropertiestableName);
        $result = $query->result();
        if (!empty($result)) {
            foreach ($result as $tenant) {
                $ids[] = $tenant->tenant_id;
            }
        }
        return $ids;
    }

    function deleteMember($mem_id) {
        $this->db->where('mem_id', $mem_id);
        $this->db->delete('tbl_members');
        //echo $this->db->last_query();die;
    }

    function getLeaseDocs($prop_id, $tenant_id, $type) {
        $this->db->where('property_id', $prop_id);
        $this->db->where('tenant_id', $tenant_id);
        $this->db->where('type', $type);
        $query = $this->db->get('tbl_lease_docs');
        return $query->result();
    }

    function getMailingaddress($owner) {
        $this->db->select('mailing_address1,mailing_address2,mailing_unit,mailing_city,mailing_state_id,mailing_zip');
        $this->db->where('owner_id', $owner);
        $this->db->limit(1);
        $query = $this->db->get('tbl_properties');
        return $query->row();
    }

    function getPropertyPhoto($p_id) {
        $this->db->where('property_id', $p_id);
        $this->db->where('is_main_photo', 1);
        $this->db->limit(1);
        $query = $this->db->get('tbl_property_photos');
        return $query->row();
    }

    function getPropertyPhotos($p_id) {
        $this->db->where('property_id', $p_id);
        //$this->db->where('is_main_photo', 1);
        $query = $this->db->get('tbl_property_photos');
        return $query->result();
    }

    public function getLandlordCSV() {
        $this->db->select(array('user.first_name as First Name', 'user.last_name as Last Name', 'user.email as Email','IF(user.third_party_sharing=1, "Yes", "No") as ThirdPartySharing','IF(account_numbers.synapse_micro_permission="CREDIT-AND-DEBIT", "Yes", "No") as tPay','user.mobile_no as Number', 'GROUP_CONCAT(DISTINCT property.address1,CONCAT(" "),property.address2,CONCAT(" "),property.city SEPARATOR " || ") AS Properties'));
        $this->db->from('tbl_members as user');
        $this->db->join('tbl_properties as property', 'property.owner_id = user.mem_id','left');
        $this->db->join('tbl_linked_account_numbers as account_numbers', 'account_numbers.user_id = user.mem_id', 'left');
        $this->db->where('user.user_type', 'landlord');
        $this->db->group_by('user.mem_id');
        $this->db->order_by('user.first_name');
        $query = $this->db->get();
        return $query;
    }

    public function getTenantCSV() {
        $this->db->select(array('user.first_name as First Name', 'user.last_name as Last Name', 'user.email as Email','IF(user.third_party_sharing=1, "Yes", "No") as ThirdPartySharing', 'user.mobile_no as Number', 'GROUP_CONCAT(DISTINCT property.address1,CONCAT(" "),property.address2,CONCAT(" "),property.city SEPARATOR " || ") AS Property'));
        $this->db->from('tbl_members as user');
        $this->db->join('tbl_properties_lease_detail as lease', 'lease.tenant_id = user.mem_id', 'inner');
        $this->db->join('tbl_properties as property', 'property.prop_id = lease.property_id', 'inner');
        $this->db->where('user.user_type', 'tenant');
        $this->db->where('user.account_closed_by_member', 'No');
        $this->db->group_by('user.mem_id');
        $this->db->order_by('user.first_name');
        $query = $this->db->get();
        return  $query;
    }

}
