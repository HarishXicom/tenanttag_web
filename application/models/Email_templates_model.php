<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Email_templates_model extends CI_Model {

 var $tableName	=	'tbl_email_templates';
    function __construct()
    {
        parent::__construct();
    }
    
    /******************* Admin functions Starts here **************************/
    
    function getCountAllRecordsForAdmin($status=NULL)
    {
		if($status)
			$this->db->where('status',$status);
		$query	=	$this->db->get($this->tableName);
		return $query->num_rows();
	}
	
    function getAllRecordsForAdmin($num,$offset)
    {
		$this->db->order_by('et_id','desc');
		$query	=	$this->db->get($this->tableName,$num,$offset);
		return $query->result();
	}
	
	function getRecordById($id)
	{
		$this->db->where('et_id',$id);
		$query	=	$this->db->get($this->tableName);
		return $query->row();
	}
	
	
	function add()
    {
		$this->db->set('title',$this->input->post('title'));
		$this->db->set('subject',$this->input->post('subject'));
		$this->db->set('description',$this->input->post('description'));
		$slug	=	$this->common_model->create_unique_slug_for_common($this->input->post('title'),$this->tableName);
		$this->db->set('slug',$slug);
		$this->db->set('add_date',time());
		$this->db->set('status','Active');
		$this->db->set('ip',$_SERVER['REMOTE_ADDR']);
		$this->db->insert($this->tableName);
	}
	
	function update($id)
    {
		if($this->input->post('title'))
		$this->db->set('title',$this->input->post('title'));
		if($this->input->post('subject'))
		$this->db->set('subject',$this->input->post('subject'));
		if($this->input->post('description'))
		$this->db->set('description',$this->input->post('description'));
		$this->db->where('et_id',$id);
		$this->db->update($this->tableName);
	}
	
	function getCountFilterAllRecords($filterKey=NULL,$status=NULL,$sort_by=NULL)
	{
		if($filterKey!='' && $filterKey != 'NULL')
		{
			$where	=	"( title like '%".$filterKey."%' OR subject like '%".$filterKey."%' OR description like '%".$filterKey."%' )";	
			$this->db->where($where);
		}		
		if($status!='' && $status != 'NULL')
		{
			$this->db->where('status',$status);
		}
		if($sort_by!='NULL')
		{	
			if($sort_by=='New')
			$this->db->order_by("et_id","desc");
			if($sort_by=='Old')
			$this->db->order_by("et_id","asc"); 
			if($sort_by=='Asc')
			$this->db->order_by("et_id","asc");
			if($sort_by=='Desc')
			$this->db->order_by("et_id","desc");
			
	 	}
		$query	=	$this->db->get($this->tableName);
		return $query->num_rows();
		
	}
	
	function getFilterAllRecords($filterKey=NULL,$status=NULL,$sort_by=NULL,$num,$offset)
	{
		if($filterKey!='' && $filterKey != 'NULL')
		{
			$where	=	"( title like '%".$filterKey."%' OR subject like '%".$filterKey."%' OR description like '%".$filterKey."%' )";	
			$this->db->where($where);
		}		
		if($status!='' && $status != 'NULL')
		{
			$this->db->where('status',$status);
		}
		if($sort_by!='NULL')
		{	
			if($sort_by=='New')
			$this->db->order_by("et_id","desc");
			if($sort_by=='Old')
			$this->db->order_by("et_id","asc"); 
			if($sort_by=='Asc')
			$this->db->order_by("et_id","asc");
			if($sort_by=='Desc')
			$this->db->order_by("et_id","desc");
			
	 	}
	 	else
			$this->db->order_by('et_id','desc');
		$query	=	$this->db->get($this->tableName,$num,$offset);
		return $query->result();
		
	}
	
	
	function performMultipleTasks($task,$iDs)
	{
		if($task=='delete')
		{
			foreach($iDs as $value)
			{
				$this->db->where('et_id',$value);
				$this->db->delete($this->tableName);
			}
			$message	=	'Selected records has been deleted successfully';
			return $message;
		}
		if($task=='Active' || $task=='Inactive')
		{
			foreach($iDs as $value)
			{
				$this->db->set('status',$task);
				$this->db->where('et_id',$value);
				$this->db->update($this->tableName);
			}
			$message	=	'Selected records has been '.$task.' successfully';
			return $message;
		}

	}
	
	function performTask($task,$iD)
	{
		if($task=='delete')
		{
			$this->db->where('et_id',$iD);
			$this->db->delete($this->tableName);
			$message	=	'Selected record has been deleted successfully';
			return $message;
		}
		if($task=='Active' || $task=='Inactive')
		{
			$this->db->set('status',$task);
			$this->db->where('et_id',$iD);
			$this->db->update($this->tableName);
			$message	=	'Selected record has been '.$task.' successfully';
			return $message;
		}

	}
    
    /******************* Admin functions Ends here **************************/
    
    /******************* Front functions Starts here **************************/
    

}
