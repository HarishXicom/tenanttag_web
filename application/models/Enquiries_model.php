<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Enquiries_model extends CI_Model {

 var $tableName	=	'tbl_contact_us';
    function __construct()
    {
        parent::__construct();
    }
    
    /******************* Admin functions Starts here **************************/
    
    function getCountAllRecordsForAdmin($status=NULL)
    {
		if($status)
			$this->db->where('status',$status);
		$query	=	$this->db->get($this->tableName);
		return $query->num_rows();
	}
	
    function getAllRecordsForAdmin($num,$offset)
    {
		$this->db->order_by('id','desc');
		$query	=	$this->db->get($this->tableName,$num,$offset);
		return $query->result();
	}
	
	function getRecordById($id)
	{
		$this->db->where('id',$id);
		$query	=	$this->db->get($this->tableName);
		return $query->row();
	}
	
	
	function addEntryToMailHistory($email)
    {
		$this->db->set('subject',$this->input->post('subject'));
		$this->db->set('message',$this->input->post('message'));
		$this->db->set('sent_to_email',$email);
		$this->db->set('sent_date',time());
		$this->db->set('ip',$_SERVER['REMOTE_ADDR']);
		$this->db->insert($this->tableName);
	}
	
	function getCountFilterAllRecords($filterKey=NULL)
	{
		if($filterKey!='' && $filterKey != 'NULL')
		{
			$where	=	"( name like '%".$filterKey."%' OR email like '%".$filterKey."%' OR message like '%".$filterKey."%' )";	
			$this->db->where($where);
		}		
		$query	=	$this->db->get($this->tableName);
		return $query->num_rows();
		
	}
	
	function getFilterAllRecords($filterKey=NULL,$num,$offset)
	{
		if($filterKey!='' && $filterKey != 'NULL')
		{
			$where	=	"( name like '%".$filterKey."%' OR email like '%".$filterKey."%' OR message like '%".$filterKey."%' )";	
			$this->db->where($where);
		}		
		$this->db->order_by('id','desc');
		$query	=	$this->db->get($this->tableName,$num,$offset);
		return $query->result();
		
	}
	
	
	function performMultipleTasks($task,$iDs)
	{
		if($task=='delete')
		{
			foreach($iDs as $value)
			{
				$this->db->where('id',$value);
				$this->db->delete($this->tableName);
			}
			$message	=	'Selected records has been deleted successfully';
			return $message;
		}
		if($task=='Active' || $task=='Inactive')
		{
			foreach($iDs as $value)
			{
				$this->db->set('status',$task);
				$this->db->where('id',$value);
				$this->db->update($this->tableName);
			}
			$message	=	'Selected records has been '.$task.' successfully';
			return $message;
		}

	}
	
	function performTask($task,$iD)
	{
		if($task=='delete')
		{
			$this->db->where('id',$iD);
			$this->db->delete($this->tableName);
			$message	=	'Selected record has been deleted successfully';
			return $message;
		}
		if($task=='Active' || $task=='Inactive')
		{
			$this->db->set('status',$task);
			$this->db->where('id',$iD);
			$this->db->update($this->tableName);
			$message	=	'Selected record has been '.$task.' successfully';
			return $message;
		}

	}
	
	function updateReplyStatus($id)
	{
		$this->db->where('id',$id);
		$this->db->set('reply_status','Yes');
		$this->db->update($this->tableName);
	}
    
    /******************* Admin functions Ends here **************************/
    
    /******************* Front functions Starts here **************************/
    

}
