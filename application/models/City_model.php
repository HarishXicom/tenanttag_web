<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class City_model extends CI_Model {

 var $tableName	=	'tbl_city';
    function __construct()
    {
        parent::__construct();
    }
    
    /******************* Front functions Starts here **************************/
    
    function getAllCities()
    {
		$this->db->where('status','Active');
		$query	=	$this->db->get($this->tableName);
		return $query->result();
	}
	
	function getAllCityByStateId($state_id)
    {
		$this->db->where('region_id',$state_id);
		$this->db->where('status','Active');
		$query	=	$this->db->get($this->tableName);
		return $query->result();
	}
	
    /******************* Front functions Ends here **************************/
    
   
	

}
