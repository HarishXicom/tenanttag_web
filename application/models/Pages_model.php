<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pages_model extends CI_Model {

    var $tableName = 'tbl_pages';

    function __construct() {
        parent::__construct();
    }

    /*     * ***************** Admin functions Starts here ************************* */

    function getCountAllRecordsForAdmin($status = NULL) {
        if ($status)
            $this->db->where('status', $status);
        //$this->db->where('type', 'page');
        $this->db->where_in('type', array('page','app_page'));
        $query = $this->db->get($this->tableName);
        return $query->num_rows();
    }

    function getAllRecordsForAdmin($num, $offset) {
        $this->db->order_by('pg_id', 'desc');
        $this->db->where_in('type', array('page','app_page'));
        $query = $this->db->get($this->tableName, $num, $offset);
        return $query->result();
    }

    function getRecordById($id) {
        $this->db->where('pg_id', $id);
        $query = $this->db->get($this->tableName);
        return $query->row();
    }

    function add() {
        $this->db->set('name', $this->input->post('name'));
        $this->db->set('type', 'page');
        $this->db->set('title', $this->input->post('title'));
        $this->db->set('description', $this->input->post('description'));
        $this->db->set('meta_title', $this->input->post('meta_title'));
        $this->db->set('meta_keywords', $this->input->post('meta_keywords'));
        $this->db->set('meta_description', $this->input->post('meta_description'));
        $slug = $this->common_model->create_unique_slug_for_common($this->input->post('name'), $this->tableName);
        $this->db->set('slug', $slug);
        $this->db->set('add_date', time());
        $this->db->set('status', 'Active');
        $this->db->set('ip', $_SERVER['REMOTE_ADDR']);
        $this->db->insert($this->tableName);
    }

    function update($id) {
        $rec = $this->getRecordById($id);
        if ($rec->name != $this->input->post('name')) {
            $slug = $this->common_model->create_unique_slug_for_common($this->input->post('name'), $this->tableName);
            $this->db->set('slug', $slug);
        }
        if ($this->input->post('title'))
            $this->db->set('title', $this->input->post('title'));
        if ($this->input->post('name'))
            $this->db->set('name', $this->input->post('name'));
        if ($this->input->post('description'))
            $this->db->set('description', $this->input->post('description'));
        if ($this->input->post('meta_title'))
            $this->db->set('meta_title', $this->input->post('meta_title'));
        if ($this->input->post('meta_keywords'))
            $this->db->set('meta_keywords', $this->input->post('meta_keywords'));
        if ($this->input->post('meta_description'))
            $this->db->set('meta_description', $this->input->post('meta_description'));
        $this->db->where('pg_id', $id);
        $this->db->update($this->tableName);
    }

    function getCountFilterAllRecords($filterKey = NULL, $status = NULL, $sort_by = NULL) {
        if ($filterKey != '' && $filterKey != 'NULL') {
            $where = "( title like '%" . $filterKey . "%' OR name like '%" . $filterKey . "%' OR description like '%" . $filterKey . "%' )";
            $this->db->where($where);
        }
        if ($status != '' && $status != 'NULL') {
            $this->db->where('status', $status);
        }
        $this->db->where_in('type', array('page','app_page'));
        if ($sort_by != 'NULL') {
            if ($sort_by == 'New')
                $this->db->order_by("pg_id", "desc");
            if ($sort_by == 'Old')
                $this->db->order_by("pg_id", "asc");
            if ($sort_by == 'Asc')
                $this->db->order_by("pg_id", "asc");
            if ($sort_by == 'Desc')
                $this->db->order_by("pg_id", "desc");
        }
        $query = $this->db->get($this->tableName);
        return $query->num_rows();
    }

    function getFilterAllRecords($filterKey = NULL, $status = NULL, $sort_by = NULL, $num, $offset) {
        if ($filterKey != '' && $filterKey != 'NULL') {
            $where = "( title like '%" . $filterKey . "%' OR name like '%" . $filterKey . "%' OR description like '%" . $filterKey . "%' )";
            $this->db->where($where);
        }
        if ($status != '' && $status != 'NULL') {
            $this->db->where('status', $status);
        }
        $this->db->where_in('type', array('page','app_page'));
        if ($sort_by != 'NULL') {
            if ($sort_by == 'New')
                $this->db->order_by("pg_id", "desc");
            if ($sort_by == 'Old')
                $this->db->order_by("pg_id", "asc");
            if ($sort_by == 'Asc')
                $this->db->order_by("pg_id", "asc");
            if ($sort_by == 'Desc')
                $this->db->order_by("pg_id", "desc");
        } else
            $this->db->order_by('pg_id', 'desc');
        $query = $this->db->get($this->tableName, $num, $offset);
        return $query->result();
    }

    function performMultipleTasks($task, $iDs) {
        if ($task == 'delete') {
            foreach ($iDs as $value) {
                $this->db->where('pg_id', $value);
                $this->db->delete($this->tableName);
            }
            $message = 'Selected records has been deleted successfully';
            return $message;
        }
        if ($task == 'Active' || $task == 'Inactive') {
            foreach ($iDs as $value) {
                $this->db->set('status', $task);
                $this->db->where('pg_id', $value);
                $this->db->update($this->tableName);
            }
            $message = 'Selected records has been ' . $task . ' successfully';
            return $message;
        }
    }

    function performTask($task, $iD) {
        if ($task == 'delete') {
            $this->db->where('pg_id', $iD);
            $this->db->delete($this->tableName);
            $message = 'Selected record has been deleted successfully';
            return $message;
        }
        if ($task == 'Active' || $task == 'Inactive') {
            $this->db->set('status', $task);
            $this->db->where('pg_id', $iD);
            $this->db->update($this->tableName);
            $message = 'Selected record has been ' . $task . ' successfully';
            return $message;
        }
    }
    
    function getFooterLinks() {
        $this->db->select(array('slug','title'));
        $this->db->order_by('orderby', 'asc');
        $this->db->where('status', 'Active');
        $this->db->where('type', 'page');
        $query = $this->db->get($this->tableName);
        return $query->result();
    }

    /*     * ***************** Admin functions Ends here ************************* */

    /*     * ***************** Front functions Starts here ************************* */
}
