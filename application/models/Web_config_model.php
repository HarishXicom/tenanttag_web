<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Web_config_model extends CI_Model {

    var $tableName = 'tbl_wesite_config';

    function __construct() {
        parent::__construct();
    }

    /*     * ***************** Admin functions Starts here ************************* */

    function getRecordById($id) {
        $this->db->where('id', $id);
        $query = $this->db->get($this->tableName);
        return $query->row();
    }

    function update_website_settings($id, $logo) {
        $record = $this->getRecordById($id);
        if ($this->input->post('site_name'))
            $this->db->set('site_name', $this->input->post('site_name'));
        if ($this->input->post('site_title'))
            $this->db->set('site_title', $this->input->post('site_title'));
        if ($this->input->post('page_size_admin'))
            $this->db->set('page_size_admin', $this->input->post('page_size_admin'));
        if ($this->input->post('page_size_front'))
            $this->db->set('page_size_front', $this->input->post('page_size_front'));
        if ($this->input->post('contact_address'))
            $this->db->set('contact_address', $this->input->post('contact_address'));
        if ($this->input->post('contact_number'))
            $this->db->set('contact_number', $this->input->post('contact_number'));
        if ($this->input->post('copyright_text'))
            $this->db->set('copyright_text', $this->input->post('copyright_text'));

        if ($logo) {
            unlink('assets/member/' . $record->site_logo);
            $this->db->set('site_logo', $logo);
        }
        $this->db->where('id', $id);
        $this->db->update($this->tableName);
    }

     function update_resources($resources_docs,$doc_name) {
        if ($resources_docs) {
            $this->db->set('resources_docs', $resources_docs);
            $this->db->set('doc_name', $doc_name);
            $this->db->set('created_date', date('Y-m-d h:i:s'));
            $this->db->set('modified_date', date('Y-m-d h:i:s'));
        }
        $this->db->insert('tbl_resources_docs');
    }

    function update_social_settings($id) {
        if ($this->input->post('facebook_url'))
            $this->db->set('facebook_url', $this->input->post('facebook_url'));
        if ($this->input->post('google_url'))
            $this->db->set('google_url', $this->input->post('google_url'));
        if ($this->input->post('twitter_url'))
            $this->db->set('twitter_url', $this->input->post('twitter_url'));
        if ($this->input->post('youtube_url'))
            $this->db->set('youtube_url', $this->input->post('youtube_url'));
        if ($this->input->post('instagram_url'))
            $this->db->set('instagram_url', $this->input->post('instagram_url'));
        if ($this->input->post('tumblr_url'))
            $this->db->set('tumblr_url', $this->input->post('tumblr_url'));
        if ($this->input->post('pinterest_url'))
            $this->db->set('pinterest_url', $this->input->post('pinterest_url'));
        $this->db->where('id', $id);
        $this->db->update($this->tableName);
    }

    function update_email_settings($id) {
        if ($this->input->post('contact_email'))
            $this->db->set('contact_email', $this->input->post('contact_email'));
        if ($this->input->post('email_goes_from_name'))
            $this->db->set('email_goes_from_name', $this->input->post('email_goes_from_name'));
        if ($this->input->post('email_goes_from_email'))
            $this->db->set('email_goes_from_email', $this->input->post('email_goes_from_email'));
        $this->db->where('id', $id);
        $this->db->update($this->tableName);
    }

    /*     * ***************** Admin functions Ends here ************************* */

    /*     * ***************** Front functions Starts here ************************* */
}
