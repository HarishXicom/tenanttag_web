 <?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Property extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('properties_model');
        $this->mem_id = $this->session->userdata('MEM_ID');
        $this->load->model(array('region_model', 'common_model'));
        $this->common_model->checkMemberLogin();
        $this->session->keep_flashdata('stripe_payment'); 
        $this->session->keep_flashdata('stripe_payment_prop_id'); 
    }

    function getLnt($loc) {
        $url = "http://maps.googleapis.com/maps/api/geocode/json?address=" . urlencode($loc) . "&sensor=false";
        $result_string = file_get_contents($url);
        $result = json_decode($result_string, true);
        $result1[] = isset($result['results'][0]) ? $result['results'][0] : '';
        $result2[] = isset($result1[0]['geometry']) ? $result1[0]['geometry'] : '';
        $result3[] = isset($result2[0]['location']) ? $result2[0]['location'] : '';
        return isset($result3[0]) ? $result3[0] : '';
    }

    function add() {
        $step_completed = $this->common_model->getHigestCompletedStepProeprty();
        $allProperties = $this->properties_model->frontGetCountAllPropertiesByLandlordId($this->session->userdata('MEM_ID'));
        //print_r($allProperties); 
        //print_r($step_completed); exit; 
        if (!$allProperties || ($allProperties == 1 && $step_completed < 3)) {
            $output['selected_tab'] = 'first-property';
        } else {
            $output['selected_tab'] = 'properties';
        }

        /* if ($step_completed == 1)
          redirect(site_url('add-property-step2'));
          if ($step_completed == 2)
          redirect(site_url('add-property-step3')); */

        $output['title'] = 'Properties';

        /*         * ******** Start loading model as required in function ***** */
        $this->load->model('amenties_model');
        $this->load->model('services_model');
        $this->load->model('region_model');
        $this->load->model('member_model');
        /*         * ******** End loading model as required in function ******* */

        /*         * ******** Get All Amenties ******************************** */
        $output['allAmenties'] = $allAmenties = $this->amenties_model->frontGetAllAmenties();
        /*         * ******** Get All Services ******************************** */
        $output['allServices'] = $this->services_model->frontGetAllServices();
        /*         * ******** Get all regions/states ************************* */
        $country_id = $this->common_model->getSingleFieldFromAnyTable('country_id', 'mem_id', $this->session->userdata('MEM_ID'), 'tbl_members');
        $state_id = $this->common_model->getSingleFieldFromAnyTable('state_id', 'mem_id', $this->session->userdata('MEM_ID'), 'tbl_members');
        $output['allRegion'] = $this->region_model->getAllRegionsByCountryId($country_id);
        $output['state_id'] = $state_id;
        $output['country_id'] = $country_id;
        if (!empty($_POST)) {
            $failure = 0;
            $com_doc = '';
            $this->form_validation->set_message('required', '%s is required');
            $this->form_validation->set_rules('property_type', 'Property Type', 'trim|required');
            $this->form_validation->set_rules('address1', 'Address 1', 'trim|required');
            //$this->form_validation->set_rules('address2', 'Address 2', 'trim|required');
            //$this->form_validation->set_rules('unit', 'Unit', 'trim|required|is_numeric|greater_than[0]');
            $this->form_validation->set_rules('state', 'State', 'trim|required');
            $this->form_validation->set_rules('zip', 'Zip', 'trim|required|is_numeric|greater_than[0]');
            //$this->form_validation->set_rules('bedrooms', 'Bedrooms', 'trim|required');
            //$this->form_validation->set_rules('bathrooms', 'Bathrooms', 'trim|required');
            //$this->form_validation->set_rules('parking', 'Parking', 'trim|required');
            //$this->form_validation->set_rules('area', 'Area', 'trim|required');
            //$this->form_validation->set_rules('build_year', 'Build Year', 'trim|required');
            $this->form_validation->set_rules('is_community', 'Property in Community', 'trim|required');
            if ($this->input->post('is_community') == 'Yes') {
                $this->form_validation->set_rules('community', 'Community', 'trim|required');
                //~ $this->form_validation->set_rules('gate_code', 'Gate code', 'trim|required');
                //~ $this->form_validation->set_rules('po_box', 'Unit PO Box', 'trim|required');
                //  $this->form_validation->set_rules('community_company', 'Community Managing Company', 'trim|required');
                //~ $this->form_validation->set_rules('company_number', 'Community Managing Company Contact Number', 'trim|required|is_numeric|greater_than[0]');
            }
            $this->form_validation->set_rules('garbage_pick_day', 'Garbage pick day', 'trim|required');
            $this->form_validation->set_rules('recycle_pick_day', 'Recycle pick day', 'trim|required');
            $this->form_validation->set_rules('yard_waste_pick_day', 'Yard Waste pick day', 'trim|required');
            $this->form_validation->set_rules('safety_equipment1', 'Smoke detectors', 'trim|required');
            $this->form_validation->set_rules('safety_equipment2', 'Fire Extinguishors', 'trim|required');
            $this->form_validation->set_rules('heat_type', 'Heat Type', 'trim|required');
            if ($this->input->post('heat_type') != 'N/A') {
                $this->form_validation->set_rules('heat_filter_size', 'Heat Filter size', 'trim|required');
            }
            $this->form_validation->set_rules('ac_type', 'AC Type', 'trim|required');
            if ($this->input->post('ac_type') != 'N/A') {
                $this->form_validation->set_rules('ac_filter_size', 'AC Filter size', 'trim|required');
            }
            $this->form_validation->set_rules('pest_control_home', 'Pest Control home', 'trim|required');
            if ($this->input->post('pest_control_home') == 'yes') {
                $this->form_validation->set_rules('pest_service_provider', 'Pest Control home service provider', 'trim|required');
                $this->form_validation->set_rules('pest_service_provider_number', 'Pest Control home service provider number', 'trim|required');
            }
            $this->form_validation->set_rules('yard', 'Yard', 'trim|required');
            //$this->form_validation->set_rules('yard_service_provided', 'Yard service provided', 'trim|required');
            if ($this->input->post('yard') == 'yes' && $this->input->post('yard_service_provided') == 'yes') {
                $this->form_validation->set_rules('yard_service_provider', 'Yard service provider', 'trim|required');
                $this->form_validation->set_rules('yard_service_provider_number', 'Yard service provider number', 'trim|required');
            }

            if (empty($_FILES['profile_pic']['name'])) {
                //$this->form_validation->set_rules('profile_pic', 'Profile Image', 'required');
            }
            $error_message = '';
            if ($this->form_validation->run()) {
                $config['upload_path'] = './assets/uploads/community_documents/';
                $config['allowed_types'] = 'jpeg|jpg|png|txt|pdf|doc|docx';
                $config['encrypt_name'] = TRUE;
                $this->load->library('upload', $config);
                if (!empty($_FILES['comm_docs']['name'])) {
                    if (!$this->upload->do_upload('comm_docs')) {
                        $error = $this->upload->display_errors();
                        $error_message.=$error;
                        $failure = TRUE;
                    } else {
                        $comm_data = array('upload_data' => $this->upload->data());
                        $com_doc = $comm_data['upload_data']['file_name'];
                    }
                }

                if (!$failure) {
                    $prop_id = $this->properties_model->frontAddProperty($allAmenties, $com_doc);
                    $this->session->set_userdata('PROPERTY_ID', $prop_id);
                    if(!empty($_FILES['profile_pic'])){
    					for ($i=0; $i < count($_FILES['profile_pic']); $i++) { 
    	                        //$this->load->library('upload');
    	                    if (!empty($_FILES['profile_pic']['name'][$i])) {
    	                        $_FILES['profile_pi']['name'] = $_FILES['profile_pic']['name'][$i];
    	                        $_FILES['profile_pi']['type'] = $_FILES['profile_pic']['type'][$i];
    	                        $_FILES['profile_pi']['tmp_name'] = $_FILES['profile_pic']['tmp_name'][$i];
    	                        $_FILES['profile_pi']['error'] = $_FILES['profile_pic']['error'][$i];
    	                        $_FILES['profile_pi']['size'] = $_FILES['profile_pic']['size'][$i];
    	                        $this->load->library('upload');
    	                        $this->upload->initialize($this->set_upload_options($prop_id, PROPERTY_PHOTOS, 'property'));
    	                        if (!$this->upload->do_upload('profile_pi')) {
    	                            $error = $this->upload->display_errors();
    	                            $error_message.=$error;
    	                            $failure = TRUE;
    	                        } else {
    	                            $data = $this->upload->data();
    	                            $gallery_add = array();
    	                            $this->thumb($data);
    	                            $thumb = $data['raw_name'] . '_thumb' . $data['file_ext'];
    	                            $orig_file = $data['raw_name'] . $data['file_ext'];
    	                            $gallery_add[] = array(
    	                                'property_id' => $prop_id,
    	                                'image' => PROPERTY_PHOTOS . "/property_" . $prop_id . "/" . $orig_file,
    	                                'thumb' => PROPERTY_PHOTOS . "/property_" . $prop_id . "/" . $thumb,
    	                                'tenant_id' => 0, //$this->session->userdata('MEM_ID'),
    	                                'status' => 'Active',
    	                                'is_main_photo' => 1,
    	                                'add_date' => current_date(),
    	                                'ip' => ''
    	                            ); 
    	                            if (isset($gallery_add) && $gallery_add != '') {
    	                                $conditions = $params = array();
    	                                $params['batch_mode'] = true;
    	                                $conditions = $gallery_add;
    	                                $params['table'] = 'tbl_property_photos';
    	                                $insert_id = $this->common_model->insert_data($conditions, $params);
    	                            }
    	                            //$this->member_model->uploadProfilePic($this->mem_id, $data['upload_data']['file_name']);
    	                        }
    	                    }
    	                }
                    }
                    if ($prop_id) {
                        $success_message = '<p>Property Information Added successfully.</p>';
                        $this->session->set_userdata('PROPERTY_ID', $prop_id);
                    } else {
                        $error_message = '<p>Sorry for inconvenience There is something going wrong...please try again later.</p>';
                        $failure = true;
                    }
                }
            } else {
                $error_message = validation_errors();
                $failure = true;
            }
            if ($this->input->is_ajax_request()) {
                if ($failure) {
                    $data['success'] = false;
                    $data['error_message'] = $error_message;
                } else {
                    $data['success'] = true;
                    $data['resetForm'] = true;
                    $data['success_message'] = $success_message;
                    $data['url'] = site_url('add-property-step2');
                }
                $data['scrollToElement'] = true;
                echo json_encode($data);
                die;
            }
        }

//get incomplete properties
        $output['incomplete'] = $this->getIncompleteProperties();
        //echo '<pre>';print_r($output);die;
        $this->load->view($this->config->item('defaultfolder') . '/header', $output);
        $this->load->view($this->config->item('defaultfolder') . '/add_property_step1');
        $this->load->view($this->config->item('defaultfolder') . '/add_property_js');
        $this->load->view($this->config->item('defaultfolder') . '/footer');
    }

    function getIncompleteProperties($step = false) {
        $conditions = $params = array();
        $params['complex'] = true;

        $conditions['tables'] = array(
            'property' => 'tbl_properties',
            'state' => 'tbl_region'
        );
        $conditions['table'] = 'property';
        $conditions['on']['state'] = array(
            'sign' => '=',
            'column' => 'region_id',
            'alias_column' => 'state_id',
            'alias_other' => 'property',
        );

        $conditions['where']['status']['alias'] = 'property';
        $conditions['where']['status']['value'] = 'Active';
        if($step == false){
            $conditions['where']['step_completed']['alias'] = 'property';
            $conditions['where']['step_completed']['value'] = array(
                'operator' => 'AND',
                'key' => 'status',
                'sign' => '<', 
                'value' => '3'
            );
        }else{
            $conditions['where']['step_completed']['alias'] = 'property';
            $conditions['where']['step_completed']['value'] = '3';
        }
        $conditions['where']['owner_id']['alias'] = 'property';
        $conditions['where']['owner_id']['value'] = $this->session->userdata('MEM_ID');
        $params['fields'] = array('property.prop_id', 'property.step_completed', 'property.city', 'property.address1', 'property.address2', 'property.zip', 'property.unit_number', 'state.region_name');
        $info = $this->common_model->get_data($conditions, $params);
        //echo $this->db->last_query(); exit;
        return $info;
    }

    function add_step2() {
        $output['title'] = 'Properties';
        //print_R($_REQUEST);
        $step_completed = $this->common_model->getHigestCompletedStepProeprty();
        $allProperties = $this->properties_model->frontGetCountAllPropertiesByLandlordId($this->session->userdata('MEM_ID'));
        if (!$allProperties || ($allProperties == 1 && $step_completed < 3)) {
            $output['selected_tab'] = 'first-property';
        } else {
            $output['selected_tab'] = 'properties';
        }
        //    echo $this->session->userdata('PROPERTY_ID');die;
        //  $step_completed = $this->common_model->getLastProeprty();
        if (isset($_GET['propid']) && !empty($_GET['propid'])) {
            $this->session->set_userdata('PROPERTY_ID', $_GET['propid']);
        } else if (!$this->session->userdata('PROPERTY_ID')) {
            redirect(site_url('add-property'));
        }
        $error_message = '';
        /* else {
          if ($step_completed == 2)
          redirect(site_url('add-property-step3'));
          if (!$step_completed)
          redirect(site_url('add-property'));

          if (!$this->session->userdata('PROPERTY_ID'))
          redirect(site_url('add-property'));
          } */


        /*         * ******** Start loading model as required in function ***** */
        $this->load->model('region_model');
        $this->load->model('member_model');
        /*         * ******** End loading model as required in function ******* */

        /*         * ******** Fetching mailing address for rent payment ************** */
        $output['mailingRecord'] = $this->properties_model->getMailingaddress($this->session->userdata('MEM_ID'));
        $output['memberRecord'] = $this->member_model->getMemberRecordById($this->session->userdata('MEM_ID'));

        /*         * ******** Get Property Detail By property Id ************** */
        $output['propertyDetail'] = $propertyDetail = $this->properties_model->getPropertyById($this->session->userdata('PROPERTY_ID'));
        if (!empty($_POST)) {
            $failure = 0;
            $this->form_validation->set_message('required', '%s is required');
            if ($this->input->post('address_1')) {
                $this->form_validation->set_rules('address_1', 'Mailing address 1 ', 'trim|required');
                // $this->form_validation->set_rules('address_2', 'Mailing address 2 ', 'trim|required');
                //$this->form_validation->set_rules('unit', 'Unit', 'trim|is_numeric');
                $this->form_validation->set_rules('city', 'City', 'trim|required');
                $this->form_validation->set_rules('state', 'State', 'trim|required');
                $this->form_validation->set_rules('zip', 'Zip', 'trim|required|is_numeric|greater_than[0]');
            }
            $this->form_validation->set_rules('is_vacant', 'Is Vacant ', 'trim');
            if ($this->input->post('is_vacant') != 'Yes') {
                for ($i = 1; $i <= $this->input->post('total_tenant'); $i++) {
                    $this->form_validation->set_rules('first_name_' . $i, 'First Name ', 'trim|required');
                    $this->form_validation->set_rules('last_name_' . $i, 'Last Name ', 'trim|required');
                    $this->form_validation->set_rules('dob_' . $i, 'DOB ', 'trim|required');
                    // $this->form_validation->set_rules('email_' . $i, 'Email ', 'trim|required|is_unique[tbl_members.email]');
                    $this->form_validation->set_rules('email_' . $i, 'Email ', 'trim|required');
                    //$this->form_validation->set_rules('password_'.$i, 'Password ', 'trim|required');
                    $this->form_validation->set_rules('mobile_' . $i, 'Mobile ', 'trim|required');
                    $this->form_validation->set_rules('preferred_language_' . $i, 'Preferred language ', 'trim|required');
                    $this->form_validation->set_rules('lease_start_date_' . $i, 'Lease Start Date ', 'trim|required');
                    $this->form_validation->set_rules('lease_end_date_' . $i, 'Lease End Date ', 'trim|required');
                    $this->form_validation->set_rules('rent_amount_' . $i, 'Rent Amount ', 'trim|required');
                    $this->form_validation->set_rules('due_date_' . $i, 'Due Date ', 'trim|required');
                    $this->form_validation->set_rules('late_fee_' . $i, 'Late Fee ', 'trim|required|is_numeric|greater_than[0]');
                    $this->form_validation->set_rules('late_fee_type_' . $i, 'Late Fee Type', 'trim|required');
                    $this->form_validation->set_rules('late_fee_start_' . $i, 'Late Fee Start', 'trim|required');
                    if (empty($_FILES['upload_lease_'.$i]['name'])){
                       //$this->form_validation->set_rules("upload_lease_".$i, ' Tenant '.$i.' lease document ', 'required');
                    }                        
                }
            }
            if ($this->form_validation->run()) {
                $data = array();
                $config['upload_path'] = './assets/uploads/lease_documents/';
                $config['allowed_types'] = 'jpeg|jpg|png|txt|pdf|doc';
                //$config['allowed_types'] = 'jpeg|jpg|png|pdf';
                $config['encrypt_name'] = TRUE;
                $this->load->library('upload', $config);
                for ($i = 1; $i <= $this->input->post('total_tenant'); $i++) {
                    if (isset($_FILES) && !empty($_FILES['upload_lease_' . $i]['name'])) {
                        if (!$this->upload->do_upload('upload_lease_' . $i)) {
                            $error = $this->upload->display_errors();
                            $error_message.=$error;
                            $failure = TRUE;
                        } else {
                            $data[] = array('upload_data' => $this->upload->data());
                        }
                    }
                }
                if (!$failure) {
                    $this->member_model->frontAddMAilingAddress();
                    $this->properties_model->frontAddTenantsToProperty($this->session->userdata('PROPERTY_ID'), $data);
                    $success_message = '<p>Tenants Added to Property successfully.</p>';
                }
            } else {
                $error_message = validation_errors();
                $failure = true;
            }


            if ($this->input->is_ajax_request()) {
                if ($failure) {
                    $data['success'] = false;
                    $data['error_message'] = $error_message;
                } else {
                    $data['success'] = true;
                    $data['resetForm'] = true;
                    $data['success_message'] = $success_message;
                    $data['url'] = site_url('add-property-step3');
                }
                $data['scrollToElement'] = true;
                echo json_encode($data);
                die;
            }
        }
        /*         * ******** Get all regions/states ************************* */
        //$output['allRegion']  =   $this->region_model->getAllRegions();
        $output['allRegion'] = $this->region_model->getAllRegionsByCountryId(223);
        //   echo '<pre>';print_r($output);die;
        $this->load->view($this->config->item('defaultfolder') . '/header', $output);
        $this->load->view($this->config->item('defaultfolder') . '/add_property_step2');
        $this->load->view($this->config->item('defaultfolder') . '/add_property_js');
        $this->load->view($this->config->item('defaultfolder') . '/footer');
    }

    function add_step3() {
        $output['title'] = 'Properties'; 
        //print_r($this->input->post()); exit;
        $step_completed = $this->common_model->getHigestCompletedStepProeprty();
        $allProperties = $this->properties_model->frontGetCountAllPropertiesByLandlordId($this->session->userdata('MEM_ID'));
        if (!$allProperties || $allProperties == 1 && $step_completed < 4) {
            $output['selected_tab'] = 'first-property';
        } else {
            $output['selected_tab'] = 'properties';
        }
        $output['tPayment'] = true;

        if (isset($_GET['propid']) && !empty($_GET['propid'])) {
            $this->session->set_userdata('PROPERTY_ID', $_GET['propid']);
        } else if (!$this->session->userdata('PROPERTY_ID')) {
            redirect(site_url('add-property'));
        }
        /*         * ******** Start loading model as required in function ***** */
        $this->load->model('services_model');
        $this->load->model('tservices_model');
        $this->load->model('member_model');
        $this->load->model('vendor_model');
        /*         * ******** End loading model as required in function ******* */

        if (!empty($_POST)) {
            if (isset($_POST['vendor_ids']) && !empty($_POST['vendor_ids'])) {
                $dataarr = $_POST['vendor_ids'];
                $vendors = array_filter($dataarr);
            } else {
                $vendors = array();
            }

            if (isset($_POST['company_name']) && !empty($_POST['company_name'])) {
                $newvndr = $_POST['company_name'];
            } else {
                $newvndr = array();
            }          
            //$this->vendor_model->deleteVendorsOfProperty($this->session->userdata('PROPERTY_ID'));
            $conditions = $params = array();
            $params['table'] = 'tbl_vendors';
            $conditions['property_id'] = $this->session->userdata('PROPERTY_ID');
            $params['fields'] = array('v_id','company_name', 'mobile_no', 'email', 'service');
            $prop_ven_info = $this->common_model->get_data($conditions, $params);
            $old_vendors_of_prop = array();
            foreach ($prop_ven_info as $prop_vndr) {
                $old_vendors_of_prop[] = $prop_vndr->v_id;
            }
            //print_R($prop_ven_info);
            if (!empty($vendors)) {
                foreach ($vendors as $vndr) {
                    $conditions = $params = array();
                    $params['table'] = 'tbl_vendors';
                    $conditions['v_id'] = $vndr;
                    $params['single_row'] = true;
                    $params['fields'] = array('company_name', 'mobile_no', 'email', 'service');
                    $info = $this->common_model->get_data($conditions, $params);
                    $insert_vndors[] = array(
                        'vendor_owner_id' => $this->session->userdata('MEM_ID'),
                        'company_name' => $info->company_name,
                        'mobile_no' => $info->mobile_no,
                        'email' => $info->email,
                        'service' => $info->service,
                        'add_date' => time(),
                        'status' => 'Active',
                        'property_id' => $this->session->userdata('PROPERTY_ID')
                    );
                }
                $conditions = $params = array();
                $params['batch_mode'] = true;
                $conditions = $insert_vndors;
                $params['table'] = 'tbl_vendors';
                $this->common_model->insert_data($conditions, $params);
            }

            if (!empty($newvndr)) {
                $cntc = $_POST['company_contact'];
                $vemail = $_POST['email'];
                $vservice = $_POST['service'];
                foreach ($newvndr as $key => $nwvndr) {
                    $insert_nvndors[] = array(
                        'vendor_owner_id' => $this->session->userdata('MEM_ID'),
                        'company_name' => $nwvndr,
                        'mobile_no' => $cntc[$key],
                        'email' => $vemail[$key],
                        'service' => implode(",",$vservice[$key]),
                        'add_date' => time(),
                        'status' => 'Active',
                        'property_id' => $this->session->userdata('PROPERTY_ID')
                    );
                }
                $conditions = $params = array();
                $params['batch_mode'] = true;
                $conditions = $insert_nvndors;
                $params['table'] = 'tbl_vendors';
                $this->common_model->insert_data($conditions, $params);
            }
            $this->vendor_model->deleteVendorsOfPropertyByVendorIds($old_vendors_of_prop);
                
            if(empty($this->input->post('tMessaging')) && empty($this->input->post('tMaintenace')) && empty($this->input->post('tPay'))){
                $this->properties_model->frontAddVendorsAndTServicesToPropertyStep3($this->session->userdata('PROPERTY_ID'),true);                
            }else{
                $this->properties_model->frontAddVendorsAndTServicesToPropertyStep3($this->session->userdata('PROPERTY_ID'));
            }
            $success_message = '<p>tServices and Vendors Information saved successfully.</p>';
            /*if ($this->input->is_ajax_request()) {
                $data['success'] = true;
                $data['resetForm'] = true;
                $data['success_message'] = $success_message;
                if ($this->input->post('action-type') == 'save') {
                    $this->session->set_flashdata('addmoreprop', 'Add more property');
                    $this->checkoutProperty($this->session->userdata('PROPERTY_ID'));
                }
                $data['url'] = site_url('dashboard');
                $data['scrollToElement'] = true;
                echo json_encode($data);
                die;
            }*/
            //print_R($_POST);
            if ($this->input->is_ajax_request()) {
                if($this->input->post('action-type') == 'save'){
                    /*if(empty($this->input->post('tMessaging')) && empty($this->input->post('tMaintenace')) && empty($this->input->post('tPay'))){
                        $data['validation_error_message'] = 'Please select any tServices';
                        $data['validation_failure'] = true; 
                        echo json_encode($data);
                        die; 
                    }else{*/
                        if(!empty($this->input->post('tMaintenace'))){
                            if(!empty($_POST['vendor_ids']) || !empty($newvndr)){
                                /*if(empty($this->input->post('tMessaging')) && empty($this->input->post('tMaintenace')) && empty($this->input->post('tPay'))){*/
                                    /*$data['success'] = true;
                                    $data['resetForm'] = true;
                                    $data['success_message'] = $success_message;                    
                                    $data['url'] = site_url('dashboard');
                                    $data['scrollToElement'] = true;
                                    echo json_encode($data);
                                    die;*/
                               /* }else{*/
                                    $data['checkout'] = 'yes';
                                    $data['url'] = base_url().'checkout_process';
                                    echo json_encode($data);
                                    die;                                                
                               /* }*/
                            }else {
                                $data['validation_error_message'] = 'Please select any vendor';
                                $data['validation_failure'] = true; 
                                echo json_encode($data);
                                die;    
                            }
                        }else{
                            $data['checkout'] = 'yes';
                            $data['url'] = base_url().'checkout_process';
                            echo json_encode($data);
                            die;                                                
                        }
                    /*}*/
                }else{
                    $data['success'] = true;
                    $data['resetForm'] = true;
                    $data['success_message'] = $success_message;                    
                    $data['url'] = site_url('dashboard');
                    $data['scrollToElement'] = true;
                    echo json_encode($data);
                    die;    
                }
            }
        }
        
        /*         * ******** Get All Services ******************************** */
        $output['allServices'] = $this->services_model->frontGetAllServices();
        $output['vendorServices'] = $this->tservices_model->frontGetVendorServices();
        $output['property_info'] = $this->properties_model->getPropertyById($this->session->userdata('PROPERTY_ID'));
 
        /*         * ******** Get All Vendors by owner ID ******************************** */
        $allVendors = $this->vendor_model->getDistinctVendors($this->mem_id, $this->session->userdata('PROPERTY_ID'));
        //echo $this->db->last_query(); exit;
        $output['allVendors'] = $allVendors;
        $allVendorsOfProperty = $this->vendor_model->getDistinctVendorsOfParticularProperty($this->session->userdata('PROPERTY_ID'));
        if(!empty($allVendorsOfProperty)){
            foreach ($allVendors as $key => $value) {
                //print_r($value); exit;
               foreach ($allVendorsOfProperty as $pro_key => $pro_value) {
                    if($value->email == $pro_value->email){
                        $value->v_id = $pro_value->v_id;
                        $value->property_id = $pro_value->property_id;
                        $value->company_name = $pro_value->company_name;
                        $value->mobile_no = $pro_value->mobile_no;
                        $value->email = $pro_value->email;
                        $value->service = $pro_value->service;
                        $value->add_date = $pro_value->add_date;
                    }
               }
            }
        }
        $output['allVendorsOfProperty'] = $allVendorsOfProperty;        
        //echo '<pre>';print_r($output['allVendors']);print_r($output['allVendorsOfProperty']);die;
        //echo '<pre>';print_r( $output['allVendors'] );die;
        $output['tpay_account_status'] = 0; // acount active
        if(getSynapseSetUpStatus($this->session->userdata('MEM_ID')) == true){
            $output['tpay_account_status'] = 1;     //acount Active
        }
        
        $this->load->view($this->config->item('defaultfolder') . '/header', $output);
        $this->load->view($this->config->item('defaultfolder') . '/add_property_step3');
        $this->load->view($this->config->item('defaultfolder') . '/step3_js');
        $this->load->view($this->config->item('defaultfolder') . '/add_property_js');
        $this->load->view($this->config->item('defaultfolder') . '/footer');
    }

    function delete_recent_bank_statment(){
        if(!empty($_REQUEST['id'])){
            $info_id = $_REQUEST['id'];                
            $insert_data = array(
                'recent_bank_statment'       => ''
            );
            $conditions['where']['id'] = $info_id;
            $conditions = $params = array();
            $conditions['value'] = $insert_data;
            $params['table'] = 'tbl_linked_account_info';
            $this->common_model->update($conditions, $params);
            echo 'success';
        }
    }
    function delete_driving_document(){
        if(!empty($_REQUEST['id'])){
            $info_id = $_REQUEST['id'];
            $insert_data = array(
                'driving_document'       => ''
            );
            $conditions['where']['id'] = $info_id;
            $conditions = $params = array();
            $conditions['value'] = $insert_data;
            $params['table'] = 'tbl_linked_account_info';
            $this->common_model->update($conditions, $params);
            echo 'success';
        }
    }


    public function account_setup(){
        $this->createSynapsepayDepositAccount($this->session->userdata('MEM_ID'));
        $output['title'] = 'Properties';
        $output['tpay_account_status'] = 0; // acount active
        if(getSynapseSetUpStatus($this->session->userdata('MEM_ID')) == true){
            $output['tpay_account_status'] = 1;     //acount Active
        }
        /**************** *************************************/
        $this->checkSynaspeUser($this->session->userdata('MEM_ID'));
        $fingerprint_auth_key = $this->createSynapseUser($this->session->userdata('MEM_ID'));
 		//print_R($fingerprint_auth_key); exit; 
        $this->updateSynapseUserDetail();
        
        //$this->getSynapseUserDetailBySynapseId($this->session->userdata('MEM_ID'));
        $conditions = $params = array();
        $conditions['user_id'] = $this->session->userdata('MEM_ID');
        $params['table'] = 'tbl_linked_account_info';
        $linked_data = $this->common_model->get_data($conditions, $params);
        if(empty($linked_data)){
	        $conditions = $params = array();
	        $conditions['user_id'] = $this->session->userdata('MEM_ID');
	        $params['table'] = 'tbl_linked_account_info';
	        $this->common_model->insert_data($conditions, $params);        	
        }
        /******************************************************************************/
        $step_completed = $this->common_model->getHigestCompletedStepProeprty();
        $allProperties = $this->properties_model->frontGetCountAllPropertiesByLandlordId($this->session->userdata('MEM_ID'));
        $failure = false;
        if (!$allProperties || $allProperties == 1 && $step_completed < 3) {
            $output['selected_tab'] = 'first-property';
        } else {
            $output['selected_tab'] = 'properties';
        }
        $output['tPayment'] = true;
        /**************** Start loading model as required in function ********************** */
        $this->load->model('services_model');
        $this->load->model('tservices_model');
        $this->load->model('member_model');
        $this->load->model('vendor_model');
        /**************** End loading model as required in function *********************/

        $conditions = $params = array();
        $params['table'] = 'tbl_linked_account_info';
        $conditions['user_id'] = $this->session->userdata('MEM_ID');
        $conditions['status'] = 1;
        $params['single_row']  = true;
        $info = $this->common_model->get_data($conditions, $params);
        //echo "<pre>"; print_R($info);exit;
        if (!empty($_POST)) {
            if($this->input->post('action-type') == 'save'){
                $this->form_validation->set_rules('first_name', 'First Name', 'trim|required');
                $this->form_validation->set_rules('last_name', 'Last name', 'trim|required');
                $this->form_validation->set_rules('phone', 'Phone', 'trim|required');
                $this->form_validation->set_rules('email', 'Email', 'trim|required');
                //$this->form_validation->set_rules('address_line_1', 'Address 1', 'trim|required');
                $this->form_validation->set_rules('address_street', 'Street address', 'trim|required');
/*                $this->form_validation->set_rules('unit', 'Unit', 'trim|is_numeric|greater_than[0]');*/
                $this->form_validation->set_rules('address_city', 'Address City', 'trim|required');
                $this->form_validation->set_rules('address_subdivision', 'Address subdivision', 'trim|required');
                $this->form_validation->set_rules('address_zip_code', 'Address zip code', 'trim|required|is_numeric');
                $this->form_validation->set_rules('dob', 'DOB', 'trim|required');
                $this->form_validation->set_rules('social_security', 'S.S', 'trim|required');
                $this->form_validation->set_rules('ach_aggrement', 'ACH aggrement', 'trim|required');
                $this->form_validation->set_rules('terms_conditions', 'Terms & conditions', 'trim|required');
                //$this->form_validation->set_rules('driving_document', 'Driving Document', 'trim|required');
                //$this->form_validation->set_rules('recent_bank_statment', 'Recent bank statement', 'trim|required');
            }
            $error = $error_message = $failure = '';
            if ($this->form_validation->run() || $this->input->post('action-type') == 'save-later') {

                // START: File upload
                if(!empty($info)){
                    $driving_document = $info->driving_document;
                   // $recent_bank_statment =  $info->recent_bank_statment;
                }else{
                    $driving_document = '';
                 //   $recent_bank_statment =  '';
                }
                $folder = USER_DOCUMENT;

                if (!is_dir($folder . "/user_" . $this->session->userdata('MEM_ID'))) {
                    mkdir($folder . "/user_" . $this->session->userdata('MEM_ID'), 0777, TRUE);
                }
                $config['upload_path'] = './assets/uploads/user_document/user_'.$this->session->userdata('MEM_ID');
                $config['allowed_types'] = 'jpeg|jpg|png|txt|pdf|doc|docx';
                $config['encrypt_name'] = TRUE;
                $config['overwrite'] = FALSE;
                $this->load->library('upload', $config);
                if (!empty($_FILES['driving_document']['name'])) {
                    if (!$this->upload->do_upload('driving_document')) {
                        $error = $this->upload->display_errors();
                        $error_message.=$error;
                        $failure = TRUE;
                    } else {
                        $driving_document_data = array('upload_data' => $this->upload->data());
                        $driving_document = $driving_document_data['upload_data']['file_name'];
                    }
                }

                //print_r($driving_document_data);
                //print_r($error_message); exit;
              /*  if (!empty($_FILES['recent_bank_statment']['name'])) {
                    if (!$this->upload->do_upload('recent_bank_statment')) {
                        $error = $this->upload->display_errors();
                        $error_message.=$error;
                        $failure = TRUE;
                    } else {
                        $recent_bank_statment_data = array('upload_data' => $this->upload->data());
                        $recent_bank_statment = $recent_bank_statment_data['upload_data']['file_name'];
                    }
                }*/
                // END: File upload
                $ach_aggrement = isset($_REQUEST['ach_aggrement']) ? 'Yes' : 'No';
                $terms_conditions = isset($_REQUEST['terms_conditions']) ? 'Yes' : 'No';
                if($this->input->post('action-type') != 'save-later'){
                    $status = 1;
                }else{
                    $status = 0;
                }

                if(!empty($driving_document)){

                    /******************************* START: Update data  in all the row of user *********************************************/
                    $conditions = $params = array();
                    $insert_data = array(
                        'first_name'            => $this->input->post('first_name'),
                        'middle_name'           => $this->input->post('middle_name'),
                        'last_name'             => $this->input->post('last_name'),
                        'phone'                 => $this->input->post('phone'),
                        'email'                 => $this->input->post('email'),
                        'address_street'        => $this->input->post('address_street'),
                        'address_country_code'  => $this->input->post('address_country_code'),
                        'address_line_1'  => $this->input->post('address_line_1'),
                        'address_city'          => $this->input->post('address_city'),
                        'address_subdivision'   => $this->input->post('address_subdivision'),
                        'social_security'       => $this->input->post('social_security'),
                        'address_zip_code'      => $this->input->post('address_zip_code'),
                        'dob'                   => getDateByFormat($this->input->post('dob'),'Y-m-d'),
                        'driving_document'      => $driving_document,
                        'recent_bank_statment'  => $driving_document,
                        'ach_aggrement'         => $ach_aggrement,
                        'terms_conditions'      => $terms_conditions,
                        'modified_date'         => current_date(),
                        'status'                => 1,
                    );
                    $conditions['where']['user_id'] = $this->session->userdata('MEM_ID');
                    $conditions['value'] = $insert_data;
                    $params['table'] = 'tbl_linked_account_info';
                    $this->common_model->update($conditions, $params);

                    if($this->input->post('bank_account') != ''){
                        $insert_data = array(
                            'status'            => 0
                        );
                        $conditions = $params = array();
                        $conditions['value'] = $insert_data;
                        $conditions['where']['user_id'] = $this->session->userdata('MEM_ID');
                        $params['table'] = 'tbl_linked_account_numbers';
                        $this->common_model->update($conditions, $params);

                        $insert_data = array(
                            'status'            => 1
                        );
                        $conditions = $params = array();
                        $conditions['value'] = $insert_data;
                        $conditions['where']['id'] = $this->input->post('bank_account');
                        $params['table'] = 'tbl_linked_account_numbers';
                        $this->common_model->update($conditions, $params);                    
                    }


                    /******************************* END: Update data  in all the row of user *********************************************/
                    

                    
                    /************************************** START: Add document SynapsePay******************************************/
                    $directory_path = $folder . "/user_" . $this->session->userdata('MEM_ID');
                    $driving_document_path = $directory_path.'/'.$driving_document;
                    $driving_document_base = $this->file_to_base64code($driving_document_path);

                    //$recent_bank_statment_path = $directory_path.'/'.$recent_bank_statment;
                    //$recent_bank_statment_base = $this->file_to_base64code($recent_bank_statment_path);

                    $recent_bank_statment_path = $driving_document_path;
                    $recent_bank_statment_base = $driving_document_base;

                    $email                  = $this->input->post('email');
                    $phone_number           = $this->input->post('phone');
                    $ip                     = str_replace(".", "", $this->get_client_ip());
                    $name                   = $this->input->post('first_name').' '.$this->input->post('last_name');
                    $alias                  = '';
                    $entity_type            = 'M';
                    $entity_scope           = 'Business Services';
                    $time                   = strtotime($this->input->post('dob'));
                    $day                    = date("j",$time);
                    $month                  = date("n",$time);
                    $year                   = date("Y",$time);
                    $address_street         = $this->input->post('address_street');
                    $address_city           = $this->input->post('address_city');
                    $address_subdivision    = $this->input->post('address_subdivision');
                    $address_postal_code    = $this->input->post('address_zip_code'); 
                    $address_country_code   = $this->input->post('address_country_code');
                    $p_document_type        = 'GOVT_ID'; // recent Bank statement
                    $p_document_value       = $driving_document_base; //$; data:image/gif;base64,R0lGODlhMAAwAPf/AK==
                    $p_document_value_2     = $recent_bank_statment_base; //GOVT_ID';
                    $p_document_type_2      = 'AUTHORIZATION'; //$driving_document_base;

                    $v_document_type        = 'SSN'; 
                    $v_document_value       = $this->input->post('social_security');

                    /********** Get linked acc info ******************************** */ 
                    $conditions = $params = array();
                    $params['table'] = 'tbl_linked_account_info';
                    $conditions['user_id'] = $this->session->userdata('MEM_ID');
                    $conditions['is_deleted'] = 0;
                    $params['single_row'] = true;
                    $account_data = $this->common_model->get_data($conditions, $params);
                    if($account_data->kyc_permission != 'SEND-AND-RECEIVE'){
                        if($account_data->kyc_id == ''){
                            $synapse_doc            = $this->addSynapsePayDocument($this->session->userdata('MEM_ID'),$email,$phone_number,$ip,$name,$alias,$entity_type,$entity_scope,$day,$month,$year,$address_street,$address_city,$address_subdivision,$address_postal_code,$address_country_code,$v_document_value,$v_document_type,$p_document_value,$p_document_type,$p_document_value_2,$p_document_type_2);
                        }else{
                            $synapse_doc            = $this->updateSynapsePayDocument($this->session->userdata('MEM_ID'),$email,$phone_number,$ip,$name,$alias,$entity_type,$entity_scope,$day,$month,$year,$address_street,$address_city,$address_subdivision,$address_postal_code,$address_country_code,$v_document_value,$v_document_type,$p_document_value,$p_document_type,$p_document_value_2,$p_document_type_2,$account_data->kyc_id);
                        }
                    }
                    //print_r($synapse_doc); exit;
                    if(isset($synapse_doc->error->en)){
                        $error_message = $synapse_doc->error->en;
                        $failure = true;
                    }
                    /************************************** END: Add document SynapsePay******************************************/

                    /******************************* START: Update status according to account id *********************************************/
                    //echo $recent_bank_statment_base; print_R($synapse_doc); exit;
                    if(isset($synapse_doc->_id)){
                        $conditions = $params = array();
                        $update_data = array(
                            'kyc_permission'    => $synapse_doc->permission,
                            'physical_doc_status' =>$synapse_doc->doc_status->physical_doc,
                            'virtual_doc_status' =>$synapse_doc->doc_status->virtual_doc,
                            'kyc_id'    => $synapse_doc->documents[0]->id
                        );
                        $conditions['where']['user_id'] = $this->session->userdata('MEM_ID');
                        $conditions['value'] = $update_data;
                        $params['table'] = 'tbl_linked_account_info';
                        $this->common_model->update($conditions, $params);
                        $failure = false;                    
                    }
                    //print_R($synapse_doc);
                    /*echo "<pre>".$synapse_doc->_id; print_R($synapse_doc);
                    exit;*/
                    /******************************* END: Update status according to account id *********************************************/
                }else{
                    $error_message = "Driver's Licence or Photo ID";
                    $failure = true;
                }


                
            }else{
                $error_message = validation_errors();
                $failure = true;
            }
            $success_message = '<p>Payment Information saved successfully.</p>';
            if($this->input->post('redirect') == 'dashboard'){
                $r_url = site_url('dashboard');
                //$r_url = site_url('account_setup?dashboard=true');
            }else{
                $r_url = site_url('account_setup');
                //$r_url = site_url('add-property-step3');
            }
            if ($this->input->is_ajax_request()) {
                if ($this->input->post('action-type') == 'save') {
                    //$this->session->set_flashdata('addmoreprop', 'Add more property');
                    //$this->checkoutProperty($this->session->userdata('PROPERTY_ID'));
                    if ($failure) {
                        $data['success'] = false;
                        $data['error_message'] = $error_message;
                    } else {
                        $data['success'] = true;
                        $data['resetForm'] = true;
                        $data['success_message'] = $success_message;
                        $data['url'] = $r_url;
                    }
                }else{
                   $data['success'] = true;
                    $data['url'] = $r_url;
                }
                $data['scrollToElement'] = true;
                echo json_encode($data);
                die;
            }
        }

        /********** Get All vendor Services ******************************** */
        $output['vendorServices'] = $this->tservices_model->frontGetVendorServices();

        
        /********** Get linked acc info ******************************** */ 
        $conditions = $params = array();
        $params['table'] = 'tbl_linked_account_info';
        $conditions['user_id'] = $this->session->userdata('MEM_ID');
        $conditions['status'] = 1;
        $conditions['is_deleted'] = 0;
        $params['single_row'] = true;
        $account_info = $this->common_model->get_data($conditions, $params);
        $output['account_info']     = $account_info;

        /********** Get mem address ******************************** */
        $conditions = $params = array();
        $params['table'] = 'tbl_members';
        $conditions['mem_id'] = $this->session->userdata('MEM_ID');
        $params['single_row'] = true;
        $mem_info = $this->common_model->get_data($conditions, $params);
        $output['mem_info']     = $mem_info;

        // linked_account_number_Detail
        $conditions = $params = array();
        $params['table'] = 'tbl_linked_account_numbers';
        $conditions['user_id'] = $this->session->userdata('MEM_ID');
        $conditions['is_deleted'] = 0;
        $linked_account_number = $this->common_model->get_data($conditions, $params);
        $output['linked_account_number']     = $linked_account_number;

        $conditions = $params = array();
        $params['table'] = 'tbl_linked_account_numbers';
        $conditions['user_id'] = $this->session->userdata('MEM_ID');
        $conditions['status'] = 1;
        $conditions['is_deleted'] = 0;
        $params['single_row'] = true;
        $selected_linked_account_number = $this->common_model->get_data($conditions, $params);
        $output['selected_linked_account_number']     = $selected_linked_account_number;
        $output['fingerprint_auth_key']     = $fingerprint_auth_key['auth_key'];

        //echo "<pre>";        //print_R($output);        //exit;
        $output['getCommonQuestionByCategory']     = $this->getCommonQuestionByCategory(6); //tpay
        $this->load->view($this->config->item('defaultfolder') . '/header', $output);
        $this->load->view($this->config->item('defaultfolder') . '/account_setup');
        $this->load->view($this->config->item('defaultfolder') . '/step3_js');
        $this->load->view($this->config->item('defaultfolder') . '/add_property_js');
        $this->load->view($this->config->item('defaultfolder') . '/footer');
    }


    function checkSynaspeUser($user_id){
        $login_id               = $user_id;
        $user_info              = $this->getUserInfo($login_id);
        $SYNAPSE_CLIENT_ID      = SYNAPSE_CLIENT_ID;
        $SYNAPSE_CLIENT_SECRET  = SYNAPSE_CLIENT_SECRET; 
        $synapse_data           = $this->gerUserSynaspeFingerprintAndIp($login_id);
        $SYNAPSE_IP_ADDRESS     = $synapse_data->synapse_ip_address; 
        $SYNAPSE_FINGERPRINT    = $synapse_data->synapse_fingerprint;         
        //print_r($synapse_data); print_r($SYNAPSE_CLIENT_ID);  
        if(empty($user_info->synapse_user_id)){
            $email      = $user_info->email;
            $mobile_no  = $user_info->mobile_no;
            $legal_name = $user_info->first_name.' '.$user_info->last_name;

            /************************** create user using curl *************************/
            $curl       = curl_init();
            curl_setopt_array($curl, array(
                                            CURLOPT_URL             => SYNAPSE_API_BASE_URL."users",
                                            CURLOPT_RETURNTRANSFER  => true,
                                            CURLOPT_ENCODING        => "",
                                            CURLOPT_MAXREDIRS       => 10,
                                            CURLOPT_TIMEOUT         => 30,
                                            CURLOPT_HTTP_VERSION    => CURL_HTTP_VERSION_1_1,
                                            CURLOPT_CUSTOMREQUEST   => "POST",
                                            CURLOPT_POSTFIELDS      => "{\n  \"logins\": [\n    {\n      \"email\": \"$email\"\n    }\n  ],\n  \"phone_numbers\": [\n    \"$mobile_no\"\n  ],\n  \"legal_names\": [\n    \"$legal_name\"\n  ],\n  \"extra\": {\n    \"note\": null,\n    \"is_business\": false,\n    \"cip_tag\":2\n  }\n}",
                                            CURLOPT_HTTPHEADER      => array(
                                                                                "cache-control: no-cache",
                                                                                "content-type: application/json",
                                                                                "x-sp-gateway: $SYNAPSE_CLIENT_ID|$SYNAPSE_CLIENT_SECRET",
                                                                                "x-sp-user: |$SYNAPSE_FINGERPRINT",
                                                                                "x-sp-user-ip: $SYNAPSE_IP_ADDRESS"
                                                                            ),
                                        )
                            );
            $response   = curl_exec($curl);
            $err        = curl_error($curl);
            curl_close($curl);
            //echo SYNAPSE_API_BASE_URL."users";
            //print_r($response); exit;
            if ($err) {
                return false;
            } else {
                $json_decode           = json_decode($response); // decode the "users's API Response
                //echo "<pre>".$mobile_no; print_r($json_decode); exit;
                $synapse_id            = $json_decode->_id; 
                $refresh_token         = $json_decode->refresh_token;
                /********************** update synapse user id in the table **************************/
                $conditions = $params = array();
                $conditions['value']['synapse_user_id'] = $synapse_id;
                $conditions['where']['mem_id']          = $login_id;
                $params['table'] = 'tbl_members';
                $this->common_model->update($conditions, $params);
            }

        }
    }

    function file_to_base64code($path) {
        $type = pathinfo($path, PATHINFO_EXTENSION);
        $data = file_get_contents($path); //'jpeg|jpg|png|txt|pdf|doc|docx'
        if($type == 'jpg' || $type == 'png' || $type == 'gif' || $type == 'jpeg'){
            $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
        }else{
            $base64 = 'data:application/' . $type . ';base64,' . base64_encode($data);
        }
        return $base64;
    }

    public function update_linked_account(){
        //print_r($_REQUEST);
        if(isset($_REQUEST['account_number'])){
            for ($i=0; $i < count($_POST['account_number']); $i++ ) {
                $account_number = $_POST['account_number'][$i]; 
                $class          = $_POST['class'][$i];
                $bank_name      = $_POST['bank_name'][$i];
                $oid            = $_POST['oid'][$i];

                $conditions = $params = array();
                $params['table'] = 'tbl_linked_account_numbers';
                $conditions['user_id'] = $this->session->userdata('MEM_ID');
                $conditions['account_number'] = $account_number;
                $params['single_row'] = true;
                $info = $this->common_model->get_data($conditions, $params);
                if(empty($info)){
                    $insert_data = array(
                        'oid'               => $oid,
                        'user_id'    => $this->session->userdata('MEM_ID'),
                        'account_number'    => $account_number,
                        'bank_name'         => $bank_name,
                        'class'             => $class,
                        'modified_date' => current_date(),
                        'created_date'  => current_date(),
                        'synapse_micro_permission' =>'',
                    );
                    $conditions = $params = array();
                    $conditions = $insert_data;
                    $params['table'] = 'tbl_linked_account_numbers';
                    $this->common_model->insert_data($conditions, $params);

                }else{
                    $insert_data = array(
                        'oid'               => $oid,
                        'class'             => $class,
                        'bank_name'         => $bank_name,
                    );
                    $conditions = $params = array();
                    $conditions['value'] = $insert_data;
                    $conditions['where']['id'] = $info->id;
                    $params['table'] = 'tbl_linked_account_numbers';
                    $this->common_model->update($conditions, $params);
                }
            }
        }
        exit;
    }
 
    public function delete_linked_account_number() {

        if($this->input->post('id')){
            $insert_data = array(
                        'is_deleted'               => 1
                    );
            $conditions = $params = array();
            $conditions['value'] = $insert_data;
            $conditions['where']['id'] = $this->input->post('id');
            $conditions['where']['user_id'] = $this->session->userdata('MEM_ID');
            $params['table'] = 'tbl_linked_account_numbers';
            $this->common_model->update($conditions, $params);
            echo 'success';
            exit;       
        }else{
            echo 'empty';
            exit;
        }
        
    }



    public function get_saved_address_linked() {
        /********** Get linked acc nfo ******************************** */
        $conditions = $params = array();
        $params['table'] = 'tbl_linked_account_info';
        $conditions['user_id'] = $this->session->userdata('MEM_ID');
        $params['single_row'] = true;
        $account_info = $this->common_model->get_data($conditions, $params);            
        echo json_encode($account_info); 
        exit;
    }

     public function get_same_address_as_rent() {           
        /********** Get mem address ******************************** */
        $conditions = $params = array();
        $params['table'] = 'tbl_members';
        $conditions['mem_id'] = $this->session->userdata('MEM_ID');
        $params['single_row'] = true;
        $mem_info = $this->common_model->get_data($conditions, $params);
        if(isset($mem_info->mailing_state_id)){
            if($mem_info->mailing_state_id != 0 ){
                $mem_info->mailing_state_id = get_state_name($mem_info->mailing_state_id);
            }else{
                $mem_info->mailing_state_id = ''; 
            }
            $mem_info->dob = getDateByFormat($mem_info->dob,'d/m/Y');
        }

        //print_R($mem_info); exit;
        echo json_encode($mem_info); 
        exit;
    }



    function checkout_process(){
        $output['title'] = 'Properties';
        $step_completed = $this->common_model->getHigestCompletedStepProeprty();
        $allProperties = $this->properties_model->frontGetCountAllPropertiesByLandlordId($this->session->userdata('MEM_ID'));
        if (!$allProperties || $allProperties == 1 && $step_completed < 3) {
            $output['selected_tab'] = 'first-property';
        } else {
            $output['selected_tab'] = 'properties';
        }
        $output['tPayment'] = true;
        //echo "<pre>"; print_R($this->session->userdata()); exit;
        if (isset($_GET['propid']) && !empty($_GET['propid'])) {
            //$this->session->set_userdata('PROPERTY_ID', $_GET['propid']);
        } else if (!$this->session->userdata('PROPERTY_ID')) {
            //redirect(site_url('add-property'));
        }
        /*         * ******** Start loading model as required in function ***** */
        $this->load->model('services_model');
        $this->load->model('tservices_model');
        $this->load->model('member_model');
        $this->load->model('vendor_model');
        /*         * ******** End loading model as required in function ******* */

        
        if (!empty($_POST)) {
            //echo '<pre>';print_r( $_POST );die; 
            $success_message = '<p>Payment Information saved successfully.</p>';
            if ($this->input->is_ajax_request()) {
                $data['success'] = true;
                $data['resetForm'] = true;
                $data['success_message'] = $success_message;
                if ($this->input->post('action-type') == 'save') {
                    //$this->session->set_flashdata('addmoreprop', 'Add more property');
                    //$this->checkoutProperty($this->session->userdata('PROPERTY_ID'));
                    $data['url'] = site_url('add-property-step3');
                }else{
                    $data['url'] = site_url('dashboard');
                }
                $data['scrollToElement'] = true;
                echo json_encode($data);
                die;
            }
        } 
        //$output['payment_info'] = $this->checkTservicesPaymentInfo($this->session->userdata('PROPERTY_ID'));
        /********** Get All vendor Services ******************************** */
        $output['vendorServices']       = $this->tservices_model->frontGetVendorServices();
        //$output['property_info']        = $this->properties_model->getPropertyById($this->session->userdata('PROPERTY_ID'));
        $output['properties_info']      = $this->getIncompleteProperties($step=3);
        $output['total_amount']         = count($output['properties_info']) * PER_PROPERTY_AMOUNT; 
        $output['memberRecord']         = $this->member_model->getMemberRecordById($this->session->userdata('MEM_ID'));
        $output['stripe_customer_card'] = $this->get_customer_saved_card($this->session->userdata('MEM_ID'));
        $this->load->view($this->config->item('defaultfolder') . '/header', $output);
        $this->load->view($this->config->item('defaultfolder') . '/checkout_process');
        $this->load->view($this->config->item('defaultfolder') . '/step3_js');
        $this->load->view($this->config->item('defaultfolder') . '/add_property_js');
        $this->load->view($this->config->item('defaultfolder') . '/footer');
    }

    function get_customer_saved_card($user_id){
        $conditions = $params_c = array();
        $params['table'] = 'tbl_stripe_card_info';
        $conditions['stripe_customer_id'] = array(
                                            'operator' => 'AND',
                                            'key' => 'stripe_customer_id',
                                            'sign' => '!=',
                                            'value' => ''
                                        );
        $conditions['user_id'] = $user_id;
        $params['group_by'] = 'fingerprint';
        $stripe_cards = $this->common_model->get_data($conditions, $params);
        return $stripe_cards;
    }

    function calculatePropertyChargesAmount($total_properties){
        $total_amount = PER_PROPERTY_AMOUNT * $total_properties;
        return $total_amount;
    }


    function send_test_etext_message() {
        // Load the library, call by the name of the
        $numbers = $this->common_model->getMobileNumbers(array($this->session->userdata('MEM_ID')));
        if (!empty($numbers)) {
            $mobile_number = $numbers[0]->mobile_no;  //(904) 683-8001
            $user = $numbers[0]->mem_id;
            $msg = 'Test message: Your rent is due in 3 days.';
            $msg = $msg." -".SIGNATURE;
            $this->load->helper('twilio_helper');  // references library/twilio_helper.php
            $service = get_twilio_service();
            try {
                $number = "+1" . preg_replace("/[^0-9]/", "", $mobile_number);
                $service->account->sms_messages->create("+19046743077", "$number", "$msg", array());
                //$this->message_log($msg, $number, $user);
                $data['scrollToElement'] = true;
                $data['success'] = true;
                $data['msg'] = 'Successfully Sent';
                $this->session->set_flashdata('addmoreprop', 'Add more property');
                echo json_encode($data);
                die;
            } catch (Exception $e) {
                $file = 'sms_error_logs.txt';
                if (file_exists($file)) {
                    $current = file_get_contents($file);
                    $current .= $e->getMessage() . "\n";
                    file_put_contents($file, $current);
                }
                $data['scrollToElement'] = true;
                $data['success'] = false;
                $data['msg'] = $e->getMessage();
                echo json_encode($data);
                die;
            }
        }
    }


    function send_test_tmaintenance_email() {
        // Load the library, call by the name of the
        $this->load->model('member_model');
        $this->load->model('mailsending_model');
        $numbers = $this->common_model->getMobileNumbers(array($this->session->userdata('MEM_ID')));
        $member_detail = $this->member_model->getMemberRecordById($this->session->userdata('MEM_ID'));
        $result = $this->mailsending_model->send_test_tmaintenance_email($member_detail->email);
        //print_R($member_detail->email);
        if($result == true){
            $data['scrollToElement'] = true;
            $data['success'] = true;
            $data['msg'] = "Test email successfully sent.";

        }else{
            $data['scrollToElement'] = true;
            $data['success'] = false;
            $data['msg'] = $result;
        }
        echo json_encode($data);
        die;
    }


    /**
     * Method Name : getUserInfo
     * Author Name : LAkhvinder Singh
     * Description : get basic details of user
     */
    public function getUserInfo($user_id) {
        $conditions = $params = array();
        $params['table'] = 'tbl_members';
        $conditions['mem_id'] = $user_id;
        $params['single_row'] = TRUE;
        $params['fields'] = array('mem_id', 'synapse_user_id','synapse_fingerprint','stripe_customer_id', 'email', 'first_name', 'last_name', 'mobile_no', 'dob', 'app_verified', 'move_status','third_party_sharing');
        $info = $this->common_model->get_data($conditions, $params);
        return $info;
    }

    /**
     * Method Name : charge_by_stripe
     * Author Name : Lakhvinder Singh
     * Description : This function is user to create the transaction using stripe account, here when user come first
     * first time then the customer account will be created and the card will be added in that customer account.
     * Now the every customer wil have unique stripe customer id.
     */
    function charge_by_stripe(){
        
        require_once(FCPATH.'application/libraries/vendor/autoload.php');
        $stripe = array(
            "secret_key"      => STRIPE_SECRET_KEY,
            "publishable_key" => STRIPE_PULBLISHABLE_KEY
        );
        \Stripe\Stripe::setApiKey($stripe['secret_key']);
        $properties_ids             = $_REQUEST['property'];
        $properties_implode_ids     = implode(",",$properties_ids);
        $amount                     = str_replace(".","",amount_formated($this->calculatePropertyChargesAmount(count($properties_ids))));
        $single_property_amount     = str_replace(".","",amount_formated($this->calculatePropertyChargesAmount(1)));
        //print_R($properties_ids); exit; 
            if($_POST['card_id'] != 0){
                $login_id = $this->session->userdata('MEM_ID');
                $landlordStripeAccount = $this->getUserInfo($login_id);
                $card_id = $_POST['card_id'];
                $conditions_c = $params_c = array();
                $params_c['table'] = 'tbl_stripe_card_info';
                $conditions_c['id'] = $card_id;
                $params_c['single_row'] = TRUE;
                $stripe_card_detail = $this->common_model->get_data($conditions_c, $params_c);

                if(!empty($landlordStripeAccount->stripe_customer_id)){  
                    if( checkTrialTimePeriod($this->session->userdata('MEM_ID')) == false){
                        try {
                            $charge = \Stripe\Charge::create(
                                                                array(
                                                                    //'customer' => $landlordStripeAccount->stripe_customer_id, 
                                                                    'amount'   => $amount,
                                                                    'currency' => 'usd',
                                                                    'customer' => $landlordStripeAccount->stripe_customer_id,
                                                                    'description' => 'Property Charges',
                                                                    'card' => $stripe_card_detail->card_id
                                                                )
                                                            );
                            $charge_id = $charge->id; 
                            $insert_data = array(
                                'mem_id'        => $this->session->userdata('MEM_ID'),
                                'property_id'   => $properties_implode_ids,
                                'transaction_id'=> $charge_id,
                                'amount'        => $charge->amount/100,
                                'status'        => 'PAID',     
                                'object'        => 'Charge',    
                                'description'   => 'Property Charges',
                                'modified_date' => current_date(),
                                'created_date'  => current_date(),
                                'date'          => $charge->created,
                                'user_type'     => 'Landlord',
                                'payment_type' => 'Stripe',
                            );
                            $conditions = $params = array();
                            $conditions = $insert_data;
                            $params['table'] = 'tbl_payment';
                            $t_id = $this->common_model->insert_data($conditions, $params);
                            $message = 'You are successfully paid';


                            foreach ($properties_ids as $key => $value) {
                                $this->properties_model->frontPropertyOrderProcessComplete($value);
                                # code...
                                /********* STARTING: insert card for property recurring*****************************/
                                $conditions = $params = array();
                                $conditions['user_id']              = $login_id;
                                $conditions['property_id']          = $value;
                                $conditions['token']                = $stripe_card_detail->token;
                                $conditions['card_id']              = $stripe_card_detail->card_id;
                                $conditions['brand']                = $stripe_card_detail->brand;
                                $conditions['address_city']         = $stripe_card_detail->address_city;
                                $conditions['country']              = $stripe_card_detail->country;
                                $conditions['exp_month']            = $stripe_card_detail->exp_month;
                                $conditions['exp_year']             = $stripe_card_detail->exp_year;
                                $conditions['funding']              = $stripe_card_detail->funding;
                                $conditions['last_digits']          = $stripe_card_detail->last_digits;
                                $conditions['fingerprint']          = $stripe_card_detail->fingerprint;
                                $conditions['created']              = $stripe_card_detail->created;
                                $conditions['stripe_customer_id']   = $stripe_card_detail->stripe_customer_id;
                                $conditions['amount']               = $single_property_amount/100;
                                //if(isset($_REQUEST['recurring_'.$card_id]) && !empty($_REQUEST['recurring_'.$card_id])){
                                    $conditions['recurring']    = 1;
                                    $conditions['payment_type'] = 'Monthly';
                                    $conditions['days']         = date("d");
                                    $conditions['times']        = 1;                        
                                //}                    
                                $params['table'] = 'tbl_stripe_card_info';
                                $last_id = $this->common_model->insert_data($conditions, $params);   // Insert card
                                /********* ENDING: insert card for property recurring*****************************/
                            }

                            $success = True;
                            $redirect = site_url('dashboard');
                            $this->session->set_userdata('stripe_payment', $message);
                            $this->session->set_userdata('stripe_payment_prop_id', $properties_implode_ids);
                            $this->session->set_userdata('payment_received', 'Payment Received : '. amount_formated($amount/100));

                        } catch (Exception $e) {
                            $message = $e->getMessage();
                            $success = false;
                            $redirect = site_url('dashboard');
                            $this->session->set_userdata('stripe_payment', $message);
                            $this->session->set_userdata('stripe_payment_prop_id', $properties_implode_ids);               
                        }
                    }else{
                        foreach ($properties_ids as $key => $value) {
                            $this->properties_model->frontPropertyOrderProcessComplete($value);
                            /********* STARTING: insert card for property recurring*****************************/
                            $conditions = $params = array();
                            $conditions['user_id']              = $login_id;
                            $conditions['property_id']          = $value;
                            $conditions['token']                = $stripe_card_detail->token;
                            $conditions['card_id']              = $stripe_card_detail->card_id;
                            $conditions['brand']                = $stripe_card_detail->brand;
                            $conditions['address_city']         = $stripe_card_detail->address_city;
                            $conditions['country']              = $stripe_card_detail->country;
                            $conditions['exp_month']            = $stripe_card_detail->exp_month;
                            $conditions['exp_year']             = $stripe_card_detail->exp_year;
                            $conditions['funding']              = $stripe_card_detail->funding;
                            $conditions['last_digits']          = $stripe_card_detail->last_digits;
                            $conditions['fingerprint']          = $stripe_card_detail->fingerprint;
                            $conditions['created']              = $stripe_card_detail->created;
                            $conditions['stripe_customer_id']   = $stripe_card_detail->stripe_customer_id;
                            $conditions['amount']               = 0;
                            //if(isset($_REQUEST['recurring_'.$card_id]) && !empty($_REQUEST['recurring_'.$card_id])){
                                $conditions['recurring']    = 1;
                                $conditions['payment_type'] = 'Monthly';
                                $conditions['days']         = date("d");
                                $conditions['times']        = 1;                        
                            //}                    
                            $params['table'] = 'tbl_stripe_card_info';
                            $last_id = $this->common_model->insert_data($conditions, $params);   // Insert card
                            /********* ENDING: insert card for property recurring*****************************/
                        }
                        $success = True;
                        $message = 'Property subscribed successfully';
                        $redirect = site_url('dashboard');
                        $this->session->set_userdata('stripe_payment', 'Property subscribed successfully');
                        $this->session->set_userdata('stripe_payment_prop_id', $properties_implode_ids);
                        $this->session->set_userdata('payment_received', 'Free 60 Days trial account ');
                    }
                }else{
                    
                    $message = 'Customer Id not exist';
                    $success = false;
                    $redirect = site_url('dashboard');
                    $this->session->set_userdata('stripe_payment', $message);
                    $this->session->set_userdata('stripe_payment_prop_id', $properties_implode_ids);
                }


            }else{
                $token          = $_POST['stripeToken'];
                $email          = $_POST['emailAddress'];            

                $user_exist = $this->getStrpeUserinfo($this->session->userdata('MEM_ID'));
                /********************Start: Create customer account of landlord using stripe account********************************/
                if(empty($user_exist->stripe_customer_id)){   // if customer account not exist
                    try {
                
                         $customer = \Stripe\Customer::create(
                                                                array(
                                                                    'email' => $email,
                                                                    'description' => 'Landlord Customer Account'
                                                                   // 'source'  => $token
                                                                )
                                                            );
                        $customer_id = $customer->id;
                        $this->updateStripeUserinfo($this->session->userdata('MEM_ID'),$customer_id);
                    } catch (Exception $e) {
                        $message = $e->getMessage();
                        $success = false;
                        $redirect = site_url('dashboard');
                        $this->session->set_userdata('stripe_payment', $message);
                        $this->session->set_userdata('stripe_payment_prop_id', $properties_implode_ids);
                        if ($this->input->is_ajax_request()) {
                            $data['success'] = $success;
                            $data['message'] = $message;
                            $data['redirect_url'] = $redirect;
                            echo json_encode($data);
                            die;
                        }
                        //redirect(site_url('dashboard'));
                    }
                }else{                            // if customer account exist
                    $customer_id = $user_exist->stripe_customer_id;                
                }
                /********************End: Create customer account of landlord using stripe account*********************************/

                
                /******************** Start: Get card id by token ********************************/
                try {
                    $token_detail = \Stripe\Token::retrieve($token);                  
                    $login_id       = $this->session->userdata('MEM_ID');
                    $card_id        = $token_detail['card']->id;
                    $exp_month      = $token_detail['card']->exp_month;
                    $exp_year       = $token_detail['card']->exp_year;
                    $last_digits    = $token_detail['card']->last_digits;
                    $brand          = $token_detail['card']->brand;       
                    $fingerprint    = $token_detail['card']->fingerprint;
                                
                } catch (Exception $e) {
                    $message = $e->getMessage();
                    $success = false;
                    $redirect = site_url('dashboard');
                    $this->session->set_userdata('stripe_payment', $message);
                    $this->session->set_userdata('stripe_payment_prop_id', $properties_implode_ids);
                    if ($this->input->is_ajax_request()) {
                        $data['success'] = $success;
                        $data['message'] = $message;
                        $data['redirect_url'] = $redirect;
                        echo json_encode($data);
                        die;
                    }
                }
                /********************END: Get card id by token********************************/

                /********************START: Link card to the customer account ********************************/
                $conditions_c = $params_c = array();
                $params_c['table'] = 'tbl_stripe_card_info';
                $conditions_c['fingerprint'] = $fingerprint;
                $conditions_c['user_id'] = $login_id;
                $params_c['single_row'] = TRUE;
                $stripe_card_fingerprint_exist = $this->common_model->get_data($conditions_c, $params_c);

                if(empty($stripe_card_fingerprint_exist)){           // Check Card is alreday added or not for this user
                    try {
                        $customer = \Stripe\Customer::retrieve($customer_id);
                        $customer->sources->create(array("source" => $token));
                    }catch (Exception $e){
                        $message = $e->getMessage();
                        $success = false;
                        //$redirect = site_url('dashboard');
                        $this->session->set_userdata('stripe_payment', $message);
                        $this->session->set_userdata('stripe_payment_prop_id', $properties_implode_ids);
                        if ($this->input->is_ajax_request()) {
                            $data['success'] = $success;
                            $data['message'] = $message;
                            //$data['redirect_url'] = $redirect;
                            echo json_encode($data);
                            die;
                        }
                    }
                }else{
                    $card_id = $stripe_card_fingerprint_exist->card_id;
                }
                /********************END: Link card to the customer account ********************************/


                /********************************* Start: Charge from stripe account *********************************************/
                if( checkTrialTimePeriod($this->session->userdata('MEM_ID')) == false){
                    try {
                        $charge = \Stripe\Charge::create(
                                                            array(
                                                                'source'        => $card_id,
                                                                'amount'        => $amount,
                                                                'currency'      => 'usd',
                                                                'description'   => 'Property Charges',
                                                                'customer'      => $customer_id
                                                            )
                                                        );
                        $charge_id = $charge->id;

                        $insert_data = array(
                            'mem_id'        => $this->session->userdata('MEM_ID'),
                            'property_id'   => $properties_implode_ids,
                            'transaction_id'=> $charge_id,
                            'amount'        => $charge->amount/100,
                            'description'   => 'Paid services fee.',
                            'status'        => 'PAID',     
                            'object'        => 'Charge', 
                            'modified_date' => current_date(),
                            'date'  => $charge->created,
                            'created_date'  => current_date(),
                            'user_type' => 'Landlord',
                            'payment_type' => 'Stripe',
                        );
                        $conditions_payment = $params_payment = array();
                        $conditions_payment = $insert_data;
                        $params_payment['table'] = 'tbl_payment';
                        $this->common_model->insert_data($conditions_payment, $params_payment);

                        foreach ($properties_ids as $key => $value) {
                            $this->properties_model->frontPropertyOrderProcessComplete($value);

                            $conditions = $params = array();
                            $conditions['user_id']          = $login_id;
                            $conditions['property_id']      = $value;
                            $conditions['token']            = $token;
                            $conditions['card_id']          = $token_detail['card']->id;
                            $conditions['brand']            =  $token_detail['card']->brand;
                            $conditions['address_city']     =  $token_detail['card']->address_city;
                            $conditions['country']          =  $token_detail['card']->country;
                            $conditions['exp_month']        =  $token_detail['card']->exp_month;
                            $conditions['exp_year']         =  $token_detail['card']->exp_year;
                            $conditions['funding']          =  $token_detail['card']->funding;
                            $conditions['last_digits']      =  $token_detail['card']->last4;
                            $conditions['fingerprint']      =  $token_detail['card']->fingerprint;
                            $conditions['created']          = $token_detail->created;
                            $conditions['amount']           = 0;
                            //if(isset($_REQUEST['recurring_0']) && !empty($_REQUEST['recurring_0'])){
                                $conditions['recurring']    = 1;
                                $conditions['payment_type'] = 'Monthly';
                                $conditions['days']         = date("d");
                                $conditions['times']        = 1;                        
                            //}  
                            $params['table'] = 'tbl_stripe_card_info';
                            $conditions['stripe_customer_id'] = $customer_id;   
                            $last_id = $this->common_model->insert_data($conditions, $params);   // Insert new card
                        }                    
                        $message = 'Successfully charged '.amount_formated($amount)/100;

                        $success = true;
                        $redirect = site_url('dashboard');
                        $this->session->set_userdata('stripe_payment', $message);
                        $this->session->set_userdata('stripe_payment_prop_id', $properties_implode_ids);
                        $this->session->set_userdata('payment_received', 'Payment Received : '. amount_formated($amount/100));
                    }catch(Exception $e) {
                        $message = $e->getMessage();
                        $success = false;
                        $redirect = site_url('checkout_process');
                        $this->session->set_userdata('stripe_payment', $message);
                        $this->session->set_userdata('stripe_payment_prop_id', $properties_implode_ids);
                    }
                }else{
                    foreach ($properties_ids as $key => $value) {
                        $this->properties_model->frontPropertyOrderProcessComplete($value);
                        $conditions = $params = array();
                        $conditions['user_id']          = $login_id;
                        $conditions['property_id']      = $value;
                        $conditions['token']            = $token;
                        $conditions['card_id']          = $token_detail['card']->id;
                        $conditions['brand']            =  $token_detail['card']->brand;
                        $conditions['address_city']     =  $token_detail['card']->address_city;
                        $conditions['country']          =  $token_detail['card']->country;
                        $conditions['exp_month']        =  $token_detail['card']->exp_month;
                        $conditions['exp_year']         =  $token_detail['card']->exp_year;
                        $conditions['funding']          =  $token_detail['card']->funding;
                        $conditions['last_digits']      =  $token_detail['card']->last4;
                        $conditions['fingerprint']      =  $token_detail['card']->fingerprint;
                        $conditions['created']          = $token_detail->created;
                        $conditions['amount']           = $single_property_amount/100;
                        //if(isset($_REQUEST['recurring_0']) && !empty($_REQUEST['recurring_0'])){
                            $conditions['recurring']    = 1;
                            $conditions['payment_type'] = 'Monthly';
                            $conditions['days']         = date("d");
                            $conditions['times']        = 1;                        
                        //}  
                        $params['table'] = 'tbl_stripe_card_info';
                        $conditions['stripe_customer_id'] = $customer_id;   
                        $last_id = $this->common_model->insert_data($conditions, $params);   // Insert new card
                    }
                    $message = 'Successfully subscribed';    
                    $success = true;
                    $redirect = site_url('dashboard');
                    $this->session->set_userdata('stripe_payment', $message);
                    $this->session->set_userdata('stripe_payment_prop_id', $properties_implode_ids);
                    $this->session->set_userdata('payment_received', 'Successfully subscribed');                    
                }
                /********************************* End: Charge from stripe account ************************************************/

                
            

        }
        //echo $message.$this->calculatePropertyChargesAmount(); exit;
        if ($this->input->is_ajax_request()) {
            $data['success'] = $success;
            $data['message'] = $message;
            $data['redirect_url'] = $redirect;
            echo json_encode($data);
            die;
        }
    }

        /**
     * Method Name : updateCard
     * Author Name : Lakhvinder Singh
     * Description : Update charge
     */
    function updateCard(){
        
        require_once(FCPATH.'application/libraries/vendor/autoload.php');
        $stripe = array(
            "secret_key"      => STRIPE_SECRET_KEY,
            "publishable_key" => STRIPE_PULBLISHABLE_KEY
        );
        $property_card_id             = $_REQUEST['property_card_id'];

        $login_id = $this->session->userdata('MEM_ID');
        \Stripe\Stripe::setApiKey($stripe['secret_key']);       
        $token          = $_POST['stripeToken'];
        $email          = $_POST['emailAddress']; 
        $user_exist = $this->getUserInfo($login_id);           
        $customer_id = $user_exist->stripe_customer_id;                
        /******************** Start: Get card id by token ********************************/
        try {
            $token_detail = \Stripe\Token::retrieve($token);                  
            $login_id       = $this->session->userdata('MEM_ID');
            $card_id        = $token_detail['card']->id;
            $exp_month      = $token_detail['card']->exp_month;
            $exp_year       = $token_detail['card']->exp_year;
            $last_digits    = $token_detail['card']->last_digits;
            $brand          = $token_detail['card']->brand;       
            $fingerprint    = $token_detail['card']->fingerprint;
        } catch (Exception $e) {
            $message = $e->getMessage();
            $success = false;
            $redirect = site_url('account/billing');
            if ($this->input->is_ajax_request()) {
                $data['success'] = $success;
                $data['message'] = $message;
                $data['redirect_url'] = $redirect;
                echo json_encode($data);
                die;
            }
        }
        /********************END: Get card id by token********************************/

        /********************START: Link card to the customer account ********************************/
        $conditions_c = $params_c = array();
        $params_c['table'] = 'tbl_stripe_card_info';
        $conditions_c['fingerprint'] = $fingerprint;
        $conditions_c['user_id'] = $login_id;
        $params_c['single_row'] = TRUE;
        $stripe_card_fingerprint_exist = $this->common_model->get_data($conditions_c, $params_c);
        if(empty($stripe_card_fingerprint_exist)){           // Check Card is alreday added or not for this user
            try {
                $customer = \Stripe\Customer::retrieve($customer_id);
                $customer->sources->create(array("source" => $token));
            }catch (Exception $e){
                $message = $e->getMessage();
                $success = false;
                if ($this->input->is_ajax_request()) {
                    $data['success'] = $success;
                    $data['message'] = $message;
                    //$data['redirect_url'] = $redirect;
                    echo json_encode($data);
                    die;
                }
            }
        }else{
            $card_id = $stripe_card_fingerprint_exist->card_id;
        }
        /********************END: Link card to the customer account ********************************/
        
        $conditions_c = $params_c = array();
        $params_c['table'] = 'tbl_stripe_card_info';
        $conditions_c['card_id'] = $card_id;
        $conditions_c['user_id'] = $login_id;
        $card_list = $this->common_model->get_data($conditions_c, $params_c);
        foreach ($card_list as $key => $value) {
            $conditions = $params = array();
            $conditions['where']['id']          = $value->id;
            $conditions['value']['card_id']          = $token_detail['card']->id;
            $conditions['value']['token']            = $token;
            $conditions['value']['brand']            =  $token_detail['card']->brand;
            $conditions['value']['address_city']     =  $token_detail['card']->address_city;
            $conditions['value']['country']          =  $token_detail['card']->country;
            $conditions['value']['exp_month']        =  $token_detail['card']->exp_month;
            $conditions['value']['exp_year']         =  $token_detail['card']->exp_year;
            $conditions['value']['funding']          =  $token_detail['card']->funding;
            $conditions['value']['last_digits']      =  $token_detail['card']->last4;
            $conditions['value']['fingerprint']      =  $token_detail['card']->fingerprint;
            $params['table'] = 'tbl_stripe_card_info';
            $this->common_model->update($conditions, $params);   // Insert new card
        }
        $message = 'Successfully updated';    
        $success = true;
        $redirect = site_url('account/billing');
        
        //echo $message.$this->calculatePropertyChargesAmount(); exit;
        if ($this->input->is_ajax_request()) {
            $data['success'] = $success;
            $data['message'] = $message;
            $data['redirect_url'] = $redirect;
            echo json_encode($data);
            die;
        }
    }

    public function welcome_text_send_to_tenant($prop_id) {

        if(!empty($prop_id)){
            $tenants = $this->properties_model->getAllTenantsByProperty($prop_id);
            $message_data = '';
            foreach ($tenants as $key => $value) {
                $failure = FALSE;
                $conditions = $params = array();
                $params['complex'] = true;

                $conditions['tables'] = array(
                    'lease' => 'tbl_properties_lease_detail',
                    'tenant' => 'tbl_members',
                );
                $conditions['table'] = 'lease';
                $conditions['on']['tenant'] = array(
                    'sign' => '=',
                    'column' => 'mem_id',
                    'alias_column' => 'tenant_id',
                    'alias_other' => 'lease',
                );
                $conditions['where']['mem_id']['alias'] = 'tenant';
                $conditions['where']['mem_id']['value'] = $value->tenant_id;
                $params['single_row'] = true;
                $params['fields'] = array('tenant.*');
                $details = $this->common_model->get_data($conditions, $params);
                $this->load->helper('twilio_helper');  // references library/twilio_helper.php
                $service = get_twilio_service();
                try {
                    $mnumber = $details->mobile_no;
                    $number = "+1" . preg_replace("/[^0-9]/", "", $mnumber);
                    $playstore = PLAYSTORE;
                    $appstore = APPSTORE;
                    $playstore_img = $this->config->item("uploads").'icons/google-play.png';
                    $appstore_img = $this->config->item("uploads").'icons/app_store.png';
                    $playstore_img_html = '<a href="'.$playstore.'"><img src="'.$playstore_img.'"></a>';
                    $appstore_img_html = '<a href="'.$appstore.'"><img src="'.$appstore_img.'"></a>';
                    
                    //$msg = "Welcome Home! Your landlord has enrolled you in TenantTag. *Download your app Now. $appstore --iOS or $playstore --Android* ";
                    $msg = "Welcome Home! Your \n landlord has enrolled you in \n TenantTag. Download your app for iOS or Android. \n \n iOS: $appstore \n \n Android: $playstore  \n \n Username:" . $details->slug . "  \n \n Password: " . $details->tpass;
                    //$service->account->sms_messages->create("+19046743077", "$number", "$msg", array());
                     $service->account->messages->create(array(
                                'To' => $number,
                                'From' => "+19046743077",
                                'Body' => $msg));           
                    //echo 'success';
                    //exit;
                } catch (Exception $e) {
                    $file = 'sms_error_logs.txt';
                    if (file_exists($file)) {
                        $current = file_get_contents($file);
                        $current .= $e->getMessage() . "\n";
                        file_put_contents($file, $current);
                        //echo $e->getMessage();
                    }
                    //exit;
                }            
            }
        }
    }


    function getStrpeUserinfo($user_id){
        $conditions = $params = array();
        $params['table'] = 'tbl_members';
        $conditions['mem_id'] = $user_id;
        $params['single_row'] = true;
        $info = $this->common_model->get_data($conditions, $params);
        return $info;
    }

    function updateStripeUserinfo($user_id,$stripe_customer_id){
         $insert_data = array(
            'stripe_customer_id'=> $stripe_customer_id,
        );
        $conditions = $params = array();
        $conditions['value'] = $insert_data;
        $conditions['where']['mem_id'] = $user_id;
        $params['table'] = 'tbl_members';
        $this->common_model->update($conditions, $params);
    }

    /*function changeTservicesStatus($prop_id, $field, $value) {
        $propertyDetail = $this->properties_model->getPropertyById($prop_id);
        $redirect_to = '';
        if($propertyDetail->step_completed == 3){
            //$payment_info = $this->checkTservicesPaymentInfo($prop_id);
            $conditions = $params = array();
            $params['table'] = 'tbl_payment';
            $conditions['property_id'] = $prop_id;
            $info = $this->common_model->get_data($conditions, $params);

            if(!empty($info)){
                $this->properties_model->changeTservicesStatus($prop_id, $field, $value);
            }else{
                $redirect_to = site_url('checkout_process').'?propid='.$prop_id.'&field='.$field;
            }
        }else{
            $new_step = $propertyDetail->step_completed+1;
            if($propertyDetail->step_completed==2){
                $redirect_to = site_url('add-property-step'.$new_step).'?propid='.$prop_id;
            }else{
                $redirect_to = site_url('update-property-step'.$new_step).'?propid='.$prop_id;
            }
        }
        if ($this->input->is_ajax_request()) {
            //$success_message  =   'selected record deleted successfully';
            $data['success'] = true;
            $data['redirect_to'] = $redirect_to;
            $data['scrollToElement'] = true;
            echo json_encode($data);
            die;
        } 
    }*/

    function changeTservicesStatus($prop_id, $field, $value) {
        $tmaintenance_message = '';
        if($value == 'not_subscribe'){            
            $redirect_to = site_url('checkout_process');                
        }else{
            $propertyDetail = $this->properties_model->getPropertyById($prop_id);
            $redirect_to = '';
            if($propertyDetail->step_completed >= 3){
                if($field == 'tmaintenance'){
                    $conditions = $params = array();
                    $conditions['property_id'] = $prop_id;
                    $conditions['status'] = 'Active';
                    $params['table'] = 'tbl_vendors';
                    $vendor_info = $this->common_model->get_data($conditions,$params);
                    if(count($vendor_info) > 0){
                        $this->properties_model->changeTservicesStatus($prop_id, $field, $value);            
                    }else{
                        $redirect_to = site_url('vendors'); 
                        $tmaintenance_message = 'vendor_not_exist';
                    }

                }else{
                    $this->properties_model->changeTservicesStatus($prop_id, $field, $value);            
                }
            }else{
                $new_step = $propertyDetail->step_completed+1;
                if($propertyDetail->step_completed==2){
                    $redirect_to = site_url('add-property-step'.$new_step).'?propid='.$prop_id;
                }else{
                    $redirect_to = site_url('update-property-step'.$new_step).'?propid='.$prop_id;
                }
            }
                
        }
        if ($this->input->is_ajax_request()) {
            //$success_message  =   'selected record deleted successfully';
            $data['success'] = true;
            $data['tmaintenance_message'] = $tmaintenance_message;
            $data['redirect_to'] = $redirect_to;
            $data['scrollToElement'] = true;
            echo json_encode($data);
            die;
        } 
    }



    /*function checkTservicesPaymentInfo($prop_id){        
        $conditions = $params = array();
        $params['table'] = 'tbl_payment';
        $conditions['property_id'] = $prop_id;
        //$params['single_row'] = true;
        $info = $this->common_model->get_data($conditions, $params);
        $data = array();
        foreach ($info as $key => $value) {
            //$data[] = $value->tservices;
            if($value->tmaintenance){
                $data[] = 'tmaintenance';
            }
            if($value->tmessaging){
                $data[] = 'tmessaging';
            }
            if($value->tpay){
                $data[] = 'tpay';
            }
        }
        //echo "<pre>"; print_R($data); exit;
        return $data;
    }*/

    function changeShopingCartTservicesStatus($prop_id, $field, $value) {
        $this->properties_model->changeShopingCartTservicesStatus($prop_id, $field, $value);
        if ($this->input->is_ajax_request()) {
            //$success_message  =   'selected record deleted successfully';
            $data['success'] = true;
            //$data['success_message'] = $success_message;
            $data['scrollToElement'] = true;
            echo json_encode($data);
            die;
        }
    }

    function update_property() {
        $output['selected_tab'] = 'properties';
        $output['title'] = 'Properties';

        /*         * ******** Start loading model as required in function ***** */
        $this->load->model('amenties_model');
        $this->load->model('services_model');
        $this->load->model('region_model');
        $this->load->model('member_model');
        /*         * ******** End loading model as required in function ******* */
        /*         * ******** Get All Amenties ******************************** */
        $output['allAmenties'] = $allAmenties = $this->amenties_model->frontGetAllAmenties();

        if (!empty($_POST)) {

            $failure = 0;
            //echo"<pre>";print_r($_POST);die;
            $this->form_validation->set_message('required', '%s is required');

            $this->form_validation->set_rules('property_id', 'Property', 'trim|required');
            //~ $this->form_validation->set_rules('garbage_pick_day', 'Garbage pick day', 'trim|required');
            //~ $this->form_validation->set_rules('recycle_pick_day', 'Recycle pick day', 'trim|required');
            //~ $this->form_validation->set_rules('yard_waste_pick_day', 'Yard Waste pick day', 'trim|required');


            if ($this->form_validation->run()) {
                $config['upload_path'] = './assets/uploads/community_documents/';
                $config['allowed_types'] = 'jpeg|jpg|png|txt|pdf|doc|docx';
                $config['encrypt_name'] = TRUE;
                if (!empty($_FILES['comm_docs']['name'])) {
                    if (!$this->upload->do_upload('comm_docs')) {
                        $error = $this->upload->display_errors();
                        $error_message.=$error;
                        $failure = TRUE;
                    } else {
                        $comm_data = array('upload_data' => $this->upload->data());
                    }
                }

                if (!$failure) {
                    $file = '';
                    if (isset($comm_data['upload_data']['file_name'])) {
                        $file = $comm_data['upload_data']['file_name'];
                    }
                    $this->properties_model->updateProperty($allAmenties, $file);
                    $success_message = '<p>Property Information Updated successfully.</p>';
                }
            } else {
                $error_message = validation_errors();
                $failure = true;
            }
            if ($this->input->is_ajax_request()) {
                if ($failure) {
                    $data['success'] = false;
                    $data['error_message'] = $error_message;
                } else {
                    $data['success'] = true;
                    $data['resetForm'] = true;
                    $data['success_message'] = $success_message;
                    $data['url'] = base_url() . 'my-properties/#tabs-' . $this->input->post('property_id');
                }
                //$data['selfReload'] = true;
                // $data['scrollToElement'] = true;
                echo json_encode($data);
                die;
            }
        }

        //~ $this->load->view($this->config->item('defaultfolder').'/header',$output);
        //~ $this->load->view($this->config->item('defaultfolder').'/add_property_step1');
        //~ $this->load->view($this->config->item('defaultfolder').'/footer');
    }

    function updateStep1() {
        $error_message = '';
        $step_completed = $this->common_model->getHigestCompletedStepProeprty();
        $allProperties = $this->properties_model->frontGetCountAllPropertiesByLandlordId($this->session->userdata('MEM_ID'));
        if (!$allProperties || ($allProperties == 1 && $step_completed < 3)) {
            $output['selected_tab'] = 'first-property';
        } else {
            $output['selected_tab'] = 'properties';
        }

        $output['title'] = 'Properties';

        /*         * ******** Start loading model as required in function ***** */
        $this->load->model('amenties_model');
        $this->load->model('services_model');
        $this->load->model('region_model');
        $this->load->model('member_model');
        /*         * ******** End loading model as required in function ******* */

        /*         * ******** Get All Amenties ******************************** */
        $output['allAmenties'] = $allAmenties = $this->amenties_model->frontGetAllAmenties();
        /*         * ******** Get All Services ******************************** */
        $output['allServices'] = $this->services_model->frontGetAllServices();
        /*         * ******** Get all regions/states ************************* */
        $country_id = $this->common_model->getSingleFieldFromAnyTable('country_id', 'mem_id', $this->session->userdata('MEM_ID'), 'tbl_members');
        $output['allRegion'] = $this->region_model->getAllRegionsByCountryId($country_id);
        /*         * ******** Fetching Property detail with property services ************************** */
        $output['propertyDetail'] = $propertyDetail = $this->properties_model->getPropertyById($this->session->userdata('PROPERTY_ID'));
        $output['property_services'] = $this->properties_model->getServicesByPropertyId($this->session->userdata('PROPERTY_ID'));
        $output['property_photo'] = $this->properties_model->getPropertyPhotos($this->session->userdata('PROPERTY_ID'));
        if (!empty($_POST)) {
            $failure = 0;
            $this->form_validation->set_message('required', '%s is required');
            $this->form_validation->set_rules('property_type', 'Property Type', 'trim|required');
            $this->form_validation->set_rules('address1', 'Address 1', 'trim|required');
            $this->form_validation->set_rules('state', 'State', 'trim|required');
            /*$this->form_validation->set_rules('zip', 'Zip', 'trim|required|is_numeric|greater_than[0]');*/
            $this->form_validation->set_rules('is_community', 'Property in Community', 'trim|required');
            if ($this->input->post('is_community') == 'Yes') {
                $this->form_validation->set_rules('community', 'Community', 'trim|required');
                // $this->form_validation->set_rules('gate_code', 'Gate code', 'trim|required');
                //  $this->form_validation->set_rules('po_box', 'Unit PO Box', 'trim|required');
                //   $this->form_validation->set_rules('community_company', 'Community Managing Company', 'trim|required');
                // $this->form_validation->set_rules('company_number', 'Community Managing Company Contact Number', 'trim|required');
            }
            $this->form_validation->set_rules('garbage_pick_day', 'Garbage pick day', 'trim|required');
            $this->form_validation->set_rules('recycle_pick_day', 'Recycle pick day', 'trim|required');
            $this->form_validation->set_rules('yard_waste_pick_day', 'Yard Waste pick day', 'trim|required');
            $this->form_validation->set_rules('safety_equipment1', 'Smoke detectors', 'trim|required');
            $this->form_validation->set_rules('safety_equipment2', 'Fire Extinguishors', 'trim|required');
            $this->form_validation->set_rules('heat_type', 'Heat Type', 'trim|required');
            if ($this->input->post('heat_type') != 'N/A') {
                $this->form_validation->set_rules('heat_filter_size', 'Heat Filter size', 'trim|required');
            }
            $this->form_validation->set_rules('ac_type', 'AC Type', 'trim|required');
            if ($this->input->post('ac_type') != 'N/A') {
                $this->form_validation->set_rules('ac_filter_size', 'AC Filter size', 'trim|required');
            }

            $this->form_validation->set_rules('ac_type', 'AC Type', 'trim|required');

            $this->form_validation->set_rules('pest_control_home', 'Pest Control home', 'trim|required');
            if ($this->input->post('pest_control_home') == 'yes') {
                $this->form_validation->set_rules('pest_service_provider', 'Pest Control home service provider', 'trim|required');
                $this->form_validation->set_rules('pest_service_provider_number', 'Pest Control home service provider number', 'trim|required');
            }
            $this->form_validation->set_rules('yard', 'Yard', 'trim|required');
            if ($this->input->post('yard_service_provided') == 'yes') {
                $this->form_validation->set_rules('yard_service_provider', 'Yard service provider', 'trim|required');
                $this->form_validation->set_rules('yard_service_provider_number', 'Yard service provider number', 'trim|required');
            }

            if (empty($_FILES['profile_pic']['name'])) {
                //$this->form_validation->set_rules('profile_pic', 'Profile Image', 'required');
            }

            if ($this->form_validation->run()) {
                $config['upload_path'] = './assets/uploads/community_documents/';
                $config['allowed_types'] = 'jpeg|jpg|png|txt|pdf|doc|docx';
                $config['encrypt_name'] = TRUE;
                $this->load->library('upload', $config);
                if (!empty($_FILES['comm_docs']['name'])) {
                    if (!$this->upload->do_upload('comm_docs')) {
                        $error = $this->upload->display_errors();
                        $error_message.=$error;
                        $failure = TRUE;
                    } else {
                        $comm_data = array('upload_data' => $this->upload->data());
                    }
                }

                $config['upload_path'] = './assets/uploads/member/';
                $config['allowed_types'] = 'jpeg|jpg|png';
                $config['encrypt_name'] = TRUE;
                $this->load->library('upload', $config);
                if (isset($_FILES['profile_pic'])) {
                    for ($i=0; $i < count($_FILES['profile_pic']); $i++) { 

                            //$this->load->library('upload');
                        if (!empty($_FILES['profile_pic']['name'][$i])) {

                            $_FILES['profile_pi']['name'] = $_FILES['profile_pic']['name'][$i];
                            $_FILES['profile_pi']['type'] = $_FILES['profile_pic']['type'][$i];
                            $_FILES['profile_pi']['tmp_name'] = $_FILES['profile_pic']['tmp_name'][$i];
                            $_FILES['profile_pi']['error'] = $_FILES['profile_pic']['error'][$i];
                            $_FILES['profile_pi']['size'] = $_FILES['profile_pic']['size'][$i];
                            $prop_id = $this->session->userdata('PROPERTY_ID');
                            $this->upload->initialize($this->set_upload_options($prop_id, PROPERTY_PHOTOS, 'property'));
                            if (!$this->upload->do_upload('profile_pi')) {
                                $error = $this->upload->display_errors();
                                $error_message.=$error;
                                $failure = TRUE;
                            } else {
                            	$gallery_add = array();
                                $data = $this->upload->data();
                                $this->thumb($data);
                                $thumb = $data['raw_name'] . '_thumb' . $data['file_ext'];
                                $orig_file = $data['raw_name'] . $data['file_ext'];
                                $gallery_add[] = array(
                                    'property_id' => $prop_id,
                                    'image' => PROPERTY_PHOTOS . "/property_" . $prop_id . "/" . $orig_file,
                                    'thumb' => PROPERTY_PHOTOS . "/property_" . $prop_id . "/" . $thumb,
                                    'tenant_id' => 0, //$this->session->userdata('MEM_ID'),
                                    'status' => 'Active',
                                    'is_main_photo' => 1,
                                    'add_date' => current_date(),
                                    'ip' => ''
                                );
                                if (isset($gallery_add) && $gallery_add != '') {
                                    $conditions = $params = array();
                                    $params['table'] = 'tbl_property_photos';
                                    $conditions['property_id'] = $prop_id;
                                    $conditions['is_main_photo'] = 1;
                                    $params['single_row'] = TRUE;
                                    $photo = $this->common_model->get_data($conditions, $params);
                                   /* if (!empty($photo)) {
                                        if (!empty($photo->thumb)) {
                                            // unlink(FCPATH . $photo->thumb);
                                        }
                                        unlink(FCPATH . $photo->image);

                                    }*/
                                    if(isset($photo->id)){
    	                                $conditions = $params = array();
    	                                $conditions['where']['id'] = $photo->id;
    	                                $conditions['value']['is_main_photo'] = 0;
    	                                $params['table'] = 'tbl_property_photos';
    	                                $this->common_model->update($conditions, $params);
                                    	
                                    }
                                    $conditions = $params = array();
                                    $params['batch_mode'] = true;
                                    $conditions = $gallery_add;
                                    $params['table'] = 'tbl_property_photos';
                                    $insert_id = $this->common_model->insert_data($conditions, $params);
                                }
                                //$this->member_model->uploadProfilePic($this->mem_id, $data['upload_data']['file_name']);
                            }
                        }
                    }
                }


                if (!$failure) {
                    $file = '';
                    if (isset($comm_data['upload_data']['file_name']) && !empty($comm_data['upload_data']['file_name'])) {
                        $file = $comm_data['upload_data']['file_name'];
                    }
                    $this->properties_model->frontUpdatePropertyStep1($allAmenties, $file);
                    $success_message = '<p>Property Information Updated successfully.</p>';
                }
            } else {
                $error_message = validation_errors();
                $failure = true;
            }
            if ($this->input->is_ajax_request()) {
                if ($failure) {
                    $data['success'] = false;
                    $data['error_message'] = $error_message;
                } else {
                    $data['success'] = true;
                    $data['resetForm'] = true;
                    $data['success_message'] = $success_message;
                    $data['url'] = site_url('update-property-step2');
                }
                $data['scrollToElement'] = true;
                echo json_encode($data);
                die;
            }
        }
        //echo '<pre>';print_r($output);die;
        $this->load->view($this->config->item('defaultfolder') . '/header', $output);
        $this->load->view($this->config->item('defaultfolder') . '/update_property_step1');
        $this->load->view($this->config->item('defaultfolder') . '/add_property_js');
        $this->load->view($this->config->item('defaultfolder') . '/footer');
    }

    function updateStep2() {
        $output['title'] = 'Properties';
        $step_completed = $this->common_model->getHigestCompletedStepProeprty();
        $allProperties = $this->properties_model->frontGetCountAllPropertiesByLandlordId($this->session->userdata('MEM_ID'));
        if (!$allProperties || ($allProperties == 1 && $step_completed < 3)) {
            $output['selected_tab'] = 'first-property';
        } else {
            $output['selected_tab'] = 'properties';
        }

        if (isset($_GET['propid']) && !empty($_GET['propid'])) {
            $this->session->set_userdata('PROPERTY_ID', $_GET['propid']);
        } else if (!$this->session->userdata('PROPERTY_ID')) {
            redirect(site_url('add-property'));
        }

        /*    ********* Start loading model as required in function ***** */
        $this->load->model('region_model');
        $this->load->model('member_model');
        /*         ********* End loading model as required in function ******* */

        /*         ********* Fetching mailing address for rent payment ************** */
        $output['memberRecord'] = $this->member_model->getMemberRecordById($this->session->userdata('MEM_ID'));
        /*         ********* Get Property Detail By property Id ************** */
        $output['propertyDetail'] = $propertyDetail = $this->properties_model->getPropertyById($this->session->userdata('PROPERTY_ID'));
        $tenantDetails = $this->properties_model->getAllTenantsByProperty($this->session->userdata('PROPERTY_ID'));
              //echo "<pre>"; print_R($tenantDetails);
        foreach ($tenantDetails as $value) {
            $value->tenantDetail = $this->member_model->getMemberRecordByAnyField($value->tenant_id, 'mem_id');
            $value->leaseDoc = $this->getLatestLease($value->tenant_id);
        }
        $output['tenantDetails'] = $tenantDetails;
              //echo $tenantDetails[0]->leaseDoc->lease_doc."<pre>"; print_R($tenantDetails); exit;

        if (!empty($_POST)) {
            // echo '<pre>';print_r($_FILES);die;
            //  echo '<pre>';print_r($_POST);die;
            $failure = 0;
            $this->form_validation->set_message('required', '%s is required');
            if ($this->input->post('address_1')) {
                $this->form_validation->set_rules('address_1', 'Mailing address 1 ', 'trim|required');
                // $this->form_validation->set_rules('address_2', 'Mailing address 2 ', 'trim|required');
                //$this->form_validation->set_rules('unit', 'Unit', 'trim|is_numeric');
                $this->form_validation->set_rules('city', 'City', 'trim|required');
                $this->form_validation->set_rules('state', 'State', 'trim|required');
                $this->form_validation->set_rules('zip', 'Zip', 'trim|required|is_numeric|greater_than[0]');
            }
            $this->form_validation->set_rules('is_vacant', 'Is Vacant ', 'trim');
            if ($this->input->post('is_vacant') != 'Yes') {
                $arr = $this->input->post('first_name');
                foreach ($arr as $i => $ar) {
                    $file_index = $i;
                    //if(empty($tenantDetails[$i]->leaseDoc) && empty($uploaded_lease_doc[$i]){
                    if(isset($_REQUEST['uploaded_lease_doc'][$file_index]) && empty($_REQUEST['uploaded_lease_doc'][$file_index])){
                        if (empty($_FILES['upload_lease']['name'][$file_index])){
                           //$this->form_validation->set_rules("upload_lease[$file_index]", ' Tenant '.$file_index.' lease document ', 'required');
                        }                        
                    }
                  // echo $this->input->post("first_name[$i]");
                  /*$this->form_validation->set_rules("first_name[$i]", 'First Name ', 'trim|required');
                  // $this->form_validation->set_rules('last_name[$i]', 'Last Name ', 'trim|required');
                  $this->form_validation->set_rules("dob[$i]", 'DOB ', 'trim|required');
                  $this->form_validation->set_rules("email[$i]", 'Email ', 'trim|required');
                  //$this->form_validation->set_rules("password_'.$i, 'Password ', 'trim|required');
                  $this->form_validation->set_rules("mobile[$i]", 'Mobile ', 'trim|required');
                  $this->form_validation->set_rules("preferred_language[$i]", 'Preferred language ', 'trim|required');
                  $this->form_validation->set_rules("lease_start_date[$i]", 'Lease Start Date ', 'trim|required');
                  $this->form_validation->set_rules("lease_end_date[$i]", 'Lease End Date ', 'trim|required');
                  $this->form_validation->set_rules("rent_amount[$i]", 'Rent Amount ', 'trim|required');
                  $this->form_validation->set_rules("due_date[$i]", 'Due Date ', 'trim|required');
                  $this->form_validation->set_rules("late_fee[$i]", 'Late Fee ', 'trim|required|is_numeric|greater_than[0]');
                  $this->form_validation->set_rules("late_fee_type[$i]", 'Late Fee Type', 'trim|required');*/

                } //die();
            } 
            $error_message = '';
            $data = array();
            if ($this->form_validation->run()) {
                //  $count = count($this->input->post('first_name'));
                $config['upload_path'] = './assets/uploads/lease_documents/';
                $config['allowed_types'] = 'jpeg|jpg|png|txt|pdf|doc';
                $config['encrypt_name'] = TRUE;
                $this->load->library('upload', $config);
                $files = $_FILES;
                //if (isset($_FILES['upload_lease']['name']) && !empty($_FILES['upload_lease']['name'])) {
                    //$count = count($_FILES['upload_lease']['name']);
                    //$cnt = 0;
                $arr = $this->input->post('first_name');
                foreach ($arr as $k => $ar) {
                    if(isset($files['upload_lease']['name'][$k])){
                        $_FILES['upload_lease']['name'] = $files['upload_lease']['name'][$k];
                        $_FILES['upload_lease']['type'] = $files['upload_lease']['type'][$k];
                        $_FILES['upload_lease']['tmp_name'] = $files['upload_lease']['tmp_name'][$k];
                        $_FILES['upload_lease']['error'] = $files['upload_lease']['error'][$k];
                        $_FILES['upload_lease']['size'] = $files['upload_lease']['size'][$k];
                        if ($files['upload_lease']['name'][$k] != '') {
                            if ($this->upload->do_upload('upload_lease') == False) {
                                $error = $this->upload->display_errors();
                                $error_message.=$error;
                                $failure = TRUE;
                            } else {
                                $data[$k] = array('upload_data' => $this->upload->data());
                               // $cnt++;
                            }
                        }
                    }else{
                        if(isset($_REQUEST['uploaded_lease_doc'][$k])){
                            $data[$k] = array('upload_data' => array('uploaded_file' => $_REQUEST['uploaded_lease_doc'][$k]));
                        }
                        //$cnt++;
                    }
                }
               // } 
                
                //echo "<pre>"; print_R($files);  print_R($data);  exit;
                if (!$failure) {
                    $this->member_model->frontAddMAilingAddress();
                    $this->properties_model->frontUpdateTenantsToProperty($this->session->userdata('PROPERTY_ID'), $data);
                    $success_message = '<p>Tenants Updated to Property successfully.</p>';
                }
            } else {
                $error_message = validation_errors();
                $failure = true;
            }


            if ($this->input->is_ajax_request()) {
                if ($failure) {
                    $data['success'] = false;
                    $data['error_message'] = $error_message;
                } else {
                    $data['success'] = true;
                    $data['resetForm'] = true;
                    $data['success_message'] = $success_message;
                    $data['url'] = site_url('add-property-step3');
                }
                $data['scrollToElement'] = true;
                echo json_encode($data);
                die;
            }
        }
        $output['allRegion'] = $this->region_model->getAllRegionsByCountryId(223);
        $this->load->view($this->config->item('defaultfolder') . '/header', $output);
        $this->load->view($this->config->item('defaultfolder') . '/update_property_step2');
        $this->load->view($this->config->item('defaultfolder') . '/add_property_js');
        $this->load->view($this->config->item('defaultfolder') . '/footer');
    }

    public function getLatestLease($tenantId) {
        $conditions = $params = array();
        $params['table'] = 'tbl_lease_docs';
        $conditions['tenant_id'] = $tenantId;
        $conditions['type'] = '0';
        $params['single_row'] = TRUE;
        $params['limit'] = 1;
        $params['fields'] = array('lease_doc');
        $data = $this->common_model->get_data($conditions, $params);
        return $data;
    }

    public function get_state() {
        if ($this->input->post('countryId')) {
            $states = $this->region_model->getAllRegionsByCountryId($this->input->post('countryId'));
            echo json_encode($states);
            exit;
        }
    }

    function update_address() {

        if (!empty($_POST)) {
            $failure = 0;
            $this->form_validation->set_message('required', '%s is required');
            $this->form_validation->set_rules('property_type', 'Property Type', 'trim|required');
            $this->form_validation->set_rules('address1', 'Address 1', 'trim|required');
            $this->form_validation->set_rules('state', 'State', 'trim|required');
            $this->form_validation->set_rules('zip', 'Zip', 'trim|required|is_numeric|greater_than[0]');
            $this->form_validation->set_rules('is_community', 'Property in Community', 'trim|required');
            if ($this->input->post('is_community') == 'Yes') {
                $this->form_validation->set_rules('community', 'Community', 'trim|required');
                // $this->form_validation->set_rules('gate_code', 'Gate code', 'trim|required');
                //$this->form_validation->set_rules('po_box', 'Unit PO Box', 'trim|required');
                //  $this->form_validation->set_rules('community_company', 'Community Managing Company', 'trim|required');
                //$this->form_validation->set_rules('company_number', 'Community Managing Company Contact Number', 'trim|required');
            }
            if ($this->form_validation->run()) {
                $latitude = $longitude = $loc = '';
                if ($this->input->post('address1')) {
                    $loc .= $this->input->post('address1') . " ";
                }
                if ($this->input->post('address2')) {
                    $loc .= $this->input->post('address2') . " ";
                }
                if ($this->input->post('city')) {
                    $loc .= $this->input->post('city') . " ";
                }
                if ($this->input->post('state')) {
                    $stname = get_state_name($this->input->post('state'));
                    $loc .=$stname . " ";
                }
                if ($this->input->post('zip')) {
                    $loc .= $this->input->post('zip');
                }
                $val = $this->getLnt($loc);
                if (!empty($val['lat']) && !empty($val['lng'])) {
                    $latitude = $val['lat'];
                    $longitude = $val['lng'];
                }

                $conditions = $params = array();
                $conditions['where']['prop_id'] = $this->input->post('pid');

                $conditions['value']['property_type'] = $this->input->post('property_type');
                $conditions['value']['address1'] = $this->input->post('address1');
                $conditions['value']['address2'] = $this->input->post('address2');
                $conditions['value']['country_id'] = $this->input->post('country');
                $conditions['value']['state_id'] = $this->input->post('state');
                $conditions['value']['city'] = $this->input->post('city');
                $conditions['value']['zip'] = $this->input->post('zip');
                $conditions['value']['latitude'] = $latitude;
                $conditions['value']['longitude'] = $longitude;
                $conditions['value']['unit_number'] = $this->input->post('unit');
                $conditions['value']['community_status'] = $this->input->post('is_community');
                if ($this->input->post('is_community') == 'NO') {
                    $conditions['value']['gate_code'] = '';
                    $conditions['value']['unit_po_box'] = '';
                    $conditions['value']['community_managing_company'] = '';
                    $conditions['value']['community_contact_number'] = '';
                    $conditions['value']['community'] = '';
                } else {
                    $config['upload_path'] = './assets/uploads/community_documents/';
                    $config['allowed_types'] = 'jpeg|jpg|png|txt|pdf|doc|docx';
                    $config['encrypt_name'] = TRUE;
                    $this->load->library('upload', $config);
                    if (!empty($_FILES['comm_docs']['name'])) {
                        if (!$this->upload->do_upload('comm_docs')) {
                            $error = $this->upload->display_errors();
                            $error_message.=$error;
                            $failure = TRUE;
                        } else {
                            $comm_data = array('upload_data' => $this->upload->data());
                        }
                    }
                    if (isset($comm_data['upload_data']['file_name']) && !empty($comm_data['upload_data']['file_name'])) {
                        $file = $comm_data['upload_data']['file_name'];
                        $conditions['value']['community_doc'] = $file;
                    }
                    $conditions['value']['gate_code'] = $this->input->post('gate_code');
                    $conditions['value']['unit_po_box'] = $this->input->post('po_box');
                    $conditions['value']['community_managing_company'] = $this->input->post('community_company');
                    $conditions['value']['community_contact_number'] = $this->input->post('company_number');
                    $conditions['value']['community'] = $this->input->post('community');
                }
                $params['table'] = 'tbl_properties';
                $this->common_model->update($conditions, $params);
                $success_message = '<p>Property Information Updated successfully.</p>';
            } else {
                $error_message = validation_errors();
                $failure = true;
            }
            if ($this->input->is_ajax_request()) {
                if ($failure) {
                    $data['success'] = false;
                    $data['error_message'] = $error_message;
                } else {
                    $data['success'] = true;
                    $data['resetForm'] = true;
                    $data['success_message'] = $success_message;
                    $data['url'] = '';
                    $data['selfReload'] = true;
                }
                $data['scrollToElement'] = true;
                echo json_encode($data);
                die;
            }
        }
    }

    public function delete_property() {
        if ($this->input->post('id')) {
            $conditions = $params = array();
            $conditions['value']['status'] = 'Inactive';
            $conditions['where']['prop_id'] = $this->input->post('id');
            $params['table'] = 'tbl_properties';
            $this->common_model->update($conditions, $params);

            $conditions = $params = array();
            $params['table']        = 'tbl_stripe_card_info';
            $conditions['user_id']  = $this->session->userdata('MEM_ID');
            $conditions['property_id']       = $this->input->post('id');
            $this->common_model->delete($conditions, $params);

            $data['success'] = true;
            $data['resetForm'] = true;
            $data['success_message'] = '';
            $data['url'] = '';
        } else {
            $data['success'] = false;
            $data['error_message'] = 'Property id not found';
        }
        echo json_encode($data);
        die;
    }

    /**
     * Method Name : set_upload_options
     * Author Name : Jasbir Singh
     * Description : config photo upload
     */
    function set_upload_options($id, $folder, $type) {
        $config = array();
        $config['upload_path'] = $folder . "/" . $type . "_" . $id;
        $config['allowed_types'] = 'jpg|jpeg|gif|png';
        $config['overwrite'] = FALSE;
        $config['encrypt_name'] = TRUE;
        if (!is_dir($folder . "/" . $type . "_" . $id)) {
            mkdir($folder . "/" . $type . "_" . $id, 0777, TRUE);
        }
        return $config;
    }

    /**
     * Method Name : thumb
     * Author Name : Jasbir Singh
     * Date : 15 June 2015
     * Description : creating Thumbnails of photos
     */
    function thumb($data) {
        $config['image_library'] = 'gd2';
        $config['source_image'] = $data['full_path'];
        $config['create_thumb'] = TRUE;
        $config['maintain_ratio'] = TRUE;
        $config['width'] = 350;
        $config['height'] = 250;
        $this->load->library('image_lib');
        $this->image_lib->initialize($config);
        $this->image_lib->resize();
        $this->image_lib->clear();
        return true;
    }

    public function upload_property_photo() {
        if (isset($_FILES) && !empty($_FILES)) {
            $failure = FALSE;
            $folder = PROPERTY_PHOTOS;
            $prop_id = $this->input->post('propty_id');
            $this->load->library('upload');
            $files = $_FILES;
            $count = count($_FILES['userfile']['name']);
            $k = $cnt = 0;
            for ($i = 0; $i < $count; $i++) {
                $_FILES['userfile']['name'] = $files['userfile']['name'][$i];
                $_FILES['userfile']['type'] = $files['userfile']['type'][$i];
                $_FILES['userfile']['tmp_name'] = $files['userfile']['tmp_name'][$i];
                $_FILES['userfile']['error'] = $files['userfile']['error'][$i];
                $_FILES['userfile']['size'] = $files['userfile']['size'][$i];
                $this->upload->initialize($this->set_upload_options($prop_id, $folder, 'property'));
                if ($files['userfile']['name'][$i] != '') {
                    if ($this->upload->do_upload() == False) {
                        $error_message = $this->upload->display_errors();
                        $failure = true;
                    } else {
                        $data = $this->upload->data();
                        $this->thumb($data);
                        $thumb = $data['raw_name'] . '_thumb' . $data['file_ext'];
                        $orig_file = $data['raw_name'] . $data['file_ext'];
                        $gallery_add[] = array(
                            'property_id' => $prop_id,
                            'image' => PROPERTY_PHOTOS . "/property_" . $prop_id . "/" . $orig_file,
                            'thumb' => PROPERTY_PHOTOS . "/property_" . $prop_id . "/" . $thumb,
                            'tenant_id' => 0, //$this->session->userdata('MEM_ID'),
                            'status' => 'Active',
                            'add_date' => current_date(),
                            'ip' => ''
                        );
                        $cnt++;
                        $success_message = 'Photos uploaded successfully';
                    }
                }
            }
//insert photo data into database
            if (isset($gallery_add) && $gallery_add != '') {
                $conditions = $params = array();
                $params['batch_mode'] = true;
                $conditions = $gallery_add;
                $params['table'] = 'tbl_property_photos';
                $insert_id = $this->common_model->insert_data($conditions, $params);
            }
        } if ($this->input->is_ajax_request()) {
            if ($failure) {
                $dataa['success'] = false;
                $dataa['error_message'] = $error_message;
            } else {
                $dataa['success'] = true;
                //    $data['resetForm'] = true;
                $dataa['success_message'] = $success_message;
                $dataa['url'] = site_url('my-properties') . '/#tabs-' . $prop_id;
            }
            $dataa['selfReload'] = true;
            $dataa['scrollToElement'] = true;
            echo json_encode($dataa);
            die;
        }
    }

    public function get_photos($id) {

        $conditions = $params = array();
        $params['table'] = 'tbl_property_photos';
        $conditions['property_id'] = $id;
        $params['fields'] = array('id', 'CONCAT("' . base_url() . '", image) AS url', 'CONCAT("' . base_url() . '", thumb) AS thumb');
        $params['order_by'] = 'id DESC';
        $output['photos'] = $this->common_model->get_data($conditions, $params);
        $this->load->view($this->config->item('defaultfolder') . '/prop_photos', $output);
    }

    public function delete_photo() {
        if ($this->input->post('id')) {
            $conditions = $params = array();
            $params['table'] = 'tbl_property_photos';
            $conditions['id'] = $this->input->post('id');
            $params['single_row'] = TRUE;
            $photo = $this->common_model->get_data($conditions, $params);
            if (!empty($photo)) {
                if (!empty($photo->thumb)) {
                    //  unlink(FCPATH . $photo->thumb);
                }
                unlink(FCPATH . $photo->image);
            }
            $conditions = $params = array();
            $conditions['id'] = $this->input->post('id');
            $params['table'] = 'tbl_property_photos';
            $this->common_model->delete($conditions, $params);
        }
        echo '1';
        exit;
    }

    public function checkoutProperty($prop_id) {
        $conditions = $params = array();
        $params['complex'] = true;

        $conditions['tables'] = array(
            'lease' => 'tbl_properties_lease_detail',
            'tenant' => 'tbl_members',
        );
        $conditions['table'] = 'lease';
        $conditions['on']['tenant'] = array(
            'sign' => '=',
            'column' => 'mem_id',
            'alias_column' => 'tenant_id',
            'alias_other' => 'lease',
        );

        $conditions['where']['property_id']['alias'] = 'lease';
        $conditions['where']['property_id']['value'] = $prop_id;
        $conditions['where']['check_out_status']['alias'] = 'lease';
        $conditions['where']['check_out_status']['value'] = '0';
        $params['fields'] = array('tenant.*');
        $info = $this->common_model->get_data($conditions, $params);
        if (!empty($info)) {
            foreach ($info as $details) {
                $this->load->helper('twilio_helper');  // references library/twilio_helper.php
                $service = get_twilio_service();
                try {
                    $mnumber = $details->mobile_no;
                    $number = "+1" . preg_replace("/[^0-9]/", "", $mnumber);
                    $playstore = PLAYSTORE;
                    $appstore = APPSTORE;
                    $playstore_img = $this->config->item("uploads").'icons/google-play.png';
                    $appstore_img = $this->config->item("uploads").'icons/app_store.png';
                    $playstore_img_html = '<a href="'.$playstore.'"><img src="'.$playstore_img.'"></a>';
                    $appstore_img_html = '<a href="'.$appstore.'"><img src="'.$appstore_img.'"></a>';
                    //$msg = "Welcome Home! Your landlord has enrolled you in TenantTag. *Download your app Now. $appstore_img_html --iOS or $playstore_img_html --Android*  Username:" . $details->slug . " and Password: " . $details->tpass;
                    $msg = "Welcome Home! Your \n landlord has enrolled you in \n TenantTag. Download your app for iOS or Android. \n \n iOS: $appstore \n \n Android: $playstore  \n \n Username:" . $details->slug . "  \n \n Password: " . $details->tpass;
                    //  $service->account->messages->create("+19046743077", "$number", "$msg", array());
                    $service->account->messages->create(array(
                        'To' => $number,
                        'From' => "+19046743077",
                        'Body' => $msg));

                    $conditions = $params = array();
                    $conditions['value']['move_status'] = 1;
                    $conditions['where']['mem_id'] = $details->mem_id;
                    $params['table'] = 'tbl_members';
                    $this->common_model->update($conditions, $params);
                } catch (Exception $e) {
                    $file = 'sms_error_logs.txt';
                    if (file_exists($file)) {
                        $current = file_get_contents($file);
                        $current .= $e->getMessage() . "\n";
                        file_put_contents($file, $current);
                    }
                }
            }
            $conditions = $params = array();
            $conditions['value']['check_out_status'] = 1;
            $conditions['where']['property_id'] = $prop_id;
            $params['table'] = 'tbl_properties_lease_detail';
            $this->common_model->update($conditions, $params);
        }
    }

    public function delete_community_doc() {
        if ($this->input->post('id')) {
            $conditions = $params = array();
            $params['table'] = 'tbl_properties';
            $conditions['prop_id'] = $this->input->post('id');
            $params['single_row'] = TRUE;
            $doc = $this->common_model->get_data($conditions, $params);
            if (!empty($doc->community_doc)) {
                if (!empty($doc->community_doc)) {
                    unlink(FCPATH . "assets/uploads/community_documents/" . $doc->community_doc);
                }
            }
            $conditions = $params = array();
            $conditions['value']['community_doc'] = '';
            $conditions['where']['prop_id'] = $this->input->post('id');
            $params['table'] = 'tbl_properties';
            $this->common_model->update($conditions, $params);
        }
        echo '1';
        exit;
    }

    /**
     * Method Name : synapse_bank_linked
     * Author Name : Lakhvinder Singh
     * Date : 02 March 2017
     * Description : on click of particular synapse bank
     */
    function synapse_bank_linked(){
        if($this->input->post('bank_code')){
            $bank_code = $this->input->post('bank_code');
            $bank_img = $this->input->post('bank_img');
            //echo $bank_img; exit;
           /* $bank_img_arr = array(
                                    'ally' => 'ally.png',
                                    'bofa' => 'bofa.png',
                                    'boftw' => 'bofw.png',
                                    'bbt' => 'bbt.png',
                                    'capone360' => 'cap360.png',
                                    'schwab' => 'charles_schwab.png',
                                    'chase' => 'chase.png',
                                    'citi' => 'citi.png',
                                    'fidelity' => 'fidelity.png',
                                    'firsttennessee' => 'first_tn.png',
                                    'nfcu' => 'nfcu.jpg',
                                    'pnc' => 'pnc.png',
                                    'regions' => 'regionsbank.png',
                                    'suntrust' => 'suntrust.png',
                                    'td' => 'td.png',
                                    'us' => 'usbank.png',
                                    'usaa' => 'usaa.png',
                                    'wells' => 'wells_fargo.png',              
                                    'altra' => 'altra.jpg'               
                                );*/
                $html = '';
            if($this->input->post('bank_code') == 'other'){
                $bank_img = $this->config->item('templateassets').'images/other-bank.jpg';
                $html .= '<form class="bank_login_form" id="bank_login_form" name="bank_login_form" method="post" action="#">';
                    $html .= '<div class="ajax_report alert display-hide" role="alert" style="margin-bottom: 10px; margin-left: 0px; width: 500px; position: relative; top: 100px;">
                                <span class="close-message"></span>
                                <div class="ajax_message">Saved</div>
                           </div>';
                    $html .= '<div class="bank_login">';
                        $html .= '<div class="bank_login_image">';
                            $html .= '<img src="'.$bank_img.'">';
                            $html .= '<input type="hidden" class="bank_code" name="bank_code" id="bank_code" value="'.$bank_code.'">';
                        $html .= '</div>';
                        
                        $html .= '<div class="bank_login_user">';
                            $html .= '<label for="Account Nickname"> </label>';
                            $html .= '<input type="text" placeholder="Account Nickname" class="account_nickname" name="nickname" id="nickname">';
                        $html .= '</div>';
                        
                        $html .= '<div class="bank_login_user">';
                            $html .= '<label for="Account Number"> </label>';
                            $html .= '<input type="text" placeholder="Account Number"  class="account_number" name="account_num" id="account_num">';
                        $html .= '</div>';

                        $html .= '<div class="bank_login_user">';
                            $html .= '<label for="Routing Number"> </label>';
                            $html .= '<label for="Routing Number"> </label>';
                            $html .= '<input type="text" placeholder="Routing Number" class="routing_number" name="routing_num" id="routing_num">';
                        $html .= '</div>';

                        $html .= '<div class="bank_login_button">';
                            $html .= '<input type="button" class="submit_login_form" value="Login" name="submit_login_form" id="submit_login_form">';
                        $html .= '</div>';
                    $html .= '</div>';
                $html .= '</form>';             
                $html .= '<button type="button" class="btn btn-default go_back_to_first_step">Go Back</button>';   
            }else{
                //$bank_img = 'https://cdn.synapsepay.com/bank_logos/'.$bank_img_arr[$bank_code];
                $html .= '<form class="bank_login_form" id="bank_login_form" name="bank_login_form" method="post" action="#">';
                    $html .= '<div class="ajax_report alert display-hide" role="alert" style="margin-bottom: 10px; margin-left: 0px; width: 500px; position: relative; top: 100px;">
                                <span class="close-message"></span>
                                <div class="ajax_message">Saved</div>
                           </div>';
                    $html .= '<div class="bank_login">';
                        $html .= '<div class="bank_login_image">';
                            $html .= '<img src="'.$bank_img.'">';
                            $html .= '<input type="hidden" class="bank_code" name="bank_code" id="bank_code" value="'.$bank_code.'">';
                        $html .= '</div>';
                        
                        $html .= '<div class="bank_login_user">';
                            $html .= '<input type="text" placeholder="Enter your Online Banking Username" class="bank_username" name="bank_name" id="bank_name">';
                        $html .= '</div>';
                        
                        $html .= '<div class="bank_login_password">';
                            $html .= '<input type="password" placeholder="Enter your Online Banking Password" class="bank_password" name="bank_pw" id="bank_pw">';
                        $html .= '</div>';

                        $html .= '<div class="bank_login_button">';
                            $html .= '<input type="button" class="submit_login_form" value="Login" name="submit_login_form" id="submit_login_form">';
                        $html .= '</div>';
                    $html .= '</div>';             
                $html .= '</form>';             
                $html .= '<button type="button" class="btn btn-default go_back_to_first_step" >Go Back</button>';   
            }
            $failure = false;
            $success_message ="successfully submitted";
        }else{
            $failure = TRUE;
            $success_message ="Post Data Empty";
        }


        if ($this->input->is_ajax_request()) {
            if ($failure) {
                $data['success'] = false;
                $data['error_message'] = $error_message;
            } else {
                $data['success'] = true;
                $data['resetForm'] = true;
                $data['success_message'] = $success_message;
                $data['html'] = $html;
                //$data['url'] = site_url('add-property-step2');
            }
            $data['scrollToElement'] = true;
            echo json_encode($data);
            die;
        }
    }


    /**
     * Method Name : gerUserSynaspeFingerprintAndIp
     * Author Name : Lakhvinder Singh
     * Date : 02 March 2017
     * Description : get synapse_fingerprint and ip address
     */   
    function gerUserSynaspeFingerprintAndIp($user_id){
        $conditions = $params = array();
        $params['table'] = 'tbl_members';
        $conditions['mem_id'] = $user_id;
        $params['single_row'] = TRUE;
        $params['fields'] = array('synapse_fingerprint','synapse_ip_address');
        $data = $this->common_model->get_data($conditions, $params);
        return $data;
    }

    /**
     * Method Name : get_client_ip
     * Author Name : Lakhvinder Singh
     * Date : 02 March 2017
     * Description : get front end user IP
     */
    function get_client_ip() {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if(getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if(getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if(getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if(getenv('HTTP_FORWARDED'))
           $ipaddress = getenv('HTTP_FORWARDED');
        else if(getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }

    /**
     * Method Name : update_fingerprint
     * Author Name : Lakhvinder Singh
     * Date : 02 March 2017
     * Description : Update user fingerprint
     */
    function update_fingerprint() {
        $login_id       = $this->session->userdata('MEM_ID');
        if ($this->input->post('fingerprint')) {
            $synapse_data           = $this->gerUserSynaspeFingerprintAndIp($login_id);
            $SYNAPSE_IP_ADDRESS     = $synapse_data->synapse_ip_address; 
            $SYNAPSE_FINGERPRINT    = $synapse_data->synapse_fingerprint; 
            if(empty($SYNAPSE_FINGERPRINT) || $SYNAPSE_FINGERPRINT == NULL){
                $conditions = $params = array();            
                $conditions['value']['synapse_fingerprint'] = $this->input->post('fingerprint');            
                $conditions['value']['synapse_ip_address'] = $this->get_client_ip();            
                $conditions['where']['mem_id'] = $login_id;
                $params['table'] = 'tbl_members';
                $this->common_model->update($conditions, $params);
                
            }
        }            
    }

    /**
     * Method Name : createSynapseUser
     * Author Name : Lakhvinder Singh
     * Date : 02 March 2017
     * Description : Create synsapse user if not exist
     */
    function createSynapseUser($user_id){
        $login_id               = $user_id;
        $user_info              = $this->getUserInfo($login_id);
        $SYNAPSE_CLIENT_ID      = SYNAPSE_CLIENT_ID;
        $SYNAPSE_CLIENT_SECRET  = SYNAPSE_CLIENT_SECRET; 
        $synapse_data           = $this->gerUserSynaspeFingerprintAndIp($login_id);
        $SYNAPSE_IP_ADDRESS     = $synapse_data->synapse_ip_address; 
        $SYNAPSE_FINGERPRINT    = $synapse_data->synapse_fingerprint;         
        //print_r($synapse_data); print_r($SYNAPSE_CLIENT_ID);  
        if(empty($user_info->synapse_user_id)){
            $email      = $user_info->email;
            $mobile_no  = $user_info->mobile_no;
            $legal_name = $user_info->first_name.' '.$user_info->last_name;

            /************************** create user using curl *************************/
            $curl       = curl_init();
            curl_setopt_array($curl, array(
                                            CURLOPT_URL             => SYNAPSE_API_BASE_URL."users",
                                            CURLOPT_RETURNTRANSFER  => true,
                                            CURLOPT_ENCODING        => "",
                                            CURLOPT_MAXREDIRS       => 10,
                                            CURLOPT_TIMEOUT         => 30,
                                            CURLOPT_HTTP_VERSION    => CURL_HTTP_VERSION_1_1,
                                            CURLOPT_CUSTOMREQUEST   => "POST",
                                            CURLOPT_POSTFIELDS      => "{\n  \"logins\": [\n    {\n      \"email\": \"$email\"\n    }\n  ],\n  \"phone_numbers\": [\n    \"$mobile_no\"\n  ],\n  \"legal_names\": [\n    \"$legal_name\"\n  ],\n  \"extra\": {\n    \"note\": null,\n    \"is_business\": false,\n    \"cip_tag\":2\n  }\n}",
                                            CURLOPT_HTTPHEADER      => array(
                                                                                "cache-control: no-cache",
                                                                                "content-type: application/json",
                                                                                "x-sp-gateway: $SYNAPSE_CLIENT_ID|$SYNAPSE_CLIENT_SECRET",
                                                                                "x-sp-user: |$SYNAPSE_FINGERPRINT",
                                                                                "x-sp-user-ip: $SYNAPSE_IP_ADDRESS"
                                                                            ),
                                        )
                            );
            $response   = curl_exec($curl);
            $err        = curl_error($curl);
            curl_close($curl);
            //echo SYNAPSE_API_BASE_URL."users";
            //print_r($response); exit;
            if ($err) {
                return false;
            } else {
                $json_decode           = json_decode($response); // decode the "users's API Response
                //echo "<pre>".$mobile_no; print_r($json_decode); exit;
                $synapse_id            = $json_decode->_id; 
                $refresh_token         = $json_decode->refresh_token;
                /********************** update synapse user id in the table **************************/
                $conditions = $params = array();
                $conditions['value']['synapse_user_id'] = $synapse_id;
                $conditions['where']['mem_id']          = $login_id;
                $params['table'] = 'tbl_members';
                $this->common_model->update($conditions, $params);
                $data_result              = $this->getSynapseAuthToken($refresh_token,$synapse_id,$SYNAPSE_IP_ADDRESS,$SYNAPSE_FINGERPRINT); // get auth key using refresh token
                if($data_result['status'] == 'oauth_key'){
                    $auth_key = $data_result['data'];
                }else{
                    $auth_key = '';
                }
                $return = array('synapse_user_id' => $synapse_id, 'refresh_token' => $refresh_token,'auth_key' => $auth_key);
                return json_encode($return);
            }

        }else{
            $synapse_user_id = $user_info->synapse_user_id;
            $curl            = curl_init();
            curl_setopt_array($curl, array(
                                            CURLOPT_URL             => SYNAPSE_API_BASE_URL."users/".$synapse_user_id,
                                            CURLOPT_RETURNTRANSFER  => true,
                                            CURLOPT_ENCODING        => "",
                                            CURLOPT_MAXREDIRS       => 10,
                                            CURLOPT_TIMEOUT         => 30,
                                            CURLOPT_HTTP_VERSION    => CURL_HTTP_VERSION_1_1,
                                            CURLOPT_CUSTOMREQUEST   => "GET",
                                            CURLOPT_HTTPHEADER      => array(
                                                                                "cache-control: no-cache",
                                                                                "content-type: application/json",
                                                                                "x-sp-gateway: $SYNAPSE_CLIENT_ID|$SYNAPSE_CLIENT_SECRET",
                                                                                "x-sp-user: |$SYNAPSE_FINGERPRINT",
                                                                                "x-sp-user-ip: $SYNAPSE_IP_ADDRESS"
                                                                            ),
                                        )
                            );

            $response = curl_exec($curl);
            $err = curl_error($curl);
            curl_close($curl);
            if ($err) {
                return false;
            } else {
                $json_decode    = json_decode($response);
                $refresh_token  = $json_decode->refresh_token;
                $data_result       = $this->getSynapseAuthToken($refresh_token,$synapse_user_id,$SYNAPSE_IP_ADDRESS,$SYNAPSE_FINGERPRINT); // get auth key using refresh token
                if($data_result['status'] == 'oauth_key'){
                    $auth_key = $data_result['data'];
                }else{
                    if($data_result['status'] == 'validate_user'){
                    	$phone_number = $data_result['data'][0];
                    	$validate_user_text = $this->sendVerificationCode($phone_number);
                        $auth_key = ''; //////////////////////////////////////////////need to change this code
                		$return         = array('synapse_user_id' => $synapse_user_id, 'auth_key' => '','response_data'=>$data_result, 'refresh_token' => $refresh_token,'auth_key_status' => 'unverfied','phone_number'=>$phone_number);
                		return $return;
                    }else{
                        $auth_key = '';
                    }
                }
                $return         = array('synapse_user_id' => $synapse_user_id, 'response_data'=>$data_result,'refresh_token' => $refresh_token,'auth_key' => $auth_key);
                return $return;
            }
        }        
    }

    function getUserRefreshToken($login_id){
    	 $user_info              = $this->getUserInfo($login_id);
        $synapse_user_id        = $user_info->synapse_user_id;
        if(empty($synapse_user_id)){
        	$this->createSynapseUser($login_id);
        }
         $user_info              = $this->getUserInfo($login_id);
        $synapse_user_id        = $user_info->synapse_user_id;
        $SYNAPSE_CLIENT_ID      = SYNAPSE_CLIENT_ID;
        $SYNAPSE_CLIENT_SECRET  = SYNAPSE_CLIENT_SECRET;
        $synapse_data           = $this->gerUserSynaspeFingerprintAndIp($login_id);
        $SYNAPSE_IP_ADDRESS     = $synapse_data->synapse_ip_address; 
        $SYNAPSE_FINGERPRINT    = $synapse_data->synapse_fingerprint; 
        $curl            = curl_init();
        curl_setopt_array($curl, array(
                                        CURLOPT_URL             => SYNAPSE_API_BASE_URL."users/".$synapse_user_id,
                                        CURLOPT_RETURNTRANSFER  => true,
                                        CURLOPT_ENCODING        => "",
                                        CURLOPT_MAXREDIRS       => 10,
                                        CURLOPT_TIMEOUT         => 30,
                                        CURLOPT_HTTP_VERSION    => CURL_HTTP_VERSION_1_1,
                                        CURLOPT_CUSTOMREQUEST   => "GET",
                                        CURLOPT_HTTPHEADER      => array(
                                                                            "cache-control: no-cache",
                                                                            "content-type: application/json",
                                                                            "x-sp-gateway: $SYNAPSE_CLIENT_ID|$SYNAPSE_CLIENT_SECRET",
                                                                            "x-sp-user: |$SYNAPSE_FINGERPRINT",
                                                                            "x-sp-user-ip: $SYNAPSE_IP_ADDRESS"
                                                                        ),
                                    )
                        );

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
            return false;
        } else {
            $json_decode    = json_decode($response);
            //print_r($response); exit;
            $refresh_token  = $json_decode->refresh_token;
            return $refresh_token;
        }
    }


    function sendVerificationCode($phone_number){
    	//print_r($phone_number); exit;
    	$login_id       		= $this->session->userdata('MEM_ID');
        $user_info              = $this->getUserInfo($login_id);
        $synapse_user_id        = $user_info->synapse_user_id;
        $SYNAPSE_CLIENT_ID      = SYNAPSE_CLIENT_ID;
        $SYNAPSE_CLIENT_SECRET  = SYNAPSE_CLIENT_SECRET;
        $synapse_data           = $this->gerUserSynaspeFingerprintAndIp($login_id);
        $SYNAPSE_IP_ADDRESS     = $synapse_data->synapse_ip_address; 
        $SYNAPSE_FINGERPRINT    = $synapse_data->synapse_fingerprint; 
        $refresh_token 			= $this->getUserRefreshToken($login_id);
    	$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL             => SYNAPSE_API_BASE_URL."oauth/".$synapse_user_id,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS => "{\n    \"refresh_token\": \"$refresh_token\",\n    \"phone_number\":\"$phone_number\"\n}",
			CURLOPT_HTTPHEADER => array(
							    "cache-control: no-cache",
							    "content-type: application/json",
                                "x-sp-gateway: $SYNAPSE_CLIENT_ID|$SYNAPSE_CLIENT_SECRET",
                                "x-sp-user: |$SYNAPSE_FINGERPRINT",
                                "x-sp-user-ip: $SYNAPSE_IP_ADDRESS"
						  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		  return false;
		} else {
		   $json_decode     = json_decode($response);
		   if(isset($json_decode->error)){
           		$message  		= $json_decode->error->en;
		   		//print_r($json_decode); exit;		   	
		   }else{

           	$message  		= $json_decode->message->en;
		   }
           return $message;
		}
    }


    function sendVerificationCodeByPost(){
    	//print_r($phone_number); exit;

    	$this->form_validation->set_message('required', '%s is required.');
        $this->form_validation->set_rules('phone_number', 'Phone number', 'trim|required');
        if ($this->form_validation->run()) { 

	    	$phone_number 			= $this->input->post('phone_number');    	
	    	$login_id       		= $this->session->userdata('MEM_ID');
	        $user_info              = $this->getUserInfo($login_id);
        	$synapse_user_id        = $user_info->synapse_user_id;
	        $SYNAPSE_CLIENT_ID      = SYNAPSE_CLIENT_ID;
	        $SYNAPSE_CLIENT_SECRET  = SYNAPSE_CLIENT_SECRET;
	        $synapse_data           = $this->gerUserSynaspeFingerprintAndIp($login_id);
	        $SYNAPSE_IP_ADDRESS     = $synapse_data->synapse_ip_address; 
	        $SYNAPSE_FINGERPRINT    = $synapse_data->synapse_fingerprint; 
	        if($user_info->mobile_no != $phone_number){
	        	$conditions = $params = array();
	            $conditions['value']['synapse_user_id'] = '';
	            $conditions['value']['mobile_no']       = $phone_number;
	            $conditions['where']['mem_id']          = $login_id;
	            $params['table'] = 'tbl_members';
	            $this->common_model->update($conditions, $params);
	        	$this->createSynapseUser($login_id); 


        	    $email      = $user_info->email;
	            $mobile_no  = $phone_number;
	            $legal_name = $user_info->first_name.' '.$user_info->last_name;

	            /************************** create user using curl *************************/
	            $curl       = curl_init();
	            curl_setopt_array($curl, array(
	                                            CURLOPT_URL             => SYNAPSE_API_BASE_URL."users",
	                                            CURLOPT_RETURNTRANSFER  => true,
	                                            CURLOPT_ENCODING        => "",
	                                            CURLOPT_MAXREDIRS       => 10,
	                                            CURLOPT_TIMEOUT         => 30,
	                                            CURLOPT_HTTP_VERSION    => CURL_HTTP_VERSION_1_1,
	                                            CURLOPT_CUSTOMREQUEST   => "POST",
	                                            CURLOPT_POSTFIELDS      => "{\n  \"logins\": [\n    {\n      \"email\": \"$email\"\n    }\n  ],\n  \"phone_numbers\": [\n    \"$mobile_no\"\n  ],\n  \"legal_names\": [\n    \"$legal_name\"\n  ],\n  \"extra\": {\n    \"note\": null,\n    \"is_business\": false,\n    \"cip_tag\":2\n  }\n}",
	                                            CURLOPT_HTTPHEADER      => array(
	                                                                                "cache-control: no-cache",
	                                                                                "content-type: application/json",
	                                                                                "x-sp-gateway: $SYNAPSE_CLIENT_ID|$SYNAPSE_CLIENT_SECRET",
	                                                                                "x-sp-user: |$SYNAPSE_FINGERPRINT",
	                                                                                "x-sp-user-ip: $SYNAPSE_IP_ADDRESS"
	                                                                            ),
	                                        )
	                            );
	            $response   = curl_exec($curl);
	            $err        = curl_error($curl);
	            curl_close($curl);
	            //echo SYNAPSE_API_BASE_URL."users";
	            //print_r($response); exit;
	            if ($err) {
	                return false;
	            } else {
	                $json_decode           = json_decode($response); // decode the "users's API Response
	                if(isset($json_decode->error->en)){

	                }else{
						$conditions = $params = array();
						$conditions['value']['synapse_user_id'] = $json_decode->_id; ;
						$conditions['where']['mem_id']          = $login_id;
						$params['table'] = 'tbl_members';
						$this->common_model->update($conditions, $params);		                	
	                } 
	            }

	        	$user_info              = $this->getUserInfo($login_id);
	        }
	        $synapse_user_id        = $user_info->synapse_user_id;
	        $SYNAPSE_CLIENT_ID      = SYNAPSE_CLIENT_ID;
	        $SYNAPSE_CLIENT_SECRET  = SYNAPSE_CLIENT_SECRET;
	        $synapse_data           = $this->gerUserSynaspeFingerprintAndIp($login_id);
	        $SYNAPSE_IP_ADDRESS     = $synapse_data->synapse_ip_address; 
	        $SYNAPSE_FINGERPRINT    = $synapse_data->synapse_fingerprint; 
	        $refresh_token 			= $this->getUserRefreshToken($login_id);
	    	$curl = curl_init();
			curl_setopt_array($curl, array(
				CURLOPT_URL             => SYNAPSE_API_BASE_URL."oauth/".$synapse_user_id,
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => "",
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 30,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => "POST",
				CURLOPT_POSTFIELDS => "{\n    \"refresh_token\": \"$refresh_token\",\n    \"phone_number\":\"$phone_number\"\n}",
				CURLOPT_HTTPHEADER => array(
								    "cache-control: no-cache",
								    "content-type: application/json",
	                                "x-sp-gateway: $SYNAPSE_CLIENT_ID|$SYNAPSE_CLIENT_SECRET",
	                                "x-sp-user: |$SYNAPSE_FINGERPRINT",
	                                "x-sp-user-ip: $SYNAPSE_IP_ADDRESS"
							  ),
			));

			$response = curl_exec($curl);
			$err = curl_error($curl);

			curl_close($curl);

			if ($err) {
			  	$return = array('status'=>'failed','message'=> 'failed','data'=>$err);
			} else {
			   $json_decode     = json_decode($response);
			   	//print_r($json_decode); echo $response; exit;		   	
			    if(isset($json_decode->error)){
			   		$error_message = $json_decode->error->en;
			   		$return = array('status'=>'failed','message'=> $error_message,'data'=>$json_decode);
			    }else{
	           		$return = array('status'=>'success','message'=> 'Successfully sent MFA code','data'=>$json_decode);
			    }
			}
		}else{
        	$notify_message = strip_tags(validation_errors());
	        $return = array('status'=>'failed','message'=> $notify_message,'data'=>'');
        }
       echo json_encode($return);
       exit;
    }

    public function verifySynapseMFA()
    {
    	$login_id       		= $this->session->userdata('MEM_ID');
        $refresh_token 			= $this->getUserRefreshToken($login_id);
    	$validation_pin 		= $this->input->post('validation_pin');
    	$user_info              = $this->getUserInfo($login_id);
        $synapse_user_id        = $user_info->synapse_user_id;
        $SYNAPSE_CLIENT_ID      = SYNAPSE_CLIENT_ID;
        $SYNAPSE_CLIENT_SECRET  = SYNAPSE_CLIENT_SECRET;
        $synapse_data           = $this->gerUserSynaspeFingerprintAndIp($login_id);
        $SYNAPSE_IP_ADDRESS     = $synapse_data->synapse_ip_address; 
        $SYNAPSE_FINGERPRINT    = $synapse_data->synapse_fingerprint; 
		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL             => SYNAPSE_API_BASE_URL."oauth/".$synapse_user_id,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => "{\n    \"refresh_token\": \"$refresh_token\",\n    \"validation_pin\":\"$validation_pin\"\n}",
		  CURLOPT_HTTPHEADER => array(
							    "cache-control: no-cache",
							    "content-type: application/json",
                                "x-sp-gateway: $SYNAPSE_CLIENT_ID|$SYNAPSE_CLIENT_SECRET",
                                "x-sp-user: |$SYNAPSE_FINGERPRINT",
                                "x-sp-user-ip: $SYNAPSE_IP_ADDRESS"
						  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		  $return = array('status'=> 'failed');
		} else {
			$json_decode = json_decode($response);
			if(isset($json_decode->error->en)){
				$error_message = $json_decode->error->en;
		  		$return = array('status'=>'failed','message'=> $error_message,'data'=>$json_decode);
			}else{
		  		$return = array('status'=>'success','message'=>'Successfully','data' =>$json_decode);
			}
		}
		echo json_encode($return);
		exit;
    }


    function addSynapsePayDocument($user_id,$email,$phone_number,$ip,$name,$alias,$entity_type,$entity_scope,$day,$month,$year,$address_street,$address_city,$address_subdivision,$address_postal_code,$address_country_code,$v_document_value,$v_document_type,$p_document_value,$p_document_type,$p_document_value_2,$p_document_type_2){
        $login_id               = $user_id;
        $user_info              = $this->getUserInfo($login_id);
        $synapse_user_id        = $user_info->synapse_user_id;
        $SYNAPSE_CLIENT_ID      = SYNAPSE_CLIENT_ID;
        $SYNAPSE_CLIENT_SECRET  = SYNAPSE_CLIENT_SECRET;
        $synapse_data           = $this->gerUserSynaspeFingerprintAndIp($login_id);
        $SYNAPSE_IP_ADDRESS     = $synapse_data->synapse_ip_address; 
        $SYNAPSE_FINGERPRINT    = $synapse_data->synapse_fingerprint; 
        $synapse_user_detail    = $this->createSynapseUser($login_id);
        $auth_key               = $synapse_user_detail['auth_key'];

        //$p_document_type = 'AUTHORIZATION';
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL             => SYNAPSE_API_BASE_URL."users/".$synapse_user_id,
            CURLOPT_RETURNTRANSFER  => true,
            CURLOPT_ENCODING        => "",
            CURLOPT_MAXREDIRS       => 10,
            CURLOPT_TIMEOUT         => 30,
            CURLOPT_HTTP_VERSION    => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST   => "PATCH",
            //CURLOPT_POSTFIELDS      => "{\n    \"documents\":[{\n        \"email\":\"$email\",\n        \"phone_number\":\"$phone_number\",\n        \"ip\":\"$ip\",\n        \"name\":\"$name\",\n        \"alias\":\"$alias\",\n        \"entity_type\":\"$entity_type\",\n        \"entity_scope\":\"$entity_scope\",\n        \"day\":$day ,\n        \"month\":$month ,\n        \"year\":$year ,\n        \"address_street\":\"$address_street\",\n        \"address_city\":\"$address_city\",\n        \"address_subdivision\":\"$address_subdivision\",\n        \"address_postal_code\":\"$address_postal_code\",\n        \"address_country_code\":\"$address_country_code\",\n        \"virtual_docs\":[{\n            \"document_value\":\"$v_document_value\",\n            \"document_type\":\"$v_document_type\"\n        }],\n        \"physical_docs\":[{\n            \"document_value\": \"$p_document_value\",\n            \"document_type\": \"$p_document_type\"\n        }]\n    }]\n}",
            CURLOPT_POSTFIELDS      => "{\n    \"documents\":[{\n        \"email\":\"$email\",\n        \"phone_number\":\"$phone_number\",\n        \"ip\":\"$ip\",\n        \"name\":\"$name\",\n        \"alias\":\"$alias\",\n        \"entity_type\":\"$entity_type\",\n        \"entity_scope\":\"$entity_scope\",\n        \"day\":$day ,\n        \"month\":$month ,\n        \"year\":$year ,\n        \"address_street\":\"$address_street\",\n        \"address_city\":\"$address_city\",\n        \"address_subdivision\":\"$address_subdivision\",\n        \"address_postal_code\":\"$address_postal_code\",\n        \"address_country_code\":\"$address_country_code\",
                    \n        \"virtual_docs\":[{\n            \"document_value\":\"$v_document_value\",\n            \"document_type\":\"$v_document_type\"\n        }],
                    \n        \"physical_docs\":
                            [\n        
                                    {\n          \"document_value\": \"$p_document_value\",\n          \"document_type\": \"$p_document_type\"\n        },\n        
                                    {\n          \"document_value\": \"$p_document_value_2\",\n          \"document_type\": \"$p_document_type_2\"\n        }\n
                            ]\n
                    }\n  
                ]\n
            }",
            CURLOPT_HTTPHEADER      => array(
                                                "cache-control: no-cache",
                                                "content-type: application/json",
                                                "x-sp-gateway: $SYNAPSE_CLIENT_ID|$SYNAPSE_CLIENT_SECRET",
                                                "x-sp-user: $auth_key|$SYNAPSE_FINGERPRINT",
                                                "x-sp-user-ip: $SYNAPSE_IP_ADDRESS"
                                            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        //echo "{\n  \"login\":{\n    \"oauth_key\":\"$auth_key\"\n  },\n  \"user\":{\n    \"documents\":[{\n        \"email\":\"$email\",\n        \"phone_number\":\"$phone_number\",\n        \"ip\":\"$ip\",\n        \"name\":\"$name\",\n        \"alias\":\"$alias\",\n        \"entity_type\":\"$entity_type\",\n        \"entity_scope\":\"$entity_scope\",\n        \"day\":$day ,\n        \"month\":$month ,\n        \"year\":$year ,\n        \"address_street\":\"$address_street\",\n        \"address_city\":\"$address_city\",\n        \"address_subdivision\":\"$address_subdivision\",\n        \"address_postal_code\":\"$address_postal_code\",\n        \"address_country_code\":\"$address_country_code\",\n        \"virtual_docs\":[{\n            \"document_value\":\"$v_document_value\",\n            \"document_type\":\"$v_document_type\"\n        }],\n        \"physical_docs\":[{\n            \"document_value\": \"$p_document_value\",\n            \"document_type\": \"$p_document_type\"\n        }]\n    }],\r\n    \"fingerprint\":\"$SYNAPSE_FINGERPRINT\"\r\n  }\r\n}"; exit;
       // print_r(json_decode($response)); exit;
        if ($err) {
            return false;
        } else {
            $json_decode    = json_decode($response);
            return $json_decode;
        }
    }

    function updateSynapsePayDocument($user_id,$email,$phone_number,$ip,$name,$alias,$entity_type,$entity_scope,$day,$month,$year,$address_street,$address_city,$address_subdivision,$address_postal_code,$address_country_code,$v_document_value,$v_document_type,$p_document_value,$p_document_type,$p_document_value_2,$p_document_type_2,$kyc_id){
        $login_id               = $user_id;
        $user_info              = $this->getUserInfo($login_id);
        $synapse_user_id        = $user_info->synapse_user_id;
        $SYNAPSE_CLIENT_ID      = SYNAPSE_CLIENT_ID;
        $SYNAPSE_CLIENT_SECRET  = SYNAPSE_CLIENT_SECRET;
        $synapse_data           = $this->gerUserSynaspeFingerprintAndIp($login_id);
        $SYNAPSE_IP_ADDRESS     = $synapse_data->synapse_ip_address; 
        $SYNAPSE_FINGERPRINT    = $synapse_data->synapse_fingerprint; 
        $synapse_user_detail    = $this->createSynapseUser($login_id);
        $auth_key               = $synapse_user_detail['auth_key'];
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL             => SYNAPSE_API_BASE_URL."users/".$synapse_user_id,
            CURLOPT_RETURNTRANSFER  => true,
            CURLOPT_ENCODING        => "",
            CURLOPT_MAXREDIRS       => 10,
            CURLOPT_TIMEOUT         => 30,
            CURLOPT_HTTP_VERSION    => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST   => "PATCH",
            CURLOPT_POSTFIELDS      => "{\n    \"documents\":[{\n        \"id\":\"$kyc_id\",\n        \"email\":\"$email\",\n        \"phone_number\":\"$phone_number\",\n        \"ip\":\"$ip\",\n        \"name\":\"$name\",\n        \"alias\":\"$alias\",\n        \"entity_type\":\"$entity_type\",\n        \"entity_scope\":\"$entity_scope\",\n        \"day\":$day ,\n        \"month\":$month ,\n        \"year\":$year ,\n        \"address_street\":\"$address_street\",\n        \"address_city\":\"$address_city\",\n        \"address_subdivision\":\"$address_subdivision\",\n        \"address_postal_code\":\"$address_postal_code\",\n        \"address_country_code\":\"$address_country_code\",\n        \"virtual_docs\":[{\n            \"document_value\":\"$v_document_value\",\n            \"document_type\":\"$v_document_type\"\n        }],
                            \n        \"physical_docs\":[
                                                            {\n          \"document_value\": \"$p_document_value\",\n          \"document_type\": \"$p_document_type\"\n        },\n        
                                                            {\n          \"document_value\": \"$p_document_value_2\",\n          \"document_type\": \"$p_document_type_2\"\n        }\n
                                                        ]\n    }]\n}",
            CURLOPT_HTTPHEADER      => array(
                                                "cache-control: no-cache",
                                                "content-type: application/json",
                                                "x-sp-gateway: $SYNAPSE_CLIENT_ID|$SYNAPSE_CLIENT_SECRET",
                                                "x-sp-user: $auth_key|$SYNAPSE_FINGERPRINT",
                                                "x-sp-user-ip: $SYNAPSE_IP_ADDRESS"
                                            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        //print_r(json_decode($response)); exit;
        if ($err) {
            return false;
        } else {
            $json_decode    = json_decode($response);
            return $json_decode;
        }
    }

    function getSynapseUserDetailBySynapseId($login_id){
        $user_info              = $this->getUserInfo($login_id);
        $synapse_user_id        = $user_info->synapse_user_id;
        $SYNAPSE_CLIENT_ID      = SYNAPSE_CLIENT_ID;
        $SYNAPSE_CLIENT_SECRET  = SYNAPSE_CLIENT_SECRET;
        $synapse_data           = $this->gerUserSynaspeFingerprintAndIp($login_id);
        $SYNAPSE_IP_ADDRESS     = $synapse_data->synapse_ip_address; 
        $SYNAPSE_FINGERPRINT    = $synapse_data->synapse_fingerprint; 
        $synapse_user_detail    = $this->createSynapseUser($login_id);
        $auth_key               = $synapse_user_detail['auth_key'];
        //echo SYNAPSE_API_BASE_URL."users/".$synapse_user_id; exit;
        $curl = curl_init();
        curl_setopt_array($curl, array(
                                        CURLOPT_URL             => SYNAPSE_API_BASE_URL."users/".$synapse_user_id,
                                        CURLOPT_RETURNTRANSFER  => true,
                                        CURLOPT_ENCODING        => "",
                                        CURLOPT_MAXREDIRS       => 10,
                                        CURLOPT_TIMEOUT         => 30,
                                        CURLOPT_HTTP_VERSION    => CURL_HTTP_VERSION_1_1,
                                        CURLOPT_CUSTOMREQUEST   => "GET",
                                        CURLOPT_POSTFIELDS      => "{}",
                                        CURLOPT_HTTPHEADER      => array(
                                                                        "cache-control: no-cache",
                                                                        "content-type: application/json",
                                                                        "x-sp-gateway: $SYNAPSE_CLIENT_ID|$SYNAPSE_CLIENT_SECRET"
                                                                ),
                                        )
                         );

        $response = curl_exec($curl);
        $err = curl_error($curl);
        //print_r($response); exit;
        curl_close($curl);
        if ($err) {
            return false;
        } else {
            $json_decode           = json_decode($response);
            echo "<pre>"; print_r($json_decode); exit;
        }
    }

    /**
     * Method Name : createSynapseSubscription
     * Author Name : Lakhvinder Singh
     * Date        : 20 March 2017
     * Description : createSynapseSubscription
     */

    function createSynapseSubscription(){
        $SYNAPSE_CLIENT_ID      = SYNAPSE_CLIENT_ID;
        $SYNAPSE_CLIENT_SECRET  = SYNAPSE_CLIENT_SECRET; 
        $webhookurl = base_url().'welcome/synapseWebookUrl/';
        $curl = curl_init();
        curl_setopt_array($curl, array(
                                        CURLOPT_URL             => SYNAPSE_API_BASE_URL."subscriptions",
                                        CURLOPT_RETURNTRANSFER  => true,
                                        CURLOPT_ENCODING        => "",
                                        CURLOPT_MAXREDIRS       => 10,
                                        CURLOPT_TIMEOUT         => 30,
                                        CURLOPT_HTTP_VERSION    => CURL_HTTP_VERSION_1_1,
                                        CURLOPT_CUSTOMREQUEST   => "POST",
                                        CURLOPT_POSTFIELDS      => "{\n  \"scope\": [\n    \"USERS|POST\",\n    \"USER|PATCH\",\n    \"NODES|POST\",\n    \"NODE|PATCH\",\n    \"NODE|DELETE\",\n    \"TRANS|POST\",\n    \"TRAN|PATCH\"\n  ],\n  \"url\": \"$webhookurl\"\n}",
                                        CURLOPT_HTTPHEADER      => array(
                                                                        "cache-control: no-cache",
                                                                        "content-type: application/json",
                                                                        "x-sp-gateway: $SYNAPSE_CLIENT_ID|$SYNAPSE_CLIENT_SECRET"
                                                                ),
                                        )
                         );

        $response = curl_exec($curl);
        $err = curl_error($curl);
        //print_r($response); exit;
        curl_close($curl);
        if ($err) {
            return false;
        } else {
            $json_decode           = json_decode($response);
           echo "<pre>"; print_r($json_decode); exit;
        }
    }



    /**
     * Method Name : getSynapseAuthToken
     * Author Name : Lakhvinder Singh
     * Date         : 02 March 2017
     * Description : get synapase auth token 
     */

    function getSynapseAuthToken($refresh_token,$synapse_user_id,$SYNAPSE_IP_ADDRESS,$SYNAPSE_FINGERPRINT){
        $SYNAPSE_CLIENT_ID      = SYNAPSE_CLIENT_ID;
        $SYNAPSE_CLIENT_SECRET  = SYNAPSE_CLIENT_SECRET; 
        //print_r($refresh_token); exit;
        
        $curl = curl_init();
        curl_setopt_array($curl, array(
                                        CURLOPT_URL             => SYNAPSE_API_BASE_URL."oauth/".$synapse_user_id,
                                        CURLOPT_RETURNTRANSFER  => true,
                                        CURLOPT_ENCODING        => "",
                                        CURLOPT_MAXREDIRS       => 10,
                                        CURLOPT_TIMEOUT         => 30,
                                        CURLOPT_HTTP_VERSION    => CURL_HTTP_VERSION_1_1,
                                        CURLOPT_CUSTOMREQUEST   => "POST",
                                        CURLOPT_POSTFIELDS      => "{\n    \"refresh_token\": \"$refresh_token\"\n}",
                                        CURLOPT_HTTPHEADER      => array(
                                                                        "cache-control: no-cache",
                                                                        "content-type: application/json",
                                                                        "x-sp-gateway: $SYNAPSE_CLIENT_ID|$SYNAPSE_CLIENT_SECRET",
                                                                        "x-sp-user: |$SYNAPSE_FINGERPRINT",
                                                                        "x-sp-user-ip: $SYNAPSE_IP_ADDRESS"
                                                                ),
                                        )
                         );

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        $data_result = array();
        if ($err) {
            //return false;
            $data_result['status'] = 'error'; 
            $data_result['message'] = 'Somthing Wents Wrong'; 
            $data_result['data'] = ''; 
        } else {
            $json_decode           = json_decode($response);
            if(isset($json_decode->phone_numbers) && isset($json_decode->error_code) && isset($json_decode->http_code)){
                $data_result['status']  = 'validate_user'; 
                $data_result['message'] = 'Somthing Wents Wrong';      
                $data_result['data'] = $json_decode->phone_numbers;                  
                $data_result['response_data']  = $json_decode;                  
            }else{
                $oauth_key              = $json_decode->oauth_key;    
                $data_result['status']  = 'oauth_key'; 
                $data_result['message'] = 'Auth key generated';                  
                $data_result['data']  = $oauth_key;                  
                $data_result['response_data']  = $json_decode;                  
            }
        }
        //print_r($data_result); exit;
        return $data_result;
    }



    /**
     * Method Name : getSynapseBankDetail
     * Author Name : Lakhvinder Singh
     * Date : 02 March 2017
     * Description : Get synapse bank account detail
     */

    function getSynapseBankDetail(){
        $login_id       = $this->session->userdata('MEM_ID');

        $SYNAPSE_CLIENT_ID      = SYNAPSE_CLIENT_ID;
        $SYNAPSE_CLIENT_SECRET  = SYNAPSE_CLIENT_SECRET; 
        $synapse_data           = $this->gerUserSynaspeFingerprintAndIp($login_id);
        $SYNAPSE_IP_ADDRESS     = $synapse_data->synapse_ip_address; 
        $SYNAPSE_FINGERPRINT    = $synapse_data->synapse_fingerprint;     
        if($this->input->post('bank_code')){
            $bank_code      = $this->input->post('bank_code');
            $nickname       = $this->input->post('nickname');
            $account_num    = $this->input->post('account_num');
            $routing_num    = $this->input->post('routing_num');
            $type           = 'PERSONAL';
            $class          = 'CHECKING';
            if($bank_code == 'other'){
                if($this->input->post('nickname') != '' && $this->input->post('account_num') != '' && $this->input->post('routing_num')  != ''){
                    $synapse_user_detail    = $this->createSynapseUser($login_id);
                    $synapse_user_id        = $synapse_user_detail['synapse_user_id'];
        			$auth_key               = $synapse_user_detail['auth_key'];
                    $curl = curl_init();
                    curl_setopt_array($curl, array( 
                                                    CURLOPT_URL            => SYNAPSE_API_BASE_URL."users/".$synapse_user_id."/nodes",
                                                    CURLOPT_RETURNTRANSFER => true,
                                                    CURLOPT_ENCODING       => "",
                                                    CURLOPT_MAXREDIRS      => 10,
                                                    CURLOPT_TIMEOUT        => 30,
                                                    CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
                                                    CURLOPT_CUSTOMREQUEST  => "POST",
                                                    //CURLOPT_POSTFIELDS     => "{\n  \"type\": \"ACH-US\",\n  \"info\": {\n    \"nickname\": \"$nickname\",\n    \"account_num\": \"$account_num\",\n    \"routing_num\": \"$routing_num\",\n    \"type\": \"$type\",\n    \"class\": \"$class\"\n  }\n}",
                                                    CURLOPT_POSTFIELDS     => "{\n  \"type\": \"ACH-US\",\n  \"info\": {\n    \"nickname\": \"$nickname\",\n    \"account_num\": \"$account_num\",\n    \"routing_num\": \"$routing_num\",\n    \"type\": \"$type\",\n    \"class\": \"$class\"\n  }\n}",
                                                    CURLOPT_HTTPHEADER     => array(
                                                                                    "cache-control: no-cache",
                                                                                    "content-type: application/json",
                                                                                    "x-sp-gateway: $SYNAPSE_CLIENT_ID|$SYNAPSE_CLIENT_SECRET",
                                                                                    "x-sp-user: ".$auth_key."|$SYNAPSE_FINGERPRINT",
                                                                                    "x-sp-user-ip: $SYNAPSE_FINGERPRINT"
                                                                                ),
                                                )
                                    );

                    $response = curl_exec($curl);
                    $err = curl_error($curl);
                    curl_close($curl);
                    //print_R($response); exit;
                    if ($err) {
                        $failure = true;
                        $error_message = 'Sory something wents wrong';
                    } else {
                        $json_decode = json_decode($response); 
                        if($json_decode->http_code == '200'){
                            $json_data = $json_decode->nodes;
                            //$this->updateSynapseLinkedAccounts($json_data);
                            $account_list_html = '';
                            if(count($json_data) >= 1){
                                $account_list_html .= '<div class="account_list_div">';
                                    $account_list_html .= '<form class="select_account_form" action="#" name="select_account_number_form" id="select_account_number_form">';
                                        $account_list_html .= '<div class="ajax_report alert display-hide" role="alert" style="margin-bottom: 0px !important; margin-left: 0px;position: relative; top: 0px;">
                                                                    <span class="close-message"></span>
                                                                    <div class="ajax_message">Saved</div>
                                                               </div>';
                                        for ($i=0; $i < count($json_data); $i++ ) {
                                            $account_number = $json_data[$i]->info->account_num;
                                            $class          = $json_data[$i]->info->class;
                                            $bank_name      = $json_data[$i]->info->bank_name;
                                            $oid            = $json_data[$i]->_id;
                                            $allowed        = $json_data[$i]->allowed;
                                                $account_list_html .= '<div class="account_list_row">';
                                                    if($i==0){
                                                        $account_list_html .= '<input checked="checked" type="radio" name="account" class="account" value="'.$i.'">';
                                                    }else{
                                                        $account_list_html .= '<input type="radio" name="account" class="account" value="'.$i.'">';
                                                    }
                                                    $account_list_html .= '<label class="aacc_label"></label>';
                                                    $account_list_html .= '<input type="hidden" name="oid_'.$i.'" class="select_oid" value="'.$oid.'">';
                                                    $account_list_html .= '<input type="hidden" name="account_number_'.$i.'" class="select_account_number" value="'.$account_number.'">';
                                                    $account_list_html .= '<input type="hidden" name="class_'.$i.'" class="select_class" value="'.$class.'">';
                                                    $account_list_html .= '<input type="hidden" name="bank_name_'.$i.'" class="select_bank_name" value="'.$bank_name.'">';
                                                    $account_list_html .= '<input type="hidden" name="allowed_'.$i.'" class="allowed" value="'.$allowed.'">';
                                                    $account_list_html .= '<input type="hidden" name="bank_code_'.$i.'" class="manually" value="other">';
                                                    $account_list_html .= '<span class="t_span">'.$bank_name.'</span>';
                                                    $account_list_html .= '<span class="s_span">'.$class.'</span>';
                                                    $account_list_html .= '<span class="f_span"> XXXX-XXXX-XXXX-'.$account_number.'</span>';
                                                $account_list_html .= '</div>';
                                        }
                                        $account_list_html .= '<div class="button_container">';
                                                $account_list_html .= '<input type="button" value="Submit" id="select_account_submit"  name="select_account_submit" class="select_account_submit">';
                                                $account_list_html .= '<input type="button" value="Cancel" id="select_account_cancel"  name="select_account_cancel" class="go_back_to_home select_account_cancel">';
                                        $account_list_html .= '</div>';   
                                    $account_list_html .= '</form>';
                                $account_list_html .= '</div>';                             
                            }else{
                                $account_list_html .= '<div class="account_list_div">There is no any account exist</div>';
                            }
                            $success_message = 'Please check the card list in bank account list';
                            $failure = false;
                        }elseif($json_decode->http_code == '202'){
                            //$json_error = $json_decode->error->en;
                            $account_list_html = '';
                            $account_list_html .= '<form class="mfa_form" action="#" name="mfa_form" id="mfa_form">';
                                $account_list_html .= '<div class="ajax_report alert display-hide" role="alert" style="margin-bottom: 0px !important; margin-left: 0px;position: relative; top: 0px;">
                                                            <span class="close-message"></span>
                                                            <div class="ajax_message">Saved</div>
                                                       </div>';     
                                $account_list_html .= '<div class="account_list_row">';                        
                                    $account_list_html .= '<p class="mfa_question_label">'.$json_decode->message.'</p>';
                                    $account_list_html .= '<input type="text" name="mfa_answer" id="mfa_answer" class="mfa_answer_txt_box" value="">';   
                                    $account_list_html .= '<input type="hidden" name="access_token" id="access_token" class="" value="'.$json_decode->mfa->access_token.'">';                                                                     
                                    $account_list_html .= '<div class="button_container">';
                                        $account_list_html .= '<input type="button" value="Submit" id="mfa_submit"  name="mfa_submit" class="mfa_submit">';
                                        $account_list_html .= '<input type="button" value="Cancel" id="mfa_cancel"  name="mfa_cancel" class="go_back_to_home mfa_cancel">';
                                    $account_list_html .= '</div>';   
                                $account_list_html .= '</div>';   
                            $account_list_html .= '</form>';

                            $failure = False;
                            $success_message = 'Question';
                        }else{
                            //$json_error = $json_decode->error->en;
                            $failure = true;
                            $error_message = 'Incorrect username or password';
                        }
                    }
                }else{
                    $failure = true;
                    $error_message = "Invalid field value supplied.";
                }
            }else{
                $bank_code = $this->input->post('bank_code');
                $bank_pw = $this->input->post('bank_pw');
                $bank_id = $this->input->post('bank_name');
                if($this->input->post('bank_code') != '' && $this->input->post('bank_pw') != '' && $this->input->post('bank_name')  != ''){
                    $synapse_user_detail    = $this->createSynapseUser($login_id);
                    $synapse_user_id        = $synapse_user_detail['synapse_user_id'];
        			$auth_key               = $synapse_user_detail['auth_key'];
                   /* print_R($synapse_user_detail);
                    print_r($this->input->post());
                    exit;*/
                    $curl = curl_init();
                    curl_setopt_array($curl, array(
                                                    CURLOPT_URL            => SYNAPSE_API_BASE_URL."users/".$synapse_user_id."/nodes",
                                                    CURLOPT_RETURNTRANSFER => true,
                                                    CURLOPT_ENCODING       => "",
                                                    CURLOPT_MAXREDIRS      => 10,
                                                    CURLOPT_TIMEOUT        => 30,
                                                    CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
                                                    CURLOPT_CUSTOMREQUEST  => "POST",
                                                    CURLOPT_POSTFIELDS     => "{\n    \"type\":\"ACH-US\",\n    \"info\":{\n      \"bank_id\":\"$bank_id\",\n      \"bank_pw\":\"$bank_pw\",\n      \"bank_name\":\"$bank_code\"\n    }\n}",
                                                    CURLOPT_HTTPHEADER     => array(
                                                                                    "cache-control: no-cache",
                                                                                    "content-type: application/json",                                                                       
                                                                                    "x-sp-gateway: $SYNAPSE_CLIENT_ID|$SYNAPSE_CLIENT_SECRET",
                                                                                    "x-sp-user: ".$auth_key."|$SYNAPSE_FINGERPRINT",
                                                                                    "x-sp-user-ip: $SYNAPSE_IP_ADDRESS"
                                                                                  ),
                                                )
                             );

                    $response = curl_exec($curl);
                    $err = curl_error($curl);
                    curl_close($curl);
                    if ($err) {
                       $failure = true;
                       $error_message = 'Sory something wents wrong';
                    } else {
                        $json_decode = json_decode($response);
                        if($json_decode->http_code == '200'){
                            $json_data = $json_decode->nodes;
                            $account_list_html = '';
                            if(count($json_data) >= 1){
                                $account_list_html .= '<div class="account_list_div">';
                                    $account_list_html .= '<form class="select_account_form" action="#" name="select_account_number_form" id="select_account_number_form">';
                                        $account_list_html .= '<div class="ajax_report alert display-hide" role="alert" style="margin-bottom: 10px; margin-left: 0px; width: 500px; position: relative; top: 0px;"><span class="close-message"></span><div class="ajax_message">Saved</div> </div>';
                                        for ($i=0; $i < count($json_data); $i++ ) {
                                            $account_number = $json_data[$i]->info->account_num;
                                            $class          = $json_data[$i]->info->class;
                                            $bank_name      = $json_data[$i]->info->bank_name;
                                            $oid            = $json_data[$i]->_id;
                                            $allowed        = $json_data[$i]->allowed;
                                                $account_list_html .= '<div class="account_list_row">';
                                                    if($i==0){
                                                        $account_list_html .= '<input checked="checked" type="radio" name="account" class="account" value="'.$i.'">';
                                                    }else{
                                                        $account_list_html .= '<input type="radio" name="account" class="account" value="'.$i.'">';
                                                    }
                                                    $account_list_html .= '<label class="aacc_label"></label>';
                                                    $account_list_html .= '<input type="hidden" name="oid_'.$i.'" class="select_oid" value="'.$oid.'">';
                                                    $account_list_html .= '<input type="hidden" name="account_number_'.$i.'" class="select_account_number" value="'.$account_number.'">';
                                                    $account_list_html .= '<input type="hidden" name="class_'.$i.'" class="select_class" value="'.$class.'">';
                                                    $account_list_html .= '<input type="hidden" name="bank_name_'.$i.'" class="select_bank_name" value="'.$bank_name.'">';
                                                    $account_list_html .= '<input type="hidden" name="allowed_'.$i.'" class="allowed" value="'.$allowed.'">';
                                                    $account_list_html .= '<span class="t_span">'.$bank_name.'</span>';
                                                    $account_list_html .= '<span class="s_span">'.$class.'</span>';
                                                    $account_list_html .= '<span class="f_span"> XXXX-XXXX-XXXX-'.$account_number.'</span>';
                                                $account_list_html .= '</div>';
                                        }
                                        $account_list_html .= '<div class="button_container">';
                                                $account_list_html .= '<input type="button" value="Submit" id="select_account_submit"  name="select_account_submit" class="select_account_submit">';
                                                $account_list_html .= '<input type="button" value="Cancel" id="select_account_cancel"  name="select_account_cancel" class="go_back_to_home select_account_cancel">';
                                        $account_list_html .= '</div>';                             
                                    $account_list_html .= '</form>';
                                $account_list_html .= '</div>';                             
                            }else{
                                $account_list_html .= '<div class="account_list_div">There is no any account exist</div>';
                            }
                            $failure = false;
                            $success_message = 'Please check the card list in bank account list';
                        }elseif($json_decode->http_code == '202'){
                            /***************** If 202 status code  will find then oprn MFA question form ***********************/
                            $account_list_html = '';
                            $account_list_html .= '<form class="mfa_form" action="#" name="mfa_form" id="mfa_form">';
                                $account_list_html .= '<div class="ajax_report alert display-hide" role="alert" style="margin-bottom: 0px !important; margin-left: 0px;position: relative; top: 0px;"><span class="close-message"></span><div class="ajax_message">Saved</div></div>';     
                                $account_list_html .= '<div class="account_list_row">';                        
                                    $account_list_html .= '<p class="mfa_question_label">'.$json_decode->mfa->message.'</p>';
                                    $account_list_html .= '<input type="text" name="mfa_answer" id="mfa_answer" class="mfa_answer_txt_box" value="">';                                                                    
                                    $account_list_html .= '<input type="hidden" name="access_token" id="access_token" class="" value="'.$json_decode->mfa->access_token.'">';                                                                    
                                    $account_list_html .= '<div class="button_container">';
                                        $account_list_html .= '<input type="button" value="Verify Account" id="mfa_submit"  name="mfa_submit" class="mfa_submit">';
                                        $account_list_html .= '<input type="button" value="Cancel" id="mfa_cancel"  name="mfa_cancel" class="go_back_to_home mfa_cancel">';
                                    $account_list_html .= '</div>';   
                                $account_list_html .= '</div>';   
                            $account_list_html .= '</form>';
                            $failure = False;
                            $success_message = 'Question';
                        }else{
                            //$json_error = $json_decode->error->en;
                            $failure = true;
                            $error_message = 'Incorrect username or password';
                        }
                    }
                }else{
                    $failure = true;
                    $error_message = "Incorrect username or password.";
                }
            }
        }elseif($this->input->post('mfa_answer')){
            $mfa_answer = $this->input->post('mfa_answer');
            $access_token = $this->input->post('access_token');
            $synapse_user_detail    = $this->createSynapseUser($login_id);
            $auth_key               = $synapse_user_detail['auth_key'];
        	$synapse_user_id               = $synapse_user_detail['synapse_user_id'];
                
            $curl = curl_init();
            curl_setopt_array($curl, array(
                                            CURLOPT_URL            => SYNAPSE_API_BASE_URL."users/".$synapse_user_id."/nodes",
                                            CURLOPT_RETURNTRANSFER => true,
                                            CURLOPT_ENCODING       => "",
                                            CURLOPT_MAXREDIRS      => 10,
                                            CURLOPT_TIMEOUT        => 30,
                                            CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
                                            CURLOPT_CUSTOMREQUEST  => "POST",
                                            CURLOPT_POSTFIELDS     => "{\n    \"access_token\":\"$access_token\",\n    \"mfa_answer\":\"$mfa_answer\"\n}",
                                            CURLOPT_HTTPHEADER     => array(
                                                                            "cache-control: no-cache",
                                                                            "content-type: application/json",                                                                       
                                                                            "x-sp-gateway: $SYNAPSE_CLIENT_ID|$SYNAPSE_CLIENT_SECRET",
                                                                            "x-sp-user: ".$auth_key."|$SYNAPSE_FINGERPRINT",
                                                                            "x-sp-user-ip: $SYNAPSE_IP_ADDRESS"
                                                                          ),
                                        )
                     );

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);
            if ($err) {
              $failure = true;
              $error_message = 'Sory something wents wrong';
            } else {
              //echo $response;
                $json_decode = json_decode($response);
                if($json_decode->http_code == '200'){
                    $json_data = $json_decode->nodes;
                    //$this->updateSynapseLinkedAccounts($json_data);
                    $account_list_html = '';
                    if(count($json_data) >= 1){
                        $account_list_html .= '<div class="account_list_div">';
                            $account_list_html .= '<form class="select_account_form" action="#" name="select_account_number_form" id="select_account_number_form">';
                                $account_list_html .= '<div class="ajax_report alert display-hide" role="alert" style="margin-bottom: 10px; margin-left: 0px; width: 500px; position: relative; top: 0px;">
                                            <span class="close-message"></span>
                                            <div class="ajax_message">Saved</div>
                                       </div>';
                                for ($i=0; $i < count($json_data); $i++ ) {
                                    $account_number = $json_data[$i]->info->account_num;
                                    $class          = $json_data[$i]->info->class;
                                    $bank_name      = $json_data[$i]->info->bank_name;
                                    $oid            = $json_data[$i]->_id;
                                        $account_list_html .= '<div class="account_list_row">';
                                            if($i==0){
                                                $account_list_html .= '<input checked="checked" type="radio" name="account" class="account" value="'.$i.'">';
                                            }else{
                                                $account_list_html .= '<input type="radio" name="account" class="account" value="'.$i.'">';
                                            }
                                            $account_list_html .= '<label class="aacc_label"></label>';
                                            $account_list_html .= '<input type="hidden" name="oid_'.$i.'" class="select_oid" value="'.$oid.'">';
                                            $account_list_html .= '<input type="hidden" name="account_number_'.$i.'" class="select_account_number" value="'.$account_number.'">';
                                            $account_list_html .= '<input type="hidden" name="class_'.$i.'" class="select_class" value="'.$class.'">';
                                            $account_list_html .= '<input type="hidden" name="bank_name_'.$i.'" class="select_bank_name" value="'.$bank_name.'">';
                                            $account_list_html .= '<span class="t_span">'.$bank_name.'</span>';
                                            $account_list_html .= '<span class="s_span">'.$class.'</span>';
                                            $account_list_html .= '<span class="f_span"> XXXX-XXXX-XXXX-'.$account_number.'</span>';
                                        $account_list_html .= '</div>';
                                }
                                $account_list_html .= '<div class="button_container">';
                                        $account_list_html .= '<input type="button" value="Submit" id="select_account_submit"  name="select_account_submit" class="select_account_submit">';
                                        $account_list_html .= '<input type="button" value="Cancel" id="select_account_cancel"  name="select_account_cancel" class="go_back_to_home select_account_cancel">';
                                $account_list_html .= '</div>';                             
                            $account_list_html .= '</form>';
                        $account_list_html .= '</div>';                             
                    }else{
                        $account_list_html .= '<div class="account_list_div">There is no any account exist</div>';
                    }
                    $failure = false;
                    $success_message = 'Please check the card list in bank account list';
                }elseif($json_decode->http_code == '202'){
                    //$json_error = $json_decode->error->en;
                    $account_list_html = '';
                    $account_list_html .= '<form class="mfa_form" action="#" name="mfa_form" id="mfa_form">';
                        $account_list_html .= '<div class="ajax_report alert display-hide" role="alert" style="margin-bottom: 0px !important; margin-left: 0px;position: relative; top: 0px;">
                                                    <span class="close-message"></span>
                                                    <div class="ajax_message">Saved</div>
                                               </div><br>';     
                        $account_list_html .= '<div class="account_list_row">';                        
                            $account_list_html .= '<p class="mfa_question_label">'.$json_decode->mfa->message.'</p>';
                            $account_list_html .= '<input type="text" name="mfa_answer" id="mfa_answer" class="mfa_answer_txt_box" value="">';        
                            $account_list_html .= '<input type="hidden" name="access_token" id="access_token" class="" value="'.$json_decode->mfa->access_token.'">';                                                              
                            $account_list_html .= '<div class="button_container">';
                                $account_list_html .= '<input type="button" value="Verify Account" id="mfa_submit"  name="mfa_submit" class="mfa_submit">';
                                $account_list_html .= '<input type="button" value="Cancel" id="mfa_cancel"  name="mfa_cancel" class="go_back_to_home mfa_cancel">';
                            $account_list_html .= '</div>';   
                        $account_list_html .= '</div>';   
                    $account_list_html .= '</form>';

                    $failure = False;
                    $success_message = 'Question';
                }else{
                    //$json_error = $json_decode->error->en;
                    $failure = true;
                    $error_message = 'Incorrect Answer';
                }
            }
        }else{
            $failure =true;
            $error_message = "Invalid field value supplied";

        }
                        //print_r($json_decode); exit;
        $json_decode = isset($json_decode) ? $json_decode : '';
        if ($this->input->is_ajax_request()) {
            if ($failure) {
                $data['success'] = false;
                $data['error_message'] = $error_message;
                $data['data'] = $json_decode;
            } else {
                $data['success'] = true;
                $data['resetForm'] = true;
                $data['success_message'] = $success_message;
                $data['data'] = $json_decode;
                $data['account_list_html'] = $account_list_html;
                //$data['url'] = site_url('add-property-step2');
            }
            $data['scrollToElement'] = true;
            echo json_encode($data);
            die;
        }
    }

    /**
     * Method Name : updateSynapseLinkedAccounts
     * Author Name : Lakhvinder Singh
     * Date : 02 March 2017
     * Description : Update synapse linked accounts
     */

    function updateSynapseLinkedAccounts(){
        //print_r($this->input->post()); exit; 
        if($this->input->post('oid_0')){
            $account        = $this->input->post('account');
            $account_number = $this->input->post('account_number_'.$account);
            $class          = $this->input->post('class_'.$account);
            $bank_name      = $this->input->post('bank_name_'.$account);
            $bank_name      = $this->input->post('bank_name_'.$account);
            $oid            = $this->input->post('oid_'.$account);
            $bank_code      = $this->input->post('bank_code_'.$account);
            $allowed        = $this->input->post('allowed_'.$account);

            $conditions = $params = array();
            $params['table'] = 'tbl_linked_account_numbers';
            $conditions['user_id'] = $this->session->userdata('MEM_ID');
            $conditions['account_number'] = $account_number;
            $conditions['oid'] = $oid;
            $params['single_row'] = true;
            $info = $this->common_model->get_data($conditions, $params);
            if(empty($info)){
                $insert_data = array(
                    'status'            => 0
                );
                $conditions = $params = array();
                $conditions['value'] = $insert_data;
                $conditions['where']['user_id'] = $this->session->userdata('MEM_ID');
                $params['table'] = 'tbl_linked_account_numbers';
                $this->common_model->update($conditions, $params);

                $insert_data = array(
                    'oid'               => $oid,
                    'user_id'           => $this->session->userdata('MEM_ID'),
                    'account_number'    => $account_number,
                    'bank_name'         => $bank_name,
                    'bank_code'         => $bank_code,
                    'synapse_micro_permission'         => $allowed,
                    'class'             => $class,
                    'status'            => 1,
                    'modified_date'     => current_date(),
                    'created_date'      => current_date()
                );
                $conditions = $params = array();
                $conditions = $insert_data;
                $params['table'] = 'tbl_linked_account_numbers';
                $last_id = $this->common_model->insert_data($conditions, $params);
                $html = '<option value="'.$last_id.'" selected="selected"> '.$bank_name.' , '. $class.' ,  xxxx-xxxx-xxxx-'.$account_number.' </option>';
                $typeis = 'add';
        
            }else{

                $insert_data = array(
                    'status'            => 0
                );
                $conditions = $params = array();
                $conditions['value'] = $insert_data;
                $conditions['where']['id'] = $this->session->userdata('MEM_ID');
                $params['table'] = 'tbl_linked_account_numbers';
                $this->common_model->update($conditions, $params);

                $insert_data = array(
                    'oid'               => $oid,
                    'class'             => $class,
                    'bank_name'         => $bank_name,
                    'synapse_micro_permission'         => $allowed,
                    'status'            => 1
                );
                $conditions = $params = array();
                $conditions['value'] = $insert_data;
                $conditions['where']['id'] = $info->id;
                $params['table'] = 'tbl_linked_account_numbers';
                $this->common_model->update($conditions, $params);
                $html = $info->id;
                $typeis = 'Update';
            }
            
            $failure = false;

        }else{
            $failure = true;
        }

        if ($this->input->is_ajax_request()) {
            if ($failure) {
                $data['success'] = false;
                $data['error_message'] = 'Server not responding';
            } else {
                $data['success'] = true;
                $data['resetForm'] = true;
                $data['success_message'] = 'You are successfully selected bank account';
                $data['html'] = $html;
                $data['typeis'] = $typeis;
                //$data['url'] = site_url('add-property-step2'); 
            }
            $data['scrollToElement'] = true;
            echo json_encode($data);
            die;
        }        
        
    }

    function createSynapsepayDepositAccount($user_id){
        /******************** START: Synapse deposit account ***************/
        $conditions = $params = array();
        $params['table'] = 'tbl_linked_account_numbers';
        $conditions['user_id'] = $user_id;
        $params['single_row'] = true;
        $info = $this->common_model->get_data($conditions, $params);

        if($info->synapse_deposit_account_id == ''){
	        $SYNAPSE_CLIENT_ID      = SYNAPSE_CLIENT_ID;
	        $SYNAPSE_CLIENT_SECRET  = SYNAPSE_CLIENT_SECRET; 
	        $synapse_data           = $this->gerUserSynaspeFingerprintAndIp($user_id);
	        $SYNAPSE_IP_ADDRESS     = $synapse_data->synapse_ip_address; 
	        $SYNAPSE_FINGERPRINT    = $synapse_data->synapse_fingerprint; 
	        $user_info              = $this->getUserInfo($user_id);
	        $nickname = $user_info->first_name. ' '.$user_info->last_name;

            $access_token = $this->input->post('access_token');
            $synapse_user_detail    = $this->createSynapseUser($user_id);
            $auth_key               = $synapse_user_detail['auth_key'];
            $synapse_user_id        = $synapse_user_detail['synapse_user_id'];
                
            $curl = curl_init();
            curl_setopt_array($curl, array(
                                            CURLOPT_URL            => SYNAPSE_API_BASE_URL."users/".$synapse_user_id."/nodes",
                                            CURLOPT_RETURNTRANSFER => true,
                                            CURLOPT_ENCODING       => "",
                                            CURLOPT_MAXREDIRS      => 10,
                                            CURLOPT_TIMEOUT        => 30,
                                            CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
                                            CURLOPT_CUSTOMREQUEST  => "POST",
                                            //CURLOPT_POSTFIELDS     => "{\n    \"access_token\":\"$access_token\",\n    \"mfa_answer\":\"$mfa_answer\"\n}",
                                            CURLOPT_POSTFIELDS => "{\n  \"type\": \"DEPOSIT-US\",\n  \"info\":{\n      \"nickname\":\"$nickname\"\n  }\n}",
                                            CURLOPT_HTTPHEADER     => array(
                                                                            "cache-control: no-cache",
                                                                            "content-type: application/json",                                                                       
                                                                            "x-sp-gateway: $SYNAPSE_CLIENT_ID|$SYNAPSE_CLIENT_SECRET",
                                                                            "x-sp-user: ".$auth_key."|$SYNAPSE_FINGERPRINT",
                                                                            "x-sp-user-ip: $SYNAPSE_IP_ADDRESS"
                                                                          ),
                                        )
                     );

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);
            //print_r($response); exit;
            if ($err) {
              //$failure = true;
              //$error_message = 'Sory something wents wrong';
            } else {
              //echo $response;
                $json_decode = json_decode($response);
                if($json_decode->http_code == '200'){
                    $json_data = $json_decode->nodes;
                    if(count($json_data) >= 1){
                        //for ($i=0; $i < count($json_data); $i++ ) {
                            $synapse_deposit_account_id = $json_data[0]->_id;
                        //}
                        $insert_data = array(
                            'synapse_deposit_account_id'         => $synapse_deposit_account_id
                        );
                        $conditions = $params = array();
                        $conditions['value'] = $insert_data;
                        $conditions['where']['user_id'] = $user_id;
                        $params['table'] = 'tbl_linked_account_numbers';
                        $this->common_model->update($conditions, $params);
                    }
                }
            }


        }
        /************************** END: synspae deposit account********************/
    }

    function check_micro_deposit(){
        $conditions = $params = array();
        $conditions['id'] = $this->input->post('id'); 
        $conditions['user_id'] = $this->session->userdata('MEM_ID');
        $params['table'] = 'tbl_linked_account_numbers';
        $params['single_row'] = true;
        $data_info = $this->common_model->get_data($conditions, $params);
        if(empty($data_info)){
            $html = 'not_exist';
        	$html_status = 'not_exist';
        }else{
            $conditions = $params = array();
            $conditions['where']['user_id'] = $this->session->userdata('MEM_ID');
            $conditions['value']['status'] = 0;
            $params['table'] = 'tbl_linked_account_numbers';
            $this->common_model->update($conditions, $params);

            $conditions = $params = array();
            $conditions['where']['id'] = $data_info->id; 
            $conditions['value']['status'] = 1;
            $params['table'] = 'tbl_linked_account_numbers';
            $this->common_model->update($conditions, $params);


        	if($data_info->bank_code == 'other' && $data_info->synapse_micro_permission != 'CREDIT-AND-DEBIT'){
		    	$html = '<div class="micro_deposit">';
                        $html .= '<input type="text" placeholder="Micro Amount 1" name="deposit1" id="deposit1">';
                        $html .= '<input type="text" placeholder="Micro Amount 2"  name="deposit2" id="deposit2">';
                        $html .= '<input type="button" class="btn" value="Update" name="update_micro_deposit" id="update_micro_deposit"> <a tooltip="Verify micro deposit" data-toggle="tooltip" href="#"><span class="glyphicon glyphicon-info-sign"></span></a>';
                    //$html .= '</form';
                $html .= '</div>';          
			    $html .= '<div class="" name="micro_deposit_message" id="micro_deposit_message"></div>';
                $html .= '<p class="micro_note">Please confirm the 2 microdeposits placed into your Bank Account.  Once confirmed, the account will be active. Microdeposits appear within 2 business days.</p>';
                $html_status = 'exist';
            }else{
                $html_status = 'verified';
        		$html = '';
        	}
        }
        $failure = false;

        if ($this->input->is_ajax_request()) {
            if ($failure) {
                $data['success'] = false;
                $data['error_message'] = 'Server not responding';
            } else {
                $data['success'] = true;
                $data['resetForm'] = true;
                $data['success_message'] = 'You are successfully selected bank account';
                $data['html'] = $html;
                $data['html_status'] = $html_status;
            }
            $data['scrollToElement'] = true;
            echo json_encode($data);
            die;
        }     
    	
    }

    function updateSynapseUserDetail(){
    	$login_id 				= $this->session->userdata('MEM_ID');
        $user_info              = $this->getUserInfo($login_id);
        
        $conditions = $params = array();
        $conditions['user_id'] = $this->session->userdata('MEM_ID');
        $params['table'] = 'tbl_linked_account_info';
        $params['single_row'] = true;
        $data_info = $this->common_model->get_data($conditions, $params);
        // print_r($user_info);
    	if(!empty($user_info->synapse_user_id) && !empty($data_info->kyc_id) && $data_info->kyc_permission != ''){

			$conditions = $params = array();
			$conditions['user_id'] = $this->session->userdata('MEM_ID');
			$params['table'] = 'tbl_linked_account_info';
			$params['single_row'] = true;
			$linked_info = $this->common_model->get_data($conditions, $params);
			if(isset($linked_info->kyc_permission) && $linked_info->kyc_permission =="UNVERIFIED"){

		    	$SYNAPSE_CLIENT_ID      = SYNAPSE_CLIENT_ID; 
		        $SYNAPSE_CLIENT_SECRET  = SYNAPSE_CLIENT_SECRET; 
		        $synapse_data           = $this->gerUserSynaspeFingerprintAndIp($login_id);
		        $SYNAPSE_IP_ADDRESS     = $synapse_data->synapse_ip_address; 
		        $SYNAPSE_FINGERPRINT    = $synapse_data->synapse_fingerprint;    
		        $synapse_user_detail    = $this->createSynapseUser($login_id);
				$auth_key               = $synapse_user_detail['auth_key'];
				$curl            = curl_init();
		        curl_setopt_array($curl, array(
		                                        CURLOPT_URL             => SYNAPSE_API_BASE_URL."users/".$user_info->synapse_user_id,
		                                        CURLOPT_RETURNTRANSFER  => true,
		                                        CURLOPT_ENCODING        => "",
		                                        CURLOPT_MAXREDIRS       => 10,
		                                        CURLOPT_TIMEOUT         => 30,
		                                        CURLOPT_HTTP_VERSION    => CURL_HTTP_VERSION_1_1,
		                                        CURLOPT_CUSTOMREQUEST   => "GET",
		                                        CURLOPT_HTTPHEADER      => array(
		                                                                            "cache-control: no-cache",
		                                                                            "content-type: application/json",
		                                                                            "x-sp-gateway: $SYNAPSE_CLIENT_ID|$SYNAPSE_CLIENT_SECRET",
		                                                                            "x-sp-user: |$SYNAPSE_FINGERPRINT",
		                                                                            "x-sp-user-ip: $SYNAPSE_IP_ADDRESS"
		                                                                        ),
		                                    )
		                        );

		        $response = curl_exec($curl);
		        $err = curl_error($curl);
		        curl_close($curl);
		        if ($err) {
		            return false;
		        } else {
		        	$synapse_doc = json_decode($response);
                    //print_r($synapse_doc); exit;
		        	$conditions = $params = array();
	                $update_data = array(
	                    'kyc_permission'    => $synapse_doc->permission,
	                    'physical_doc_status' =>$synapse_doc->doc_status->physical_doc,
	                    'virtual_doc_status' =>$synapse_doc->doc_status->virtual_doc,
	                    'kyc_id'    => $synapse_doc->documents[0]->id
	                );
	                //print_r($update_data); exit;
	                $conditions['where']['user_id'] = $this->session->userdata('MEM_ID');
	                $conditions['value'] = $update_data;
	                $params['table'] = 'tbl_linked_account_info';
	                $this->common_model->update($conditions, $params);
	    			return json_decode($response);
		        }

			}else{
            	return false;				
			}
    	}else{
            return false;
    	}
    }


    function synapseMicroDepositVerify(){ 
    	$conditions = $params = array();
        $conditions['id'] = $this->input->post('bank_account'); 
        $conditions['user_id'] = $this->session->userdata('MEM_ID');
        $params['table'] = 'tbl_linked_account_numbers';
        $params['single_row'] = true;
        $data_info = $this->common_model->get_data($conditions, $params);
        $login_id = $this->session->userdata('MEM_ID');
    	$response_array = array();
    	if(!empty($data_info)){
    		 $this->form_validation->set_message('required', '%s is required.');
            $this->form_validation->set_rules('deposit1', 'Micro Deposit 1', 'trim|required');
            $this->form_validation->set_rules('deposit2', 'Micro Deposit 2', 'trim|required');
            if ($this->form_validation->run()) {
            	$node_id = $data_info->oid;
		        $deposit1 = 1 * $this->input->post('deposit1');
		        $deposit2 = 1 * $this->input->post('deposit2');
		        $SYNAPSE_CLIENT_ID      = SYNAPSE_CLIENT_ID; 
		        $SYNAPSE_CLIENT_SECRET  = SYNAPSE_CLIENT_SECRET; 
		        $synapse_data           = $this->gerUserSynaspeFingerprintAndIp($login_id);
		        $SYNAPSE_IP_ADDRESS     = $synapse_data->synapse_ip_address; 
		        $SYNAPSE_FINGERPRINT    = $synapse_data->synapse_fingerprint;    
		        $synapse_user_detail    = $this->createSynapseUser($login_id);
                $auth_key               = $synapse_user_detail['auth_key'];
        		$synapse_user_id        = $synapse_user_detail['synapse_user_id'];
                $deposit_amount = '['.$deposit1.','.$deposit2.']';
		        $curl = curl_init();
		        curl_setopt_array($curl, array(
		            CURLOPT_URL 			=> SYNAPSE_API_BASE_URL."users/".$synapse_user_id."/nodes/".$node_id,
		            CURLOPT_RETURNTRANSFER  => true,
		            CURLOPT_ENCODING 		=> "",
		            CURLOPT_MAXREDIRS 		=> 10,
		            CURLOPT_TIMEOUT 		=> 30,
		            CURLOPT_HTTP_VERSION 	=> CURL_HTTP_VERSION_1_1,
		            CURLOPT_CUSTOMREQUEST 	=> "PATCH",
		            CURLOPT_POSTFIELDS 		=> "{\n    \"micro\":$deposit_amount\n}",
		            CURLOPT_HTTPHEADER 		=> array(
		                                            "cache-control: no-cache",
		                                                "content-type: application/json",
		                                                "x-sp-gateway: $SYNAPSE_CLIENT_ID|$SYNAPSE_CLIENT_SECRET",
		                                                "x-sp-user: ".$auth_key."|$SYNAPSE_FINGERPRINT",
		                                                "x-sp-user-ip: $SYNAPSE_FINGERPRINT"
		                                            ),
		        ));

		        $response = curl_exec($curl);
		        $err = curl_error($curl);
		        curl_close($curl);

		        if ($err) {
		            $notify_message = "curl error";
	                $failure = true;
		        } else {
		            $response_array = json_decode($response);
		            if(isset($response_array->_id)){
		                $allowed = $response_array->allowed;
	                    $failure = false;
	                }else{
                        if(isset($response_array->error_code) && $response_array->error_code == '404'){
                           $notify_message = 'Cannot be Found.';
                        }elseif(isset($response_array->error_code) && $response_array->error_code == '400' && $response_array->http_code == '409'){
                           $notify_message = 'Invalid micro deposit amount supplied.';
                        }elseif(isset($response_array->error_code) && $response_array->error_code == '200' && $response_array->http_code == '400'){
                           $notify_message = 'Error in Payload (Error in amount formatting).';
                        }else{
	                       $notify_message = $response_array->error->en;
                        }
	                    $failure = true;
	                }
	            }

            }else{
            	$notify_message = strip_tags(validation_errors());
                $failure = true;
            }    		
        }else{
            $notify_message = "post data empty";
            $failure = true;
    	}
        if($failure == false){
            $conditions = $params = array();
            $conditions['where']['id'] = $data_info->id;
            $conditions['value']['synapse_micro_permission'] = $allowed;
            $params['table'] = 'tbl_linked_account_numbers';
            $this->common_model->update($conditions, $params);
        }
        
         if ($this->input->is_ajax_request()) {
            if ($failure) {
                $data['success'] = false;
                $data['notify_message'] = $notify_message;
                $data['message'] = $response_array;
            } else {
                $data['success'] = true;
                $data['resetForm'] = true;
                $data['notify_message'] = 'You are successfully updated';
                $data['message'] = $response_array;
            }
            $data['scrollToElement'] = true;
            echo json_encode($data);
            die;
        }   

    }



    function getCommonQuestionByCategory($category_id){
          /*  $conditions = $params = array();
            $conditions['category_id'] = $category_id;
            $conditions['type'] = 1;
            $params['order_by'] = 'id desc';
            //$params['limit']    = 20;
            //$params['offset']   = 0;
            $params['table']    = 'tbl_questions';
            $question_info      = $this->common_model->get_data($conditions, $params); 
            //echo $this->db->last_query(); exit;
            $html = '';
            if(!empty($question_info)){
                foreach ($question_info as $key => $value) {
                    $html .= '<span>';
                        $html .= $value->question;
                    $html .= '</span>';
                }
            }else{
                $html .= '<span>';
                        $html .= "Question not exists";
                    $html .= '</span>';
            }*/
            $conditions = $params = array();
            $conditions['category_id'] = $category_id;
            $conditions['type'] = 1;
            $params['table'] = 'tbl_questions';
            $params['order_by'] = 'id desc';
            $question_info = $this->common_model->get_data($conditions, $params); 
            $html = '<div class="common_question_account_setup">';
            if(!empty($question_info)){
                foreach ($question_info as $key => $value) {
                    $html .= '<span>';
                    $html .= '<div class="cq_header">';
                        $html .= "<div class='header_q'> Q </div><div class='header_question'>".$value->question. "</div>";
                    $html .= '</div>';
                    $conditions = $params = array();
                    $conditions['question_id'] = $value->id;
                    $params['table'] = 'tbl_answers';
                    $params['order_by'] = 'id desc';
                    $answer_info = $this->common_model->get_data($conditions, $params); 
                    if(!empty($answer_info)){
                        $html .= '<div class="cq_content">';
                            $html .= '<ul>';
                                foreach ($answer_info as $a_key => $a_value) {
                                    $html .= '<li class="cm_row">';
                                        $html .= '<div class="header_a"> A</div><div class="header_answer">'.$a_value->answer.'</div>';
                                    $html .= '</li>';
                                }
                            $html .= '</ul>';
                        $html .= '</div>';

                    }else{
                        $html .= '<div class="cq_content">';
                            $html .= '<ul>';
                                    $html .= '<li class="cm_row">';
                                        $html .= '<div class="header_a"> </div><div class="header_answer">Answers not exist</div>';
                                    $html .= '</li>';
                            $html .= '</ul>';
                        $html .= '</div>';
                    }
                    $html .= '</span>';
                }
            }else{
                $html .= '<div class="cq_header">';
                        $html .= "<div class='header_q'> </div><div class='header_question'>Question not exists</div>";
                    $html .= '</div>';
            }
            $html .='</div>';
            return$html;
    }

    function testmail(){
        $this->load->model('member_model');
        $this->load->model('mailsending_model');
        $this->mailsending_model->testmailsend('lakhvinder.singh@xicom.biz','Test Message');
        echo "hi"; exit;
    }




}
