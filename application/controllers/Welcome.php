<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Welcome extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('country_model');
        $this->load->model('region_model');
        $this->load->model('city_model');
        $this->load->model('properties_model');
        $this->load->model('member_model');
        $this->load->model('common_model');
        $this->load->model('mailsending_model');

        /*         * *** Unset property id Session *** */
        $this->session->unset_userdata('PROPERTY_ID');
        if ($this->session->userdata('MEM_ID'))
            $this->common_model->getLastProeprty();
    }

    public function index() {
        $this->load->helper('cookie');
        $cookieData = get_cookie('remember');
//if users checked remember me option
        if ($cookieData != '') {
            $conditions = $params = array();
            $conditions['cookie_token'] = $cookieData;
            $params['cnt'] = true;
            $params['table'] = 'tbl_members';
            $login_check = $this->common_model->get_data($conditions, $params);
            if (!empty($login_check) && $login_check == 1) {
                $conditions = $params = array();
                $conditions['cookie_token'] = $cookieData;
                $params['table'] = 'tbl_members';
                $params['single_row'] = true;
                $login_check = $this->common_model->get_data($conditions, $params);
                $this->session->set_userdata('MEM_ID', $login_check->mem_id);
            }
        }
        if ($this->session->userdata('MEM_ID')) {
            $allProperties = $this->properties_model->frontGetAllPropertiesByLandlordId($this->session->userdata('MEM_ID'));
            // echo '<pre>';print_r($allProperties);die;
            if (!$allProperties) {
                redirect(site_url('add-property'));
            } else {
                redirect(site_url('dashboard'));
            }
        } else {
            $this->member_model->doLogout();
        }
        /************captcha &*******************/
        
    	$this->load->library('mathcaptcha');
        $config['question_format'] = 'numeric';
        $config['answer_format'] = 'numeric';
        $config['question_max_number_size'] = 10;
        $this->mathcaptcha->init($config);
        $output['math_captcha_question'] = $this->mathcaptcha->get_question();
        $output['allcountry'] = $this->country_model->getAllCountries();
        $output['regions'] = $this->region_model->getAllRegionsByCountryId(223);

        //echo $this->session->userdata('mathcaptcha_answer').'-aaaaaaaaaaaaaaaaaaaaa'; exit;
        $this->load->view($this->config->item('defaultfolder') . '/home_header', $output);
        $this->load->view($this->config->item('defaultfolder') . '/home');
        $this->load->view($this->config->item('defaultfolder') . '/auth_js');
        $this->load->view($this->config->item('defaultfolder') . '/home_footer');
    }


    public function myform()
    {
        $this->load->library('mathcaptcha');
        $config['question_format'] = 'numeric';
        $config['answer_format'] = 'numeric';
        $config['question_max_number_size'] = 10;
        $this->mathcaptcha->init($config);
        if($this->input->post('math_captcha')){
            //$data['math_captcha_question'] = $this->mathcaptcha->get_question();
            $this->form_validation->set_rules('math_captcha', 'Math CAPTCHA', 'required|callback__check_answer');

            if ($this->form_validation->run() == FALSE)
            {
                $data['math_captcha_question'] = $this->mathcaptcha->get_question();
                $this->load->view('myform', $data);
            }
            else
            {
                $data['math_captcha_question'] = $this->mathcaptcha->get_question();
                $this->load->view('myform', $data);
            }
        }else{
            $data['math_captcha_question'] = $this->mathcaptcha->get_question();
            $this->load->view('myform', $data);
        }
    }

    public function _check_answer($answer){
        if($this->session->userdata('mathcaptcha_answer') == $answer ){
            $this->form_validation->set_message('_check_answer', $this->session->userdata('mathcaptcha_answer').' = '.'Congrats.');
            return true;
        }else{
            $this->form_validation->set_message('_check_answer', $this->session->userdata('mathcaptcha_answer').' = '.$answer.' Please enter valid captcha.');
            return false;
        }
    }


    public function index1() {
        $this->load->helper('cookie');
        $cookieData = get_cookie('remember');
//if users checked remember me option
        if ($cookieData != '') {
            $conditions = $params = array();
            $conditions['cookie_token'] = $cookieData;
            $params['cnt'] = true;
            $params['table'] = 'tbl_members';
            $login_check = $this->common_model->get_data($conditions, $params);
            if (!empty($login_check) && $login_check == 1) {
                $conditions = $params = array();
                $conditions['cookie_token'] = $cookieData;
                $params['table'] = 'tbl_members';
                $params['single_row'] = true;
                $login_check = $this->common_model->get_data($conditions, $params);
                $this->session->set_userdata('MEM_ID', $login_check->mem_id);
            }
        }
        if ($this->session->userdata('MEM_ID')) {
            $allProperties = $this->properties_model->frontGetAllPropertiesByLandlordId($this->session->userdata('MEM_ID'));
            // echo '<pre>';print_r($allProperties);die;
            if (!$allProperties) {
                redirect(site_url('add-property'));
            } else {
                redirect(site_url('dashboard'));
            }
        } else {
            $this->member_model->doLogout();
        }
        $output['allcountry'] = $this->country_model->getAllCountries();
        $output['regions'] = $this->region_model->getAllRegionsByCountryId(223);
        $this->load->view($this->config->item('defaultfolder') . '/home_header1111', $output);
        $this->load->view($this->config->item('defaultfolder') . '/home1111');
        $this->load->view($this->config->item('defaultfolder') . '/auth_js1111');
        $this->load->view($this->config->item('defaultfolder') . '/footer1111');
    }

    function getStateByCountryId($con_id) {
        $country_code = $this->common_model->getSingleFieldFromAnyTable('prefix_code', 'id', $con_id, 'tbl_country');
        $regions = $this->region_model->getAllRegionsByCountryId($con_id);
        if ($regions) {
            $data['regions'] = $regions;
            $data['success'] = true;
        } else {
            $data['success'] = false;
        }
        $data['country_code'] = $country_code;
        echo json_encode($data);
        die;
    }

    function getCityByStateId($state_id) {
        $cities = $this->city_model->getAllCityByStateId($state_id);
        if ($cities) {
            $data['cities'] = $cities;
            $data['success'] = true;
        } else {
            $data['success'] = false;
        }
        echo json_encode($data);
        die;
    }

    public function terms() {
        $this->load->view($this->config->item('defaultfolder') . '/home_header');
        $this->load->view($this->config->item('defaultfolder') . '/terms_page');
        $this->load->view($this->config->item('defaultfolder') . '/footer');
    }

    public function dosubmit() {
        if (!empty($_POST)) {
            $failure = 0;
            $this->form_validation->set_rules('name', 'Name', 'trim|required');
            $this->form_validation->set_rules('mobile', 'Mobile', 'trim|required');
            $this->form_validation->set_rules('cont_email', 'Email', 'trim|required');
            if ($this->form_validation->run()) {
                
                $mathcaptcha_answer = $this->input->post('mathcaptcha_answer');
                $answer = $this->input->post('math_captcha');
                if($this->direct_check_answer($answer,$mathcaptcha_answer) == true){

                    $success_message = '<p>Thank you for contacting us. Get back to you soon</p>';
                    $conditions = $params = array();
                    $conditions['name'] = $this->input->post('name');
                    $conditions['email'] = $this->input->post('cont_email');
                    $conditions['mobile_no'] = $this->input->post('mobile');
                    $conditions['message'] = $this->input->post('message');
                    $conditions['reply_status'] = 'No';
                    $conditions['add_date'] = time();
                    $conditions['status'] = 'Active';
                    $conditions['ip'] = $_SERVER['REMOTE_ADDR'];
                    $params['table'] = 'tbl_contact_us';
                    $this->common_model->insert_data($conditions, $params);
                    $str = "Name:- ".$conditions['name']." <br/> Mobile:- ".$conditions['mobile_no']." <br/> Email:- ".$conditions['email']."<br/> Message:- ".$conditions['message'];
                    $this->load->helper('email');
                    $this->load->library('email');
                    $this->email->from($this->input->post('cont_email'), $conditions['name']);
                    $this->email->to("info@tenanttag.com");
                    $this->email->subject('Conatct Us');
                    $this->email->message($str);
                    $this->email->send();

                }else{
                    $error_message = 'Please enter valid captcha.';
                    $failure = true;
                }

            } else {
                $error_message = validation_errors();
                $failure = true;
            }
            if ($this->input->is_ajax_request()) {
                if (isset($failure) && $failure == TRUE) {
                    $data['success'] = false;
                    $data['error_message'] = $error_message;
                } else {
                    $data['success'] = true;
                    $data['resetform'] = true;
                    $data['success_message'] = $success_message;
                }
                $data['selfReload'] = true;
                echo json_encode($data);
                die;
            }
        }
    }

     public function direct_check_answer($answer,$mathcaptcha_answer){
        if($mathcaptcha_answer == $answer ){
            return true;
        }else{
            return false;
        }
    }
    public function checkEmailExists($email) {
        $conditions = $params = array();
        $params['table'] = 'tbl_subscribers';
        $conditions['email'] = $email;
        $conditions['status'] = '1';
        $params['single_row'] = TRUE;
        $check = $this->common_model->get_data($conditions, $params);
        return $check;
    }

    public function subscribe() {
        if (!empty($_POST)) {
            $failure = 0;
            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
            if ($this->form_validation->run()) {

                $check = $this->checkEmailExists($this->input->post('email'));
                if (empty($check)) {
                    $strnew2 = 'abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
                    $shuff = str_shuffle($strnew2);
                    $token = substr($shuff, 0, 8);
                    $success_message = '<p style="color:#000">Thank you for subscribing us.</p>';
                    $conditions = $params = array();
                    $conditions['email'] = $this->input->post('email');
                    $conditions['status'] = '1';
                    $conditions['date_created'] = date('Y-m-d H:i:s');
                    $conditions['token'] = $token;
                    $params['table'] = 'tbl_subscribers';
                    $this->common_model->insert_data($conditions, $params);
                } else {
                    $success_message = '<p style="color:#000">Already subscribed.</p>';
                }
            } else {
                $error_message = validation_errors();
                $failure = true;
            }
            if ($this->input->is_ajax_request()) {
                if (isset($failure) && $failure == TRUE) {
                    $data['success'] = false;
                    $data['error_message'] = $error_message;
                } else {
                    $data['success'] = true;
                    $data['resetform'] = true;
                    $data['success_message'] = $success_message;
                }
                //      $data['selfReload'] = true;
                echo json_encode($data);
                die;
            }
        }
    }

    function get_common_question(){
        if($this->input->post('id')){
            $question_id = $this->input->post('id');
            //$question_id = 8;
            $conditions = $params = array();
            $conditions['category_id'] = $question_id;
            $conditions['type'] = 1;
            $params['table'] = 'tbl_questions';
            $question_info = $this->common_model->get_data($conditions, $params); 
            $html = '';
            if(!empty($question_info)){
                foreach ($question_info as $key => $value) {
                    $html .= '<div class="cq_header">';
                        $html .= "<div class='header_q'> Q </div><div class='header_question'>".$value->question. "</div>";
                    $html .= '</div>';
                    $conditions = $params = array();
                    $conditions['question_id'] = $value->id;
                    $params['table'] = 'tbl_answers';
                    $params['order_by'] = 'id desc';
                    $answer_info = $this->common_model->get_data($conditions, $params); 
                    if(!empty($answer_info)){
                        $html .= '<div class="cq_content">';
                            $html .= '<ul>';
                                foreach ($answer_info as $a_key => $a_value) {
                                    $html .= '<li class="cm_row">';
                                        $html .= '<div class="header_a"> A</div><div class="header_answer">'.$a_value->answer.'</div>';
                                    $html .= '</li>';
                                }
                            $html .= '</ul>';
                        $html .= '</div>';

                    }else{
                        $html .= '<div class="cq_content">';
                            $html .= '<ul>';
                                    $html .= '<li class="cm_row">';
                                        $html .= '<div class="header_a"> </div><div class="header_answer">Answers not exist</div>';
                                    $html .= '</li>';
                            $html .= '</ul>';
                        $html .= '</div>';
                    }
                }
            }else{
                $html .= '<div class="cq_header">';
                        $html .= "<div class='header_q'> </div><div class='header_question'>Question not exists</div>";
                    $html .= '</div>';
            }
            $data['success'] = true; 
            $data['html'] = $html; 
            echo json_encode($data);
            exit;
        }
        exit;
    }

    function get_survey_question(){       

        $conditions = $params = array();
        $conditions['type'] = 2;
        $params['table'] = 'tbl_questions';
        $question_info = $this->common_model->get_data($conditions, $params); 
        //print_r($question_info); exit;
        $html = 'Question not found';
        $html = '<form class="survey_question_form" action="/update_survey_question" method="post">';
        $html .= '<div class="ajax_report alert display-hide" role="alert" style="float: left; width: 85%; padding: 5px; margin-top: 4px;">
                                        <span class="close close_custom_popup"></span>
                                        <span class="ajax_message" style="line-height:normal;margin: 0;padding: 10px; 5px 10px 5px"></span>
                                    </div>';
                $i=1;
            foreach ($question_info as $key => $value) {
                //print_r($value); exit;
                $selected_option = '';
                $survey_answer = $this->get_survey_answer_by_question_id($value->id,$this->session->userdata('MEM_ID'));
                $selected_option = isset($survey_answer->answer) ? $survey_answer->answer : '';
                $answer1 = (isset($survey_answer->answer1) && !empty($survey_answer->answer1)) ? 'checked' : '';
                $answer2 = (isset($survey_answer->answer2) && !empty($survey_answer->answer2)) ? 'checked' : '';
                $answer3 = (isset($survey_answer->answer3) && !empty($survey_answer->answer3)) ? 'checked' : '';
                $answer4 = (isset($survey_answer->answer4) && !empty($survey_answer->answer4)) ? 'checked' : '';
                $answer5 = (isset($survey_answer->answer5) && !empty($survey_answer->answer5)) ? 'checked' : '';
                $answer_type = $value->answer_type;
                $html .= '<div class="cq_content">';
                    $html .= "<b>".$i.'. '.$value->question. "</b>";
                        $html .= '<div class="cm_row">';
                        $html .= '<div class="cm_row">';
                            if($answer_type == 1){
                                if($selected_option == "yes"){
                                    $html .= '<p class="my-radio"><input checked type="radio"  id="1_'.$value->id.'" value="yes" name="'.$value->id.'[answer]"> <label for="1_'.$value->id.'">Yes</label></p>';          
                                    $html .= '<p class="my-radio"><input type="radio"  id="2_'.$value->id.'" value="no"  name="'.$value->id.'[answer]"> <label for="2_'.$value->id.'">No</label></p><br>';          
                                }elseif ($selected_option == "no") {
                                    $html .= '<p class="my-radio"><input type="radio"  id="1_'.$value->id.'" value="yes" name="'.$value->id.'[answer]"> <label for="1_'.$value->id.'">Yes</label></p>';          
                                    $html .= '<p class="my-radio"><input checked type="radio"  id="2_'.$value->id.'" value="no"  name="'.$value->id.'[answer]"> <label for="2_'.$value->id.'">No</label></p><br>';          
                                }else{
                                    $html .= '<p class="my-radio"><input type="radio"  id="1_'.$value->id.'" value="yes" name="'.$value->id.'[answer]"> <label for="1_'.$value->id.'">Yes</label></p>';          
                                    $html .= '<p class="my-radio"><input type="radio"  id="2_'.$value->id.'" value="no"  name="'.$value->id.'[answer]"> <label for="2_'.$value->id.'">No</label></p><br>';          
                                }
                            
                            }else{
                                if(!empty($value->answer1)){
                                    $html .= '<p class="my-radio"><input type="checkbox"  '.$answer1.'  id="1_'.$value->id.'" value="'.$value->answer1 .'" name="'.$value->id.'[answer1]"> <label for="1_'.$value->id.'">'.$value->answer1.'</label></p>';          
                                }
                                if(!empty($value->answer2)){
                                    $html .= '<p class="my-radio"><input type="checkbox"  '.$answer2.'  id="2_'.$value->id.'" value="'.$value->answer2 .'" name="'.$value->id.'[answer2]"> <label for="2_'.$value->id.'">'.$value->answer2.'</label></p>';          
                                }
                                if(!empty($value->answer3)){
                                    $html .= '<p class="my-radio"><input type="checkbox"  '.$answer3.'  id="3_'.$value->id.'" value="'.$value->answer3 .'" name="'.$value->id.'[answer3]"> <label for="3_'.$value->id.'">'.$value->answer3.'</label></p>';          
                                }
                                if(!empty($value->answer4)){
                                    $html .= '<p class="my-radio"><input type="checkbox"  '.$answer4.'  id="4_'.$value->id.'" value="'.$value->answer4 .'" name="'.$value->id.'[answer4]"> <label for="4_'.$value->id.'">'.$value->answer4.'</label></p>';          
                                }
                                if(!empty($value->answer5)){
                                    $html .= '<p class="my-radio"><input type="checkbox"  '.$answer5.'  id="5_'.$value->id.'" value="'.$value->answer5 .'" name="'.$value->id.'[answer5]"> <label for="5_'.$value->id.'">'.$value->answer5.'</label></p>';          
                                }

                            }
                        $html .= '</div>';
                $html .= '</div><br>';
                $i++;
            }
            $html .= '<input type="button"  name="survey_submit" class="btn-tenant" id="survey_submit" value="Submit" style="text-align:center"> ';          
                $html .= '</br></br>';
        $html .= '</form>';
        $data['success'] = true; 
        $data['html'] = $html; 
        echo json_encode($data);
        exit;
       
    }




    function get_survey_answer_by_question_id($question_id,$user_id){       

        $conditions = $params = array();
        $params['table'] = 'tbl_survey_answers';
        $params['single_row'] = true;
        $conditions['question_id'] = $question_id;
        $conditions['user_id'] = $user_id;
        $answer_info = $this->common_model->get_data($conditions, $params); 

        return $answer_info;
        

    }

    function update_survey_question(){ 
        if($this->input->post()){
            $data_Arr = $this->input->post();
            foreach ($data_Arr as $key => $value) {
                $answer = isset($value['answer']) ? $value['answer'] : '';
                $answer1 = isset($value['answer1']) ? $value['answer1'] : '';
                $answer2 = isset($value['answer2']) ? $value['answer2'] : '';
                $answer3 = isset($value['answer3']) ? $value['answer3'] : '';
                $answer4 = isset($value['answer4']) ? $value['answer4'] : '';
                $answer5 = isset($value['answer5']) ? $value['answer5'] : '';

                $conditions = $params = array();
                $conditions['id'] = $key;
                $params['table'] = 'tbl_questions';
                $params['single_row'] = true;
                $question_info = $this->common_model->get_data($conditions, $params); 
                if(!empty($question_info)){
                    if($question_info->answer_type == 1){
                        if($answer == ''){
                            // Error
                            $validation_message = 'Please select any options.';
                        }else{
                            $validation_message = '';
                        }
                    }else{
                        if($answer1 == '' && $answer2 == '' && $answer3 == '' && $answer4 == '' && $answer5 == ''){
                            $validation_message = 'Please select any options.';
                        }else{
                            $validation_message = '';
                        }
                    }
                }else{
                    $validation_message = 'Question not exist! Please try after some time.';
                }

                if($validation_message == ''){

                    $conditions = $params = array();
                    $conditions['question_id'] = $key;
                    $conditions['user_id'] = $this->session->userdata('MEM_ID');
                    $params['table'] = 'tbl_survey_answers';
                    $params['single_row'] = true;
                    $answer_info = $this->common_model->get_data($conditions, $params); 
                    //print_R($answer_info); exit;
                    if(!isset($answer_info->id)){
                        $conditions = $params = array();
                        $conditions['question_id'] = $key;
                        $conditions['answer'] = $answer;
                        $conditions['answer1'] = $answer1;
                        $conditions['answer2'] = $answer2;
                        $conditions['answer3'] = $answer3;
                        $conditions['answer4'] = $answer4;
                        $conditions['answer5'] = $answer5;
                        $conditions['user_id'] = $this->session->userdata('MEM_ID');
                        $conditions['modified_date'] = date('Y-m-d h:i:s');
                        $params['table'] = 'tbl_survey_answers';
                        $this->common_model->insert_data($conditions, $params);
                    }else{
                        $conditions = $params = array();
                        $conditions['value']['answer'] = $answer;
                        $conditions['value']['answer1'] = $answer1;
                        $conditions['value']['answer2'] = $answer2;
                        $conditions['value']['answer3'] = $answer3;
                        $conditions['value']['answer4'] = $answer4;
                        $conditions['value']['answer5'] = $answer5;
                        $conditions['value']['modified_date'] = date('Y-m-d h:i:s');
                        $conditions['where']['id'] = $answer_info->id;
                        $params['table'] = 'tbl_survey_answers';
                        $this->common_model->update($conditions, $params);
                    } 
                    $success = true;     
                    $success_message =   'You are successfully updated';        
                    $selfReload  = true;
                }else{
                    $success = false;              
                    $success_message = $validation_message;
                    $selfReload  = false;
                    $validation_failure = true;
                }
            }
        }else{
            $success_message = 'Please select any options.';
            $success = false;
            $selfReload = false;
            $validation_failure = true;
        }
        if ($this->input->is_ajax_request()) {
            
            $data['success'] = $success;
            $data['success_message'] = $success_message;
            $data['selfReload'] = $selfReload;
            $data['scrollToElement'] = true;
            echo json_encode($data);
            die;
        }
    }

    public function getTenantUser(){
        $params['table'] = 'tbl_members';
        $params['order_by'] = 'mem_id desc';
        $params['limit'] = '20';
        $params['fields'] = array('email','slug','tpass','user_type');
        $conditions['status'] = 'Active';
        $conditions['user_type'] = 'tenant';
        $conditions['move_status'] = 1;
        $Check = $this->common_model->get_data($conditions, $params);
        echo "<pre>";
        print_r($Check);

    }

    /**
     * Method Name : synapseWebookUrl
     * Author Name : Lakhvinder Singh
     * Date         : 19 March 2017
     * Description : synapseWebookUrl
     */

    function synapseWebookUrl($id=false){
        $json = file_get_contents('php://input');
        $action = json_decode($json, true);        
        $file = 'cron_synapse_response.txt';
        if (file_exists($file)) {
            $current = file_get_contents($file);
            $current .= $json . "\n";
            file_put_contents($file, $current);
        }
        if(isset($action['webhook_meta']['function']) && $action['webhook_meta']['function'] == 'USER|PATCH'){
            /********************* User Account Update *******************************************/
            $synapse_user_id        = $action['_id']['$oid'];
            $conditions = $params = array();
            $params['table'] = 'tbl_members';
            $params['single_row'] = true;
            $params['fields'] = array('mem_id','email','slug','tpass','user_type');
            $conditions['synapse_user_id'] = $synapse_user_id;
            $user_info = $this->common_model->get_data($conditions, $params);
            if(!empty($user_info)){
                $permission             = $action['permission'];
                $virtual_doc_status     = $action['doc_status']['virtual_doc'];
                $physical_doc_status    = $action['doc_status']['physical_doc'];
                $update_data = array(
                    'kyc_permission'    => $permission,
                    'physical_doc_status' =>$physical_doc_status,
                    'virtual_doc_status' =>$virtual_doc_status
                );
                $conditions = $params = array();
                $conditions['user_id'] = $user_info->mem_id;
                $params['table'] = 'tbl_linked_account_info';
                $params['single_row'] = true;
                $data_info = $this->common_model->get_data($conditions, $params);
                $message = 'Synapse User status: '.$permission;
                if($data_info->kyc_permission != $permission){
                    $this->mailsending_model->sendSynapseUserStatusEmail($user_info->email,$message);
                }
                $conditions = $params = array();
                $conditions['where']['user_id'] = $user_info->mem_id;
                $conditions['value'] = $update_data;
                $params['table'] = 'tbl_linked_account_info';
                $this->common_model->update($conditions, $params);
            }else{
                echo 'User not exist';
            }
        }elseif(isset($action['webhook_meta']['function']) && $action['webhook_meta']['function'] == 'NODE|PATCH'){
            /****************************** Bank account update *********************************/
            $oid        = $action['_id']['$oid'];
            $conditions = $params = array();
            $params['table'] = 'tbl_linked_account_numbers'; 
            $conditions['oid'] = $oid;
            $params['single_row'] = true;
            $info = $this->common_model->get_data($conditions, $params);
            if(!empty($info)){
                $synapse_micro_permission       = $action['allowed'];
                $update_data = array(
                    'synapse_micro_permission'          => $synapse_micro_permission
                );
                $conditions = $params = array();
                $conditions['value'] = $update_data;
                $conditions['where']['id'] = $info->id;
                $params['table'] = 'tbl_linked_account_numbers';
                $this->common_model->update($conditions, $params);

                $conditions = $params = array();
                $params['table'] = 'tbl_members';
                $params['single_row'] = true;
                $params['fields'] = array('mem_id','email','slug','tpass','user_type');
                $conditions['mem_id'] = $info->user_id;
                $user_info = $this->common_model->get_data($conditions, $params);
                $message = 'Synapse bank account( XXXX-XXXX-XXXX-'. $info->account_number. ') status: '.$synapse_micro_permission;
                $this->mailsending_model->sendSynapseBankAccountStatusEmail($user_info->email,$message);
                $this->createSynapsepayDepositAccount($info->user_id);
            }else{
                echo 'Bank Account not exist';
            }
        }elseif(isset($action['webhook_meta']['function']) && $action['webhook_meta']['function'] == 'TRAN|PATCH'){
            /******************************** Transaction Update  *****************************/
            $transaction_id = $action['_id']['$oid'];
            $conditions = $params = array();
            $params['table'] = 'tbl_payment';
            $conditions['transaction_id'] = $transaction_id;
            $params['single_row'] = true;
            $transaction_info = $this->common_model->get_data($conditions, $params);
            

            if(!empty($transaction_info)){
                $recent_status = $action['recent_status']['status'];
                
                if($recent_status == 'CANCELED' || $recent_status == 'CANCELLED'){
                    $status = 'CANCELED';
                }elseif($recent_status == 'SETTLED'){
                    $status = 'PAID';
                }else{
                    $status = $recent_status;
                }
                //if(isset($transaction_info->status) && $transaction_info->status != $status && $status != 'QUEUED-BY-SYNAPSE' && $status != 'QUEUED-BY-RECEIVER'){
                if(isset($transaction_info->status) && $transaction_info->status != $status){
                    $sent_mail = "Yes";
                }else{
                    $sent_mail = "No";
                }
                if(!isset($transaction_info->status)){
                    $sent_mail = 'Yes';
                }

                $update_data = array(
                    'status'        => $status,
                    'modified_date' => current_date()
                );
                $conditions = $params = array();
                $conditions['value'] = $update_data;
                $conditions['where']['id'] = $transaction_info->id;
                $params['table'] = 'tbl_payment';
                $this->common_model->update($conditions, $params);
                
                $conditions = $params = array();
                $params['table'] = 'tbl_members';
                $conditions['mem_id'] = $transaction_info->mem_id;
                $params['single_row'] = TRUE;
                $params['fields'] = array('mem_id', 'synapse_user_id','synapse_fingerprint','synapse_ip_address','stripe_customer_id','stripe_external_account_id', 'email', 'first_name', 'last_name', 'mobile_no', 'dob', 'app_verified', 'move_status','third_party_sharing');
                $user_detail = $this->common_model->get_data($conditions, $params);
                $user_name      = $user_detail->first_name.' '.$user_detail->last_name;
                $user_email     = $user_detail->email;
                $amount = $transaction_info->amount;
                $mail_message = 'Payment('.$amount.') update.';

                $conditions = $params = array();
                $params['table'] = 'tbl_synapsepay_card_info';
                $conditions['user_id'] = $transaction_info->mem_id;
                $conditions['account_number'] = $transaction_info->account_number;
                $conditions['is_deleted'] = 1;
                $params['single_row'] = TRUE;
                $bank_user_info = $this->common_model->get_data($conditions, $params);
                $bank_info = $bank_user_info->bank_name. ' '.$bank_user_info->account_number;

                if($sent_mail == 'Yes'){
                    $this->mailsending_model->sendTransactionMail($user_name, $user_email,'$ '.$amount, $status,$bank_info,$transaction_id);
                    $sms_response = $this->smsSendToUser($user_detail->mobile_no,'Rent($'.$amount.') transaction('.$transaction_id.') status update - '.$status); // send sms
                    /*********** START: Testing ***********************************/
                    $sms_response = $this->smsSendToUser('+919996528190','Rent($'.$amount.') transaction('.$transaction_id.') status update - '.$status.' - WelcomeController'); // send sms
                    $this->mailsending_model->sendTransactionMail($user_name.' WelcomeController ', 'lakhvinder.singh@xicom.biz','$ '.$amount, $status,$bank_info,$transaction_id);
                    /*********** End: Testing ***********************************/
                    echo "success";
                }
            }else{
                echo 'Transaction not exist';
            }
        }else{
            echo 'Transaction not exist';
        }

        //$result = $this->mailsending_model->testmailsend('lakhvinder.singh@xicom.biz',$json);

    }


    /* Method Name : gerUserSynaspeFingerprintAndIp
     * Author Name : Lakhvinder Singh
     * Date : 02 March 2017
     * Description : get synapse_fingerprint and ip address
     */   
    function gerUserSynaspeFingerprintAndIp($user_id){
        $conditions = $params = array();
        $params['table'] = 'tbl_members';
        $conditions['mem_id'] = $user_id;
        $params['single_row'] = TRUE;
        $params['fields'] = array('synapse_fingerprint','synapse_ip_address');
        $data = $this->common_model->get_data($conditions, $params);
        return $data;
    }



    /**
     * Method Name : getUserInfo
     * Author Name : LAkhvinder Singh
     * Description : get basic details of user
     */
    public function getUserInfo($user_id) {
        $conditions = $params = array();
        $params['table'] = 'tbl_members';
        $conditions['mem_id'] = $user_id;
        $params['single_row'] = TRUE;
        $params['fields'] = array('mem_id', 'synapse_user_id','synapse_fingerprint','stripe_customer_id', 'email', 'first_name', 'last_name', 'mobile_no', 'dob', 'app_verified', 'move_status','third_party_sharing');
        $info = $this->common_model->get_data($conditions, $params);
        return $info;
    }


    function createSynapsepayDepositAccount($user_id){
        /******************** START: Synapse deposit account ***************/
        $conditions = $params = array();
        $params['table'] = 'tbl_linked_account_numbers';
        $conditions['user_id'] = $user_id;
        $params['single_row'] = true;
        $info = $this->common_model->get_data($conditions, $params);

        if($info->synapse_deposit_account_id == ''){

            $SYNAPSE_CLIENT_ID      = SYNAPSE_CLIENT_ID;
            $SYNAPSE_CLIENT_SECRET  = SYNAPSE_CLIENT_SECRET; 
            $synapse_data           = $this->gerUserSynaspeFingerprintAndIp($user_id);
            $SYNAPSE_IP_ADDRESS     = $synapse_data->synapse_ip_address; 
            $SYNAPSE_FINGERPRINT    = $synapse_data->synapse_fingerprint; 
            $user_info              = $this->getUserInfo($user_id);
            $nickname = $user_info->first_name. ' '.$user_info->last_name;
            
            $access_token = $this->input->post('access_token');
            $synapse_user_detail    = $this->createSynapseUser($user_id);
            $auth_key               = $synapse_user_detail['auth_key'];
            $synapse_user_id        = $synapse_user_detail['synapse_user_id'];
                
            $curl = curl_init();
            curl_setopt_array($curl, array(
                                            CURLOPT_URL            => SYNAPSE_API_BASE_URL."users/".$synapse_user_id."/nodes",
                                            CURLOPT_RETURNTRANSFER => true,
                                            CURLOPT_ENCODING       => "",
                                            CURLOPT_MAXREDIRS      => 10,
                                            CURLOPT_TIMEOUT        => 30,
                                            CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
                                            CURLOPT_CUSTOMREQUEST  => "POST",
                                            //CURLOPT_POSTFIELDS     => "{\n    \"access_token\":\"$access_token\",\n    \"mfa_answer\":\"$mfa_answer\"\n}",
                                            CURLOPT_POSTFIELDS => "{\n  \"type\": \"DEPOSIT-US\",\n  \"info\":{\n      \"nickname\":\"$nickname\"\n  }\n}",
                                            CURLOPT_HTTPHEADER     => array(
                                                                            "cache-control: no-cache",
                                                                            "content-type: application/json",                                                                       
                                                                            "x-sp-gateway: $SYNAPSE_CLIENT_ID|$SYNAPSE_CLIENT_SECRET",
                                                                            "x-sp-user: ".$auth_key."|$SYNAPSE_FINGERPRINT",
                                                                            "x-sp-user-ip: $SYNAPSE_IP_ADDRESS"
                                                                          ),
                                        )
                     );

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);
            //print_r($response); exit;
            if ($err) {
              //$failure = true;
              //$error_message = 'Sory something wents wrong';
            } else {
              //echo $response;
                $json_decode = json_decode($response);
                if($json_decode->http_code == '200'){
                    $json_data = $json_decode->nodes;
                    if(count($json_data) >= 1){
                        //for ($i=0; $i < count($json_data); $i++ ) {
                            $synapse_deposit_account_id = $json_data[0]->_id;
                        //}
                        $insert_data = array(
                            'synapse_deposit_account_id'         => $synapse_deposit_account_id
                        );
                        $conditions = $params = array();
                        $conditions['value'] = $insert_data;
                        $conditions['where']['user_id'] = $user_id;
                        $params['table'] = 'tbl_linked_account_numbers';
                        $this->common_model->update($conditions, $params);
                    }
                }
            }


        }
        /************************** END: synspae deposit account********************/
    }

        /**
     * Method Name : createSynapseUser
     * Author Name : Lakhvinder Singh
     * Date : 02 March 2017
     * Description : Create synsapse user if not exist
     */
    function createSynapseUser($user_id){
        $login_id               = $user_id;
        $user_info              = $this->getUserInfo($login_id);
        $SYNAPSE_CLIENT_ID      = SYNAPSE_CLIENT_ID;
        $SYNAPSE_CLIENT_SECRET  = SYNAPSE_CLIENT_SECRET; 
        $synapse_data           = $this->gerUserSynaspeFingerprintAndIp($login_id);
        $SYNAPSE_IP_ADDRESS     = $synapse_data->synapse_ip_address; 
        $SYNAPSE_FINGERPRINT    = $synapse_data->synapse_fingerprint;         
        //print_r($synapse_data); print_r($SYNAPSE_CLIENT_ID);  
        if(empty($user_info->synapse_user_id)){
            $email      = $user_info->email;
            $mobile_no  = $user_info->mobile_no;
            $legal_name = $user_info->first_name.' '.$user_info->last_name;

            /************************** create user using curl *************************/
            $curl       = curl_init();
            curl_setopt_array($curl, array(
                                            CURLOPT_URL             => SYNAPSE_API_BASE_URL."users",
                                            CURLOPT_RETURNTRANSFER  => true,
                                            CURLOPT_ENCODING        => "",
                                            CURLOPT_MAXREDIRS       => 10,
                                            CURLOPT_TIMEOUT         => 30,
                                            CURLOPT_HTTP_VERSION    => CURL_HTTP_VERSION_1_1,
                                            CURLOPT_CUSTOMREQUEST   => "POST",
                                            CURLOPT_POSTFIELDS      => "{\n  \"logins\": [\n    {\n      \"email\": \"$email\"\n    }\n  ],\n  \"phone_numbers\": [\n    \"$mobile_no\"\n  ],\n  \"legal_names\": [\n    \"$legal_name\"\n  ],\n  \"extra\": {\n    \"note\": null,\n    \"is_business\": false,\n    \"cip_tag\":2\n  }\n}",
                                            CURLOPT_HTTPHEADER      => array(
                                                                                "cache-control: no-cache",
                                                                                "content-type: application/json",
                                                                                "x-sp-gateway: $SYNAPSE_CLIENT_ID|$SYNAPSE_CLIENT_SECRET",
                                                                                "x-sp-user: |$SYNAPSE_FINGERPRINT",
                                                                                "x-sp-user-ip: $SYNAPSE_IP_ADDRESS"
                                                                            ),
                                        )
                            );
            $response   = curl_exec($curl);
            $err        = curl_error($curl);
            curl_close($curl);
            //echo SYNAPSE_API_BASE_URL."users";
            //print_r($response); exit;
            if ($err) {
                return false;
            } else {
                $json_decode           = json_decode($response); // decode the "users's API Response
                //echo "<pre>".$mobile_no; print_r($json_decode); exit;
                $synapse_id            = $json_decode->_id; 
                $refresh_token         = $json_decode->refresh_token;
                /********************** update synapse user id in the table **************************/
                $conditions = $params = array();
                $conditions['value']['synapse_user_id'] = $synapse_id;
                $conditions['where']['mem_id']          = $login_id;
                $params['table'] = 'tbl_members';
                $this->common_model->update($conditions, $params);
                $data_result              = $this->getSynapseAuthToken($refresh_token,$synapse_id,$SYNAPSE_IP_ADDRESS,$SYNAPSE_FINGERPRINT); // get auth key using refresh token
                if($data_result['status'] == 'oauth_key'){
                    $auth_key = $data_result['data'];
                }else{
                    $auth_key = '';
                }
                $return = array('synapse_user_id' => $synapse_id, 'refresh_token' => $refresh_token,'auth_key' => $auth_key);
                return json_encode($return);
            }

        }else{
            $synapse_user_id = $user_info->synapse_user_id;
            $curl            = curl_init();
            curl_setopt_array($curl, array(
                                            CURLOPT_URL             => SYNAPSE_API_BASE_URL."users/".$synapse_user_id,
                                            CURLOPT_RETURNTRANSFER  => true,
                                            CURLOPT_ENCODING        => "",
                                            CURLOPT_MAXREDIRS       => 10,
                                            CURLOPT_TIMEOUT         => 30,
                                            CURLOPT_HTTP_VERSION    => CURL_HTTP_VERSION_1_1,
                                            CURLOPT_CUSTOMREQUEST   => "GET",
                                            CURLOPT_HTTPHEADER      => array(
                                                                                "cache-control: no-cache",
                                                                                "content-type: application/json",
                                                                                "x-sp-gateway: $SYNAPSE_CLIENT_ID|$SYNAPSE_CLIENT_SECRET",
                                                                                "x-sp-user: |$SYNAPSE_FINGERPRINT",
                                                                                "x-sp-user-ip: $SYNAPSE_IP_ADDRESS"
                                                                            ),
                                        )
                            );

            $response = curl_exec($curl);
            $err = curl_error($curl);
            curl_close($curl);
            if ($err) {
                return false;
            } else {
                $json_decode    = json_decode($response);
                $refresh_token  = $json_decode->refresh_token;
                $data_result       = $this->getSynapseAuthToken($refresh_token,$synapse_user_id,$SYNAPSE_IP_ADDRESS,$SYNAPSE_FINGERPRINT); // get auth key using refresh token
                if($data_result['status'] == 'oauth_key'){
                    $auth_key = $data_result['data'];
                }else{
                    if($data_result['status'] == 'validate_user'){
                        $phone_number = $data_result['data'][0];
                        $validate_user_text = $this->sendVerificationCode($phone_number);
                        $auth_key = ''; //////////////////////////////////////////////need to change this code
                        $return         = array('synapse_user_id' => $synapse_user_id, 'auth_key' => '','response_data'=>$data_result, 'refresh_token' => $refresh_token,'auth_key_status' => 'unverfied','phone_number'=>$phone_number);
                        return $return;
                    }else{
                        $auth_key = '';
                    }
                }
                $return         = array('synapse_user_id' => $synapse_user_id, 'response_data'=>$data_result,'refresh_token' => $refresh_token,'auth_key' => $auth_key);
                return $return;
            }
        }        
    }

    public function smsSendToUser($mobile_number,$msg){
        $this->load->helper('twilio_helper');  // references library/twilio_helper.php
        $service = get_twilio_service();
        //send sms     
        $i = 0;
        try {
            $mnumber = $mobile_number;
            $number = "+1" . preg_replace("/[^0-9]/", "", $mnumber);
            //$msg = "Transaction has been failed, due to insufficient balance in your account";
            $service->account->sms_messages->create("+19046743077", "$number", "$msg", array());
            $sms_status_message = 'SMS Sent';
            $i++;
        } catch (Exception $e) {
            $file = 'sms_error_logs.txt';
            if (file_exists($file)) {
                $current = file_get_contents($file);
                $current .= $e->getMessage() . "\n";
                file_put_contents($file, $current);
            }
            $sms_status_message = 'SMS failed'.$e->getMessage();
        }
        //echo $sms_status_message; exit;
        return $i;
    }

    function check(){

        $json = '{"client": {"name": "TenantTag", "id": "5956590e1ac0e700247569c2"}, "_rest": {"client": {"name": "TenantTag", "id": "5956590e1ac0e700247569c2"}, "phone_numbers": ["(463) 221-4082"], "photos": [], "documents": [], "_id": "59b8c3451a715d002ee16a05", "doc_status": {"virtual_doc": "MISSING|INVALID", "physical_doc": "MISSING|INVALID"}, "is_hidden": false, "logins": [{"email": "lucy@maiinator.com", "scope": "READ_AND_WRITE"}], "emails": [], "legal_names": ["lucy@maiinator.com"], "permission": "UNVERIFIED", "extra": {"extra_security": false, "is_business": false, "cip_tag": 1, "supp_id": null, "public_note": null, "last_updated": 1505281331371, "date_joined": 1505280836864}}, "phone_numbers": ["(463) 221-4082"], "photos": [], "documents": [], "_id": {"$oid": "59b8c3451a715d002ee16a05"}, "doc_status": {"virtual_doc": "MISSING|INVALID", "physical_doc": "MISSING|INVALID"}, "webhook_meta": {"function": "USER|PATCH", "updated_by": "SELF"}, "is_hidden": false, "logins": [{"email": "lucy@maiinator.com", "scope": "READ_AND_WRITE"}], "emails": [], "legal_names": ["lucy@maiinator.com"], "permission": "UNVERIFIED", "extra": {"extra_security": false, "is_business": false, "cip_tag": 1, "supp_id": null, "public_note": null, "last_updated": {"$date": 1505281331371}, "date_joined": {"$date": 1505280836864}}}';
        $action = json_decode($json, true);
       
        echo "<pre>"; 
        print_r($action['_id']['$oid']);
        print_r($action);
    }


}
