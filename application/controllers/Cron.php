<?php

/**
 * Cron Controller
 * Purpose : Manage call tracking API
 * @project CRM
 * @since 17 Aug 2015
 * @version Codeignitor 2.2.1
 * @author : Jasbir Singh
 */
require(APPPATH . 'libraries/synapsepay/init.php');
use SynapsePayRest\Client;
class Cron extends CI_Controller {

    public function __construct() {
        parent::__construct();
        //$this->load->model(array('common_model'));
        $this->load->model(array('common_model', 'mailsending_model'));
        ini_set('display_errors', 1);
        $this->load->model('cron_mail_Sending_model');
    }

    /**
     * Method Name : notify_tenants_via_sms
     * Author Name : Jasbir Singh
     * Description : Used For Sending Notification SMS to tenanats for Due Rent and Lease Expiration when hitted by cron job.
     */
    public function notify_tenants_via_sms() {

        $this->db->where('status', 'Active');
        $m = $this->db->get('tbl_messages');
        $messages = $m->result_array();

        $rentNotificaionIsactive = false;
        $LeaseNotificaionIsactive = false;

        $rentSMSbody = $leaseSMSbody = '';
        foreach ($messages as $message) {

            if ($message['slug'] == 'alert-message') {
                $rentNotificaionIsactive = true;
                $rentSMSbody = $message['description'];
                $rentSMSbody = str_ireplace('<p>', '', $rentSMSbody);
                $rentSMSbody = str_ireplace('</p>', '', $rentSMSbody);
            } else if ($message['slug'] == 'lease-alert-message') {

                $LeaseNotificaionIsactive = true;
                $leaseSMSbody = $message['description'];
                $leaseSMSbody = str_ireplace('<p>', '', $leaseSMSbody);
                $leaseSMSbody = str_ireplace('</p>', '', $leaseSMSbody);
            } else if ($message['slug'] == 'birthday-message') {

                $birthdaymessage = true;
                $birthdaySMSbody = $message['description'];
                $birthdaySMSbody = str_ireplace('<p>', '', $birthdaySMSbody);
                $birthdaySMSbody = str_ireplace('</p>', '', $birthdaySMSbody);
            } else {
                
            }
        }

        
        //echo '<pre>';print_r($birthday_info);die;

        $users = $this->common_model->get_tenants_list_for_notification();
        $this->load->helper('twilio_helper');
        $service = get_twilio_service();

        if ($rentNotificaionIsactive) {

            //Sent Rent Due SMS
            foreach ($users['rentusers'] as $tenant) {
                
                $subscription_status = $this->common_model->getPropertySubscriptionStatus($tenant['owner_id'],$tenant['property_id']);
                //print_r($subscription_status); exit;
                if($subscription_status == true){
                    
                    try {
                        $due_date = date('m/' . $tenant['due_date'] . '/Y');
                        $current = date('Y-m-d');
                        if (strtotime($due_date) < strtotime($current)) {
                            $due_on = "on " . date('m/' . $tenant['due_date'] . '/Y', strtotime("+1 month"));
                        } else {
                            $due_on = "on " . date('m/' . $tenant['due_date'] . '/Y');
                        }
                        if (date('j') + 1 == $tenant['due_date']) {
                            $rentSMS = str_replace("{ }", 'today', $rentSMSbody);
                        } else {
                            $rentSMS = str_replace("{ }", $due_on, $rentSMSbody);
                        }
                        //echo $rentSMS."<br/>";
                        $this->db->where('mem_id', $tenant['tenant_id']);
                        $tquery = $this->db->get('tbl_members');
                        $ret = $tquery->row();
                        $mobileno = '+1' . $ret->mobile_no;
                        $msg = $service->account->sms_messages->create("+19046743077", $mobileno, $rentSMS, array());
                        $this->message_log($rentSMS, $mobileno, $tenant['tenant_id'], $tenant['property_id'], $tenant['owner_id']);
                    } catch (Exception $e) {
                        $file = 'cron_sms_error_logs.txt';
                        if (file_exists($file)) {
                            $current = file_get_contents($file);
                            $current .= $e->getMessage() . "\n";
                            file_put_contents($file, $current);
                        }
                    }
                }
            }
        }

        if ($LeaseNotificaionIsactive) {
            //Sent Lease Expiration Notification SMS
            foreach ($users['leaseusers'] as $tenant) {
                try {
                    $due_date = "on " . date('m/d/Y', strtotime($tenant['lease_end_date']));
                    if (date('Y-m-d') == $tenant['lease_end_date']) {
                        $leaseSMS = str_replace("{}", 'today', $leaseSMSbody);
                    } else {
                        $leaseSMS = str_replace("{}", $due_date, $leaseSMSbody);
                    }
                    // echo $leaseSMS."<br/>";;
                    $this->db->where('mem_id', $tenant['tenant_id']);
                    $tquery = $this->db->get('tbl_members');
                    $ret = $tquery->row();
                    $mobileno = '+1' . $ret->mobile_no;
                    $msg = $service->account->sms_messages->create("+19046743077", $mobileno, $leaseSMS, array());
                    $this->message_log($leaseSMS, $mobileno, $tenant['tenant_id'], $tenant['property_id'], $tenant['owner_id']);
                } catch (Exception $e) {
                    $file = 'cron_sms_error_logs.txt';
                    if (file_exists($file)) {
                        $current = file_get_contents($file);
                        $current .= $e->getMessage() . "\n";
                        file_put_contents($file, $current);
                    }
                }
            }
        }


        $birthday_info = $this->common_model->get_tenants_birthday_list();
        foreach ($birthday_info as $key => $value) {
            $phone = preg_replace('/\D+/', '', $value['phone']);
            $phone = preg_replace('/\s+/', '', $phone);
            try {
                $mobileno = '+1' . $phone;
                $msg = $service->account->sms_messages->create("+19046743077", $mobileno, $birthdaySMSbody, array());
                $this->message_log($birthdaySMSbody, $mobileno, $value['user_id'], $value['user_id'], $value['user_id']);
               // echo 'yes';
            } catch (Exception $e) {
                $file = 'cron_sms_error_logs.txt';
                if (file_exists($file)) {
                    $current = file_get_contents($file);
                    $current .= $e->getMessage() . "\n";
                    file_put_contents($file, $current);
                }
                //echo 'no';
            }
        }

        echo 'Notification SMS Sent';
    }

    /**
     * Method Name : send_schedule_sms
     * Author Name : Jasbir Singh
     * Description : used for running crons to send schedule sms
     */
    public function send_schedule_sms() {
        $params = $conditions = array();
        $params['complex'] = true;
        $conditions['tables'] = array(
            'sms' => 'tbl_sms_schedule',
            'tenant' => 'tbl_members',
        );
        $conditions['table'] = 'sms';
        $conditions['on']['tenant'] = array(
            'sign' => '=',
            'column' => 'mem_id',
            'alias_column' => 'tenant_id',
            'alias_other' => 'sms',
        );
        $conditions['where']['scheduled_on']['alias'] = 'sms';
        $conditions['where']['scheduled_on']['value'] = array(
            'operator' => 'AND',
            'sign' => '<',
            'key' => 'scheduled_on',
            'value' => current_date()
        );
        $params['fields'] = array('sms.*', 'tenant.mobile_no');
        $msg = $this->common_model->get_data($conditions, $params);
        //echo '<pre>';print_r($conditions);die;
        if (!empty($msg)) {
            $this->load->helper('twilio_helper');  // references library/twilio_helper.php
            $service = get_twilio_service();
            foreach ($msg as $sms) {
                try {
                    $number = "+1" . preg_replace("/[^0-9]/", "", $sms->mobile_no);
                    $msgtext = $sms->message . " -" . SIGNATURE;

                    //send sms                    
                    if ($sms->end_repeat == 'infinte') {
                        // $service->account->sms_messages->create("+19046743077", "$number", "$msgtext", array());
                        $service->account->messages->create(array(
                            'To' => $number,
                            'From' => "+19046743077",
                            'Body' => $msgtext));
                        $this->message_log($msgtext, $number, $sms->tenant_id, $sms->property_id, $sms->owner_id);
                    } else {
                        if ($sms->occurences > 0) {
                            //   $service->account->sms_messages->create("+19046743077", "$number", "$msgtext", array());
                            $service->account->messages->create(array(
                                'To' => $number,
                                'From' => "+19046743077",
                                'Body' => $msgtext));
                            $this->message_log($msgtext, $number, $sms->tenant_id, $sms->property_id, $sms->owner_id);
                        }
                    }
                    //update dates                    
                    if ($sms->repeat_type == 'year') {
                        $newdate = date('Y-m-d H:i:s', strtotime('+1 year', strtotime($sms->scheduled_on)));
                    } elseif ($sms->repeat_type == 'month') {
                        $newdate = date('Y-m-' . $sms->repeat_every . ' H:i:s', strtotime('+1 month', strtotime($sms->scheduled_on)));
                    } elseif ($sms->repeat_type == 'quarterly') {
                        $newdate = date('Y-m-' . $sms->repeat_every . ' H:i:s', strtotime('+3 month', strtotime($sms->scheduled_on)));
                    } else {
                        $newdate = date('Y-m-d' . ' H:i:s', strtotime('+' . $sms->repeat_every . ' week', strtotime($sms->scheduled_on)));
                    }
                    $conditions = $params = array();
                    $conditions['where']['id'] = $sms->id;
                    $conditions['value']['scheduled_on'] = $newdate;
                    if ($sms->occurences > 0) {
                        $conditions['value']['occurences'] = $sms->occurences - 1;
                    }
                    $params['table'] = 'tbl_sms_schedule';
                    $this->common_model->update($conditions, $params);
                } catch (Exception $e) {
                    $file = 'cron_sms_error_logs.txt';
                    if (file_exists($file)) {
                        $current = file_get_contents($file);
                        $current .= $e->getMessage() . "\n";
                        file_put_contents($file, $current);
                    }
                }
            }
        }
        echo 'done';
        exit;
    }

    function sendPetMessage(){
        $this->load->helper('twilio_helper');
        $service = get_twilio_service();
        $this->db->where('status', 'Active');
        $m = $this->db->get('tbl_messages');
        $messages = $m->result_array();
        $petSMSbody = '';
        foreach ($messages as $message) {

            if ($message['slug'] == 'pet-message') {
                $petSMSbody = $message['description'];
                $petSMSbody = str_ireplace('<p>', '', $petSMSbody);
                $petSMSbody = str_ireplace('</p>', '', $petSMSbody);
            }  else {
                
            }
        }
        $pet_list = $this->common_model->get_tenants_pet_list();
        foreach ($pet_list as $key => $value) {
            $phone = preg_replace('/\D+/', '', $value['mobile_no']);
            $phone = preg_replace('/\s+/', '', $phone);
            try {
                $mobileno = '+1' . $phone;
                $msg = $service->account->sms_messages->create("+19046743077", $mobileno, $petSMSbody, array());
                $this->message_log($petSMSbody, $mobileno, $value['tenant_id'], $value['tenant_id'], $value['tenant_id']);
               // echo 'yes';
            } catch (Exception $e) {
                $file = 'cron_sms_error_logs.txt';
                if (file_exists($file)) {
                    $current = file_get_contents($file);
                    $current .= $e->getMessage() . "\n";
                    file_put_contents($file, $current);
                }
                //echo 'no';
            }
        }

        echo 'Notification SMS Sent';
    }

    public function message_log($msg, $number, $tenant, $property, $owner_id) {
        $conditions = $params = array();
        $conditions['message'] = $msg;
        $conditions['owner_id'] = $owner_id;
        $conditions['number'] = $number;
        $conditions['tenant_id'] = $tenant;
        $conditions['property_id'] = $property;
        $conditions['date_created'] = date('Y-m-d H:i:s');
        $params['table'] = 'tbl_sms_logs';
        $this->common_model->insert_data($conditions, $params);
    }

    /**
     * Method Name : send_schedule_sms
     * Author Name : Jasbir Singh
     * Description : used for running crons to send schedule sms
     */
    public function send_admin_schedule_sms() {
        $params = $conditions = array();
        $params['complex'] = true;
        $conditions['tables'] = array(
            'sms' => 'tbl_admin_schedule',
            'user' => 'tbl_members',
        );
        $conditions['table'] = 'sms';
        $conditions['on']['user'] = array(
            'sign' => '=',
            'column' => 'mem_id',
            'alias_column' => 'user_id',
            'alias_other' => 'sms',
        );
        $conditions['where']['scheduled_on']['alias'] = 'sms';
        $conditions['where']['scheduled_on']['value'] = array(
            'operator' => 'AND',
            'sign' => '<',
            'key' => 'scheduled_on',
            'value' => current_date()
        );
        $params['fields'] = array('sms.*', 'user.mobile_no');
        $msg = $this->common_model->get_data($conditions, $params);
        //echo '<pre>';print_r($conditions);die;
        if (!empty($msg)) {
            $this->load->helper('twilio_helper');  // references library/twilio_helper.php
            $service = get_twilio_service();
            foreach ($msg as $sms) {
                try {
                    $number = "+1" . preg_replace("/[^0-9]/", "", $sms->mobile_no);
                    $msgtext = $sms->message . " -" . SIGNATURE;
                    //send sms                    
                    if ($sms->end_repeat == 'infinte') {
                        //   $service->account->sms_messages->create("+19046743077", "$number", "$msgtext", array());
                        $service->account->messages->create(array(
                            'To' => $number,
                            'From' => "+19046743077",
                            'Body' => $msgtext));
                        $this->admin_message_log($msgtext, $number, $sms->user_id);
                    } else {
                        if ($sms->occurences > 0) {
                            //   $service->account->sms_messages->create("+19046743077", "$number", "$msgtext", array());
                            $service->account->messages->create(array(
                                'To' => $number,
                                'From' => "+19046743077",
                                'Body' => $msgtext));
                            $this->admin_message_log($msgtext, $number, $sms->user_id);
                        }
                    }
                    //update dates                    
                    if ($sms->repeat_type == 'year') {
                        $newdate = date('Y-m-d H:i:s', strtotime('+1 year', strtotime($sms->scheduled_on)));
                    } elseif ($sms->repeat_type == 'month') {
                        $newdate = date('Y-m-' . $sms->repeat_every . ' H:i:s', strtotime('+1 month', strtotime($sms->scheduled_on)));
                    } elseif ($sms->repeat_type == 'quarterly') {
                        $newdate = date('Y-m-' . $sms->repeat_every . ' H:i:s', strtotime('+3 month', strtotime($sms->scheduled_on)));
                    } else {
                        $newdate = date('Y-m-d' . ' H:i:s', strtotime('+' . $sms->repeat_every . ' week', strtotime($sms->scheduled_on)));
                    }
                    $conditions = $params = array();
                    $conditions['where']['id'] = $sms->id;
                    $conditions['value']['scheduled_on'] = $newdate;
                    if ($sms->occurences > 0) {
                        $conditions['value']['occurences'] = $sms->occurences - 1;
                    }
                    $params['table'] = 'tbl_admin_schedule';
                    $this->common_model->update($conditions, $params);
                } catch (Exception $e) {
                    $file = 'cron_sms_error_logs.txt';
                    if (file_exists($file)) {
                        $current = file_get_contents($file);
                        $current .= $e->getMessage() . "\n";
                        file_put_contents($file, $current);
                    }
                }
            }
        }
        echo 'done';
        exit;
    }

    public function admin_message_log($msg, $number, $user) {
        $conditions = $params = array();
        $conditions['message'] = $msg;
        $conditions['number'] = $number;
        $conditions['user_id'] = $user;
        $conditions['date_created'] = date('Y-m-d H:i:s');
        $params['table'] = 'tbl_admin_sms_logs';
        $this->common_model->insert_data($conditions, $params);
    }

    public function send_amenity_sms() {
        $month = date('n');
        $day = date('j');
        $params = $conditions = array();
        $params['complex'] = true;
        $conditions['tables'] = array(
            'dt' => 'tbl_sms_dates',
            'sms' => 'tbl_saved_sms',
        );
        $conditions['table'] = 'dt';
        $conditions['on']['sms'] = array(
            'sign' => '=',
            'column' => 'id',
            'alias_column' => 'saved_sms_id',
            'alias_other' => 'dt',
        );
        $conditions['where']['month']['alias'] = 'dt';
        $conditions['where']['month']['value'] = $month;
        $conditions['where']['day']['alias'] = 'dt';
        $conditions['where']['day']['value'] = $day;
        $conditions['where']['status']['alias'] = 'sms';
        $conditions['where']['status']['value'] = 'Active';
        //fetch any sms for today        
        $msg = $this->common_model->get_data($conditions, $params);
        
        if (!empty($msg)) {
        //fetch active tenants 
            $tenants = $this->fetchActiveTenants();
          //  echo '<pre>';print_r($tenants);die;
            if (!empty($tenants)) {
                foreach ($msg as $sms) {
                    foreach ($tenants as $tenant) {
                        // $check = true;
                        $check = $this->checkAmenityExists($tenant->property_id, $sms->amenity);
                        if ($check) {
                            if ($sms->amenity == 'heat' || $sms->amenity == 'ac') {
                                //   echo $check;
                                $sms_body = str_replace("{}", $check, "$sms->message");
                            } else {
                                $sms_body = $sms->message;
                            }
                            $data[] = (object) array(
                                        'prop' => $tenant->property_id,
                                        'user_id' => $tenant->tenant_id,
                                        'mobile_no' => $tenant->mobile_no,
                                        'owner_id' => $tenant->owner_id,
                                        'message' => $sms_body
                            );
                        }
                    }
                }
            }
        }

        if (!empty($data)) {
            $this->load->helper('twilio_helper');  // references library/twilio_helper.php
            $service = get_twilio_service();
            foreach ($data as $sms) {
                try {
                    $number = "+1" . preg_replace("/[^0-9]/", "", $sms->mobile_no);
                    $msgtext = $sms->message . " -" . SIGNATURE;
                    // $msgtext = $sms->message . " -" . SIGNATURE . ", Owner Name - " . $sms->landlord_name;
                    //send sms
                    $service->account->messages->sendMessage("+19046743077", "$number", "$msgtext", array());
                    $mail_text = $msgtext.'('.$sms->mobile_no. ')';
                    //$this->cron_mail_Sending_model->send_aminity_mail($mail_text);
                    $this->admin_message_log($msgtext, $number, $sms->user_id);
                    $this->message_log($msgtext, $number, $sms->user_id, $sms->prop, $sms->owner_id);
                } catch (Exception $e) {
                    $file = 'cron_sms_error_logs.txt';
                    //$this->cron_mail_Sending_model->send_aminity_mail($e->getMessage());
                    if (file_exists($file)) {
                        $current = file_get_contents($file);
                        $current .= $e->getMessage() . "\n";
                        file_put_contents($file, $current);
                    }
                }
            }
        }else{
            //$this->cron_mail_Sending_model->send_aminity_mail('Cron Working but no amenities exist.');
        }
        
        echo 'done';
        die;
    }

    public function fetchActiveTenants() {
        $params = $conditions = array();
        $params['complex'] = true;
        $conditions['tables'] = array(
            'l' => 'tbl_properties_lease_detail',
            'u' => 'tbl_members',
            'o' => 'tbl_members',
        );
        $conditions['table'] = 'l';
        $conditions['on']['u'] = array(
            'sign' => '=',
            'column' => 'mem_id',
            'alias_column' => 'tenant_id',
            'alias_other' => 'l',
            'join' => 'left'
        );
        $conditions['on']['o'] = array(
            'sign' => '=',
            'column' => 'mem_id',
            'alias_column' => 'owner_id',
            'alias_other' => 'l',
            'join' => 'left'
        );
        $conditions['where']['move_status']['alias'] = 'u';
        $conditions['where']['move_status']['value'] = '1'; //checkin status
        $conditions['where']['status']['alias'] = 'u';
        $conditions['where']['status']['value'] = 'Active'; //checkin status
        $params['fields'] = array('l.tenant_id', 'u.mobile_no', 'u.first_name', 'l.property_id', 'l.owner_id');
        $tenants = $this->common_model->get_data($conditions, $params);
        return $tenants;
    }

    public function checkAmenityExists($prop_id, $amen) {
        if (is_numeric($amen)) {
            $check = $this->common_model->checkAmenity($prop_id, $amen);
            return $check->cnt;
        } else {
            $match = 'N/A';
            if ($amen == 'smoke') {
                $col = 'safety_equipment1';
            } elseif ($amen == 'heat') {
                $col = 'heat_type';
            } elseif ($amen == 'ac') {
                $col = 'ac_type';
            } elseif ($amen == 'fireexten') {
                $col = 'safety_equipment2';
            } elseif ($amen == 'yard') {
                $col = 'yard';
                $match = 'no';
            }
            $check = $this->common_model->checkService($prop_id, $col, $match);
            if (isset($check->cnt)) {
                return $check->cnt;
            } else {
                return FALSE;
            }
        }
        return true;
    }

    public function current_time(){
        $this->load->helper('date');
        $datestring = "Year: %Y Month: %m Day: %d - %h:%i %a";
        $time = time();
        echo mdate($datestring, $time);
    }

    public function testxicom(){
        echo 'dsfsdf';
        $this->load->helper('twilio_helper');  // references library/twilio_helper.php
        $service = get_twilio_service();
            try {
                $msgtext = "It is easy to forget, but checking the location and condition of your fire extinguishers can give you piece of mind. The arrow on the guage should be in the green zone" . " -" . SIGNATURE;
                // $msgtext = $sms->message . " -" . SIGNATURE . ", Owner Name - " . $sms->landlord_name;
                //send sms
                $service->account->messages->sendMessage("+19046743077", "+919996864474", "$msgtext", array());                
            } catch (Exception $e) {              
                    echo $e->getMessage() . "\n";                   
            }
            
        echo 'done';
        die;
    }

    /*
      |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
      ||||||||||||||||||||||||||||||| START: Payment Recurring        |||||||||||||||||||||||||||||||||||||||
      |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
    */

    /**
     * Method Name : getUserInfo
     * Author Name : Jasbir Singh
     * Description : get basic details of user
     */
    public function getUserInfo($user_id) {
        $conditions = $params = array();
        $params['table'] = 'tbl_members';
        $conditions['mem_id'] = $user_id;
        $params['single_row'] = TRUE;
        $params['fields'] = array('mem_id', 'synapse_user_id','synapse_fingerprint','synapse_ip_address','stripe_external_account_id','stripe_customer_id', 'email', 'first_name', 'user_type','last_name', 'mobile_no', 'dob', 'app_verified', 'move_status','third_party_sharing');
        $info = $this->common_model->get_data($conditions, $params);
        return $info;
    }


        /**
     * Method Name : propertyDetails
     * Author Name : Jasbir Singh
     * Description : return details of particular property
     */
    public function propertyDetails($tenant_id) {
        $conditions = $params = array();
        $params['complex'] = true;
        $params['single_row'] = true;
        $conditions['tables'] = array(
            'lease' => 'tbl_properties_lease_detail',
            'property' => 'tbl_properties',
            'state' => 'tbl_region'
        );
        $conditions['table'] = 'lease';
        $conditions['on']['property'] = array(
            'sign' => '=',
            'column' => 'prop_id',
            'alias_column' => 'property_id',
            'alias_other' => 'lease',
        );
        $conditions['on']['state'] = array(
            'sign' => '=',
            'column' => 'region_id',
            'alias_column' => 'state_id',
            'alias_other' => 'property',
        );
        $conditions['where']['tenant_id']['alias'] = 'lease';
        $conditions['where']['tenant_id']['value'] = $tenant_id;

        $params['single_row'] = TRUE;
        $params['fields'] = array('property.*', 'state.region_name', 'lease.lease_start_date', 'lease.lease_end_date', 'lease.rent_amount');
        $info = $this->common_model->get_data($conditions, $params);
        return $info;
    }

    public function disabled_card_delete(){
        $current_day =  date("d");
        $current_timestamp = time();
        $previous_month_timestamp =  strtotime("-1 month", $current_timestamp); 
        $conditions = $params = array();
        $params['table']        = 'tbl_stripe_card_info';
        $conditions['disable_card']       = 1;
        $conditions['created'] = array(
                                        'operator' => 'AND',
                                        'sign' => '<=',
                                        'key' => 'created',
                                        'value' => $previous_month_timestamp
                                    );
        $data_info = $this->common_model->get_data($conditions, $params);        
        foreach ($data_info as $key => $value) {
            $conditions_u = $params_u = array();
            $params_u['table']        = 'tbl_properties';
            $conditions_u['where']['prop_id']  = $value->property_id;
            $conditions_u['value']['step_completed']       = 3;
            $this->common_model->update($conditions_u, $params_u);

            $conditions_d = $params_d = array();
            $params_d['table']        = 'tbl_stripe_card_info';
            $conditions_d['id']  = $value->id;
            $this->common_model->delete($conditions_d, $params_d);
        }
    }





    public function payByStripeRecurring(){
        $this->disabled_card_delete(); //  delete disabled card if complete subscription period
        $total_pending = 0;
        $total_completed = 0;
        $total_sms_sent = 0;
        $total_sms_not_sent = 0;
        $current_timestamp = time();
        $previous_month_timestamp =  strtotime("-1 month", $current_timestamp);
        $previous_month_timestamp =  $current_timestamp;
        $current_day =  date("d"); 
        $conditions = $params = array();
        $params['table'] = 'tbl_stripe_card_info';
        $conditions['recurring'] = 1;
        $conditions['is_deleted'] = 0;
        $conditions['disable_card']       = 0;
        $conditions['status_by_landlord']       = 1;
        $conditions['created'] = array(
                                        'operator' => 'AND',
                                        'sign' => '<=',
                                        'key' => 'created',
                                        'value' => $previous_month_timestamp
                                    );
        $recurring_info = $this->common_model->get_data($conditions, $params);
        /*echo "<br>".strtotime("+1 month");
        echo "<br>".date('m/d/Y', 1516432787);
        echo "<br>".time(); 
        echo "<br>".date('m/d/Y', 1513754387);
        exit;*/
        //echo $this->db->last_query(); exit;
        //echo date('Y-m-d'); echo "<pre>"; print_r($recurring_info); exit;
        require_once(FCPATH.'application/libraries/vendor/autoload.php');
        $stripe = array(
            "secret_key"      => STRIPE_SECRET_KEY,
            "publishable_key" => STRIPE_PULBLISHABLE_KEY
        );
        \Stripe\Stripe::setApiKey($stripe['secret_key']);
        $total = 0;
        foreach ($recurring_info as $key => $value) {
            if(checkTrialTimePeriod($value->user_id) == false){
                if($value->payment_type == 'Monthly'){
                    $userStripeAccount = $this->getUserInfo($value->user_id);
                   // print_r($userStripeAccount);
                    if(isset($userStripeAccount->user_type) && $userStripeAccount->user_type == 'landlord'){
                        //echo "<pre>"; print_r($value); exit;
                        $amount = PER_PROPERTY_AMOUNT;
                        $amount = str_replace(".","",amount_formated($amount));
                        try {
                            $charge = \Stripe\Charge::create(array(
                                            'amount'   => $amount,
                                            'currency' => 'usd',
                                            'customer' => $userStripeAccount->stripe_customer_id,
                                            'description' => 'Rent paid',
                                            'card' => $value->card_id,
                                        ));
                            $charge_id = $charge->id; 

                            
                            $insert_data = array(
                                'mem_id'        => $value->user_id,
                                'property_id'   => $value->property_id,
                                'transaction_id'=> $charge_id,
                                'amount'        => $charge->amount/100,
                                'status'        => 'PAID',     
                                'description'   => 'Property Subscription charges',
                                'modified_date' => current_date(),
                                'created_date'  => current_date(),
                                'date'  => $charge->created,
                                'user_type' => 'Landlord',
                                'payment_type' => 'Stripe',
                            );
                            $conditions = $params = array();
                            $conditions = $insert_data;
                            $params['table'] = 'tbl_payment';
                            $t_id = $this->common_model->insert_data($conditions, $params);

                            $conditions = $params = array();
                            $conditions['where']['id'] = $value->id;
                            $conditions['value']['created'] = strtotime("+1 month");
                            $params['table'] = 'tbl_stripe_card_info';
                            $this->common_model->update($conditions, $params);
  
                            $total_completed++;
                            $sms_response = $this->smsSendToUser($userStripeAccount->mobile_no,'Property Subscription charges succesfully completed'); // send sms
                            if($sms_response == 0){
                                $total_sms_not_sent++;
                            }else{
                                $total_sms_sent++;
                            }
                            /*****************************************Mail Send *****************************/
                            $user_name      = $userStripeAccount->first_name.' '.$userStripeAccount->last_name;
                            $user_email     = $userStripeAccount->email;
                            $mail_message = 'Property Subscription charges('.$insert_data['amount'] .') has been paid successfully';
                            $this->mailsending_model->sentPaymentInfo($user_name, $user_email, $mail_message);
                        }  catch (Exception $e) {
                            $message = $e->getMessage();
                            $total_pending++;            
                            $sms_response = $this->smsSendToUser($userStripeAccount->mobile_no,'Property Subscription charges has been failed'); // send sms
                            if($sms_response == 0){
                                $total_sms_not_sent++;
                            }else{
                                $total_sms_sent++;
                            }
                            /*****************************************Mail Send *****************************/
                            $user_name      = $userStripeAccount->first_name.' '.$userStripeAccount->last_name;
                            $user_email     = $userStripeAccount->email;
                            $mail_message = 'Property Subscription charges has been failed.';
                            $this->mailsending_model->sentPaymentInfo($user_name, $user_email, $mail_message);
                        }
                        
                    }
                }
            }
        }
        echo 'Total Completed Transactions:- '. $total_completed;
        echo '<br> Total Pending Transactions:- '. $total_pending;
        echo '<br> Total SMS Sent:- '. $total_sms_sent;
        echo '<br> Total SMS not sent:- '. $total_sms_not_sent;
    }

    public function payBySynapseRecurring(){
        
        //echo date('t'); exit;
        //insert into sms log 
        $SYNAPSE_CLIENT_ID      = SYNAPSE_CLIENT_ID;
        $SYNAPSE_CLIENT_SECRET  = SYNAPSE_CLIENT_SECRET; 
        $current_day =  date("j");  //j - The day of the month without leading zeros (1 to 31)
        $month_end_date = date('t');
        $day_arr = array();
        if($month_end_date == $current_day){
            for ($cur_ini=$current_day; $cur_ini <= 31; $cur_ini++) { 
                $day_arr[] = $cur_ini;
            }
        }else{
            $day_arr[] = $current_day;
        }

        //$this->createSynapsepayTransaction('5a165f7d321f48002eb547a4',"5a166218fc0a980043c2c718","HTC-HTC One X-c71231cc45c34a58-GOOG",$SYNAPSE_CLIENT_ID,$SYNAPSE_CLIENT_SECRET,"192.168.1.24","test","10","USD");
        //exit;
        $conditions = $params = array();
        $params['table'] = 'tbl_synapsepay_card_info';
        $conditions['recurring'] = 1;
        $conditions['is_deleted'] = 1;  //0 deleted, 1 not
        //$conditions['days'] = $current_day;  //current_day
        $conditions['days']  = array(
                                        'operator' => 'IN',
                                        'key' => 'days',
                                        'value' => $day_arr
                                    );
        $conditions['times'] = array(
                                        'operator' => 'AND',
                                        'sign' => '>=',
                                        'key' => 'times',
                                        'value' => 1
                                    );
        $recurring_info = $this->common_model->get_data($conditions, $params);
        //echo $this->db->last_query(); //exit;
        //echo $current_day."<pre>"; print_r($recurring_info); exit;
        $total_pending = 0;
        $total_completed = 0;
        $total_sms_sent = 0;
        $total_sms_not_sent = 0;
        foreach ($recurring_info as $key => $value) {
            //if(checkTrialTimePeriod($value->user_id) == false){
                if($value->days == $current_day ){                   // Check current day of the month is equal to the recurring days
                    if($value->payment_type == 'Monthly'){
                        $tenantStripeAccount = $this->getUserInfo($value->user_id);
                        //print_r($tenantStripeAccount); exit;
                        $property_details = $this->propertyDetails($value->user_id);   // get property detail by tenant id 
                        $ownerStripeAccount = $this->getUserInfo($property_details->owner_id); // get owner stripe account id using owner id
                        $amount         = $value->amount; 
                        $user_id        = $value->synapse_user_id; # optionals
                        $node_id        = $value->node_id; # optionals

                        /******************** get landlord synapse node id ****************************************/
                        $SynapseDetails = $this->getSynapseDetails($property_details->owner_id);
                        $landlord_synapse_deposit_account_id = isset($SynapseDetails->synapse_deposit_account_id) ? $SynapseDetails->synapse_deposit_account_id : ''; # optionals
                        /******************** get landlord synapse node id ****************************************/
                        if($landlord_synapse_deposit_account_id != ''){
                            $SYNAPSE_FINGERPRINT    = $tenantStripeAccount->synapse_fingerprint;
                            $SYNAPSE_IP_ADDRESS     = $tenantStripeAccount->synapse_ip_address;
                            $currency               = 'USD';
                            $note                   = "PAID";
                            $data                   = $this->createSynapsepayTransaction($user_id,$node_id,$SYNAPSE_FINGERPRINT,$SYNAPSE_CLIENT_ID,$SYNAPSE_CLIENT_SECRET,$SYNAPSE_IP_ADDRESS,$note,$amount,$currency,$landlord_synapse_deposit_account_id);
                            //print_r($data); exit();
                            if(isset($data['_id'])){
                                $status = $data['recent_status']['status'];
                                $insert_data = array(
                                    'mem_id'        => $value->user_id,
                                    'property_id'   => $property_details->prop_id,
                                    'transaction_id'=> $data['_id'],
                                    'amount'        => $amount,
                                    'description'   => 'Synapse auto rent paid',
                                    'routing_number'=> $value->routing_number,
                                    'account_number'=> $value->account_number,
                                    'status'        => $status,
                                    'user_type'     => 'Tenant',
                                    'payment_type'  => 'Synapsepay',
                                    'date'          => $data['recent_status']['date'],
                                    'modified_date' => current_date(),
                                    'created_date'  => current_date(),
                                );
                                $conditions = $params = array();
                                $conditions = $insert_data;
                                $params['table'] = 'tbl_payment';
                                $this->common_model->insert_data($conditions, $params);


                                $conditions = $params = array();
                                $conditions['where']['id'] = $value->id;
                                $conditions['value']['times'] = $value->times - 1;
                                $conditions['value']['modified_date'] = date('Y-m-d H:i:s');
                                $params['table'] = 'tbl_synapsepay_card_info';
                                $this->common_model->update($conditions, $params);  
                                $total_completed++;
                                $sms_response = $this->smsSendToUser($tenantStripeAccount->mobile_no,'Rent($'.$amount .')Transaction('.$data['_id'].') created successfully'); // send sms
                                if($sms_response == 0){
                                    $total_sms_not_sent++;
                                }else{
                                    $total_sms_sent++;
                                }
                                /*****************************************Mail Send *****************************/
                                $user_name      = $tenantStripeAccount->first_name.' '.$tenantStripeAccount->last_name;
                                $user_email     = $tenantStripeAccount->email;
                               /* $mail_message = 'Rent($'.$amount .') transaction('.$data['_id'].') has been created successfully';
                                $this->mailsending_model->sentPaymentInfo($user_name, $user_email, $mail_message);*/                                
                                $bank_info = $value->bank_name. ' '.$value->account_number;
                                $this->mailsending_model->sendTransactionMail($user_name, $user_email,'$ '.$amount, $status,$bank_info,$data['_id']);
                                
                                /*********** START: Testing ***********************************/
                                $sms_response = $this->smsSendToUser('+919996528190','Rent($'.$amount .')Transaction('.$data['_id'].') created successfully-Cron'); // send sms
                                $this->mailsending_model->sendTransactionMail($user_name.' Cron ', 'lakhvinder.singh@xicom.biz','$ '.$amount, $status,$bank_info,$data['_id']);
                                /*********** End: Testing ***********************************/

                            }elseif($data == 'transaction_failed'){
                                $total_pending++;
                                $sms_response = $this->smsSendToUser($tenantStripeAccount->mobile_no,'Auto payment ($'.$amount.') has been failed.'); // send sms
                                if($sms_response == 0){
                                    $total_sms_not_sent++;
                                }else{
                                    $total_sms_sent++;
                                }
                                /*****************************************Mail Send *****************************/
                                $user_name      = $tenantStripeAccount->first_name.' '.$tenantStripeAccount->last_name;
                                $user_email     = $tenantStripeAccount->email;
                                //$mail_message = 'Transaction has been failed.';
                                //$this->mailsending_model->sentPaymentInfo($user_name, $user_email, $mail_message);
                                $bank_info = $value->bank_name. ' '.$value->account_number;
                                $this->mailsending_model->sendTransactionMail($user_name, $user_email,'$ '.$amount, 'Failed',$bank_info,'Not available');

                                /*********** START: Testing ***********************************/
                                $sms_response = $this->smsSendToUser('+919996528190','Auto payment ($'.$amount.') has been failed.-Cron'); // send sms
                                $this->mailsending_model->sendTransactionMail($user_name.' Cron ', 'lakhvinder.singh@xicom.biz','$ '.$amount, 'Failed',$bank_info,'Not available');
                                /*********** End: Testing ***********************************/
                            }

                        }else{
                            $total_pending++;
                            $sms_response = $this->smsSendToUser($tenantStripeAccount->mobile_no,'Auto payment ($'.$amount.') has been failed.'); // send sms
                            if($sms_response == 0){
                                $total_sms_not_sent++;
                            }else{
                                $total_sms_sent++;
                            }
                            /*****************************************Mail Send *****************************/
                            $user_name      = $tenantStripeAccount->first_name.' '.$tenantStripeAccount->last_name;
                            $user_email     = $tenantStripeAccount->email;
                            /*$mail_message = 'Transaction has been failed.';
                            $this->mailsending_model->sentPaymentInfo($user_name, $user_email, $mail_message);*/
                            $bank_info = $value->bank_name. ' '.$value->account_number;
                            $this->mailsending_model->sendTransactionMail($user_name, $user_email,'$ '.$amount, 'Failed',$bank_info,'Not available');

                            /*********** START: Testing ***********************************/
                            $sms_response = $this->smsSendToUser('+919996528190','Auto payment ($'.$amount.') has been failed.-Cron'); // send sms
                            $this->mailsending_model->sendTransactionMail($user_name.' Cron ', 'lakhvinder.singh@xicom.biz','$ '.$amount, 'Failed',$bank_info,'Not available');
                            /*********** End: Testing ***********************************/


                        }
                    }
                }
            //}
        }
        echo 'Total Completed Transactions:- '. $total_completed;
        echo '<br> Total Pending Transactions:- '. $total_pending;
        echo '<br> Total SMS Sent:- '. $total_sms_sent;
        echo '<br> Total SMS not sent:- '. $total_sms_not_sent;
        /*$user_id = '58a146e184f2d0001e02964b'; # optionals
        $node_id = '58a149037e0887001fd6a5e8';
        $fingerprint = 'HTC-HTC One X-e7921e54c8e05686-GOOG';
        $client_id = 'id-c83d6205-785a-4776-b4de-8584aebc045d';
        $client_secret = 'secret-076f2412-69cf-46b1-a304-ad27e62f38f3';
        $ip_address = '192.1.1.1';
        $note = "Deposit to bank account";
        $amount = 10.10;
        $currency = 'USD';
        echo "<pre>";
        print_r($data);
        exit;*/
    }

    function test2ndTransaction(){
        $user_id = '59e955abdd9e5c002fb81546';
        $node_id = '59f77c5b12e17a0032b46b47';
        $landlord_synapse_deposit_account_id = '5a6174eb81f50b004917cee5';
        $SYNAPSE_FINGERPRINT = 'ef6d881027920fa839713acfc4de1c78';
        $SYNAPSE_CLIENT_ID      = SYNAPSE_CLIENT_ID;
        $SYNAPSE_CLIENT_SECRET  = SYNAPSE_CLIENT_SECRET; 
        $SYNAPSE_IP_ADDRESS  = '108.249.146.42'; 
        $note = 'Deposit to landlord';
        $amount = 7;
        $currency = 'USD';
        //$this->createSynapsepayTransaction($user_id,$node_id,$SYNAPSE_FINGERPRINT,$SYNAPSE_CLIENT_ID,$SYNAPSE_CLIENT_SECRET,$SYNAPSE_IP_ADDRESS,$note,$amount,$currency,$landlord_synapse_deposit_account_id)
        
        $options = array(
            'oauth_key'         => '', # Optional,
            'fingerprint'       => $SYNAPSE_FINGERPRINT,
            'client_id'         => $SYNAPSE_CLIENT_ID,
            'client_secret'     => $SYNAPSE_CLIENT_SECRET,
            'development_mode'  => false, # true will ping sandbox.synapsepay.com
            'ip_address'        => $SYNAPSE_IP_ADDRESS
        );

        $id_tenantag = '5a15a20de552da002bcee8cc';
        $client = new Client($options, $user_id); // $user_id optional
        /**************** Create user ***************************************/
        $create_response = $client->user->create($options);
        $user = $client->user->get($user_id);
        //print_r($create_response); //exit;
        /**************** Create oauth_key by refresh_token ***************************************/
        if(isset($user['refresh_token'])){
            $refresh_payload = array('refresh_token' => $user['refresh_token']); 

            $refresh_response = $client->user->refresh($refresh_payload);

            /**************** Create transaction  ***************************************/
            $oauth_key = $refresh_response['oauth_key'];
            $options = array(
                'oauth_key'         => $oauth_key, # Optional,
                'fingerprint'       => $SYNAPSE_FINGERPRINT,
                'client_id'         => $SYNAPSE_CLIENT_ID,
                'client_secret'     => $SYNAPSE_CLIENT_SECRET,
                'development_mode'  => false, # true will ping sandbox.synapsepay.com
                'ip_address'        => $SYNAPSE_IP_ADDRESS
            );
            $client_auth = new Client($options, $user_id); // $user_id optional
            $node = $client_auth->node->get($landlord_synapse_deposit_account_id);
            //echo "<pre>"; print_r($node); exit;
            $node_amount = $node['info']['balance']['amount'];
            //$node_currency = $nodes['info']['balance']['currency'];
            //echo $node_amount .'<='. $amount."<pre>"; print_R($node_amount); exit;
            /*************Check amount exist or not in user account ****************************/
            //if($node_amount >= $amount){
                $trans_payload = array(
                    "to" => array(
                        "type" => "ACH-US",
                        "id" => $node_id
                    ),
                    "amount" => array(
                        "amount" => $amount,
                        "currency" => $currency
                    ),
                    "extra" => array(
                        "note" => $note,
                        "process_on" => 1,
                        "ip" => $SYNAPSE_IP_ADDRESS  

                    ),
                    "fees" => array(array(
                                    "fee"=> -0.10,
                                    "note"=> "Facilitator Fee",
                                    "to"=> array(
                                        "id" => $id_tenantag
                                        ) 
                                )                       
                                )                       
                );
                $create_response = $client_auth->trans->create($landlord_synapse_deposit_account_id,$trans_payload);
                print_r($create_response); exit;

            //}else{
               // $create_response = 'amount_not_exist';
            //}
        }else{
            $create_response = 'transaction_failed';
            
        }
        print_r($create_response); exit;
    }

        /**
     * Method Name : propertyDetails
     * Author Name : Jasbir Singh
     * Description : return details of particular property
     */
    public function getSynapseDetails($landlord_id) {
        $conditions = $params = array();
        $conditions['user_id'] = $landlord_id;
        $conditions['status'] = 1;
        $conditions['is_deleted'] = 0;
        $params['single_row'] = TRUE;
        $params['table'] = 'tbl_linked_account_numbers';
        $info = $this->common_model->get_data($conditions, $params);
        if(!empty($info)){
            if($info->bank_code == 'other'){
                if($info->synapse_micro_permission == 'CREDIT-AND-DEBIT'){
                    $data = $info;
                }else{
                    $data = array();
                }
            }else{
                $data = $info;
            }
        }
       // echo $this->db->last_query(); //exit;
        return $info;
    }

    public function createSynapsepayTransaction($user_id,$node_id,$SYNAPSE_FINGERPRINT,$SYNAPSE_CLIENT_ID,$SYNAPSE_CLIENT_SECRET,$SYNAPSE_IP_ADDRESS,$note,$amount,$currency,$landlord_synapse_deposit_account_id){    
        $options = array(
            'oauth_key'         => '', # Optional,
            'fingerprint'       => $SYNAPSE_FINGERPRINT,
            'client_id'         => $SYNAPSE_CLIENT_ID,
            'client_secret'     => $SYNAPSE_CLIENT_SECRET,
            'development_mode'  => false, # true will ping sandbox.synapsepay.com
            'ip_address'        => $SYNAPSE_IP_ADDRESS
        );

        $id_tenantag = '5a15a20de552da002bcee8cc';
        $client = new Client($options, $user_id); // $user_id optional
        /**************** Create user ***************************************/
        $create_response = $client->user->create($options);
        $user = $client->user->get($user_id);
        //print_r($create_response); //exit;
        /**************** Create oauth_key by refresh_token ***************************************/
        if(isset($user['refresh_token'])){
            $refresh_payload = array('refresh_token' => $user['refresh_token']); 

            $refresh_response = $client->user->refresh($refresh_payload);

            /**************** Create transaction  ***************************************/
            $oauth_key = $refresh_response['oauth_key'];
            $options = array(
                'oauth_key'         => $oauth_key, # Optional,
                'fingerprint'       => $SYNAPSE_FINGERPRINT,
                'client_id'         => $SYNAPSE_CLIENT_ID,
                'client_secret'     => $SYNAPSE_CLIENT_SECRET,
                'development_mode'  => false, # true will ping sandbox.synapsepay.com
                'ip_address'        => $SYNAPSE_IP_ADDRESS
            );
            $client_auth = new Client($options, $user_id); // $user_id optional
            $node = $client_auth->node->get($node_id);
            //echo "<pre>"; print_r($node); exit;
            $node_amount = $node['info']['balance']['amount'];
            //$node_currency = $nodes['info']['balance']['currency'];
            //echo $node_amount .'<='. $amount."<pre>"; print_R($node_amount); exit;
            /*************Check amount exist or not in user account ****************************/
            //if($node_amount >= $amount){
                $trans_payload = array(
                    "to" => array(
                        "type" => "ACH-US",
                        "id" => $landlord_synapse_deposit_account_id
                    ),
                    "amount" => array(
                        "amount" => $amount,
                        "currency" => $currency
                    ),
                    "extra" => array(
                        "note" => $note,
                        "process_on" => 1,
                        "ip" => $SYNAPSE_IP_ADDRESS  

                    ),
                    "fees" => array(array(
                                    "fee"=> -0.10,
                                    "note"=> "Facilitator Fee",
                                    "to"=> array(
                                        "id" => $id_tenantag
                                        ) 
                                )                       
                                )                       
                );
                $create_response = $client_auth->trans->create($node_id,$trans_payload);
                ///print_r($create_response); exit;

            //}else{
               // $create_response = 'amount_not_exist';
            //}
        }else{
            $create_response = 'transaction_failed';
            
        }
        return $create_response;
    }

    public function smsSendToUser($mobile_number,$msg){
        $this->load->helper('twilio_helper');  // references library/twilio_helper.php
        $service = get_twilio_service();
        //send sms     
        $i = 0;
        try {
            $mnumber = $mobile_number;
            $number = "+1" . preg_replace("/[^0-9]/", "", $mnumber);
            //$msg = "Transaction has been failed, due to insufficient balance in your account";
            $service->account->sms_messages->create("+19046743077", "$number", "$msg", array());
            $sms_status_message = 'SMS Sent';
            $i++;
        } catch (Exception $e) {
            $file = 'sms_error_logs.txt';
            if (file_exists($file)) {
                $current = file_get_contents($file);
                $current .= $e->getMessage() . "\n";
                file_put_contents($file, $current);
            }
            $sms_status_message = 'SMS failed'.$e->getMessage();
        }
        //echo $sms_status_message; exit;
        return $i;
    }

    /*
      |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
      |||||||||||||||||||||||||||||||   END: Payment Recurring        |||||||||||||||||||||||||||||||||||||||
      |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
    */


}
