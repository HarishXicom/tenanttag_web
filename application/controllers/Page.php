<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Page extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('country_model');
        $this->load->model('region_model');
        $this->load->model('city_model');
        $this->load->model('properties_model');
        $this->load->model('member_model');
        $this->load->model('common_model');
        /*         * *** Unset property id Session *** */
        $this->session->unset_userdata('PROPERTY_ID');
        if ($this->session->userdata('MEM_ID'))
            $this->common_model->getLastProeprty();
    }

    public function index() {
        $slug = $this->uri->segment(2);
        if (empty($slug)) {
            redirect('/');
        }
        $params['table'] = 'tbl_pages';
        $params['single_row'] = true;
        $conditions['slug'] = $slug;
        $content = $this->common_model->get_data($conditions, $params);
        if (empty($content)) {
            show_404();
        }
        if($slug == 'common-questions'){
            //$question_id = $this->input->post('id');
            //$question_id = 8;
            $conditions = $params = array();
            //$conditions['category_id'] = $question_id;
            $conditions['type'] = 1;
            $params['table'] = 'tbl_questions';
            $question_info = $this->common_model->get_data($conditions, $params); 
            $html = '';
            $html .= '<div class="common_question_body">';
            if(!empty($question_info)){
                foreach ($question_info as $key => $value) {
                    $html .= '<div class="cq_header">';
                        $html .= "<div class='header_q'> Q </div><div class='header_question'>".$value->question. "</div>";
                    $html .= '</div>';
                    $conditions = $params = array();
                    $conditions['question_id'] = $value->id;
                    $params['table'] = 'tbl_answers';
                    $params['order_by'] = 'id desc';
                    $answer_info = $this->common_model->get_data($conditions, $params); 
                    if(!empty($answer_info)){
                        $html .= '<div class="cq_content">';
                            $html .= '<ul>';
                                foreach ($answer_info as $a_key => $a_value) {
                                    $html .= '<li class="cm_row">';
                                        $html .= '<div class="header_a"> A</div><div class="header_answer">'.$a_value->answer.'</div>';
                                    $html .= '</li>';
                                }
                            $html .= '</ul>';
                        $html .= '</div>';

                    }else{
                        $html .= '<div class="cq_content">';
                            $html .= '<ul>';
                                    $html .= '<li class="cm_row">';
                                        $html .= '<div class="header_a"> </div><div class="header_answer">Answers not exist</div>';
                                    $html .= '</li>';
                            $html .= '</ul>';
                        $html .= '</div>';
                    }
                }
            }else{
                $html .= '<div class="cq_header">';
                        $html .= "<div class='header_q'> </div><div class='header_question'>Question not exists</div>";
                    $html .= '</div>';
            }
            $html .= '</div>';
            $content->description = $html;
        }
        //echo '<pre>';print_r($content);die;
        /************captcha &*******************/
        
        $this->load->library('mathcaptcha');
        $config['question_format'] = 'numeric';
        $config['answer_format'] = 'numeric';
        $config['question_max_number_size'] = 10;
        $this->mathcaptcha->init($config);
        $output['math_captcha_question'] = $this->mathcaptcha->get_question();
        
        $output['content'] = $content;            
        $this->load->view($this->config->item('defaultfolder') . '/home_header');
        $this->load->view($this->config->item('defaultfolder') . '/static_page', $output);
        //$this->load->view($this->config->item('defaultfolder') . '/auth_js');
        $this->load->view($this->config->item('defaultfolder') . '/home_footer');
    }

    public function app() {
        $slug = $this->uri->segment(3);
        if (empty($slug)) {
            redirect('/');
        }
        $params['table'] = 'tbl_pages';
        $conditions['type'] = 'app_page';
        $params['single_row'] = true;
        $conditions['slug'] = $slug;
        $content = $this->common_model->get_data($conditions, $params);
        if (empty($content)) {
            show_404();
        }
        if($slug == 'common-questions'){
            //$question_id = $this->input->post('id');
            //$question_id = 8;
            $conditions = $params = array();
            $conditions['category_id'] = 5; //TENANT MOBILE APP
            $conditions['type'] = 1;
            $params['table'] = 'tbl_questions';
            $question_info = $this->common_model->get_data($conditions, $params); 
            $html = '';
            $html .= '<div class="common_question_body">';
            if(!empty($question_info)){
                foreach ($question_info as $key => $value) {
                    $html .= '<div class="cq_header">';
                        $html .= "<div class='header_q'> Q </div><div class='header_question'>".$value->question. "</div>";
                    $html .= '</div>';
                    $conditions = $params = array();
                    $conditions['question_id'] = $value->id;
                    $params['table'] = 'tbl_answers';
                    $params['order_by'] = 'id desc';
                    $answer_info = $this->common_model->get_data($conditions, $params); 
                    if(!empty($answer_info)){
                        $html .= '<div class="cq_content">';
                            $html .= '<ul>';
                                foreach ($answer_info as $a_key => $a_value) {
                                    $html .= '<li class="cm_row">';
                                        $html .= '<div class="header_a"> A</div><div class="header_answer">'.$a_value->answer.'</div>';
                                    $html .= '</li>';
                                }
                            $html .= '</ul>';
                        $html .= '</div>';

                    }else{
                        $html .= '<div class="cq_content">';
                            $html .= '<ul>';
                                    $html .= '<li class="cm_row">';
                                        $html .= '<div class="header_a"> </div><div class="header_answer">Answers not exist</div>';
                                    $html .= '</li>';
                            $html .= '</ul>';
                        $html .= '</div>';
                    }
                }
            }else{
                $html .= '<div class="cq_header">';
                        $html .= "<div class='header_q'> </div><div class='header_question'>Question not exists</div>";
                    $html .= '</div>';
            }
            $html .= '</div>';
            $content->description = $html;
        }
        //echo '<pre>';print_r($content);die;
        $output['content'] = $content;            
        $this->load->view($this->config->item('defaultfolder') . '/app_header');
        $this->load->view($this->config->item('defaultfolder') . '/static_page_app', $output);
        //$this->load->view($this->config->item('defaultfolder') . '/auth_js');
        $this->load->view($this->config->item('defaultfolder') . '/app_footer');
    }
    
   

  

}
