<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Leases extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('common_model');
        $this->mem_id = $this->session->userdata('MEM_ID');
        $this->common_model->checkMemberLogin();
    }

    public function index() {

        $output['selected_tab'] = 'leases';
        $output['title'] = 'Leases';

        $params = $conditions = array();

        $params['complex'] = true;
        $conditions['tables'] = array(
            'property' => 'tbl_properties',
            'lease' => 'tbl_properties_lease_detail',
        );
        $conditions['table'] = 'property';
        $conditions['on']['lease'] = array(
            'sign' => '=',
            'column' => 'property_id',
            'alias_column' => 'prop_id',
            'alias_other' => 'property',
        );
        $conditions['where']['owner_id']['alias'] = 'property';
        $conditions['where']['owner_id']['value'] = $this->session->userdata('MEM_ID');
        $conditions['where']['status']['alias'] = 'property';
        $conditions['where']['status']['value'] = 'Active';

        $params['fields'] = array(
            'property.prop_id', 'property.property_code', 'property.property_code', 'property.city',
            'property.address1', 'property.address2', 'property.zip', 'property.unit_number'
        );
        $params['group_by'] = array('property.prop_id');
        $property_data = $this->common_model->get_data($conditions, $params);
        $infi = array();
        if (!empty($property_data)) {
            foreach ($property_data as $property) {
                $ls = $this->getLeaseInfo($property->prop_id);
                if (!empty($ls)) {
                    $infi[$property->prop_id]['details'] = $property;
                    $infi[$property->prop_id]['leases'] = $ls;
                }
            }
        }

        $output['properties'] = $infi;
        //   echo '<pre>';print_r($output);die;
        $this->load->view($this->config->item('defaultfolder') . '/header', $output);
        $this->load->view($this->config->item('defaultfolder') . '/view_leases');
        $this->load->view($this->config->item('defaultfolder') . '/leases_js');
        $this->load->view($this->config->item('defaultfolder') . '/footer');
    }

    function getLeaseInfo($prop_id) {
        $params = $conditions = array();
        $params['complex'] = true;
        $conditions['tables'] = array(
            'lease' => 'tbl_properties_lease_detail',
            'tenant' => 'tbl_members',
        );
        $conditions['table'] = 'lease';
        $conditions['on']['tenant'] = array(
            'sign' => '=',
            'column' => 'mem_id',
            'alias_column' => 'tenant_id',
            'alias_other' => 'lease',
        );
        $conditions['where']['property_id']['alias'] = 'lease';
        $conditions['where']['property_id']['value'] = $prop_id;
        $conditions['where']['account_closed_by_member']['alias'] = 'tenant';
        $conditions['where']['account_closed_by_member']['value'] = 'No';

        $params['fields'] = array(
            'lease.id as lease_id', 'lease.lease_start_date', 'lease.lease_end_date', 'tenant.first_name',
            'tenant.last_name', 'tenant.mem_id', 'lease.property_id','lease.rent_amount'
        );

        $property_data = $this->common_model->get_data($conditions, $params);

        return $property_data;
    }

    function getLeaseDocs($prop_id, $tenant_id) {
        $params['table'] = 'tbl_properties_lease_detail';
        $conditions['property_id'] = $prop_id;

        //  $conditions['owner_id'] = $this->session->userdata('MEM_ID');
        $leases = $this->common_model->get_data($conditions, $params);
        echo '<pre>';
        print_r($leases);
        die;
    }

    public function upload_lease() {
        if (!empty($_FILES)) {
            $info = $this->input->post('prop_id');
            $attr = explode('_', $info);
            //$config['allowed_types'] = '*';
            $config['allowed_types'] = 'jpeg|jpg|png|txt|pdf|doc';
            $config['upload_path'] = './assets/uploads/lease_documents/';
            $config['encrypt_name'] = TRUE;
            $this->load->library('upload', $config);

            if (!empty($_FILES['file']['name'])) {
                if (!$this->upload->do_upload('file')) {
                    $error = $this->upload->display_errors();
                    echo '<pre>';
                    print_r($error);
                    die;
                    $failure = TRUE;
                } else {
                    $conditions = $params = array();
                    $data = array('upload_data' => $this->upload->data());
                    $conditions['lease_doc'] = $data['upload_data']['file_name'];
                    $conditions['property_id'] = $attr[0];
                    $conditions['tenant_id'] = $attr[1];
                    $conditions['status'] = 'Active';
                    $params['table'] = 'tbl_lease_docs';
                    $this->common_model->insert_data($conditions, $params);
                    echo $data['upload_data']['file_name'];
                    exit;
                }
            }
        } else {
            echo 'error';
            exit;
        }
    }


    public function upload_lease_doc() {
        if (!empty($_FILES)) {
            $info = $this->input->post('prop_id');
            $attr = explode('_', $info);
            //$config['allowed_types'] = '*';
            $config['allowed_types'] = 'jpeg|jpg|png|txt|pdf|doc';
            $config['upload_path'] = './assets/uploads/lease_documents/';
            $config['encrypt_name'] = TRUE;
            $this->load->library('upload', $config);

            if (!empty($_FILES['file']['name'])) {
                if (!$this->upload->do_upload('file')) {
                    $error = $this->upload->display_errors();
                    $data['success'] = false;
                    $data['message'] = $error;
                    $data['scrollToElement'] = true;
                    echo json_encode($data);
                    die;
                } else {
                    $conditions = $params = array();
                    $data = array('upload_data' => $this->upload->data());
                    $conditions['lease_doc'] = $data['upload_data']['file_name'];
                    $conditions['property_id'] = $attr[0];
                    $conditions['tenant_id'] = $attr[1];
                    $conditions['status'] = 'Active';
                    $params['table'] = 'tbl_lease_docs';
                    $this->common_model->insert_data($conditions, $params);
                    $message = $data['upload_data']['file_name'];
                    $data['success'] = true;
                    $data['message'] = $message;
                    $data['scrollToElement'] = true;
                    echo json_encode($data);
                    die;
                }
            }
        } else {
            $message = 'error';
            $data['success'] = false;
            $data['message'] = $message;
            $data['scrollToElement'] = true;
            echo json_encode($data);
            die;
        }
    }

    public function delete_document() {
        if ($this->input->post()) {
            $conditions = $params = array();
            $conditions['id'] = $this->input->post('id');
            $params['single_row'] = true;
            $params['table'] = 'tbl_lease_docs';
            $info = $this->common_model->get_data($conditions, $params);
            if (!empty($info)) {
                $src = "assets/uploads/lease_documents/" . $info->lease_doc;

                if (file_exists($src)) {
                    unlink($src);
                }
                $conditions = $params = array();
                $conditions['id'] = $this->input->post('id');
                $params['table'] = 'tbl_lease_docs';
                $this->common_model->delete($conditions, $params);
            }
            $success_message = 'selected record deleted successfully';
            $data['success'] = true;
            $data['success_message'] = $success_message;
            $data['scrollToElement'] = true;
            echo json_encode($data);
            die;
        } else {
            $success_message = 'Post data empty';
            $data['success'] = false;
            $data['success_message'] = $success_message;
            $data['scrollToElement'] = true;
            echo json_encode($data);
            die;
        }
    }

    public function update_lease() {

        if ($this->input->post()) {
            $conditions = $params = array();
            $conditions['id'] = $this->input->post('id');
            $params['single_row'] = true;
            $params['table'] = 'tbl_properties_lease_detail';
            $info = $this->common_model->get_data($conditions, $params);
            $conditions = $params = array();
            $datefrom = date('Y-m-d', strtotime($this->input->post('datefrom')));
            $dateto = date('Y-m-d', strtotime($this->input->post('dateto')));
            $rent_amount = $this->input->post('rent_amount');
            $conditions['where']['id'] = $this->input->post('id');
            //$conditions['value']['lease_start_date'] = strtotime($datefrom);
            //$conditions['value']['lease_end_date'] = strtotime($dateto);
            $conditions['value']['lease_start_date'] = $datefrom;
            $conditions['value']['lease_end_date'] = $dateto;
            $conditions['value']['rent_amount'] = $rent_amount;
            $params['table'] = 'tbl_properties_lease_detail';
            $this->common_model->update($conditions, $params);
            $success_message = 'Saved Successfully';
            $data['success'] = true;
            $data['success_message'] = $success_message;
            $data['scrollToElement'] = true;
            echo json_encode($data);
            die;
        } else {
            $success_message = 'Post data empty';
            $data['success'] = false;
            $data['success_message'] = $success_message;
            $data['scrollToElement'] = true;
            echo json_encode($data);
            die;
        }
    }

}
