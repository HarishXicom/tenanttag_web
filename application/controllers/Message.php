<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Message extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model(array('vendor_model', 'properties_model', 'common_model', 'services_model','member_model'));
        $this->mem_id = $this->session->userdata('MEM_ID');
        $this->common_model->checkMemberLogin();
    }

    function index() {
        $output['selected_tab'] = 'messages';
        $output['title'] = 'Messages';
        $conditions = $params = array();
        $params['table'] = 'tbl_properties';
        $conditions['owner_id'] = $this->session->userdata('MEM_ID');
        $conditions['status'] = 'Active';
        $params['fields'] = array('prop_id', 'city', 'address1', 'address2', 'zip');
        $output['properties'] = $this->common_model->get_data($conditions, $params);

        $params = $conditions = array();
        $params['complex'] = true;
        $conditions['tables'] = array(
            'tenant' => 'tbl_members',
            'property' => 'tbl_properties_lease_detail',
            'prop' => 'tbl_properties'
        );
        $conditions['table'] = 'property';
        $conditions['on']['tenant'] = array(
            'sign' => '=',
            'column' => 'mem_id',
            'alias_column' => 'tenant_id',
            'alias_other' => 'property',
        );
        $conditions['on']['prop'] = array(
            'sign' => '=',
            'column' => 'prop_id',
            'alias_column' => 'property_id',
            'alias_other' => 'property',
        );
        $conditions['where']['owner_id']['alias'] = 'property';
        $conditions['where']['owner_id']['value'] = $this->session->userdata('MEM_ID');
        $condition_string = "tenant.move_status != 2";
        $conditions['where']['move_status']['alias'] = 'tenant';
        $conditions['where']['move_status']['value'] = array(
            'sign' => 'spl_where',
            'value' => $condition_string
        );
        $conditions['where']['account_closed_by_member']['alias'] = 'tenant';
        $conditions['where']['account_closed_by_member']['value'] = 'No';
    
        $conditions['where']['status']['alias'] = 'prop';
        $conditions['where']['status']['value'] = 'Active';
        $params['fields'] = array(
            'property.property_id', 'property.property_unique_id', 'property.tenant_id',
            'tenant.first_name', 'tenant.last_name');
        $params['order_by'] = 'tenant.first_name';
        $tenant_data = $this->common_model->get_data($conditions, $params);
        $output['tenants'] = $tenant_data;

        $output['sch_msgs'] = $this->getSMSbyType('tbl_sms_schedule');

        $output['sent_msgs'] = $this->getSMSbyType('tbl_sms_logs');
        //echo "<pre>"; print_r($output); exit;
        $this->load->view($this->config->item('defaultfolder') . '/header', $output);
        $this->load->view($this->config->item('defaultfolder') . '/messages');
        $this->load->view($this->config->item('defaultfolder') . '/message_js');
        $this->load->view($this->config->item('defaultfolder') . '/footer');
    }

    public function getSMSbyType($type, $pid = NULL) {
        $params = $conditions = array();
        $params['complex'] = true;
        $conditions['tables'] = array(
            'sms' => $type,
            'tenant' => 'tbl_members',
            'property' => 'tbl_properties',
        );
        $conditions['table'] = 'sms';
        $conditions['on']['tenant'] = array(
            'sign' => '=',
            'column' => 'mem_id',
            'alias_column' => 'tenant_id',
            'alias_other' => 'sms',
        );
        $conditions['on']['property'] = array(
            'sign' => '=',
            'column' => 'prop_id',
            'alias_column' => 'property_id',
            'alias_other' => 'sms',
        );
        $conditions['where']['owner_id']['alias'] = 'sms';
        $conditions['where']['owner_id']['value'] = $this->session->userdata('MEM_ID');
        if ($pid != NULL) {
            $conditions['where']['prop_id']['alias'] = 'property';
            $conditions['where']['prop_id']['value'] = $pid;
        }
        $conditions['where']['account_closed_by_member']['alias'] = 'tenant';
        $conditions['where']['account_closed_by_member']['value'] = 'No';
        
        $params['fields'] = array(
            'property.prop_id', 'sms.*',
            'property.address1', 'property.address2', 'property.city', 'tenant.first_name', 'tenant.last_name');
        if ($type == 'tbl_sms_schedule') {
            // $params['order_by'] = '(sms.scheduled_on=CURDATE()) DESC, sms.scheduled_on';
        } else {
            //  $params['order_by'] = 'sms.id';
        }
        $params['order_by'] = 'sms.id';
        $sms_data = $this->common_model->get_data($conditions, $params);

        return $sms_data;
    }

    public function send_save_msg() {
        // echo '<pre>';print_r($_POST);die;
        if ($this->input->post('custom_message')) {
            $numbers = array();
            if ($this->input->post('message_for') == 'tenant') {
                $tenats = $this->input->post('tenant_ids');
                if (!empty($tenats)) {
                    $numbers = $this->common_model->getCellNumbers($tenats);
                }
            } else {
                $properties = $this->input->post('property_ids'); //get tenants of properties
                $tenatids = $this->common_model->getTenants($properties);
                if (!empty($tenatids)) {
                    foreach ($tenatids as $tnt) {
                        $tenats[] = $tnt->tenant_id;
                    }
                    if (!empty($tenats)) {
                        $numbers = $this->common_model->getCellNumbers($tenats);
                    }
                }
            }
            //echo '<pre>';print_r($numbers);die;
            //send message now
            if ($this->input->post('sch_msg') == 'No') {
                if (!empty($numbers)) {
                    $counts = 0;
                   $memDetail = $this->member_model->getMemberInfo('mem_id', $this->session->userdata('MEM_ID'));
                    foreach ($numbers as $num) {
                        $msgtext = $this->input->post('custom_message') . " -" . $memDetail->first_name." ".$memDetail->last_name;
                        $this->send_sms($msgtext, $num->mobile_no, $num->tenant_id, $num->property_id);
                        $counts++;
                    }
                }
                $success_message = 'SMS Sent';
                $data['success'] = true;
                $data['success_message'] = $success_message;
                $data['scrollToElement'] = true;
                echo json_encode($data);
                die;
            } else { //schedule message    
                if (!empty($numbers)) {

                    $counts = 0;
                    $date = date('Y-m-d H:i:s', strtotime($this->input->post('sch_date')));
                    // echo '<pre>';print_r($date);die;
                    foreach ($numbers as $num) {
                        $repeat_type = $this->input->post('repeat_msg');
                        if ($repeat_type == 'year') {
                            $repeat_on = '';
                            $repeat_every = '';
                        } elseif ($repeat_type == 'month') {
                            $repeat_on = '';
                            $repeat_every = $this->input->post('repeat_every');
                        } elseif ($repeat_type == 'quarterly') {
                            $repeat_on = '';
                            $repeat_every = $this->input->post('repeat_every');
                        } else {
                            $repeat_on = $this->input->post('repeat_on');
                            $repeat_every = $this->input->post('repeat_every');
                        }
                        $sms_add[] = array(
                            'owner_id' => $this->session->userdata('MEM_ID'),
                            'tenant_id' => $num->tenant_id,
                            'property_id' => $num->property_id,
                            'number' => $num->mobile_no,
                            'message' => $this->input->post('custom_message'),
                            'scheduled_on' => $date,
                            'repeat_type' => $this->input->post('repeat_msg'),
                            'repeat_every' => $repeat_every,
                            'repeat_on' => $repeat_on,
                            'end_repeat' => $this->input->post('end_msg'),
                            'occurences' => $this->input->post('msg_occurence'),
                            'date_created' => date('Y-m-d H:i:s'),
                        );
                        $counts++;
                    }

                    $conditions = $params = array();
                    $params['batch_mode'] = true;
                    $conditions = $sms_add;
                    $params['table'] = 'tbl_sms_schedule';
                    $this->common_model->insert_data($conditions, $params);
                    $success_message = 'SMS scheduled for ' . $counts . '  tenants/properties';
                    $data['success'] = true;
                    $data['success_message'] = $success_message;
                    $data['scrollToElement'] = true;
                    echo json_encode($data);
                    die;
                }
            }
        } else {
            $success_message = 'Server error';
            $data['success'] = false;
            $data['success_message'] = $success_message;
            $data['scrollToElement'] = true;
            echo json_encode($data);
            die;
        }
    }

    public function send_sms($msg, $mobile_number, $tenant, $property) {
        // Load the library, call by the name of the

        $this->load->helper('twilio_helper');  // references library/twilio_helper.php
        $service = get_twilio_service();
        try {
            $number = "+1" . preg_replace("/[^0-9]/", "", $mobile_number);
            $service->account->sms_messages->create("+19046743077", "$number", "$msg", array());
            $this->message_log($msg, $number, $tenant, $property);
            return TRUE;
        } catch (Exception $e) {
            $file = 'sms_error_logs.txt';
            if (file_exists($file)) {
                $current = file_get_contents($file);
                $current .= $e->getMessage() . "\n";
                file_put_contents($file, $current);
            }
            return TRUE;
        }
    }

    public function message_log($msg, $number, $tenant, $property) {
        $conditions = $params = array();
        $conditions['message'] = $msg;
        $conditions['owner_id'] = $this->session->userdata('MEM_ID');
        $conditions['number'] = $number;
        $conditions['tenant_id'] = $tenant;
        $conditions['property_id'] = $property;
        $conditions['date_created'] = date('Y-m-d H:i:s');
        $params['table'] = 'tbl_sms_logs';
        $this->common_model->insert_data($conditions, $params);
    }

    public function delete_message() {
        if ($this->input->post('id')) {
            $conditions = $params = array();
            $conditions['id'] = $this->input->post('id');
            $params['table'] = 'tbl_sms_schedule';
            $this->common_model->delete($conditions, $params);
            echo '1';
            exit;
        } else {
            echo '0';
            exit;
        }
    }
    public function delete_log() {
        if ($this->input->post('id')) {
            $conditions = $params = array();
            $conditions['id'] = $this->input->post('id');
            $params['table'] = 'tbl_sms_logs';
            $this->common_model->delete($conditions, $params);
            echo '1';
            exit;
        } else {
            echo '0';
            exit;
        }
    }
    

    public function update_message() {
        if ($this->input->post('id')) {
            $conditions = $params = array();
            $conditions['where']['id'] = $this->input->post('id');
            $conditions['value']['scheduled_on'] = date('Y-m-d H:i:s', strtotime($this->input->post('scheduled_on')));
            $conditions['value']['repeat_type'] = $this->input->post('repeat_type');
            $conditions['value']['repeat_every'] = $this->input->post('repeat_every');
            $conditions['value']['repeat_on'] = $this->input->post('repeat_on');
            $conditions['value']['message'] = $this->input->post('sms');
            if ($this->input->post('occurences') == 'noend') {
                $conditions['value']['end_repeat'] = 'infinite';
            } else {
                $conditions['value']['end_repeat'] = 'finite';
                $conditions['value']['occurences'] = $this->input->post('occurences');
            }
            $params['table'] = 'tbl_sms_schedule';
            $this->common_model->update($conditions, $params);
            echo '1';
            exit;
        } else {
            echo '0';
            exit;
        }
    }

    public function getSinglePropertySMS() {
        if ($this->input->post('id')) {
            $output['prop_id'] = $this->input->post('id');
            $output['sch_msgs'] = $this->getSMSbyType('tbl_sms_schedule', $this->input->post('id'));
            $output['sent_msgs'] = $this->getSMSbyType('tbl_sms_logs', $this->input->post('id'));
            $this->load->view($this->config->item('defaultfolder') . '/ajaxSMS', $output);
            
        } else {
            $output['sch_msgs'] = $this->getSMSbyType('tbl_sms_schedule');
            $output['sent_msgs'] = $this->getSMSbyType('tbl_sms_logs');
            $this->load->view($this->config->item('defaultfolder') . '/ajaxScheduled', $output);
        }
    }

}

?>