<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Account extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('member_model');
        $this->load->model(array('properties_model', 'vendor_model', 'common_model'));
        $this->mem_id = $this->session->userdata('MEM_ID');
        $this->common_model->checkMemberLogin();
        $this->perPage = 20;
        //$this->session->keep_flashdata('stripe_payment');
        //$this->session->unset_userdata('PROPERTY_ID');
    }


    function index($type = NULL) {
        $this->checkFOrProperty();
        $output['selected_tab'] = 'dashboard';
        $output['title'] = 'Dashboard';
        $output['tPayment'] = true;
        $this->checkSynaspeUser($this->mem_id);
        /*         * **** function to get detail of login landlord **** */
        $memDetail = $this->member_model->getMemberInfo('mem_id', $this->mem_id);
        $output['memDetail'] = $memDetail;
        /*         * **** function to get all properties of login landlord **** */
        if($type!= 'NULL'){
            $opp = array();
            $occupiedprop = $this->properties_model->occupiedProperties($this->mem_id);
        
            if(!empty($occupiedprop)){
                foreach($occupiedprop as $op){
                    $opp[]=$op->prop_id;
                }
                $allProperties = $this->properties_model->frontGetAllPropertiesByType($this->mem_id, $opp,$type);
            }elseif($type=='occupied'){
                $allProperties = array();
            }else{
                $allProperties = $this->properties_model->frontGetAllPropertiesByLandlordId($this->mem_id);
            }
             
        }else{
            $allProperties = $this->properties_model->frontGetAllPropertiesByLandlordId($this->mem_id, $type);
        }
      
        $totalTenants = 0;
        $j = 0;
        $vendors = array();
        foreach ($allProperties as $value) {
            $value->tenants = $this->properties_model->getCountAllTenantsByProperty($value->prop_id);
            $totalTenants+=$value->tenants;
            $vendors = $this->vendor_model->countVendors($this->mem_id, $value->prop_id);
            $value->vendors = count($vendors);
            $params = $conditions = $feeds = array();
            $params['cnt'] = true;
            $params['table'] = 'tbl_sms_logs';
            $params['order_by'] = 'id DESC';
            $conditions['property_id'] = $value->prop_id;
            $value->messages = $this->common_model->get_data($conditions, $params);
            $timestamp = date('Y-m-d',strtotime("+60 day"));
            $params = $conditions = $feeds = array();
            $params['cnt'] = true;
            $params['table'] = 'tbl_properties_lease_detail';
            $conditions['property_id'] = $value->prop_id;
            $conditions['tbl_property_photos'] = array(
                'operator' => 'AND',
                'sign' => '<',
                'key' => 'lease_end_date',
                'value' => $timestamp
            );
            $val = $this->common_model->get_data($conditions, $params);
            if ($val > 0) {
                $value->expiry = 'Yes';
            } else {
                $value->expiry = 'No';
            }
            //$payment_info = $this->properties_model->getPaymentInfoByPropertyId($value->prop_id);
            //$allProperties[$j]->rent_amount = !empty($payment_info) ? $payment_info->amount : '';
            $tenantDetails = $this->properties_model->getAllTenantsByProperty($value->prop_id);
            $total_due_amount = '';
            foreach ($tenantDetails as $t_key => $t_value) {
                $due_info = getDuePaymentInfo($t_value->tenant_id);
                $total_due_amount      = $due_info['due_amount']; 
            }

            if(empty($total_due_amount)){
                $total_due_amount = 0;
            }else{
                $total_due_amount = '$'.$total_due_amount;
            }
            $allProperties[$j]->rent_amount = $total_due_amount;
            $allProperties[$j]->subscription_status = $this->common_model->getPropertySubscriptionStatus($this->mem_id,$value->prop_id);
            $j++;
        }
        $output['totalTenants'] = $totalTenants;
        $countall = $this->properties_model->frontGetCountAllPropertiesByLandlordId($this->mem_id, '');;
        $output['countAllProperties'] = $this->properties_model->frontGetCountAllPropertiesByLandlordId($this->mem_id, '');
        $occupied = $this->properties_model->occupiedProperties($this->mem_id);
        $output['countVacantProperties'] = $countall - count($occupied);
        $output['countOccupiedProperties'] = count($occupied);
        $output['allProperties'] = $allProperties;        

        $output['tpay_account_status'] = 0; // acount active
        if(getSynapseSetUpStatus($this->session->userdata('MEM_ID')) == true){
            $output['tpay_account_status'] = 1;     //acount Active
        }

        //echo"<pre>";print_r($output);die;
        if ($type == 'NULL')
            $type = 'all';
        $output['type'] = $type;
        if ($this->input->is_ajax_request()) {
            $data['success'] = true;
            $data['html'] = $this->load->view($this->config->item('defaultfolder') . '/dashboard_properties', $output, true);
            echo json_encode($data);
            die;
        }
        //echo "<pre>"; print_R($allProperties); exit;

        
        $this->load->view($this->config->item('defaultfolder') . '/header', $output);
        $this->load->view($this->config->item('defaultfolder') . '/dashboard');
        $this->load->view($this->config->item('defaultfolder') . '/script_js');
        $this->load->view($this->config->item('defaultfolder') . '/footer');
    }


            /**
     * Method Name : getUserInfo
     * Author Name : LAkhvinder Singh
     * Description : get basic details of user
     */
    public function getUserInfo($user_id) {
        $conditions = $params = array();
        $params['table'] = 'tbl_members';
        $conditions['mem_id'] = $user_id;
        $params['single_row'] = TRUE;
        $params['fields'] = array('mem_id', 'synapse_user_id','synapse_fingerprint','stripe_customer_id', 'email', 'first_name', 'slug','last_name', 'mobile_no', 'dob', 'app_verified', 'move_status','third_party_sharing');
        $info = $this->common_model->get_data($conditions, $params);
        return $info;
    }

    
    /**
     * Method Name : gerUserSynaspeFingerprintAndIp
     * Author Name : Lakhvinder Singh
     * Date : 02 March 2017
     * Description : get synapse_fingerprint and ip address
     */   
    function gerUserSynaspeFingerprintAndIp($user_id){
        $conditions = $params = array();
        $params['table'] = 'tbl_members';
        $conditions['mem_id'] = $user_id;
        $params['single_row'] = TRUE;
        $params['fields'] = array('synapse_fingerprint','synapse_ip_address');
        $data = $this->common_model->get_data($conditions, $params);
        return $data;
    }


    function checkSynaspeUser($user_id){
        $login_id               = $user_id;
        $user_info              = $this->getUserInfo($login_id);
        $SYNAPSE_CLIENT_ID      = SYNAPSE_CLIENT_ID;
        $SYNAPSE_CLIENT_SECRET  = SYNAPSE_CLIENT_SECRET; 
        $synapse_data           = $this->gerUserSynaspeFingerprintAndIp($login_id);
        $SYNAPSE_IP_ADDRESS     = $synapse_data->synapse_ip_address; 
        $SYNAPSE_FINGERPRINT    = $synapse_data->synapse_fingerprint;         
        //print_r($synapse_data); print_r($SYNAPSE_CLIENT_ID);  
        if(empty($user_info->synapse_user_id)){
            $email      = $user_info->email;
            $mobile_no  = $user_info->mobile_no;
            $legal_name = $user_info->first_name.' '.$user_info->last_name;

            /************************** create user using curl *************************/
            $curl       = curl_init();
            curl_setopt_array($curl, array(
                                            CURLOPT_URL             => SYNAPSE_API_BASE_URL."users",
                                            CURLOPT_RETURNTRANSFER  => true,
                                            CURLOPT_ENCODING        => "",
                                            CURLOPT_MAXREDIRS       => 10,
                                            CURLOPT_TIMEOUT         => 30,
                                            CURLOPT_HTTP_VERSION    => CURL_HTTP_VERSION_1_1,
                                            CURLOPT_CUSTOMREQUEST   => "POST",
                                            CURLOPT_POSTFIELDS      => "{\n  \"logins\": [\n    {\n      \"email\": \"$email\"\n    }\n  ],\n  \"phone_numbers\": [\n    \"$mobile_no\"\n  ],\n  \"legal_names\": [\n    \"$legal_name\"\n  ],\n  \"extra\": {\n    \"note\": null,\n    \"is_business\": false,\n    \"cip_tag\":2\n  }\n}",
                                            CURLOPT_HTTPHEADER      => array(
                                                                                "cache-control: no-cache",
                                                                                "content-type: application/json",
                                                                                "x-sp-gateway: $SYNAPSE_CLIENT_ID|$SYNAPSE_CLIENT_SECRET",
                                                                                "x-sp-user: |$SYNAPSE_FINGERPRINT",
                                                                                "x-sp-user-ip: $SYNAPSE_IP_ADDRESS"
                                                                            ),
                                        )
                            );
            $response   = curl_exec($curl);
            $err        = curl_error($curl);
            curl_close($curl);
            //echo SYNAPSE_API_BASE_URL."users";
            //print_r($response); exit;
            if ($err) {
                return false;
            } else {
                $json_decode           = json_decode($response); // decode the "users's API Response
                //echo "<pre>".$mobile_no; print_r($json_decode); exit;
                $synapse_id            = $json_decode->_id; 
                $refresh_token         = $json_decode->refresh_token;
                /********************** update synapse user id in the table **************************/
                $conditions = $params = array();
                $conditions['value']['synapse_user_id'] = $synapse_id;
                $conditions['where']['mem_id']          = $login_id;
                $params['table'] = 'tbl_members';
                $this->common_model->update($conditions, $params);
            }

        }
    }
    

    function my_properties() {
        $output['selected_tab'] = 'properties';
        $output['title'] = 'My Properties';

        /*         * ***** Load additional models as per requirement ********************* */
        $this->load->model('amenties_model');
        $this->load->model('services_model');

        /*         * **** function to get detail of login landlord **** */
        $memDetail = $this->member_model->getMemberInfo('mem_id', $this->mem_id);
        $output['memDetail'] = $memDetail;
        /*         * ******** Get All Amenties ******************************** */
        $output['allAmenties'] = $allAmenties = $this->amenties_model->frontGetAllAmenties();
        /*         * ******** Get All Services ******************************** */
        $output['allServices'] = $this->services_model->frontGetAllServices();
        /*         * **** function to get all properties of login landlord **** */
        $allProperties = $this->properties_model->frontGetAllPropertiesByLandlordId($this->mem_id);
        foreach ($allProperties as $value) {
            $value->property_services = $this->properties_model->getServicesByPropertyId($value->prop_id);
            $value->tenants = $this->properties_model->getAllTenantsByProperty($value->prop_id);
            $value->photos = $this->get_photos($value->prop_id);
        }
        $output['allProperties'] = $allProperties;
        $this->load->view($this->config->item('defaultfolder') . '/header', $output);
        $this->load->view($this->config->item('defaultfolder') . '/my-properties');
        $this->load->view($this->config->item('defaultfolder') . '/footer');
    }

    public function get_photos($prop_id) {
        $conditions = $params = array();
        $params['table'] = 'tbl_property_photos';
        $conditions['property_id'] = $prop_id;
        //$params['cnt'] = true;
        $params['order_by'] = 'id DESC';
        $info = $this->common_model->get_data($conditions, $params);
        return $info;
    }

    function checkFOrProperty() {
        $allProperties = $this->properties_model->frontGetCountAllPropertiesByLandlordId($this->session->userdata('MEM_ID'));
        $step_completed = $this->common_model->getHigestCompletedStepProeprty();
        if (!$allProperties || ($allProperties == 1 && $step_completed < 3)) {
            redirect(site_url('add-property'));
        }
    }

    public function settings() {
        $output['selected_tab'] = '';
        $output['title'] = '';
        if ($this->input->post('state')) {
            $failure = 0;
            $this->form_validation->set_message('required', '%s is required');
           /* $this->form_validation->set_rules('address', 'Address', 'trim|required');
            $this->form_validation->set_rules('city', 'City', 'trim|required');
            $this->form_validation->set_rules('state', 'State', 'trim|required');*/
            $this->form_validation->set_rules('first_name', 'First Name', 'trim|required');
            $this->form_validation->set_rules('last_name', 'Last Name', 'trim|required');
            $this->form_validation->set_rules('mobile_no', 'Mobile', 'trim|required');
            if ($this->form_validation->run()) {
                $conditions = $params = array();
                $conditions['where']['mem_id'] = $this->session->userdata('MEM_ID');
              /*  $conditions['value']['address'] = $this->input->post('address');
                $conditions['value']['city_id'] = $this->input->post('city');
                $conditions['value']['state_id'] = $this->input->post('state');*/
                $conditions['value']['first_name'] = $this->input->post('first_name');
                $conditions['value']['last_name'] = $this->input->post('last_name');
                $conditions['value']['mobile_no'] = $this->input->post('mobile_no');
                $conditions['value']['company_name'] = $this->input->post('company_name');
                
                $params['table'] = 'tbl_members';
                $this->common_model->update($conditions, $params);
                $success_message = '<p>Information updated successfully.</p>';
            } else {
                $error_message = validation_errors();
                $failure = true;
            }
            if ($this->input->is_ajax_request()) {
                if ($failure) {
                    $data['success'] = false;
                    $data['error_message'] = $error_message;
                } else {
                    $data['success'] = true;
                    //$data['resetForm'] = true;
                    $data['success_message'] = $success_message;
                     $data['settingForm'] = true;
                }
                $data['scrollToElement'] = true;
                echo json_encode($data);
                die;
            }
        }
        
        if ($this->input->post('mailing_state_id')) {
            $failure = 0;
            $this->form_validation->set_message('required', '%s is required');
            $this->form_validation->set_rules('mailing_address1', 'Address 1', 'trim|required');
            $this->form_validation->set_rules('mailing_city', 'City', 'trim|required');
            $this->form_validation->set_rules('mailing_state_id', 'State', 'trim|required');
            $this->form_validation->set_rules('mailing_zip', 'Zip', 'trim|required');
            if ($this->form_validation->run()) {
                $conditions = $params = array();
                $conditions['where']['mem_id'] = $this->session->userdata('MEM_ID');
                $conditions['value']['mailing_address1'] = $this->input->post('mailing_address1');
                $conditions['value']['mailing_city'] = $this->input->post('mailing_city');
                $conditions['value']['mailing_state_id'] = $this->input->post('mailing_state_id');
                $conditions['value']['mailing_zip'] = $this->input->post('mailing_zip');
                $conditions['value']['mailing_address2'] = $this->input->post('mailing_address2');
                $conditions['value']['mailing_unit'] = $this->input->post('mailing_unit');
                $params['table'] = 'tbl_members';
                $this->common_model->update($conditions, $params);
                $success_message = '<p>Information updated successfully.</p>';
            } else {
                $error_message = validation_errors();
                $failure = true;
            }
            if ($this->input->is_ajax_request()) {
                if ($failure) {
                    $data['success'] = false;
                    $data['error_message'] = $error_message;
                } else {
                    $data['success'] = true;
                 //   $data['resetForm'] = true;
                    $data['success_message'] = $success_message;
                    $data['settingForm'] = true;
                }
                $data['scrollToElement'] = true;
                echo json_encode($data);
                die;
            }
        }

        if ($this->input->post('change_pass')) {
            $failure = 0;
            $this->form_validation->set_message('required', '%s is required');
            $this->form_validation->set_rules('old_pass', 'Old Password', 'trim|required|callback_oldpassword_check');
            $this->form_validation->set_rules('new_pass', 'New Password', 'trim|required|min_length[6]|matches[confirm_pass]');
            $this->form_validation->set_rules('confirm_pass', 'Confirm Password', 'trim|required');
            if ($this->form_validation->run()) {
                $conditions = $params = array();
                $conditions['where']['mem_id'] = $this->session->userdata('MEM_ID');
                $conditions['value']['password'] = md5($this->input->post('new_pass'));
                $conditions['value']['tpass'] = $this->input->post('new_pass');
                $params['table'] = 'tbl_members';
                $this->common_model->update($conditions, $params);
                $success_message = '<p>Password updated successfully.</p>';
            } else {
                $error_message = validation_errors();
                $failure = true;
            }
            if ($this->input->is_ajax_request()) {
                if ($failure) {
                    $data['success'] = false;
                    $data['error_message'] = $error_message;
                } else {
                    $data['success'] = true;
                    $data['resetForm'] = true;
                    $data['success_message'] = $success_message;
                }
                $data['scrollToElement'] = true;
                echo json_encode($data);
                die;
            }
        }
        if ($this->input->post('sharing')) {            
            $conditions = $params = array();
            $conditions['where']['mem_id'] = $this->session->userdata('MEM_ID');
            $conditions['value']['third_party_sharing'] = $this->input->post('third_party_sharing');               
            $params['table'] = 'tbl_members';
            $this->common_model->update($conditions, $params);
            $success_message = '<p>Information updated successfully.</p>';
            $failure = false;
            if ($this->input->is_ajax_request()) {
                if ($failure) {
                    $data['success'] = false;
                    $data['error_message'] = $error_message;
                } else {
                    $data['success'] = true;
                    //$data['resetForm'] = true;
                    $data['success_message'] = $success_message;
                     $data['settingForm'] = true;
                }
                $data['scrollToElement'] = true;
                echo json_encode($data);
                die;
            }
        }

        $conditions = $params = array();
        $params['complex'] = true;
        $params['single_row'] = true;
        $conditions['tables'] = array(
            'mem' => 'tbl_members',
            'state' => 'tbl_region'
        );
        $conditions['table'] = 'mem';
        $conditions['on']['state'] = array(
            'sign' => '=',
            'column' => 'region_id',
            'alias_column' => 'state_id',
            'alias_other' => 'mem',
            'join' => 'left'
        );
        $conditions['where']['mem_id']['alias'] = 'mem';
        $conditions['where']['mem_id']['value'] = $this->session->userdata('MEM_ID');
        $params['single_row'] = TRUE;
        $params['fields'] = array('mem.*', 'state.region_name', 'state.region_id');
        $info = $this->common_model->get_data($conditions, $params);
        $output['info'] = $info;
        $this->load->view($this->config->item('defaultfolder') . '/header', $output);
        $this->load->view($this->config->item('defaultfolder') . '/settings');
        $this->load->view($this->config->item('defaultfolder') . '/settings_js');
        $this->load->view($this->config->item('defaultfolder') . '/footer');
    }

    public function billing($user_name =false) {
        $output['selected_tab'] = '';
        $output['title'] = '';
        //echo $user_name; exit;
        $login_id = $this->session->userdata('MEM_ID');        

        if ($this->input->post('change_pass')) {
            $failure = 0;
            $this->form_validation->set_message('required', '%s is required');
            $this->form_validation->set_rules('old_pass', 'Old Password', 'trim|required|callback_oldpassword_check');
            $this->form_validation->set_rules('new_pass', 'New Password', 'trim|required|min_length[6]|matches[confirm_pass]');
            $this->form_validation->set_rules('confirm_pass', 'Confirm Password', 'trim|required');
            if ($this->form_validation->run()) {
                $conditions = $params = array();
                $conditions['where']['mem_id'] = $this->session->userdata('MEM_ID');
                $conditions['value']['password'] = md5($this->input->post('new_pass'));
                $conditions['value']['tpass'] = $this->input->post('new_pass');
                $params['table'] = 'tbl_members';
                $this->common_model->update($conditions, $params);
                $success_message = '<p>Password updated successfully.</p>';
            } else {
                $error_message = validation_errors();
                $failure = true;
            }
            if ($this->input->is_ajax_request()) {
                if ($failure) {
                    $data['success'] = false;
                    $data['error_message'] = $error_message;
                } else {
                    $data['success'] = true;
                    $data['resetForm'] = true;
                    $data['success_message'] = $success_message;
                }
                $data['scrollToElement'] = true;
                echo json_encode($data);
                die;
            }
        }

        $conditions = $params = array();
        $params['complex'] = true;
        $params['single_row'] = true;
        $conditions['tables'] = array(
            'mem' => 'tbl_members',
            'state' => 'tbl_region'
        );
        $conditions['table'] = 'mem';
        $conditions['on']['state'] = array(
            'sign' => '=',
            'column' => 'region_id',
            'alias_column' => 'state_id',
            'alias_other' => 'mem',
            'join' => 'left'
        );
        $conditions['where']['mem_id']['alias'] = 'mem';
        $conditions['where']['mem_id']['value'] = $this->session->userdata('MEM_ID');
        $params['single_row'] = TRUE;
        $params['fields'] = array('mem.*', 'state.region_name', 'state.region_id');
        $info = $this->common_model->get_data($conditions, $params);
        $output['info'] = $info;
        $output['memberRecord']         = $this->member_model->getMemberRecordById($this->session->userdata('MEM_ID'));
        $output['stripe_customer_card'] = $this->get_customer_saved_card($this->session->userdata('MEM_ID'));
        $card_info                      = $this->getStripeBillingCardInfo($this->session->userdata('MEM_ID'));
        if(!empty($card_info)){
            $card_info->status = $this->checkAccountStatus($card_info->stripe_external_account_id);
        }
        $output['card_info'] = $card_info;


        /**************** Payment History **************************************/

        $this->load->model('transaction');
        if($user_name == false){
            $user_name = 'all';        
        }
        $TenantTotalRec = count($this->transaction->getRows(array('user_id'=>$this->session->userdata('MEM_ID'),'slug' => $user_name)));        
        $config = array();
        $config['target']      = '#transactionList';
        $config['base_url']    = base_url().'account/TransactionData/'.$user_name;
        $config['total_rows']  = $TenantTotalRec;
        $config['per_page']    = $this->perPage;
        $this->load->library('Ajax_pagination');
        $this->ajax_pagination->initialize($config);         
        $output['transactions'] = $this->transaction->getRows(array('limit'=>$this->perPage,'user_id' => $this->session->userdata('MEM_ID'),'slug' => $user_name));

        /************* End Payment History ****************************************/
        $tenantList = $this->transaction->get_all_tenant($this->session->userdata('MEM_ID'));
        $output['tenantList'] = $tenantList;
        $output['current_tenant'] = $user_name;
        $output['landlord_info'] = $this->getUserInfo($this->session->userdata('MEM_ID'));

        $this->load->view($this->config->item('defaultfolder') . '/header', $output);
        $this->load->view($this->config->item('defaultfolder') . '/billing');
        $this->load->view($this->config->item('defaultfolder') . '/footer');
    } 


    function TransactionData($user_name)
    {
        $page = $this->input->post('page'); 
        if(!$page){
            $offset = 0;
        }else{
            $offset = $page;
        }
        $this->load->model('transaction');
        $this->load->library('Ajax_pagination');
        //total rows count
        $totalRec = count($this->transaction->getRows(array('user_id'=> $this->session->userdata('MEM_ID'),'slug'=> $user_name)));
        
        //Pagination configuration
        $config['target']      = '#transactionList';
        $config['base_url']    = base_url().'account/TransactionData/'.$user_name;
        $config['total_rows']  = $totalRec;
        $config['per_page']    = $this->perPage;
        $config['uri_segment'] = 4;

        $this->ajax_pagination->initialize($config);
        
        //get the transactions data 
        $data['transactions'] = $this->transaction->getRows(array('start'=>$offset,'limit'=>$this->perPage,'user_id'=>$this->session->userdata('MEM_ID'),'slug'=> $user_name));
        //load the view
        $this->load->view('default/transactions', $data, false);
    }




    function get_customer_saved_card($user_id){
        $conditions = $params = array();
        $params['complex'] = true;
        $conditions['tables'] = array(
            'stripe_card_info' => 'tbl_stripe_card_info',
            'properties' => 'tbl_properties'
        );
        $conditions['table'] = 'stripe_card_info';
        $conditions['on']['properties'] = array(
            'sign' => '=',
            'column' => 'prop_id',
            'alias_column' => 'property_id',
            'alias_other' => 'stripe_card_info',
        );

        $conditions['where']['user_id']['alias'] = 'stripe_card_info';
        $conditions['where']['user_id']['value'] = $user_id;

        $conditions['where']['status']['alias'] = 'properties';
        $conditions['where']['status']['value'] = 'Active';

        $conditions['where']['disable_card']['alias'] = 'stripe_card_info';
        $conditions['where']['disable_card']['value'] = 0;

        $conditions['where']['stripe_customer_id']['alias'] = 'stripe_card_info';
        $conditions['where']['stripe_customer_id']['value'] =  array(
                                                                    'operator' => 'AND',
                                                                    'key' => 'stripe_customer_id',
                                                                    'sign' => '!=',
                                                                    'value' => ''
                                                                );
        $params['fields'] = array('stripe_card_info.*','properties.city as property_city','properties.address1 as property_address1','properties.address2 as property_address2');
        $stripe_cards = $this->common_model->get_data($conditions, $params);
        foreach ($stripe_cards as $key => $value) {
            $value->subscription_info = $this->common_model->getPropertySubscriptionDetail($value->user_id,$value->property_id);
        }
        return $stripe_cards; 
    }

    public function change_stripe_recurring_status(){
        $login_id = $this->session->userdata('MEM_ID'); 
        $failure = false;
        $success_message = '';
        if ($this->input->post('id')) {
            $recurring = $this->input->post('recurring');
            $id = $this->input->post('id');
            $conditions = $params = array();
            $conditions['user_id'] = $login_id;
            $conditions['id'] = $id;
            $params['table'] = 'tbl_stripe_card_info';
            $info = $this->common_model->get_data($conditions, $params);  
            if(!empty($info)){
                $insert_data = array(
                    'recurring'=> $recurring
                );
                $conditions = $params = array();
                $conditions['value'] = $insert_data;
                $conditions['where']['user_id'] = $login_id;
                $conditions['where']['id']  = $id;
                $params['table']            = 'tbl_stripe_card_info';
                $this->common_model->update($conditions, $params);  
                $success_message = "You are successfully changes the recurring status";
            }else{
                $failure = true;
                $error_message = 'Something wents Wrong';
            }
            
        }else{
            $failure = false;
            $error_message = 'Something wents Wrong';
        } 

        if ($this->input->is_ajax_request()) {
            if ($failure) {
                $data['success'] = false;
                $data['error_message'] = $error_message;
            } else {
                $data['success'] = true;
                $data['resetForm'] = true;
                $data['success_message'] = $success_message;
            }
            $data['scrollToElement'] = true;
            echo json_encode($data);
            die;
        }
        
    }
    
    public function update_stripe_billing_information(){
        $login_id = $this->session->userdata('MEM_ID');     
        require_once(FCPATH.'application/libraries/vendor/autoload.php');
         $stripe = array(
            "secret_key"      => STRIPE_SECRET_KEY,
            "publishable_key" => STRIPE_PULBLISHABLE_KEY
        );
        \Stripe\Stripe::setApiKey($stripe['secret_key']);
        try {
            $token          = $_POST['stripeToken'];
            $email          = $_POST['emailAddress'];
            // Start: Create property owner account in stripe account
            $user_exist = $this->getStrpeUserinfo($this->session->userdata('MEM_ID'));
            if(empty($user_exist->stripe_external_account_id)){
                $customer =\Stripe\Account::create(
                                                  array(
                                                      "managed" => True,
                                                      "email" => $email,
                                                      "country" => "US"
                                                  )
                                                );
                $account_id = $customer->id;
                $this->updateStripeUserinfo($this->session->userdata('MEM_ID'),$account_id);
            }else{
                $account_id = $user_exist->stripe_external_account_id;
            }
            // End: Create property owner account in stripe account
            \Stripe\Stripe::setApiKey($stripe['secret_key']);
            $account = \Stripe\Account::retrieve($account_id);
            $account->external_accounts->create(array("external_account" => $token));
            $message = $account;   

            //print_R($account); exit;
            /********************** START: Add Billing card detail *****************************************/
            $card_info = $this->addStripeBillingCard($login_id, $token, $account_id);
            if($card_info != 'Success'){
                $message = $card_info;
                $success = false;
                $redirect = site_url('dashboard');
                $this->session->set_userdata('stripe_payment', $message);
                $this->session->set_userdata('stripe_payment_prop_id', $this->session->userdata('PROPERTY_ID'));
            }
            /********************** End: Add Billing card detail *****************************************/

            $success = true;
            $redirect = site_url('dashboard');

            //redirect(site_url('dashboard'));
        } catch (Exception $e) {
            $message = $e->getMessage();
            $success = false;
            $redirect = site_url('dashboard');
            $this->session->set_userdata('stripe_payment', $message);
            $this->session->set_userdata('stripe_payment_prop_id', $this->session->userdata('PROPERTY_ID'));
            //redirect(site_url('dashboard'));
        }
        //echo $message.$this->calculatePropertyChargesAmount(); exit;
        if ($this->input->is_ajax_request()) {
            $data['success'] = $success;
            $data['message'] = $message;
           //$data['redirect_url'] = $redirect;
            echo json_encode($data);
            die;
        }

    }


    function addStripeBillingCard($login_id,$token,$account_id){
        try {
            $token_detail = \Stripe\Token::retrieve($token);

            $conditions = $params = array();
            $conditions['user_id'] = $login_id;            
            $conditions['billing_card'] = 'Yes'; 
            $params['table'] = 'tbl_stripe_card_info';
            $this->common_model->delete($conditions, $params);

            $conditions = $params = array();
            $conditions['user_id'] = $login_id;
            $conditions['token'] = $token;
            $conditions['card_id'] = $token_detail['card']->id;
            $conditions['brand'] =  $token_detail['card']->brand;
            $conditions['address_city'] =  $token_detail['card']->address_city;
            $conditions['country'] =  $token_detail['card']->country;
            $conditions['exp_month'] =  $token_detail['card']->exp_month;
            $conditions['exp_year'] =  $token_detail['card']->exp_year;
            $conditions['funding'] =  $token_detail['card']->funding;
            $conditions['last_digits'] =  $token_detail['card']->last4;
            $conditions['fingerprint'] =  $token_detail['card']->fingerprint;
            $conditions['created']  = $token_detail->created;
            $conditions['stripe_external_account_id'] = $account_id; 
            $conditions['billing_card'] = 'Yes'; 
            $params['table'] = 'tbl_stripe_card_info';
            $last_id = $this->common_model->insert_data($conditions, $params);
            return 'Success';
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
    function getStripeBillingCardInfo($user_id){
        $conditions = $params = array();
        $params['table'] = 'tbl_stripe_card_info';
        $conditions['user_id'] = $user_id;
        $conditions['billing_card'] = 'Yes';
        $conditions['disable_card'] = 0;
        $params['order_by'] = 'id desc';
        $params['single_row'] = true;
        $info = $this->common_model->get_data($conditions, $params);
        return $info;
    }

    function verify_stripe_billing_information(){

        $this->form_validation->set_rules('first_name', 'First Name', 'trim|required');
        $this->form_validation->set_rules('last_name', 'Last Name', 'trim|required');
        $this->form_validation->set_rules('dob', 'Dob', 'trim|required');
        //print_r($_FILES); exit;
        if (empty($_FILES['verify_doc']['name']))
        {
            $this->form_validation->set_rules('verify_doc', 'Identity Document', 'trim|required');
        }
        $this->form_validation->set_rules('address_line_1', 'address_line_1', 'trim|required');
        $this->form_validation->set_rules('postal_code', 'postal_code', 'trim|required');
        $this->form_validation->set_rules('city', 'city', 'trim|required');
        $this->form_validation->set_rules('state', 'state', 'trim|required');
        $this->form_validation->set_rules('personal_id_number', 'personal_id_number', 'trim|required');
        if ($this->form_validation->run()) {
            require_once(FCPATH.'application/libraries/vendor/autoload.php');
             $stripe = array(
                "secret_key"      => STRIPE_SECRET_KEY,
                "publishable_key" => STRIPE_PULBLISHABLE_KEY
            );
            \Stripe\Stripe::setApiKey($stripe['secret_key']);

            $config['upload_path'] = './assets/uploads/verify_document/';
            $config['allowed_types'] = 'jpeg|jpg|png|txt|pdf|doc|docx|odt';
            $config['encrypt_name'] = TRUE;
            $this->load->library('upload', $config);
            if (!empty($_FILES['verify_doc']['name'])) {
                if (!$this->upload->do_upload('verify_doc')) {
                    $error = $this->upload->display_errors();
                    $error_message =$error;
                    $message = $error_message;
                    $success = false;
                    $redirect = site_url('account/billing');
                    $this->session->set_userdata('stripe_payment', $message);
                    $this->session->set_userdata('stripe_payment_prop_id', $this->session->userdata('PROPERTY_ID'));
                } else {
                    $comm_data = array('upload_data' => $this->upload->data());
                    $com_doc = $comm_data['upload_data']['file_name'];
                    //print_R($com_doc); exit;
                    //echo 'assets/uploads/verify_document/'.$com_doc; exit;
                    try {

                        $user_exist = $this->getStrpeUserinfo($this->session->userdata('MEM_ID'));
                        if(!empty($user_exist)){
                            $fp = fopen('assets/uploads/verify_document/'.$com_doc, 'r');
                            $file_obj = \Stripe\FileUpload::create(
                                                              array(
                                                                "purpose" => "identity_document",
                                                                "file" => $fp
                                                              ),
                                                              array(
                                                                "stripe_account" => $user_exist->stripe_external_account_id
                                                              )
                            );
                            $file = $file_obj->id;

                            $account = \Stripe\Account::retrieve($user_exist->stripe_external_account_id);

                            $time   =   strtotime($this->input->post('dob'));
                            $month  =   date("m",$time);
                            $day    =   date("d",$time);
                            $year   =   date("Y",$time);                    
                            $account->legal_entity->dob->day        = $day;
                            $account->legal_entity->dob->month      = $month;
                            $account->legal_entity->dob->year       = $year;
                            $account->legal_entity->first_name      = $this->input->post('first_name');
                            $account->legal_entity->last_name       = $this->input->post('last_name');
                            $account->legal_entity->type            = "individual";
                            $account->legal_entity->address->line1  = $this->input->post('address_line_1');
                            $account->legal_entity->address->postal_code = $this->input->post('postal_code'); //94111
                            $account->legal_entity->address->city   = $this->input->post('city');
                            $account->legal_entity->address->state  = $this->input->post('state');
                            //$account->legal_entity->ssn_last_4 = 1234;
                            $account->legal_entity->personal_id_number      = $this->input->post('personal_id_number'); //123456789
                            $account->legal_entity->verification->document  = $file;
                            $account->tos_acceptance->date                  = time();
                            $account->tos_acceptance->ip                    = '192.1.1.1';

                            $account->save();
                            $message = $account;
                            $success = true;
                        }else{
                            $message = 'No account_exist';
                            $success = false;
                        }
                    }catch (Exception $e) {
                        $message = $e->getMessage();
                        $success = false;
                        $redirect = site_url('account/billing');
                        $this->session->set_userdata('stripe_payment', $message);
                        $this->session->set_userdata('stripe_payment_prop_id', $this->session->userdata('PROPERTY_ID'));
                        //redirect(site_url('dashboard'));                
                    }
                }
            }

        } else {
            $message = validation_errors();
            $success = false;   
        }
        //echo "<pre>"; print_r($message); exit;
        if ($this->input->is_ajax_request()) {
            $data['success'] = $success;
            $data['message'] = $message;
           //$data['redirect_url'] = $redirect;
            echo json_encode($data);
            die;
        }
    }

    function checkAccountStatus($account){
        require_once(FCPATH.'application/libraries/vendor/autoload.php');
         $stripe = array(
            "secret_key"      => STRIPE_SECRET_KEY,
            "publishable_key" => STRIPE_PULBLISHABLE_KEY
        );
        \Stripe\Stripe::setApiKey($stripe['secret_key']);
        try {
            $info = \Stripe\Account::retrieve("$account");
            return $info->legal_entity['verification']->status;
        }catch (Exception $e) {
            return $e->getMessage();
        }
    }

    function get_varifivation_fields($country){
        require_once(FCPATH.'application/libraries/vendor/autoload.php');
         $stripe = array(
            "secret_key"      => STRIPE_SECRET_KEY,
            "publishable_key" => STRIPE_PULBLISHABLE_KEY
        );
        \Stripe\Stripe::setApiKey($stripe['secret_key']);
        try {

            $user_exist = $this->getStrpeUserinfo($this->session->userdata('MEM_ID'));
            if(!empty($user_exist)){

                $account =  \Stripe\CountrySpec::retrieve("$country");
                $message = $account->verification_fields->individual->minimum;
                $success = true;
                echo "<pre>"; print_r($message); 
                $country_list =\Stripe\CountrySpec::all(array("limit" => 20));

                
                foreach($country_list->data as $key=>$value){
                    //echo "<pre>"; print_r($value); exit; exit;
                    echo $value->id."<br>";
                }

                exit;
            }else{
                $message = 'No account_exist';
                $success = false;
            }
        }catch (Exception $e) {
            $message = $e->getMessage();
            $success = false;
            $redirect = site_url('dashboard');
            $this->session->set_userdata('stripe_payment', $message);
            $this->session->set_userdata('stripe_payment_prop_id', $this->session->userdata('PROPERTY_ID'));
            //redirect(site_url('dashboard'));
        }
        if ($this->input->is_ajax_request()) {
            $data['success'] = $success;
            $data['message'] = $message;
           //$data['redirect_url'] = $redirect;
            echo json_encode($data);
            die;
        }
    }

    function getStrpeUserinfo($user_id){
        $conditions = $params = array();
        $params['table'] = 'tbl_members';
        $conditions['mem_id'] = $user_id;
        $params['single_row'] = true;
        $info = $this->common_model->get_data($conditions, $params);
        return $info;
    }

    function updateStripeUserinfo($user_id,$stripe_external_account_id){
         $insert_data = array(
            'stripe_external_account_id'=> $stripe_external_account_id,
        );
        $conditions = $params = array();
        $conditions['value'] = $insert_data;
        $conditions['where']['mem_id'] = $user_id;
        $params['table'] = 'tbl_members';
        $this->common_model->update($conditions, $params);
    }

    function disableStripeCard(){
        if($this->input->post('deleteStripeCard')){
            /*            
            $conditions = $params = array();
            $params['table']        = 'tbl_stripe_card_info';
            $conditions['user_id']  = $this->session->userdata('MEM_ID');
            $conditions['id']       = $this->input->post('deleteStripeCard');
            $this->common_model->delete($conditions, $params);*/

            $conditions = $params = array();
            $params['table']        = 'tbl_stripe_card_info';
            $conditions['where']['user_id']  = $this->session->userdata('MEM_ID');
            $conditions['where']['id']       = $this->input->post('deleteStripeCard');
            $conditions['value']['disable_card']       = 1;
            $this->common_model->update($conditions, $params);


            /*
            $conditions = $params = array();
            $params['table']        = 'tbl_properties';
            $conditions['where']['prop_id']  = $this->input->post('property_id');
            $conditions['value']['step_completed']       = 3;
            $this->common_model->update($conditions, $params);*/

            $data['success'] = true;
            $data['resetForm'] = true;
            $data['message'] = 'You have been successfully deleted.';
            $data['redirect_url'] = site_url('account/billing');
        }else{
            $data['success'] = false;
            $data['resetForm'] = false;
            $data['message'] = 'Something wents wrong';
            $data['redirect_url'] = '';
        }
        //redirect(site_url('add-property'));
        if ($this->input->is_ajax_request()) {
            echo json_encode($data);
            die;
        }
    }

    function deleteStripeCard(){
        if($this->input->post('deleteStripeCard')){
            
            $conditions = $params = array();
            $params['table']        = 'tbl_stripe_card_info';
            $conditions['user_id']  = $this->session->userdata('MEM_ID');
            $conditions['id']       = $this->input->post('deleteStripeCard');
            $this->common_model->delete($conditions, $params);

            $conditions = $params = array();
            $params['table']        = 'tbl_properties';
            $conditions['where']['prop_id']  = $this->input->post('property_id');
            $conditions['value']['step_completed']       = 3;
            $this->common_model->update($conditions, $params);

            $data['success'] = true;
            $data['resetForm'] = true;
            $data['message'] = 'You have been successfully deleted.';
            $data['redirect_url'] = site_url('account/billing');
        }else{
            $data['success'] = false;
            $data['resetForm'] = false;
            $data['message'] = 'Something wents wrong';
            $data['redirect_url'] = '';
        }
        //redirect(site_url('add-property'));
        if ($this->input->is_ajax_request()) {
            echo json_encode($data);
            die;
        }
    }

    function updateStripeCardStatus(){
        if($this->input->post('card_id')){
            $conditions = $params            = array();
            $params['table']                 = 'tbl_stripe_card_info';
            $conditions['user_id']  = $this->session->userdata('MEM_ID');
            $conditions['id']       = $this->input->post('card_id');
            $params['single_row'] =true;
            $info = $this->common_model->get_data($conditions, $params);
            if(!empty($info)){
                if($info->status_by_landlord == 1){
                    $status = 0;
                }else{
                    $status = 1;
                }
                $conditions = $params            = array();
                $params['table']                 = 'tbl_stripe_card_info';
                $conditions['where']['user_id']  = $this->session->userdata('MEM_ID');
                $conditions['where']['id']       = $this->input->post('card_id');
                $conditions['value']['status_by_landlord']  = $status;
                $this->common_model->update($conditions, $params);      
            }

            $data['success'] = true;
            $data['resetForm'] = true;
            $data['message'] = 'You have been successfully updated.';
            $data['redirect_url'] = site_url('account/billing');
        }else{
            $data['success'] = false;
            $data['resetForm'] = false;
            $data['message'] = 'Something wents wrong';
            $data['redirect_url'] = '';
        }
        //redirect(site_url('add-property'));
        if ($this->input->is_ajax_request()) {
            echo json_encode($data);
            die;
        }
    }


    function oldpassword_check($old_password) {
        $old_password_hash = md5($old_password);
        $conditions = $params = array();
        $params['table'] = 'tbl_members';
        $conditions['mem_id'] = $this->session->userdata('MEM_ID');
        $params['fields'] = array('password');
        $params['single_row'] = TRUE;
        $Check = $this->common_model->get_data($conditions, $params);
        if ($old_password_hash != $Check->password) {
            $this->form_validation->set_message('oldpassword_check', 'Old password not match');
            return FALSE;
        }
        return TRUE;
    }




    
}
