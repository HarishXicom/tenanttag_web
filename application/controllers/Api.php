<?php

/**
 * Api Controller
 *
 * @project TenantTag APP
 * @version CI Php 2.3.8
 * @author : Lakhvinder Singh
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require(APPPATH . 'libraries/REST_Controller.php');

class Api extends REST_Controller {

    public function __construct() {
        parent::__construct();
        $ip = $_SERVER['REMOTE_ADDR'];
        ini_set('display_errors', 1);
        date_default_timezone_set('America/Los_Angeles');
        $this->load->model(array('member_model', 'mailsending_model'));
        if ($ip != '127.0.0.1') {
            $method_name = $this->router->fetch_method();
            $authCheck = $this->input->get_request_header('Authstring');
            $timestamp = $this->input->get_request_header('Timestamp');
            $deviceToken = $this->input->get_request_header('Devicetoken');
            if ($deviceToken == '') {
                $data = array(
                    'status' => FALSE,
                    'message' => 'Device token not found',
                    'data' => (object) array(),
                );
                $status_code = '404';
                $this->response($data, $status_code);
            }
            $auth_key = API_TOKEN;
            $authString = md5($method_name . $auth_key . $timestamp);

            //echo '<pre>';print_r($authString);die;
            if ($authString != $authCheck) {
                $data = array(
                    'status' => FALSE,
                    'message' => 'Authentication failed',
                    'data' => (object) array(),
                );
                $status_code = '404';
                $this->response($data, $status_code);
            }
        }
    }

    /* ||||||||||||||||||||||||||||||||||||||
     * |||||||   Common Functions  ||||||||||
     * ||||||||||||||||||||||||||||||||||||||
     */

    /**
     * Method Name : generate_otp
     * Author Name : Jasbir Singh
     * Description : return random 4 digit string
     */
    function generate_otp() {
        $strnew2 = 'abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $shuff2 = str_shuffle($strnew2);
        $otp = substr($shuff2, 0, 4);
        return $otp;
    }

    /**
     * Method Name : checkEmailExists
     * Author Name : Jasbir Singh
     * Description : check email exists in our database
     */
    public function checkEmailExists($email) {
        $conditions = $params = array();
        $params['table'] = 'tbl_members';
        $conditions['email'] = $email;
        $conditions['user_type'] = 'tenant';
        $params['single_row'] = TRUE;
        $loginCheck = $this->common_model->get_data($conditions, $params);
        return $loginCheck;
    }

    /**
     * Method Name : checkEmailExists
     * Author Name : Jasbir Singh
     * Description : check email exists in our database
     */
    public function checkUserExists($username) {
        $conditions = $params = array();
        $params['table'] = 'tbl_members';
        $conditions['slug'] = $username;
        $conditions['user_type'] = 'tenant';
        $params['single_row'] = TRUE;
        $loginCheck = $this->common_model->get_data($conditions, $params);
        return $loginCheck;
    }

    /**
     * Method Name : checkAccountExists
     * Author Name : Jasbir Singh
     * Description : validate login and password
     */
    public function checkAccountExists($username, $password) {
        $conditions = $params = array();
        $params['table'] = 'tbl_members';
        $conditions['email'] = $username;
        $conditions['user_type'] = 'tenant';
        $conditions['password'] = md5($password);
        $params['single_row'] = TRUE;
        $loginCheck = $this->common_model->get_data($conditions, $params);
        //echo $this->db->last_query();
        //print_r($conditions); exit;
        return $loginCheck;
    }

    /**
     * Method Name : checkUsernameExists
     * Author Name : Jasbir Singh
     * Description : validate login and password
     */
    public function checkUsernameExists($username, $password) {
        $conditions = $params = array();
        $params['table'] = 'tbl_members';
        $conditions['slug'] = $username;
        $conditions['user_type'] = 'tenant';
        $conditions['password'] = md5($password);
        $params['single_row'] = TRUE;
        $loginCheck = $this->common_model->get_data($conditions, $params);
        return $loginCheck;
    }

    /**
     * Method Name : checkSession
     * Author Name : Jasbir Singh
     * Description : check Session token exists in our database
     */
    public function checkSession($token) {
        if (empty($token)) {
            $data = array(
                'status' => FALSE,
                'message' => 'empty token',
                'data' => (object) array(),
            );
            $status_code = '201';
            $this->response($data, $status_code);
        }
        $conditions = $params = array();
        $params['table'] = 'tbl_api_sessions';
        $conditions['session_token'] = $token;
        $params['single_row'] = TRUE;
        $Check = $this->common_model->get_data($conditions, $params);
        if (empty($Check)) {
            $data = array(
                'status' => FALSE,
                'message' => 'current session expired',
                'data' => (object) array(),
            );
            $status_code = '201';
            $this->response($data, $status_code);
        } else {
            return $Check;
        }
    }

    /**
     * Method Name : setSession
     * Author Name : Jasbir Singh
     * Description : set Session token 
     */
    public function setSession($user_id, $device_type) {
        //session management
        $deviceToken = $this->input->get_request_header('Devicetoken');
        $conditions = $params = array();
        $params['table'] = 'tbl_api_sessions';
        $conditions['device_id'] = $deviceToken;
        $conditions['user_id'] = $user_id;
        $conditions['device_type'] = $device_type;
        $params['single_row'] = TRUE;
        $Check = $this->common_model->get_data($conditions, $params);
        $session_token = md5(uniqid());
        if (empty($Check)) { //updating session token
            $conditions = $params = array();
            $conditions['user_id'] = $user_id;
            $conditions['session_token'] = $session_token;
            $conditions['device_type'] = $device_type;
            $conditions['device_id'] = $deviceToken;
            $conditions['login_status'] = 1;
            $params['table'] = 'tbl_api_sessions';
            $this->common_model->insert_data($conditions, $params);
        } else {                               //inserting session token
            $conditions = $params = array();
            $conditions['where']['device_id'] = $deviceToken;
            $conditions['where']['user_id'] = $user_id;
            $conditions['where']['device_type'] = $device_type;
            $conditions['value']['session_token'] = $session_token;
            $conditions['login_status'] = 1;
            $params['table'] = 'tbl_api_sessions';
            $this->common_model->update($conditions, $params);
        }
        return $session_token;
    }

    /**
     * Method Name : set_upload_options
     * Author Name : Jasbir Singh
     * Description : config photo upload
     */
    function set_upload_options($id, $folder, $type) {
        $config = array();
        $config['upload_path'] = $folder . "/" . $type . "_" . $id;
        $config['allowed_types'] = '*';
        $config['overwrite'] = FALSE;
        $config['encrypt_name'] = TRUE;
        if (!is_dir($folder . "/" . $type . "_" . $id)) {
            mkdir($folder . "/" . $type . "_" . $id, 0777, TRUE);
        }
        return $config;
    }

    /**
     * Method Name : set_profile_options
     * Author Name : Jasbir Singh
     * Description : config profile photo upload
     */
    function set_profile_options($user_id) {

        $config = array();
        $config['upload_path'] = './assets/profile_images/user_' . $user_id;
        $config['allowed_types'] = '*';
        $config['overwrite'] = FALSE;
        $config['encrypt_name'] = TRUE;

        if (!is_dir('assets/profile_images/user_' . $user_id)) {
            mkdir('assets/profile_images/user_' . $user_id, 0777, TRUE);
        }
        return $config;
    }

    /**
     * Method Name : getUserInfo
     * Author Name : Jasbir Singh
     * Description : get basic details of user
     */
    public function getUserInfo($user_id) {
        $conditions = $params = array();
        $params['table'] = 'tbl_members';
        $conditions['mem_id'] = $user_id;
        $params['single_row'] = TRUE;
        $params['fields'] = array('mem_id', 'synapse_user_id','synapse_fingerprint','synapse_ip_address','stripe_customer_id','stripe_external_account_id', 'email', 'first_name', 'last_name', 'mobile_no', 'dob', 'app_verified', 'move_status','third_party_sharing');
        $info = $this->common_model->get_data($conditions, $params);
        return $info;
    }

    /**
     * Method Name : getLandlordInfo
     * Author Name : Jasbir Singh
     * Description : get basic details of user
     */
    public function getLandlordInfo($user_id) {
        $conditions = $params = array();
        $params['table'] = 'tbl_members';
        $conditions['mem_id'] = $user_id;
        $params['single_row'] = TRUE;
        $params['fields'] = array('mem_id', 'email', 'first_name', 'last_name', 'mobile_no', 'company_name');
        $info = $this->common_model->get_data($conditions, $params);
        return $info;
    }

    /* ||||||||||||||||||||||||||||||||||||||
     * ||||||| Login SignUp API's  ||||||||||
     * ||||||||||||||||||||||||||||||||||||||
     */

    /**
     * Method Name : signin_post
     * Author Name : Jasbir Singh
     * Description : Autheticate and log in user into the app
     */
    public function signin_post() {
        if ($this->post('email') != '' && $this->post('password') != '') {
            $username = $this->post('email');
            $password = $this->post('password');

            //check email exists
            $user_data = $this->checkAccountExists($username, $password);
            if (empty($user_data)) {
                $user_data = $this->checkUsernameExists($username, $password);
            }
            if (empty($user_data)) {
                $data = array(
                    'status' => FALSE,
                    'message' => 'invalid username/email or password',
                    'data' => (object) array(),
                );
                $status_code = '202';
                $this->response($data, $status_code);
            } elseif ($user_data->account_closed_by_member == 'Yes') {
                $data = array(
                    'status' => FALSE,
                    'message' => 'inactive user account',
                    'data' => (object) array(),
                );
                $status_code = '202';
                $this->response($data, $status_code);
            } elseif ($user_data->status == 'Inactive') {
                $data = array(
                    'status' => FALSE,
                    'message' => 'account closed by administrator',
                    'data' => (object) array(),
                );
                $status_code = '202';
                $this->response($data, $status_code);
            } else {
//set session for device     
                $property_details = $this->propertyDetails($user_data->mem_id);
                if ($property_details->status == 'Inactive') {
                    $data = array(
                        'status' => FALSE,
                        'message' => 'inactive property',
                        'data' => (object) array(),
                    );
                    $status_code = '202';
                    $this->response($data, $status_code);
                }
                $owner = $this->getUserInfo($property_details->owner_id);
                $session_token = $this->setSession($user_data->mem_id, $this->post('deviceType'));
                $info = new stdClass();
                $info = $this->getUserInfo($user_data->mem_id);
                $info->sessionId = $session_token;
                $info->propertyId = $property_details->prop_id;
                //send otp to landlord                
                /* if ($user_data->app_verified == '0') {
                  $number = $this->sendlandlordOTP($user_data->mem_id);
                  $msg = 'OTP sent to landlord';
                  $info->landlord_number = $number;
                  } else {
                  $msg = 'User successfully logged in';
                  $info->landlord_number = '';
                  } */

                $msg = 'User successfully logged in';
                $info->landlord_number = $owner->mobile_no;
                ;
                $data = array(
                    'status' => TRUE,
                    'message' => $msg,
                    'data' => $info
                );
                $status_code = '200';
                $this->response($data, $status_code);
            }
        } else {
            $data = array(
                'status' => FALSE,
                'message' => 'Post data empty',
                'data' => (object) array(),
            );
            $status_code = '404';
            $this->response($data, $status_code);
        }
    }

    /**
     * Method Name : forgot_post
     * Author Name : Jasbir Singh
     * Description :send email to user to recover password
     */
    public function forgotpassword_post() {
        if ($this->post('email') != '') {
            //$loginCheck = $this->checkEmailExists($this->post('email'));
            $loginCheck = $this->checkUserExists($this->post('email'));
            if (empty($loginCheck)) {
                $loginCheck = $this->checkEmailExists($this->post('email'));
            }
            if (empty($loginCheck)) {
                $data = array(
                    'status' => FALSE,
                    'message' => 'Username/Email not exists in database',
                    'data' => (object) array(),
                );
                $status_code = '202';
                $this->response($data, $status_code);
            } else {
                $strnew2 = 'abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
                $shuff2 = str_shuffle($strnew2);
                $verification_code = substr($shuff2, 0, 8);
                $conditions = $params = array();
                $conditions['value']['verification_code'] = $verification_code;
                $conditions['where']['mem_id'] = $loginCheck->mem_id;
                $params['table'] = 'tbl_members';
                $this->common_model->update($conditions, $params);
                $memRec = $this->member_model->customCheckUsername($loginCheck->mem_id);
                $this->mailsending_model->userforgotPasswordEmail($memRec);
                $data = array(
                    'status' => TRUE,
                    'message' => 'Instructions sent by text and email.',
                );
                $status_code = '200';
                $this->response($data, $status_code);
            }
        } else {
            $data = array(
                'status' => FALSE,
                'message' => 'Post data empty',
                'data' => (object) array(),
            );
            $status_code = '404';
            $this->response($data, $status_code);
        }
    }


    /**
     * Method Name : forgot_post
     * Author Name : Jasbir Singh
     * Description :send email to user to recover password
     */
    public function sendMailToLandlord_post() {
        if ($this->post('email') != '') {
                $name = $this->post('name');
                $email = $this->post('email');
                $text = $this->post('text');
                $data = array(
                            'name'=>$name,
                            'email'=>$email,
                            'text'=>$text
                            );
                $this->mailsending_model->sendMailToLandlord($data);
                $data = array(
                    'status' => TRUE,
                    'message' => 'Sent', 
                );
                $status_code = '200';
                $this->response($data, $status_code);
            
        } else {
            $data = array(
                'status' => FALSE,
                'message' => 'Post data empty',
                'data' => (object) array(),
            );
            $status_code = '404';
            $this->response($data, $status_code);
        }
    }

    /**
     * Method Name : userHome
     * Author Name : Jasbir Singh
     * Description : returns all details required for home screen
     */
    public function userHome_get() {
        $session_token = $this->input->get_request_header('Sessionid');
        $user = $this->checkSession($session_token);
        $result = $photos = $docs = $cdoc = array();
        $info = new stdClass();
        $login_id = $user->user_id;
        $property_details = $this->propertyDetails($login_id);
        $services = $this->propertyServices($property_details->prop_id);
        $photosbyTenant = $this->getPropertyPhotos($login_id, $property_details->prop_id);
        $photosbylandlord = $this->getPropertyPhotos('0', $property_details->prop_id);
        $docs = $this->getLeaseDocs($login_id, $property_details->prop_id); 
        //$docs = $this->getSingleLeaseDocs($login_id, $property_details->prop_id);
        if (!empty($property_details)) {
            $address = $communityAddress = $communityPhoneNo = '';
            if ($property_details->address1 != '') {
                $address .=$property_details->address1;
            }
            if ($property_details->address2 != '') {
                $address .=" " . $property_details->address2;
            }
            if ($property_details->unit_number != 0) {
                $address .=" " . $property_details->unit_number;
            }
            if ($property_details->city != '') {
                $address .=" " . $property_details->city . ",";
            }
            if ($property_details->region_name != '') {
                $address .=" " . $property_details->region_name;
            }
            if ($property_details->zip != '') {
                $address .=" " . $property_details->zip;
            }
            if ($property_details->community_status != 'No') {
                if ($property_details->community != '') {
                    $communityAddress .= $property_details->community . " Community";
                }
                if ($property_details->gate_code != '') {
                    $communityAddress .=" Gate: " . $property_details->gate_code;
                }
                if ($property_details->unit_po_box != 0) {
                    $communityAddress .=" PO Box : " . $property_details->unit_po_box;
                }
                if ($property_details->community_managing_company != '') {
                    $communityAddress .= " " . $property_details->community_managing_company;
                }
                if ($property_details->community_contact_number != '') {
                    $communityPhoneNo = $property_details->community_contact_number;
                }
                if ($property_details->community_doc != '') {
                    $cdoc = array(array('url' => base_url() . "assets/uploads/community_documents/" . $property_details->community_doc));
                }
            }
//property address
            $info->propertyDetails = array(
                'propertyAddress' => $address,
                'communityAddress' => $communityAddress,
                'communityPhoneNo' => $communityPhoneNo,
                'coordinates' => array('lat' => $property_details->latitude, 'lng' => $property_details->longitude)
            );
//property amenities
            $info->amenities = array(
                array('name' => 'Garbage Pickup', 'day' => $property_details->garbage_day),
                array('name' => 'Recycle Pickup', 'day' => $property_details->recycle_day),
                array('name' => 'Yard Waste Pickup', 'day' => $property_details->yard_waste_day),
            );
//property services
            $amenties = $this->getAmenties();
            $amen = array();
            if ($property_details->amenties != '') {
                $amen = explode(',', $property_details->amenties);
            }
            /* echo '<pre>';print_r($services);die;
              if (!empty($amenties)) {
              foreach ($amenties as $value) {
              if (in_array($value->id, $amen)) {
              array_push($result, array('serviceName' => $value->name, 'serviceProvider' => 'Yes', 'phone' => ''));
              } else {
              array_push($result, array('serviceName' => $value->name, 'serviceProvider' => 'No', 'phone' => ''));
              }
              }
              } */
            if (!empty($services)) {
                foreach ($services as $service) {
                    array_push($result, array('serviceName' => $service->service, 'serviceProvider' => $service->company, 'phone' => $service->contact));
                }
            }

            //  array_push($result, array('serviceName' => 'Smoke Detectors', 'serviceProvider' => $property_details->safety_equipment1, 'phone' => ''));
            //   array_push($result, array('serviceName' => 'Fire Extinguishers', 'serviceProvider' => $property_details->safety_equipment2, 'phone' => ''));
            //   array_push($result, array('serviceName' => 'Heat Type', 'serviceProvider' => $property_details->heat_type, 'phone' => '', 'filter' => $property_details->heat_filter_size));
            //   array_push($result, array('serviceName' => 'A/C Type', 'serviceProvider' => $property_details->ac_type, 'phone' => '', 'filter' => $property_details->ac_filter_size));
            if ($property_details->pest_service_provider != '') {
                array_push($result, array('serviceName' => 'Pest Control Home', 'serviceProvider' => $property_details->pest_service_provider != '' ? $property_details->pest_service_provider : 'No', 'phone' => $property_details->pest_service_provider_number));
            }
            if ($property_details->yard_service_provider != '') {
                array_push($result, array('serviceName' => 'Yard Service', 'serviceProvider' => $property_details->yard_service_provider != '' ? $property_details->yard_service_provider : 'No', 'phone' => $property_details->yard_service_provider_number));
            }
        }
        $info->services = $result;
        $info->imagessAttached = array('landlord' => $photosbylandlord, 'tenant' => $photosbyTenant);
        $info->documentsAttached = array('lease' => $docs, 'community' => $cdoc);
        $data = array(
            'status' => TRUE,
            'message' => 'details found',
            'data' => $info
        );
        $status_code = '200';
        $this->response($data, $status_code);
    }

    /**
     * Method Name : propertyDetails
     * Author Name : Jasbir Singh
     * Description : return details of particular property
     */
    public function propertyDetails($tenant_id) {
        $conditions = $params = array();
        $params['complex'] = true;
        $params['single_row'] = true;
        $conditions['tables'] = array(
            'lease' => 'tbl_properties_lease_detail',
            'property' => 'tbl_properties',
            'state' => 'tbl_region'
        );
        $conditions['table'] = 'lease';
        $conditions['on']['property'] = array(
            'sign' => '=',
            'column' => 'prop_id',
            'alias_column' => 'property_id',
            'alias_other' => 'lease',
        );
        $conditions['on']['state'] = array(
            'sign' => '=',
            'column' => 'region_id',
            'alias_column' => 'state_id',
            'alias_other' => 'property',
        );
        $conditions['where']['tenant_id']['alias'] = 'lease';
        $conditions['where']['tenant_id']['value'] = $tenant_id;

        $params['single_row'] = TRUE;
        $params['fields'] = array('property.*', 'state.region_name', 'lease.lease_start_date', 'lease.lease_end_date', 'lease.rent_amount');
        $info = $this->common_model->get_data($conditions, $params);
        return $info;
    }

    /**
     * Method Name : propertyServices
     * Author Name : Jasbir Singh
     * Description : return services associated with particular property
     */
    public function propertyServices($prop_id) {
        $conditions = $params = array();
        $params['complex'] = true;
        $conditions['tables'] = array(
            'service' => 'tbl_property_services',
            'all_services' => 'tbl_services',
        );
        $conditions['table'] = 'service';
        $conditions['on']['all_services'] = array(
            'sign' => '=',
            'column' => 'id',
            'alias_column' => 'service_id',
            'alias_other' => 'service',
        );

        $conditions['where']['property_id']['alias'] = 'service';
        $conditions['where']['property_id']['value'] = $prop_id;
        $info = $this->common_model->get_data($conditions, $params);

        return $info;
    }

    /**
     * Method Name : getPropertyPhotos
     * Author Name : Jasbir Singh
     * Description : return photos of property
     */
    public function getPropertyPhotos($user_id, $property_id) {
        $conditions = $params = array();
        $params['table'] = 'tbl_property_photos';
        $conditions['tenant_id'] = $user_id;
        $conditions['property_id'] = $property_id;
        /* $conditions['tbl_property_photos'] = array(
          'operator' => 'OR',
          'sign' => '=',
          'key' => 'tenant_id',
          'value' => $user_id
          ); */
        $params['fields'] = array('id', 'CONCAT("' . base_url() . '", image) AS url', 'CONCAT("' . base_url() . '", thumb) AS thumb');
        $params['order_by'] = 'id DESC';
        $info = $this->common_model->get_data($conditions, $params);
        return $info;
    }

    /**
     * Method Name : getLeaseDocs
     * Author Name : Jasbir Singh
     * Description : return lease documents
     */
    public function getLeaseDocs($user_id, $property_id) {
        $conditions = $params = array();
        $params['table'] = 'tbl_lease_docs';
        $conditions['tenant_id'] = $user_id;
        $conditions['property_id'] = $property_id;
        $params['fields'] = array('id', 'CONCAT("' . base_url() . 'assets/uploads/lease_documents/", lease_doc) AS url');
        $info = $this->common_model->get_data($conditions, $params);
        return $info;
    }

        /**
     * Method Name : getLeaseDocs
     * Author Name : Jasbir Singh
     * Description : return lease documents
     */
    public function getSingleLeaseDocs($user_id, $property_id) {
        $conditions = $params = array();
        $params['table'] = 'tbl_lease_docs';
        $params['order_by'] = 'id desc';
        $params['single_row'] = true;
        $conditions['tenant_id'] = $user_id;
        $conditions['property_id'] = $property_id;
        $params['fields'] = array('id', 'CONCAT("' . base_url() . 'assets/uploads/lease_documents/", lease_doc) AS url');
        $info = $this->common_model->get_data($conditions, $params);
        $lease_doc = empty($info) ? '' : $info->url;
        return $lease_doc;
    }

    /**
     * Method Name : getAmenties
     * Author Name : Jasbir Singh
     * Description : get all amenties
     */
    public function getAmenties() {
        $conditions = $params = array();
        $params['table'] = 'tbl_amenties';
        $params['fields'] = array('id', 'name');
        $info = $this->common_model->get_data($conditions, $params);
        return $info;
    }

    /**
     * Method Name : messages
     * Author Name : Jasbir Singh
     * Description : return message logs
     */
    public function messages_get() {
        $session_token = $this->input->get_request_header('Sessionid');
        $user = $this->checkSession($session_token);
        $login_id = $user->user_id;
        //check move out status      
        $this->tenantMoveStatus($login_id);
        $params = $conditions = $feeds = array();
        $params['cnt'] = true;
        $params['table'] = 'tbl_sms_logs';
        $params['order_by'] = 'id DESC';
        if ($this->get('searchTerm') != '') {
            $conditions['message'] = array(
                'operator' => 'OR',
                'sign' => 'LIKE',
                'key' => 'message',
                'value' => $this->get('searchTerm')
            );
        }
        $conditions['tenant_id'] = $login_id;
        $conditions['is_deleted'] = '0';
        if ($this->get('searchDate') != '') {
            $dt = date('Y-m-d', strtotime($this->get('searchDate')));
            $conditions['DATE(date_created)'] = $dt;
        }

        $message_count = $this->common_model->get_data($conditions, $params);
        unset($params['cnt']);
        $params['limit'] = $this->get('limit');
        $params['offset'] = $this->get('offset');
        $params['fields'] = array('id', 'message', 'date_created', 'type');
        $messages = $this->common_model->get_data($conditions, $params);
        $info = new stdClass();
        $info->total = $message_count;
        $info->messages = $messages;
        $data = array(
            'status' => TRUE,
            'message' => 'Found total ' . $message_count . " messages",
            'data' => $info,
        );
        $status_code = '200';
        $this->response($data, $status_code);
    }

    /**
     * Method Name : tenantMoveStatus
     * Author Name : Jasbir Singh
     * Description : return tenant status
     */
    public function tenantMoveStatus($tenant_id) {
        $info = $this->getUserInfo($tenant_id);
        if ($info->move_status == 2) {
            $data = array(
                'status' => TRUE,
                'message' => 'Tenant Moved Out',
                'move_status' => 2
            );
            $status_code = '200';
            $this->response($data, $status_code);
        } else {
            return true;
        }
    }

    /**
     * Method Name : landlordDetails
     * Author Name : Jasbir Singh
     * Description : return lanlord details
     */
    public function landlordDetails_get() {
        $session_token = $this->input->get_request_header('Sessionid');
        $user = $this->checkSession($session_token);
        $login_id = $user->user_id;
//check move out status      
        $this->tenantMoveStatus($login_id);
        $params = $conditions = $feeds = array();
        $params['table'] = 'tbl_properties_lease_detail';
        $conditions['tenant_id'] = $login_id;
        $params['single_row'] = true;
        $params['fields'] = array('owner_id', 'property_id');
        $landlord = $this->common_model->get_data($conditions, $params);

        $address = '';
        $property_details = $this->propertyMailingDetails($landlord->owner_id);

        if (!empty($property_details)) {
            if ($property_details->mailing_address1 != '') {
                $address .=$property_details->mailing_address1;
            }
            if ($property_details->mailing_address2 != '') {
                $address .=" " . $property_details->mailing_address2;
            }
            if ($property_details->mailing_unit != 0) {
                $address .=" " . $property_details->mailing_unit;
            }
            if ($property_details->mailing_city != '') {
                $address .=" " . $property_details->mailing_city . ",";
            }
            if ($property_details->region_name != '') {
                $address .=" " . $property_details->region_name;
            }
            if ($property_details->mailing_zip != '') {
                $address .=" " . $property_details->mailing_zip;
            }
        }
        $info = new stdClass();
        $info = $this->getLandlordInfo($landlord->owner_id);
        $info->address = $address;
        $data = array(
            'status' => TRUE,
            'message' => 'success',
            'data' => $info,
        );
        $status_code = '200';
        $this->response($data, $status_code);
    }

    /**
     * Method Name : propertyMailingDetails
     * Author Name : Jasbir Singh
     * Description : return mailing details of particular property
     */
    public function propertyMailingDetails($id) {
        $conditions = $params = array();
        $params['complex'] = true;
        $params['single_row'] = true;
        $conditions['tables'] = array(
            'property' => 'tbl_members',
            'state' => 'tbl_region'
        );
        $conditions['table'] = 'property';
        $conditions['on']['state'] = array(
            'sign' => '=',
            'column' => 'region_id',
            'alias_column' => 'mailing_state_id',
            'alias_other' => 'property',
        );
        $conditions['where']['mem_id']['alias'] = 'property';
        $conditions['where']['mem_id']['value'] = $id;
        $params['single_row'] = TRUE;
        $params['fields'] = array('property.*', 'state.region_name');
        $info = $this->common_model->get_data($conditions, $params);
        return $info;
    }

    /**
     * Method Name : editUserInfo_post
     * Author Name : Jasbir Singh
     * Description :update user profile
     */
    public function editUserInfo_post() {
        $session_token = $this->input->get_request_header('Sessionid');
        $user = $this->checkSession($session_token);
        if ($this->post()) {
            $conditions = $params = array();
            if ($this->post('password')) {
                $pass = $this->post('password');
                $conditions['value']['password'] = md5($pass);
                $conditions['value']['tpass'] = $pass;
            }
            if ($this->post('first_name')) {
                $conditions['value']['first_name'] = $this->post('first_name');
            }
            if ($this->post('last_name')) {
                $conditions['value']['last_name'] = $this->post('last_name');
            }
            if ($this->post('dob')) {
                $conditions['value']['dob'] = $this->post('dob');
            }
            if ($this->post('third_party_sharing')) {
                $conditions['value']['third_party_sharing'] = $this->post('third_party_sharing');
            }
            if ($this->post('synapse_user_id')) {
                $conditions['value']['synapse_user_id'] = $this->post('synapse_user_id');
            }
            if ($this->post('synapse_fingerprint')) {
                $conditions['value']['synapse_fingerprint'] = $this->post('synapse_fingerprint');
            }
            if ($this->post('synapse_ip_address')) {
                $conditions['value']['synapse_ip_address'] = $this->post('synapse_ip_address');
            }   
            
            $conditions['where']['mem_id'] = $user->user_id;
            $params['table'] = 'tbl_members';
            $this->common_model->update($conditions, $params);
            $info = $this->getUserInfo($user->user_id);
            $data = array(
                'status' => TRUE,
                'message' => 'User information successfully updated',
                'data' => $info,
            );
            $status_code = '200';
            $this->response($data, $status_code);
        } else {
            $data = array(
                'status' => FALSE,
                'message' => 'Post data empty',
                'data' => (object) array(),
            );
            $status_code = '404';
            $this->response($data, $status_code);
        }
    }

    /**
     * Method Name : maintenanceCategories
     * Author Name : Jasbir Singh
     * Description : return maintenance Categories
     */
    public function maintenanceCategories_get() {
        $session_token = $this->input->get_request_header('Sessionid');
        $user = $this->checkSession($session_token);
        $categories = array();
        $info = new stdClass();
        $login_id = $user->user_id;
        //check move out status      
        $this->tenantMoveStatus($login_id);
        $property_details = $this->propertyDetails($login_id);
        if ($property_details->tmaintenance == 'No') {
            $data = array(
                'status' => TRUE,
                'message' => 'tmaintenance off',
                'tmaintenance' => FALSE
            );
            $status_code = '200';
            $this->response($data, $status_code);
        } else {
            $services = get_vendor_services();
            if (!empty($services)) {
                foreach ($services as $service) {
                    $categories[] = array(
                        'categoryId' => $service->t_id,
                        'categoryName' => $service->name,
                    );
                }
            }
            $data = array(
                'status' => TRUE,
                'message' => 'tmaintenance on',
                'tmaintenance' => TRUE,
                'data' => $categories
            );
            $status_code = '200';
            $this->response($data, $status_code);
        }
    }

    /**
     * Method Name : submitMaintenanceReport_post
     * Author Name : Jasbir Singh
     * Description :submit Maintenance Report
     */
    public function submitMaintenanceReport_post() {
        $session_token = $this->input->get_request_header('Sessionid');
        $user = $this->checkSession($session_token);
        $login_id = $user->user_id;
        if ($this->post('categoryId') != '' && $this->post('message') != '') {
//get property info 
            $all_info = $this->allDetails($login_id);
//get vendor
            $params = $conditions = array();
            $params['table'] = 'tbl_vendors';
            $conditions['property_id'] = $all_info->prop_id;
            $params['fields'] = array('email', 'mobile_no', 'company_name');
            $vendor_info = $this->common_model->get_data($conditions, $params);
//get category name
            $params = $conditions = array();
            $params['table'] = 'tbl_tservices';
            $conditions['t_id'] = $this->post('categoryId');
            $params['fields'] = array('name');
            $params['single_row'] = TRUE;
            $service_name = $this->common_model->get_data($conditions, $params);
//insert report into database            
            $conditions = $params = array();
            $conditions['category_id'] = $this->post('categoryId');
            $conditions['tenant_id'] = $login_id;
            $conditions['property_id'] = $all_info->prop_id;
            $conditions['owner_id'] = $all_info->owner_id;
            $conditions['report'] = $this->post('message');
            $conditions['status'] = 0;
            $conditions['date_created'] = date('Y-m-d H:i:s');
            $conditions['date_modified'] = date('Y-m-d H:i:s');
            $params['table'] = 'tbl_maintenance_reports';
            $this->common_model->insert_data($conditions, $params);

//send email to vendor
            if (!empty($vendor_info)) {
                $mail_ids = array($all_info->lemail);
                foreach ($vendor_info as $vendor_emails) {
                    $mail_ids[] = $vendor_emails->email;
                }
            }
            $sms = $this->post('message');
            $this->mailsending_model->sendMaintenanceMails($mail_ids, $all_info, $sms, $service_name->name);
//send sms     
            $this->load->helper('twilio_helper');  // references library/twilio_helper.php
            $service = get_twilio_service();
            try {
                $mnumber = $all_info->lmobile;
                $address = $all_info->address1 . ' ' . $all_info->address2 . ' ' . $all_info->city . ', ' . $all_info->region_name . ' ' . $all_info->zip;
                $number = "+1" . preg_replace("/[^0-9]/", "", $mnumber);
                $msg = "A Maintenance Request has been made by " . $all_info->tfname . " " . $all_info->tlname . " at " . $address . ", " . $all_info->tmobile . " : " . urldecode($sms) . " -" . SIGNATURE;
                // $service->account->sms_messages->create("+19046743077", "$number", "$msg", array());
                $service->account->messages->create(array(
                    'To' => $number,
                    'From' => "+19046743077",
                    'Body' => $msg));
            } catch (Exception $e) {
                $file = 'sms_error_logs.txt';
                if (file_exists($file)) {
                    $current = file_get_contents($file);
                    $current .= $e->getMessage() . "\n";
                    file_put_contents($file, $current);
                }
            }
//insert into sms log
            $conditions = $params = array();
            $conditions['type'] = 1;
            $conditions['tenant_id'] = $login_id;
            $conditions['property_id'] = $all_info->prop_id;
            $conditions['owner_id'] = $all_info->owner_id;
            $conditions['message'] = $this->post('message');
            $conditions['number'] = $number;
            $conditions['date_created'] = date('Y-m-d H:i:s');
            $params['table'] = 'tbl_sms_logs';
            $this->common_model->insert_data($conditions, $params);

            $data = array(
                'status' => TRUE,
                'message' => 'Request Submitted Successful',
            );
            $status_code = '200';
            $this->response($data, $status_code);
        } else {
            $data = array(
                'status' => FALSE,
                'message' => 'Post data empty',
                'data' => (object) array(),
            );
            $status_code = '404';
            $this->response($data, $status_code);
        }
    }

    /**
     * Method Name : deleteMessage_post
     * Author Name : Jasbir Singh
     * Description :delete message
     */
    public function deleteMessage_post() {
        $session_token = $this->input->get_request_header('Sessionid');
        $user = $this->checkSession($session_token);
        if ($this->post('messageId')) {
            $conditions = $params = array();
            $conditions['value']['is_deleted'] = 1;
            $conditions['where']['tenant_id'] = $user->user_id;
            $conditions['where']['id'] = $this->post('messageId');
            $params['table'] = 'tbl_sms_logs';
            $this->common_model->update($conditions, $params);
            $data = array(
                'status' => TRUE,
                'message' => 'Message deleted successfully',
            );
            $status_code = '200';
            $this->response($data, $status_code);
        } else {
            $data = array(
                'status' => FALSE,
                'message' => 'Post data empty',
                'data' => (object) array(),
            );
            $status_code = '404';
            $this->response($data, $status_code);
        }
    }

    /**
     * Method Name : editPhone_post
     * Author Name : Jasbir Singh
     * Description : send otp to mobile number for verification
     */
    public function editPhone_post() {
        $session_token = $this->input->get_request_header('Sessionid');
        $user = $this->checkSession($session_token);
        if ($this->post('phoneNo')) {
            $otp = $this->generate_otp();
            $conditions = $params = array();
            $conditions['value']['otp'] = $otp;
            $conditions['where']['mem_id'] = $user->user_id;
            $params['table'] = 'tbl_members';
            $this->common_model->update($conditions, $params);
            $this->load->helper('twilio_helper');  // references library/twilio_helper.php
            $service = get_twilio_service();
            try {
                $mnumber = $this->post('phoneNo');
                $number = "+1" . preg_replace("/[^0-9]/", "", $mnumber);
                $msg = $otp . " is your TenantTag verification code.";
                $service->account->sms_messages->create("+19046743077", "$number", "$msg", array());

                $data = array(
                    'status' => TRUE,
                    'message' => 'OTP sent to user',
                );
                $status_code = '200';
                $this->response($data, $status_code);
            } catch (Exception $e) {
                $file = 'sms_error_logs.txt';
                if (file_exists($file)) {
                    $current = file_get_contents($file);
                    $current .= $e->getMessage() . "\n";
                    file_put_contents($file, $current);
                }
                $data = array(
                    'status' => FALSE,
                    'message' => 'Unable to send OTP',
                );
                $status_code = '200';
                $this->response($data, $status_code);
            }
        } else {
            $data = array(
                'status' => FALSE,
                'message' => 'Post data empty',
                'data' => (object) array(),
            );
            $status_code = '404';
            $this->response($data, $status_code);
        }
    }

    /**
     * Method Name : editEmail_post
     * Author Name : Lakhvinder Singh
     * Description : send otp to mobile number for verification
     */
    public function editEmail_post() {
        $session_token = $this->input->get_request_header('Sessionid');
        $user = $this->checkSession($session_token);
        if ($this->post('email')) {
            $user_info = $this->getUserInfo($user->user_id);

            $email_otp = $this->generate_otp();
            $conditions = $params = array();
            $conditions['value']['email_otp'] = $email_otp;
            $conditions['where']['mem_id'] = $user->user_id;
            $params['table'] = 'tbl_members';
            $this->common_model->update($conditions, $params);
            
            $name = $user_info->first_name .' ' .$user_info->last_name ;
            $this->mailsending_model->tenantEmailUpdate($name, $this->post('email'), $email_otp);
            $data = array(
                'status' => TRUE,
                'message' => 'OTP sent to email',
            );
            $status_code = '200';
            $this->response($data, $status_code);
        } else {
            $data = array(
                'status' => FALSE,
                'message' => 'Post data empty',
                'data' => (object) array(),
            );
            $status_code = '404';
            $this->response($data, $status_code);
        }
    }

    /**
     * Method Name : verifyOtpForEmail_post
     * Author Name : Lakhvinder Singh
     * Description : verify otp and update email
     */
    public function verifyOtpForEmail_post() {
        $session_token = $this->input->get_request_header('Sessionid');
        $user = $this->checkSession($session_token);
        if ($this->post('email') && $this->post('email_otp')) {
            $userOtp = $this->post('email_otp');
            $conditions = $params = array();
            $params['table'] = 'tbl_members';
            $conditions['mem_id'] = $user->user_id;
            $params['fields'] = array('email_otp');
            $params['single_row'] = TRUE;
            $otpCheck = $this->common_model->get_data($conditions, $params);
            if ($otpCheck->email_otp == $userOtp) {
                $conditions = $params = array();
                $conditions['value']['email_otp'] = '';
                $conditions['value']['email'] = $this->post('email');
                $conditions['where']['mem_id'] = $user->user_id;
                $params['table'] = 'tbl_members';
                $this->common_model->update($conditions, $params);
                $data = array(
                    'status' => TRUE,
                    'message' => 'Email updated',
                    'data' => array('email' => $this->post('email'))
                );
                $status_code = '200';
                $this->response($data, $status_code);
            } else {
                $data = array(
                    'status' => FALSE,
                    'message' => 'OTP not matched',
                );
                $status_code = '200';
                $this->response($data, $status_code);
            }
        } else {
            $data = array(
                'status' => FALSE,
                'message' => 'Post data empty',
                'data' => (object) array(),
            );
            $status_code = '404';
            $this->response($data, $status_code);
        }
    }

    /**
     * Method Name : verifyOtp_post
     * Author Name : Jasbir Singh
     * Description : verify otp and update mobile number
     */
    public function verifyOtp_post() {
        $session_token = $this->input->get_request_header('Sessionid');
        $user = $this->checkSession($session_token);
        if ($this->post('phoneNo') && $this->post('otp')) {
            $userOtp = $this->post('otp');
            $conditions = $params = array();
            $params['table'] = 'tbl_members';
            $conditions['mem_id'] = $user->user_id;
            $params['fields'] = array('otp');
            $params['single_row'] = TRUE;
            $otpCheck = $this->common_model->get_data($conditions, $params);
            if ($otpCheck->otp == $userOtp) {
                $conditions = $params = array();
                $conditions['value']['otp'] = '';
                $conditions['value']['mobile_no'] = $this->post('phoneNo');
                $conditions['where']['mem_id'] = $user->user_id;
                $params['table'] = 'tbl_members';
                $this->common_model->update($conditions, $params);
                $data = array(
                    'status' => TRUE,
                    'message' => 'Phone No. updated',
                    'data' => array('mobile_no' => $this->post('phoneNo'))
                );
                $status_code = '200';
                $this->response($data, $status_code);
            } else {
                $data = array(
                    'status' => FALSE,
                    'message' => 'OTP not matched',
                );
                $status_code = '200';
                $this->response($data, $status_code);
            }
        } else {
            $data = array(
                'status' => FALSE,
                'message' => 'Post data empty',
                'data' => (object) array(),
            );
            $status_code = '404';
            $this->response($data, $status_code);
        }
    }

    /**
     * Method Name : logout_get
     * Author Name : Jasbir Singh
     * Description : logout user from application
     */
    public function logout_get() {
        $session_token = $this->input->get_request_header('Sessionid');
        $user = $this->checkSession($session_token);
        $deviceToken = $this->input->get_request_header('Devicetoken');
        $conditions = $params = array();
        $conditions['where']['device_id'] = $deviceToken;
        $conditions['where']['user_id'] = $user->user_id;
        $conditions['value']['session_token'] = $session_token;
        $conditions['login_status'] = 0;
        $params['table'] = 'tbl_api_sessions';
        $this->common_model->update($conditions, $params);
        $data = array(
            'status' => TRUE,
            'message' => 'Logout successful',
        );
        $status_code = '200';
        $this->response($data, $status_code);
    }

    /**
     * Method Name : thumb
     * Author Name : Jasbir Singh
     * Date : 15 June 2015
     * Description : creating Thumbnails of photos
     */
    function thumb($data) {
        $config['image_library'] = 'gd2';
        $config['source_image'] = $data['full_path'];
        $config['create_thumb'] = TRUE;
        $config['maintain_ratio'] = TRUE;
        $config['width'] = 350;
        $config['height'] = 250;
        $this->load->library('image_lib');
        $this->image_lib->initialize($config);
        $this->image_lib->resize();
        $this->image_lib->clear();
        return true;
    }

    /**
     * Method Name : uploadPhotos_post
     * Author Name : Jasbir Singh
     * Description : upload photos to property
     */
    public function uploadPhotos_post() {
        $session_token = $this->input->get_request_header('Sessionid');
        $user = $this->checkSession($session_token);
        $login_id = $user->user_id;
        $property_details = $this->propertyDetails($login_id);
        $k=0;
        if (isset($_FILES) && !empty($_FILES)) {
            $login_id = $user->user_id;
            $folder = PROPERTY_PHOTOS;
            $this->load->library('upload');
            $files = $_FILES;
            $_FILES['userfile']['name'] = $files['userfile']['name'];
            $_FILES['userfile']['type'] = $files['userfile']['type'];
            $_FILES['userfile']['tmp_name'] = $files['userfile']['tmp_name'];
            $_FILES['userfile']['error'] = $files['userfile']['error'];
            $_FILES['userfile']['size'] = $files['userfile']['size'];
            $this->upload->initialize($this->set_upload_options($property_details->prop_id, $folder, 'property'));
            if ($files['userfile']['name'] != '') {
                if ($this->upload->do_upload() == False) {
                    $k++;
                } else {
                    $data = $this->upload->data();
                    $this->thumb($data);
                    $thumb = $data['raw_name'] . '_thumb' . $data['file_ext'];
                    $orig_file = $data['raw_name'] . $data['file_ext'];
                    $gallery_add[] = array(
                        'property_id' => $property_details->prop_id,
                        'image' => PROPERTY_PHOTOS . "/property_" . $property_details->prop_id . "/" . $orig_file,
                        'thumb' => PROPERTY_PHOTOS . "/property_" . $property_details->prop_id . "/" . $thumb,
                        'tenant_id' => $login_id,
                        'status' => 'Active',
                        'add_date' => current_date(),
                        'ip' => ''
                    );
                }
            }

//insert photo data into database
            if (isset($gallery_add) && $gallery_add != '') {
                $conditions = $params = array();
                $params['batch_mode'] = true;
                $conditions = $gallery_add;
                $params['table'] = 'tbl_property_photos';
                $insert_id = $this->common_model->insert_data($conditions, $params);
            }
            $photos = $this->getPropertyPhotos($login_id, $property_details->prop_id);
            $message = 'Request Submitted Successfully';
            if ($k > 0) {
                $message .='But Unable to upload' . $k . " photos";
            }
            $data = array(
                'status' => TRUE,
                'message' => $message,
                'data' => $photos,
            );
            $status_code = '200';
            $this->response($data, $status_code);
        } else {
            $data = array(
                'status' => FALSE,
                'message' => 'Post data empty',
                'data' => array(),
            );
            $status_code = '404';
            $this->response($data, $status_code);
        }
    }

    /**
     * Method Name : deletePhoto_post
     * Author Name : Jasbir Singh
     * Description : delete photo of property uploaded by tenant
     */
    public function deletePhoto_post() {
        $session_token = $this->input->get_request_header('Sessionid');
        $user = $this->checkSession($session_token);
        $login_id = $user->user_id;
        if ($this->post('id')) {
            $conditions = $params = array();
            $params['table'] = 'tbl_property_photos';
            $conditions['id'] = $this->post('id');
            $params['single_row'] = TRUE;
            $photo = $this->common_model->get_data($conditions, $params);
            if (!empty($photo)) {
                if (!empty($photo->thumb)) {
                    // unlink(FCPATH . $photo->thumb);
                }
                unlink(FCPATH . $photo->image);
            }
            $conditions = $params = array();
            $conditions['id'] = $this->post('id');
            $params['table'] = 'tbl_property_photos';
            $this->common_model->delete($conditions, $params);
            $photos = $this->getPropertyPhotos($login_id, $photo->property_id);
            $data = array(
                'status' => TRUE,
                'message' => 'Photo deleted sucessfully',
                'data' => $photos,
            );
            $status_code = '200';
            $this->response($data, $status_code);
        } else {
            $data = array(
                'status' => FALSE,
                'message' => 'Post data empty',
            );
            $status_code = '404';
            $this->response($data, $status_code);
        }
    }

    /**
     * Method Name : sendlandlordOTP
     * Author Name : Jasbir Singh
     * Description : send otp to landlord to verify tenant 
     */
    public function sendlandlordOTP($login_id) {
        $this->load->helper('twilio_helper');  // references library/twilio_helper.php
        $service = get_twilio_service();
        $params = $conditions = array();
        $params['table'] = 'tbl_properties_lease_detail';
        $conditions['tenant_id'] = $login_id;
        $params['single_row'] = true;
        $params['fields'] = array('owner_id', 'property_id');
        $landlord = $this->common_model->get_data($conditions, $params);
        $owner = $this->getUserInfo($landlord->owner_id);
        $otp = $this->generate_otp();
        $mnumber = $owner->mobile_no;
        $number = "+1" . preg_replace("/[^0-9]/", "", $mnumber);
        $msg = "One Time Password for your tenant is : " . $otp;
        try {
//get landlord id
            $service->account->sms_messages->create("+19046743077", "$number", "$msg", array());
//save otp to database for verification
            $params = $conditions = array();
            $params['table'] = 'tbl_landlord_otp';
            $conditions['tenant_id'] = $login_id;
            $params['single_row'] = true;
            $oldotp = $this->common_model->get_data($conditions, $params);
            if (empty($oldotp)) {
                $conditions = $params = array();
                $conditions['mobile_no'] = $number;
                $conditions['owner_id'] = $landlord->owner_id;
                $conditions['tenant_id'] = $login_id;
                $conditions['otp'] = $otp;
                $conditions['status'] = 0;
                $conditions['date_created'] = date('Y-m-d H:i:s');
                $params['table'] = 'tbl_landlord_otp';
                $this->common_model->insert_data($conditions, $params);
            } else {
                $conditions = $params = array();
                $conditions['where']['tenant_id'] = $login_id;
                $conditions['value']['otp'] = $otp;
                $params['table'] = 'tbl_landlord_otp';
                $this->common_model->update($conditions, $params);
            }
            return $number;
        } catch (Exception $e) {
            $file = 'sms_error_logs.txt';
            if (file_exists($file)) {
                $current = file_get_contents($file);
                $current .= $e->getMessage() . "\n";
                file_put_contents($file, $current);
            }
            $data = array(
                'status' => FALSE,
                'message' => 'Unable to send OTP to Landlord at ' . $number,
            );
            $status_code = '200';
            $this->response($data, $status_code);
        }
    }

    /**
     * Method Name : landlordOTP_post
     * Author Name : Jasbir Singh
     * Description : send otp to landlord to verify tenant 
     */
    public function landlordOTP_post() {
        $session_token = $this->input->get_request_header('Sessionid');
        $user = $this->checkSession($session_token);
        $login_id = $user->user_id;
        $this->load->helper('twilio_helper');  // references library/twilio_helper.php
        $service = get_twilio_service();
        $params = $conditions = array();
        $params['table'] = 'tbl_properties_lease_detail';
        $conditions['tenant_id'] = $login_id;
        $params['single_row'] = true;
        $params['fields'] = array('owner_id', 'property_id');
        $landlord = $this->common_model->get_data($conditions, $params);
        $owner = $this->getUserInfo($landlord->owner_id);
        $otp = $this->generate_otp();
        $mnumber = $owner->mobile_no;
        $number = "+1" . preg_replace("/[^0-9]/", "", $mnumber);
        $msg = "One Time Password for your tenant is : " . $otp;
        if ($this->post('phoneNo')) {
            $this->load->helper('twilio_helper');  // references library/twilio_helper.php
            $service = get_twilio_service();
            try {
                $service->account->sms_messages->create("+19046743077", "$number", "$msg", array());
//get landlord id
                $params = $conditions = $feeds = array();
                $params['table'] = 'tbl_properties_lease_detail';
                $conditions['tenant_id'] = $login_id;
                $params['single_row'] = true;
                $params['fields'] = array('owner_id', 'property_id');
                $landlord = $this->common_model->get_data($conditions, $params);
//save otp to database for verification
                $params = $conditions = array();
                $params['table'] = 'tbl_landlord_otp';
                $conditions['tenant_id'] = $login_id;
                $params['single_row'] = true;
                $oldotp = $this->common_model->get_data($conditions, $params);
                if (empty($oldotp)) {
                    $conditions = $params = array();
                    $conditions['mobile_no'] = $number;
                    $conditions['owner_id'] = $landlord->owner_id;
                    $conditions['tenant_id'] = $login_id;
                    $conditions['otp'] = $otp;
                    $conditions['status'] = 0;
                    $conditions['date_created'] = date('Y-m-d H:i:s');
                    $params['table'] = 'tbl_landlord_otp';
                    $this->common_model->insert_data($conditions, $params);
                } else {
                    $conditions = $params = array();
                    $conditions['where']['tenant_id'] = $login_id;
                    $conditions['value']['otp'] = $otp;
                    $params['table'] = 'tbl_landlord_otp';
                    $this->common_model->update($conditions, $params);
                }
                $data = array(
                    'status' => TRUE,
                    'message' => 'OTP sent to landlord',
                    'number' => $number
                );
                $status_code = '200';
                $this->response($data, $status_code);
            } catch (Exception $e) {
                $file = 'sms_error_logs.txt';
                if (file_exists($file)) {
                    $current = file_get_contents($file);
                    $current .= $e->getMessage() . "\n";
                    file_put_contents($file, $current);
                }
                $data = array(
                    'status' => FALSE,
                    'message' => 'Unable to send OTP',
                );
                $status_code = '200';
                $this->response($data, $status_code);
            }
        } else {
            $data = array(
                'status' => FALSE,
                'message' => 'Post data empty',
            );
            $status_code = '404';
            $this->response($data, $status_code);
        }
    }

    /**
     * Method Name : verifyTenant_post
     * Author Name : Jasbir Singh
     * Description : verify OTP sent to landlord by tenant
     */
    public function verifyTenant_post() {
        $session_token = $this->input->get_request_header('Sessionid');
        $user = $this->checkSession($session_token);
        $login_id = $user->user_id;
        if ($this->post('otp')) {
            $userOtp = $this->post('otp');
            $conditions = $params = array();
            $params['table'] = 'tbl_landlord_otp';
            $conditions['tenant_id'] = $login_id;
            $conditions['otp'] = $userOtp;
            $params['single_row'] = TRUE;
            $otpCheck = $this->common_model->get_data($conditions, $params);
            if (!empty($otpCheck)) {
                $conditions = $params = array();
                $conditions['value']['status'] = 1;
                $conditions['where']['id'] = $otpCheck->id;
                $params['table'] = 'tbl_landlord_otp';
                $this->common_model->update($conditions, $params);
                $conditions = $params = array();
                $conditions['value']['app_verified'] = 1;
                $conditions['where']['mem_id'] = $login_id;
                $params['table'] = 'tbl_members';
                $this->common_model->update($conditions, $params);
                $info = $this->getUserInfo($user->user_id);
                $data = array(
                    'status' => TRUE,
                    'message' => 'Tenant verified successfully',
                    'data' => $info
                );
                $status_code = '200';
                $this->response($data, $status_code);
            } else {
                $data = array(
                    'status' => FALSE,
                    'message' => 'OTP not matched',
                );
                $status_code = '200';
                $this->response($data, $status_code);
            }
        } else {
            $data = array(
                'status' => FALSE,
                'message' => 'Post data empty',
            );
            $status_code = '404';
            $this->response($data, $status_code);
        }
    }

    /**
     * Method Name : checklist_get
     * Author Name : Jasbir Singh
     * Description : return tenant checklist
     */
    public function checklist_get() {
        $session_token = $this->input->get_request_header('Sessionid');
        $user = $this->checkSession($session_token);
        $login_id = $user->user_id;
        $params = $conditions = array();
        $params['table'] = 'tbl_checklist';
        $check_list = $this->common_model->get_data($conditions, $params);
        $data = array(
            'status' => TRUE,
            'message' => 'success',
            'data' => $check_list,
        );
        $status_code = '200';
        $this->response($data, $status_code);
    }

    /**
     * Method Name : userinfo_get
     * Author Name : Jasbir Singh
     * Description : return tenant basic info
     */
    public function userinfo_get() {
        $session_token = $this->input->get_request_header('Sessionid');
        $user = $this->checkSession($session_token);
        $login_id = $user->user_id;
        $data = array(
            'status' => TRUE,
            'message' => 'success',
            'data' => $this->getUserInfo($login_id),
        );
        $status_code = '200';
        $this->response($data, $status_code);
    }

    /**
     * Method Name : propertyDetails
     * Author Name : Jasbir Singh
     * Description : return details of particular property
     */
    public function allDetails($tenant_id) {
        $conditions = $params = array();
        $params['complex'] = true;
        $params['single_row'] = true;
        $conditions['tables'] = array(
            'lease' => 'tbl_properties_lease_detail',
            'property' => 'tbl_properties',
            'tenant' => 'tbl_members',
            'landlord' => 'tbl_members',
            'state' => 'tbl_region'
        );
        $conditions['table'] = 'lease';
        $conditions['on']['property'] = array(
            'sign' => '=',
            'column' => 'prop_id',
            'alias_column' => 'property_id',
            'alias_other' => 'lease',
        );
        $conditions['on']['landlord'] = array(
            'sign' => '=',
            'column' => 'mem_id',
            'alias_column' => 'owner_id',
            'alias_other' => 'lease',
        );
        $conditions['on']['tenant'] = array(
            'sign' => '=',
            'column' => 'mem_id',
            'alias_column' => 'tenant_id',
            'alias_other' => 'lease',
        );
        $conditions['on']['state'] = array(
            'sign' => '=',
            'column' => 'region_id',
            'alias_column' => 'state_id',
            'alias_other' => 'property',
        );
        $conditions['where']['tenant_id']['alias'] = 'lease';
        $conditions['where']['tenant_id']['value'] = $tenant_id;

        $params['single_row'] = TRUE;
        $params['fields'] = array('landlord.mem_id as owner_id',
            'landlord.first_name as lfname', 'landlord.last_name as llname', 'landlord.mobile_no as lmobile', 'landlord.email as lemail',
            'tenant.first_name as tfname', 'tenant.last_name as tlname', 'tenant.mobile_no as tmobile',
            'state.region_name', 'property.prop_id', 'property.address1', 'property.address2', 'property.city', 'property.zip'
        );
        $info = $this->common_model->get_data($conditions, $params);
        return $info;
    }

    /**
     * Method Name : contactNewLandlord
     * Author Name : Jasbir Singh
     * Description :send email to new landlord
     */
    public function contactNewLandlord_post() {
        $session_token = $this->input->get_request_header('Sessionid');
        $user = $this->checkSession($session_token);
        $login_id = $user->user_id;
        if ($this->post('email') != '' && $this->post('name') != '' && $this->post('message') != '') {
            $conditions = $params = array();
            $conditions['tenant_id'] = $login_id;
            $conditions['landlord_email'] = $this->post('email');
            $conditions['landlord_name'] = $this->post('name');
            $conditions['message'] = $this->post('message');
            $conditions['date_created'] = current_date();
            $params['table'] = 'tbl_newlandlords';
            $this->common_model->insert_data($conditions, $params);
            $this->mailsending_model->contactNewLandlord($this->post('name'), $this->post('email'), $this->post('message'));
            $data = array(
                'status' => TRUE,
                'message' => 'email sent to landlord successfully',
            );
            $status_code = '200';
            $this->response($data, $status_code);
        } else {
            $data = array(
                'status' => FALSE,
                'message' => 'Post data empty',
                'data' => (object) array(),
            );
            $status_code = '404';
            $this->response($data, $status_code);
        }
    }

    /**
     * Method Name : rentalHistory
     * Author Name : Jasbir Singh
     * Description : returns tenant rental history
     */
    public function rentalHistory_get() {
        $session_token = $this->input->get_request_header('Sessionid');
        $user = $this->checkSession($session_token);
        $docs = array();
        $info = new stdClass();
        $login_id = $user->user_id;
        $property_details = $this->propertyDetails($login_id);
        $docs = $this->getLeaseDocs($login_id, $property_details->prop_id);
        if (!empty($property_details)) {
            $address = $communityAddress = $communityPhoneNo = '';
            if ($property_details->address1 != '') {
                $address .=$property_details->address1;
            }
            if ($property_details->address2 != '') {
                $address .=" " . $property_details->address2;
            }
            if ($property_details->unit_number != 0) {
                $address .=" " . $property_details->unit_number;
            }
            if ($property_details->city != '') {
                $address .=" " . $property_details->city . ",";
            }
            if ($property_details->region_name != '') {
                $address .=" " . $property_details->region_name;
            }
            if ($property_details->zip != '') {
                $address .=" " . $property_details->zip;
            }
            if ($property_details->community_status != 'No') {
                if ($property_details->community != '') {
                    $communityAddress .= $property_details->community . " Community";
                }
                if ($property_details->gate_code != '') {
                    $communityAddress .=" Gate: " . $property_details->gate_code;
                }
                if ($property_details->unit_po_box != 0) {
                    $communityAddress .=" PO Box : " . $property_details->unit_po_box;
                }
                if ($property_details->community_managing_company != '') {
                    $communityAddress .= " " . $property_details->community_managing_company;
                }
                if ($property_details->community_contact_number != '') {
                    $communityPhoneNo = $property_details->community_contact_number;
                }
            }
//property address
            $info->propertyDetails = array(
                'propertyAddress' => $address,
                'communityAddress' => $communityAddress,
                'communityPhoneNo' => $communityPhoneNo,
                'coordinates' => array('lat' => $property_details->latitude, 'lng' => $property_details->longitude)
            );
            $info->leaseDetails = array(
                'leaseStart' => date('d/M/Y', strtotime($property_details->lease_start_date)),
                'leaseEnd' => date('d/M/Y', strtotime($property_details->lease_end_date)),
                'rentAmount' => $property_details->rent_amount
            );
            $info->landlord = $this->getLandlordInfo($property_details->owner_id);
        }
        $info->documentsAttached = $docs;
        $data = array(
            'status' => TRUE,
            'message' => 'details found',
            'data' => $info
        );
        $status_code = '200';
        $this->response($data, $status_code);
    }

    /**
     * Method Name : declineLandlord
     * Author Name : Jasbir Singh
     * Description : tenant declined landlord information and an email will be
     * sent to landlord
     */
    public function acceptLandlord_post() {
        $session_token = $this->input->get_request_header('Sessionid');
        $user = $this->checkSession($session_token);
        $login_id = $user->user_id;
        if ($this->post('accept') == true) {
            $conditions = $params = array();
            $conditions['value']['app_verified'] = 1;
            $conditions['where']['mem_id'] = $login_id;
            $params['table'] = 'tbl_members';
            $this->common_model->update($conditions, $params);
            $data = array(
                'status' => TRUE,
                'message' => 'You are all set to enjoy tServices from TenantTag',
            );
            $status_code = '200';
            $this->response($data, $status_code);
        } else {
            $data = array(
                'status' => FALSE,
                'message' => 'Post data empty',
                'data' => (object) array(),
            );
            $status_code = '404';
            $this->response($data, $status_code);
        }
    }

    public function check_post() {
        $check = array('a', 'b');
        echo '<pre>';
        print_r($check);
        die;
        echo '<pre>';
        print_r($user);
        die;
    }



/*
  |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
  ||||||||||||||||||||START: Synapspay Related APIs and functions |||||||||||||||||||||||||||||||||||||||
  |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
*/


    /**
     * Method Name : propertyDetails
     * Author Name : Jasbir Singh
     * Description : return details of particular property
     */
    public function getSynapseDetails($landlord_id) {
        $conditions = $params = array();
        $conditions['user_id'] = $landlord_id;
        $conditions['status'] = 1;
        $conditions['is_deleted'] = 0;
        $params['single_row'] = TRUE;
        $params['table'] = 'tbl_linked_account_numbers';
        $info = $this->common_model->get_data($conditions, $params);
        if(!empty($info)){
            if($info->bank_code == 'other'){
                if($info->synapse_micro_permission == 'CREDIT-AND-DEBIT'){
                    $data = $info;
                }else{
                    $data = array();
                }
            }else{
                $data = $info;
            }
        }
       // echo $this->db->last_query(); //exit;
        return $info;
    }

    public function setSynapseTransactionDetail_post(){
        $session_token  = $this->input->get_request_header('Sessionid');
        $user           = $this->checkSession($session_token);
        $login_id       = $user->user_id;
        $user_detail 	= $this->getUserInfo($login_id);
        $amount = $this->post('amount');
        if($this->post('amount')){

            $property_details = $this->propertyDetails($login_id);
            if($this->post('status') == 'CANCELED' || $this->post('status') == 'CANCELLED'){
                $status = 'CANCELED';
            }elseif($this->post('status') == 'SETTLED'){
                $status = 'PAID';
            }else{
                $status = $this->post('status');
            }
            $transaction_id = $this->post('transaction_id');
            $conditions = $params = array();
            $conditions['transaction_id'] = $transaction_id;
            $params['table'] = 'tbl_payment';
            $params['single_row'] = true;
            $transaction_info = $this->common_model->get_data($conditions, $params); 
            //if(isset($transaction_info->status) && $transaction_info->status != $status && $status != 'QUEUED-BY-SYNAPSE' && $status != 'QUEUED-BY-RECEIVER'){
            if(isset($transaction_info->status) && $transaction_info->status != $status){
                $sent_mail = "Yes";
            }else{
                $sent_mail = "No";
            }
            if(!isset($transaction_info->status)){
                $sent_mail = 'Yes';
            }
            if(empty($transaction_info)){
                $insert_data = array(
                        'mem_id'        => $login_id,
                        'property_id'   => $property_details->prop_id,
                        'transaction_id'=> $transaction_id,
                        'amount'        => $this->post('amount'),
                        'description'   => $this->post('description'),
                        'routing_number'=> $this->post('routing_number'),
                        'account_number'=> $this->post('account_number'),
                        'status'        => $status,
                        'user_type'     => 'Tenant',
                        'payment_type'  => 'Synapsepay',
                        'date'          => $this->post('date'),
                        'modified_date' => current_date(),
                        'created_date'  => current_date(),
                    );
                $conditions = $params = array();
                $conditions = $insert_data;
                $params['table'] = 'tbl_payment';
                $this->common_model->insert_data($conditions, $params);
                /*****************************************Mail Send *****************************/
    	        $user_name 		= $user_detail->first_name.' '.$user_detail->last_name;
    	        $user_email 	= $user_detail->email;
                $mail_message = 'Payment('.$amount.') update';
                //$this->mailsending_model->sentPaymentInfo($user_name, $user_email, $mail_message);

            }else{
                $update_data = array(
                        'status'        => $status,
                        'modified_date' => current_date()
                    );
                $conditions = $params = array();
                $conditions['value'] = $update_data;
                $conditions['where']['transaction_id'] = $transaction_id;
                $params['table'] = 'tbl_payment';
                $this->common_model->update($conditions, $params);
                /*****************************************Mail Send *****************************/
                $user_name      = $user_detail->first_name.' '.$user_detail->last_name;
                $user_email     = $user_detail->email;
                $mail_message = 'Payment('.$amount.') update';
            }


            $node_id        = $this->post('node_id');
            $synapse_user_id= $this->post('synapse_user_id');
            $amount         = $this->post('amount');
            $account_number = $this->post('account_number');
            $routing_number = $this->post('routing_number');
            $recurring      = $this->post('recurring');
            $payment_type   = $this->post('payment_type'); 
            $days           = $this->post('days');
            $times          = $this->post('times');
            $bank_name          = $this->post('bank_name');
            $bank_logo          = $this->post('bank_logo');

            if ($node_id) {
                $conditions = $params = array();
                $params['table'] = 'tbl_synapsepay_card_info';
                $conditions['user_id'] = $login_id;
                $conditions['account_number'] = $account_number;
                $conditions['is_deleted'] = 1;
                $params['single_row'] = TRUE;
                $bank_user_info = $this->common_model->get_data($conditions, $params);
                if(empty($bank_user_info)){
                    $this->saveSynapsepayAccount($login_id,$node_id,$synapse_user_id,$amount,$account_number,$routing_number,$recurring,$payment_type,$days,$times,$bank_name,$bank_logo);
                    $bank_info = $bank_name. ' '.$account_number;
                }else{
                    $conditions = $params = array();
                    $conditions['where']['user_id']      = $login_id;
                    $conditions['value']['recurring']    = 0;
                    $conditions['value']['modified_date'] = date('Y-m-d H:i:s');
                    $params['table'] = 'tbl_synapsepay_card_info';
                    $this->common_model->update($conditions, $params);

                    $conditions = $params = array();
                    $conditions['where']['id']      = $bank_user_info->id;
                    $conditions['value']['amount']       = $amount;
                    $conditions['value']['payment_type']= $payment_type;
                    $conditions['value']['recurring']    = $recurring;
                    $conditions['value']['days']         = $days;
                    $conditions['value']['times']        = $times;
                    $conditions['value']['status']       = 1;
                    $conditions['value']['modified_date'] = date('Y-m-d H:i:s');
                    $params['table'] = 'tbl_synapsepay_card_info';
                    $this->common_model->update($conditions, $params);

                    $bank_info = $bank_user_info->bank_name. ' '.$bank_user_info->account_number;
                }
            }
            $debited_date = $this->post('date');
            $debited_date = date("Y-m-d",$debited_date);

            if($sent_mail == 'Yes'){
                $this->mailsending_model->sendTransactionMail($user_name, $user_email,'$ '.$amount, $status,$bank_info,$transaction_id);
                $sms_response = $this->smsSendToUser($user_detail->mobile_no,'$ '.$amount.' transaction('.$transaction_id.') created succesfully'); // send sms
            }
            $data = array(
                'status' => TRUE,
                'message' => "Updated.",
                'data' => (object) array()
            );
            $status_code = '200';
            $this->response($data, $status_code);
        }else{
            $data = array(
                'status' => FALSE,
                'message' => "Post data empty.",
                'data' => (object) array()
            );
            $status_code = '404';
            $this->response($data, $status_code);
        }
    }

    public function smsSendToUser($mobile_number,$msg){
        $this->load->helper('twilio_helper');  // references library/twilio_helper.php
        $service = get_twilio_service();
        //send sms     
        $i = 0;
        try {
            $mnumber = $mobile_number;
            $number = "+1" . preg_replace("/[^0-9]/", "", $mnumber);
            //$msg = "Transaction has been failed, due to insufficient balance in your account";
            $service->account->sms_messages->create("+19046743077", "$number", "$msg", array());
            $sms_status_message = 'SMS Sent';
            $i++;
        } catch (Exception $e) {
            $file = 'sms_error_logs.txt';
            if (file_exists($file)) {
                $current = file_get_contents($file);
                $current .= $e->getMessage() . "\n";
                file_put_contents($file, $current);
            }
            $sms_status_message = 'SMS failed'.$e->getMessage();
        }
        //echo $sms_status_message; exit;
        return $i;
    }
    public function setSynapseKycDetail_post(){
        $session_token  = $this->input->get_request_header('Sessionid');
        $user           = $this->checkSession($session_token);
        $login_id       = $user->user_id;
        $user_detail    = $this->getUserInfo($login_id);
        if($this->post('kyc_id')){ 

            $property_details = $this->propertyDetails($login_id);
            $insert_data = array(
                    'user_id'        => $login_id,
                    'property_id'   => $property_details->prop_id,
                    'kyc_id'        => $this->post('kyc_id'),
                    'user_type'        => 'tenant',
                    'kyc_physical_id'        => $this->post('kyc_physical_id'),
                    'kyc_virtual_id'        => $this->post('kyc_id'),
                    'kyc_permission'        => $this->post('kyc_permission'),
                    'physical_doc_status'   => $this->post('physical_doc_status'),
                    'virtual_doc_status'=> $this->post('virtual_doc_status'),
                    'first_name'=> $this->post('first_name'),
                    'middle_name'        => $this->post('middle_name'),
                    'last_name'        => $this->post('last_name'),
                    'phone'        => $this->post('phone'),
                    'email'        => $this->post('email'),
                    'address_country_code'        => $this->post('address_country_code'),
                    'address_line_1'        => $this->post('address_line_1'),
                    'address_street'        => $this->post('address_street'),
                    'address_city'        => $this->post('address_city'),
                    'address_subdivision'        => $this->post('state'),
                    'address_zip_code'        => $this->post('address_zip_code'),
                    'dob'        => getDateByFormat($this->post('dob'),'Y-m-d'),
                    'social_security'        => $this->post('social_security'),
                    'driving_document'        => $this->post('driving_document'),
                    'recent_bank_statment'        => $this->post('recent_bank_statment'),
                    'modified_date' => current_date(),
                    'created_date'  => current_date(),
                );
            $update_data = array(
                    'kyc_id'        => $this->post('kyc_id'),
                    'user_type'        => 'tenant',
                    'kyc_physical_id'        => $this->post('kyc_physical_id'),
                    'kyc_virtual_id'        => $this->post('kyc_id'),
                    'kyc_permission'        => $this->post('kyc_permission'),
                    'physical_doc_status'   => $this->post('physical_doc_status'),
                    'virtual_doc_status'=> $this->post('virtual_doc_status'),
                    'first_name'=> $this->post('first_name'),
                    'middle_name'        => $this->post('middle_name'),
                    'last_name'        => $this->post('last_name'),
                    'phone'        => $this->post('phone'),
                    'email'        => $this->post('email'),
                    'address_country_code'        => $this->post('address_country_code'),
                    'address_line_1'        => $this->post('address_line_1'),
                    'address_street'        => $this->post('address_street'),
                    'address_city'        => $this->post('address_city'),
                    'address_subdivision'        => $this->post('state'),
                    'address_zip_code'        => $this->post('address_zip_code'),
                    'dob'        => getDateByFormat($this->post('dob'),'Y-m-d'),
                    'social_security'        => $this->post('social_security'),
                    'driving_document'        => $this->post('driving_document'),
                    'recent_bank_statment'        => $this->post('recent_bank_statment'),
                    'modified_date' => current_date(),
                );

            $conditions = $params = array();
            $params['table'] = 'tbl_linked_account_info';
            $conditions['user_id'] = $login_id;
            $params['single_row'] = TRUE;
            $data_info = $this->common_model->get_data($conditions, $params);
            if(empty($data_info)){
                $conditions = $params = array();
                $conditions = $insert_data;
                $params['table'] = 'tbl_linked_account_info';
                $this->common_model->insert_data($conditions, $params);

            }else{
                $conditions = $params = array();
                $conditions['value'] = $update_data;
                $conditions['where']['id'] = $data_info->id;
                $params['table'] = 'tbl_linked_account_info';
                $this->common_model->update($conditions, $params);
            }

           
            $data = array(
                'status' => TRUE,
                'message' => "Updated.",
                'data' => (object) array()
            );
            $status_code = '200';
            $this->response($data, $status_code);
        }else{
            $data = array(
                'status' => FALSE,
                'message' => "Post data empty.",
                'data' => (object) array()
            );
            $status_code = '404';
            $this->response($data, $status_code);
        }
    }

    public function getSynapseKycDetail_post(){
        $session_token  = $this->input->get_request_header('Sessionid');
        $user           = $this->checkSession($session_token);
        $login_id       = $user->user_id;
        $user_detail    = $this->getUserInfo($login_id);
        $conditions = $params = array();
        $params['table'] = 'tbl_linked_account_info';
        $conditions['user_id'] = $login_id;
        $params['single_row'] = TRUE;
        $data_info = $this->common_model->get_data($conditions, $params);
        $data = array(
            'status' => TRUE,
            'message' => "KYC detail.",
            'data' => $data_info
        );
        $status_code = '200';
        $this->response($data, $status_code);
       
    }



    /**
     * Method Name : getSynapsepayUserInfo_post
     * Author Name : Lakhvinder Singh
     * Description :getSynapsepayUserInfo_post
     */
    public function getSynapsepayAccounts_post() {
        $session_token = $this->input->get_request_header('Sessionid');
        $user = $this->checkSession($session_token);
        $login_id = $user->user_id;
        $conditions = $params = array();
        $params['table'] = 'tbl_synapsepay_card_info';
        $conditions['user_id'] = $login_id;
        $conditions['is_deleted'] = 1;
        //$params['single_row'] = TRUE;
        $params['fields'] = array('id as record_id','synapse_user_id','node_id','amount','account_number','routing_number','recurring','payment_type','days','times','node_id','bank_name','bank_logo');
        $user_info = $this->common_model->get_data($conditions, $params);
        $result['synapse_account'] = $user_info;

        $conditions = $params = array();
        $params['table'] = 'tbl_linked_account_numbers';
        $conditions['user_id'] = $login_id;
        $conditions['is_deleted'] = 0;
        $synapse_unverfied_account = $this->common_model->get_data($conditions, $params);
        
        $synapse_unverfied_account_Arr = array();
        foreach ($synapse_unverfied_account as $key => $value) {
            $node_id = $value->oid;
            $conditions = $params = array();
            $params['table'] = 'tbl_synapsepay_card_info';
            $conditions['node_id'] = $node_id;
            $params['single_row'] = TRUE;
            $result_info = $this->common_model->get_data($conditions, $params);
            if(empty($result_info)){
                $synapse_unverfied_account_Arr[] = $value;
            }
        }
        $result['synapse_unverfied_account'] = $synapse_unverfied_account_Arr;

        $data = array(
            'status' => TRUE,
            'message' => 'Synapsepay Cards Detail',
            'data' => $result
        );
        $status_code = '200';
        $this->response($data, $status_code);
    }

    public function getSynapsepayLinkedAccounts_post() {
        $session_token = $this->input->get_request_header('Sessionid');
        $user = $this->checkSession($session_token);
        $login_id = $user->user_id;
        $conditions = $params = array();
        $params['table'] = 'tbl_linked_account_numbers';
        $conditions['user_id'] = $login_id;
        $conditions['is_deleted'] = 0;
        $user_info = $this->common_model->get_data($conditions, $params);
        $data = array(
            'status' => TRUE,
            'message' => 'Synapsepay Cards Detail',
            'data' => $user_info
        );
        $status_code = '200';
        $this->response($data, $status_code);
    }


    public function setSynapseLinkedAccount_post(){
        $session_token  = $this->input->get_request_header('Sessionid');
        $user           = $this->checkSession($session_token);
        $login_id       = $user->user_id;
        if($this->post('oid')){

            $insert_data = array(
                    'user_id'                   => $login_id,
                    'oid'                       => $this->post('oid'),
                    'account_number'            => $this->post('account_number'),
                    'routing_number'            => $this->post('routing_number'),
                    'amount'                    => $this->post('amount'),
                    'bank_name'                 => $this->post('bank_name'),
                    'bank_code'                 => $this->post('bank_code'),
                    'class'                     => $this->post('class'),
                    'synapse_micro_permission'  => $this->post('synapse_micro_permission'),
                    'is_deleted'  => $this->post('is_deleted'),
                    'status'                    => 1,
                    'modified_date' => current_date(),
                    'created_date'  => current_date(),
                );

            $conditions = $params = array();
            $params['table'] = 'tbl_linked_account_numbers';
            $conditions['user_id'] = $login_id;
            $conditions['account_number'] = $this->post('account_number');
            $conditions['oid'] = $this->post('oid');
            $params['single_row'] = true;
            $data_info = $this->common_model->get_data($conditions, $params);

            if(empty($data_info)){
                $conditions = $params = array();
                $conditions = $insert_data;
                $params['table'] = 'tbl_linked_account_numbers';
                $this->common_model->insert_data($conditions, $params);

            }else{
                $update_data = array(
                    'user_id'                   => $login_id,
                    'oid'                       => $this->post('oid'),
                    'amount'                    => $this->post('amount'),
                    'class'                     => $this->post('class'),
                    'synapse_micro_permission'  => $this->post('synapse_micro_permission'),
                    'is_deleted'  => $this->post('is_deleted'),
                    'modified_date' => current_date(),
                );
                $conditions = $params = array();
                $conditions['value'] = $update_data;
                $conditions['where']['id'] = $data_info->id;
                $params['table'] = 'tbl_linked_account_numbers';
                $this->common_model->update($conditions, $params);
            }

           
            $data = array(
                'status' => TRUE,
                'message' => "Updated.",
                'data' => (object) array()
            );
            $status_code = '200';
            $this->response($data, $status_code);
        }else{
            $data = array(
                'status' => FALSE,
                'message' => "Post data empty.",
                'data' => (object) array()
            );
            $status_code = '404';
            $this->response($data, $status_code);
        }
    }

    public function updateSynapseLinkedAccountStatus_post(){
        $session_token  = $this->input->get_request_header('Sessionid');
        $user           = $this->checkSession($session_token);
        $login_id       = $user->user_id;
        if($this->post('id')){
            $id = $this->post('id');
            $insert_data = array(
                    'synapse_micro_permission'  => $this->post('synapse_micro_permission'),
                    'modified_date' => current_date()
                );

            $conditions = $params = array();
            $params['table'] = 'tbl_linked_account_numbers';
            $conditions['id'] = $id;
            $conditions['user_id'] = $login_id;
            $params['single_row'] = TRUE;
            $data_info = $this->common_model->get_data($conditions, $params);
            if(empty($data_info)){
                $data = array(
                    'status' => FALSE,
                    'message' => "Account not exist.",
                    'data' => (object) array()
                );
                $status_code = '404';
                $this->response($data, $status_code);
            }else{
                $conditions = $params = array();
                $conditions['value'] = $insert_data;
                $conditions['where']['id'] = $data_info->id;
                $params['table'] = 'tbl_linked_account_numbers';
                $this->common_model->update($conditions, $params);
            }
           
            $data = array(
                'status' => TRUE,
                'message' => "Updated.",
                'data' => (object) array()
            );
            $status_code = '200';
            $this->response($data, $status_code);
        }else{
            $data = array(
                'status' => FALSE,
                'message' => "Post data empty.",
                'data' => (object) array()
            );
            $status_code = '404';
            $this->response($data, $status_code);
        }
    }

    public function deleteSynapseLinkedAccount_post(){
        $session_token  = $this->input->get_request_header('Sessionid');
        $user           = $this->checkSession($session_token);
        $login_id       = $user->user_id;
        if($this->post('id')){
            $id = $this->post('id');
            $insert_data = array(
                    'is_deleted'  => 1,
                    'modified_date' => current_date()
                );

            $conditions = $params = array();
            $params['table'] = 'tbl_linked_account_numbers';
            $conditions['id'] = $id;
            $conditions['user_id'] = $login_id;
            $params['single_row'] = TRUE;
            $data_info = $this->common_model->get_data($conditions, $params);
            if(empty($data_info)){
                $data = array(
                    'status' => FALSE,
                    'message' => "Account not exist.",
                    'data' => (object) array()
                );
                $status_code = '404';
                $this->response($data, $status_code);
            }else{
                $conditions = $params = array();
                $conditions['value'] = $insert_data;
                $conditions['where']['id'] = $data_info->id;
                $params['table'] = 'tbl_linked_account_numbers';
                $this->common_model->update($conditions, $params);
            }
           
            $data = array(
                'status' => TRUE,
                'message' => "Deleted.",
                'data' => (object) array()
            );
            $status_code = '200';
            $this->response($data, $status_code);
        }else{
            $data = array(
                'status' => FALSE,
                'message' => "Post data empty.",
                'data' => (object) array()
            );
            $status_code = '404';
            $this->response($data, $status_code);
        }
    }

    /**
     * Method Name : getSynapsepayUserInfo_post
     * Author Name : Lakhvinder Singh
     * Description :getSynapsepayUserInfo_post
     */
    public function deleteSynapsepayAccount_post() {
        $session_token = $this->input->get_request_header('Sessionid');
        $user = $this->checkSession($session_token);
        $login_id = $user->user_id;
        $conditions = $params = array();
        if($this->post('record_id')){
            $conditions = $params = array();
            $conditions['where']['id'] = $this->post('record_id');
            $conditions['value']['is_deleted'] = 0;
            $conditions['value']['modified_date'] = date('Y-m-d H:i:s');
            $params['table'] = 'tbl_synapsepay_card_info';
            $this->common_model->update($conditions, $params);
            $data = array(
                'status' => TRUE,
                'message' => 'Synapsepay Cards Deleted successfully',
                'data' => (object) array()
            );
            $status_code = '200';
            $this->response($data, $status_code);
        }else{
            $data = array(
                'status' => FALSE,
                'message' => 'Post data empty',
                'data' => (object) array()
            );
            $status_code = '404';
            $this->response($data, $status_code);            
        }
    }


       /**
     * Method Name : saveSynapsepayAccount
     * Author Name : Lakhvinder Singh
     * Description :saveSynapsepayAccount function using to store the value of synapsepay account 
     */
    public function saveSynapsepayAccount($login_id,$node_id,$synapse_user_id,$amount,$account_number,$routing_number,$recurring,$payment_type,$days,$times,$bank_name,$bank_logo) {        

        
                /*************************************Add Account_info **************************/
                $conditions = $params = array();
                $conditions['user_id']      = $login_id;
                $conditions['synapse_user_id'] = $synapse_user_id;
                $conditions['node_id']      = $node_id;
                $conditions['amount']       = $amount;
                $conditions['account_number']= $account_number;
                $conditions['payment_type']= $payment_type;
                $conditions['routing_number']= $routing_number;
                $conditions['recurring']    = $recurring;
                $conditions['days']         = $days;
                $conditions['times']        = $times;
                $conditions['bank_name']    = $bank_name;
                $conditions['bank_logo']    = $bank_logo;
                $conditions['status']       = 1;
                $conditions['modified_date'] = date('Y-m-d H:i:s');
                $params['table'] = 'tbl_synapsepay_card_info';
                $this->common_model->insert_data($conditions, $params);
           
    }



/*
  |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
  |||||||||||||||||||| END: Synapspay Related APIs and functions |||||||||||||||||||||||||||||||||||||||
  |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
*/





    function updateRecurringInfo_post(){
        $session_token  = $this->input->get_request_header('Sessionid');
        $user           = $this->checkSession($session_token);
        $login_id       = $user->user_id;
        
        $recurring      = $this->post('recurring');  //monthly,weekly
        $payment_type   = $this->post('payment_type');  //monthly,weekly
        $days           = $this->post('days');
        $times          = $this->post('times');
        $id             = $this->post('record_id'); 
        if($this->post('payment_method') == 'Stripe'){
            $conditions = $params = array();
            $conditions['where']['id'] = $id;
            $conditions['value']['recurring'] = $recurring;
            $conditions['value']['days'] = $days;
            $conditions['value']['times'] = $times;
            $conditions['modified_date'] = date('Y-m-d H:i:s');
            $params['table'] = 'tbl_stripe_card_info';
            $this->common_model->update($conditions, $params);
        }else{
            $conditions = $params = array();
            $conditions['where']['id'] = $id;
            $conditions['value']['recurring'] = $recurring;
            $conditions['value']['days'] = $days;
            $conditions['value']['times'] = $times;
            $conditions['modified_date'] = date('Y-m-d H:i:s');
            $params['table'] = 'tbl_synapsepay_card_info';
            $this->common_model->update($conditions, $params);
        }
        $data = array(
                'status' => TRUE,
                'message' => 'Updated Successful',
                'data' => (object) array()
        );
        $status_code = '200';
        $this->response($data, $status_code);
    }





/*
  |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
  ||||||||||||||||||||START: STRIPE Related APIs and functions |||||||||||||||||||||||||||||||||||||||
  |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
*/

    
    function setStripeCardToken_post(){
        $session_token  = $this->input->get_request_header('Sessionid');
        $user           = $this->checkSession($session_token);
        $login_id       = $user->user_id;
        $user_detail    = $this->getUserInfo($login_id);
        //print_r($user_detail); exit;
        $user_email     = $user_detail->email;
        require_once(FCPATH.'application/libraries/vendor/autoload.php');
        $stripe = array(
            "secret_key"      => STRIPE_SECRET_KEY,
            "publishable_key" => STRIPE_PULBLISHABLE_KEY
        );
        \Stripe\Stripe::setApiKey($stripe['secret_key']);
        $name_on_card = $this->post('name_on_card'); 
        if($this->post('save_card') == 1 && $this->post('token')){
            $stripe_user_info = $this->getUserInfo($login_id);

            $token          = $this->post('token');           
            try {
                $token_detail = \Stripe\Token::retrieve($token);                  
            } catch (Exception $e) {
                $status = TRUE;
                $result = (object) array();
                $data = array(
                    'status' => $status,
                    'message' => $e->getMessage(),
                    'data' => $result
                );
                $status_code = '200';
                $this->response($data, $status_code);
            }              // retrive the token information
            //$card_id = $token_detail['card']->created;
            //print_r($token_detail); exit;


            $exp_month      = $token_detail['card']->exp_month;
            $exp_year       = $token_detail['card']->exp_year;
            $last_digits    = $token_detail['card']->last_digits;
            $brand          = $token_detail['card']->brand;       
            $fingerprint    = $token_detail['card']->fingerprint;       
            

            $conditions = $params = array();
            $conditions['user_id'] = $login_id;
            $conditions['token'] = $token;
            $conditions['name_on_card'] = $name_on_card;
            $conditions['card_id'] = $token_detail['card']->id;
            $conditions['brand'] =  $token_detail['card']->brand;
            $conditions['address_city'] =  $token_detail['card']->address_city;
            $conditions['country'] =  $token_detail['card']->country;
            $conditions['exp_month'] =  $token_detail['card']->exp_month;
            $conditions['exp_year'] =  $token_detail['card']->exp_year;
            $conditions['funding'] =  $token_detail['card']->funding;
            $conditions['last_digits'] =  $token_detail['card']->last4;
            $conditions['fingerprint'] =  $token_detail['card']->fingerprint;
            $conditions['created']  = $token_detail->created;
            $params['table'] = 'tbl_stripe_card_info';

                
            $conditions_c = $params_c = array();
            $params_c['table'] = 'tbl_stripe_card_info';
            $conditions_c['fingerprint'] = $fingerprint;
            $conditions_c['user_id'] = $login_id;
            $params_c['single_row'] = TRUE;
            $stripe_card_exist = $this->common_model->get_data($conditions_c, $params_c);

            if(empty($stripe_card_exist)){           // Check Card is alreday added or not for this user
                if(empty($stripe_user_info->stripe_customer_id)){       // check account user is alreay exist or not
                    try {
                        $customer = \Stripe\Customer::create(array(
                            'email' => $user_email,
                            'description' => $user_email,
                            'source'  => $token
                        ));               
                        $this->updateStripeUserinfo($login_id,$customer->id);  // add stripe user 
                        $conditions['stripe_customer_id'] = $customer->id;            // Add Stripe customer id                    
                    } catch (Stripe_InvalidRequestError $e) {
                        $message = 'Invalid parameters were supplied to Stripes API';
                        $status = TRUE;
                        $result = (object) array();
                        $data = array(
                            'status' => $status,
                            'message' => $message,
                            'data' => $result
                        );
                        $status_code = '200';
                        $this->response($data, $status_code);
                    } catch (Stripe_AuthenticationError $e) {
                        $message = ' Authentication with Stripes API failed';
                        $status = TRUE;
                        $result = (object) array();
                        $data = array(
                            'status' => $status,
                            'message' => $message,
                            'data' => $result
                        );
                        $status_code = '200';
                        $this->response($data, $status_code);
                      // (maybe you changed API keys recently)
                    } catch (Stripe_ApiConnectionError $e) {
                        $message = ' Network communication with Stripe failed';
                        $status = TRUE;
                        $result = (object) array();
                        $data = array(
                            'status' => $status,
                            'message' => $message,
                            'data' => $result
                        );
                        $status_code = '200';
                        $this->response($data, $status_code);
                    } catch (Stripe_Error $e) {
                        $message = ' Display a very generic error to the user, and maybe send';
                        $status = TRUE;
                        $result = (object) array();
                        $data = array(
                            'status' => $status,
                            'message' => $message,
                            'data' => $result
                        );
                        $status_code = '200';
                        $this->response($data, $status_code);
                      // yourself an email
                    } catch (Exception $e) {
                        $message = 'Something wents wrong';
                        $status = TRUE;
                        $result = (object) array();
                        $data = array(
                            'status' => $status,
                            'message' => $message,
                            'data' => $result
                        );
                        $status_code = '200';
                        $this->response($data, $status_code);
                    } 
                                  
                }else{

                    
                    try {
                        //$token_detail = \Stripe\Token::retrieve($token);                  
                        $customer = \Stripe\Customer::retrieve($stripe_user_info->stripe_customer_id);
                        $customer->sources->create(array("source" => $token));
                    } catch (Exception $e) {
                        $status = TRUE;
                        $result = (object) array();
                        $data = array(
                            'status' => $status,
                            'message' => $e->getMessage(),
                            'data' => $result
                        );
                        $status_code = '200';
                        $this->response($data, $status_code);
                    }              
                    $conditions['stripe_customer_id'] = $stripe_user_info->stripe_customer_id;          // Add Stripe customer id if already exist         
                }
            }else{
  /*              $stripe_last_info = array();
                $conditions_get = $params_get = array();
                $params_get['table'] = 'tbl_stripe_card_info';
                $params_get['fields'] = array('id as record_id','name_on_card','stripe_customer_id','stripe_external_account_id','card_id','recurring','payment_type','days','times','brand','country','exp_month','exp_year','funding','last_digits','last_digits','fingerprint','created'); 
                $conditions_get['id'] = $stripe_card_exist->id;
                $params['single_row'] = TRUE;*/
                $conditions = $params = array();
                $params['table'] = 'tbl_stripe_card_info';
                $params['fields'] = array('id as record_id','name_on_card','stripe_customer_id','stripe_external_account_id','card_id','recurring','payment_type','days','times','brand','country','exp_month','exp_year','funding','last_digits','last_digits','fingerprint','created'); 
                $conditions['id'] = $stripe_card_exist->id;
                $params['single_row'] = TRUE;

                $stripe_info_is = $this->common_model->get_data($conditions, $params);

                $data = array(
                    'status' => TRUE,
                    'message' => 'Card already exist',
                    'data' => $stripe_info_is
                );
                $status_code = '200';
                $this->response($data, $status_code);
            }

            $last_id = $this->common_model->insert_data($conditions, $params);   // Insert new card
            $conditions = $params = array();
            $params['table'] = 'tbl_stripe_card_info';
            $params['fields'] = array('id as record_id','name_on_card','stripe_customer_id','stripe_external_account_id','card_id','recurring','payment_type','days','times','brand','country','exp_month','exp_year','funding','last_digits','last_digits','fingerprint','created'); 
            $conditions['id'] = $last_id;
            $params['single_row'] = TRUE;
            $stripe_last_info = $this->common_model->get_data($conditions, $params);
            $data = array(
                    'status' => TRUE,
                    'message' => 'Updated',
                    'data' => $stripe_last_info
                );
            $status_code = '200';
            $this->response($data, $status_code);
        }else{
            $data = array(
                    'status' => FALSE,
                    'message' => 'Post data empty',
                    'data' => (object) array()
                );
            $status_code = '404';
            $this->response($data, $status_code);
        }
    }

    // Delete after next build
    function getUserCardsDetail_post(){
        $session_token  = $this->input->get_request_header('Sessionid');
        $user           = $this->checkSession($session_token);
        $login_id       = $user->user_id;
        
        $users_cards = $this->getUserInfo($login_id);
        if(!empty($users_cards)){
            require_once(FCPATH.'application/libraries/vendor/autoload.php');
            $stripe = array(
                "secret_key"      => STRIPE_SECRET_KEY,
                "publishable_key" => STRIPE_PULBLISHABLE_KEY
            );
            \Stripe\Stripe::setApiKey($stripe['secret_key']);

            try{
                $customer_data = \Stripe\Customer::retrieve($users_cards->stripe_customer_id); 
                $message = 'Cards info';
            }  catch (Stripe_InvalidRequestError $e) {
                $message = 'Invalid parameters were supplied to Stripes API';
                $customer_data = (object) array();                
            } catch (Stripe_AuthenticationError $e) {
                $message = ' Authentication with Stripes API failed';
                $customer_data = (object) array();                
              // (maybe you changed API keys recently)
            } catch (Stripe_ApiConnectionError $e) {
                $message = ' Network communication with Stripe failed';
                $customer_data = (object) array();               
            } catch (Stripe_Error $e) {
                $message = ' Display a very generic error to the user, and maybe send';
                $customer_data = (object) array();                
              // yourself an email
            } catch (Exception $e) {
                $message = $e->getMessage();
                $customer_data = (object) array();                
            }            
        }else{
            $message = 'Cards info';
            $customer_data = (object) array();
        }
        $data = array(
            'status' => TRUE,
            'message' => $message,
            'data' => $customer_data,
        );
        $status_code = '200';
        $this->response($data, $status_code);
       
    }

    /**
     * Method Name : getStripeCardsDetail_post
     * Author Name : Lakhvinder Singh
     * Description : return stripe card list
     */
    function getStripeCardsDetail_post(){
        $session_token  = $this->input->get_request_header('Sessionid');
        $user           = $this->checkSession($session_token);
        $login_id       = $user->user_id;
        
        $conditions = $params = array();
        $params['table'] = 'tbl_stripe_card_info';
        $conditions['user_id'] = $login_id; 
        $params['fields'] = array('id as record_id','name_on_card','stripe_customer_id','stripe_external_account_id','card_id','recurring','payment_type','days','times','brand','country','exp_month','exp_year','funding','last_digits','last_digits','fingerprint','created'); 
        $info = $this->common_model->get_data($conditions, $params);
        $result['stripe_card'] = $info;
        $data = array(
            'status' => TRUE,
            'message' => 'Stripe Card List',
            'data' => $result,
        );
        $status_code = '200';
        $this->response($data, $status_code);
       
    }

    /**
     * Method Name : payByStripe_post
     * Author Name : Lakhvinder Singh
     * Description : payByStripe_post
     */
    function payByStripe_post(){
        $session_token  = $this->input->get_request_header('Sessionid');
        $user           = $this->checkSession($session_token);
        $login_id       = $user->user_id;
        $save_card      = $this->post('save_card');
        $card_id        = $this->post('card_id');
        $amount         = $this->post('amount');
        if($save_card == 1){
            $tenantStripeAccount = $this->getUserInfo($login_id);
            //echo $this->db->last_query(); exit;
            if(!empty($tenantStripeAccount->stripe_customer_id)){
                $amount = str_replace(".","",amount_formated($amount));
                require_once(FCPATH.'application/libraries/vendor/autoload.php');
                $stripe = array(
                    "secret_key"      => STRIPE_SECRET_KEY,
                    "publishable_key" => STRIPE_PULBLISHABLE_KEY
                );
                \Stripe\Stripe::setApiKey($stripe['secret_key']);

                $property_details = $this->propertyDetails($login_id);   // get property detail by tenant id 
                $ownerStripeAccount = $this->getUserInfo($property_details->owner_id); // get owner stripe account id using owner id
                //echo $property_details->owner_id.'--'.$login_id; exit;
                if(!empty($ownerStripeAccount)){            // if owner stripe account id exist
                    try {
                            $charge = \Stripe\Charge::create(
                                                                array(
                                                                    //'customer' => $tenantStripeAccount->stripe_customer_id, 
                                                                    'amount'   => $amount,
                                                                    'currency' => 'usd',
                                                                    'customer' => $tenantStripeAccount->stripe_customer_id,
                                                                    'description' => 'Rent paid',
                                                                    'card' => $card_id,
                                                                    'destination' => $ownerStripeAccount->stripe_external_account_id
                                                                )
                                                            );
                            $charge_id = $charge->id; 
                            $insert_data = array(
                                'mem_id'        => $login_id,
                                'property_id'   => $property_details->prop_id,
                                'transaction_id'=> $charge_id,
                                'amount'        => $charge->amount/100,
                                'status'        => 'PAID',     
                                'object'        => 'Charge',     
                                'account_number'=> $charge->source->last4,     
                                'description'   => 'Rent paid',
                                'modified_date' => current_date(),
                                'created_date'  => current_date(),
                                'date'  => $charge->created,
                                'user_type' => 'Tenant',
                                'payment_type' => 'Stripe',
                            );
                            $conditions = $params = array();
                            $conditions = $insert_data;
                            $params['table'] = 'tbl_payment';
                            $t_id = $this->common_model->insert_data($conditions, $params);
                            /*****************************************Mail Send *****************************/
					        $user_name 		= $tenantStripeAccount->first_name.' '.$tenantStripeAccount->last_name;
					        $user_email 	= $tenantStripeAccount->email;
				            $mail_message = 'Rent('.$insert_data['amount'] .') has been paid successfully';
				            $this->mailsending_model->sentPaymentInfo($user_name, $user_email, $mail_message);

                            $message = 'Paid';
                            $charge = $this->getTransaction($t_id);

                            $recurring      = $this->post('recurring');  //monthly,weekly
                            if($recurring == 1){
								$payment_type   = $this->post('payment_type');  //monthly,weekly
								$days           = $this->post('days');
								$times          = $this->post('times');
								$id             = $this->post('record_id'); 
								$conditions = $params = array();
								$conditions['where']['id'] = $id;
								$conditions['value']['amount'] = $charge->amount/100;
								$conditions['value']['recurring'] = $recurring;
								$conditions['value']['payment_type'] = $payment_type;
								$conditions['value']['days'] = $days;
								$conditions['value']['times'] = $times;
								$conditions['modified_date'] = date('Y-m-d H:i:s');
								$params['table'] = 'tbl_stripe_card_info';
								$this->common_model->update($conditions, $params);                            	
                            }
                          

                    } catch (Stripe_InvalidRequestError $e) {
                        $message = 'Invalid parameters were supplied to Stripes API';
                        $charge = (object) array();                
                    } catch (Stripe_AuthenticationError $e) {
                        $message = ' Authentication with Stripes API failed';
                        $charge = (object) array();                
                      // (maybe you changed API keys recently)
                    } catch (Stripe_ApiConnectionError $e) {
                        $message = ' Network communication with Stripe failed';
                        $charge = (object) array();               
                    } catch (Stripe_Error $e) {
                        $message = ' Display a very generic error to the user, and maybe send';
                        $charge = (object) array();                
                      // yourself an email
                    } catch (Exception $e) {
                        $message = $e->getMessage();
                        $charge = (object) array();                
                    }
                }else{   // if owner stripe account id not exist
                    $message = 'Property owner had been disabled payment for this property.';
                    $charge = (object) array();
                }

                $data = array(
                    'status' => TRUE,
                    'message' => $message,
                    'data' => $charge
                );
                $status_code = '200';
                $this->response($data, $status_code);
            }else{
                $data = array(
                    'status' => TRUE,
                    'message' => 'Customer Id not exist',
                    'data' =>(object) array(),
                );
                $status_code = '200';
                $this->response($data, $status_code);
            }

        }else{
          
                $token = $this->post('token');
                
                $amount = str_replace(".","",amount_formated($amount));
                require_once(FCPATH.'application/libraries/vendor/autoload.php');
                $stripe = array(
                    "secret_key"      => STRIPE_SECRET_KEY,
                    "publishable_key" => STRIPE_PULBLISHABLE_KEY
                );
                \Stripe\Stripe::setApiKey($stripe['secret_key']);

                $property_details = $this->propertyDetails($login_id);   // get property detail by tenant id
                //print_r($property_details); 
                $ownerStripeAccount = $this->getUserInfo($property_details->owner_id); // get owner stripe account id using owner id
                $tenantStripeAccount = $this->getUserInfo($login_id); // get tenant stripe account id using owner id
                //echo $property_details->owner_id.'--'.$login_id; exit;
                if(!empty($ownerStripeAccount)){            // if owner stripe account id exist
                    try {
                            $charge = \Stripe\Charge::create(array(
                                    //'customer' => $stripe_user_info->stripe_customer_id, 
                                    'amount'   => $amount,
                                    'currency' => 'usd',
                                    'description' => 'Rent paid',
                                    'source' => $token,
                                    'destination' => $ownerStripeAccount->stripe_external_account_id
                                ));
                            $charge_id = $charge->id; 
                            $insert_data = array(
                                'mem_id'        => $login_id,
                                'property_id'   => $property_details->prop_id,
                                'transaction_id'=> $charge_id,
                                'amount'        => $charge->amount/100,
                                'status'        => 'PAID',     
                                'object'        => 'Charge', 
                                'description'   => 'Rent paid',
                                'account_number'=> $charge->source->last4,
                                'modified_date' => current_date(),
                                'created_date'  => current_date(),
                                'date'  => $charge->created,
                                'user_type' => 'Tenant',
                                'payment_type' => 'Stripe',
                            );
                            $conditions = $params = array();
                            $conditions = $insert_data;
                            $params['table'] = 'tbl_payment';
                            $t_id = $this->common_model->insert_data($conditions, $params);
                            /*****************************************Mail Send *****************************/
					        $user_name 		= $tenantStripeAccount->first_name.' '.$tenantStripeAccount->last_name;
					        $user_email 	= $tenantStripeAccount->email;
				            $mail_message = 'Rent('.$insert_data['amount'] .') has been paid successfully';
				            $this->mailsending_model->sentPaymentInfo($user_name, $user_email, $mail_message);

                            $message = 'Paid';
                            $charge = $this->getTransaction($t_id);
                    } catch (Exception $e) {
                        $message = $e->getMessage();
                        $charge = (object) array();
                    }
                }else{   // if owner stripe account id not exist
                    $message = 'Property owner had been disabled payment for this property.';
                    $charge = (object) array();
                }

                $data = array(
                    'status' => TRUE,
                    'message' => $message,
                    'data' => $charge
                );
                $status_code = '200';
                $this->response($data, $status_code);
            

        }
    }

    /**
     * Method Name : updateStripeRecurringinfo_post
     * Author Name : Lakhvinder Singh
     * Description : updateStripeRecurringinfo_post
     */
/*    function updateStripeRecurringinfo_post(){
        $insert_data = array(
                                'recurring'     => $recurring,      // recurring info
                                'payment_type'  => $payment_type,   // recurring info
                                'days'          => $days,           // recurring info
                                'times'         => $times,          // recurring info
                            );
        $conditions = $params = array();
        $conditions['where']['id'] = $id;
        $conditions['value']['recurring'] = $recurring;
        $conditions['value']['payment_type'] = $payment_type;
        $conditions['value']['days'] = $days;
        $conditions['value']['times'] = $times;
        $params['table'] = 'tbl_payment';
        $t_id = $this->common_model->update($conditions, $params);
    }*/

    /**
     * Method Name : refundAmount_post
     * Author Name : Lakhvinder Singh
     * Description : refundAmount_post
     */
    function refundAmount_post(){
        $session_token  = $this->input->get_request_header('Sessionid');
        $user           = $this->checkSession($session_token);
        $login_id       = $user->user_id;
        $user_detail = $this->getUserInfo($login_id);
        if($this->post('transaction_id')){
            $transaction_id = $this->post('transaction_id');
            $transactionDetail = $this->getTransactionByTransactionId($transaction_id);
            $property_details = $this->propertyDetails($login_id);
            if(!empty($transactionDetail)){
                if($transactionDetail->payment_type == 'Stripe'){
                    //  Synapsepay Stripe
                    require_once(FCPATH.'application/libraries/vendor/autoload.php');
                    $stripe = array(
                        "secret_key"      => STRIPE_SECRET_KEY,
                        "publishable_key" => STRIPE_PULBLISHABLE_KEY
                    );
                    \Stripe\Stripe::setApiKey($stripe['secret_key']);
                    try {
                        $refund = \Stripe\Refund::create(array(
                          "charge" => $transaction_id
                        ));
                        $refund_id = $refund->id; 
                        $insert_data = array(
                            'mem_id'        => $login_id,
                            'property_id'   => $property_details->prop_id,
                            'transaction_id'=> $refund_id,
                            'amount'        => $refund->amount/100,
                            'status'        => 'Refund',
                            'description'   => 'Refund',
                            'modified_date' => current_date(),
                            'created_date'  => current_date(),
                            'date'  => $refund->created,
                            'user_type' => 'Tenant',
                            'payment_type' => 'Stripe',
                            'object' => 'Refund',
                        );
                        $conditions = $params = array();
                        $conditions = $insert_data;
                        $params['table'] = 'tbl_payment';
                        $t_id = $this->common_model->insert_data($conditions, $params);
                        /*****************************************Mail Send *****************************/
				        $user_name 		= $user_detail->first_name.' '.$user_detail->last_name;
				        $user_email 	= $user_detail->email;
			            $mail_message = 'Payment('.$insert_data['amount'] .') has been successfully cancelled';
			            $this->mailsending_model->sentPaymentInfo($user_name, $user_email, $mail_message);

                        $message = 'Refund';
                        $refund = $this->getTransaction($t_id);
                    } catch (Stripe_InvalidRequestError $e) {
                        $message = 'Invalid parameters were supplied to Stripes API';
                        $refund = (object) array();                
                    } catch (Stripe_AuthenticationError $e) {
                        $message = ' Authentication with Stripes API failed';
                        $refund = (object) array();                
                      // (maybe you changed API keys recently)
                    } catch (Stripe_ApiConnectionError $e) {
                        $message = ' Network communication with Stripe failed';
                        $refund = (object) array();               
                    } catch (Stripe_Error $e) {
                        $message = ' Display a very generic error to the user, and maybe send';
                        $refund = (object) array();                
                      // yourself an email
                    } catch (Exception $e) {
                        $message = $e->getMessage();
                        $refund = (object) array();                
                    }
                     $data = array(
                            'status' => TRUE,
                            'message' => $message,
                            'data' => $refund
                        );
                    $status_code = '200';
                    $this->response($data, $status_code);
                }else{
                    //  Synapsepay Transaction ID
                     $data = array(
                        'status' => TRUE,
                        'message' => 'Synapsepay pending',
                        'data' => (object) array()
                    );
                    $status_code = '200';
                    $this->response($data, $status_code);
                }
            }else{
                $data = array(
                    'status' => TRUE,
                    'message' => 'Transaction not exist',
                    'data' => (object) array()
                );
                $status_code = '200';
                $this->response($data, $status_code);
            }           
        }else{
            $data = array(
                'status' => FALSE,
                'message' => "Post data empty.",
                'data' => (object) array()
            );
            $status_code = '404';
            $this->response($data, $status_code);
        }
    }

    /**
     * Method Name : deleteUserStripeCard_post
     * Author Name : Lakhvinder Singh
     * Description : deleteUserStripeCard_post
     */
    function deleteUserStripeCard_post(){
        $session_token  = $this->input->get_request_header('Sessionid');
        $user           = $this->checkSession($session_token);
        $login_id       = $user->user_id;
        if($this->post('card_id')){
            $card_id = $this->post('card_id');
            require_once(FCPATH.'application/libraries/vendor/autoload.php');
            $stripe = array(
                "secret_key"      => STRIPE_SECRET_KEY,
                "publishable_key" => STRIPE_PULBLISHABLE_KEY
            );
            \Stripe\Stripe::setApiKey($stripe['secret_key']);
           
            try {
                $userStripeInfo = $this->getUserInfo($login_id); // get stripe customer id 
                $userStripeId = $userStripeInfo->stripe_customer_id;
                $customer = \Stripe\Customer::retrieve($userStripeId);
                $customer->sources->retrieve($card_id)->delete();  

                $conditions = $params = array();
                $conditions['card_id'] = $card_id;
                $params['table'] = 'tbl_stripe_card_info';
                $this->common_model->delete($conditions, $params);

                $message = "Successfully Deleted.";
                $charge = (object) array();                   
            } catch (Exception $e) {
                $message = $e->getMessage(); 
                $charge = (object) array();
            }
           
            $data = array(
                'status' => TRUE,
                'message' => $message,
                'data' => $charge
            );
            $status_code = '200';
            $this->response($data, $status_code);
        }else{
            $data = array(
                'status' => FALSE,
                'message' => "Post data empty.",
                'data' => (object) array()
            );
            $status_code = '404';
            $this->response($data, $status_code);
        }
    }


    /**
     * Method Name : updateStripeUserinfo
     * Author Name : Lakhvinder Singh
     * Description : updateStripeUserinfo
     */
    function updateStripeUserinfo($user_id,$stripe_customer_id){
    	 $insert_data = array(
            'stripe_customer_id'=> $stripe_customer_id,
        );
        $conditions = $params = array();
        $conditions['value'] = $insert_data;
        $conditions['where']['mem_id'] = $user_id;
        $params['table'] = 'tbl_members';
        $this->common_model->update($conditions, $params);
    }



/*
  |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
  ||||||||||||||||||||END: STRIPE Related APIs and functions |||||||||||||||||||||||||||||||||||||||
  |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
*/





    /**
     * Method Name : getTransaction
     * Author Name : Lakhvinder Singh
     * Description : getTransaction
     */
    function getTransaction($id){
        $conditions = $params = array();
        $conditions['id']   = $id;
        $params['single_row'] = true;
        $params['table']        = 'tbl_payment';
        $info             = $this->common_model->get_data($conditions, $params);
        return $info;
    }

    /**
     * Method Name : getTransactionByTransactionId
     * Author Name : Lakhvinder Singh
     * Description : getTransactionByTransactionId
     */   
    function getTransactionByTransactionId($transaction_id){
        $conditions = $params = array();
        $conditions['transaction_id']   = $transaction_id;
        $params['single_row'] = true;
        $params['table']        = 'tbl_payment';
        $info             = $this->common_model->get_data($conditions, $params);
        return $info;
    }


    function getTenantPropertyInfo_post(){
        $session_token  = $this->input->get_request_header('Sessionid');
        $user           = $this->checkSession($session_token);
        $login_id       = $user->user_id;

        $conditions = $params = array();
        $params['table'] = 'tbl_properties_lease_detail';
        $conditions['tenant_id'] = $login_id;
        $params['single_row'] = true;
        $info = $this->common_model->get_data($conditions, $params);
        //print_R($info); exit;
        $result = new stdClass();
        $property_details = $this->propertyDetails($info->tenant_id);
        $result->property_detail = $property_details;

        $SynapseDetails = $this->getSynapseDetails($info->owner_id);
        $result->synapseDetail = $SynapseDetails;
        $result->synapseLinkedAccountStatus = getSynapseSetUpStatus($info->owner_id);

        $StripeDetails = $this->getUserInfo($info->owner_id);
        $result->landlord_stripe_id = $StripeDetails->stripe_customer_id;

        $result->subscription_status = $this->common_model->getPropertySubscriptionStatus($info->owner_id,$property_details->prop_id);

        $tenantDetails = $this->getUserInfo($login_id);
        $result->tenant_info = $tenantDetails;
        $result->stripe_secret_key              = STRIPE_SECRET_KEY;
        $result->stripe_secret_publishable_key  = STRIPE_PULBLISHABLE_KEY;
        $result->stripe_client_id               = SYNAPSE_CLIENT_ID;
        $result->synapse_client_secret          = SYNAPSE_CLIENT_SECRET;
        //$result->static_ip_address              = SYNAPSE_IP_ADDRESS;
        //$result->static_synapse_fingerprint     = SYNAPSE_FINGERPRINT;
        $result->synapse_api_base_url           = SYNAPSE_API_BASE_URL;
        $result->lease_doc                   = $this->getSingleLeaseDocs($login_id, $property_details->prop_id);

        $data = array(
            'status' => TRUE,
            'message' => 'Property Detail',
            'data' => $result
        );
        $status_code = '200';
        $this->response($data, $status_code);
    }


    function getPymentHistory_post(){
        $session_token  = $this->input->get_request_header('Sessionid');
        $user           = $this->checkSession($session_token);
        $login_id       = $user->user_id;
        if($this->post('limit')){

            $conditions = $params = array();
            $params['table']        = 'tbl_payment';
            $conditions['mem_id']   = $login_id;
            $params['limit']        = $this->post('limit');
            $params['offset']       = $this->post('offset');
            $params['order_by']     = 'id DESC';
            $info             = $this->common_model->get_data($conditions, $params);
            //echo $this->db->last_query(); exit;
            $data = array(
                'status' => TRUE,
                'message' => 'Payment History',
                'data' => $info
            );
            $status_code = '200';
            $this->response($data, $status_code);
        }else{
            $data = array(
                'status' => FALSE,
                'message' => "Post data empty.",
                'data' => (object) array()
            );
            $status_code = '404';
            $this->response($data, $status_code);
        }
    }




    public function rentDueAmount_get(){
        $session_token  = $this->input->get_request_header('Sessionid');
        $user           = $this->checkSession($session_token);
        $login_id       = $user->user_id;       
        
        $due_info = getDuePaymentInfo($login_id);
        $due_amount      = $due_info['due_amount']; 
        $due_date_formated      = $due_info['due_date']; 

        $conditions = $params = array();
        $params['table']        = 'tbl_payment';
        $conditions['mem_id']   = $login_id;
        $params['order_by']     = 'id DESC';
        $params['single_row']   = TRUE;
        $last_transaction       = $this->common_model->get_data($conditions, $params);

        $conditions = $params = array();
        $params['table'] = 'tbl_stripe_card_info';
        $conditions['user_id'] = $login_id; 
        $params['fields'] = array('id as record_id'); 
        $card_info = $this->common_model->get_data($conditions, $params);
        if(empty($card_info)){
            $saved_stripe_card = false;
        }else{
            $saved_stripe_card = true;
        }

        $conditions = $params = array();
        $conditions['user_id']   = $login_id;
        $conditions['is_deleted'] = 1;
        $params['table']        = 'tbl_synapsepay_card_info'; 
        $synapse_data           = $this->common_model->get_data($conditions, $params);
        if(empty($synapse_data)){
            $saved_synapse_card = false;
        }else{
            $saved_synapse_card = true;
        }

        $result = array(
                    'due_date' => $due_date_formated, 
                    'due_amount' => $due_amount,
                    'last_transaction' => $last_transaction,
                    'recurring_info' => $this->getRecurringDetail($login_id),
                    'saved_stripe_card' => $saved_stripe_card,
                    'saved_synapse_card' => $saved_synapse_card
                );
        $data = array(
            'status' => TRUE,
            'message' => "Detail.",
            'data' => $result
        );
        $status_code = '200';
        $this->response($data, $status_code);
       
    }
    

    function getRecurringDetail($user_id){
        $recurring_status = false;
        $recurring_detail = (object) array();
        $conditions = $params = array();
        $conditions['user_id']   = $user_id;
        $conditions['recurring']   = 1;
        $params['single_row'] = true;
        $params['table']        = 'tbl_stripe_card_info';
        $stripe_data             = $this->common_model->get_data($conditions, $params);
        if(empty($stripe_data)){
            $conditions = $params = array();
            $conditions['user_id']   = $user_id;
            $conditions['recurring'] = 1;
            $conditions['is_deleted'] = 1;
            $params['single_row'] = true;
            $params['table']        = 'tbl_synapsepay_card_info'; 
            $synapse_data           = $this->common_model->get_data($conditions, $params);
            //print_r($synapse_data); exit;
            if(empty($synapse_data)){
                $recurring_status = FALSE;
                $recurring_detail = (object) array();
            }else{
                $synapse_data->recurring_payment_type = 'Synapse';
                $recurring_status = TRUE;
                $recurring_detail = $synapse_data;
            }
        }else{
            $stripe_data->recurring_payment_type = 'Stripe';
            $recurring_status = TRUE;
            $recurring_detail = $stripe_data;
        }
        $data = array('recurring_status' => $recurring_status, 'recurring_detail' => $recurring_detail);
        return $data;
    }

    

}

/*
public function all_categories() {

   $session_token = $this->input->get_request_header('Sessionid');
        $user = $this->checkSession($session_token);
        if ($this->post()) {
            $user_id = $user->user_id;
        } else {
            $data = array(
                'status' => FALSE,
                'message' => 'Post data empty',
                'data' => array(),
            );
            $status_code = '404';
            $this->response($data, $status_code);
        }
 }
 * 
 * 
 *  */