<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Register extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('mailsending_model');
        $this->load->library('form_validation');
        $this->load->model('member_model');
        $this->load->model('properties_model');
        $this->mem_id = $this->session->userdata('MEM_ID');
        /*         * *** Unset property id Session *** */
        $this->session->unset_userdata('PROPERTY_ID');
    }

    #============================== User Register Function ===================#

    public function index() {
        /*         * ****** Start Loading models as per function requirement ************* */
        $this->load->model('country_model');
        /*         * ****** End Loading models as per function requirement ************* */
        $output['title'] = 'Register';
        $this->checkLogin();
        $output['title'] = 'Register';
        $page_type = $this->input->get('page');

        if ($this->session->userdata('MEM_ID') != '')
            redirect('dashboard', 'refresh');

        /************captcha &*******************/
        /*$this->load->library('mathcaptcha');
        $config['question_format'] = 'numeric';
        $config['answer_format'] = 'numeric';
        $config['question_max_number_size'] = 10;
        $this->mathcaptcha->init($config);*/
        if (!empty($_POST)) {
            $failure = 0;
            $this->form_validation->set_rules('first_name', 'First Name', 'trim|required');
            $this->form_validation->set_rules('last_name', 'Last Name', 'trim|required');
            //$this->form_validation->set_rules('company_name', 'Company Name', 'trim|required');
            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[tbl_members.email]');
            $this->form_validation->set_rules('password', 'Password', 'required|min_length[6]');
            $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'required|matches[password]');
            //   $this->form_validation->set_rules('country_code', 'Country Code', 'trim|required');
            $this->form_validation->set_rules('mobile', 'Mobile', 'required');
            //$this->form_validation->set_rules('country', 'Country', 'required');
            // $this->form_validation->set_rules('state', 'State', 'required');
            $this->form_validation->set_rules('terms', 'Terms & conditions', 'trim|callback_terms_condition[]');

            $this->form_validation->set_message('is_unique', '%s is already taken.');
            $this->form_validation->set_message('required', '%s is required');
            $this->form_validation->set_rules('math_captcha', 'Math CAPTCHA', 'required');

            if ($this->form_validation->run()) {
                $mathcaptcha_answer = $this->input->post('mathcaptcha_answer');
                $answer = $this->input->post('math_captcha');
                if($this->direct_check_answer($answer,$mathcaptcha_answer) == true){

                    $regId = $this->member_model->register();
                    $this->mailsending_model->landlordRegistrationFromFrontEmail($regId);
                    $this->session->set_userdata('MEM_ID', $regId);
                    $success_message = '<p>Your registration on our site is successfull. Please check your mail for further details.</p>';

                }else{
                    $error_message = 'Please enter valid captcha.';
                    $failure = true;
                }
            } else {
                //$data['math_captcha_question'] = $this->mathcaptcha->get_question();
                $error_message = validation_errors();
                $failure = true;
            }
            if ($this->input->is_ajax_request()) {
                if (isset($failure) && $failure == TRUE) {
                    $data['success'] = false;
                    $data['error_message'] = $error_message;
                } else {
                    $last_url = site_url('add-property');
                    $data['success'] = true;
                    $data['resetform'] = true;
                    $data['success_message'] = $success_message;
                    $data['url'] = $last_url;
                }
                $data['slideToThisForm'] = true;
                echo json_encode($data);
                die;
            }
        }
        //$data['math_captcha_question'] = $this->mathcaptcha->get_question();
        $output['allcountry'] = $this->country_model->getAllCountries();
        //~ if($page_type=="popup")
        //~ {
        //~ $this->load->view($this->config->item('defaultfolder').'/register_popup',$output);
        //~ }
        //~ else
        //~ {
        //~ $this->load->view($this->config->item('defaultfolder').'/home_header',$output);
        //~ $this->load->view($this->config->item('defaultfolder').'/register');
        //~ $this->load->view($this->config->item('defaultfolder').'/footer');
        //~ }
    }

    public function _check_answer($answer){
        if($this->session->flashdata('mathcaptcha_answer') == $answer ){
            return true;
        }else{
            $this->form_validation->set_message('_check_answer', $this->session->flashdata('mathcaptcha_answer').'Please enter valid captcha.');
            return false;
        }
    }

     public function direct_check_answer($answer,$mathcaptcha_answer){
        if($mathcaptcha_answer == $answer ){
            return true;
        }else{
            return false;
        }
    }

    public function forgot_password() {
        $output['title'] = 'Forgot Password';
        $this->checkLogin();
        if (!empty($_POST)) {
            $errorMsg = '';
            $output['email'] = trim($this->input->post('email'));
            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
            $this->form_validation->set_message('required', '%s is required');
            #===============Form Validation Run Here ====================================================================#
            if ($this->form_validation->run()) {
                $memRec = $this->member_model->checkEmail($this->input->post('email'));
                //print_r($memRec); exit;
                if (isset($memRec->email) && strtolower($memRec->email) == strtolower($this->input->post('email'))) {
                    $strnew2 = 'abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
                    $shuff2 = str_shuffle($strnew2);
                    $verification_code = substr($shuff2, 0, 8);
                    $conditions = $params = array();
                    $conditions['value']['verification_code'] = $verification_code;
                    $conditions['where']['mem_id'] = $memRec->mem_id;
                    $params['table'] = 'tbl_members';
                    $this->common_model->update($conditions, $params);
                    $memRec2 = $this->member_model->checkEmail($this->input->post('email'));
                    $this->mailsending_model->userforgotPasswordEmail($memRec2);
                    $success_message = 'Success,New Password has successfully sent to registered Email Account.';
                } else {
                    $errorMsg.= 'Sorry,Email you entered is not registered with our site.';
                    $failure = true;
                }
            } else {
                $errorMsg.= validation_errors();
                $failure = true;
            }
            if ($this->input->is_ajax_request()) {
                if (isset($failure) && $failure == TRUE) {
                    $data['success'] = false;
                    $data['error_message'] = $errorMsg;
                } else {
                    $data['success'] = true;
                    $data['url'] = site_url('');
                    $data['resetForm'] = true;
                    $data['success_message'] = $success_message;
                }
                $data['scrollToThisForm'] = true;
                echo json_encode($data);
                die;
            }
        }

        $this->load->view($this->config->item('defaultfolder') . '/home_header', $output);
        $this->load->view($this->config->item('defaultfolder') . '/forget_password');
        $this->load->view($this->config->item('defaultfolder') . '/footer');
    }

    public function reset_password($verification_code) {
        $output['title'] = 'Reset Password';
        $errorMsg = '';
        if (!$verification_code)
            redirect('login');
        //$this->checkLogin();
        if (!empty($_POST)) {
            $failure = FALSE;
            $this->form_validation->set_rules('password', 'Password', 'required|min_length[6]');
            $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'required|matches[password]');

            $this->form_validation->set_message('required', '%s is required');
            #===============Form Validation Run Here ====================================================================#
            if ($this->form_validation->run()) {
                $memId = $this->member_model->resetPassword($verification_code);
                $success_message = 'Your password has been changed successfully.';
                $this->mailsending_model->userResetSucessEmail($memId);
            } else {
                $errorMsg.= validation_errors();
                $failure = true;
            }
            if ($this->input->is_ajax_request()) {
                if (isset($failure) && $failure == TRUE) {
                    $data['success'] = false;
                    $data['error_message'] = $errorMsg;
                } else {
                    $data['success'] = true;
                    $data['url'] = site_url('');
                    $data['resetForm'] = true;
                    $data['success_message'] = $success_message;
                }
                $data['scrollToThisForm'] = true;
                echo json_encode($data);
                die;
            }
        }

        $conditions = $params = array();
        $params['table'] = 'tbl_members';
        $conditions['verification_code'] = $verification_code;
        $params['single_row'] = TRUE;
        $output['check'] = $this->common_model->get_data($conditions, $params);


        $this->load->view($this->config->item('defaultfolder') . '/home_header', $output);
        $this->load->view($this->config->item('defaultfolder') . '/reset_password');
        $this->load->view($this->config->item('defaultfolder') . '/footer');
    }

    #=============Email Verification Process=======================================================#

    public function validateAccount($verified_code) {
        $val = $this->member_model->doValidateEmail($verified_code);
        if ($val == 1) {
            $this->session->set_userdata('SUCCESS_MESSAGE', 'Your email has been verified, you may now login to you acount');
            redirect(site_url('login'));
        } elseif ($val == 0) {
            $this->session->set_userdata('ERROR_MESSAGE', 'This link has been expired');
            redirect('');
        }
    }

    #============================== Registration Information Page ========================================================#

    public function checkLogin() {
        if ($this->session->userdata('MEM_ID') != '') {
            if ($this->input->is_ajax_request()) {
                $data['success'] = true;
                $data['resetform'] = true;
                $data['url'] = site_url();

                echo json_encode($data);
                die;
            } else {
                redirect('dashboard');
            }
        }
    }

    #============================== User Login Function ==================================================================#

    public function login() {
        $output['title'] = 'Login';
        $this->checkLogin();
        $output['pagename'] = 'login';
        if (!empty($_POST)) {
            $this->session->unset_userdata('PAGE_ERROR_MESSAGE');
            $url = '';
            $output['email'] = trim($this->input->post('email'));
            $this->form_validation->set_rules('email', 'email', 'trim|required');
            $this->form_validation->set_rules('password', 'password', 'trim|required');
            $this->form_validation->set_message('required', '%s is required');
            #===============Form Validation Run Here =============================#
            if ($this->form_validation->run()) {
                $error_msg = $this->member_model->checkLogin();
                if ($this->input->post('keeplogin')) {
                    $this->load->helper('cookie');
                    $cookie_str = md5($this->input->post('email') . rand());
                    $cookie = array(
                        'name' => 'remember',
                        'value' => $cookie_str,
                        'expire' => time() + 86500,
                        'path' => '/',
                    );
                    //remember user                         
                    set_cookie($cookie);
                    $conditions = $params = array();
                    $conditions['where']['email'] = $this->input->post('email');
                    $conditions['value']['cookie_token'] = $cookie_str;
                    $params['table'] = 'tbl_members';
                    $this->common_model->update($conditions, $params);
                }


                if ($error_msg != "") {
                    $error_message = $error_msg;
                    $failure = true;
                } else {
                    $success_message = 'Successfully logged in.';
                }
            } else {
                $error_message = validation_errors();
                $failure = true;
            }
            if ($this->input->is_ajax_request()) {
                if (isset($failure) && $failure == TRUE) {
                    $data['success'] = false;
                    $data['error_message'] = $error_message;
                } else {

                    $allProperties = $this->properties_model->frontGetCountAllPropertiesByLandlordId($this->session->userdata('MEM_ID'));
                    if (!$allProperties) {
                        $last_url = site_url('add-property');
                    } else if ($this->session->userdata('REDIRECT_URL')) {
                        $last_url = $this->session->userdata('REDIRECT_URL');
                        $this->session->unset_userdata('REDIRECT_URL');
                    } else {
                        $last_url = site_url('dashboard');
                    }
                    $data['success'] = true;
                    $data['resetform'] = true;
                    $data['selfReload'] = false;
                    $data['url'] = $last_url;
                    $data['success_message'] = $success_message;
                }
                $data['slideToThisForm'] = true;
                echo json_encode($data);
                die;
            }else{
                redirect('dashboard');
            }
        }
        //~ if($page_type=="popup")
        //~ {
        //~ $this->load->view($this->config->item('defaultfolder').'/register_popup',$output);
        //~ }
        //~ else
        //~ {
        //~ $this->load->view($this->config->item('defaultfolder').'/header',$output);
        //~ $this->load->view($this->config->item('defaultfolder').'/login');
        //~ $this->load->view($this->config->item('defaultfolder').'/footer');
        //~ }
    }

    #=================== For Logout =============================#

    public function logout() {
        $this->load->helper('cookie');
        $cookie = array(
            'name' => 'remember',
            'value' => '',
            'expire' => '0',
        );
        delete_cookie($cookie); //forgot remeber me option
        $this->member_model->doLogout();
        //$this->session->sess_destroy();
        redirect('');
    }

    function terms_condition() {
        $termsCondition = $this->input->post('terms');
        if (empty($termsCondition)) {
            $this->form_validation->set_message('terms_condition', 'Please accept the Terms & Conditions Policy.');
            return FALSE;
        } else
            return TRUE;
    }

    public function underconstruction() {
        $this->load->view($this->config->item('template') . '/header');
        $this->load->view($this->config->item('template') . '/under_construction');
        $this->load->view($this->config->item('template') . '/footer');
    }

}
