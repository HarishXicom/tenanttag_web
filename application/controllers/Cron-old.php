<?php

/**
 * Cron Controller
 * Purpose : Manage call tracking API
 * @project CRM
 * @since 17 Aug 2015
 * @version Codeignitor 2.2.1
 * @author : Jasbir Singh
 */
class Cron extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model(array('common_model'));
        ini_set('display_errors', 1);
    }

    /**
     * Method Name : send_schedule_sms
     * Author Name : Jasbir Singh
     * Description : used for running crons to send schedule sms
     */
    public function send_schedule_sms() {
        $params = $conditions = array();
        $params['complex'] = true;
        $conditions['tables'] = array(
            'sms' => 'tbl_sms_schedule',
            'tenant' => 'tbl_members',
        );
        $conditions['table'] = 'sms';
        $conditions['on']['tenant'] = array(
            'sign' => '=',
            'column' => 'mem_id',
            'alias_column' => 'tenant_id',
            'alias_other' => 'sms',
        );
        $conditions['where']['scheduled_on']['alias'] = 'sms';
        $conditions['where']['scheduled_on']['value'] = array(
            'operator' => 'AND',
            'sign' => '<',
            'key' => 'scheduled_on',
            'value' => current_date()
        );
        $params['fields'] = array('sms.*', 'tenant.mobile_no');
        $msg = $this->common_model->get_data($conditions, $params);
        //echo '<pre>';print_r($conditions);die;
        if (!empty($msg)) {
            $this->load->helper('twilio_helper');  // references library/twilio_helper.php
            $service = get_twilio_service();
            foreach ($msg as $sms) {
                try {
                    $number = "+1" . preg_replace("/[^0-9]/", "", $sms->mobile_no);
                    $msgtext = $sms->message." -".SIGNATURE;;
//send sms                    
                    if ($sms->end_repeat == 'infinte') {
                        $service->account->sms_messages->create("+19046743077", "$number", "$msgtext", array());
                        $this->message_log($msgtext, $number, $sms->tenant_id, $sms->property_id, $sms->owner_id);
                    } else {
                        if ($sms->occurences > 0) {
                            $service->account->sms_messages->create("+19046743077", "$number", "$msgtext", array());
                            $this->message_log($msgtext, $number, $sms->tenant_id, $sms->property_id, $sms->owner_id);
                        }
                    }
//update dates                    
                    if ($sms->repeat_type == 'year') {
                        $newdate = date('Y-m-d H:i:s', strtotime('+1 year', strtotime($sms->scheduled_on)));
                    } elseif ($sms->repeat_type == 'month') {
                        $newdate = date('Y-m-' . $sms->repeat_every . ' H:i:s', strtotime('+1 month', strtotime($sms->scheduled_on)));
                    } else {
                        $newdate = date('Y-m-d' . ' H:i:s', strtotime('+' . $sms->repeat_every . ' week', strtotime($sms->scheduled_on)));
                    }
                    $conditions = $params = array();
                    $conditions['where']['id'] = $sms->id;
                    $conditions['value']['scheduled_on'] = $newdate;
                    if ($sms->occurences > 0) {
                        $conditions['value']['occurences'] = $sms->occurences - 1;
                    }
                    $params['table'] = 'tbl_sms_schedule';
                    $this->common_model->update($conditions, $params);
                } catch (Exception $e) {
                    $file = 'cron_sms_error_logs.txt';
                    if (file_exists($file)) {
                        $current = file_get_contents($file);
                        $current .= $e->getMessage() . "\n";
                        file_put_contents($file, $current);
                    }
                }
            }
        }
        echo 'done';
        exit;
    }

    public function message_log($msg, $number, $tenant, $property, $owner_id) {
        $conditions = $params = array();
        $conditions['message'] = $msg;
        $conditions['owner_id'] = $owner_id;
        $conditions['number'] = $number;
        $conditions['tenant_id'] = $tenant;
        $conditions['property_id'] = $property;
        $conditions['date_created'] = date('Y-m-d H:i:s');
        $params['table'] = 'tbl_sms_logs';
        $this->common_model->insert_data($conditions, $params);
    }

    /**
     * Method Name : send_schedule_sms
     * Author Name : Jasbir Singh
     * Description : used for running crons to send schedule sms
     */
    public function send_admin_schedule_sms() {
        $params = $conditions = array();
        $params['complex'] = true;
        $conditions['tables'] = array(
            'sms' => 'tbl_admin_schedule',
            'user' => 'tbl_members',
        );
        $conditions['table'] = 'sms';
        $conditions['on']['user'] = array(
            'sign' => '=',
            'column' => 'mem_id',
            'alias_column' => 'user_id',
            'alias_other' => 'sms',
        );
        $conditions['where']['scheduled_on']['alias'] = 'sms';
        $conditions['where']['scheduled_on']['value'] = array(
            'operator' => 'AND',
            'sign' => '<',
            'key' => 'scheduled_on',
            'value' => current_date()
        );
        $params['fields'] = array('sms.*', 'user.mobile_no');
        $msg = $this->common_model->get_data($conditions, $params);
        //echo '<pre>';print_r($conditions);die;
        if (!empty($msg)) {
            $this->load->helper('twilio_helper');  // references library/twilio_helper.php
            $service = get_twilio_service();
            foreach ($msg as $sms) {
                try {
                    $number = "+1" . preg_replace("/[^0-9]/", "", $sms->mobile_no);
                    $msgtext = $sms->message." -".SIGNATURE;
//send sms                    
                    if ($sms->end_repeat == 'infinte') {
                        $service->account->sms_messages->create("+19046743077", "$number", "$msgtext", array());
                        $this->admin_message_log($msgtext, $number, $sms->user_id);
                    } else {
                        if ($sms->occurences > 0) {
                            $service->account->sms_messages->create("+19046743077", "$number", "$msgtext", array());
                            $this->admin_message_log($msgtext, $number, $sms->user_id);
                        }
                    }
//update dates                    
                    if ($sms->repeat_type == 'year') {
                        $newdate = date('Y-m-d H:i:s', strtotime('+1 year', strtotime($sms->scheduled_on)));
                    } elseif ($sms->repeat_type == 'month') {
                        $newdate = date('Y-m-' . $sms->repeat_every . ' H:i:s', strtotime('+1 month', strtotime($sms->scheduled_on)));
                    } else {
                        $newdate = date('Y-m-d' . ' H:i:s', strtotime('+' . $sms->repeat_every . ' week', strtotime($sms->scheduled_on)));
                    }
                    $conditions = $params = array();
                    $conditions['where']['id'] = $sms->id;
                    $conditions['value']['scheduled_on'] = $newdate;
                    if ($sms->occurences > 0) {
                        $conditions['value']['occurences'] = $sms->occurences - 1;
                    }
                    $params['table'] = 'tbl_admin_schedule';
                    $this->common_model->update($conditions, $params);
                } catch (Exception $e) {
                    $file = 'cron_sms_error_logs.txt';
                    if (file_exists($file)) {
                        $current = file_get_contents($file);
                        $current .= $e->getMessage() . "\n";
                        file_put_contents($file, $current);
                    }
                }
            }
        }
        echo 'done';
        exit;
    }

    public function admin_message_log($msg, $number, $user) {
        $conditions = $params = array();
        $conditions['message'] = $msg;
        $conditions['number'] = $number;
        $conditions['user_id'] = $user;
        $conditions['date_created'] = date('Y-m-d H:i:s');
        $params['table'] = 'tbl_admin_sms_logs';
        $this->common_model->insert_data($conditions, $params);
    }

    public function send_amenity_sms() {
        $month = date('n');
        $day = date('j');
        $params = $conditions = array();
        $params['complex'] = true;
        $conditions['tables'] = array(
            'dt' => 'tbl_sms_dates',
            'sms' => 'tbl_saved_sms',
        );
        $conditions['table'] = 'dt';
        $conditions['on']['sms'] = array(
            'sign' => '=',
            'column' => 'id',
            'alias_column' => 'saved_sms_id',
            'alias_other' => 'dt',
        );
        $conditions['where']['month']['alias'] = 'dt';
        $conditions['where']['month']['value'] = $month;
        $conditions['where']['day']['alias'] = 'dt';
        $conditions['where']['day']['value'] = $day;
        $conditions['where']['status']['alias'] = 'sms';
        $conditions['where']['status']['value'] = 'Active';
//fetch any sms for today        
        $msg = $this->common_model->get_data($conditions, $params);
        if (!empty($msg)) {
//fetch active tenants 
            $tenants = $this->fetchActiveTenants();
            if (!empty($tenants)) {
                foreach ($msg as $sms) {
                    foreach ($tenants as $tenant) {
                       // $check = true;
                        $check = $this->checkAmenityExists($tenant->property_id, $sms->amenity);
                        if ($check) {
                            $data[] = (object) array('prop'=>$tenant->property_id,'user_id' => $tenant->tenant_id, 'mobile_no' => $tenant->mobile_no, 'message' => $sms->message);
                        }
                    }
                }
            }
        }
        if (!empty($data)) {
            $this->load->helper('twilio_helper');  // references library/twilio_helper.php
            $service = get_twilio_service();
            foreach ($data as $sms) {
                try {
                    $number = "+1" . preg_replace("/[^0-9]/", "", $sms->mobile_no);
                    $msgtext = $sms->message." -".SIGNATURE;
                    //send sms
                    $service->account->sms_messages->create("+19046743077", "$number", "$msgtext", array());
                    $this->admin_message_log($msgtext, $number, $sms->user_id);
                } catch (Exception $e) {
                    $file = 'cron_sms_error_logs.txt';
                    if (file_exists($file)) {
                        $current = file_get_contents($file);
                        $current .= $e->getMessage() . "\n";
                        file_put_contents($file, $current);
                    }
                }
            }
        }
        echo 'done';
        die;
    }

    public function fetchActiveTenants() {
        $params = $conditions = array();
        $params['complex'] = true;
        $conditions['tables'] = array(
            'l' => 'tbl_properties_lease_detail',
            'u' => 'tbl_members',
        );
        $conditions['table'] = 'l';
        $conditions['on']['u'] = array(
            'sign' => '=',
            'column' => 'mem_id',
            'alias_column' => 'tenant_id',
            'alias_other' => 'l',
        );
        $conditions['where']['move_status']['alias'] = 'u';
        $conditions['where']['move_status']['value'] = '1'; //checkin status
        $params['fields'] = array('l.tenant_id', 'u.mobile_no', 'u.first_name', 'l.property_id');
        $tenants = $this->common_model->get_data($conditions, $params);
        return $tenants;
    }

    public function checkAmenityExists($prop_id, $amen) {
        if (is_numeric($amen)) {
            $check = $this->common_model->checkAmenity($prop_id, $amen);
            return $check->cnt;
        } else {
            $match = 'N/A';
            if ($amen == 'smoke') {
                $col = 'safety_equipment1';
            } elseif ($amen == 'heat') {
                $col = 'heat_type';
            } elseif ($amen == 'ac') {
                $col = 'ac_type';
            } elseif ($amen == 'fireexten') {
                $col = 'safety_equipment2';
            } elseif ($amen == 'yard') {
                $col = 'yard';
                $match = 'no';
            }
            $check = $this->common_model->checkService($prop_id, $col, $match);
            return $check->cnt;
        }
        return true;
    }

}
