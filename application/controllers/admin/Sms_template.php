<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Sms_template extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->pageName = 'Sms Templates';
        $this->breadcrum1 = 'Sms Templates';
        $this->breadcrum2 = '';
        $this->breadcrum3 = '';
        $this->breadcrum4 = 'Edit Template';
        $this->breadcrum2_url = site_url('admin/sms_template');
        //$this->breadcrum3_url = site_url('admin/checklist/add');
        $this->breadcrum4_url = site_url('admin/sms_template/edit');
        $this->load->model('messages_model');
        $this->common_model->checkAdminLogin();
        $this->js();
    }

    private function js() {
        $config[] = 'ckeditor/ckeditor.js';
        $this->config->set_item('adminjs', $config);
    }

    public function index($currentPage = NULL) {
        $output['title'] = 'SMS Template';

        if ($this->input->post('perform_task') != '') {

            $redirect_url = $_SERVER['HTTP_REFERER'];
            $task = $this->input->post('perform_task');
            $ids = $this->input->post('checkIds');
            if (sizeof($ids) > 0) {
                $message = $this->messages_model->performMultipleChecklist($task, $ids);
                if ($message) {
                    $this->session->set_userdata('SUCCESS_MESSAGE', $message);
                } else {
                    $message = "Oops something is going wrong there...try again later.";
                    $this->session->set_userdata('ERROR_MESSAGE', $message);
                }
            } else {
                $message = "Oops something is going wrong there...try again later.";
                $this->session->set_userdata('ERROR_MESSAGE', $message);
            }
            if ($redirect_url)
                redirect($redirect_url);
            else
                redirect(site_url('admin/messages/template_sms'));
        }
        $keyword = '';
        if ($this->input->post('search') != '') {
            $keyword = $this->input->post('search');
        }

        $this->load->library('pagination');

        $config['base_url'] = base_url() . $this->config->config['adminfolder'] . '/checklist/';
        $config['total_rows'] = $this->getChecklist('1', '0', '10', '', $keyword);
        $config['per_page'] = $this->config->item('page_size_admin');
        $this->pagination->initialize($config);
        $currentPage = (!$currentPage) ? 1 : $currentPage;
        $config['currentpage'] = $currentPage;
        if ($currentPage != "")
            $offset = ($currentPage - 1) * $config['per_page'];
        if ($config['total_rows'] > 0)
            $output['paging'] = $this->paging($config);
        $checklist = $this->getChecklist('0', $config['per_page'], $offset, '', $keyword);

        $output['records'] = $checklist;
        // $output['amenties'] = $this->getAmenties();
        /*         * ******** For Selected menu and sub-menu in left bar ************** */
        $output['pageName'] = $this->pageName;
        $output['subpageName'] = 'Tenant Checklist';

        /*         * ******** For Breadcrum ************** */
        $output['breadcrum1'] = $this->breadcrum1;
        $output['breadcrum2'] = $this->breadcrum2;
        $output['breadcrum3'] = '';

        $output['breadcrum2_url'] = $this->breadcrum2_url;
        $output['breadcrum3_url'] = $this->breadcrum3_url;
        //  echo '<pre>';print_r($output);die;
        $this->load->view($this->config->item('adminfolder') . '/header', $output);
        $this->load->view($this->config->item('adminfolder') . '/checklist');
        $this->load->view($this->config->item('adminfolder') . '/footer');
    }

    function getChecklist($cnt, $limit, $offset, $single = NULL, $keyword) {
        $conditions = $params = array();
        $params['table'] = 'tbl_checklist';
        if ($cnt == 1) {
            $params['cnt'] = true;
        } else if ($single != NULL) {
            $params['single_row'] = true;
            $conditions['id'] = $single;
        } else {
            $params['offset'] = $offset;
            $params['limit'] = $limit;
        }
        $info = $this->common_model->get_data($conditions, $params);
        return $info;
    }

    function deleteCheckList($task, $id) {
        $output['task'] = $task;
        if (!empty($_POST)) {
            $conditions = $params = array();
            $conditions['id'] = $id;
            $params['table'] = 'tbl_checklist';
            $this->common_model->delete($conditions, $params);
            $this->session->set_userdata('SUCCESS_MESSAGE', 'Selected record has been deleted successfully');
            $redirect_url = $_SERVER['HTTP_REFERER'];
            if ($redirect_url)
                redirect($redirect_url);
            else
                redirect(site_url('admin/checklist/'));
        }
        $this->load->view($this->config->item('adminfolder') . '/active_inactive_delete', $output);
    }

    public function add() {
        $output['title'] = 'Create Checklist Item';
        /*         * ******** For Selected menu and sub-menu in left bar ************** */
        $output['pageName'] = $this->pageName;
        $output['subpageName'] = 'Create Checklist Item';

        /*         * ******** For Breadcrum ************** */
        $output['breadcrum1'] = $this->breadcrum1;
        $output['breadcrum2'] = $this->breadcrum2;
        $output['breadcrum3'] = $this->breadcrum3;

        $output['breadcrum2_url'] = $this->breadcrum2_url;
        $output['breadcrum3_url'] = '';
        //  $output['amenties'] = $this->getAmenties();
        //  $output['tenants'] = $this->getUserByType('tenant');

        if (!empty($_POST)) {
            $failure = FALSE;
            $this->form_validation->set_rules('item', 'Checklist Item', 'trim|required');
            if ($this->form_validation->run()) {
                $conditions = $params = array();
                $conditions['content'] = $this->input->post('item');
                $conditions['status'] = '1';
                $conditions['date_created'] = date('Y-m-d H:i:s');
                $conditions['date_modified'] = date('Y-m-d H:i:s');
                $params['table'] = 'tbl_checklist';
                $id = $this->common_model->insert_data($conditions, $params);
                $success_message = 'Added successfully';
            } else {
                $error_message = validation_errors();
                $failure = true;
            }
            if ($this->input->is_ajax_request()) {
                if ($failure) {
                    $data['success'] = false;
                    $data['error_message'] = $error_message;
                } else {
                    $data['success'] = true;
                    $data['success_message'] = $success_message;
                    $data['url'] = site_url('admin/checklist');
                }
                $data['scrollToElement'] = true;
                echo json_encode($data);
                die;
            }
        }
        $this->load->view($this->config->item('adminfolder') . '/header', $output);
        $this->load->view($this->config->item('adminfolder') . '/add_checklist');
        $this->load->view($this->config->item('adminfolder') . '/footer');
    }

    function edit($id) {
        $output['title'] = 'Edit Checklist Item';
        /*         * ******** For Selected menu and sub-menu in left bar ************** */
        $output['pageName'] = $this->pageName;
        $output['subpageName'] = 'Edit Checklist Item';

        /*         * ******** For Breadcrum ************** */
        $output['breadcrum1'] = $this->breadcrum1;
        $output['breadcrum2'] = $this->breadcrum2;
        $output['breadcrum3'] = "Edit Checklist Item";

        $output['breadcrum2_url'] = $this->breadcrum2_url;
        $output['breadcrum3_url'] = '';

        if (!empty($_POST)) {
            $failure = FALSE;
             $this->form_validation->set_rules('item', 'Checklist Item', 'trim|required');
            if ($this->form_validation->run()) {
                $conditions = $params = array();
                $conditions['where']['id'] = $id;
                $conditions['value']['content'] = $this->input->post('item');
                $params['table'] = 'tbl_checklist';
                $this->common_model->update($conditions, $params);
                $success_message = 'Updated successfully';
            } else {
                $error_message = validation_errors();
                $failure = true;
            }
            if ($this->input->is_ajax_request()) {
                if ($failure) {
                    $data['success'] = false;
                    $data['error_message'] = $error_message;
                } else {
                    $data['success'] = true;
                    $data['success_message'] = $success_message;
                    $data['url'] = site_url('admin/checklist');
                }
                $data['scrollToElement'] = true;
                echo json_encode($data);
                die;
            }
        }

        $checklist = $this->getChecklist('0', '0', '10', $id, '');
        $output['records'] = $checklist;
       
        $this->load->view($this->config->item('adminfolder') . '/header', $output);
        $this->load->view($this->config->item('adminfolder') . '/add_checklist');
        $this->load->view($this->config->item('adminfolder') . '/footer');
    }

    function getUserByType($type) {
        $params = $conditions = array();
        $params['table'] = 'tbl_members';
        $conditions['user_type'] = $type;
        $conditions['status'] = 'Active';
        /* if($type=='landlord'){
          $conditions['verification_status'] = 'Yes';
          } */
        $params['fields'] = array('mem_id', 'first_name', 'last_name');
        $params['order_by'] = 'first_name';
        $list = $this->common_model->get_data($conditions, $params);
        return $list;
    }

    function doTask($task, $id) {
        $output['task'] = $task;
        if (!empty($_POST)) {
            if ($task == 'Active' || $task == 'Inactive') {
                $ststus = $task == 'Active' ? '1' : '0';
                $conditions = $params = array();
                $conditions['where']['id'] = $id;
                $conditions['value']['status'] = $ststus;
                $params['table'] = 'tbl_checklist';
                $this->common_model->update($conditions, $params);
            }
            $this->session->set_userdata('SUCCESS_MESSAGE', $message);
            $redirect_url = $_SERVER['HTTP_REFERER'];
            if ($redirect_url)
                redirect($redirect_url);
            else
                redirect(site_url('admin/checklist'));
        }
        $this->load->view($this->config->item('adminfolder') . '/active_inactive_delete', $output);
    }

#================= Paging ==========================================================#

    protected function paging($config) {
        $total_pages = ceil($config['total_rows'] / $config['per_page']);

        $total_pages = ceil($config['total_rows'] / $config['per_page']);
        $start = max($config['currentpage'] - intval($config['per_page'] / 2), 1);
        $end = $start + $config['per_page'] - 1;

        if ($config['currentpage'] == 1) {
            if ($config['total_rows'] > $config['per_page']) {
                $showing = '1-' . $config['per_page'];
            } else {
                $showing = '1-' . $config['total_rows'];
            }
        } else {
            $showing = ((($config['currentpage'] - 1) * $config['per_page']) + 1) . '-';
            if (($config['currentpage'] * $config['per_page']) > $config['total_rows']) {
                $showing.=$config['total_rows'];
            } else {
                $showing.=($config['currentpage'] * $config['per_page']);
            }
        }

        $output = '<ul class="pagination">';

        if ($config['currentpage'] > 1) {
            $output.='<li class=""><a href="' . $config['base_url'] . ($config['currentpage'] - 1) . '"> <i class="fa fa-angle-double-left"></i> </a></li>';
        } else {
            $output.='<li class="prev disabled "><a> <i class="fa fa-angle-double-left"></i> </a></li>';
        }

        for ($i = $start; $i <= $end && $i <= $total_pages; $i++) {
            if ($i == $config['currentpage']) {
                $output .= '<li class="active"><a>' . $i . '</a></li>';
            } else {
                $output .='<li><a href="' . $config['base_url'] . $i . '">' . $i . '</a></li>';
            }
        }
        $output .='</li>';

        if ($total_pages > $config['currentpage']) {
            $output.='<li class="next"><a href="' . $config['base_url'] . ($config['currentpage'] + 1) . '"> <i class="fa fa-angle-double-right"></i></a></li>';
        } else {
            $output.='<li class="next disabled"><a > <i class="fa fa-angle-double-right"></i> </a></li>';
        }
        $output.='</ul>';
        return $output;
    }

}
