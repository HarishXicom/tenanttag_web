<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class More_Notifications extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('notification_model');
        $this->adm_id = $this->session->userdata('ADM_ID');
    }

    public function index() {
        $output['title'] = 'Home';
        $output['page_name'] = 'home';
        $notifications = $this->notification_model->getAllAdminNotificationsById($this->adm_id, 'Unread');
        $output['notifications'] = $notifications;
        $this->load->view($this->config->config['adminfolder'] . '/notification', $output);
    }

    function getAllNotifications() {
        $output['title'] = 'Notifications';
        $output['drop_down'] = 'notifications';
        $memDetail = $this->member_model->getMemberInfo($this->mem_id);
        $notifications = $this->notification_model->getAllNotificationsById($this->mem_id);
        //echo"<pre>";print_r($notifications);die;
        foreach ($notifications as $value) {
            $record = '';
            if ($value->task_type == 'album' && $value->gallery_delete_status == 'No') {
                //echo $value->gallery_delete_status;die;
                $aRecord = $this->album_model->getRecordsBySlug($value->slug, 'Active');
                $aRecord->title = $aRecord->album_title;
                $image = $this->image_model->getAlbumCoverImageById($value->task_id);
                $aRecord->image = $image->image;
                $value->record = $aRecord;
            }
            if ($value->task_type == 'image' && $value->gallery_delete_status == 'No') {
                $iRecord = $this->image_model->getRecordsBySlug($value->slug, 'Active');
                $iRecord->title = $iRecord->title;
                $iRecord->image = $iRecord->image;
                $value->record = $iRecord;
            }
        }
        //echo"<pre>";print_r($notifications);die;
        $output['memDetail'] = $memDetail;
        $output['allNotifications'] = $notifications;

        for ($i = 0; isset($notifications[$i]); $i++) {
            $this->notification_model->changeNotificationReadStatus($this->mem_id, $notifications[$i]->notification_by, $notifications[$i]->task_id);
        }

        $this->load->view($this->config->item('template') . '/header', $output);
        $this->load->view($this->config->item('template') . '/all_notifications');
        $this->load->view($this->config->item('template') . '/footer');
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
