<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pages extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->pageName = 'Contents';
        $this->breadcrum1 = 'Contents';
        $this->breadcrum2 = 'Static Pages';
        $this->breadcrum3 = 'Add Static Page';
        $this->breadcrum4 = 'Edit Static Page';
        $this->breadcrum2_url = site_url('admin/pages');
        $this->breadcrum3_url = site_url('admin/pages/add');
        $this->breadcrum4_url = site_url('admin/pages/edit');
        $this->load->model('pages_model');
        $this->common_model->checkAdminLogin();
        $this->js();
    }

    private function js() {
        $config[] = 'ckeditor/ckeditor.js';
        $this->config->set_item('adminjs', $config);
    }

    public function index($currentPage = NULL) {
        $output['title'] = 'Static Pages';

        if (!empty($_POST)) {
            $redirect_url = $_SERVER['HTTP_REFERER'];
            $task = $this->input->post('perform_task');
            $ids = $this->input->post('checkIds');
            if (sizeof($ids) > 0) {
                $message = $this->pages_model->performMultipleTasks($task, $ids);
                if ($message) {
                    $this->session->set_userdata('SUCCESS_MESSAGE', $message);
                } else {
                    $message = "Oops something is going wrong there...try again later.";
                    $this->session->set_userdata('ERROR_MESSAGE', $message);
                }
            } else {
                $message = "Oops something is going wrong there...try again later.";
                $this->session->set_userdata('ERROR_MESSAGE', $message);
            }
            if ($redirect_url)
                redirect($redirect_url);
            else
                redirect(site_url('admin/pages'));
        }

        $this->load->library('pagination');

        $config['base_url'] = base_url() . $this->config->config['adminfolder'] . '/pages/';
        $config['total_rows'] = $this->pages_model->getCountAllRecordsForAdmin('');
        $config['per_page'] = $this->config->item('page_size_admin');
        $this->pagination->initialize($config);
        $currentPage = (!$currentPage) ? 1 : $currentPage;
        $config['currentpage'] = $currentPage;
        if ($currentPage != "")
            $offset = ($currentPage - 1) * $config['per_page'];
        if ($config['total_rows'] > 0)
            $output['paging'] = $this->paging($config);


        $output['total'] = $this->pages_model->getCountAllRecordsForAdmin('');
        $output['totalActive'] = $this->pages_model->getCountAllRecordsForAdmin('Active');
        $output['totalInactive'] = $this->pages_model->getCountAllRecordsForAdmin('Inactive');
        $output['records'] = $this->pages_model->getAllRecordsForAdmin($config['per_page'], $offset);


        /*         * ******** For Selected menu and sub-menu in left bar ************** */
        $output['pageName'] = $this->pageName;
        $output['subpageName'] = 'Static Pages';

        /*         * ******** For Breadcrum ************** */
        $output['breadcrum1'] = $this->breadcrum1;
        $output['breadcrum2'] = $this->breadcrum2;
        $output['breadcrum3'] = '';

        $output['breadcrum2_url'] = $this->breadcrum2_url;
        $output['breadcrum3_url'] = $this->breadcrum3_url;

        $this->load->view($this->config->item('adminfolder') . '/header', $output);
        $this->load->view($this->config->item('adminfolder') . '/pages_list');
        $this->load->view($this->config->item('adminfolder') . '/footer');
    }

    function add() {
        $output['title'] = 'Add Static Page';
        if (!empty($_POST)) {
            $failure = FALSE;
            $this->form_validation->set_rules('name', 'Page Name', 'trim|required');
            $this->form_validation->set_rules('title', 'Title', 'trim|required');
            $this->form_validation->set_rules('description', 'Description', 'trim|required');
            $this->form_validation->set_rules('meta_title', 'Page Meta title', 'trim|required');
            $this->form_validation->set_rules('meta_keywords', 'Meta Keywords', 'trim|required');
            $this->form_validation->set_rules('meta_description', 'Description', 'trim|required');
            if ($this->form_validation->run()) {

                $this->pages_model->add();
                $success_message = 'Static Page added successfully';
            } else {

                $error_message = validation_errors();
                $failure = true;
            }
            if ($this->input->is_ajax_request()) {
                if ($failure) {
                    $data['success'] = false;
                    $data['error_message'] = $error_message;
                } else {
                    $data['success'] = true;
                    $data['success_message'] = $success_message;
                    $data['url'] = site_url('admin/pages');
                }
                $data['scrollToElement'] = true;
                echo json_encode($data);
                die;
            }
        }
        /*         * ******** For Selected menu and sub-menu in left bar ************** */
        $output['pageName'] = $this->pageName;
        $output['subpageName'] = 'Add Static Page';

        /*         * ******** For Breadcrum ************** */
        $output['breadcrum1'] = $this->breadcrum1;
        $output['breadcrum2'] = $this->breadcrum2;
        $output['breadcrum3'] = $this->breadcrum3;

        $output['breadcrum2_url'] = $this->breadcrum2_url;
        $output['breadcrum3_url'] = '';

        $this->load->view($this->config->item('adminfolder') . '/header', $output);
        $this->load->view($this->config->item('adminfolder') . '/add_static_page');
        $this->load->view($this->config->item('adminfolder') . '/footer');
    }

    function edit($id) {
        $output['title'] = 'Edit Static Page';
        if (!empty($_POST)) {
            $failure = FALSE;
            $this->form_validation->set_rules('name', 'Page Name', 'trim|required');
            $this->form_validation->set_rules('title', 'Title', 'trim|required');
            $this->form_validation->set_rules('description', 'Description', 'trim|required');
            $this->form_validation->set_rules('meta_title', 'Page Meta title', 'trim|required');
            $this->form_validation->set_rules('meta_keywords', 'Meta Keywords', 'trim|required');
            $this->form_validation->set_rules('meta_description', 'Description', 'trim|required');
            if ($this->form_validation->run()) {

                $this->pages_model->update($id);
                $success_message = 'Static Page record updated successfully';
            } else {

                $error_message = validation_errors();
                $failure = true;
            }
            if ($this->input->is_ajax_request()) {
                if ($failure) {
                    $data['success'] = false;
                    $data['error_message'] = $error_message;
                } else {
                    $data['success'] = true;
                    $data['success_message'] = $success_message;
                    $data['url'] = site_url('admin/pages');
                }
                $data['scrollToElement'] = true;
                echo json_encode($data);
                die;
            }
        }
        /*         * ******** For Selected menu and sub-menu in left bar ************** */
        $output['pageName'] = $this->pageName;
        $output['subpageName'] = 'Edit Static Page';

        /*         * ******** For Breadcrum ************** */
        $output['breadcrum1'] = $this->breadcrum1;
        $output['breadcrum2'] = $this->breadcrum2;
        $output['breadcrum3'] = '';
        $output['breadcrum4'] = $this->breadcrum4;

        $output['breadcrum2_url'] = $this->breadcrum2_url;
        $output['breadcrum3_url'] = $this->breadcrum3_url;
        $output['breadcrum4_url'] = '';

        $output['recordDetail'] = $recordDetail = $this->pages_model->getRecordById($id);

        $output['action'] = 'edit';

        $this->load->view($this->config->item('adminfolder') . '/header', $output);
        $this->load->view($this->config->item('adminfolder') . '/add_static_page');
        $this->load->view($this->config->item('adminfolder') . '/footer');
    }

    function view($id) {
        $output['records'] = $this->pages_model->getRecordById($id);
        $this->load->view($this->config->item('adminfolder') . '/static_page_view', $output);
    }

    function filter($filterKey = NULL, $show_me = NULL, $sort_by = NULL, $currentPage = NULL) {
        $output['title'] = 'Static Pages';

        $filterKey = urldecode($filterKey);
        if ($filterKey != 'NULL')
            $output['filterKey'] = $filterKey;
        if ($show_me != 'NULL')
            $output['show_me'] = $show_me;
        if ($sort_by != 'NULL')
            $output['sort_by'] = $sort_by;


        if ($show_me != 'NULL' || $sort_by != 'NULL')
            $output['advanceSearch'] = 'Yes';

        if (!empty($_POST)) {
            $redirect_url = $_SERVER['HTTP_REFERER'];
            $task = $this->input->post('perform_task');
            $ids = $this->input->post('checkIds');
            if (sizeof($ids) > 0) {
                $message = $this->pages_model->performMultipleTasks($task, $ids);
                if ($message) {
                    $this->session->set_userdata('SUCCESS_MESSAGE', $message);
                } else {
                    $message = "Oops something is going wrong there...try again later.";
                    $this->session->set_userdata('ERROR_MESSAGE', $message);
                }
            } else {
                $message = "Oops something is going wrong there...try again later.";
                $this->session->set_userdata('ERROR_MESSAGE', $message);
            }
            if ($redirect_url)
                redirect($redirect_url);
            else
                redirect(site_url('admin/pages'));
        }

        $filterKey = $filterKey;
        if ($filterKey != 'NULL')
            $output['filterKey'] = $filterKey;

        $this->load->library('pagination');
        $config['base_url'] = base_url() . $this->config->config['adminfolder'] . '/pages/filter/' . $filterKey . '/' . $show_me . '/' . $sort_by . '/';
        $config['total_rows'] = $this->pages_model->getCountFilterAllRecords($filterKey, $show_me, $sort_by);
        $config['per_page'] = $this->config->item('page_size_admin');
        $this->pagination->initialize($config);
        $currentPage = (!$currentPage) ? 1 : $currentPage;
        $config['currentpage'] = $currentPage;
        if ($currentPage != "")
            $offset = ($currentPage - 1) * $config['per_page'];
        if ($config['total_rows'] > 0)
            $output['paging'] = $this->paging($config);

        $output['total'] = $this->pages_model->getCountAllRecordsForAdmin('');
        $output['totalActive'] = $this->pages_model->getCountAllRecordsForAdmin('Active');
        $output['totalInactive'] = $this->pages_model->getCountAllRecordsForAdmin('Inactive');
        $output['records'] = $this->pages_model->getFilterAllRecords($filterKey, $show_me, $sort_by, $config['per_page'], $offset);

        /*         * ******** For Selected menu and sub-menu in left bar ************** */
        $output['pageName'] = $this->pageName;
        $output['subpageName'] = 'Static Pages';

        /*         * ******** For Breadcrum ************** */
        $output['breadcrum1'] = $this->breadcrum1;
        $output['breadcrum2'] = $this->breadcrum2;
        $output['breadcrum3'] = '';

        $output['breadcrum2_url'] = $this->breadcrum2_url;
        $output['breadcrum3_url'] = $this->breadcrum3_url;

        $this->load->view($this->config->item('adminfolder') . '/header', $output);
        $this->load->view($this->config->item('adminfolder') . '/pages_list');
        $this->load->view($this->config->item('adminfolder') . '/footer');
    }

    function doTask($task, $id) {
        $output['task'] = $task;
        if (!empty($_POST)) {
            $message = $this->pages_model->performTask($task, $id);
            $this->session->set_userdata('SUCCESS_MESSAGE', $message);
            $redirect_url = $_SERVER['HTTP_REFERER'];
            if ($redirect_url)
                redirect($redirect_url);
            else
                redirect(site_url('admin/pages'));
        }
        $this->load->view($this->config->item('adminfolder') . '/active_inactive_delete', $output);
    }

    #================= Paging ==========================================================#

    protected function paging($config) {
        $total_pages = ceil($config['total_rows'] / $config['per_page']);

        $total_pages = ceil($config['total_rows'] / $config['per_page']);
        $start = max($config['currentpage'] - intval($config['per_page'] / 2), 1);
        $end = $start + $config['per_page'] - 1;

        if ($config['currentpage'] == 1) {
            if ($config['total_rows'] > $config['per_page']) {
                $showing = '1-' . $config['per_page'];
            } else {
                $showing = '1-' . $config['total_rows'];
            }
        } else {
            $showing = ((($config['currentpage'] - 1) * $config['per_page']) + 1) . '-';
            if (($config['currentpage'] * $config['per_page']) > $config['total_rows']) {
                $showing.=$config['total_rows'];
            } else {
                $showing.=($config['currentpage'] * $config['per_page']);
            }
        }

        $output = '<ul class="pagination">';

        if ($config['currentpage'] > 1) {
            $output.='<li class=""><a href="' . $config['base_url'] . ($config['currentpage'] - 1) . '"> <i class="fa fa-angle-double-left"></i> </a></li>';
        } else {
            $output.='<li class="prev disabled "><a> <i class="fa fa-angle-double-left"></i> </a></li>';
        }

        for ($i = $start; $i <= $end && $i <= $total_pages; $i++) {
            if ($i == $config['currentpage']) {
                $output .= '<li class="active"><a>' . $i . '</a></li>';
            } else {
                $output .='<li><a href="' . $config['base_url'] . $i . '">' . $i . '</a></li>';
            }
        }
        $output .='</li>';

        if ($total_pages > $config['currentpage']) {
            $output.='<li class="next"><a href="' . $config['base_url'] . ($config['currentpage'] + 1) . '"> <i class="fa fa-angle-double-right"></i></a></li>';
        } else {
            $output.='<li class="next disabled"><a > <i class="fa fa-angle-double-right"></i> </a></li>';
        }
        $output.='</ul>';
        return $output;
    }

}
