<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Landlords extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->adm_id = $this->session->userdata('ADM_ID');
        $this->pageName = 'Member Manager';
        $this->type = 'landlord';
        $this->breadcrum1 = 'Member Manager';
        $this->breadcrum2 = 'All Landlords';
        $this->breadcrum3 = 'Add Landlord';
        $this->breadcrum4 = 'Edit Landlord';
        $this->breadcrum2_url = site_url('admin/landlords');
        $this->breadcrum3_url = site_url('admin/landlords/add');
        $this->breadcrum4_url = site_url('admin/landlords/edit');
        $this->load->model('member_model');
        $this->load->model('properties_model');
        $this->load->model('city_model');
        $this->load->model('region_model');
        $this->load->model('country_model');
        $this->common_model->checkAdminLogin();
    }

    public function index($currentPage = NULL) {
        $output['title'] = 'Landlords';

        if (!empty($_POST)) {
            $redirect_url = $_SERVER['HTTP_REFERER'];
            $task = $this->input->post('perform_task');
            $ids = $this->input->post('checkIds');
            if (sizeof($ids) > 0) {
                $message = $this->member_model->performMultipleTasks($task, $ids);
                if ($message) {
                    $this->session->set_userdata('SUCCESS_MESSAGE', $message);
                } else {
                    $message = "Oops something is going wrong there...try again later.";
                    $this->session->set_userdata('ERROR_MESSAGE', $message);
                }
            } else {
                $message = "Oops something is going wrong there...try again later.";
                $this->session->set_userdata('ERROR_MESSAGE', $message);
            }
            if ($redirect_url)
                redirect($redirect_url);
            else
                redirect(site_url('admin/landlords'));
        }
        $this->load->library('pagination');

        $config['base_url'] = base_url() . $this->config->config['adminfolder'] . '/landlords/';
        $config['total_rows'] = $this->member_model->getCountAllRecordsForAdminByType($this->type, '');
        $config['per_page'] = 10; //$this->config->item('page_size_admin');
        $this->pagination->initialize($config);
        $currentPage = (!$currentPage) ? 1 : $currentPage;
        $config['currentpage'] = $currentPage;
        if ($currentPage != "")
            $offset = ($currentPage - 1) * $config['per_page'];
        if ($config['total_rows'] > 0)
            $output['paging'] = $this->paging($config);


        $output['total'] = $this->member_model->getCountAllRecordsForAdminByType($this->type, '');
        $output['totalActive'] = $this->member_model->getCountAllRecordsForAdminByType($this->type, 'Active');
        $output['totalInactive'] = $this->member_model->getCountAllRecordsForAdminByType($this->type, 'Inactive');
        $output['totalDeleted'] = $this->member_model->getCountAllRecordsForAdminByType($this->type, 'Deleted');
        $output['records'] = $this->member_model->getAllRecordsForAdminByType($this->type, '', $config['per_page'], $offset);

        //$output['allMembers']		=	$this->member_model->getAllMembersByType($this->type);

        /*         * ******** For Selected menu and sub-menu in left bar ************** */
        $output['pageName'] = $this->pageName;
        $output['subpageName'] = 'All Landlords';

        /*         * ******** For Breadcrum ************** */
        $output['breadcrum1'] = $this->breadcrum1;
        $output['breadcrum2'] = $this->breadcrum2;
        $output['breadcrum3'] = '';

        $output['breadcrum2_url'] = $this->breadcrum2_url;
        $output['breadcrum3_url'] = $this->breadcrum3_url;

        $this->load->view($this->config->item('adminfolder') . '/header', $output);
        $this->load->view($this->config->item('adminfolder') . '/landlords_list');
        $this->load->view($this->config->item('adminfolder') . '/footer');
    }

    function add() {
        $this->load->model('mailsending_model');
        $this->load->model('notification_model');
        $output['title'] = 'Add Landlord';
        $error_message = '';
        if (!empty($_POST)) {
            $failure = FALSE;
            $this->form_validation->set_rules('first_name', 'First Name', 'trim|required');
            $this->form_validation->set_rules('last_name', 'Last Name', 'trim|required');
            //$this->form_validation->set_rules('company_name', 'Company Name', 'trim|required');
            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[tbl_members.email]');
            $this->form_validation->set_rules('password', 'Password', 'trim|required');
            $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'trim|required|matches[password]');
            $this->form_validation->set_rules('mobile', 'Mobile No', 'trim|required');
            //   $this->form_validation->set_rules('dob', 'Date Of Birth', 'trim|required');
            $this->form_validation->set_rules('address', 'Address', 'trim|required');
            //  $this->form_validation->set_rules('country', 'Country', 'trim|required');
            $this->form_validation->set_rules('state', 'State', 'trim|required');
            //$this->form_validation->set_rules('city', 'City', 'trim|required');
            //~ if (empty($_FILES['profile_pic']['name']))
            //~ {
            //~ $this->form_validation->set_rules('profile_pic', 'Profile Image', 'required');
            //~ }
            if ($this->form_validation->run()) {
                $config['upload_path'] = './assets/uploads/member/';
                $config['allowed_types'] = 'jpeg|jpg|png';
                $config['encrypt_name'] = TRUE;
                if (!empty($_FILES['profile_pic']['name'])) {
                    $this->load->library('upload', $config);
                    //$this->upload->initialize($config);
                    if (!$this->upload->do_upload('profile_pic')) {
                        $error = $this->upload->display_errors();
                        $error_message .=$error;
                        $failure = TRUE;
                    } else {
                        $data = array('upload_data' => $this->upload->data());
                    }
                }
                if (!$failure) {
                    $data_im = (isset($data['upload_data']['file_name']) && !empty($data['upload_data']['file_name'])) ? $data['upload_data']['file_name'] : '';
                    $mem_id = $this->member_model->add($data_im);
                    if ($mem_id) {
                        $this->mailsending_model->registrationEmail($mem_id);
                        $this->notification_model->addNotification('A new landlord is registered on your site', $this->adm_id, $this->adm_id);
                    }
                    $success_message = 'Landlord added successfully';
                }
            } else {

                $error_message = validation_errors();
                $failure = true;
            }
            if ($this->input->is_ajax_request()) {
                if ($failure) {
                    $data['success'] = false;
                    $data['error_message'] = $error_message;
                } else {
                    $data['success'] = true;
                    $data['success_message'] = $success_message;
                    $data['url'] = site_url('admin/landlords');
                }
                $data['scrollToElement'] = true;
                echo json_encode($data);
                die;
            }
        }
        /*         * ******** For Selected menu and sub-menu in left bar ************** */
        $output['pageName'] = $this->pageName;
        $output['subpageName'] = 'Add Landlord';

        /*         * ******** For Breadcrum ************** */
        $output['breadcrum1'] = $this->breadcrum1;
        $output['breadcrum2'] = $this->breadcrum2;
        $output['breadcrum3'] = $this->breadcrum3;

        $output['breadcrum2_url'] = $this->breadcrum2_url;
        $output['breadcrum3_url'] = '';

        $output['allCity'] = $this->city_model->getAllCities();
        $output['allRegion'] = $this->region_model->getAllRegions();
        $output['allcountry'] = $this->country_model->getAllCountries();

        $this->load->view($this->config->item('adminfolder') . '/header', $output);
        $this->load->view($this->config->item('adminfolder') . '/add_landlord');
        $this->load->view($this->config->item('adminfolder') . '/footer');
    }

    function edit($id) {
        $output['title'] = 'Edit Landlord';
        if (!empty($_POST)) {
            $failure = FALSE;
            $this->form_validation->set_rules('first_name', 'First Name', 'trim|required');
            $this->form_validation->set_rules('last_name', 'Last Name', 'trim|required');
            //$this->form_validation->set_rules('company_name', 'Company Name', 'trim|required');
            $this->form_validation->set_rules('mobile', 'Mobile No', 'trim|required');
            // $this->form_validation->set_rules('dob', 'Date Of Birth', 'trim|required');
            $this->form_validation->set_rules('mailing_address1', 'Address', 'trim|required');
            //  $this->form_validation->set_rules('country', 'Country', 'trim|required');
            $this->form_validation->set_rules('mailing_state_id', 'State', 'trim|required');
            //$this->form_validation->set_rules('mailing_unit', 'Unit', 'trim|required');
            //$this->form_validation->set_rules('city', 'City', 'trim|required');
            if ($this->form_validation->run()) {
                $config['upload_path'] = './assets/uploads/member/';
                $config['allowed_types'] = 'jpeg|jpg|png';
                $config['encrypt_name'] = TRUE;
                if (!empty($_FILES['profile_pic']['name'])) {
                    //$this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    if (!$this->upload->do_upload('profile_pic')) {
                        $error = $this->upload->display_errors();
                        $error_message.=$error;
                        $failure = TRUE;
                    } else {
                        $data = array('upload_data' => $this->upload->data());
                    }
                }
                if (!$failure) {
                    $file = '';
                    if (isset($data['upload_data']['file_name'])) {
                        $file = $data['upload_data']['file_name'];
                    }
                    $this->member_model->update($id, $file);
                    $success_message = 'Landlord record updated successfully';
                }
            } else {

                $error_message = validation_errors();
                $failure = true;
            }
            if ($this->input->is_ajax_request()) {
                if ($failure) {
                    $data['success'] = false;
                    $data['error_message'] = $error_message;
                } else {
                    $data['success'] = true;
                    $data['success_message'] = $success_message;
                    $data['url'] = site_url('admin/landlords');
                }
                $data['scrollToElement'] = true;
                echo json_encode($data);
                die;
            }
        }
        /*         * ******** For Selected menu and sub-menu in left bar ************** */
        $output['pageName'] = $this->pageName;
        $output['subpageName'] = 'Edit Landlord';

        /*         * ******** For Breadcrum ************** */
        $output['breadcrum1'] = $this->breadcrum1;
        $output['breadcrum2'] = $this->breadcrum2;
        $output['breadcrum3'] = '';
        $output['breadcrum4'] = $this->breadcrum4;

        $output['breadcrum2_url'] = $this->breadcrum2_url;
        $output['breadcrum3_url'] = $this->breadcrum3_url;
        $output['breadcrum4_url'] = '';

        $output['memberDetail'] = $memberDetail = $this->member_model->getMemberRecordByIdAndType($id, $this->type);

        $output['allCity'] = $this->city_model->getAllCityByStateId($memberDetail->state_id);
        $output['allRegion'] = $this->region_model->getAllRegionsByCountryId($memberDetail->country_id);
        $output['allcountry'] = $this->country_model->getAllCountries();

        $output['action'] = 'edit';

        $this->load->view($this->config->item('adminfolder') . '/header', $output);
        $this->load->view($this->config->item('adminfolder') . '/add_landlord');
        $this->load->view($this->config->item('adminfolder') . '/footer');
    }

    function view($memId) {
        $output['records'] = $this->member_model->getMemberRecordByIdAndType($memId, $this->type);
        $this->load->view($this->config->item('adminfolder') . '/landlord_view', $output);
    }

    function filter($filterKey = NULL, $show_me = NULL, $sort_by = NULL, $currentPage = NULL) {
        $output['title'] = 'Landlords';

        $filterKey = urldecode($filterKey);
        if ($filterKey != 'NULL')
            $output['filterKey'] = $filterKey;
        if ($show_me != 'NULL')
            $output['show_me'] = $show_me;
        if ($sort_by != 'NULL')
            $output['sort_by'] = $sort_by;


        if ($show_me != 'NULL' || $sort_by != 'NULL')
            $output['advanceSearch'] = 'Yes';

        if (!empty($_POST)) {
            $redirect_url = $_SERVER['HTTP_REFERER'];
            $task = $this->input->post('perform_task');
            $ids = $this->input->post('checkIds');
            if (sizeof($ids) > 0) {
                $message = $this->member_model->performMultipleTasks($task, $ids);
                if ($message) {
                    $this->session->set_userdata('SUCCESS_MESSAGE', $message);
                } else {
                    $message = "Oops something is going wrong there...try again later.";
                    $this->session->set_userdata('ERROR_MESSAGE', $message);
                }
            } else {
                $message = "Oops something is going wrong there...try again later.";
                $this->session->set_userdata('ERROR_MESSAGE', $message);
            }
            if ($redirect_url)
                redirect($redirect_url);
            else
                redirect(site_url('admin/landlords'));
        }

        $this->load->library('pagination');

        $config['base_url'] = base_url() . $this->config->config['adminfolder'] . '/landlords/filter/' . $filterKey . '/' . $show_me . '/' . $sort_by . '/';
        $config['total_rows'] = $this->member_model->getCountFilterAllRecords($this->type, $filterKey, $show_me, $sort_by);
        $config['per_page'] = 10; //$this->config->item('page_size_admin');
        $this->pagination->initialize($config);
        $currentPage = (!$currentPage) ? 1 : $currentPage;
        $config['currentpage'] = $currentPage;
        if ($currentPage != "")
            $offset = ($currentPage - 1) * $config['per_page'];
        if ($config['total_rows'] > 0)
            $output['paging'] = $this->paging($config);

        $output['total'] = $this->member_model->getCountAllRecordsForAdminByType($this->type, '');
        $output['totalActive'] = $this->member_model->getCountAllRecordsForAdminByType($this->type, 'Active');
        $output['totalInactive'] = $this->member_model->getCountAllRecordsForAdminByType($this->type, 'Inactive');
        $output['records'] = $this->member_model->getFilterAllRecords($this->type, $filterKey, $show_me, $sort_by, $config['per_page'], $offset);
        $output['totalDeleted'] = $this->member_model->getCountAllRecordsForAdminByType($this->type, 'Deleted');

        /*         * ******** For Selected menu and sub-menu in left bar ************** */
        $output['pageName'] = $this->pageName;
        $output['subpageName'] = 'All Landlords';

        /*         * ******** For Breadcrum ************** */
        $output['breadcrum1'] = $this->breadcrum1;
        $output['breadcrum2'] = $this->breadcrum2;
        $output['breadcrum3'] = '';

        $output['breadcrum2_url'] = $this->breadcrum2_url;
        $output['breadcrum3_url'] = $this->breadcrum3_url;

        $this->load->view($this->config->item('adminfolder') . '/header', $output);
        $this->load->view($this->config->item('adminfolder') . '/landlords_list');
        $this->load->view($this->config->item('adminfolder') . '/footer');
    }

    function doTask($task, $id) {
        $output['task'] = $task;
        if (!empty($_POST)) {
            $propertyRecord = $this->properties_model->getAllPropertiesByLandlordId($id);
            if (sizeof($propertyRecord) > 0 && $task == 'delete') {
                $message = 'You can not delete this landlord because it has properties listed on site.To delete this landlord you have to delete his/her properties first.';
                $this->session->set_userdata('ERROR_MESSAGE', $message);
            } else {
                $message = $this->member_model->performTask($task, $id);
                $this->session->set_userdata('SUCCESS_MESSAGE', $message);
            }
            $redirect_url = $_SERVER['HTTP_REFERER'];
            if ($redirect_url)
                redirect($redirect_url);
            else
                redirect(site_url('admin/landlords'));
        }
        $this->load->view($this->config->item('adminfolder') . '/active_inactive_delete', $output);
    }

    /*     * ************* Get properties by landlord ID ************************* */

    function getPropertiesByLandlordId($id) {
        $allProperties = $this->properties_model->getAllPropertiesByLandlordId($id);
        if ($allProperties) {
            $data['properties'] = $allProperties;
            $data['success'] = true;
        } else {
            $data['success'] = false;
        }
        echo json_encode($data);
        die;
    }

    /*     * ************* End Get properties by landlord ID ********************* */

    #================= Paging ==========================================================#

    protected function paging($config) {
        $total_pages = ceil($config['total_rows'] / $config['per_page']);

        $total_pages = ceil($config['total_rows'] / $config['per_page']);
        $start = max($config['currentpage'] - intval($config['per_page'] / 2), 1);
        $end = $start + $config['per_page'] - 1;

        if ($config['currentpage'] == 1) {
            if ($config['total_rows'] > $config['per_page']) {
                $showing = '1-' . $config['per_page'];
            } else {
                $showing = '1-' . $config['total_rows'];
            }
        } else {
            $showing = ((($config['currentpage'] - 1) * $config['per_page']) + 1) . '-';
            if (($config['currentpage'] * $config['per_page']) > $config['total_rows']) {
                $showing.=$config['total_rows'];
            } else {
                $showing.=($config['currentpage'] * $config['per_page']);
            }
        }

        $output = '<ul class="pagination">';

        if ($config['currentpage'] > 1) {
            $output.='<li class=""><a href="' . $config['base_url'] . ($config['currentpage'] - 1) . '"> <i class="fa fa-angle-double-left"></i> </a></li>';
        } else {
            $output.='<li class="prev disabled "><a> <i class="fa fa-angle-double-left"></i> </a></li>';
        }

        for ($i = $start; $i <= $end && $i <= $total_pages; $i++) {
            if ($i == $config['currentpage']) {
                $output .= '<li class="active"><a>' . $i . '</a></li>';
            } else {
                $output .='<li><a href="' . $config['base_url'] . $i . '">' . $i . '</a></li>';
            }
        }
        $output .='</li>';

        if ($total_pages > $config['currentpage']) {
            $output.='<li class="next"><a href="' . $config['base_url'] . ($config['currentpage'] + 1) . '"> <i class="fa fa-angle-double-right"></i></a></li>';
        } else {
            $output.='<li class="next disabled"><a > <i class="fa fa-angle-double-right"></i> </a></li>';
        }
        $output.='</ul>';
        return $output;
    }

    function details($memId) {
        $output['selected_tab'] = 'tenants';
        $output['title'] = 'Tenants';
        /*         * **** function to get detail of login landlord **** */
        $memDetail = $this->member_model->getMemberInfo('mem_id', $memId);
        $output['records'] = $memDetail;
        /*         * **** function to get all properties of login landlord **** */
        $allProperties = $this->properties_model->frontGetAllPropertiesByLandlordId($memId);
        foreach ($allProperties as $value) {
            $value->tenants = $this->properties_model->getAllTenantsByProperty($value->prop_id);
        }
        $output['allProperties'] = $allProperties;
        /*$this->load->view($this->config->item('adminfolder') . '/landlord_details', $output);*/
         $this->load->view($this->config->item('adminfolder') . '/header', $output);
          $this->load->view($this->config->item('adminfolder') . '/landlord_details');
          $this->load->view($this->config->item('adminfolder') . '/footer'); 
    }

    public function sent_messages($memId) {
        $output['sent_msgs'] = $this->getSMSbyType('tbl_sms_logs', $memId);
        //echo '<pre>';print_r($output['sent_msgs']);die;
        $this->load->view($this->config->item('adminfolder') . '/landlord_sms', $output);
    }

    public function getSMSbyType($type, $memId) {

        $params = $conditions = array();
        $params['complex'] = true;
        $conditions['tables'] = array(
            'sms' => $type,
            'tenant' => 'tbl_members',
            'property' => 'tbl_properties',
        );
        $conditions['table'] = 'sms';
        $conditions['on']['tenant'] = array(
            'sign' => '=',
            'column' => 'mem_id',
            'alias_column' => 'tenant_id',
            'alias_other' => 'sms',
        );
        $conditions['on']['property'] = array(
            'sign' => '=',
            'column' => 'prop_id',
            'alias_column' => 'property_id',
            'alias_other' => 'sms',
        );
        $conditions['where']['owner_id']['alias'] = 'sms';
        $conditions['where']['owner_id']['value'] = $memId;
        $params['fields'] = array(
            'property.prop_id', 'sms.*',
            'property.address1', 'property.address2', 'property.city', 'tenant.first_name', 'tenant.last_name');
        $params['order_by'] = 'sms.id';
        $sms_data = $this->common_model->get_data($conditions, $params);

        return $sms_data;
    }

    public function exportLandlords() {
        $this->load->dbutil();
        $this->load->helper('file');
        $this->load->helper('download');
        $data = $this->properties_model->getLandlordCSV();
        //print_r($data); exit;
        $new_report = $this->dbutil->csv_from_result($data);
        $file = "Landlords_" . date('Y-m-d H:i:s') . ".csv";
        force_download($file, $new_report);
    }

}
