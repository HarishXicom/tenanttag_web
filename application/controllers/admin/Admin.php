<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('admin_model');
        $this->load->library('user_agent');
    }

    public function index() {

        $output['title'] = 'Dashboard';
        /*         * ********Start Loading model as per function requirement ************* */
        $this->load->model('member_model');
        $this->load->model('services_model');
        $this->load->model('amenties_model');
        $this->load->model('enquiries_model');
        $this->load->model('sent_mail_history_model');
        /*         * ********End Loading model as per function requirement ************* */
        if (!$this->session->userdata('ADM_ID'))
            redirect('admin/login');
        $output['adminDetail'] = $this->admin_model->getRecordByIdOrEmail($this->session->userdata('ADM_ID'));
        /*         * ********Start Fetching data for dashboard ************* */
        $output['allServices'] = $this->services_model->getCountAllRecordsForAdmin();
        $output['allAmenties'] = $this->amenties_model->getCountAllRecordsForAdmin();
        $output['allEnquiries'] = $this->enquiries_model->getCountAllRecordsForAdmin();
        $output['allSentMails'] = $this->sent_mail_history_model->getCountAllRecordsForAdmin();
        /*         * ********End Fetching data for dashboard ************* */
        $this->load->view($this->config->item('adminfolder') . '/header', $output);
        $this->load->view($this->config->item('adminfolder') . '/home');
        $this->load->view($this->config->item('adminfolder') . '/footer');
    }

    function login() {

        $failure = FALSE;
        $output['title'] = 'Login';
        if ($this->session->userdata('ADM_ID'))
            redirect('admin/dashboard');

        $this->load->helper('captcha');
        if (!empty($_POST)) {
            $word = $this->session->userdata('captchaWord');
            /*             * ************ For admin login start***************************************** */
            if ($this->input->post('type') == 'login') {
                $this->form_validation->set_rules('username', 'Username', 'trim|required|min_length[5]|max_length[12]');
                $this->form_validation->set_rules('password', 'Password', 'trim|required');
                $this->form_validation->set_rules('captcha', "Captcha", 'trim|required');
                /* Get the user's entered captcha value from the form */
                $userCaptcha = $this->input->post('captcha');
                /* Get the actual captcha value that we stored in the session (see below) */


                if ($this->form_validation->run()) {
                    if (strtoupper($userCaptcha) == strtoupper($word)) {
                        $row = $this->admin_model->doLogin();
                        if (isset($row->username) && $row->username != '') {

                            $this->session->set_userdata('ADM_ID', $row->adm_id);
                            if ($this->input->post('remember_me') == 'Yes') {
                                $values = base64_encode(base64_encode($row->mem_id . ',' . $row->email . ',' . $rec->password . ',' . $this->agent->browser()));
                                $cookie = array(
                                    'name' => 'remember_me',
                                    'value' => $values,
                                    'expire' => 3600 * 24 * 365
                                );
                                $this->input->set_cookie($cookie);
                            }
                            $this->admin_model->create_log($this->agent->browser(), $this->agent->version(), $this->agent->platform(), $this->agent->mobile());
                            $success_message = 'Login Successfully';
                        } else {
                            $error_message = 'Username or password you entered is wrong';
                            $failure = true;
                        }
                    } else {
                        $error_message = 'Captcha you entered is wrong';
                        $failure = true;
                    }
                } else {

                    $error_message = validation_errors();
                    $failure = true;
                }
                if ($this->input->is_ajax_request()) {
                    if ($failure) {
                        $data['success'] = false;
                        $data['error_message'] = $error_message;
                    } else {
                        $data['success'] = true;
                        $data['success_message'] = $success_message;
                        $data['url'] = site_url('admin/dashboard');
                    }
                    echo json_encode($data);
                    die;
                }
            }
            /*             * ************ For admin login End***************************************** */
            /*             * ************ For forget password start***************************************** */
            if ($this->input->post('type') == 'forget_password') {
                $this->load->model('mailsending_model');

                $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
                if ($this->form_validation->run()) {
                    $rec = $this->admin_model->getRecordByIdOrEmail($this->input->post('email'));
                    if (isset($rec->adm_id) && $rec->adm_id != '') {
                        $adminDetail = $this->admin_model->getRecordByIdOrEmail($this->input->post('email'));
                        $this->admin_model->forgetPassword();
                        $this->mailsending_model->adminforgotPasswordEmail($adminDetail);
                        $success_message = 'Your password has been reset successfully and sent to your registered mail id';
                    } else {
                        $error_message = 'Please enter registered email Id to reset your password';
                        $failure = true;
                    }
                } else {
                    $error_message = validation_errors();
                    $failure = true;
                }
                if ($this->input->is_ajax_request()) {
                    if ($failure) {
                        $data['success'] = false;
                        $data['error_message'] = $error_message;
                    } else {
                        $data['success'] = true;
                        $data['success_message'] = $success_message;
                        $data['url'] = site_url('admin/login');
                    }
                    echo json_encode($data);
                    die;
                }
            }
            /*             * ************ For forget password end***************************************** */
            if ($this->input->post('reload') == '1') {
                $this->load->helper('captcha');
                $vals = array(
                    'img_path' => './captcha/',
                    'img_url' => base_url() . 'captcha/',
                );

                /* Generate the captcha */
                $captcha = create_captcha($vals);
                /* Store the captcha value (or 'word') in a session to retrieve later */
                $this->session->set_userdata('captchaWord', $captcha['word']);

                echo $captcha['image'];
                exit;
            }
        }

        $vals = array(
            'img_path' => './captcha/',
            'img_url' => base_url() . 'captcha/',
        );

        /* Generate the captcha */
        $captcha = create_captcha($vals);

        /* Store the captcha value (or 'word') in a session to retrieve later */
        $this->session->set_userdata('captchaWord', $captcha['word']);

        $output['image'] = $captcha['image'];
        $this->load->view($this->config->item('adminfolder') . '/login', $output);
    }

    function logout() {
        $this->admin_model->logout();
    }

    //*********************** Admin My profile function starts *******************************//

    public function admin_profile() {
        $record = $this->admin_model->getRecordByIdOrEmail($this->session->userdata('ADM_ID'));
        $output['record'] = $record;
        $output['adminPage'] = "Admin Settings";
        $output['adminSubPage'] = "Update Profile";

        $output['name'] = $record->name;
        $output['email'] = $record->email;
        $output['mobile_no'] = $record->mobile_no;
        $output['address'] = $record->address;
        $output['profile_pic'] = $record->profile_pic;
        $output['password'] = $record->password;


        $logsData = $this->admin_model->getAdminLogsData($this->session->userdata('ADM_ID'));
        for ($i = 0; isset($logsData[$i]); $i++)
            $logsData[$i]->someDetail = $this->admin_model->getAdminSomeDetail($logsData[$i]->admin_id);
        $output['logsData'] = $logsData;

        if (!empty($_POST)) {


            if ($this->input->post('form_post_for') == 'personal_profile') {
                $failure = false;

                $this->form_validation->set_rules('name', 'Name', 'trim|required');
                $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
                $this->form_validation->set_rules('mobile_no', 'Mobile Number', 'trim|required');
                $this->form_validation->set_rules('address', 'Address', 'trim|required');

                if ($this->form_validation->run()) {
                    $this->admin_model->updateAdminPersonalProfile($this->session->userdata('ADM_ID'));
                    $success_message = 'Your profile has been updated successfully.';
                } else {

                    $errorMsg.= validation_errors();
                    $failure = true;
                }

                if ($this->input->is_ajax_request()) {
                    if ($failure) {
                        $data['success'] = false;
                        $data['error_message'] = $errorMsg;
                    } else {
                        $data['success'] = true;
                        $data['url'] = '';
                        $data['resetForm'] = false;
                        $data['success_message'] = $success_message;
                        $data['selfReload'] = true;
                    }
                    $data['scrollToThisForm'] = true;
                    echo json_encode($data);
                    die;
                }
            }
            if ($this->input->post('form_post_for') == 'admin_image') {
                $failure = false;

                $this->form_validation->set_rules('form_post_for', 'form_post_for', 'trim|required');

                if ($this->form_validation->run()) {
                    if ($_FILES['admin_icon_image']['name'] != '') {
                        $config['upload_path'] = './assets/uploads/admin_image/';
                        $config['allowed_types'] = 'gif|jpg|png|jpeg';
                        $this->load->library('upload', $config);
                        if (!$this->upload->do_upload('admin_icon_image')) {
                            $errorMsg = $this->upload->display_errors();
                            $failure = true;
                        } else {
                            $dataImage = array('upload_data' => $this->upload->data());
                        }
                    } else {
                        $failure = true;
                        $errorMsg.= "Please Select Image If You Want Change Image";
                    }

                    if (!$failure) {
                        $this->admin_model->updateAdminAvatar($this->session->userdata('ADM_ID'), $dataImage['upload_data']['file_name']);
                        $success_message = 'Your avtar has been updated successfully.';
                    }
                } else {
                    $errorMsg.= validation_errors();
                    $failure = true;
                }

                if ($this->input->is_ajax_request()) {
                    if ($failure) {
                        $data['success'] = false;
                        $data['error_message'] = $errorMsg;
                    } else {
                        $data['success'] = true;
                        $data['url'] = '';
                        $data['resetForm'] = false;
                        $data['success_message'] = $success_message;
                        $data['selfReload'] = true;
                    }
                    $data['scrollToThisForm'] = true;
                    echo json_encode($data);
                    die;
                }
            }

            if ($this->input->post('form_post_for') == 'change_password') {
                $failure = false;

                $this->form_validation->set_rules('old_password', 'Current Password', 'trim|required');
                $this->form_validation->set_rules('new_password', 'New Password', 'trim|required');
                $this->form_validation->set_rules('re_password', 'Repeat Password', 'trim|required|matches[new_password]');

                if ($this->form_validation->run()) {
                    if ($record->password == md5($this->input->post('old_password'))) {
                        $this->admin_model->updateAdminPasswords($this->session->userdata('ADM_ID'));
                        $success_message = 'Your admin panel password has been updated successfully.';
                    } else {
                        $errorMsg.= "<p>Your current password is wrong.</p>";
                        $failure = true;
                    }
                } else {

                    $errorMsg.= validation_errors();
                    $failure = true;
                }

                if ($this->input->is_ajax_request()) {
                    if ($failure) {
                        $data['success'] = false;
                        $data['error_message'] = $errorMsg;
                    } else {
                        $data['success'] = true;
                        $data['url'] = '';
                        $data['resetForm'] = true;
                        $data['success_message'] = $success_message;
                    }
                    $data['scrollToThisForm'] = true;
                    echo json_encode($data);
                    die;
                }
            }
        }

        $this->load->view($this->config->config['adminfolder'] . '/header', $output);
        $this->load->view($this->config->config['adminfolder'] . '/admin_profile');
        $this->load->view($this->config->config['adminfolder'] . '/footer');
    }

    //*********************** Admin My profile function Ends *******************************//
}
