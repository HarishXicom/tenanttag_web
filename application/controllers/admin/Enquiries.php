<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Enquiries extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->pageName = 'Enquiry';
        $this->breadcrum1 = 'Inquiry';
        $this->breadcrum2 = 'Inquiries';
        $this->breadcrum3 = 'Reply';
        $this->breadcrum4 = '';
        $this->breadcrum2_url = site_url('admin/enquiries');
        $this->breadcrum3_url = site_url('admin/enquiries/reply');
        $this->breadcrum4_url = '';
        $this->load->model('member_model');
        $this->load->model('enquiries_model');
        $this->common_model->checkAdminLogin();
        $this->js();
    }

    private function js() {
        $config[] = 'ckeditor/ckeditor.js';
        $this->config->set_item('adminjs', $config);
    }

    public function index($currentPage = NULL) {
        $output['title'] = 'Inquiries';

        if (!empty($_POST)) {
            $redirect_url = $_SERVER['HTTP_REFERER'];
            $task = $this->input->post('perform_task');
            $ids = $this->input->post('checkIds');
            if (sizeof($ids) > 0) {
                $message = $this->enquiries_model->performMultipleTasks($task, $ids);
                if ($message) {
                    $this->session->set_userdata('SUCCESS_MESSAGE', $message);
                } else {
                    $message = "Oops something is going wrong there...try again later.";
                    $this->session->set_userdata('ERROR_MESSAGE', $message);
                }
            } else {
                $message = "Oops something is going wrong there...try again later.";
                $this->session->set_userdata('ERROR_MESSAGE', $message);
            }
            if ($redirect_url)
                redirect($redirect_url);
            else
                redirect(site_url('admin/enquiries'));
        }

        $this->load->library('pagination');
        $config['base_url'] = base_url() . $this->config->config['adminfolder'] . '/enquiries/';
        $config['total_rows'] = $this->enquiries_model->getCountAllRecordsForAdmin('');
        $config['per_page'] = $this->config->item('page_size_admin');
        $this->pagination->initialize($config);
        $currentPage = (!$currentPage) ? 1 : $currentPage;
        $config['currentpage'] = $currentPage;
        if ($currentPage != "")
            $offset = ($currentPage - 1) * $config['per_page'];
        if ($config['total_rows'] > 0)
            $output['paging'] = $this->paging($config);

        $output['total'] = $this->enquiries_model->getCountAllRecordsForAdmin('');
        $output['records'] = $this->enquiries_model->getAllRecordsForAdmin($config['per_page'], $offset);


        /*         * ******** For Selected menu and sub-menu in left bar ************** */
        $output['pageName'] = $this->pageName;
        $output['subpageName'] = 'Inquiries';

        /*         * ******** For Breadcrum ************** */
        $output['breadcrum1'] = $this->breadcrum1;
        $output['breadcrum2'] = $this->breadcrum2;
        $output['breadcrum3'] = '';

        $output['breadcrum2_url'] = $this->breadcrum2_url;
        $output['breadcrum3_url'] = $this->breadcrum3_url;

        $this->load->view($this->config->item('adminfolder') . '/header', $output);
        $this->load->view($this->config->item('adminfolder') . '/enquiries_list');
        $this->load->view($this->config->item('adminfolder') . '/footer');
    }

    function view($id) {
        $output['records'] = $this->enquiries_model->getRecordById($id);
        $this->load->view($this->config->item('adminfolder') . '/enquiries_view', $output);
    }

    public function reply($id) {
        $this->load->model('mailsending_model');
        $output['enquiryDetail'] = $enquiryDetail = $this->enquiries_model->getRecordById($id);

        $output['title'] = 'Reply';

        if (!empty($_POST)) {
            $failure = FALSE;
            $this->form_validation->set_rules('message', 'Reply', 'trim|required');
            if ($this->form_validation->run()) {
                $this->mailsending_model->contactUsReply();
                $this->enquiries_model->updateReplyStatus($id);
                $success_message = 'Reply has been sent to Selected Members.';
            } else {
                $errorMsg.= validation_errors();
                $failure = true;
            }
            if ($this->input->is_ajax_request()) {
                if ($failure) {
                    $data['success'] = false;
                    $data['error_message'] = $errorMsg;
                } else {
                    $data['success'] = true;
                    $data['url'] = site_url($this->config->item('adminfolder') . '/enquiries');
                    $data['resetForm'] = false;
                    $data['success_message'] = $success_message;
                }
                $data['scrollToThisForm'] = true;
                echo json_encode($data);
                die;
            }
        }

        /*         * ******** For Selected menu and sub-menu in left bar ************** */
        $output['pageName'] = $this->pageName;
        $output['subpageName'] = "Reply";

        /*         * ******** For Breadcrum ************** */
        $output['breadcrum1'] = $this->breadcrum1;
        $output['breadcrum2'] = $this->breadcrum2;
        $output['breadcrum3'] = $this->breadcrum3;

        $output['breadcrum2_url'] = $this->breadcrum2_url;
        $output['breadcrum3_url'] = '';

        $this->load->view($this->config->config['adminfolder'] . '/header', $output);
        $this->load->view($this->config->config['adminfolder'] . '/enquiries_reply');
        $this->load->view($this->config->config['adminfolder'] . '/footer');
    }

    function filter($filterKey = NULL, $currentPage = NULL) {
        $output['title'] = 'Enquiries';

        if (!empty($_POST)) {
            $redirect_url = $_SERVER['HTTP_REFERER'];
            $task = $this->input->post('perform_task');
            $ids = $this->input->post('checkIds');
            if (sizeof($ids) > 0) {
                $message = $this->enquiries_model->performMultipleTasks($task, $ids);
                if ($message) {
                    $this->session->set_userdata('SUCCESS_MESSAGE', $message);
                } else {
                    $message = "Oops something is going wrong there...try again later.";
                    $this->session->set_userdata('ERROR_MESSAGE', $message);
                }
            } else {
                $message = "Oops something is going wrong there...try again later.";
                $this->session->set_userdata('ERROR_MESSAGE', $message);
            }
            if ($redirect_url)
                redirect($redirect_url);
            else
                redirect(site_url('admin/enquiries'));
        }

        $filterKey = $filterKey;
        if ($filterKey != 'NULL')
            $output['filterKey'] = $filterKey;

        $this->load->library('pagination');
        $config['base_url'] = base_url() . $this->config->config['adminfolder'] . '/enquiries/filter/' . $filterKey . '/';
        $config['total_rows'] = $this->enquiries_model->getCountFilterAllRecords($filterKey);
        $config['per_page'] = $this->config->item('page_size_admin');
        $this->pagination->initialize($config);
        $currentPage = (!$currentPage) ? 1 : $currentPage;
        $config['currentpage'] = $currentPage;
        if ($currentPage != "")
            $offset = ($currentPage - 1) * $config['per_page'];
        if ($config['total_rows'] > 0)
            $output['paging'] = $this->paging($config);

        $output['total'] = $this->enquiries_model->getCountAllRecordsForAdmin('');
        $output['records'] = $this->enquiries_model->getFilterAllRecords($filterKey, $config['per_page'], $offset);

        /*         * ******** For Selected menu and sub-menu in left bar ************** */
        $output['pageName'] = $this->pageName;
        $output['subpageName'] = 'Enquiries';

        /*         * ******** For Breadcrum ************** */
        $output['breadcrum1'] = $this->breadcrum1;
        $output['breadcrum2'] = $this->breadcrum2;
        $output['breadcrum3'] = '';

        $output['breadcrum2_url'] = $this->breadcrum2_url;
        $output['breadcrum3_url'] = $this->breadcrum3_url;

        $this->load->view($this->config->item('adminfolder') . '/header', $output);
        $this->load->view($this->config->item('adminfolder') . '/enquiries_list');
        $this->load->view($this->config->item('adminfolder') . '/footer');
    }

    function doTask($task, $id) {
        $output['task'] = $task;
        if (!empty($_POST)) {
            $message = $this->enquiries_model->performTask($task, $id);
            $this->session->set_userdata('SUCCESS_MESSAGE', $message);
            $redirect_url = $_SERVER['HTTP_REFERER'];
            if ($redirect_url)
                redirect($redirect_url);
            else
                redirect(site_url('admin/enquiries'));
        }
        $this->load->view($this->config->item('adminfolder') . '/active_inactive_delete', $output);
    }

    #================= Paging ==========================================================#

    protected function paging($config) {
        $total_pages = ceil($config['total_rows'] / $config['per_page']);

        $total_pages = ceil($config['total_rows'] / $config['per_page']);
        $start = max($config['currentpage'] - intval($config['per_page'] / 2), 1);
        $end = $start + $config['per_page'] - 1;

        if ($config['currentpage'] == 1) {
            if ($config['total_rows'] > $config['per_page']) {
                $showing = '1-' . $config['per_page'];
            } else {
                $showing = '1-' . $config['total_rows'];
            }
        } else {
            $showing = ((($config['currentpage'] - 1) * $config['per_page']) + 1) . '-';
            if (($config['currentpage'] * $config['per_page']) > $config['total_rows']) {
                $showing.=$config['total_rows'];
            } else {
                $showing.=($config['currentpage'] * $config['per_page']);
            }
        }

        $output = '<ul class="pagination">';

        if ($config['currentpage'] > 1) {
            $output.='<li class=""><a href="' . $config['base_url'] . ($config['currentpage'] - 1) . '"> <i class="fa fa-angle-double-left"></i> </a></li>';
        } else {
            $output.='<li class="prev disabled "><a> <i class="fa fa-angle-double-left"></i> </a></li>';
        }

        for ($i = $start; $i <= $end && $i <= $total_pages; $i++) {
            if ($i == $config['currentpage']) {
                $output .= '<li class="active"><a>' . $i . '</a></li>';
            } else {
                $output .='<li><a href="' . $config['base_url'] . $i . '">' . $i . '</a></li>';
            }
        }
        $output .='</li>';

        if ($total_pages > $config['currentpage']) {
            $output.='<li class="next"><a href="' . $config['base_url'] . ($config['currentpage'] + 1) . '"> <i class="fa fa-angle-double-right"></i></a></li>';
        } else {
            $output.='<li class="next disabled"><a > <i class="fa fa-angle-double-right"></i> </a></li>';
        }
        $output.='</ul>';
        return $output;
    }

}
