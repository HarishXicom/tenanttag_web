<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Common_question extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->pageName = 'Questions';
        $this->breadcrum1 = 'Questions';
        //$this->common_model->checkAdminLogin();   
    }

    function index(){
        $output['title'] = 'Question';
        if (!empty($this->input->post())) {
           
            $failure = 0;
            $this->form_validation->set_rules('question', 'Question', 'trim|required');
            $this->form_validation->set_rules('category', 'Category', 'trim|required');
            if ($this->form_validation->run()) {
                $conditions = $params = array();
                $conditions['question'] = $this->input->post('question');
                $conditions['category_id'] = $this->input->post('category');
                $conditions['status'] = 1;
                $conditions['type'] = 1;
                $conditions['created_Date'] = date('Y-m-d h:i:s');
                $params['table'] = 'tbl_questions';
                $this->common_model->insert_data($conditions, $params);
                $success_message = '<p>Question Added successfully.</p>';
            } else {
                $error_message = validation_errors();
                $failure = true;
            }
            if ($this->input->is_ajax_request()) {
                if ($failure) {
                    $data['success'] = false;
                    $data['error_message'] = $error_message;
                } else {
                    $data['success'] = true;
                    $data['resetForm'] = true;
                    $data['success_message'] = $success_message;
                    $data['url'] = site_url('admin/common_question');
                }
                $data['scrollToElement'] = true;
                echo json_encode($data);
                die;
            }
        }
        /*         * ******** For Selected menu and sub-menu in left bar ************** */
        $output['pageName'] = $this->pageName;
        $output['subpageName'] = 'Common Question';

        /*         * ******** For Breadcrum ************** */
        $output['breadcrum1'] = $this->breadcrum1;
        $output['breadcrum2'] = 'Question';
        $output['breadcrum2_url'] = site_url('admin/common_question');

        $output['records'] = $this->common_model->getQuestions(1);
        //print_r($output); exit;

        $conditions = $params = array();
        $params['table'] = 'tbl_question_category';
        $output['question_category'] = $this->common_model->get_data($conditions, $params);  

        $this->load->view($this->config->item('adminfolder') . '/header', $output);
        $this->load->view($this->config->item('adminfolder') . '/common_question');
        $this->load->view($this->config->item('adminfolder') . '/footer');
    }

    public function delete_question($id) {
        $conditions = $params = array();
        $conditions['id'] = $id;
        $params['table'] = 'tbl_questions';
        $this->common_model->delete($conditions, $params);
        redirect(site_url('admin/common_question'));    
    }

    public function edit_question($id) {

    	if (!empty($this->input->post())) {
           
            $failure = 0;
            $this->form_validation->set_rules('question', 'Question', 'trim|required');
            $this->form_validation->set_rules('category', 'Category', 'trim|required');
            if ($this->form_validation->run()) {
                $conditions = $params = array();
                $conditions['value']['question'] = $this->input->post('question');
                $conditions['value']['category_id'] = $this->input->post('category');
                $conditions['where']['id'] = $id;
                $params['table'] = 'tbl_questions';
                $this->common_model->update($conditions, $params);
                $success_message = '<p>Question updated successfully.</p>';
            } else {
                $error_message = validation_errors();
                $failure = true;
            }
            if ($this->input->is_ajax_request()) {
                if ($failure) {
                    $data['success'] = false;
                    $data['error_message'] = $error_message;
                } else {
                    $data['success'] = true;
                    $data['resetForm'] = true;
                    $data['success_message'] = $success_message;
                    $data['url'] = site_url('admin/common_question');
                }
                $data['scrollToElement'] = true;
                echo json_encode($data);
                die;
            }
        }
        $conditions = $params = array();
        $conditions['id'] = $id;
        $params['table'] = 'tbl_questions';
        $params['single_row'] = true;
        $output['edit_record'] = $this->common_model->get_data($conditions, $params);
        //echo "<pre>"; print_r($output); exit;

        $output['title'] = 'Edit Question';
        $output['pageName'] = $this->pageName;
        $output['subpageName'] = 'Edit Question';

        /*         * ******** For Breadcrum ************** */
        $output['breadcrum1'] = $this->breadcrum1;
        $output['breadcrum2'] = 'Edit Question';
        $output['breadcrum2_url'] = site_url('admin/common_question');

        $output['records'] = $this->common_model->getQuestions(1);

        $conditions = $params = array();
        $params['table'] = 'tbl_question_category';
        $output['question_category'] = $this->common_model->get_data($conditions, $params);  
        //print_r($output); exit;

        $this->load->view($this->config->item('adminfolder') . '/header', $output);
        $this->load->view($this->config->item('adminfolder') . '/common_question');
        $this->load->view($this->config->item('adminfolder') . '/footer');

    }

    public function view_answer($question_id) {
        $conditions = $params = array();

        if (!empty($this->input->post())) {
           
            $failure = 0;
            $this->form_validation->set_rules('answer', 'Answer', 'trim|required');
            if ($this->form_validation->run()) {
                $conditions = $params = array();
                $conditions['question_id'] = $question_id;
                $conditions['answer'] = $this->input->post('answer');
                $conditions['status'] = 1;
                $conditions['created_date'] = date('Y-m-d h:i:s');
                $conditions['modified_date'] = date('Y-m-d h:i:s');
                $params['table'] = 'tbl_answers';
                $this->common_model->insert_data($conditions, $params);
                $success_message = '<p>Answer Added successfully.</p>';
            } else {
                $error_message = validation_errors();
                $failure = true;
            }
            if ($this->input->is_ajax_request()) {
                if ($failure) {
                    $data['success'] = false;
                    $data['error_message'] = $error_message;
                } else {
                    $data['success'] = true;
                    $data['resetForm'] = true;
                    $data['success_message'] = $success_message;
                    $data['url'] = site_url('admin/common_question/view_answer/'.$question_id);
                }
                $data['scrollToElement'] = true;
                echo json_encode($data);
                die;
            }
        }

        /*         * ******** Question info ************** */
        $conditions = $params = array();
        $conditions['id'] = $question_id;
        $params['table'] = 'tbl_questions';
        $params['single_row'] = true;
        $output['question_info'] = $this->common_model->get_data($conditions, $params);         

        $output['title'] = 'Answer';
        $output['pageName'] = $this->pageName;
        $output['subpageName'] = 'View Answer';

        /*         * ******** For Breadcrum ************** */
        $output['breadcrum1'] = $this->breadcrum1;
        $output['breadcrum2'] = 'View Answer';
        $output['breadcrum2_url'] = site_url('admin/common_question');

        
        /*         * ******** All answers ************** */
        $conditions = $params = array();
        $conditions['question_id'] = $question_id;
        $params['table'] = 'tbl_answers';
        $output['answer_records'] = $this->common_model->get_data($conditions, $params); 


        $this->load->view($this->config->item('adminfolder') . '/header', $output);
        $this->load->view($this->config->item('adminfolder') . '/common_qus_answer_list');
        $this->load->view($this->config->item('adminfolder') . '/footer'); 
    }


    public function edit_answer($question_id,$id) {
        $conditions = $params = array();

        if (!empty($this->input->post())) {
           
            $failure = 0;
            $this->form_validation->set_rules('answer', 'Answer', 'trim|required');
            if ($this->form_validation->run()) {

            	 $conditions = $params = array();
                $conditions['value']['answer'] = $this->input->post('answer');
                $conditions['where']['id'] = $id;
                $conditions['value']['modified_date'] = date('Y-m-d h:i:s');
                $params['table'] = 'tbl_answers';
                $this->common_model->update($conditions, $params);
                $success_message = '<p>Answer updated successfully.</p>';
                
            } else {
                $error_message = validation_errors();
                $failure = true;
            }
            if ($this->input->is_ajax_request()) {
                if ($failure) {
                    $data['success'] = false;
                    $data['error_message'] = $error_message;
                } else {
                    $data['success'] = true;
                    $data['resetForm'] = true;
                    $data['success_message'] = $success_message;
                    $data['url'] = site_url('admin/common_question/view_answer/'.$question_id);
                }
                $data['scrollToElement'] = true;
                echo json_encode($data);
                die;
            }
        }

        $conditions = $params = array();
        $conditions['id'] = $question_id;
        $params['table'] = 'tbl_questions';
        $params['single_row'] = true;
        $output['question_info'] = $this->common_model->get_data($conditions, $params); 

        $output['title'] = 'Answer';
        $output['pageName'] = $this->pageName;
        $output['subpageName'] = 'Edit Answer';

        /*         * ******** For Breadcrum ************** */
        $output['breadcrum1'] = $this->breadcrum1;
        $output['breadcrum2'] = 'Edit Answer';
        $output['breadcrum2_url'] = site_url('admin/common_question');

        /*         * ******** Question info ************** */
        $conditions = $params = array();
        $conditions['id'] = $id;
        $params['table'] = 'tbl_answers';
        $params['single_row'] = true;
        $output['edit_answer_info'] = $this->common_model->get_data($conditions, $params); 

        $conditions = $params = array();
        $conditions['question_id'] = $question_id;
        $params['table'] = 'tbl_answers';
        $output['answer_records'] = $this->common_model->get_data($conditions, $params); 


        $this->load->view($this->config->item('adminfolder') . '/header', $output);
        $this->load->view($this->config->item('adminfolder') . '/common_qus_answer_list');
        $this->load->view($this->config->item('adminfolder') . '/footer'); 
    }

    public function delete_answer($question_id,$id) {
        $conditions = $params = array();
        $conditions['id'] = $id;
        $params['table'] = 'tbl_answers';
        $this->common_model->delete($conditions, $params);
        redirect(site_url('admin/common_question/view_answer/'.$question_id));    
    }




    public function view_category() {
        $conditions = $params = array();

        if (!empty($this->input->post())) {
           
            $failure = 0;
            $this->form_validation->set_rules('category', 'category', 'trim|required');
            if ($this->form_validation->run()) {
                $conditions = $params = array();
                $conditions['category_name'] = $this->input->post('category');
                $conditions['created_date'] = date('Y-m-d h:i:s');
                $params['table'] = 'tbl_question_category';
                $this->common_model->insert_data($conditions, $params);
                $success_message = '<p>Answer Added successfully.</p>';
            } else {
                $error_message = validation_errors();
                $failure = true;
            }
            if ($this->input->is_ajax_request()) {
                if ($failure) {
                    $data['success'] = false;
                    $data['error_message'] = $error_message;
                } else {
                    $data['success'] = true;
                    $data['resetForm'] = true;
                    $data['success_message'] = $success_message;
                    $data['url'] = site_url('admin/view_category');
                }
                $data['scrollToElement'] = true;
                echo json_encode($data);
                die;
            }
        }

        /*         * ******** Question info ************** */
        $conditions = $params = array();
        $params['table'] = 'tbl_question_category';
        $output['question_category'] = $this->common_model->get_data($conditions, $params);         

        $output['title'] = 'View Question Category';
        $output['pageName'] = $this->pageName;
        $output['subpageName'] = 'View Question Category';

        /*         * ******** For Breadcrum ************** */
        $output['breadcrum1'] = $this->breadcrum1;
        $output['breadcrum2'] = 'View Question Category';
        $output['breadcrum2_url'] = site_url('admin/view_category');



        $this->load->view($this->config->item('adminfolder') . '/header', $output);
        $this->load->view($this->config->item('adminfolder') . '/common_question_category');
        $this->load->view($this->config->item('adminfolder') . '/footer'); 
    }


    public function edit_category($id) {
        $conditions = $params = array();

        if (!empty($this->input->post())) {
           
            $failure = 0;
            $this->form_validation->set_rules('category', 'Category', 'trim|required');
            if ($this->form_validation->run()) {

            	 $conditions = $params = array();
                $conditions['value']['category_name'] = $this->input->post('category');
                $conditions['where']['id'] = $id;
                $params['table'] = 'tbl_question_category';
                $this->common_model->update($conditions, $params);
                $success_message = '<p>Answer updated successfully.</p>';
                
            } else {
                $error_message = validation_errors();
                $failure = true;
            }
            if ($this->input->is_ajax_request()) {
                if ($failure) {
                    $data['success'] = false;
                    $data['error_message'] = $error_message;
                } else {
                    $data['success'] = true;
                    $data['resetForm'] = true;
                    $data['success_message'] = $success_message;
                    $data['url'] = site_url('admin/view_category');
                }
                $data['scrollToElement'] = true;
                echo json_encode($data);
                die;
            }
        }

        $output['title'] = 'Answer';
        $output['pageName'] = $this->pageName;
        $output['subpageName'] = 'Edit Answer';

        /*         * ******** For Breadcrum ************** */
        $output['breadcrum1'] = $this->breadcrum1;
        $output['breadcrum2'] = 'Edit Answer';
        $output['breadcrum2_url'] = site_url('admin/view_category');

        /*         * ******** Question info ************** */
         $conditions = $params = array();
        $params['table'] = 'tbl_question_category';
        $output['question_category'] = $this->common_model->get_data($conditions, $params); 

        $conditions = $params = array();
        $conditions['id'] = $id;
        $params['table'] = 'tbl_question_category';
        $params['single_row'] = true;
        $output['edit_record'] = $this->common_model->get_data($conditions, $params); 

        $this->load->view($this->config->item('adminfolder') . '/header', $output);
        $this->load->view($this->config->item('adminfolder') . '/common_question_category');
        $this->load->view($this->config->item('adminfolder') . '/footer'); 
    }

    public function delete_category($id) {
        $conditions = $params = array();
        $conditions['id'] = $id;
        $params['table'] = 'tbl_question_category';
        $this->common_model->delete($conditions, $params);
        redirect(site_url('admin/view_category'));    
    }


    function survey_question(){
        $output['title'] = 'Survey Question';
        if (!empty($this->input->post())) {
           
            $failure = 0;
            $this->form_validation->set_rules('question', 'Question', 'trim|required');
            if ($this->form_validation->run()) {
                $conditions = $params = array();
                $conditions['question'] = $this->input->post('question');                
                $conditions['answer_type'] = $this->input->post('answer_type');
                if($this->input->post('answer_type') == 1){
                	$conditions['answer1'] = '';
	                $conditions['answer2'] = '';
	                $conditions['answer3'] = '';
	                $conditions['answer4'] = '';
	                $conditions['answer5'] = '';
                }else{
	                $conditions['answer1'] = $this->input->post('answer1');
	                $conditions['answer2'] = $this->input->post('answer2');
	                $conditions['answer3'] = $this->input->post('answer3');
	                $conditions['answer4'] = $this->input->post('answer4');
	                $conditions['answer5'] = $this->input->post('answer5');
                }
                $conditions['status'] = 1;
                $conditions['type'] = 2;
                $conditions['created_Date'] = date('Y-m-d h:i:s');
                $params['table'] = 'tbl_questions';
                $this->common_model->insert_data($conditions, $params);
                $success_message = '<p>Question Added successfully.</p>';
            } else {
                $error_message = validation_errors();
                $failure = true;
            }
            if ($this->input->is_ajax_request()) {
                if ($failure) {
                    $data['success'] = false;
                    $data['error_message'] = $error_message;
                } else {
                    $data['success'] = true;
                    $data['resetForm'] = true;
                    $data['success_message'] = $success_message;
                    $data['url'] = site_url('admin/survey_question');
                }
                $data['scrollToElement'] = true;
                echo json_encode($data);
                die;
            }
        }
        /*         * ******** For Selected menu and sub-menu in left bar ************** */
        $output['pageName'] = 'Survey Question';
        $output['subpageName'] = 'Survey Question';

        /*         * ******** For Breadcrum ************** */
        $output['breadcrum1'] = 'Survey';
        $output['breadcrum2'] = 'Survey Question';
        $output['breadcrum2_url'] = site_url('admin/survey_question');

        $output['records'] = $this->common_model->getQuestions(2);
        //print_r($output); exit;

        $this->load->view($this->config->item('adminfolder') . '/header', $output);
        $this->load->view($this->config->item('adminfolder') . '/survey_question');
        $this->load->view($this->config->item('adminfolder') . '/footer');
    }

     function survey_question_answer($id){
        $output['title'] = 'Survey Answer';
       
        /*         * ******** For Selected menu and sub-menu in left bar ************** */
        $output['pageName'] = 'Survey Question';
        $output['subpageName'] = 'Survey Answer';

        /*         * ******** For Breadcrum ************** */
        $output['breadcrum1'] = 'Survey';
        $output['breadcrum2'] = 'Survey Answer';
        $output['breadcrum2_url'] = site_url('admin/survey_question');


        $conditions = $params = array();
        $conditions['id'] = $id;
        $params['table'] = 'tbl_questions';
        $params['single_row'] = true;
        $output['question'] = $this->common_model->get_data($conditions, $params);

        $output['records'] = $this->common_model->getSurveyAnswer($id);
        //echo $this->db->last_query();
        //print_r($output); exit;

        $this->load->view($this->config->item('adminfolder') . '/header', $output);
        $this->load->view($this->config->item('adminfolder') . '/survey_answer');
        $this->load->view($this->config->item('adminfolder') . '/footer');
    }

    public function edit_survey_question($id) {

        if (!empty($this->input->post())) {
           
            $failure = 0;
            $this->form_validation->set_rules('question', 'Question', 'trim|required');
            if ($this->form_validation->run()) {
                $conditions = $params = array();
                $conditions['value']['question'] = $this->input->post('question');
                $conditions['value']['answer_type'] = $this->input->post('answer_type');
                if($this->input->post('answer_type') == 1){
                	$conditions['value']['answer1'] = ' ';
	                $conditions['value']['answer2'] = ' ';
	                $conditions['value']['answer3'] = ' ';
	                $conditions['value']['answer4'] = ' ';
	                $conditions['value']['answer5'] = ' ';
                }else{
	                $conditions['value']['answer1'] = $this->input->post('answer1');
	                $conditions['value']['answer2'] = $this->input->post('answer2');
	                $conditions['value']['answer3'] = $this->input->post('answer3');
	                $conditions['value']['answer4'] = $this->input->post('answer4');
	                $conditions['value']['answer5'] = $this->input->post('answer5');
                	
                }
                $conditions['where']['id'] = $id;
                $params['table'] = 'tbl_questions';
                //print_r($conditions); exit;
                $this->common_model->update($conditions, $params);
                $success_message = '<p>Question updated successfully.</p>';
            } else {
                $error_message = validation_errors();
                $failure = true;
            }
            if ($this->input->is_ajax_request()) {
                if ($failure) {
                    $data['success'] = false;
                    $data['error_message'] = $error_message;
                } else {
                    $data['success'] = true;
                    $data['resetForm'] = true;
                    $data['success_message'] = $success_message;
                    $data['url'] = site_url('admin/survey_question');
                }
                $data['scrollToElement'] = true;
                echo json_encode($data);
                die;
            }
        }
        $conditions = $params = array();
        $conditions['id'] = $id;
        $params['table'] = 'tbl_questions';
        $params['single_row'] = true;
        $output['edit_record'] = $this->common_model->get_data($conditions, $params);
        //echo "<pre>"; print_r($output); exit;

        $output['title'] = 'Edit Survey Question';
        $output['pageName'] = 'Survey Question';
        $output['subpageName'] = 'Edit Survey Question';

        /*         * ******** For Breadcrum ************** */
        $output['breadcrum1'] = 'Survey';
        $output['breadcrum2'] = 'Edit Survey  Question';
        $output['breadcrum2_url'] = site_url('admin/survey_question');

        $output['records'] = $this->common_model->getQuestions(2);

        //print_r($output); exit;

        $this->load->view($this->config->item('adminfolder') . '/header', $output);
        $this->load->view($this->config->item('adminfolder') . '/survey_question');
        $this->load->view($this->config->item('adminfolder') . '/footer');

    }

    function delete_survey_question($id) {
        $conditions = $params = array();
        $conditions['id'] = $id;
        $params['table'] = 'tbl_questions';
        $this->common_model->delete($conditions, $params);
        redirect(site_url('admin/survey_question'));    
    }
    


}
