<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Reports extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->pageName = 'Messages';
        $this->breadcrum1 = 'Messages';
        $this->breadcrum2 = 'tMaintenance Messages';
        $this->breadcrum3 = 'Add Request';
        $this->breadcrum4 = 'Edit Request';
        $this->breadcrum2_url = site_url('admin/reports');
        $this->breadcrum3_url = site_url('admin/reports/add');
        $this->breadcrum4_url = site_url('admin/reports/edit');
        $this->load->model('messages_model');
        // $this->load->model('reports_model');
        $this->common_model->checkAdminLogin();
        $this->js();
    }

    private function js() {
        $config[] = 'ckeditor/ckeditor.js';
        $this->config->set_item('adminjs', $config);
    }

    public function index($currentPage = NULL) {
        $output['title'] = 'tMaintenance Messages';

        if ($this->input->post('perform_task')!='') {

            $redirect_url = $_SERVER['HTTP_REFERER'];
            $task = $this->input->post('perform_task');
            $ids = $this->input->post('checkIds');
            if (sizeof($ids) > 0) {
                $message = $this->messages_model->performMultipleTasksReport($task, $ids);
                if ($message) {
                    $this->session->set_userdata('SUCCESS_MESSAGE', $message);
                } else {
                    $message = "Oops something is going wrong there...try again later.";
                    $this->session->set_userdata('ERROR_MESSAGE', $message);
                }
            } else {
                $message = "Oops something is going wrong there...try again later.";
                $this->session->set_userdata('ERROR_MESSAGE', $message);
            }
            if ($redirect_url)
                redirect($redirect_url);
            else
                redirect(site_url('admin/messages'));
        }

        $this->load->library('pagination');
        $keyword = '';
        if ($this->input->post('search') != '') {
            $keyword = $this->input->post('search');
        }
        $config['base_url'] = base_url() . $this->config->config['adminfolder'] . '/reports/';
        $config['total_rows'] = $this->getMaintainenceReports('1', '0', '10','',$keyword);
        $config['per_page'] = $this->config->item('page_size_admin');
        $this->pagination->initialize($config);
        $currentPage = (!$currentPage) ? 1 : $currentPage;
        $config['currentpage'] = $currentPage;
        if ($currentPage != "")
            $offset = ($currentPage - 1) * $config['per_page'];
        if ($config['total_rows'] > 0)
            $output['paging'] = $this->paging($config);
        

        $output['records'] = $this->getMaintainenceReports('0', $config['per_page'], $offset,'',$keyword);
        //  echo '<pre>';print_r($output);die;
        /*         * ******** For Selected menu and sub-menu in left bar ************** */
        $output['pageName'] = $this->pageName;
        $output['subpageName'] = 'tMaintenance Messages';

        /*         * ******** For Breadcrum ************** */
        $output['breadcrum1'] = $this->breadcrum1;
        $output['breadcrum2'] = $this->breadcrum2;
        $output['breadcrum3'] = '';

        $output['breadcrum2_url'] = $this->breadcrum2_url;
        $output['breadcrum3_url'] = $this->breadcrum3_url;

        $this->load->view($this->config->item('adminfolder') . '/header', $output);
        $this->load->view($this->config->item('adminfolder') . '/request_list');
        $this->load->view($this->config->item('adminfolder') . '/footer');
    }

    function getMaintainenceReports($cnt, $limit, $offset, $single,$keyword) {
        $conditions = $params = array();
        $params['complex'] = true;
        $conditions['tables'] = array(
            'reports' => 'tbl_maintenance_reports',
            'property' => 'tbl_properties',
            'category' => 'tbl_tservices',
            'tenant' => 'tbl_members',
            'landlord' => 'tbl_members',
            'state' => 'tbl_region'
        );
        $conditions['table'] = 'reports';
        $conditions['on']['property'] = array(
            'sign' => '=',
            'column' => 'prop_id',
            'alias_column' => 'property_id',
            'alias_other' => 'reports',
        );
        $conditions['on']['category'] = array(
            'sign' => '=',
            'column' => 't_id',
            'alias_column' => 'category_id',
            'alias_other' => 'reports',
        );
        $conditions['on']['tenant'] = array(
            'sign' => '=',
            'column' => 'mem_id',
            'alias_column' => 'tenant_id',
            'alias_other' => 'reports',
        );
        $conditions['on']['landlord'] = array(
            'sign' => '=',
            'column' => 'mem_id',
            'alias_column' => 'owner_id',
            'alias_other' => 'reports',
        );
        $conditions['on']['state'] = array(
            'sign' => '=',
            'column' => 'region_id',
            'alias_column' => 'state_id',
            'alias_other' => 'property',
        );
         if ($keyword != '') {
           
            
            $conditions['where']['landlord.first_name']['alias'] = '';
            $conditions['where']['landlord.first_name']['value'] = array(
                'operator' => 'OR',
                'sign' => 'LIKE',
                'key' => 'landlord',
                'value' => $keyword
            );
            $conditions['where']['landlord.last_name']['alias'] = '';
            $conditions['where']['landlord.last_name']['value'] = array(
                'operator' => 'OR',
                'sign' => 'LIKE',
                'key' => 'landlord',
                'value' => $keyword
            );
            
            $conditions['where']['name']['alias'] = 'category';
            $conditions['where']['name']['value'] = array(
                'operator' => 'OR',
                'sign' => 'LIKE',
                'key' => 'name',
                'value' => $keyword
            );
             $conditions['where']['first_name']['alias'] = 'tenant';
            $conditions['where']['first_name']['value'] = array(
                'operator' => 'OR',
                'sign' => 'LIKE',
                'key' => 'tenant',
                'value' => $keyword
            );
            $conditions['where']['last_name']['alias'] = 'tenant';
            $conditions['where']['last_name']['value'] = array(
                'operator' => 'OR',
                'sign' => 'LIKE',
                'key' => 'tenant',
                'value' => $keyword
            );
        }
        if ($cnt == 1) {
            $params['cnt'] = true;
        } else if ($single != '') {
            $params['single_row'] = true;
            $conditions['where']['id']['alias'] = 'reports';
            $conditions['where']['id']['value'] = $single;
        } else {
            $params['offset'] = $offset;
            $params['limit'] = $limit;
        }

            $params['fields'] = array('reports.id as report_id', 'reports.report', 'reports.date_created', 'category.name as category',
            'tenant.first_name as tfname', 'tenant.last_name as tlname',
            'landlord.first_name as lfname', 'landlord.last_name as llname',
            'state.region_name', 'property.prop_id', 'property.unit_number', 'property.address1', 'property.address2', 'property.city', 'property.zip');
        $params['order_by'] = 'reports.id DESC';
        $info = $this->common_model->get_data($conditions, $params);
        return $info;
    }

    function add() {
        $output['title'] = 'Add Message';
        if (!empty($_POST)) {
            $failure = FALSE;
            $this->form_validation->set_rules('title', 'Title', 'trim|required');
            $this->form_validation->set_rules('description', 'Description', 'trim|required');
            if ($this->form_validation->run()) {

                $this->messages_model->add();
                $success_message = 'Message added successfully';
            } else {

                $error_message = validation_errors();
                $failure = true;
            }
            if ($this->input->is_ajax_request()) {
                if ($failure) {
                    $data['success'] = false;
                    $data['error_message'] = $error_message;
                } else {
                    $data['success'] = true;
                    $data['success_message'] = $success_message;
                    $data['url'] = site_url('admin/messages');
                }
                $data['scrollToElement'] = true;
                echo json_encode($data);
                die;
            }
        }
        /*         * ******** For Selected menu and sub-menu in left bar ************** */
        $output['pageName'] = $this->pageName;
        $output['subpageName'] = 'Add Message';

        /*         * ******** For Breadcrum ************** */
        $output['breadcrum1'] = $this->breadcrum1;
        $output['breadcrum2'] = $this->breadcrum2;
        $output['breadcrum3'] = $this->breadcrum3;

        $output['breadcrum2_url'] = $this->breadcrum2_url;
        $output['breadcrum3_url'] = '';

        $this->load->view($this->config->item('adminfolder') . '/header', $output);
        $this->load->view($this->config->item('adminfolder') . '/add_message');
        $this->load->view($this->config->item('adminfolder') . '/footer');
    }

    function edit($id) {
        $output['title'] = 'Edit Message';
        if (!empty($_POST)) {
            $failure = FALSE;
            $this->form_validation->set_rules('title', 'Title', 'trim|required');
            $this->form_validation->set_rules('description', 'Description', 'trim|required');
            if ($this->form_validation->run()) {

                $this->messages_model->update($id);
                $success_message = 'Message record updated successfully';
            } else {

                $error_message = validation_errors();
                $failure = true;
            }
            if ($this->input->is_ajax_request()) {
                if ($failure) {
                    $data['success'] = false;
                    $data['error_message'] = $error_message;
                } else {
                    $data['success'] = true;
                    $data['success_message'] = $success_message;
                    $data['url'] = site_url('admin/messages');
                }
                $data['scrollToElement'] = true;
                echo json_encode($data);
                die;
            }
        }
        /*         * ******** For Selected menu and sub-menu in left bar ************** */
        $output['pageName'] = $this->pageName;
        $output['subpageName'] = 'Edit Message';

        /*         * ******** For Breadcrum ************** */
        $output['breadcrum1'] = $this->breadcrum1;
        $output['breadcrum2'] = $this->breadcrum2;
        $output['breadcrum3'] = '';
        $output['breadcrum4'] = $this->breadcrum4;

        $output['breadcrum2_url'] = $this->breadcrum2_url;
        $output['breadcrum3_url'] = $this->breadcrum3_url;
        $output['breadcrum4_url'] = '';

        $output['recordDetail'] = $recordDetail = $this->messages_model->getRecordById($id);

        $output['action'] = 'edit';

        $this->load->view($this->config->item('adminfolder') . '/header', $output);
        $this->load->view($this->config->item('adminfolder') . '/add_message');
        $this->load->view($this->config->item('adminfolder') . '/footer');
    }

    function view($id) {
        $output['records'] = $this->getMaintainenceReports('0', '0', '10', $id,'');
        $this->load->view($this->config->item('adminfolder') . '/request_view', $output);
    }

    function filter($filterKey = NULL, $show_me = NULL, $sort_by = NULL, $currentPage = NULL) {
        $output['title'] = 'Messages';

        $filterKey = urldecode($filterKey);
        if ($filterKey != 'NULL')
            $output['filterKey'] = $filterKey;
        if ($show_me != 'NULL')
            $output['show_me'] = $show_me;
        if ($sort_by != 'NULL')
            $output['sort_by'] = $sort_by;


        if ($show_me != 'NULL' || $sort_by != 'NULL')
            $output['advanceSearch'] = 'Yes';

        if (!empty($_POST)) {
            $redirect_url = $_SERVER['HTTP_REFERER'];
            $task = $this->input->post('perform_task');
            $ids = $this->input->post('checkIds');
            if (sizeof($ids) > 0) {
                $message = $this->messages_model->performMultipleTasksReport($task, $ids);
                if ($message) {
                    $this->session->set_userdata('SUCCESS_MESSAGE', $message);
                } else {
                    $message = "Oops something is going wrong there...try again later.";
                    $this->session->set_userdata('ERROR_MESSAGE', $message);
                }
            } else {
                $message = "Oops something is going wrong there...try again later.";
                $this->session->set_userdata('ERROR_MESSAGE', $message);
            }
            if ($redirect_url)
                redirect($redirect_url);
            else
                redirect(site_url('admin/messages'));
        }

        $filterKey = $filterKey;
        if ($filterKey != 'NULL')
            $output['filterKey'] = $filterKey;

        $this->load->library('pagination');
        $config['base_url'] = base_url() . $this->config->config['adminfolder'] . '/messages/filter/' . $filterKey . '/' . $show_me . '/' . $sort_by . '/';
        $config['total_rows'] = $this->messages_model->getCountFilterAllRecords($filterKey, $show_me, $sort_by);
        $config['per_page'] = $this->config->item('page_size_admin');
        $this->pagination->initialize($config);
        $currentPage = (!$currentPage) ? 1 : $currentPage;
        $config['currentpage'] = $currentPage;
        if ($currentPage != "")
            $offset = ($currentPage - 1) * $config['per_page'];
        if ($config['total_rows'] > 0)
            $output['paging'] = $this->paging($config);

      
        $output['records'] = $this->messages_model->getFilterAllRecords($filterKey, $show_me, $sort_by, $config['per_page'], $offset);

        /*         * ******** For Selected menu and sub-menu in left bar ************** */
        $output['pageName'] = $this->pageName;
        $output['subpageName'] = 'All Messages';

        /*         * ******** For Breadcrum ************** */
        $output['breadcrum1'] = $this->breadcrum1;
        $output['breadcrum2'] = $this->breadcrum2;
        $output['breadcrum3'] = '';

        $output['breadcrum2_url'] = $this->breadcrum2_url;
        $output['breadcrum3_url'] = $this->breadcrum3_url;

        $this->load->view($this->config->item('adminfolder') . '/header', $output);
        $this->load->view($this->config->item('adminfolder') . '/request_list');
        $this->load->view($this->config->item('adminfolder') . '/footer');
    }

    function doTask($task, $id) {
        $output['task'] = $task;
        if (!empty($_POST)) {
            $conditions = $params = array();
            $conditions['id'] =$id;
            $params['table'] = 'tbl_maintenance_reports';
            $this->common_model->delete($conditions, $params);
            $this->session->set_userdata('SUCCESS_MESSAGE', 'Selected record has been deleted successfully');
            $redirect_url = $_SERVER['HTTP_REFERER'];
            if ($redirect_url)
                redirect($redirect_url);
            else
                redirect(site_url('admin/reports'));
        }
        $this->load->view($this->config->item('adminfolder') . '/active_inactive_delete', $output);
    }

    #================= Paging ==========================================================#

    protected function paging($config) {
        $total_pages = ceil($config['total_rows'] / $config['per_page']);

        $total_pages = ceil($config['total_rows'] / $config['per_page']);
        $start = max($config['currentpage'] - intval($config['per_page'] / 2), 1);
        $end = $start + $config['per_page'] - 1;

        if ($config['currentpage'] == 1) {
            if ($config['total_rows'] > $config['per_page']) {
                $showing = '1-' . $config['per_page'];
            } else {
                $showing = '1-' . $config['total_rows'];
            }
        } else {
            $showing = ((($config['currentpage'] - 1) * $config['per_page']) + 1) . '-';
            if (($config['currentpage'] * $config['per_page']) > $config['total_rows']) {
                $showing.=$config['total_rows'];
            } else {
                $showing.=($config['currentpage'] * $config['per_page']);
            }
        }

        $output = '<ul class="pagination">';

        if ($config['currentpage'] > 1) {
            $output.='<li class=""><a href="' . $config['base_url'] . ($config['currentpage'] - 1) . '"> <i class="fa fa-angle-double-left"></i> </a></li>';
        } else {
            $output.='<li class="prev disabled "><a> <i class="fa fa-angle-double-left"></i> </a></li>';
        }

        for ($i = $start; $i <= $end && $i <= $total_pages; $i++) {
            if ($i == $config['currentpage']) {
                $output .= '<li class="active"><a>' . $i . '</a></li>';
            } else {
                $output .='<li><a href="' . $config['base_url'] . $i . '">' . $i . '</a></li>';
            }
        }
        $output .='</li>';

        if ($total_pages > $config['currentpage']) {
            $output.='<li class="next"><a href="' . $config['base_url'] . ($config['currentpage'] + 1) . '"> <i class="fa fa-angle-double-right"></i></a></li>';
        } else {
            $output.='<li class="next disabled"><a > <i class="fa fa-angle-double-right"></i> </a></li>';
        }
        $output.='</ul>';
        return $output;
    }

}
