<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Send_email extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->pageName = 'Email Settings';
        $this->breadcrum1 = 'Email Settings';
        $this->breadcrum2 = 'All Sent Mails';
        $this->breadcrum3 = 'Send Mail';
        $this->breadcrum4 = '';
        $this->breadcrum2_url = site_url('admin/promotional/emails');
        $this->breadcrum3_url = site_url('admin/promotional/emails/send');
        $this->breadcrum4_url = '';
        $this->load->model('member_model');
        $this->load->model('sent_mail_history_model');
        $this->common_model->checkAdminLogin();
        $this->js();
    }

    private function js() {
        $config[] = 'ckeditor/ckeditor.js';
        $this->config->set_item('adminjs', $config);
    }

    public function index($currentPage = NULL) {
        $output['title'] = 'Sent Mails';

        if (!empty($_POST)) {
            $redirect_url = $_SERVER['HTTP_REFERER'];
            $task = $this->input->post('perform_task');
            $ids = $this->input->post('checkIds');
            if (sizeof($ids) > 0) {
                $message = $this->sent_mail_history_model->performMultipleTasks($task, $ids);
                if ($message) {
                    $this->session->set_userdata('SUCCESS_MESSAGE', $message);
                } else {
                    $message = "Oops something is going wrong there...try again later.";
                    $this->session->set_userdata('ERROR_MESSAGE', $message);
                }
            } else {
                $message = "Oops something is going wrong there...try again later.";
                $this->session->set_userdata('ERROR_MESSAGE', $message);
            }
            if ($redirect_url)
                redirect($redirect_url);
            else
                redirect(site_url('admin/promotional/emails'));
        }

        $this->load->library('pagination');
        $config['base_url'] = base_url() . $this->config->config['adminfolder'] . '/promotional/emails/';
        $config['total_rows'] = $this->sent_mail_history_model->getCountAllRecordsForAdmin('');
        $config['per_page'] = $this->config->item('page_size_admin');
        $this->pagination->initialize($config);
        $currentPage = (!$currentPage) ? 1 : $currentPage;
        $config['currentpage'] = $currentPage;
        if ($currentPage != "")
            $offset = ($currentPage - 1) * $config['per_page'];
        if ($config['total_rows'] > 0)
            $output['paging'] = $this->paging($config);

        $output['total'] = $this->sent_mail_history_model->getCountAllRecordsForAdmin('');
        $output['records'] = $this->sent_mail_history_model->getAllRecordsForAdmin($config['per_page'], $offset);


        /*         * ******** For Selected menu and sub-menu in left bar ************** */
        $output['pageName'] = $this->pageName;
        $output['subpageName'] = 'All Sent Mails';

        /*         * ******** For Breadcrum ************** */
        $output['breadcrum1'] = $this->breadcrum1;
        $output['breadcrum2'] = $this->breadcrum2;
        $output['breadcrum3'] = '';

        $output['breadcrum2_url'] = $this->breadcrum2_url;
        $output['breadcrum3_url'] = $this->breadcrum3_url;

        $this->load->view($this->config->item('adminfolder') . '/header', $output);
        $this->load->view($this->config->item('adminfolder') . '/sent_mails_list');
        $this->load->view($this->config->item('adminfolder') . '/footer');
    }

    function view($id) {
        $output['records'] = $this->sent_mail_history_model->getRecordById($id);
        $this->load->view($this->config->item('adminfolder') . '/sent_mail_view', $output);
    }

    public function sendEmail() {
        $this->load->model('mailsending_model');

        $output['title'] = 'Send Mail';

        $emaillistMember = $this->member_model->getAllRecordsForAdminByType('', 'Active', 'all', 'all');
        $output['allMemberList'] = $emaillistMember;
        if (!empty($_POST)) {
            $failure = FALSE;
            $this->form_validation->set_rules('subject', 'Subject', 'trim|required');
            $this->form_validation->set_rules('message', 'Message', 'trim|required');
            $temp = true;
            if (isset($_POST['SelectedEmail'])) {
                for ($i = 0; isset($_POST['SelectedEmail'][$i]); $i++) {
                    if ($_POST['SelectedEmail'][$i] == '') {
                        $temp = false;
                        $failure = true;
                    }
                }
            } else {
                $temp = false;
                $errorMsg.= 'You should select at least one mail id to send Mail.';
                $failure = true;
            }


            if ($this->form_validation->run() && $temp) {
                for ($i = 0; isset($_POST['SelectedEmail'][$i]); $i++) {
                    $this->mailsending_model->sendMailToRegistereduser($_POST['SelectedEmail'][$i]);
                    $this->sent_mail_history_model->addEntryToMailHistory($_POST['SelectedEmail'][$i]);
                }
                $success_message = 'Mail has been sent to Selected Members.';
            } else {
                $errorMsg.= validation_errors();
                $failure = true;
            }
            if ($this->input->is_ajax_request()) {
                if ($failure) {
                    $data['success'] = false;
                    $data['error_message'] = $errorMsg;
                } else {
                    $data['success'] = true;
                    $data['url'] = site_url($this->config->item('adminfolder') . '/promotional/emails');
                    $data['resetForm'] = false;
                    $data['success_message'] = $success_message;
                }
                $data['scrollToThisForm'] = true;
                echo json_encode($data);
                die;
            }
        }

        /*         * ******** For Selected menu and sub-menu in left bar ************** */
        $output['pageName'] = $this->pageName;
        $output['subpageName'] = "Send Mail";

        /*         * ******** For Breadcrum ************** */
        $output['breadcrum1'] = $this->breadcrum1;
        $output['breadcrum2'] = $this->breadcrum2;
        $output['breadcrum3'] = $this->breadcrum3;

        $output['breadcrum2_url'] = $this->breadcrum2_url;
        $output['breadcrum3_url'] = '';

        $this->load->view($this->config->config['adminfolder'] . '/header', $output);
        $this->load->view($this->config->config['adminfolder'] . '/sent_mail');
        $this->load->view($this->config->config['adminfolder'] . '/footer');
    }

    function filter($filterKey = NULL, $sort_by = NULL, $currentPage = NULL) {
        $output['title'] = 'Sent Mails';

        $filterKey = urldecode($filterKey);
        if ($filterKey != 'NULL')
            $output['filterKey'] = $filterKey;
        if ($show_me != 'NULL')
            $output['show_me'] = $show_me;
        if ($sort_by != 'NULL')
            $output['sort_by'] = $sort_by;


        //~ if($show_me!='NULL' || $sort_by!='NULL')
        //~ $output['advanceSearch']     = 'Yes';

        if (!empty($_POST)) {
            $redirect_url = $_SERVER['HTTP_REFERER'];
            $task = $this->input->post('perform_task');
            $ids = $this->input->post('checkIds');
            if (sizeof($ids) > 0) {
                $message = $this->sent_mail_history_model->performMultipleTasks($task, $ids);
                if ($message) {
                    $this->session->set_userdata('SUCCESS_MESSAGE', $message);
                } else {
                    $message = "Oops something is going wrong there...try again later.";
                    $this->session->set_userdata('ERROR_MESSAGE', $message);
                }
            } else {
                $message = "Oops something is going wrong there...try again later.";
                $this->session->set_userdata('ERROR_MESSAGE', $message);
            }
            if ($redirect_url)
                redirect($redirect_url);
            else
                redirect(site_url('admin/promotional/emails'));
        }

        $filterKey = $filterKey;
        if ($filterKey != 'NULL')
            $output['filterKey'] = $filterKey;

        $this->load->library('pagination');
        $config['base_url'] = base_url() . $this->config->config['adminfolder'] . '/promotional/emails/filter/' . $filterKey . '/' . $sort_by . '/';
        $config['total_rows'] = $this->sent_mail_history_model->getCountFilterAllRecords($filterKey, $sort_by);
        $config['per_page'] = $this->config->item('page_size_admin');
        $this->pagination->initialize($config);
        $currentPage = (!$currentPage) ? 1 : $currentPage;
        $config['currentpage'] = $currentPage;
        if ($currentPage != "")
            $offset = ($currentPage - 1) * $config['per_page'];
        if ($config['total_rows'] > 0)
            $output['paging'] = $this->paging($config);

        $output['total'] = $this->sent_mail_history_model->getCountAllRecordsForAdmin('');
        $output['records'] = $this->sent_mail_history_model->getFilterAllRecords($filterKey, $sort_by, $config['per_page'], $offset);

        /*         * ******** For Selected menu and sub-menu in left bar ************** */
        $output['pageName'] = $this->pageName;
        $output['subpageName'] = 'All Sent Mails';

        /*         * ******** For Breadcrum ************** */
        $output['breadcrum1'] = $this->breadcrum1;
        $output['breadcrum2'] = $this->breadcrum2;
        $output['breadcrum3'] = '';

        $output['breadcrum2_url'] = $this->breadcrum2_url;
        $output['breadcrum3_url'] = $this->breadcrum3_url;

        $this->load->view($this->config->item('adminfolder') . '/header', $output);
        $this->load->view($this->config->item('adminfolder') . '/sent_mails_list');
        $this->load->view($this->config->item('adminfolder') . '/footer');
    }

    function doTask($task, $id) {
        $output['task'] = $task;
        if (!empty($_POST)) {
            $message = $this->sent_mail_history_model->performTask($task, $id);
            $this->session->set_userdata('SUCCESS_MESSAGE', $message);
            $redirect_url = $_SERVER['HTTP_REFERER'];
            if ($redirect_url)
                redirect($redirect_url);
            else
                redirect(site_url('admin/promotional/emails'));
        }
        $this->load->view($this->config->item('adminfolder') . '/active_inactive_delete', $output);
    }

    #================= Paging ==========================================================#

    protected function paging($config) {
        $total_pages = ceil($config['total_rows'] / $config['per_page']);

        $total_pages = ceil($config['total_rows'] / $config['per_page']);
        $start = max($config['currentpage'] - intval($config['per_page'] / 2), 1);
        $end = $start + $config['per_page'] - 1;

        if ($config['currentpage'] == 1) {
            if ($config['total_rows'] > $config['per_page']) {
                $showing = '1-' . $config['per_page'];
            } else {
                $showing = '1-' . $config['total_rows'];
            }
        } else {
            $showing = ((($config['currentpage'] - 1) * $config['per_page']) + 1) . '-';
            if (($config['currentpage'] * $config['per_page']) > $config['total_rows']) {
                $showing.=$config['total_rows'];
            } else {
                $showing.=($config['currentpage'] * $config['per_page']);
            }
        }

        $output = '<ul class="pagination">';

        if ($config['currentpage'] > 1) {
            $output.='<li class=""><a href="' . $config['base_url'] . ($config['currentpage'] - 1) . '"> <i class="fa fa-angle-double-left"></i> </a></li>';
        } else {
            $output.='<li class="prev disabled "><a> <i class="fa fa-angle-double-left"></i> </a></li>';
        }

        for ($i = $start; $i <= $end && $i <= $total_pages; $i++) {
            if ($i == $config['currentpage']) {
                $output .= '<li class="active"><a>' . $i . '</a></li>';
            } else {
                $output .='<li><a href="' . $config['base_url'] . $i . '">' . $i . '</a></li>';
            }
        }
        $output .='</li>';

        if ($total_pages > $config['currentpage']) {
            $output.='<li class="next"><a href="' . $config['base_url'] . ($config['currentpage'] + 1) . '"> <i class="fa fa-angle-double-right"></i></a></li>';
        } else {
            $output.='<li class="next disabled"><a > <i class="fa fa-angle-double-right"></i> </a></li>';
        }
        $output.='</ul>';
        return $output;
    }

}
