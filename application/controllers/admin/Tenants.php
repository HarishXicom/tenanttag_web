<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Tenants extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->pageName = 'Member Manager';
        $this->type = 'tenant';
        $this->breadcrum1 = 'Member Manager';
        $this->breadcrum2 = 'All Tenants';
        $this->breadcrum3 = 'Add Tenant';
        $this->breadcrum4 = 'Edit Tenant';
        $this->breadcrum2_url = site_url('admin/tenants');
        $this->breadcrum3_url = site_url('admin/tenants/add');
        $this->breadcrum4_url = site_url('admin/tenants/edit');
        $this->load->model('member_model');
        $this->load->model('properties_model');
        $this->load->model('city_model');
        $this->load->model('region_model');
        $this->load->model('country_model');
        $this->common_model->checkAdminLogin();
    }

    public function index($currentPage = NULL) {
        $output['title'] = 'Tenants';
        

        if (!empty($_POST)) {
            $redirect_url = $_SERVER['HTTP_REFERER'];
            $task = $this->input->post('perform_task');
            $ids = $this->input->post('checkIds');
            if (sizeof($ids) > 0) {
                $message = $this->member_model->performMultipleTasks($task, $ids);
                if ($message) {
                    $this->session->set_userdata('SUCCESS_MESSAGE', $message);
                } else {
                    $message = "Oops something is going wrong there...try again later.";
                    $this->session->set_userdata('ERROR_MESSAGE', $message);
                }
            } else {
                $message = "Oops something is going wrong there...try again later.";
                $this->session->set_userdata('ERROR_MESSAGE', $message);
            }
            if ($redirect_url)
                redirect($redirect_url);
            else
                redirect(site_url('admin/tenants'));
        }
        $this->load->library('pagination');

        $config['base_url'] = base_url() . $this->config->config['adminfolder'] . '/tenants/';
        $config['total_rows'] = $this->member_model->getCountAllRecordsForAdminByType($this->type, '');
        $config['per_page'] = $this->config->item('page_size_admin');
        $this->pagination->initialize($config);
        $currentPage = (!$currentPage) ? 1 : $currentPage;
        $config['currentpage'] = $currentPage;
        if ($currentPage != "")
            $offset = ($currentPage - 1) * $config['per_page'];
        if ($config['total_rows'] > 0)
            $output['paging'] = $this->paging($config);


        $output['total'] = $this->member_model->getCountAllRecordsForAdminByType($this->type, '');
        $output['totalActive'] = $this->member_model->getCountAllRecordsForAdminByType($this->type, 'Active');
        $output['totalInactive'] = $this->member_model->getCountAllRecordsForAdminByType($this->type, 'Inactive');
        $output['totalDeleted'] = $this->member_model->getCountAllRecordsForAdminByType($this->type, 'Deleted');
        $output['records'] = $this->member_model->getAllRecordsForAdminByType($this->type, '', $config['per_page'], $offset);
        //echo '<pre>';print_r( $output); die;
        //$output['allMembers']		=	$this->member_model->getAllMembersByType($this->type);

        /*         * ******** For Selected menu and sub-menu in left bar ************** */
        $output['pageName'] = $this->pageName;
        $output['subpageName'] = 'All Tenants';

        /*         * ******** For Breadcrum ************** */
        $output['breadcrum1'] = $this->breadcrum1;
        $output['breadcrum2'] = $this->breadcrum2;
        $output['breadcrum3'] = '';

        $output['breadcrum2_url'] = $this->breadcrum2_url;
        $output['breadcrum3_url'] = $this->breadcrum3_url;

        $this->load->view($this->config->item('adminfolder') . '/header', $output);
        $this->load->view($this->config->item('adminfolder') . '/tenants_list');
        $this->load->view($this->config->item('adminfolder') . '/footer');
    }

    function add() {
        $this->load->model('mailsending_model');
        $output['title'] = 'Add Tenant';
        if (!empty($_POST)) {
            $this->form_validation->set_rules('first_name', 'First Name', 'trim|required');
            $this->form_validation->set_rules('last_name', 'Last Name', 'trim|required');
            //$this->form_validation->set_rules('company_name', 'Company Name', 'trim|required');
            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[tbl_members.email]');
            //  $this->form_validation->set_rules('password', 'Password', 'trim|required');
            //  $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'trim|required|matches[password]');
            $this->form_validation->set_rules('mobile', 'Mobile No', 'trim|required');
            $this->form_validation->set_rules('dob', 'Date Of Birth', 'trim|required');
            //  $this->form_validation->set_rules('address', 'Address', 'trim|required');
            ////  $this->form_validation->set_rules('country', 'Country', 'trim|required');
            //   $this->form_validation->set_rules('state', 'State', 'trim|required');
            $this->form_validation->set_rules('landlord', 'Property owner', 'trim|required');
            $this->form_validation->set_rules('property', 'Property', 'trim|required');
            $this->form_validation->set_rules('lease_start_date', 'Lease start date', 'trim|required');
            $this->form_validation->set_rules('lease_end_date', 'Lease end date', 'trim|required');
            $this->form_validation->set_rules('due_date', 'Due date', 'trim|required');
            $this->form_validation->set_rules('rent_amount', 'Rent Amount', 'trim|required|is_numeric|greater_than[0]');
            //  $this->form_validation->set_rules('late_fee', 'Late Fee', 'trim|required|is_numeric|greater_than[0]');
            //$this->form_validation->set_rules('city', 'City', 'trim|required');
            //~ if (empty($_FILES['profile_pic']['name']))
            //~ {
            //~ $this->form_validation->set_rules('profile_pic', 'Profile Image', 'required');
            //~ }
            if ($this->form_validation->run()) {
                $failure = FALSE;
                $config['upload_path'] = './assets/uploads/member/';
                $config['allowed_types'] = 'jpeg|jpg|png';
                $config['encrypt_name'] = TRUE;
                if (isset($_FILES) && !empty($_FILES['profile_pic']['name'])) {
                    $this->load->library('upload', $config);
                    //$this->upload->initialize($config);
                    if (!$this->upload->do_upload('profile_pic')) {
                        $error = $this->upload->display_errors();
                        $error_message.=$error;
                        $failure = TRUE;
                    } else {
                        $data = array('upload_data' => $this->upload->data());
                    }
                }
                if (isset($failure) && $failure != TRUE) {
                    $file = '';
                    if (isset($data['upload_data']['file_name']) && $data['upload_data']['file_name'] != '') {
                        $file = $data['upload_data']['file_name'];
                    }
                    $mem_id = $this->member_model->add($file);
                    if ($mem_id)
                        $this->mailsending_model->registrationEmail($mem_id);
                    $success_message = 'Tenant added successfully';
                }
            }
            else {

                $error_message = validation_errors();
                $failure = true;
            }
            if ($this->input->is_ajax_request()) {
                if (isset($failure) && $failure == TRUE) {
                    $data['success'] = false;
                    $data['error_message'] = $error_message;
                } else {
                    $data['success'] = true;
                    $data['success_message'] = $success_message;
                    $data['url'] = site_url('admin/tenants');
                }
                $data['scrollToElement'] = true;
                echo json_encode($data);
                die;
            }
        }
        /*         * ******** For Selected menu and sub-menu in left bar ************** */
        $output['pageName'] = $this->pageName;
        $output['subpageName'] = 'Add Tenant';

        /*         * ******** For Breadcrum ************** */
        $output['breadcrum1'] = $this->breadcrum1;
        $output['breadcrum2'] = $this->breadcrum2;
        $output['breadcrum3'] = $this->breadcrum3;

        $output['breadcrum2_url'] = $this->breadcrum2_url;
        $output['breadcrum3_url'] = '';

        $output['allCity'] = $this->city_model->getAllCities();
        $output['allRegion'] = $this->region_model->getAllRegions();
        $output['allcountry'] = $this->country_model->getAllCountries();

        $output['allLandlords'] = $this->member_model->getAllRecordsForAdminByType('landlord', 'Active', 'all', 'all');

        $this->load->view($this->config->item('adminfolder') . '/header', $output);
        $this->load->view($this->config->item('adminfolder') . '/add_tenant');
        $this->load->view($this->config->item('adminfolder') . '/footer');
    }

    function edit($id) {
        $output['title'] = 'Edit Tenant';
        if (!empty($_POST)) {
            $this->form_validation->set_rules('first_name', 'First Name', 'trim|required');
            $this->form_validation->set_rules('last_name', 'Last Name', 'trim|required');
            //$this->form_validation->set_rules('company_name', 'Company Name', 'trim|required');
            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[tbl_members.email]');
            $this->form_validation->set_rules('password', 'Password', 'trim|required');
            $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'trim|required|matches[password]');
            $this->form_validation->set_rules('mobile', 'Mobile No', 'trim|required');
            $this->form_validation->set_rules('dob', 'Date Of Birth', 'trim|required');
            $this->form_validation->set_rules('address', 'Address', 'trim|required');
            $this->form_validation->set_rules('country', 'Country', 'trim|required');
            $this->form_validation->set_rules('state', 'State', 'trim|required');
            $this->form_validation->set_rules('landlord', 'Property owner', 'trim|required');
            $this->form_validation->set_rules('property', 'Property', 'trim|required');
            $this->form_validation->set_rules('lease_start_date', 'Lease start date', 'trim|required');
            $this->form_validation->set_rules('lease_end_date', 'Lease end date', 'trim|required');
            $this->form_validation->set_rules('due_date', 'Due date', 'trim|required');
            $this->form_validation->set_rules('rent_amount', 'Rent Amount', 'trim|required|is_numeric|greater_than[0]');
            $this->form_validation->set_rules('late_fee', 'Late Fee', 'trim|required|is_numeric|greater_than[0]');
            if ($this->form_validation->run()) {
                $config['upload_path'] = './assets/uploads/member/';
                $config['allowed_types'] = 'jpeg|jpg|png';
                $config['encrypt_name'] = TRUE;
                if (!empty($_FILES['profile_pic']['name'])) {
                    //$this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    if (!$this->upload->do_upload('profile_pic')) {
                        $error = $this->upload->display_errors();
                        $error_message.=$error;
                        $failure = TRUE;
                    } else {
                        $data = array('upload_data' => $this->upload->data());
                    }
                }
                if (!$failure) {
                    $this->member_model->update($id, $data['upload_data']['file_name']);
                    $success_message = 'Tenant record updated successfully';
                }
            } else {

                $error_message = validation_errors();
                $failure = true;
            }
            if ($this->input->is_ajax_request()) {
                if ($failure) {
                    $data['success'] = false;
                    $data['error_message'] = $error_message;
                } else {
                    $data['success'] = true;
                    $data['success_message'] = $success_message;
                    $data['url'] = site_url('admin/tenants');
                }
                $data['scrollToElement'] = true;
                echo json_encode($data);
                die;
            }
        }
        /*         * ******** For Selected menu and sub-menu in left bar ************** */
        $output['pageName'] = $this->pageName;
        $output['subpageName'] = 'Edit Tenant';

        /*         * ******** For Breadcrum ************** */
        $output['breadcrum1'] = $this->breadcrum1;
        $output['breadcrum2'] = $this->breadcrum2;
        $output['breadcrum3'] = '';
        $output['breadcrum4'] = $this->breadcrum4;

        $output['breadcrum2_url'] = $this->breadcrum2_url;
        $output['breadcrum3_url'] = $this->breadcrum3_url;
        $output['breadcrum4_url'] = '';

        $output['memberDetail'] = $memberDetail = $this->member_model->getMemberRecordByIdAndType($id, $this->type);

        /*         * *********  get detail of owned property by tenant id ********************************* */
        $output['ownedPropertyDetail'] = $ownedPropertyDetail = $this->properties_model->getDetailByTenantId($memberDetail->mem_id);
        /*         * ************************************************************************************** */
        /*         * *********  get all properties by owner id ********************************* */
        $output['allProperties'] = $memberDetail = $this->properties_model->getAllPropertiesByLandlordId($ownedPropertyDetail->owner_id);
        /*         * ************************************************************************************** */
        $output['allCity'] = $this->city_model->getAllCityByStateId($memberDetail->state_id);
        $output['allRegion'] = $this->region_model->getAllRegionsByCountryId($memberDetail->country_id);
        $output['allcountry'] = $this->country_model->getAllCountries();

        $output['action'] = 'edit';

        $output['allLandlords'] = $this->member_model->getAllRecordsForAdminByType('landlord', 'Active');

        $this->load->view($this->config->item('adminfolder') . '/header', $output);
        $this->load->view($this->config->item('adminfolder') . '/add_tenant');
        $this->load->view($this->config->item('adminfolder') . '/footer');
    }

    function view($memId) {
        $output['adminSubPage'] = 'Tenants';
        $records = $this->member_model->getMemberRecordByIdAndType($memId, $this->type);
        $conditions = $params = array();
        $params['complex'] = true;
        $params['single_row'] = true;
        $conditions['tables'] = array(
            'lease' => 'tbl_properties_lease_detail',
            'property' => 'tbl_properties',
            'state' => 'tbl_region'
        );
        $conditions['table'] = 'lease';
        $conditions['on']['property'] = array(
            'sign' => '=',
            'column' => 'prop_id',
            'alias_column' => 'property_id',
            'alias_other' => 'lease',
        );
        $conditions['on']['state'] = array(
            'sign' => '=',
            'column' => 'region_id',
            'alias_column' => 'state_id',
            'alias_other' => 'property',
        );
        $conditions['where']['tenant_id']['alias'] = 'lease';
        $conditions['where']['tenant_id']['value'] = $memId;

        $params['single_row'] = TRUE;
        $params['fields'] = array('property.*', 'state.region_name', 'lease.lease_start_date', 'lease.lease_end_date', 'lease.rent_amount');
        $info = $this->common_model->get_data($conditions, $params);
        $records->property_info = $info;

        $output['records'] = $records;
        //print_R($output); exit;
        $this->load->view($this->config->item('adminfolder') . '/tenant_view', $output);
    }

    function filter($filterKey = NULL, $show_me = NULL, $sort_by = NULL, $currentPage = NULL) {
        $output['title'] = 'Tenants';

        $filterKey = urldecode($filterKey);
        if ($filterKey != 'NULL')
            $output['filterKey'] = $filterKey;
        if ($show_me != 'NULL')
            $output['show_me'] = $show_me;
        if ($sort_by != 'NULL')
            $output['sort_by'] = $sort_by;


        if ($show_me != 'NULL' || $sort_by != 'NULL')
            $output['advanceSearch'] = 'Yes';

        if (!empty($_POST)) {
            $redirect_url = $_SERVER['HTTP_REFERER'];
            $task = $this->input->post('perform_task');
            $ids = $this->input->post('checkIds');
            if (sizeof($ids) > 0) {
                $message = $this->member_model->performMultipleTasks($task, $ids);
                if ($message) {
                    $this->session->set_userdata('SUCCESS_MESSAGE', $message);
                } else {
                    $message = "Oops something is going wrong there...try again later.";
                    $this->session->set_userdata('ERROR_MESSAGE', $message);
                }
            } else {
                $message = "Oops something is going wrong there...try again later.";
                $this->session->set_userdata('ERROR_MESSAGE', $message);
            }
            if ($redirect_url)
                redirect($redirect_url);
            else
                redirect(site_url('admin/tenants'));
        }

        $this->load->library('pagination');

        $config['base_url'] = base_url() . $this->config->config['adminfolder'] . '/tenants/filter/' . $filterKey . '/' . $show_me . '/' . $sort_by . '/';
        $config['total_rows'] = $this->member_model->getCountFilterAllRecords($this->type, $filterKey, $show_me, $sort_by);
        $config['per_page'] = $this->config->item('page_size_admin');
        $this->pagination->initialize($config);
        $currentPage = (!$currentPage) ? 1 : $currentPage;
        $config['currentpage'] = $currentPage;
        if ($currentPage != "")
            $offset = ($currentPage - 1) * $config['per_page'];
        if ($config['total_rows'] > 0)
            $output['paging'] = $this->paging($config);

        $output['totalActive'] = $this->member_model->getCountAllRecordsForAdminByType($this->type, 'Active');
        $output['total'] = $this->member_model->getCountAllRecordsForAdminByType($this->type, '');
        $output['totalInactive'] = $this->member_model->getCountAllRecordsForAdminByType($this->type, 'Inactive');
        $output['records'] = $this->member_model->getFilterAllRecords($this->type, $filterKey, $show_me, $sort_by, $config['per_page'], $offset);
        $output['totalDeleted'] = $this->member_model->getCountAllRecordsForAdminByType($this->type, 'Deleted');

        //print_r($output); exit;
        /*         * ******** For Selected menu and sub-menu in left bar ************** */
        $output['pageName'] = $this->pageName;
        $output['subpageName'] = 'All Tenants';

        /*         * ******** For Breadcrum ************** */
        $output['breadcrum1'] = $this->breadcrum1;
        $output['breadcrum2'] = $this->breadcrum2;
        $output['breadcrum3'] = '';

        $output['breadcrum2_url'] = $this->breadcrum2_url;
        $output['breadcrum3_url'] = $this->breadcrum3_url;

        $this->load->view($this->config->item('adminfolder') . '/header', $output);
        $this->load->view($this->config->item('adminfolder') . '/tenants_list');
        $this->load->view($this->config->item('adminfolder') . '/footer');
    }

    public function sent_messages($memId) {
        $output['sent_msgs'] = $this->getSMSbyType('tbl_sms_logs', $memId);
        //echo '<pre>';print_r($output['sent_msgs']);die;
        $this->load->view($this->config->item('adminfolder') . '/landlord_sms', $output);
    }

    public function getSMSbyType($type, $memId) {

        $params = $conditions = array();
        $params['complex'] = true;
        $conditions['tables'] = array(
            'sms' => $type,
            'tenant' => 'tbl_members',
            'property' => 'tbl_properties',
        );
        $conditions['table'] = 'sms';
        $conditions['on']['tenant'] = array(
            'sign' => '=',
            'column' => 'mem_id',
            'alias_column' => 'tenant_id',
            'alias_other' => 'sms',
        );
        $conditions['on']['property'] = array(
            'sign' => '=',
            'column' => 'prop_id',
            'alias_column' => 'property_id',
            'alias_other' => 'sms',
        );
        $conditions['where']['owner_id']['alias'] = 'sms';
        $conditions['where']['owner_id']['value'] = $memId;
        $params['fields'] = array(
            'property.prop_id', 'sms.*',
            'property.address1', 'property.address2', 'property.city', 'tenant.first_name', 'tenant.last_name');
        $params['order_by'] = 'sms.id';
        $sms_data = $this->common_model->get_data($conditions, $params);

        return $sms_data;
    }

    function doTask($task, $id) {
        $output['task'] = $task;
        if (!empty($_POST)) {
            if ($task == 'delete') {
                $message = $this->member_model->deleteTenant($task, $id);
            } else {
                $message = $this->member_model->performTask($task, $id);
            }
            $this->session->set_userdata('SUCCESS_MESSAGE', $message);
            $redirect_url = $_SERVER['HTTP_REFERER'];
            if ($redirect_url)
                redirect($redirect_url);
            else
                redirect(site_url('admin/tenants'));
        }
        $this->load->view($this->config->item('adminfolder') . '/active_inactive_delete', $output);
    }

    #================= Paging ==========================================================#

    protected function paging($config) {
        $total_pages = ceil($config['total_rows'] / $config['per_page']);

        $total_pages = ceil($config['total_rows'] / $config['per_page']);
        $start = max($config['currentpage'] - intval($config['per_page'] / 2), 1);
        $end = $start + $config['per_page'] - 1;

        if ($config['currentpage'] == 1) {
            if ($config['total_rows'] > $config['per_page']) {
                $showing = '1-' . $config['per_page'];
            } else {
                $showing = '1-' . $config['total_rows'];
            }
        } else {
            $showing = ((($config['currentpage'] - 1) * $config['per_page']) + 1) . '-';
            if (($config['currentpage'] * $config['per_page']) > $config['total_rows']) {
                $showing.=$config['total_rows'];
            } else {
                $showing.=($config['currentpage'] * $config['per_page']);
            }
        }

        $output = '<ul class="pagination">';

        if ($config['currentpage'] > 1) {
            $output.='<li class=""><a href="' . $config['base_url'] . ($config['currentpage'] - 1) . '"> <i class="fa fa-angle-double-left"></i> </a></li>';
        } else {
            $output.='<li class="prev disabled "><a> <i class="fa fa-angle-double-left"></i> </a></li>';
        }

        for ($i = $start; $i <= $end && $i <= $total_pages; $i++) {
            if ($i == $config['currentpage']) {
                $output .= '<li class="active"><a>' . $i . '</a></li>';
            } else {
                $output .='<li><a href="' . $config['base_url'] . $i . '">' . $i . '</a></li>';
            }
        }
        $output .='</li>';

        if ($total_pages > $config['currentpage']) {
            $output.='<li class="next"><a href="' . $config['base_url'] . ($config['currentpage'] + 1) . '"> <i class="fa fa-angle-double-right"></i></a></li>';
        } else {
            $output.='<li class="next disabled"><a > <i class="fa fa-angle-double-right"></i> </a></li>';
        }
        $output.='</ul>';
        return $output;
    }
     public function exportTenants() {
        $this->load->dbutil();
        $this->load->helper('file');
        $this->load->helper('download');
        $data = $this->properties_model->getTenantCSV();
        $new_report = $this->dbutil->csv_from_result($data);
        $file = "Tenants_" . date('Y-m-d H:i:s') . ".csv";
        force_download($file, $new_report);
    }

}
