<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Web_config extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->pageName = 'General Settings';
        $this->breadcrum1 = 'General Settings';
        $this->load->model('web_config_model');
        //$this->common_model->checkAdminLogin();	
    }

    function resources(){
        $output['title'] = 'Resources';
        if (!empty($_FILES)) {
            $failure = FALSE;

            @mkdir('assets/uploads/resources', 0777, true);
            @chmod('assets/uploads/resources', 0777);
            $config['upload_path'] = './assets/uploads/resources/';
            $config['allowed_types'] = 'jpeg|jpg|png|pdf|doc|xlsx|gif';
            $config['encrypt_name'] = TRUE;
            if (!empty($_FILES['file']['name'])) {
                $this->load->library('upload', $config);
                //$this->upload->initialize($config);
                if (!$this->upload->do_upload('file')) {
                    $error = $this->upload->display_errors();
                    $error_message =$error;
                    $failure = TRUE;
                } else {
                    $data = array('upload_data' => $this->upload->data());
                }
            }
            if (!$failure) {
                $file='';
                if(isset($data['upload_data']['file_name'])){
                 $file = $data['upload_data']['file_name'];   
                }
                if($this->input->post('doc_name')){
                    $doc_name = $this->input->post('doc_name');
                }else{
                    $doc_name = 'Resource';
                }
                $this->web_config_model->update_resources($file,$doc_name);
                $success_message = 'Resources updated successfully';
            }

            if ($this->input->is_ajax_request()) {
                if ($failure) {
                    $data['success'] = false;
                    $data['error_message'] = $error_message;
                } else {
                    $data['success'] = true;
                    $data['success_message'] = $success_message;
                    $data['url'] = site_url('admin/resources');
                }
                $data['scrollToElement'] = true;
                echo json_encode($data);
                die;
            }
        }
        /*         * ******** For Selected menu and sub-menu in left bar ************** */
        $output['pageName'] = $this->pageName;
        $output['subpageName'] = 'Resources';

        /*         * ******** For Breadcrum ************** */
        $output['breadcrum1'] = $this->breadcrum1;
        $output['breadcrum2'] = 'Resources';
        $output['breadcrum2_url'] = site_url('admin/resources');

        $output['records'] = $resources = $this->common_model->getResources();

        $this->load->view($this->config->item('adminfolder') . '/header', $output);
        $this->load->view($this->config->item('adminfolder') . '/resources');
        $this->load->view($this->config->item('adminfolder') . '/footer');
    }

    public function delete_resources($id) {
        $conditions = $params = array();
        $conditions['id'] = $id;
        $params['table'] = 'tbl_resources_docs';
        $this->common_model->delete($conditions, $params);
        redirect(site_url('admin/resources'));    
    }

    function update_wesite_settings() {
        $output['title'] = 'Website Settings';
        if (!empty($_POST)) {
            $failure = FALSE;
            $this->form_validation->set_rules('site_name', 'Site Name', 'trim|required');
            $this->form_validation->set_rules('site_title', 'Site Title', 'trim|required');
           // $this->form_validation->set_rules('page_size_front', 'Page Size Fornt', 'trim|required|is_numeric|greater_than[0]');
          //  $this->form_validation->set_rules('page_size_admin', 'Page Size Admin', 'trim|required|is_numeric|greater_than[0]');
            if ($this->form_validation->run()) {

                @mkdir('assets/uploads/site_logo', 0777, true);
                @chmod('assets/uploads/site_logo', 0777);
                $config['upload_path'] = './assets/uploads/site_logo/';
                $config['allowed_types'] = 'jpeg|jpg|png';
                $config['encrypt_name'] = TRUE;
                if (!empty($_FILES['site_logo']['name'])) {
                    $this->load->library('upload', $config);
                    //$this->upload->initialize($config);
                    if (!$this->upload->do_upload('site_logo')) {
                        $error = $this->upload->display_errors();
                        $error_message.=$error;
                        $failure = TRUE;
                    } else {
                        $data = array('upload_data' => $this->upload->data());
                    }
                }
                if (!$failure) {
                    $file='';
                    if(isset($data['upload_data']['file_name'])){
                     $file = $data['upload_data']['file_name'];   
                    }
                    $this->web_config_model->update_website_settings(1, $file);
                    $success_message = 'Website Settings updated successfully';
                }
            } else {

                $error_message = validation_errors();
                $failure = true;
            }
            if ($this->input->is_ajax_request()) {
                if ($failure) {
                    $data['success'] = false;
                    $data['error_message'] = $error_message;
                } else {
                    $data['success'] = true;
                    $data['success_message'] = $success_message;
                    $data['url'] = site_url('admin/website-settings');
                }
                $data['scrollToElement'] = true;
                echo json_encode($data);
                die;
            }
        }
        /*         * ******** For Selected menu and sub-menu in left bar ************** */
        $output['pageName'] = $this->pageName;
        $output['subpageName'] = 'Website Settings';

        /*         * ******** For Breadcrum ************** */
        $output['breadcrum1'] = $this->breadcrum1;
        $output['breadcrum2'] = 'Website Settings';
        $output['breadcrum2_url'] = site_url('admin/website-settings');

        $output['recordDetail'] = $recordDetail = $this->web_config_model->getRecordById(1);

        $this->load->view($this->config->item('adminfolder') . '/header', $output);
        $this->load->view($this->config->item('adminfolder') . '/website_settings');
        $this->load->view($this->config->item('adminfolder') . '/footer');
    }

    function update_social_settings() {
        $output['title'] = 'Social Settings';
        if (!empty($_POST)) {
            $failure = FALSE;
            $this->form_validation->set_rules('facebook_url', 'Facebook Url', 'trim|required');
          //  $this->form_validation->set_rules('google_url', 'Google Url', 'trim|required');
            $this->form_validation->set_rules('twitter_url', 'Twitter Url', 'trim|required');
          //  $this->form_validation->set_rules('youtube_url', 'Youtube Url', 'trim|required');
            $this->form_validation->set_rules('pinterest_url', 'pinterest Url', 'trim|required');
            $this->form_validation->set_rules('tumblr_url', 'tumblr Url', 'trim|required');
            $this->form_validation->set_rules('instagram_url', 'instagram Url', 'trim|required');
            if ($this->form_validation->run()) {
                $this->web_config_model->update_social_settings(1);
                $success_message = 'Social Settings updated successfully';
            } else {

                $error_message = validation_errors();
                $failure = true;
            }
            if ($this->input->is_ajax_request()) {
                if ($failure) {
                    $data['success'] = false;
                    $data['error_message'] = $error_message;
                } else {
                    $data['success'] = true;
                    $data['success_message'] = $success_message;
                    $data['url'] = site_url('admin/social-settings');
                }
                $data['scrollToElement'] = true;
                echo json_encode($data);
                die;
            }
        }
        /*         * ******** For Selected menu and sub-menu in left bar ************** */
        $output['pageName'] = $this->pageName;
        $output['subpageName'] = 'Social Settings';

        /*         * ******** For Breadcrum ************** */
        $output['breadcrum1'] = $this->breadcrum1;
        $output['breadcrum2'] = 'Social Settings';
        $output['breadcrum2_url'] = site_url('admin/social-settings');

        $output['recordDetail'] = $recordDetail = $this->web_config_model->getRecordById(1);

        $this->load->view($this->config->item('adminfolder') . '/header', $output);
        $this->load->view($this->config->item('adminfolder') . '/social_settings');
        $this->load->view($this->config->item('adminfolder') . '/footer');
    }

    function update_email_settings() {
        $output['title'] = 'Email Settings';
        if (!empty($_POST)) {
            $failure = FALSE;
            $this->form_validation->set_rules('contact_email', 'Contact Email', 'trim|required|valid_email');
            $this->form_validation->set_rules('email_goes_from_name', 'Site emails goes from Name', 'trim|required');
            $this->form_validation->set_rules('email_goes_from_email', 'Site emails goes from Email', 'trim|required|valid_email');
            if ($this->form_validation->run()) {
                $this->web_config_model->update_email_settings(1);
                $success_message = 'Email Settings updated successfully';
            } else {

                $error_message = validation_errors();
                $failure = true;
            }
            if ($this->input->is_ajax_request()) {
                if ($failure) {
                    $data['success'] = false;
                    $data['error_message'] = $error_message;
                } else {
                    $data['success'] = true;
                    $data['success_message'] = $success_message;
                    $data['url'] = site_url('admin/email-settings');
                }
                $data['scrollToElement'] = true;
                echo json_encode($data);
                die;
            }
        }
        /*         * ******** For Selected menu and sub-menu in left bar ************** */
        $output['pageName'] = 'Email Settings';
        $output['subpageName'] = 'Email Settings';

        /*         * ******** For Breadcrum ************** */
        $output['breadcrum1'] = $this->breadcrum1;
        $output['breadcrum2'] = 'Email Settings';
        $output['breadcrum2_url'] = site_url('admin/email-settings');

        $output['recordDetail'] = $recordDetail = $this->web_config_model->getRecordById(1);

        $this->load->view($this->config->item('adminfolder') . '/header', $output);
        $this->load->view($this->config->item('adminfolder') . '/email_settings');
        $this->load->view($this->config->item('adminfolder') . '/footer');
    }

}
