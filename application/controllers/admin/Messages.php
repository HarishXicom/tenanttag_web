<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Messages extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->pageName = 'Messages';
        $this->breadcrum1 = 'Messages';
        $this->breadcrum2 = 'All Messages';
        $this->breadcrum3 = 'Add Message';
        $this->breadcrum4 = 'Edit Messages';
        $this->breadcrum2_url = site_url('admin/messages/sent_sms');
        $this->breadcrum3_url = site_url('admin/messages/add');
        $this->breadcrum4_url = site_url('admin/messages/edit');
        $this->load->model('messages_model');
        $this->common_model->checkAdminLogin();
        $this->js();
    }

    private function js() {
        $config[] = 'ckeditor/ckeditor.js';
        $this->config->set_item('adminjs', $config);
    }

    public function index($currentPage = NULL) {
        $output['title'] = 'Messages';

        if (!empty($_POST)) {

            $redirect_url = $_SERVER['HTTP_REFERER'];
            $task = $this->input->post('perform_task');
            $ids = $this->input->post('checkIds');
            if (sizeof($ids) > 0) {
                $message = $this->messages_model->performMultipleTasks($task, $ids);
                if ($message) {
                    $this->session->set_userdata('SUCCESS_MESSAGE', $message);
                } else {
                    $message = "Oops something is going wrong there...try again later.";
                    $this->session->set_userdata('ERROR_MESSAGE', $message);
                }
            } else {
                $message = "Oops something is going wrong there...try again later.";
                $this->session->set_userdata('ERROR_MESSAGE', $message);
            }
            if ($redirect_url)
                redirect($redirect_url);
            else
                redirect(site_url('admin/messages'));
        }

        $this->load->library('pagination');

        $config['base_url'] = base_url() . $this->config->config['adminfolder'] . '/messages/';
        $config['total_rows'] = $this->messages_model->getCountAllRecordsForAdmin('');
        $config['per_page'] = $this->config->item('page_size_admin');
        $this->pagination->initialize($config);
        $currentPage = (!$currentPage) ? 1 : $currentPage;
        $config['currentpage'] = $currentPage;
        if ($currentPage != "")
            $offset = ($currentPage - 1) * $config['per_page'];
        if ($config['total_rows'] > 0)
            $output['paging'] = $this->paging($config);


        $output['total'] = $this->messages_model->getCountAllRecordsForAdmin('');
        $output['totalActive'] = $this->messages_model->getCountAllRecordsForAdmin('Active');
        $output['totalInactive'] = $this->messages_model->getCountAllRecordsForAdmin('Inactive');
        $output['records'] = $this->messages_model->getAllRecordsForAdmin($config['per_page'], $offset);


        /*         * ******** For Selected menu and sub-menu in left bar ************** */
        $output['pageName'] = $this->pageName;
        $output['subpageName'] = 'All Messages';

        /*         * ******** For Breadcrum ************** */
        $output['breadcrum1'] = $this->breadcrum1;
        $output['breadcrum2'] = $this->breadcrum2;
        $output['breadcrum3'] = '';

        $output['breadcrum2_url'] = $this->breadcrum2_url;
        $output['breadcrum3_url'] = $this->breadcrum3_url;

        $this->load->view($this->config->item('adminfolder') . '/header', $output);
        $this->load->view($this->config->item('adminfolder') . '/messages_list');
        $this->load->view($this->config->item('adminfolder') . '/footer');
    }

    function add() {
        $output['title'] = 'Add Message';
        if (!empty($_POST)) {
            $failure = FALSE;
            $this->form_validation->set_rules('title', 'Title', 'trim|required');
            $this->form_validation->set_rules('description', 'Description', 'trim|required');
            if ($this->form_validation->run()) {

                $this->messages_model->add();
                $success_message = 'Message added successfully';
            } else {

                $error_message = validation_errors();
                $failure = true;
            }
            if ($this->input->is_ajax_request()) {
                if ($failure) {
                    $data['success'] = false;
                    $data['error_message'] = $error_message;
                } else {
                    $data['success'] = true;
                    $data['success_message'] = $success_message;
                    $data['url'] = site_url('admin/messages');
                }
                $data['scrollToElement'] = true;
                echo json_encode($data);
                die;
            }
        }
        /*         * ******** For Selected menu and sub-menu in left bar ************** */
        $output['pageName'] = $this->pageName;
        $output['subpageName'] = 'Add Message';

        /*         * ******** For Breadcrum ************** */
        $output['breadcrum1'] = $this->breadcrum1;
        $output['breadcrum2'] = $this->breadcrum2;
        $output['breadcrum3'] = $this->breadcrum3;

        $output['breadcrum2_url'] = $this->breadcrum2_url;
        $output['breadcrum3_url'] = '';

        $this->load->view($this->config->item('adminfolder') . '/header', $output);
        $this->load->view($this->config->item('adminfolder') . '/add_message');
        $this->load->view($this->config->item('adminfolder') . '/footer');
    }

    function edit($id) {
        $output['title'] = 'Edit Message';
        if (!empty($_POST)) {
            $failure = FALSE;
         //   $this->form_validation->set_rules('title', 'Title', 'trim|required');
            $this->form_validation->set_rules('description', 'Description', 'trim|required');
            if ($this->form_validation->run()) {

                $this->messages_model->update($id);
                $success_message = 'Message record updated successfully';
            } else {

                $error_message = validation_errors();
                $failure = true;
            }
            if ($this->input->is_ajax_request()) {
                if ($failure) {
                    $data['success'] = false;
                    $data['error_message'] = $error_message;
                } else {
                    $data['success'] = true;
                    $data['success_message'] = $success_message;
                    $data['url'] = site_url('admin/messages');
                }
                $data['scrollToElement'] = true;
                echo json_encode($data);
                die;
            }
        }
        /*         * ******** For Selected menu and sub-menu in left bar ************** */
        $output['pageName'] = $this->pageName;
        $output['subpageName'] = 'Edit Message';

        /*         * ******** For Breadcrum ************** */
        $output['breadcrum1'] = $this->breadcrum1;
        $output['breadcrum2'] = $this->breadcrum2;
        $output['breadcrum3'] = '';
        $output['breadcrum4'] = $this->breadcrum4;

        $output['breadcrum2_url'] = $this->breadcrum2_url;
        $output['breadcrum3_url'] = $this->breadcrum3_url;
        $output['breadcrum4_url'] = '';

        $output['recordDetail'] = $recordDetail = $this->messages_model->getRecordById($id);

        $output['action'] = 'edit';

        $this->load->view($this->config->item('adminfolder') . '/header', $output);
        $this->load->view($this->config->item('adminfolder') . '/add_message');
        $this->load->view($this->config->item('adminfolder') . '/footer');
    }

    function view($id) {
        $output['records'] = $this->messages_model->getRecordById($id);
        $this->load->view($this->config->item('adminfolder') . '/messages_view', $output);
    }

    function filter($filterKey = NULL, $show_me = NULL, $sort_by = NULL, $currentPage = NULL) {
        $output['title'] = 'Messages';

        $filterKey = urldecode($filterKey);
        if ($filterKey != 'NULL')
            $output['filterKey'] = $filterKey;
        if ($show_me != 'NULL')
            $output['show_me'] = $show_me;
        if ($sort_by != 'NULL')
            $output['sort_by'] = $sort_by;


        if ($show_me != 'NULL' || $sort_by != 'NULL')
            $output['advanceSearch'] = 'Yes';

        if (!empty($_POST)) {
            $redirect_url = $_SERVER['HTTP_REFERER'];
            $task = $this->input->post('perform_task');
            $ids = $this->input->post('checkIds');
            if (sizeof($ids) > 0) {
                $message = $this->messages_model->performMultipleTasks($task, $ids);
                if ($message) {
                    $this->session->set_userdata('SUCCESS_MESSAGE', $message);
                } else {
                    $message = "Oops something is going wrong there...try again later.";
                    $this->session->set_userdata('ERROR_MESSAGE', $message);
                }
            } else {
                $message = "Oops something is going wrong there...try again later.";
                $this->session->set_userdata('ERROR_MESSAGE', $message);
            }
            if ($redirect_url)
                redirect($redirect_url);
            else
                redirect(site_url('admin/messages'));
        }

        $filterKey = $filterKey;
        if ($filterKey != 'NULL')
            $output['filterKey'] = $filterKey;

        $this->load->library('pagination');
        $config['base_url'] = base_url() . $this->config->config['adminfolder'] . '/messages/filter/' . $filterKey . '/' . $show_me . '/' . $sort_by . '/';
        $config['total_rows'] = $this->messages_model->getCountFilterAllRecords($filterKey, $show_me, $sort_by);
        $config['per_page'] = $this->config->item('page_size_admin');
        $this->pagination->initialize($config);
        $currentPage = (!$currentPage) ? 1 : $currentPage;
        $config['currentpage'] = $currentPage;
        if ($currentPage != "")
            $offset = ($currentPage - 1) * $config['per_page'];
        if ($config['total_rows'] > 0)
            $output['paging'] = $this->paging($config);

        $output['total'] = $this->messages_model->getCountAllRecordsForAdmin('');
        $output['totalActive'] = $this->messages_model->getCountAllRecordsForAdmin('Active');
        $output['totalInactive'] = $this->messages_model->getCountAllRecordsForAdmin('Inactive');
        $output['records'] = $this->messages_model->getFilterAllRecords($filterKey, $show_me, $sort_by, $config['per_page'], $offset);

        /*         * ******** For Selected menu and sub-menu in left bar ************** */
        $output['pageName'] = $this->pageName;
        $output['subpageName'] = 'All Messages';

        /*         * ******** For Breadcrum ************** */
        $output['breadcrum1'] = $this->breadcrum1;
        $output['breadcrum2'] = $this->breadcrum2;
        $output['breadcrum3'] = '';

        $output['breadcrum2_url'] = $this->breadcrum2_url;
        $output['breadcrum3_url'] = $this->breadcrum3_url;

        $this->load->view($this->config->item('adminfolder') . '/header', $output);
        $this->load->view($this->config->item('adminfolder') . '/messages_list');
        $this->load->view($this->config->item('adminfolder') . '/footer');
    }

    function doTask($task, $id) {
        $output['task'] = $task;
        if (!empty($_POST)) {
            $message = $this->messages_model->performTask($task, $id);
            $this->session->set_userdata('SUCCESS_MESSAGE', $message);
            $redirect_url = $_SERVER['HTTP_REFERER'];
            if ($redirect_url)
                redirect($redirect_url);
            else
                redirect(site_url('admin/messages'));
        }
        $this->load->view($this->config->item('adminfolder') . '/active_inactive_delete', $output);
    }

    #================= Paging ==========================================================#

    protected function paging($config) {
        $total_pages = ceil($config['total_rows'] / $config['per_page']);

        $total_pages = ceil($config['total_rows'] / $config['per_page']);
        $start = max($config['currentpage'] - intval($config['per_page'] / 2), 1);
        $end = $start + $config['per_page'] - 1;

        if ($config['currentpage'] == 1) {
            if ($config['total_rows'] > $config['per_page']) {
                $showing = '1-' . $config['per_page'];
            } else {
                $showing = '1-' . $config['total_rows'];
            }
        } else {
            $showing = ((($config['currentpage'] - 1) * $config['per_page']) + 1) . '-';
            if (($config['currentpage'] * $config['per_page']) > $config['total_rows']) {
                $showing.=$config['total_rows'];
            } else {
                $showing.=($config['currentpage'] * $config['per_page']);
            }
        }

        $output = '<ul class="pagination">';

        if ($config['currentpage'] > 1) {
            $output.='<li class=""><a href="' . $config['base_url'] . ($config['currentpage'] - 1) . '"> <i class="fa fa-angle-double-left"></i> </a></li>';
        } else {
            $output.='<li class="prev disabled "><a> <i class="fa fa-angle-double-left"></i> </a></li>';
        }

        for ($i = $start; $i <= $end && $i <= $total_pages; $i++) {
            if ($i == $config['currentpage']) {
                $output .= '<li class="active"><a>' . $i . '</a></li>';
            } else {
                $output .='<li><a href="' . $config['base_url'] . $i . '">' . $i . '</a></li>';
            }
        }
        $output .='</li>';

        if ($total_pages > $config['currentpage']) {
            $output.='<li class="next"><a href="' . $config['base_url'] . ($config['currentpage'] + 1) . '"> <i class="fa fa-angle-double-right"></i></a></li>';
        } else {
            $output.='<li class="next disabled"><a > <i class="fa fa-angle-double-right"></i> </a></li>';
        }
        $output.='</ul>';
        return $output;
    }

    public function create() {
        $output['title'] = 'Create SMS';
        /*         * ******** For Selected menu and sub-menu in left bar ************** */
        $output['pageName'] = $this->pageName;
        $output['subpageName'] = 'Create Message';

        /*         * ******** For Breadcrum ************** */
        $output['breadcrum1'] = $this->breadcrum1;
        //$output['breadcrum2'] = $this->breadcrum2;
        $output['breadcrum2'] = "Create Message";

        //$output['breadcrum2_url'] = $this->breadcrum2_url;
        $output['breadcrum2_url'] = '';
        $output['landlords'] = $this->getUserByType('landlord');
        $output['tenants'] = $this->getUserByType('tenant');

        if (!empty($_POST)) {
            $failure = FALSE;
            $this->form_validation->set_rules('custom_message', 'Message', 'trim|required');
            $this->form_validation->set_rules('users[]', 'Check atleast one tenant or landlord', 'required');
            if ($this->input->post('sch_msg') == 'Yes') {
                $this->form_validation->set_rules('sch_date', 'Schedule Date', 'trim|required');
            }
            if ($this->form_validation->run()) {
                //send message now
                $numbers = $this->common_model->getMobileNumbers($this->input->post('users'));
                if ($this->input->post('sch_msg') == 'No') {
                    if (!empty($numbers)) {
                        $counts = 0;
                        foreach ($numbers as $num) {
                            $this->send_sms($this->input->post('custom_message'), $num->mobile_no, $num->mem_id);
                            $counts++;
                        }
                    }
                    $success_message = 'SMS Sent';
                    $data['success'] = true;
                    $data['success_message'] = $success_message;
                    $data['scrollToElement'] = true;
                    $data['selfReload'] = true;
                    echo json_encode($data);
                    die;
                } else { //schedule message    
                    if (!empty($numbers)) {
                        $counts = 0;
                        $date = date('Y-m-d H:i:s', strtotime($this->input->post('sch_date')));
                        foreach ($numbers as $num) {
                            $repeat_type = $this->input->post('repeat_msg');
                            if ($repeat_type == 'year') {
                                $repeat_on = '';
                                $repeat_every = '';
                            } elseif ($repeat_type == 'month') {
                                $repeat_on = '';
                                $repeat_every = $this->input->post('repeat_every');
                            } else {
                                $repeat_on = $this->input->post('repeat_on');
                                $repeat_every = $this->input->post('repeat_every');
                            }
                            $sms_add[] = array(
                                'user_id' => $num->mem_id,
                                'number' => $num->mobile_no,
                                'message' => $this->input->post('custom_message'),
                                'scheduled_on' => $date,
                                'repeat_type' => $this->input->post('repeat_msg'),
                                'repeat_every' => $repeat_every,
                                'repeat_on' => $repeat_on,
                                'end_repeat' => $this->input->post('end_msg'),
                                'occurences' => $this->input->post('msg_occurence'),
                                'date_created' => date('Y-m-d H:i:s'),
                            );
                            $counts++;
                        }

                        $conditions = $params = array();
                        $params['batch_mode'] = true;
                        $conditions = $sms_add;
                        $params['table'] = 'tbl_admin_schedule';
                        $this->common_model->insert_data($conditions, $params);
                        $success_message = 'SMS scheduled for ' . $counts . '  tenants/properties';
                        $data['success'] = true;
                        $data['success_message'] = $success_message;
                        $data['scrollToElement'] = true;
                        $data['selfReload'] = true;
                        echo json_encode($data);
                        die;
                    }
                }
            } else {

                $error_message = validation_errors();
                $failure = true;
            }
            if ($this->input->is_ajax_request()) {
                if ($failure) {
                    $data['success'] = false;
                    $data['error_message'] = $error_message;
                } else {
                    $data['success'] = true;
                    $data['success_message'] = $success_message;
                    $data['url'] = site_url('admin/landlords');
                }
                $data['scrollToElement'] = true;
                echo json_encode($data);
                die;
            }
        }


        $this->load->view($this->config->item('adminfolder') . '/header', $output);
        $this->load->view($this->config->item('adminfolder') . '/add_sms');
        $this->load->view($this->config->item('adminfolder') . '/footer');
    }

    public function send_sms($msg, $mobile_number, $user) {
        // Load the library, call by the name of the
       $msg = $msg." -".SIGNATURE;
        $this->load->helper('twilio_helper');  // references library/twilio_helper.php
        $service = get_twilio_service();
        try {
            $number = "+1" . preg_replace("/[^0-9]/", "", $mobile_number);
             $service->account->sms_messages->create("+19046743077", "$number", "$msg", array());
            $this->message_log($msg, $number, $user);
            return TRUE;
        } catch (Exception $e) {
            $file = 'sms_error_logs.txt';
            if (file_exists($file)) {
                $current = file_get_contents($file);
                $current .= $e->getMessage() . "\n";
                file_put_contents($file, $current);
            }
            return TRUE;
        }
    }

    public function message_log($msg, $number, $user) {
        $conditions = $params = array();
        $conditions['message'] = $msg;
        $conditions['number'] = $number;
        $conditions['user_id'] = $user;
        $conditions['date_created'] = date('Y-m-d H:i:s');
        $params['table'] = 'tbl_admin_sms_logs';
        $this->common_model->insert_data($conditions, $params);
    }

    public function sent_sms($currentPage = NULL) {
        $output['title'] = 'Sent Messages';

        if ($this->input->post('perform_task') != '') {

            $redirect_url = $_SERVER['HTTP_REFERER'];
            $task = $this->input->post('perform_task');
            $ids = $this->input->post('checkIds');
            if (sizeof($ids) > 0) {
                $message = $this->messages_model->deleteMultipleLogs($task, $ids);
                if ($message) {
                    $this->session->set_userdata('SUCCESS_MESSAGE', $message);
                } else {
                    $message = "Oops something is going wrong there...try again later.";
                    $this->session->set_userdata('ERROR_MESSAGE', $message);
                }
            } else {
                $message = "Oops something is going wrong there...try again later.";
                $this->session->set_userdata('ERROR_MESSAGE', $message);
            }
            if ($redirect_url)
                redirect($redirect_url);
            else
                redirect(site_url('admin/messages/sent_sms'));
        }
        $keyword = '';
        if ($this->input->post('search') != '') {
            $keyword = $this->input->post('search');
        }

        $this->load->library('pagination');

        $config['base_url'] = base_url() . $this->config->config['adminfolder'] . '/messages/sent_sms/';
        $config['total_rows'] = $this->getSMSLogs('1', '0', '10', '', $keyword);
        $config['per_page'] = $this->config->item('page_size_admin');
        $this->pagination->initialize($config);
        $currentPage = (!$currentPage) ? 1 : $currentPage;
        $config['currentpage'] = $currentPage;
        if ($currentPage != "")
            $offset = ($currentPage - 1) * $config['per_page'];
        if ($config['total_rows'] > 0)
            $output['paging'] = $this->paging($config);
        $output['records'] = $this->getSMSLogs('0', $config['per_page'], $offset, '', $keyword);
        //  echo '<pre>';print_r($output);die;
        /*         * ******** For Selected menu and sub-menu in left bar ************** */
        $output['pageName'] = $this->pageName;
        $output['subpageName'] = 'Sent Messages';

        /*         * ******** For Breadcrum ************** */
        $output['breadcrum1'] = $this->breadcrum1;
        $output['breadcrum2'] = "Sent Messages";
        $output['breadcrum3'] = '';

        $output['breadcrum2_url'] = site_url('admin/messages/sent_sms');
        $output['breadcrum3_url'] = '';
        //  echo '<pre>';print_r($output);die;
        $this->load->view($this->config->item('adminfolder') . '/header', $output);
        $this->load->view($this->config->item('adminfolder') . '/sms_logs');
        $this->load->view($this->config->item('adminfolder') . '/footer');
    }

    function getSMSLogs($cnt, $limit, $offset, $single = NULL, $keyword) {
        $conditions = $params = array();
        $params['complex'] = true;
        $conditions['tables'] = array(
            'sms' => 'tbl_admin_sms_logs',
            'user' => 'tbl_members'
        );
        $conditions['table'] = 'sms';

        $conditions['on']['user'] = array(
            'sign' => '=',
            'column' => 'mem_id',
            'alias_column' => 'user_id',
            'alias_other' => 'sms',
        );
        if ($keyword != '') {
            $conditions['where']['first_name']['alias'] = 'user';
            $conditions['where']['first_name']['value'] = array(
                'operator' => 'OR',
                'sign' => 'LIKE',
                'key' => 'name',
                'value' => $keyword
            );
            $conditions['where']['last_name']['alias'] = 'user';
            $conditions['where']['last_name']['value'] = array(
                'operator' => 'OR',
                'sign' => 'LIKE',
                'key' => 'name',
                'value' => $keyword
            );
            $conditions['where']['message']['alias'] = 'sms';
            $conditions['where']['message']['value'] = array(
                'operator' => 'OR',
                'sign' => 'LIKE',
                'key' => 'name',
                'value' => $keyword
            );
        }
        if ($cnt == 1) {
            $params['cnt'] = true;
        } else if ($single != NULL) {
            $params['single_row'] = true;
            $conditions['where']['id']['alias'] = 'sms';
            $conditions['where']['id']['value'] = $single;
        } else {
            $params['offset'] = $offset;
            $params['limit'] = $limit;
        }

        $params['fields'] = array('user.first_name as lfname', 'user.last_name as llname', 'user.user_type', 'sms.*');
        $params['order_by'] = 'sms.id DESC';
        $info = $this->common_model->get_data($conditions, $params);
        return $info;
    }

    function view_log($id) {
        $output['records'] = $this->getSMSLogs('0', '0', '10', $id, '');
        $this->load->view($this->config->item('adminfolder') . '/log_view', $output);
    }

    function deleteLog($task, $id) {
        $output['task'] = $task;
        if (!empty($_POST)) {
            $conditions = $params = array();
            $conditions['id'] = $id;
            $params['table'] = 'tbl_admin_sms_logs';
            $this->common_model->delete($conditions, $params);
            $this->session->set_userdata('SUCCESS_MESSAGE', 'Selected record has been deleted successfully');
            $redirect_url = $_SERVER['HTTP_REFERER'];
            if ($redirect_url)
                redirect($redirect_url);
            else
                redirect(site_url('admin/messages/sent_sms'));
        }
        $this->load->view($this->config->item('adminfolder') . '/active_inactive_delete', $output);
    }

    function getUserByType($type) {
        $params = $conditions = array();
        $params['table'] = 'tbl_members';
        $conditions['user_type'] = $type;
        $conditions['status'] = 'Active';
        /* if($type=='landlord'){
          $conditions['verification_status'] = 'Yes';
          } */
        $params['fields'] = array('mem_id', 'first_name', 'last_name');
        $params['order_by'] = 'first_name';
        $list = $this->common_model->get_data($conditions, $params);
        return $list;
    }

    /*scheduled admin sms logs and operations*/
    
     public function scheduled_sms($currentPage = NULL) {
        $output['title'] = 'Scheduled Messages';

        if ($this->input->post('perform_task') != '') {

            $redirect_url = $_SERVER['HTTP_REFERER'];
            $task = $this->input->post('perform_task');
            $ids = $this->input->post('checkIds');
            if (sizeof($ids) > 0) {
                $message = $this->messages_model->deleteMultipleLogs($task, $ids);
                if ($message) {
                    $this->session->set_userdata('SUCCESS_MESSAGE', $message);
                } else {
                    $message = "Oops something is going wrong there...try again later.";
                    $this->session->set_userdata('ERROR_MESSAGE', $message);
                }
            } else {
                $message = "Oops something is going wrong there...try again later.";
                $this->session->set_userdata('ERROR_MESSAGE', $message);
            }
            if ($redirect_url)
                redirect($redirect_url);
            else
                redirect(site_url('admin/messages/sent_sms'));
        }
        $keyword = '';
        if ($this->input->post('search') != '') {
            $keyword = $this->input->post('search');
        }

        $this->load->library('pagination');

        $config['base_url'] = base_url() . $this->config->config['adminfolder'] . '/messages/scheduled_sms/';
        $config['total_rows'] = $this->getScheduledSMS('1', '0', '10', '', $keyword);
        $config['per_page'] = $this->config->item('page_size_admin');
        $this->pagination->initialize($config);
        $currentPage = (!$currentPage) ? 1 : $currentPage;
        $config['currentpage'] = $currentPage;
        if ($currentPage != "")
            $offset = ($currentPage - 1) * $config['per_page'];
        if ($config['total_rows'] > 0)
            $output['paging'] = $this->paging($config);
        $output['records'] = $this->getScheduledSMS('0', $config['per_page'], $offset, '', $keyword);
        //  echo '<pre>';print_r($output);die;
        /*         * ******** For Selected menu and sub-menu in left bar ************** */
        $output['pageName'] = $this->pageName;
        $output['subpageName'] = 'Scheduled Messages';

        /*         * ******** For Breadcrum ************** */
        $output['breadcrum1'] = $this->breadcrum1;
        $output['breadcrum2'] = "Scheduled Messages";
        $output['breadcrum3'] = '';

        $output['breadcrum2_url'] = site_url('admin/messages/scheduled_sms');;
        
        $output['breadcrum3_url'] = $this->breadcrum3_url;
       // echo '<pre>';print_r($output);die;
        $this->load->view($this->config->item('adminfolder') . '/header', $output);
        $this->load->view($this->config->item('adminfolder') . '/scheduled_sms');
        $this->load->view($this->config->item('adminfolder') . '/footer');
    }

    function getScheduledSMS($cnt, $limit, $offset, $single = NULL, $keyword) {
        $conditions = $params = array();
        $params['complex'] = true;
        $conditions['tables'] = array(
            'sms' => 'tbl_admin_schedule',
            'user' => 'tbl_members'
        );
        $conditions['table'] = 'sms';

        $conditions['on']['user'] = array(
            'sign' => '=',
            'column' => 'mem_id',
            'alias_column' => 'user_id',
            'alias_other' => 'sms',
        );
        if ($keyword != '') {
            $conditions['where']['first_name']['alias'] = 'user';
            $conditions['where']['first_name']['value'] = array(
                'operator' => 'OR',
                'sign' => 'LIKE',
                'key' => 'name',
                'value' => $keyword
            );
            $conditions['where']['last_name']['alias'] = 'user';
            $conditions['where']['last_name']['value'] = array(
                'operator' => 'OR',
                'sign' => 'LIKE',
                'key' => 'name',
                'value' => $keyword
            );
            $conditions['where']['message']['alias'] = 'sms';
            $conditions['where']['message']['value'] = array(
                'operator' => 'OR',
                'sign' => 'LIKE',
                'key' => 'name',
                'value' => $keyword
            );
        }
        if ($cnt == 1) {
            $params['cnt'] = true;
        } else if ($single != NULL) {
            $params['single_row'] = true;
            $conditions['where']['id']['alias'] = 'sms';
            $conditions['where']['id']['value'] = $single;
        } else {
            $params['offset'] = $offset;
            $params['limit'] = $limit;
        }

        $params['fields'] = array('user.first_name as lfname', 'user.last_name as llname', 'user.user_type', 'sms.*');
        $params['order_by'] = 'sms.id DESC';
        $info = $this->common_model->get_data($conditions, $params);
        return $info;
    }

    function view_schedule($id) {
        $output['records'] = $this->getScheduledSMS('0', '0', '10', $id, '');
        $this->load->view($this->config->item('adminfolder') . '/sch_view', $output);
    }
    
     function edit_schedule($id) {
        $output['records'] = $this->getScheduledSMS('0', '0', '10', $id, '');
        $this->load->view($this->config->item('adminfolder') . '/edit_sch', $output);
    }

    function deleteSchedule($task, $id) {
        $output['task'] = $task;
        if (!empty($_POST)) {
            $conditions = $params = array();
            $conditions['id'] = $id;
            $params['table'] = 'tbl_admin_schedule';
            $this->common_model->delete($conditions, $params);
            $this->session->set_userdata('SUCCESS_MESSAGE', 'Selected record has been deleted successfully');
            $redirect_url = $_SERVER['HTTP_REFERER'];
            if ($redirect_url)
                redirect($redirect_url);
            else
                redirect(site_url('admin/messages/sent_sms'));
        }
        $this->load->view($this->config->item('adminfolder') . '/active_inactive_delete', $output);
    }
    
    public function update_message() {
        if ($this->input->post('id')) {
            $conditions = $params = array();
            $conditions['where']['id'] = $this->input->post('id');
            $conditions['value']['scheduled_on'] = date('Y-m-d H:i:s', strtotime($this->input->post('sch_date')));
            $conditions['value']['repeat_type'] = $this->input->post('edit_repeat_msg');
            $conditions['value']['repeat_every'] = $this->input->post('edit_repeat_every');
            $conditions['value']['repeat_on'] = $this->input->post('edit_repeat_on');
            if ($this->input->post('repeat_occurence') == 'noend') {
                $conditions['value']['end_repeat'] = 'infinite';
            } else {
                $conditions['value']['end_repeat'] = 'finite';
                $conditions['value']['occurences'] = $this->input->post('repeat_occurence');
            }
            $params['table'] = 'tbl_admin_schedule';
            $this->common_model->update($conditions, $params);
            echo '1';
            exit;
        } else {
            echo '0';
            exit;
        }
    }
    
    
}
