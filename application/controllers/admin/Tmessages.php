<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Tmessages extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->pageName = 'Messages';
        $this->breadcrum1 = 'Messages';
        $this->breadcrum2 = 'Amenities Messages';
        $this->breadcrum3 = 'Add Message';
        $this->breadcrum4 = 'Edit Message';
        $this->breadcrum2_url = site_url('admin/tmessages');
        $this->breadcrum3_url = site_url('admin/messages/add');
        $this->breadcrum4_url = site_url('admin/messages/edit');
        $this->load->model('messages_model');
        $this->common_model->checkAdminLogin();
        $this->js();
    }

    private function js() {
        $config[] = 'ckeditor/ckeditor.js';
        $this->config->set_item('adminjs', $config);
    }

    public function index($currentPage = NULL) {
        $output['title'] = 'Amenities Messages';

        if ($this->input->post('perform_task') != '') {

            $redirect_url = $_SERVER['HTTP_REFERER'];
            $task = $this->input->post('perform_task');
            $ids = $this->input->post('checkIds');
            if (sizeof($ids) > 0) {
                $message = $this->messages_model->performMultipleSaved($task, $ids);
                if ($message) {
                    $this->session->set_userdata('SUCCESS_MESSAGE', $message);
                } else {
                    $message = "Oops something is going wrong there...try again later.";
                    $this->session->set_userdata('ERROR_MESSAGE', $message);
                }
            } else {
                $message = "Oops something is going wrong there...try again later.";
                $this->session->set_userdata('ERROR_MESSAGE', $message);
            }
            if ($redirect_url)
                redirect($redirect_url);
            else
                redirect(site_url('admin/messages/sent_sms'));
        }
        $keyword = '';
        if ($this->input->post('search') != '') {
            $keyword = $this->input->post('search');
        }

        $this->load->library('pagination');

        $config['base_url'] = base_url() . $this->config->config['adminfolder'] . '/tmessages/';
        $config['total_rows'] = $this->getSavedSms('1', '0', '10', '', $keyword);
        $config['per_page'] = $this->config->item('page_size_admin');
        $this->pagination->initialize($config);
        $currentPage = (!$currentPage) ? 1 : $currentPage;
        $config['currentpage'] = $currentPage;
        if ($currentPage != "")
            $offset = ($currentPage - 1) * $config['per_page'];
        if ($config['total_rows'] > 0)
            $output['paging'] = $this->paging($config);
        $sms = $this->getSavedSms('0', $config['per_page'], $offset, '', $keyword);
        $srr = array();
        if (!empty($sms)) {
            foreach ($sms as $msg) {
                $srr[$msg->id]['sms'] = $msg;
                $srr[$msg->id]['dates'] = $this->getSMSDates($msg->id);
            }
        }
        $output['records'] = $srr;
        $output['amenties'] = $this->getAmenties();
        /*         * ******** For Selected menu and sub-menu in left bar ************** */
        $output['pageName'] = $this->pageName;
        $output['subpageName'] = 'Amenities Messages';

        /*         * ******** For Breadcrum ************** */
        $output['breadcrum1'] = $this->breadcrum1;
        $output['breadcrum2'] = $this->breadcrum2;
        $output['breadcrum3'] = '';

        $output['breadcrum2_url'] = $this->breadcrum2_url;
        $output['breadcrum3_url'] = $this->breadcrum3_url;
        //  echo '<pre>';print_r($output);die;
        $this->load->view($this->config->item('adminfolder') . '/header', $output);
        $this->load->view($this->config->item('adminfolder') . '/saved_sms');
        $this->load->view($this->config->item('adminfolder') . '/footer');
    }

    function getSavedSms($cnt, $limit, $offset, $single = NULL, $keyword) {
        $conditions = $params = array();
        $params['table'] = 'tbl_saved_sms';
        if ($cnt == 1) {
            $params['cnt'] = true;
        } else if ($single != NULL) {
            $params['single_row'] = true;
            $conditions['id'] = $single;
        } else {
            $params['offset'] = $offset;
            $params['limit'] = $limit;
        }
        $info = $this->common_model->get_data($conditions, $params);
        return $info;
    }

    public function getSMSDates($id) {
        $conditions = $params = array();
        $params['table'] = 'tbl_sms_dates';
        $conditions['saved_sms_id'] = $id;
        $info = $this->common_model->get_data($conditions, $params);
        return $info;
    }

    public function getAmenties() {
        $conditions = $params = array();
        $params['table'] = 'tbl_amenties';
        $params['fields'] = array('id', 'name');
        $info = $this->common_model->get_data($conditions, $params);
        return $info;
    }

    function view_saved($id) {
        $sms = $this->getSavedSms('0', '0', '10', $id, '');

        $srr = array();
        if (!empty($sms)) {
            $sms->dates = $this->getSMSDates($sms->id);
        }
        $output['records'] = $sms;
        //  echo '<pre>';print_r($sms);die;
        $output['amenties'] = $this->getAmenties();

        $this->load->view($this->config->item('adminfolder') . '/view_saved', $output);
    }

    function deleteSMS($task, $id) {
        $output['task'] = $task;
        if (!empty($_POST)) {
            $conditions = $params = array();
            $conditions['id'] = $id;
            $params['table'] = 'tbl_saved_sms';
            $this->common_model->delete($conditions, $params);
            $conditions = $params = array();
            $conditions['saved_sms_id'] = $id;
            $params['table'] = 'tbl_sms_dates';
            $this->common_model->delete($conditions, $params);
            $this->session->set_userdata('SUCCESS_MESSAGE', 'Selected record has been deleted successfully');
            $redirect_url = $_SERVER['HTTP_REFERER'];
            if ($redirect_url)
                redirect($redirect_url);
            else
                redirect(site_url('admin/tmessages/'));
        }
        $this->load->view($this->config->item('adminfolder') . '/active_inactive_delete', $output);
    }

    public function create() {
        $output['title'] = 'Create Amenitiy Message';
        /*         * ******** For Selected menu and sub-menu in left bar ************** */
        $output['pageName'] = $this->pageName;
        $output['subpageName'] = 'Create Amenity Message';

        /*         * ******** For Breadcrum ************** */
        $output['breadcrum1'] = $this->breadcrum1;
        //$output['breadcrum2'] = $this->breadcrum2;
        $output['breadcrum2'] = $this->breadcrum3;

        //$output['breadcrum2_url'] = $this->breadcrum2_url;
        $output['breadcrum2_url'] = '';
        $output['amenties'] = $this->getAmenties();
        //  $output['tenants'] = $this->getUserByType('tenant');

        if (!empty($_POST)) {
            $failure = FALSE;
            $this->form_validation->set_rules('custom_message', 'Message', 'trim|required');
            if ($this->form_validation->run()) {

                $conditions = $params = array();
                $conditions['message'] = $this->input->post('custom_message');
                $conditions['amenity'] = $this->input->post('amenty');
                $conditions['status'] = 'Active';
                $conditions['date_created'] = date('Y-m-d H:i:s');
                $params['table'] = 'tbl_saved_sms';
                $id = $this->common_model->insert_data($conditions, $params);

                $months = $this->input->post('month');
                $days = $this->input->post('day');
                foreach ($months as $key => $num) {
                    $add[] = array(
                        'saved_sms_id' => $id,
                        'month' => $num,
                        'day' => $days[$key],
                    );
                }
                $conditions = $params = array();
                $params['batch_mode'] = true;
                $conditions = $add;
                $params['table'] = 'tbl_sms_dates';
                $this->common_model->insert_data($conditions, $params);
                $success_message = 'SMS added successfully';
                $data['success'] = true;
                $data['success_message'] = $success_message;
                $data['scrollToElement'] = true;
                $data['selfReload'] = true;
                echo json_encode($data);
                die;
            } else {
                $error_message = validation_errors();
                $failure = true;
            }
            if ($this->input->is_ajax_request()) {
                if ($failure) {
                    $data['success'] = false;
                    $data['error_message'] = $error_message;
                } else {
                    $data['success'] = true;
                    $data['success_message'] = $success_message;
                    $data['url'] = site_url('admin/landlords');
                }
                $data['scrollToElement'] = true;
                echo json_encode($data);
                die;
            }
        }
        $this->load->view($this->config->item('adminfolder') . '/header', $output);
        $this->load->view($this->config->item('adminfolder') . '/add_tsms');
        $this->load->view($this->config->item('adminfolder') . '/footer');
    }

    function edit($id) {
        $output['title'] = 'Edit SMS';
        /*         * ******** For Selected menu and sub-menu in left bar ************** */
        $output['pageName'] = $this->pageName;
        $output['subpageName'] = 'Add/Schedule Messgae';

        /*         * ******** For Breadcrum ************** */
        $output['breadcrum1'] = $this->breadcrum1;
        $output['breadcrum2'] = $this->breadcrum2;
        $output['breadcrum3'] = "Edit Message";

        $output['breadcrum2_url'] = $this->breadcrum2_url;
        $output['breadcrum3_url'] = '';

        if (!empty($_POST)) {
            $failure = FALSE;
            $this->form_validation->set_rules('custom_message', 'Message', 'trim|required');
            if ($this->form_validation->run()) {
                $conditions = $params = array();
                $conditions['where']['id'] = $id;
                $conditions['value']['message'] = $this->input->post('custom_message');
                $conditions['value']['amenity'] = $this->input->post('amenty');
                $params['table'] = 'tbl_saved_sms';
                $this->common_model->update($conditions, $params);

                $conditions = $params = array();
                $conditions['saved_sms_id'] = $id;
                $params['table'] = 'tbl_sms_dates';
                $this->common_model->delete($conditions, $params);

                $months = $this->input->post('month');
                $days = $this->input->post('day');
                foreach ($months as $key => $num) {
                    $add[] = array(
                        'saved_sms_id' => $id,
                        'month' => $num,
                        'day' => $days[$key],
                    );
                }
                $conditions = $params = array();
                $params['batch_mode'] = true;
                $conditions = $add;
                $params['table'] = 'tbl_sms_dates';
                $this->common_model->insert_data($conditions, $params);
                $success_message = 'SMS updated successfully';
                $data['success'] = true;
                $data['success_message'] = $success_message;
                $data['scrollToElement'] = true;
                $data['selfReload'] = true;
                echo json_encode($data);
                die;
            } else {
                $error_message = validation_errors();
                $failure = true;
            }
            if ($this->input->is_ajax_request()) {
                if ($failure) {
                    $data['success'] = false;
                    $data['error_message'] = $error_message;
                } else {
                    $data['success'] = true;
                    $data['success_message'] = $success_message;
                    $data['url'] = site_url('admin/landlords');
                }
                $data['scrollToElement'] = true;
                echo json_encode($data);
                die;
            }
        }

        $sms = $this->getSavedSms('0', '0', '10', $id, '');
        $srr = array();
        if (!empty($sms)) {
            $sms->dates = $this->getSMSDates($sms->id);
        }
        $output['records'] = $sms;
        $output['amenties'] = $this->getAmenties();
        //   echo '<pre>';print_r($output);die;
        $this->load->view($this->config->item('adminfolder') . '/header', $output);
        $this->load->view($this->config->item('adminfolder') . '/edit_tsms');
        $this->load->view($this->config->item('adminfolder') . '/footer');
    }

    function getUserByType($type) {
        $params = $conditions = array();
        $params['table'] = 'tbl_members';
        $conditions['user_type'] = $type;
        $conditions['status'] = 'Active';
        /* if($type=='landlord'){
          $conditions['verification_status'] = 'Yes';
          } */
        $params['fields'] = array('mem_id', 'first_name', 'last_name');
        $params['order_by'] = 'first_name';
        $list = $this->common_model->get_data($conditions, $params);
        return $list;
    }

    function doTask($task, $id) {
        $output['task'] = $task;
        if (!empty($_POST)) {
            if ($task == 'Active' || $task == 'Inactive') {
                $conditions = $params = array();
                $conditions['where']['id'] = $id;
                $conditions['value']['status'] = $task;
                $params['table'] = 'tbl_saved_sms';
                $this->common_model->update($conditions, $params);
            }
            $this->session->set_userdata('SUCCESS_MESSAGE', $message);
            $redirect_url = $_SERVER['HTTP_REFERER'];
            if ($redirect_url)
                redirect($redirect_url);
            else
                redirect(site_url('admin/tmessages'));
        }
        $this->load->view($this->config->item('adminfolder') . '/active_inactive_delete', $output);
    }

#================= Paging ==========================================================#

    protected function paging($config) {
        $total_pages = ceil($config['total_rows'] / $config['per_page']);

        $total_pages = ceil($config['total_rows'] / $config['per_page']);
        $start = max($config['currentpage'] - intval($config['per_page'] / 2), 1);
        $end = $start + $config['per_page'] - 1;

        if ($config['currentpage'] == 1) {
            if ($config['total_rows'] > $config['per_page']) {
                $showing = '1-' . $config['per_page'];
            } else {
                $showing = '1-' . $config['total_rows'];
            }
        } else {
            $showing = ((($config['currentpage'] - 1) * $config['per_page']) + 1) . '-';
            if (($config['currentpage'] * $config['per_page']) > $config['total_rows']) {
                $showing.=$config['total_rows'];
            } else {
                $showing.=($config['currentpage'] * $config['per_page']);
            }
        }

        $output = '<ul class="pagination">';

        if ($config['currentpage'] > 1) {
            $output.='<li class=""><a href="' . $config['base_url'] . ($config['currentpage'] - 1) . '"> <i class="fa fa-angle-double-left"></i> </a></li>';
        } else {
            $output.='<li class="prev disabled "><a> <i class="fa fa-angle-double-left"></i> </a></li>';
        }

        for ($i = $start; $i <= $end && $i <= $total_pages; $i++) {
            if ($i == $config['currentpage']) {
                $output .= '<li class="active"><a>' . $i . '</a></li>';
            } else {
                $output .='<li><a href="' . $config['base_url'] . $i . '">' . $i . '</a></li>';
            }
        }
        $output .='</li>';

        if ($total_pages > $config['currentpage']) {
            $output.='<li class="next"><a href="' . $config['base_url'] . ($config['currentpage'] + 1) . '"> <i class="fa fa-angle-double-right"></i></a></li>';
        } else {
            $output.='<li class="next disabled"><a > <i class="fa fa-angle-double-right"></i> </a></li>';
        }
        $output.='</ul>';
        return $output;
    }

}
