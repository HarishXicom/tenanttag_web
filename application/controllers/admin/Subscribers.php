<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Subscribers extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->pageName = 'Subscribers';
        $this->breadcrum1 = 'Subscribers';
        $this->breadcrum2 = '';
        $this->breadcrum3 = '';
        $this->breadcrum4 = '';
        $this->breadcrum2_url = site_url('admin/subscribers');
        $this->breadcrum3_url = site_url('admin/subscribers');
        $this->breadcrum4_url = site_url('admin/subscribers');
        $this->common_model->checkAdminLogin();
        $this->load->model('messages_model');
        $this->js();
    }

    private function js() {
        $config[] = 'ckeditor/ckeditor.js';
        $this->config->set_item('adminjs', $config);
    }

    public function index($currentPage = NULL) {
        $output['title'] = 'Subscribers';

        if ($this->input->post('perform_task') != '') {

            $redirect_url = $_SERVER['HTTP_REFERER'];
            $task = $this->input->post('perform_task');
            $ids = $this->input->post('checkIds');
            if (sizeof($ids) > 0) {
                $message = $this->messages_model->performMultipleSubscribers($task, $ids);
                if ($message) {
                    $this->session->set_userdata('SUCCESS_MESSAGE', $message);
                } else {
                    $message = "Oops something is going wrong there...try again later.";
                    $this->session->set_userdata('ERROR_MESSAGE', $message);
                }
            } else {
                $message = "Oops something is going wrong there...try again later.";
                $this->session->set_userdata('ERROR_MESSAGE', $message);
            }
            if ($redirect_url)
                redirect($redirect_url);
            else
                redirect(site_url('admin/messages/sent_sms'));
        }
        $keyword = '';
        if ($this->input->post('search') != '') {
            $keyword = $this->input->post('search');
        }

        $this->load->library('pagination');

        $config['base_url'] = base_url() . $this->config->config['adminfolder'] . '/subscribers/index/';
        $config['total_rows'] = $this->getSubscribers('1', '0', '10', '', $keyword);
        $config['per_page'] = $this->config->item('page_size_admin');
        $this->pagination->initialize($config);
        $currentPage = (!$currentPage) ? 1 : $currentPage;
        $config['currentpage'] = $currentPage;
        if ($currentPage != "")
            $offset = ($currentPage - 1) * $config['per_page'];
        if ($config['total_rows'] > 0)
            $output['paging'] = $this->paging($config);
        $checklist = $this->getSubscribers('0', $config['per_page'], $offset, '', $keyword);

        $output['records'] = $checklist;
        // $output['amenties'] = $this->getAmenties();
        /*         * ******** For Selected menu and sub-menu in left bar ************** */
        $output['pageName'] = $this->pageName;
        $output['subpageName'] = 'Subscribers';

        /*         * ******** For Breadcrum ************** */
        $output['breadcrum1'] = $this->breadcrum1;
        $output['breadcrum2'] = $this->breadcrum2;
        $output['breadcrum3'] = '';

        $output['breadcrum2_url'] = $this->breadcrum2_url;
        $output['breadcrum3_url'] = $this->breadcrum3_url;
        //   echo '<pre>';print_r($output);die;
        $this->load->view($this->config->item('adminfolder') . '/header', $output);
        $this->load->view($this->config->item('adminfolder') . '/subscribers');
        $this->load->view($this->config->item('adminfolder') . '/footer');
    }

    function getSubscribers($cnt, $limit, $offset, $single = NULL, $keyword) {
        $conditions = $params = array();
        $params['table'] = 'tbl_subscribers';
        if ($cnt == 1) {
            $params['cnt'] = true;
        } else if ($single != NULL) {
            $params['single_row'] = true;
            $conditions['id'] = $single;
        } else {
            $params['offset'] = $offset;
            $params['limit'] = $limit;
        }
        $conditions['status'] = '1';
        $info = $this->common_model->get_data($conditions, $params);
        return $info;
    }

    function deleteSubscriber($task, $id) {
        $output['task'] = $task;
        if (!empty($_POST)) {
            $conditions = $params = array();
            $conditions['id'] = $id;
            $params['table'] = 'tbl_subscribers';
            $this->common_model->delete($conditions, $params);
            $this->session->set_userdata('SUCCESS_MESSAGE', 'Selected record has been deleted successfully');
            $redirect_url = $_SERVER['HTTP_REFERER'];
            if ($redirect_url)
                redirect($redirect_url);
            else
                redirect(site_url('admin/subscribers/'));
        }
        $this->load->view($this->config->item('adminfolder') . '/active_inactive_delete', $output);
    }

    function getUserByType($type) {
        $params = $conditions = array();
        $params['table'] = 'tbl_members';
        $conditions['user_type'] = $type;
        $conditions['status'] = 'Active';
        /* if($type=='landlord'){
          $conditions['verification_status'] = 'Yes';
          } */
        $params['fields'] = array('mem_id', 'first_name', 'last_name');
        $params['order_by'] = 'first_name';
        $list = $this->common_model->get_data($conditions, $params);
        return $list;
    }

    function doTask($task, $id) {
        $output['task'] = $task;
        if (!empty($_POST)) {
            if ($task == 'Active' || $task == 'Inactive') {
                $ststus = $task == 'Active' ? '1' : '0';
                $conditions = $params = array();
                $conditions['where']['id'] = $id;
                $conditions['value']['status'] = $ststus;
                $params['table'] = 'tbl_checklist';
                $this->common_model->update($conditions, $params);
            }
            $this->session->set_userdata('SUCCESS_MESSAGE', $message);
            $redirect_url = $_SERVER['HTTP_REFERER'];
            if ($redirect_url)
                redirect($redirect_url);
            else
                redirect(site_url('admin/checklist'));
        }
        $this->load->view($this->config->item('adminfolder') . '/active_inactive_delete', $output);
    }

    public function exportEmails() {
        $this->load->dbutil();
        $this->load->helper('file');
        $this->load->helper('download');
        $report = $this->common_model->getSubscribersCSV();
        $new_report = $this->dbutil->csv_from_result($report);
        $file = "Users_" . date('Y-m-d H:i:s') . ".csv";
        force_download($file, $new_report);
    }

#================= Paging ==========================================================#

    protected function paging($config) {
        $total_pages = ceil($config['total_rows'] / $config['per_page']);

        $total_pages = ceil($config['total_rows'] / $config['per_page']);
        $start = max($config['currentpage'] - intval($config['per_page'] / 2), 1);
        $end = $start + $config['per_page'] - 1;

        if ($config['currentpage'] == 1) {
            if ($config['total_rows'] > $config['per_page']) {
                $showing = '1-' . $config['per_page'];
            } else {
                $showing = '1-' . $config['total_rows'];
            }
        } else {
            $showing = ((($config['currentpage'] - 1) * $config['per_page']) + 1) . '-';
            if (($config['currentpage'] * $config['per_page']) > $config['total_rows']) {
                $showing.=$config['total_rows'];
            } else {
                $showing.=($config['currentpage'] * $config['per_page']);
            }
        }

        $output = '<ul class="pagination">';

        if ($config['currentpage'] > 1) {
            $output.='<li class=""><a href="' . $config['base_url'] . ($config['currentpage'] - 1) . '"> <i class="fa fa-angle-double-left"></i> </a></li>';
        } else {
            $output.='<li class="prev disabled "><a> <i class="fa fa-angle-double-left"></i> </a></li>';
        }

        for ($i = $start; $i <= $end && $i <= $total_pages; $i++) {
            if ($i == $config['currentpage']) {
                $output .= '<li class="active"><a>' . $i . '</a></li>';
            } else {
                $output .='<li><a href="' . $config['base_url'] . $i . '">' . $i . '</a></li>';
            }
        }
        $output .='</li>';

        if ($total_pages > $config['currentpage']) {
            $output.='<li class="next"><a href="' . $config['base_url'] . ($config['currentpage'] + 1) . '"> <i class="fa fa-angle-double-right"></i></a></li>';
        } else {
            $output.='<li class="next disabled"><a > <i class="fa fa-angle-double-right"></i> </a></li>';
        }
        $output.='</ul>';
        return $output;
    }

}
