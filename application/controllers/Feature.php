<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Feature extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('country_model');
        $this->load->model('region_model');
        $this->load->model('city_model');
        $this->load->model('properties_model');
        $this->load->model('member_model');
        $this->load->model('common_model');
        /*         * *** Unset property id Session *** */
        $this->session->unset_userdata('PROPERTY_ID');
        if ($this->session->userdata('MEM_ID'))
            $this->common_model->getLastProeprty();
    }

    public function index() {
        $slug = $this->uri->segment(2);
        if (empty($slug)) {
            redirect('/');
        }
        $params['table'] = 'tbl_pages';
        $params['single_row'] = true;
        $conditions['slug'] = $slug;
        $content = $this->common_model->get_data($conditions, $params);
        if (empty($content)) {
            show_404();
        }
        //echo '<pre>';print_r($content);die;
        $output['content'] = $content;
        $this->load->view($this->config->item('defaultfolder') . '/app_header');
        $this->load->view($this->config->item('defaultfolder') . '/static_page', $output);
        //$this->load->view($this->config->item('defaultfolder') . '/auth_js');
        $this->load->view($this->config->item('defaultfolder') . '/footer');
    }

    public function app() {
        $slug = $this->uri->segment(3);
        if (empty($slug)) {
            redirect('/');
        }
        $params['table'] = 'tbl_pages';
        $params['single_row'] = true;
        $conditions['slug'] = $slug;
        $content = $this->common_model->get_data($conditions, $params);
        if (empty($content)) {
            show_404();
        }
        //echo '<pre>';print_r($content);die;
        $output['content'] = $content;
        $this->load->view($this->config->item('defaultfolder') . '/app_header');
        $this->load->view($this->config->item('defaultfolder') . '/static_page', $output);
        //$this->load->view($this->config->item('defaultfolder') . '/auth_js');
        $this->load->view($this->config->item('defaultfolder') . '/footer');
    }
    
   

  

}
