<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Vendors extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model(array('vendor_model', 'properties_model', 'tservices_model'));
        $this->load->model('services_model');
        $this->mem_id = $this->session->userdata('MEM_ID');
        $this->common_model->checkMemberLogin();
    }

    function index() {
        $output['selected_tab'] = 'vendors';
        $output['title'] = 'Vendors';

        $params = $conditions = array();

        $params['complex'] = true;
        $conditions['tables'] = array(
            'vendor' => 'tbl_vendors',
            'property' => 'tbl_properties',
        );
        $conditions['table'] = 'vendor';
        $conditions['on']['property'] = array(
            'sign' => '=',
            'column' => 'prop_id',
            'alias_column' => 'property_id',
            'alias_other' => 'vendor',
            'join' => 'left'
        );
        $conditions['where']['vendor_owner_id']['alias'] = 'vendor';
        $conditions['where']['vendor_owner_id']['value'] = $this->session->userdata('MEM_ID');
        $conditions['where']['status']['alias'] = 'vendor';
        $conditions['where']['status']['value'] = 'Active';
        $conditions['where']['status']['alias'] = 'property';
        $conditions['where']['status']['value'] = 'Active';
        $params['fields'] = array(
            'property.prop_id', 'property.property_code', 'property.city',
            'property.address1', 'property.address2', 'property.zip', 'property.unit_number'
            , 'vendor.v_id as vendor_id', 'vendor.company_name', 'vendor.mobile_no', 'vendor.email', 'vendor.service',
        );
        //$params['group_by'] = 'vendor.email';
        $infi = array();
        $property_data = $this->common_model->get_data($conditions, $params);
        //  echo '<pre>';print_r($property_data);die;
        if (!empty($property_data)) {
            foreach ($property_data as $key => $data) {
                $infi[$data->prop_id]['details'] = array(
                    "prop_id" => $data->prop_id, "code" => $data->property_code,
                    "city" => $data->city, "address1" => $data->address1,
                    "address2" => $data->address2, "unit_number" => $data->unit_number,
                );
                $infi[$data->prop_id]['vendors'][] = array(
                    "vendor_id" => $data->vendor_id, "cname" => $data->company_name,
                    "mobile_no" => $data->mobile_no, "email" => $data->email, "service" => $data->service,
                );
            }
        }
        $params['group_by'] = 'vendor.email';
        $summary_data = $this->common_model->get_data($conditions, $params);
        // echo '<pre>';print_r($property_data);die;
        $conditions = $params = array();
        $params['table'] = 'tbl_properties';
        $conditions['owner_id'] = $this->session->userdata('MEM_ID');
        $conditions['status'] = 'Active';
        $params['fields'] = array('prop_id', 'city', 'address1', 'address2', 'zip');

        $output['all_properties'] = $this->common_model->get_data($conditions, $params);
        $output['properties'] = $infi;
        $output['summary'] = $summary_data;
        //echo '<pre>';print_r($output);die;
        $this->load->view($this->config->item('defaultfolder') . '/header', $output);
        $this->load->view($this->config->item('defaultfolder') . '/list_vendors');
        $this->load->view($this->config->item('defaultfolder') . '/vendor_js');
        $this->load->view($this->config->item('defaultfolder') . '/footer');
    }

    function add_vendor() {
        if (!empty($_POST)) {
            $failure = 0;
            $this->form_validation->set_message('is_unique', '%s is already taken.');
            $this->form_validation->set_message('required', '%s is required');
            //$this->form_validation->set_rules('company_name', 'Company Name', 'trim|required');
            $this->form_validation->set_rules('company_contact', 'Phone', 'trim|required');
            $this->form_validation->set_rules('email', 'Email', 'trim|required');
            $this->form_validation->set_rules('service', 'Service', 'trim|required');

            if ($this->form_validation->run()) {                
        
                //$memId = $this->vendor_model->addVendor();
                if ($memId) {
                    $success_message = '<p>Vendor added successfully.</p>';
                } else {
                    $error_message = '<p>Sorry for inconvenience, something is going wrong there...please try again later.</p>';
                    $failure = true;
                }
            } else {
                $error_message = validation_errors();
                $failure = true;
            }

            if ($this->input->is_ajax_request()) {
                if ($failure) {
                    $data['success'] = false;
                    $data['error_message'] = $error_message;
                } else {
                    $data['row'] = $record = $this->vendor_model->getMemberInfo('v_id', $memId);
                    $sname = $this->tservices_model->getRecordById($this->input->post('service'));
                    $data['service_name'] = $sname->name;
                    /*                     * ******** Get All Services ******************************** */
                    $data['vendorServices'] = $this->tservices_model->frontGetVendorServices();
                    $data['allServices'] = $this->services_model->frontGetAllServices();
                    $data['success'] = true;
                    $data['resetForm'] = true;
                    $data['success_message'] = $success_message;
                    $data['url'] = '';
                    //$data['ajaxCallBackFunction'] = true;
                    //$data['selfReload'] = true;
                }
                //   $data['scrollToElement'] = true;
                echo json_encode($data);
                die;
            }
        }
    }

    function edit_vendor($id) {
        if (!empty($_POST)) {
            $failure = 0;
            $this->form_validation->set_message('is_unique', '%s is already taken.');
            $this->form_validation->set_message('required', '%s is required');
            $old_email = $this->common_model->getSingleFieldFromAnyTable('email', 'v_id', $id, 'tbl_vendors');
            //$this->form_validation->set_rules('company_name', 'Company Name', 'trim|required');
            $this->form_validation->set_rules('company_contact', 'Phone', 'trim|required');
            if ($old_email != $this->input->post('email'))
                $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[tbl_vendors.email]');
            else
                $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
            $this->form_validation->set_rules('service', 'Service', 'trim|required');

            if ($this->form_validation->run()) {
                $this->vendor_model->updateVendor($id);
                $success_message = '<p>Vendor record updated successfully.</p>';
            } else {
                $error_message = validation_errors();
                $failure = true;
            }

            if ($this->input->is_ajax_request()) {
                if ($failure) {
                    $data['success'] = false;
                    $data['error_message'] = $error_message;
                } else {
                    $data['row'] = $record = $this->vendor_model->getMemberInfo('v_id', $id);
                    $sname = $this->tservices_model->getRecordById($this->input->post('service'));
                    $data['service_name'] = $sname->name;
                    $data['success'] = true;
                    $data['resetForm'] = true;
                    $data['success_message'] = $success_message;
                    $data['url'] = '';
                }
                //   $data['scrollToElement'] = true;
                echo json_encode($data);
                die;
            }
        }
    }

    function delete_vendor($id) {
        $conditions = $params = array();
        $conditions['v_id'] = $id;
        $params['single_row'] = true;
        $params['table'] = 'tbl_vendors';
        $info = $this->common_model->get_data($conditions, $params);
        $this->vendor_model->deleteRecordById($id);
        //$this->vendor_model->deleteRecordByEmail($info->email, $info->vendor_owner_id);
        if ($this->input->is_ajax_request()) {
            $success_message = 'selected record deleted successfully';
            $data['success'] = true;
            $data['success_message'] = $success_message;
            //   $data['scrollToElement'] = true;
            echo json_encode($data);
            die;
        }
    }

    public function update_vendor() {
        if (!empty($_POST['data'])) {
            $data = $_POST['data'];
            $service = $data[0];
            $cname = trim($data[7]);
            $email = trim($data[8]);
            $phone = trim($data[9]);
            $vid = trim($data[4]);
            $conditions = $params = array();
            $conditions['v_id'] = $vid;
            $params['single_row'] = true;
            $params['table'] = 'tbl_vendors';
            $info = $this->common_model->get_data($conditions, $params);
            $conditions = $params = array();
            $conditions['where']['email'] = $info->email;
            $conditions['where']['vendor_owner_id'] = $info->vendor_owner_id;
            $conditions['value']['company_name'] = $cname;
            $conditions['value']['mobile_no'] = $phone;
            $conditions['value']['email'] = $email;
            $conditions['value']['service'] = implode(",",$service);
            $params['table'] = 'tbl_vendors';
            $this->common_model->update($conditions, $params);
            $dat['success'] = true;
            $dat['message'] = 'Successfully updated';
            echo json_encode($dat);
            die;
        } else {
            $dat['success'] = false;
            $dat['message'] = 'Post data empty';
            echo json_encode($dat);
            die;
        }
    }

    public function new_vendor() {

        if ($this->input->post()) {
            $data = $_POST;
            //print_r($this->input->post); exit;
            foreach ($data['prop_id'] as $info) {
                $insert_vndors[] = array(
                    'vendor_owner_id' => $this->session->userdata('MEM_ID'),
                    'company_name' => $data['cname'][0],
                    'mobile_no' => $data['phone'][0],
                    'email' => $data['email'][0],
                    'service' => implode(",",$data['service_id'][0]),
                    'add_date' => time(),
                    'status' => 'Active',
                    'property_id' => $info
                );
            }
            $conditions = $params = array();
            $params['batch_mode'] = true;
            $conditions = $insert_vndors;
            $params['table'] = 'tbl_vendors';
            $this->common_model->insert_data($conditions, $params);
            $dat['success'] = true;
            $dat['message'] = 'Saved Successfully';
            echo json_encode($dat);
            die;
        } else {
            $dat['success'] = false;
            $dat['message'] = 'Post data empty';
            echo json_encode($dat);
            die;
        }
    }

    public function send_sms() {
        // Load the library, call by the name of the
        $this->load->helper('twilio_helper');  // references library/twilio_helper.php

        $service = get_twilio_service();
        try {
            $sms = $service->account->sms_messages->create("+19046743077", "+919417392737", "test message tenantag", array());
            echo '<pre>';
            print_r($sms);
            die;
        } catch (Exception $e) {
            // handle any error conditions here
        }
    }

    public function assign_vendor() {
        if ($this->input->post()) {
            $data = $_POST['vendors'];
            $vendors = array_filter($data);

            if (!empty($vendors)) {
                foreach ($vendors as $vndr) {
                    $conditions = $params = array();
                    $params['table'] = 'tbl_vendors';
                    $conditions['v_id'] = $vndr;
                    $params['single_row'] = true;
                    $params['fields'] = array('company_name', 'mobile_no', 'email', 'service');
                    $info = $this->common_model->get_data($conditions, $params);
                    $insert_vndors[] = array(
                        'vendor_owner_id' => $this->session->userdata('MEM_ID'),
                        'company_name' => $info->company_name,
                        'mobile_no' => $info->mobile_no,
                        'email' => $info->email,
                        'service' => $info->service,
                        'add_date' => time(),
                        'status' => 'Active',
                        'property_id' => $this->input->post('prp_id')
                    );
                }
            }
            $conditions = $params = array();
            $params['batch_mode'] = true;
            $conditions = $insert_vndors;
            $params['table'] = 'tbl_vendors';
            $this->common_model->insert_data($conditions, $params);
            $dat['success'] = true;
            $dat['success_message'] = 'Saved Successfully';
            echo json_encode($dat);
            die;
        } else {
            $dat['success'] = false;
            $dat['message'] = 'Post data empty';
            echo json_encode($dat);
            die;
        }
        echo '<pre>';
        print_r($_POST);
        exit;
    }


    public function updateVendorsOfProperty() {
         if (!empty($_POST)) {
            if (isset($_POST['vendor_ids']) && !empty($_POST['vendor_ids'])) {
                $dataarr = $_POST['vendor_ids'];
                $vendors = array_filter($dataarr);
            } else {
                $vendors = array();
            }

            if (isset($_POST['company_name']) && !empty($_POST['company_name'])) {
                $newvndr = $_POST['company_name'];
            } else {
                $newvndr = array();
            }          
            //$this->vendor_model->deleteVendorsOfProperty($this->session->userdata('PROPERTY_ID'));
            $conditions = $params = array();
            $params['table'] = 'tbl_vendors';
            $conditions['property_id'] = $this->input->post('property_id');
            $params['fields'] = array('v_id','company_name', 'mobile_no', 'email', 'service');
            $prop_ven_info = $this->common_model->get_data($conditions, $params);
            $old_vendors_of_prop = array();
            foreach ($prop_ven_info as $prop_vndr) {
                $old_vendors_of_prop[] = $prop_vndr->v_id;
            }
            //print_R($prop_ven_info);
            if (!empty($vendors)) {
                foreach ($vendors as $vndr) {
                    $conditions = $params = array();
                    $params['table'] = 'tbl_vendors';
                    $conditions['v_id'] = $vndr;
                    $params['single_row'] = true;
                    $params['fields'] = array('company_name', 'mobile_no', 'email', 'service');
                    $info = $this->common_model->get_data($conditions, $params);
                    $insert_vndors[] = array(
                        'vendor_owner_id' => $this->session->userdata('MEM_ID'),
                        'company_name' => $info->company_name,
                        'mobile_no' => $info->mobile_no,
                        'email' => $info->email,
                        'service' => $info->service,
                        'add_date' => time(),
                        'status' => 'Active',
                        'property_id' => $this->input->post('property_id')
                    );
                }
                $conditions = $params = array();
                $params['batch_mode'] = true;
                $conditions = $insert_vndors;
                $params['table'] = 'tbl_vendors';
                $this->common_model->insert_data($conditions, $params);
            }

            if (!empty($newvndr) && isset($_POST['service'])) {
                $cntc = $_POST['company_contact'];
                $vemail = $_POST['email'];
                $vservice = $_POST['service'];
                foreach ($newvndr as $key => $nwvndr) {
                    $insert_nvndors[] = array(
                        'vendor_owner_id' => $this->session->userdata('MEM_ID'),
                        'company_name' => $nwvndr,
                        'mobile_no' => $cntc[$key],
                        'email' => $vemail[$key],
                        'service' => implode(",",$vservice[$key]),
                        'add_date' => time(),
                        'status' => 'Active',
                        'property_id' => $this->input->post('property_id')
                    );
                }
                $conditions = $params = array();
                $params['batch_mode'] = true;
                $conditions = $insert_nvndors;
                $params['table'] = 'tbl_vendors';
                $this->common_model->insert_data($conditions, $params);
            }
            //$this->vendor_model->deleteVendorsOfPropertyByVendorIds($old_vendors_of_prop);
            
            $success_message = '<p>Information saved successfully.</p>';
            if ($this->input->is_ajax_request()) {               
                $data['success'] = true;
                $data['resetForm'] = true;
                $data['success_message'] = $success_message;                    
                $data['url'] = site_url('dashboard');
                $data['scrollToElement'] = true;
                echo json_encode($data);
                die;    
            }
        }    
    }

}
