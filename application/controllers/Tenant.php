<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Tenant extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('member_model');
        $this->load->model('properties_model');
        $this->mem_id = $this->session->userdata('MEM_ID');
        $this->common_model->checkMemberLogin();
        //   $this->session->unset_userdata('PROPERTY_ID');
        $this->checkFOrProperty();
    }

    //~ function checkFOrProperty()
    //~ {
    //~ $step_completed	=	$this->common_model->getLastProeprty();
    //~ if($step_completed && $step_completed!=3)
    //~ {
    //~ if($step_completed == 1)
    //~ redirect(site_url('add-property-step2'));
    //~ if($step_completed == 2)
    //~ redirect(site_url('add-property-step3'));
    //~ }	
    //~ else
    //~ {
    //~ $allProperties		=	$this->properties_model->frontGetCountAllPropertiesByLandlordId($this->session->userdata('MEM_ID'));
    //~ if(!$allProperties)
    //~ {
    //~ redirect(site_url('add-property'));
    //~ }
    //~ }
    //~ 
    //~ }
    //~ 

    function checkFOrProperty() {
        $allProperties = $this->properties_model->frontGetCountAllPropertiesByLandlordId($this->session->userdata('MEM_ID'));
        $step_completed = $this->common_model->getHigestCompletedStepProeprty();
        if (!$allProperties || ($allProperties == 1 && $step_completed < 3)) {
            redirect(site_url('add-property'));
        }
    }

    function index($type = NULL) {
        $output['selected_tab'] = 'tenants';
        $output['title'] = 'Tenants';
        /*         * **** function to get detail of login landlord **** */
        $memDetail = $this->member_model->getMemberInfo('mem_id', $this->mem_id);
        $output['memDetail'] = $memDetail;
        /*         * **** function to get all properties of login landlord ****  */
        $allProperties = $this->properties_model->frontGetAllPropertiesByLandlordId($this->mem_id);
        foreach ($allProperties as $value) {
            $value->tenants = $this->properties_model->getAllTenantsByProperty($value->prop_id);
            /*********************************** Tenant Due Amount************************************************/
            $value->due_rent_amount = 0;
            foreach ($value->tenants as $t_key => $t_value) {
                $due_info = getDuePaymentInfo($t_value->tenant_id);
                $t_value->due_rent_amount      = '$'.$due_info['due_amount']; 
                //$total_due_amount = $total_due_amount + $due_amount;
            }
        }

        $output['allProperties'] = $allProperties;

        //echo"<pre>";print_r($allProperties);die;
        $this->load->view($this->config->item('defaultfolder') . '/header', $output);
        $this->load->view($this->config->item('defaultfolder') . '/tenants');
        $this->load->view($this->config->item('defaultfolder') . '/tenant_js');
        $this->load->view($this->config->item('defaultfolder') . '/footer');
    }

    function edit($tenantId) {
        $mamberDetail = $this->member_model->getMemberRecordByIdAndType($tenantId, 'tenant');
        if (!empty($_POST)) {
            //echo"<pre>";print_r($_POST);die;
            $this->form_validation->set_message('required', '%s is required');
            $this->form_validation->set_rules('last_name_'.$tenantId, 'Last Name', 'trim|required');
            $this->form_validation->set_rules('middle_name_'.$tenantId, 'Middle Name', 'trim');
            $this->form_validation->set_rules('first_name_'.$tenantId, 'First Name ', 'trim|required');
            $this->form_validation->set_rules('dob_'.$tenantId, 'DOB ', 'trim|required');
            $this->form_validation->set_rules('preferred_lang_'.$tenantId, 'Language ', 'trim|required');
            if ($mamberDetail->email != $this->input->post('email_'.$tenantId))
                $this->form_validation->set_rules('email_'.$tenantId, 'Email ', 'trim|required|is_unique[tbl_members.email]');
            $this->form_validation->set_rules('mobile_no_'.$tenantId, 'Mobile ', 'trim|required');
            $this->form_validation->set_rules('lease_start_date_'.$tenantId, 'Lease Start Date ', 'trim|required');
            $this->form_validation->set_rules('lease_end_date_'.$tenantId, 'Lease End Date ', 'trim|required');
            $this->form_validation->set_rules('rent_amount_'.$tenantId, 'Rent Amount ', 'trim|required|numeric');
            $this->form_validation->set_rules('due_date_'.$tenantId, 'Due Date ', 'trim|required');
            // $this->form_validation->set_rules('late_fee', 'Late Fee ', 'trim|required|numeric');
            if ($this->form_validation->run()) {
                $failure = 0;
                $file_name = '';
                $config['upload_path'] = './assets/uploads/lease_documents/';
                $config['allowed_types'] = 'jpeg|jpg|png|txt|pdf|doc';
                $config['encrypt_name'] = TRUE;
                $this->load->library('upload', $config);
                if (!empty($_FILES['lease_' . $tenantId]['name'])) {
                    if (!$this->upload->do_upload('lease_' . $tenantId)) {
                        $error = $this->upload->display_errors();
                        $error_message.=$error;
                        $failure = TRUE;
                    } else {
                        $data = array('upload_data' => $this->upload->data());
                        $file_name = $data['upload_data']['file_name'];
                    }
                }
                if (!$failure) {
                    $this->member_model->updateTenantRecord($tenantId, $file_name);
                    $success_message = 'Tenant record updated successfully';
                }
            } else {
                $error_message = validation_errors();
                $failure = true;
            }
            if ($this->input->is_ajax_request()) {
                if ($failure) {
                    $data['success'] = false;
                    $error_message =strip_tags($error_message);
                    $data['error_message'] = $error_message;
                } else {
                    $data['rec'] = $tenantDetail = $this->member_model->getMemberRecordByIdAndType($tenantId, 'tenant');
                    $data['leaseDeatil'] = $leaseDetail = $this->properties_model->getLeaseDetailByTenantAndProperty($tenantId, $this->input->post('property_id_'.$tenantId));
                    $data['dob'] = date('m/d/Y', $tenantDetail->dob);
                    $data['lease_start_date'] = date('m/d/Y', strtotime($leaseDetail->lease_start_date));
                    $data['lease_end_date'] = date('m/d/Y', strtotime($leaseDetail->lease_end_date));
                    $data['due_date'] = date("m/" . $leaseDetail->due_date . "/Y");
                    $data['success'] = true;
                    $data['success_message'] = $success_message;
                }
                $data['scrollToElement'] = true;
                echo json_encode($data);
                die;
            }
        }
    }

    function add() {
        //echo '<pre>';print_r($_REQUEST); print_r($_FILES); die;
        if (!empty($_POST)) {
            $this->form_validation->set_message('required', '%s is required');
            $this->form_validation->set_rules('first_name', 'First Name ', 'trim|required');
            $this->form_validation->set_rules('middle_name', 'Middle Name ', 'trim');
            $this->form_validation->set_rules('last_name', 'Last Name', 'trim|required');
            $this->form_validation->set_rules('dob', 'DOB ', 'trim|required');
            $this->form_validation->set_rules('preferred_language', 'Language ', 'trim|required');
            $this->form_validation->set_rules('email', 'Email ', 'trim|required');
            //     $this->form_validation->set_rules('email', 'Email ', 'trim|required|is_unique[tbl_members.email]');
            $this->form_validation->set_rules('mobile_no', 'Mobile ', 'trim|required');
            $this->form_validation->set_rules('lease_start_date', 'Lease Start Date ', 'trim|required');
            $this->form_validation->set_rules('lease_end_date', 'Lease End Date ', 'trim|required');
            $this->form_validation->set_rules('rent_amount', 'Rent Amount ', 'trim|required|numeric');
            $this->form_validation->set_rules('due_date', 'Due Date ', 'trim|required');
            $this->form_validation->set_rules('late_fee', 'Late Fee ', 'trim|required|numeric'); 
            $this->form_validation->set_rules('late_fee_start', 'Late Fee Start', 'trim|required|numeric'); 
            if($this->input->post('homepage')){

            }else{
                if (empty($_FILES['upload_lease']['name'])){ 
                    //$this->form_validation->set_rules('upload_lease', 'Lease document', 'trim|required');
                } 
            }
            $file_name = '';
            if ($this->form_validation->run()) {
                $config['upload_path'] = './assets/uploads/lease_documents/';
                $config['allowed_types'] = 'jpeg|jpg|png|txt|pdf|doc';
                $config['encrypt_name'] = TRUE;
                $data = array();
                $failure = FALSE;
                $this->load->library('upload', $config);
                if (isset($_FILES) && !empty($_FILES['upload_lease']['name'])) {
                    if (!$this->upload->do_upload('upload_lease')) {
                        $error = $this->upload->display_errors();
                        $error_message =$error;
                        $failure = TRUE;
                    } else {
                        $data = array('upload_data' => $this->upload->data());
                        $file_name = $data['upload_data']['file_name'];
                    }
                }
                if (isset($failure) && $failure != TRUE) {

                	$this->load->helper('twilio_helper');  // references library/twilio_helper.php
	                $service = get_twilio_service();

					$strnew2 = 'abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
					$shuff2 = str_shuffle($strnew2);
					$shuff = str_shuffle($strnew2);
					$verification_code = substr($shuff2, 0, 8);
					$newPassword = substr($shuff, 0, 8); 
					$slug = $this->common_model->create_unique_slug_for_common($this->input->post('first_name'), 'tbl_members');

	                try {
	                    $mnumber = $this->input->post('mobile_no');
	                    $number = "+1" . preg_replace("/[^0-9]/", "", $mnumber);
	                    $playstore = PLAYSTORE;
	                    $appstore = APPSTORE;
						 

	                    //$msg = "Welcome Home! Your landlord has enrolled you in TenantTag. *Download your app Now. $appstore --iOS or $playstore --Android* ";
                        $playstore_img = $this->config->item("uploads").'icons/google-play.png';
                        $appstore_img = $this->config->item("uploads").'icons/app_store.png';
                        $playstore_img_html = '<a href="'.$playstore.'"><img src="'.$playstore_img.'"></a>';
                        $appstore_img_html = '<a href="'.$appstore.'"><img src="'.$appstore_img.'"></a>';
                        //$msg = "Welcome Home! Your landlord has enrolled you in TenantTag. *Download your app Now. $appstore_img_html --iOS or $playstore_img_html --Android* Username: " . $slug . " and Password: " . $newPassword;
                        
	                    $msg = "Welcome Home! Your \n landlord has enrolled you in \n TenantTag. Download your app for iOS or Android. \n \n iOS: $appstore \n \n Android: $playstore \n \n Username: " . $slug . "  \n \n Password: " . $newPassword;
	                    //$service->account->sms_messages->create("+19046743077", "$number", "$msg", array());
	                     $service->account->messages->create(array(
	                                'To' => $number,
	                                'From' => "+19046743077",
	                                'Body' => $msg));           
	                    
                        $this->member_model->updateAllTenantRentAndLeaseInfo();
	                    $last_id = $this->member_model->addTenant($file_name,$verification_code,$newPassword,$slug);
	                    $conditions = $params = array();
			            $conditions['value']['move_status'] = 1;
			            $conditions['where']['mem_id'] = $last_id;
			            $params['table'] = 'tbl_members';
			            $this->common_model->update($conditions, $params);
	                    $success_message = 'Tenant record added successfully';
	                } catch (Exception $e) {
	                    $file = 'sms_error_logs.txt';
	                    if (file_exists($file)) {
	                        $current = file_get_contents($file);
	                        $current .= $e->getMessage() . "\n";
	                        file_put_contents($file, $current);
	                        //echo $e->getMessage();
	                    }
	                     $error_message = $e->getMessage() . "\n";
	                    //exit;
	                }
                
                }
            } else {
                $error_message = validation_errors();
                $error_message = strip_tags($error_message);
                $failure = true;
            }
            if ($this->input->is_ajax_request()) {
                if (isset($failure) && $failure == TRUE) {
                    $data['success'] = false;
                    $data['error_message'] = $error_message;
                } else {
                	if(isset($last_id)){
                    	$data['mem_id']  = $last_id;
	                    $data['success'] = true;
	                    $data['resetForm'] = true;
	                    $data['success_message'] = $success_message;
                	}else{
                    	$data['success']  = false;                		
	                    $data['error_message'] = $error_message;
                	}
                    //   $data['selfReload'] = true;
                }
                $data['scrollToElement'] = true;
                echo json_encode($data);
                die;
            }
        }
    }

    public function upload_doc() {
        $failure = false;
        if (!empty($_FILES)) {
            $info = $this->input->post('prop_id');
            $attr = explode('_', $info);
            //$config['allowed_types'] = 'jpeg|jpg|png|pdf';
            $config['allowed_types'] = 'jpeg|jpg|png|txt|pdf|doc';
            $config['upload_path'] = './assets/uploads/lease_documents/';
            $config['encrypt_name'] = TRUE;
            $this->load->library('upload', $config);

            if (!empty($_FILES['file']['name'])) {
                if (!$this->upload->do_upload('file')) {
                    $error = $this->upload->display_errors();
                    $failure = TRUE;
                } else {
                    $conditions = $params = array();
                    $data = array('upload_data' => $this->upload->data());
                    $conditions['lease_doc'] = $data['upload_data']['file_name'];
                    $conditions['property_id'] = $attr[0];
                    $conditions['tenant_id'] = $attr[1];
                    $conditions['type'] = '0';
                    $conditions['status'] = 'Active';
                    $params['table'] = 'tbl_lease_docs';
                    $this->common_model->insert_data($conditions, $params);
                    $success_message =  $data['upload_data']['file_name'];
                }
            }
        } else {
            $failure = TRUE;
            $error = 'File not exist';
            
        }
        if (isset($failure) && $failure == TRUE) {
            $data['success'] = false;
            $data['error_message'] = strip_tags($error);
        } else {
            $data['success'] = true;
            $data['resetForm'] = true;
            $data['success_message'] = $success_message;
            //   $data['selfReload'] = true;
        }
        $data['scrollToElement'] = true;
        echo json_encode($data);
        die;
    }

    public function renew_lease() {
        $failure = FALSE;
        if (!empty($_FILES)) {
            $prp_id = $this->input->post('properti_id');
            $config['allowed_types'] = 'jpeg|jpg|png|txt|pdf|doc';
            $config['upload_path'] = './assets/uploads/lease_documents/';
            $config['encrypt_name'] = TRUE;
            $this->load->library('upload', $config);

            if (!empty($_FILES['file']['name'])) {
                if (!$this->upload->do_upload('file')) {
                    $error = $this->upload->display_errors();
                    $failure = TRUE;
                } else {
                    $conditions = $params = array();
                    $data = array('upload_data' => $this->upload->data());
                    $conditions['lease_doc'] = $data['upload_data']['file_name'];
                    $conditions['property_id'] = $this->input->post('properti_id');
                    $conditions['tenant_id'] = $this->input->post('tennant_id');
                    $conditions['status'] = 'Active';
                    $params['table'] = 'tbl_lease_docs';
                    $this->common_model->insert_data($conditions, $params);
                }
            }
        }
        $conditions = $params = array();
        $conditions['where']['property_id'] = $this->input->post('properti_id');
        $conditions['where']['tenant_id'] = $this->input->post('tennant_id');
        $conditions['where']['owner_id'] = $this->session->userdata('MEM_ID');
        //$conditions['value']['lease_start_date'] = strtotime($this->input->post('lease_start_date'));
        //$conditions['value']['lease_end_date'] = strtotime($this->input->post('lease_end_date'));
        $conditions['value']['lease_start_date'] = date('Y-m-d', strtotime($this->input->post('lease_start_date')));
        $conditions['value']['lease_end_date'] = date('Y-m-d', strtotime($this->input->post('lease_end_date')));

        $conditions['value']['rent_amount'] = $this->input->post('rent_amount');
        $conditions['value']['due_date'] = $this->input->post('due_date');
        $conditions['value']['late_fee'] = $this->input->post('late_fee');
        $conditions['value']['late_fee_type'] = $this->input->post('late_fee_type');
        $params['table'] = 'tbl_properties_lease_detail';
        $this->common_model->update($conditions, $params);
        if ($failure) {
            $dat['success'] = false;
            $dat['message'] = 'Fail to upload document';
        } else {
            $dat['success'] = true;
            $dat['message'] = 'Successfully updated';
        }
        echo json_encode($dat);
        die;
    }

    public function check_in() {
        $failure = FALSE;
        $dat = array();
        $conditions = $params = array();
        $params['complex'] = true;

        $conditions['tables'] = array(
            'lease' => 'tbl_properties_lease_detail',
            'tenant' => 'tbl_members',
        );
        $conditions['table'] = 'lease';
        $conditions['on']['tenant'] = array(
            'sign' => '=',
            'column' => 'mem_id',
            'alias_column' => 'tenant_id',
            'alias_other' => 'lease',
        );
        $conditions['where']['mem_id']['alias'] = 'tenant';
        $conditions['where']['mem_id']['value'] = $this->input->post('id');
        $params['single_row'] = true;
        $params['fields'] = array('lease.property_id','tenant.*');
        $details = $this->common_model->get_data($conditions, $params);
        $property_id = $details->property_id;
        $propertyDetail = $this->properties_model->getPropertyById($property_id);
        //print_r($propertyDetail); exit;
        //if($propertyDetail->step_completed >= 3){
            $this->load->helper('twilio_helper');  // references library/twilio_helper.php
            $service = get_twilio_service();
            try {
                $mnumber = $details->mobile_no;
                $number = "+1" . preg_replace("/[^0-9]/", "", $mnumber);
                $playstore = PLAYSTORE;
                $appstore = APPSTORE;

                $playstore_img = $this->config->item("uploads").'icons/google-play.png';
                $appstore_img = $this->config->item("uploads").'icons/app_store.png';
                $playstore_img_html = '<a href="'.$playstore.'"><img src="'.$playstore_img.'"></a>';
                $appstore_img_html = '<a href="'.$appstore.'"><img src="'.$appstore_img.'"></a>';
                //$msg = "Welcome Home! Your landlord has enrolled you in TenantTag. *Download your app Now. $appstore_img --iOS or $playstore_img --Android* Username: " . $details->slug . " and Password: " . $details->tpass;
                

                $msg = "Welcome Home! Your \n landlord has enrolled you in \n TenantTag. Download your app for iOS or Android. \n \n iOS: $appstore \n \n Android: $playstore \n \n Username: " . $details->slug . "  \n \n Password: " . $details->tpass;
                //$service->account->sms_messages->create("+19046743077", "$number", "$msg", array());
                 $service->account->messages->create(array(
                            'To' => $number,
                            'From' => "+19046743077",
                            'Body' => $msg));

                $conditions = $params = array();
                $conditions['value']['move_status'] = 1;
                $conditions['where']['mem_id'] = $details->mem_id;
                $params['table'] = 'tbl_members';
                $this->common_model->update($conditions, $params);

                $dat['success'] = TRUE;
                $dat['message'] = 'Credentials sent to tenant via sms';
                $dat['redirect'] = FALSE;
            } catch (Exception $e) {
                $file = 'sms_error_logs.txt';
                if (file_exists($file)) {
                    $current = file_get_contents($file);
                    $current .= $e->getMessage() . "\n";
                    file_put_contents($file, $current);
                    //echo $e->getMessage();
                }
                $dat['success'] = FALSE;
                $dat['redirect'] = FALSE;
                $dat['message'] = $e->getMessage();
            }            
        /*}else{
            $dat['success'] = TRUE;
            $dat['redirect'] = TRUE;
            $dat['message'] = site_url('add-property');
        }*/
        echo json_encode($dat);
        exit;
    }


    public function check_in_using_popup() {
        $failure = FALSE;
        $dat = array();
        $conditions = $params = array();
        $params['complex'] = true;

        $conditions['tables'] = array(
            'lease' => 'tbl_properties_lease_detail',
            'tenant' => 'tbl_members',
        );
        $conditions['table'] = 'lease';
        $conditions['on']['tenant'] = array(
            'sign' => '=',
            'column' => 'mem_id',
            'alias_column' => 'tenant_id',
            'alias_other' => 'lease',
        );
        $conditions['where']['mem_id']['alias'] = 'tenant';
        $conditions['where']['mem_id']['value'] = $this->input->post('id');
        $params['single_row'] = true;
        $params['fields'] = array('lease.property_id','tenant.*');
        $details = $this->common_model->get_data($conditions, $params);
        $property_id = $details->property_id;
        $propertyDetail = $this->properties_model->getPropertyById($property_id);
        //print_r($propertyDetail); exit;
        //if($propertyDetail->step_completed >= 3){
            $this->load->helper('twilio_helper');  // references library/twilio_helper.php
            $service = get_twilio_service();
            try {
                $mnumber = $details->mobile_no;
                $number = "+1" . preg_replace("/[^0-9]/", "", $mnumber);
                $playstore = PLAYSTORE;
                $appstore = APPSTORE;

                $playstore_img = $this->config->item("uploads").'icons/google-play.png';
                $appstore_img = $this->config->item("uploads").'icons/app_store.png';
                $playstore_img_html = '<a href="'.$playstore.'"><img src="'.$playstore_img.'"></a>';
                $appstore_img_html = '<a href="'.$appstore.'"><img src="'.$appstore_img.'"></a>';
                //$msg = "Welcome Home! Your landlord has enrolled you in TenantTag. *Download your app Now. $appstore_img --iOS or $playstore_img --Android* Username: " . $details->slug . " and Password: " . $details->tpass;
                

                $msg = "Welcome Home! Your \n landlord has enrolled you in \n TenantTag. Download your app for iOS or Android. \n \n iOS: $appstore \n \n Android: $playstore \n \n Username: " . $details->slug . "  \n \n Password: " . $details->tpass;
                //$service->account->sms_messages->create("+19046743077", "$number", "$msg", array());
                 $service->account->messages->create(array(
                            'To' => $number,
                            'From' => "+19046743077",
                            'Body' => $msg));

                $conditions = $params = array();
                $conditions['value']['move_status'] = 1;
                $conditions['where']['mem_id'] = $details->mem_id;
                $params['table'] = 'tbl_members';
                $this->common_model->update($conditions, $params);

                echo 'success'; exit;
            } catch (Exception $e) {
                $file = 'sms_error_logs.txt';
                if (file_exists($file)) {
                    $current = file_get_contents($file);
                    $current .= $e->getMessage() . "\n";
                    file_put_contents($file, $current);
                    //echo $e->getMessage();
                }
                echo $e->getMessage(); exit;
            }            
        /*}else{
            $dat['success'] = TRUE;
            $dat['redirect'] = TRUE;
            $dat['message'] = site_url('add-property');
        }*/
        echo json_encode($dat);
        exit;
    }


    public function welcome_text_send_to_tenant() {

        if(!empty($this->input->post('prop_id'))){
            $tenants = $this->properties_model->getAllTenantsByProperty($this->input->post('prop_id'));
            $message_data = '';
            foreach ($tenants as $key => $value) {
                $failure = FALSE;
                $conditions = $params = array();
                $params['complex'] = true;

                $conditions['tables'] = array(
                    'lease' => 'tbl_properties_lease_detail',
                    'tenant' => 'tbl_members',
                );
                $conditions['table'] = 'lease';
                $conditions['on']['tenant'] = array(
                    'sign' => '=',
                    'column' => 'mem_id',
                    'alias_column' => 'tenant_id',
                    'alias_other' => 'lease',
                );
                $conditions['where']['mem_id']['alias'] = 'tenant';
                $conditions['where']['mem_id']['value'] = $value->tenant_id;
                $params['single_row'] = true;
                $params['fields'] = array('tenant.*');
                $details = $this->common_model->get_data($conditions, $params);
                $this->load->helper('twilio_helper');  // references library/twilio_helper.php
                $service = get_twilio_service();
                try {
                    $mnumber = $details->mobile_no;
                    $number = "+1" . preg_replace("/[^0-9]/", "", $mnumber);
                    $playstore = PLAYSTORE;
                    $appstore = APPSTORE;
                    $playstore_img = $this->config->item("uploads").'icons/google-play.png';
                    $appstore_img = $this->config->item("uploads").'icons/app_store.png';
                    $playstore_img_html = '<a href="'.$playstore.'"><img src="'.$playstore_img.'"></a>';
                    $appstore_img_html = '<a href="'.$appstore.'"><img src="'.$appstore_img.'"></a>';
                    //$msg = "Welcome Home! Your landlord has enrolled you in TenantTag. *Download your app Now. $appstore_img --iOS or $playstore_img --Android* ";
                    
                    //$msg = "Welcome Home! Your landlord has enrolled you in TenantTag. *Download your app Now. $appstore --iOS or $playstore --Android* ";
                    $msg = "Welcome Home! Your \n landlord has enrolled you in \n TenantTag. Download your app for iOS or Android. \n \n iOS: $appstore \n \n Android: $playstore  \n \n Username:" . $details->slug . "  \n \n Password: " . $details->tpass;
                    //$service->account->sms_messages->create("+19046743077", "$number", "$msg", array());
                     $service->account->messages->create(array(
                                'To' => $number,
                                'From' => "+19046743077",
                                'Body' => $msg));           
                    //echo 'success';
                    //exit;
                } catch (Exception $e) {
                    $file = 'sms_error_logs.txt';
                    if (file_exists($file)) {
                        $current = file_get_contents($file);
                        $current .= $e->getMessage() . "\n";
                        file_put_contents($file, $current);
                        //echo $e->getMessage();
                    }
                    //exit;
                }            
            }
        }
    }

    public function welcome_text_send_using_prop() {

        if(!empty($this->input->post('prop_id'))){
            $properties = explode(',', $this->input->post('prop_id'));
            foreach ($properties as $prop_key => $prop_value) {
                $prop_id = $prop_value;
                $tenants = $this->properties_model->getAllTenantsByProperty($prop_id);
                $message_data = '';
                foreach ($tenants as $key => $value) {
                    $failure = FALSE;
                    $conditions = $params = array();
                    $params['complex'] = true;

                    $conditions['tables'] = array(
                        'lease' => 'tbl_properties_lease_detail',
                        'tenant' => 'tbl_members',
                    );
                    $conditions['table'] = 'lease';
                    $conditions['on']['tenant'] = array(
                        'sign' => '=',
                        'column' => 'mem_id',
                        'alias_column' => 'tenant_id',
                        'alias_other' => 'lease',
                    );
                    $conditions['where']['mem_id']['alias'] = 'tenant';
                    $conditions['where']['mem_id']['value'] = $value->tenant_id;
                    $params['single_row'] = true;
                    $params['fields'] = array('tenant.*');
                    $details = $this->common_model->get_data($conditions, $params);
                    $this->load->helper('twilio_helper');  // references library/twilio_helper.php
                    $service = get_twilio_service();
                    try {
                        $mnumber = $details->mobile_no;
                        $number = "+1" . preg_replace("/[^0-9]/", "", $mnumber);
                        $playstore = PLAYSTORE;
                        $appstore = APPSTORE;
                        $playstore_img = $this->config->item("uploads").'icons/google-play.png';
                        $appstore_img = $this->config->item("uploads").'icons/app_store.png';
                        $playstore_img_html = '<a href="'.$playstore.'"><img src="'.$playstore_img.'"></a>';
                        $appstore_img_html = '<a href="'.$appstore.'"><img src="'.$appstore_img.'"></a>';
                        //$msg = "Welcome Home! Your landlord has enrolled you in TenantTag. *Download your app Now. $appstore_img --iOS or $playstore_img --Android* ";
                        
                        //$msg = "Welcome Home! Your landlord has enrolled you in TenantTag. *Download your app Now. $appstore --iOS or $playstore --Android* ";
                        $msg = "Welcome Home! Your \n landlord has enrolled you in \n TenantTag. Download your app for iOS or Android. \n \n iOS: $appstore \n \n Android: $playstore  \n \n Username:" . $details->slug . "  \n \n Password: " . $details->tpass;
                        //$service->account->sms_messages->create("+19046743077", "$number", "$msg", array());
                         $service->account->messages->create(array(
                                    'To' => $number,
                                    'From' => "+19046743077",
                                    'Body' => $msg));           
                        //echo 'success';
                        //exit;
                    } catch (Exception $e) {
                        $file = 'sms_error_logs.txt';
                        if (file_exists($file)) {
                            $current = file_get_contents($file);
                            $current .= $e->getMessage() . "\n";
                            file_put_contents($file, $current);
                            //echo $e->getMessage();
                        }
                        //exit;
                    }            
                }
            }
        }
    }
    

    public function move_out() {
        $tenants = $this->input->post('tenants');
        $msg = $this->input->post('message') . " -" . SIGNATURE;
        if (!empty($tenants)) {
            foreach ($tenants as $tenant) {
                $conditions = $params = array();
                $params['complex'] = true;

                $conditions['tables'] = array(
                    'lease' => 'tbl_properties_lease_detail',
                    'tenant' => 'tbl_members',
                );
                $conditions['table'] = 'lease';
                $conditions['on']['tenant'] = array(
                    'sign' => '=',
                    'column' => 'mem_id',
                    'alias_column' => 'tenant_id',
                    'alias_other' => 'lease',
                );
                $conditions['where']['mem_id']['alias'] = 'tenant';
                $conditions['where']['mem_id']['value'] = $tenant;
                $params['single_row'] = true;
                $params['fields'] = array('tenant.*');
                $details = $this->common_model->get_data($conditions, $params);
                $this->load->helper('twilio_helper');  // references library/twilio_helper.php
                $service = get_twilio_service();
                try {
                    $mnumber = $details->mobile_no;
                    $number = "+1" . preg_replace("/[^0-9]/", "", $mnumber);
                    $service->account->sms_messages->create("+19046743077", "$number", "$msg", array());
                } catch (Exception $e) {
                    $file = 'sms_error_logs.txt';
                    if (file_exists($file)) {
                        $current = file_get_contents($file);
                        $current .= $e->getMessage() . "\n";
                        file_put_contents($file, $current);
                    }
                }
                $conditions = $params = array();
                $conditions['value']['move_status'] = 2;
                $conditions['value']['status'] = 'inactive';
                $conditions['where']['mem_id'] = $details->mem_id;
                $params['table'] = 'tbl_members';
                $this->common_model->update($conditions, $params);
            }
        }
        echo 'success';
        exit;
    }

    public function move_in() {
        $tenants = $this->input->post('tenants');
        $msg = $this->input->post('message') . " -" . SIGNATURE;
        if (!empty($tenants)) {
            foreach ($tenants as $tenant) {
                $conditions = $params = array();
                $params['complex'] = true;

                $conditions['tables'] = array(
                    'lease' => 'tbl_properties_lease_detail',
                    'tenant' => 'tbl_members',
                );
                $conditions['table'] = 'lease';
                $conditions['on']['tenant'] = array(
                    'sign' => '=',
                    'column' => 'mem_id',
                    'alias_column' => 'tenant_id',
                    'alias_other' => 'lease',
                );
                $conditions['where']['mem_id']['alias'] = 'tenant';
                $conditions['where']['mem_id']['value'] = $tenant;
                $params['single_row'] = true;
                $params['fields'] = array('tenant.*');
                $details = $this->common_model->get_data($conditions, $params);
                $this->load->helper('twilio_helper');  // references library/twilio_helper.php
                $service = get_twilio_service();
                try {
                    $mnumber = $details->mobile_no;
                    $number = "+1" . preg_replace("/[^0-9]/", "", $mnumber);
                    $service->account->sms_messages->create("+19046743077", "$number", "$msg", array());
                } catch (Exception $e) {
                    $file = 'sms_error_logs.txt';
                    if (file_exists($file)) {
                        $current = file_get_contents($file);
                        $current .= $e->getMessage() . "\n";
                        file_put_contents($file, $current);
                    }
                }
                $conditions = $params = array();
                $conditions['value']['move_status'] = 1;
                $conditions['value']['status'] = 'active';
                $conditions['where']['mem_id'] = $details->mem_id;
                $params['table'] = 'tbl_members';
                $this->common_model->update($conditions, $params);
            }
        }
        echo 'success';
        exit;
    }

    public function delete_tenant() {
        if ($this->input->post()) {
            $conditions = $params = array();
            $conditions['where']['mem_id'] = $this->input->post('id');
            $conditions['value']['status'] = 'inactive';
            $conditions['value']['account_closed_by_member'] = 'Yes';
            $params['table'] = 'tbl_members';
            $this->common_model->update($conditions, $params);
        }exit;
    }


    function getRentDueAmount(){
    	$tenant_id = $this->input->post('id');
        $due_info = getDuePaymentInfo($tenant_id);
        $due_amount      = '$'.$due_info['due_amount']; 
        $due_date_formated      = $due_info['due_date']; 
        $conditions = $params = array();
        $params['table']        = 'tbl_payment';
        $conditions['mem_id']   = $tenant_id;
        $params['order_by']     = 'id DESC';
        $params['single_row']   = TRUE;
        $last_transaction       = $this->common_model->get_data($conditions, $params);

        $html = '';
        $html .= '<div class="due_container">';
	        $html .= '<div class="due_container_left due_container_left_header">';
	        	$html .= 'Due Date';
	        $html .= '</div>';
	        $html .= '<div class="due_container_right due_container_right_header">';
	        	$html .= 'Due Amount';
	        $html .= '</div>';
	        $html .= '<div class="due_container_left">';
	        	$html .= $due_date_formated;
	        $html .= '</div>';
	        $html .= '<div class="due_container_right">';
	        	$html .= $due_amount;
	        $html .= '</div>';
        $html .= '</div';
        //$result = array('due_date' => $due_date_formated, 'due_amount' => $due_amount,'last_transaction' => $last_transaction);
        //echo json_encode($result);
        echo $html;
        exit;

    }



        /**
     * Method Name : getSynapseDetails
     * Author Name : Lakhvinder Singh
     * Description : return details of particular user Synapse account
     */
    function getSynapseDetails($landlord_id) {
        $conditions = $params = array();
        $params['complex'] = true;
        $params['single_row'] = true;
        $conditions['table'] = 'linked_account_info';
        $conditions['where']['user_id']['alias'] = 'linked_account_info';
        $conditions['where']['user_id']['value'] = $landlord_id;

        $params['single_row'] = TRUE;
        $params['fields'] = array('prop_id','oid','account_number','bank_name','class');
        $info = $this->common_model->get_data($conditions, $params);
       // echo $this->db->last_query(); //exit;
        return $info;
    }

    function getTenantAccountInfo(){
        $tenant_id = $this->input->post('id');
        $conditions = $params = array();
        $params['table'] = 'tbl_synapsepay_card_info';
        $conditions['user_id'] = $tenant_id;
        $conditions['is_deleted'] = 1;
        $params['order_by']     = 'id DESC';
        //$params['single_row'] = true;
        $info = $this->common_model->get_data($conditions, $params);
        if(!empty($info)){
            $html = '';
            $html .= '<div class="account_container">';
                $html .= '<div class="account_container_left account_container_left_header">';
                    $html .= 'Routing';
                $html .= '</div>';
                $html .= '<div class="account_container_right account_container_right_header">';
                    $html .= 'Account number';
                $html .= '</div>';
                foreach ($info as $key => $value) {
                    $html .= '<div class="account_container_left">';
                        $html .= $value->routing_number;
                    $html .= '</div>';
                    $html .= '<div class="account_container_right">';
                        $html .= $value->account_number;
                    $html .= '</div>';
                }
            $html .= '</div';
        }else{
            $routing_no ='Not Available'; 
            $account_no ='Not Available';           
            $html = '';
            $html .= '<div class="account_container">';
                $html .= '<div class="account_container_left account_container_left_header">';
                    $html .= 'Routing';
                $html .= '</div>';
                $html .= '<div class="account_container_right account_container_right_header">';
                    $html .= 'Account number';
                $html .= '</div>';
                $html .= '<div class="account_container_left">';
                    $html .= $routing_no;
                $html .= '</div>';
                $html .= '<div class="account_container_right">';
                    $html .= $account_no;
                $html .= '</div>';
            $html .= '</div';
        }

        $data = json_encode(array('html' => $html, 'tenant_id'=>$tenant_id));
        echo $data;
        exit;
    }

    function addmemo(){
        if (!empty($_POST)) {
            $this->form_validation->set_message('required', '%s is required');
            $this->form_validation->set_rules('issue_amount', 'Amount ', 'trim|required|numeric');
            $this->form_validation->set_rules('issue_tenant_id', 'Tenant', 'required');
            $file_name = '';
            if ($this->form_validation->run()) {
                $current_month = date('m');
                $current_year = date('Y');
                $due_info = getDuePaymentInfo($this->input->post('issue_tenant_id'));
                $duePayment      = '$'.$due_info['due_amount']; 
                if($this->input->post('issue_memo') == 'credit_memo'){
                	if($due_info['due_amount'] >= $this->input->post('issue_amount')){
                		$conditions = $params = array();
	                    $conditions['issue_memo_month'] = $current_month;
	                    $conditions['issue_memo_year'] = $current_year;
	                    $conditions['tenant_id'] = $this->input->post('issue_tenant_id');
	                    $conditions['amount'] = $this->input->post('issue_amount');
	                    $conditions['issue_memo'] = $this->input->post('issue_memo');
	                    $conditions['landlord_id'] = $this->session->userdata('MEM_ID');
	                    $params['table'] = 'tbl_issue_memo';
	                    $failure = FALSE;
	                    $success_message = "Updated";
	                    $this->common_model->insert_data($conditions, $params);
                	}else{
                		$failure = true;
                        $error_message = 'Issue credit amount can not more then rent amount due';
                	}
                }else{
                	$conditions = $params = array();
                    $conditions['issue_memo_month'] = $current_month;
                    $conditions['issue_memo_year'] = $current_year;
                    $conditions['tenant_id'] = $this->input->post('issue_tenant_id');
                    $conditions['amount'] = $this->input->post('issue_amount');
                    $conditions['issue_memo'] = $this->input->post('issue_memo');
                    $conditions['landlord_id'] = $this->session->userdata('MEM_ID');
                    $params['table'] = 'tbl_issue_memo';
                    $failure = FALSE;
                    $success_message = "Updated";
                    $this->common_model->insert_data($conditions, $params);

                }
                
                
            } else {
                $failure = true;
                $error_message = validation_errors();
                $error_message = strip_tags($error_message);
            }
            if ($this->input->is_ajax_request()) {
                if (isset($failure) && $failure == TRUE) {
                    $data['success'] = false;
                    $data['error_message'] = $error_message;
                } else {
                    $data['success'] = true;
                    $data['resetForm'] = true;
                    $data['success_message'] = $success_message;                    
                    //   $data['selfReload'] = true; 
                }
                $data['scrollToElement'] = true;
                echo json_encode($data);
                die;
            }
        }
    }

}
