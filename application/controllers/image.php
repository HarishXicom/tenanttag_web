<?php
if (!defined('BASEPATH'))exit('No direct script access allowed');

class Image extends CI_Controller {

	
    var $useCache=true;
    var $quality=100;
    var $compress=null;
    var $sharpen=true;
    var $image_path='assets/';
    var $verbose=false;
    var $cache_path='./application/cache';
    var $saveAs="img.jpg";
    var $cropToFit=false;
    var $keepRatio=true;
    var $useOriginal=true;
    function __construct()
	{
	   //echo "testing";
	   //error_reporting(E_ALL);
	   parent::__construct();
	   $this->load->library('cimage');
	}
    public function resize() {
     $srcImage = 'assets/'.$this->uri->uri_string();
     $pathinfo = pathinfo($srcImage);

		//echo $this->input->get('t');die;
       $size = end(explode("-", $pathinfo["filename"]));
       $mainimg=str_ireplace("-" . $size, "", $pathinfo["basename"]);
        $original = $pathinfo["dirname"] . "/" . str_ireplace("-" . $size, "", $pathinfo["basename"]);
		echo $original;die;
        // original image not found, show 404
        if (!file_exists($original))
        {
            show_404($original);
        }



        if (stripos($size, "x") !== FALSE) {
            // dimensions are provided as size
            @list($width, $height) = explode("x", $size);

            // security check, to avoid users requesting random sizes

        } else if (isset($sizes[$size])) {
            // optional, the preset is provided instead of the dimensions
            // NOTE: the controller will be executed EVERY time you request the image this way
            @list($width, $height) = $sizes[$size];
            $allowed = TRUE;

            // set the correct output path
            $path = str_ireplace($size, $width . "x" . $height, $path);
        }
        if($width==0)
        {
	       $width=null;

        }
        if($height==0)
        {
	        $height=null;
        }
        $newWidth = $width;
        $sizes=$this->size_constant();
        if (isset($sizes[$newWidth]))
         {
			 $newWidth = $sizes[$newWidth];
         }


         $newHeight =$height;

         if (isset($sizes[$newHeight]))
         {
           $newHeight = $sizes[$newHeight];
         }


         //$aspectRatio='golden';
         $aspectRatio = $this->get(array('aspect-ratio', 'ar'));
         $aspectRatios =$this->aspect_ratio_constant();
         $negateAspectRatio = ($aspectRatio[0] == '!') ? true : false;
         $aspectRatio = $negateAspectRatio ? substr($aspectRatio, 1) : $aspectRatio;


        $this->cropToFit = $this->getDefined(array('crop-to-fit', 'cf'), true, false);

         $this->keepRatio = $this->getDefined(array('no-ratio', 'nr', 'stretch'), false, true);
        $crop = $this->get(array('crop', 'c'));
        $area = $this->get(array('area', 'a'));


         if (isset($aspectRatios[$aspectRatio])) {
			 $aspectRatio = $aspectRatios[$aspectRatio];
			}

		if ($negateAspectRatio) {
			    $aspectRatio = 1 / $aspectRatio;
			}
        // crop-to-fit&area=25,20,20,35
         //$this->cropToFit=true;
        // $area='0,0.5,0.5,0.5';
         //echo "testing";die;
        // echo $this->quality;die;
          $this->cimage->setVerbose($this->verbose)
                       ->setSource($mainimg, $pathinfo["dirname"])
                       ->setOptions(
							        array
							        (
							          // Options for calculate dimensions
								          'newWidth'  => $newWidth,
								          'newHeight' => $newHeight,
								          'aspectRatio' => $aspectRatio,
								          'keepRatio' => $this->keepRatio,
								          'cropToFit' => $this->cropToFit,
								          'crop'      => $crop,
								          'area'      => $area,

								          // Pre-processing, before resizing is done
								          'scale'     => $scale,

								          // Post-processing, after resizing is done
								          'palette'   => $palette,
								          'filters'   => $filters,
								          'sharpen'   => $this->sharpen,
								          'emboss'    => $emboss,
								          'blur'      => $blur,
							        )
							     )
					->initDimensions()
					->calculateNewWidthAndHeight()
				    //->setSaveAsExtension($this->saveAs)
				    ->setJpegQuality($this->quality)
				    ->setPngCompression($this->compress)
				    ->useOriginalIfPossible($this->useOriginal)
				    ->generateFilename($this->cache_path)
				    ->useCacheIfPossible($this->useCache)
				    ->load()
				    ->preResize()
				    ->resize()
				    ->postResize()
				    ->setPostProcessingOptions($this->postprocessing())
				    ->save()
				    ->output();


    }
     function postprocessing()
     {
	    return array('png_filter'        => false,
        'png_filter_cmd'    => '/usr/local/bin/optipng -q',

        'png_deflate'       => false,
        'png_deflate_cmd'   => '/usr/local/bin/pngout -q',

        'jpeg_optimize'     => false,
        'jpeg_optimize_cmd' => '/usr/local/bin/jpegtran -copy none -optimize');

     }
     function size_constant()
     {
	     $sizes = array(
          'w1' => 613,
          'w2' => 630,
        );

        // Add grid column width, useful for use as predefined size for width (or height).
        $gridColumnWidth = 30;
        $gridGutterWidth = 10;
        $gridColumns     = 24;

        for ($i = 1; $i <= $gridColumns; $i++) {
            $sizes['c' . $i] = ($gridColumnWidth + $gridGutterWidth) * $i - $gridGutterWidth;
        }

        return $sizes;
     }
     function get($key, $default = null)
		{
		    if (is_array($key))
		    {
		        foreach ($key as $val)
		         {
		            if (($this->input->get($val)!=""))
		             {
		                return $this->input->get($val);
		            }
		        }
		    }
		    elseif ($this->input->get($key)!=""){
		        return $this->input->get($key);
		    }
		    return $default;
		}
	 function getDefined($key, $defined, $undefined)
	{
	    return $this->get($key) === null ? $undefined : $defined;
	}
     function aspect_ratio_constant()
     {
	     return array(
            '3:1'   => 3/1,
            '3:2'   => 3/2,
            '4:3'   => 4/3,
            '8:5'   => 8/5,
            '16:10' => 16/10,
            '16:9'  => 16/9,
            'golden' => 1.618,
        );

     }



    }
