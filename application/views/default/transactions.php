
  <table class="trans_table" border="1" style="width:100%">
    <tr>
        <th> Transaction Id</th>
        <th> User Name</th>
        <th> Property</th>
        <th> Transaction Amount</th>
        <th> Transaction Type</th>
        <th> Status</th>
        <th> Date</th>
    </tr>

        <?php foreach ($transactions as $key => $value) { ?>
            <tr class="list-item">
                <td><?php echo $value['transaction_id']; ?></td>
                <td><?php echo getUserName($value['mem_id']); ?></td>
                 <td>
                        <?php 
                        $pr_array = explode(',',$value['property_id']);
                        foreach ($pr_array as $pr_array_key => $pr_array_value) {                                                                
                            echo getPropertyAddress($value['property_id']).' <br> '; 
                        }
                        ?>
               </td>
                <td><?php echo amount_format_view($value['amount']); ?></td>
                <td><?php echo ($value['user_type'] == 'Tenant') ? 'Rent' : 'Subscription Fee'; ?> </td>
                <td><?php echo $value['status']; ?></td>
                <td><?php echo $value['modified_date']; ?></td>
            </tr>
            
        <?php } ?>
<div class="clearfix"></div>
</table>
  <?php echo $this->ajax_pagination->create_links(); ?>

