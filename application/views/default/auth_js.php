<script>
    $(document).ready(function () {        
       
        //edit user
        $("#sign-up").validate({
            // Specify the validation rules
            rules: {
                /*last_name: {
                    required: true,
                    customvalidation: true,
                    maxlength: 20,
                },
                first_name: {
                    required: true,
                    customvalidation: true,
                    maxlength: 20,
                },
                email: {
                    required: true,
                    email: true
                },
                password: {
                    required: true,
                    minlength: 6,
                },
                confirm_password: {
                    required: true,
                    equalTo: "#signPassword",
                },
                mobile: {
                    required: true,
                //    phoneUS: true
                },
                country: {
                    valueNotEquals: "",
                },
                state: {
                    valueNotEquals: "",
                },
                country_code: {
                    required: true,
                },*/
                terms: {
                    required: true,
                }
            },
            // Specify the validation error messages
            messages: {
                /*last_name: {
                    required: 'Enter last name',
                    customvalidation: 'Only alphabets allowed'
                },
                first_name: {
                    required: 'Enter first name',
                    customvalidation: 'Only alphabets allowed'
                },
                mobile: {
                    required: 'Enter mobile number',
                },
                email: {
                    email: 'Enter valid email'
                },
                country: {
                    valueNotEquals: 'Select country',
                },
                country_code: {
                    required: 'Enter country code',
                },
                state: {
                    valueNotEquals: 'Select state',
                },
                password: {
                    required: "Provide a password",
                },
                confirm_password: {
                    required: "Provide a password",
                    equalTo: "Enter the same password as left",
                },*/
                terms: {
                    required: "Please accept the Terms & Conditions Policy.",
                }
            },
            submitHandler: function (form) {
                var posturl = siteUrl + 'register';
                var data = $('#sign-up').serialize();
                var formClass = '#sign-up';
                $('#TemsAndConditionModal').modal('hide');
                $.ajax({
                    url: posturl,
                    data: data,
                    dataType: 'json',
                    type: "POST",
                    beforeSend: function () {
                        $('#wait-div').show();
                    },
                    success: function (response) {
                        $('#wait-div').hide();
                        $(formClass).find('.ajax_report').removeClass('alert-success').removeClass('alert-danger').fadeIn(200);
                        $('wait_div').hide();
                        if (response.success)
                            $(formClass).find('.ajax_report').addClass('alert-success').children('.ajax_message').html(response.success_message);
                        else
                            $(formClass).find('.ajax_report').addClass('alert-danger').children('.ajax_message').html(response.error_message);
                        if (response.url)
                        {
                            setTimeout(function () {
                                window.location.href = response.url;
                            }, 700);
                        }
                        if (response.resetForm)
                            $(formClass).resetForm();
                        if (response.selfReload)
                        {
                            //location.reload();
                            setTimeout(function () {
                                location.reload();
                            }, 700);
                        }
                        if (response.scrollToElement)
                            scrollToElement(formClass, 1000);
                        if (response.ajaxCallBackFunction)
                            ajaxCallBackFunction(response);
                        setTimeout(function () {
                            $(formClass).find('.ajax_report').fadeOut(500);
                        }, 5000);
                    },
                    error: function (data) {
                        alert("Server Error.");
                        return false;
                    }
                });

                return false;
            }
        });



        jQuery.validator.addMethod("valueNotEquals", function (value, element, arg) {
            return arg != value;
        }, "Value must not equal arg.");
        jQuery.validator.addMethod("customvalidation", function (value, element) {
            return /^[A-Za-z_ -]+$/.test(value);
        }, "Alpha Characters Only.");

        jQuery.validator.addMethod('phoneUS', function (phone_number, element) {
            phone_number = phone_number.replace(/\s+/g, '');
            return this.optional(element) || phone_number.length > 9 &&
                    phone_number.match(/^(1-?)?(\([2-9]\d{2}\)|[2-9]\d{2})-?[2-9]\d{2}-?\d{4}$/);
        }, 'Enter a valid phone.');

        jQuery.validator.addMethod("valueNotEquals", function (value, element, arg) {
            return arg != value;
        }, "Value must not equal arg.");




    });

</script>
