<section class="dr-banner" style="min-height:600px;">
	<div class="container">
		<div class="right-banner" style="width: 654px !important; margin-right:330px;">
			<h4>Login</h4>
			<?php if($this->session->userdata('PAGE_ERROR_MESSAGE')) { ?>
				<div class="ajax_report alert alert-danger" role="alert" style="margin-left:0px;margin-bottom:10px;width:500px;"><span class="close-message"></span><div class="ajax_message"><?=$this->session->userdata('PAGE_ERROR_MESSAGE')?></div></div>
				<?php $this->session->unset_userdata('PAGE_ERROR_MESSAGE');?>
			<?php } ?>	
			<?php echo form_open('login',array('class'=>'ajaxForm','id'=>'login-page'))?>
			<div class="ajax_report alert display-hide" role="alert" style="margin-left:0px;margin-bottom:10px;width:500px;"><span class="close-message"></span><div class="ajax_message">Hello Message</div></div>
			<div class="form-group1">
				<div class="my-div"><input type="email" placeholder="Email" class="form-control my-txt-fleid " name="email" style="width:500px;"></div>
			</div>
			<div class="form-group1">
				<div class="my-div"><input type="password" placeholder="Password" class="form-control my-txt-fleid " name="password"  style="width:500px;"></div>
			</div>
			
			<input type="submit" class="formsign-bttn" value="LOGIN" style="width:50%"/> <a href="<?=site_url('forget_password')?>"><span class="changecolor" style="font-size:20px;margin-left:25px;">Forgot password?</span></a>
			<?=form_close();?>
		</div>
	</div>
</section>
<section class="dr-content">
	<section class="howit-works">
		<div class="container">
			<div class="drwork-text">
				<h5>How it works</h5>
				<p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio.</p>
				<a href="#">Learn More</a>
			</div>
			<div class="dr-rightvideo">
				<a href="#"><img src="<?=$this->config->item('templateassets')?>images/video-img.png" alt=""/></a>
			</div>
		</div>
	</section>
	<section class="dr-testimonial">
		<div class="container test-part">
			<h2>Why Use TenantTag?</h2>
			<div class="testimonial">
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit<br>
in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
				<span class="client-name">- Jeffrey Zeldman</span>
			</div>
			<div class="testimonial1">
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim<br> veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit <br>esse cillum dolore eu, fugiat nulla pariatur.</p>
				<span class="client-name">- Paul Boag</span>
			</div>
		</div>
	</section>
</section>

