<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>TenantTag</title>
        <link rel="shortcut icon" href="<?php echo base_url() ?>/favicon.ico" type="image/x-icon">
        <link rel="icon" href="<?php echo base_url() ?>/favicon.ico" type="image/x-icon">
        <link href="<?= $this->config->item('templateassets') ?>css/font-awesome.css" rel="stylesheet">
        <link href="<?= $this->config->item('templateassets') ?>css/style.css" rel="stylesheet">
        <link href="<?= $this->config->item('templateassets') ?>css/responsive.css" rel="stylesheet">
		 <link href="<?= $this->config->item('templateassets') ?>css/slick.css" rel="stylesheet">

        <script type="text/javascript" src="<?= $this->config->item('templateassets') ?>js/jquery-1.11.0.js"></script>
        <script type="text/javascript" src="<?= $this->config->item('templateassets') ?>js/jquery.form.js"></script>
        <script type="text/javascript" src="<?= $this->config->item('templateassets') ?>js/formClass.js"></script>
        <script type="text/javascript" src="<?= $this->config->item('templateassets') ?>js/jquery.validate.min.js"></script>
        <script type="text/javascript" src="<?= $this->config->item('templateassets') ?>js/jquery.mask.js"></script>    
<script type="text/javascript" src="<?= $this->config->item('templateassets') ?>js/slick.js"></script> 		
    </head>

    <body>
        <header>
            <div class="container">
                <div class="dr-logo"><a href="<?= site_url() ?>"><img src="<?= $this->config->item('uploads') ?>site_logo/<?= $this->config->item('site_logo') ?>" alt="logo"/></a></div>	
            </div>
        </header>
        <div id="wait-div"></div>
        <script>
            var siteUrl = '<?= site_url() ?>';
        </script>	
