<link href="<?= $this->config->item('templateassets') ?>css/bootstrap.min.css" rel="stylesheet">
<script type="text/javascript" src="<?= $this->config->item('templateassets') ?>js/bootstrap.min.js"></script>
<link href="<?= $this->config->item('templateassets') ?>css/bootstrap-dialog.min.css" rel="stylesheet">
<script type="text/javascript" src="<?= $this->config->item('templateassets') ?>js/jquery.validate.min.js"></script>  
<script>
    $(document).ready(function () {
        $('.lease-file').change(function (e) {
            var file_data = $(this).prop('files')[0];
            var prop_id = $(this).attr('id');
            var form_data = new FormData();
            form_data.append('file', file_data);
            form_data.append('prop_id', prop_id);
            $.ajax({
                url: '<?php echo base_url() ?>leases/upload_lease_doc', // Url to which the request is send
                dataType: 'text', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                type: 'post',
                beforeSend: function () {
                    $('#wait-div').show();
                },
                success: function (response) {
                    $('#wait-div').hide();
                    var response = $.parseJSON(response);
                    if(response.success){
                        //if (response != 'error') {
                        //console.log($("#14_51").parent().html());
                        var html = '<li><a target="_blank" href="' + siteUrl + '/assets/uploads/lease_documents/' + response.message + '" class="docname">' +
                                '<img src="' + siteUrl + '/assets/default/images/file-upload.png" alt=""><span>' + response.message + '</span></a></li>';
                        $("#" + prop_id).parent().parent().prepend(html);
                    }else{
                        var message_r = response.message;
                        message_r = message_r.replace(/<p[^>]*>/g, "");
                        swal("Error!", message_r, "error");
                    }
                },
                error: function () {
                    $('#wait-div').hide();
                    alert('server error');
                }
            });


        });

        $('.lease-file-list').change(function (e) {
            var file_data = $(this).prop('files')[0];
            var prop_id = $(this).attr('name');
            var form_data = new FormData();
            form_data.append('file', file_data);
            form_data.append('prop_id', prop_id);
            $.ajax({
                url: '<?php echo base_url() ?>leases/upload_lease', // Url to which the request is send
                dataType: 'text', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                type: 'post',
                beforeSend: function () {
                    $('#wait-div').show();
                },
                success: function (response) {
                    window.location = window.location;
                    
                },
                error: function () {
                    $('#wait-div').hide();
                    alert('server error');
                }
            });


        });

        $(document).on("click", ".all-tabs-panel-li", function () {

            $('.all-tabs-panel-li').removeClass('active');
            $(this).addClass('active');
        });

        $(document).on('click', '.crossicon', del_document);
        function del_document(e)
        {
            var id = $(this).attr('id');
            swal({
                title: "Alert?",
                text: "Are you sure to delete this record?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            },
            function () {
                $.ajax({
                    url: '<?php echo base_url('leases/delete_document'); ?>',
                    type: 'POST',
                    data: {id: id},
                    beforeSend: function () {
                        $('#wait-div').show();
                    },
                    success: function (data) {
                        $("#wait-div").hide();
                        swal({
                            title: "Deleted",
                            text: "Deleted Successfully",
                            type: "success"
                        },
                        function () {
                            $(e.target).closest('li').remove();
                        });
                    },
                    error: function () {
                        $("#wait-div").hide();
                        swal("Error!", "Server not responding.", "error");
                    }
                });
            });
        }

        $(document).on('click', '.to_date', edit_lease_to);
        function edit_lease_to(e)
        {
            var id = $(this).attr('id');
            $(this).removeClass('fa-pencil');
            $(this).removeClass('frm_date');
            $(this).addClass('fa-save');
            $(this).addClass('update_to');
            $(this).prev().find('input').removeAttr('disabled');
            $(this).parent().prev().find('input').removeAttr('disabled');
        }

        $(document).on('click', '.update_to', update_lease);
        function update_lease(e)
        {
            var id = $(this).attr('id');
            var selct = $(this);
            var dateto = $(this).parent().prev().find('input').val();
            var datefrom = $(this).parent().prev().prev().find('input').val();
            var rent_amount = $(this).prev().find('input').val();
            $.ajax({
                url: '<?php echo base_url('leases/update_lease'); ?>',
                type: 'POST',
                data: {'id': id, 'datefrom': datefrom, 'dateto': dateto, 'rent_amount': rent_amount},
                beforeSend: function () {
                    $('#wait-div').show();
                },
                success: function (data) {
                    $("#wait-div").hide();
                    var myArray = JSON.parse(data);
                    if (myArray.success) {
                        swal({
                            title: "Success",
                            text: myArray.success_message,
                            type: "success"
                        },
                        function () {
                            $(selct).removeClass('fa-save');
                            $(selct).removeClass('update_to');
                            $(selct).addClass('fa-pencil');
                            $(selct).addClass('frm_date');
                            $(selct).prev().find('input').attr('disabled', 'disabled');
                            $(selct).parent().prev().find('input').attr('disabled', 'disabled');
                        });
                    } else {
                        swal("Error!", myArray.success_message, "error");
                    }

                },
                error: function () {
                    $("#wait-div").hide();
                    swal("Error!", "Server not responding.", "error");
                }
            });

        }

        $("body").delegate(".leasefrom", "focusin", function () {
            $(this).datepicker({
                defaultDate: "+1w",
                changeMonth: true,
                changeYear: true,
               //   minDate: 0,
                numberOfMonths: 1,
                onSelect: function (selectedDate) {
                    $(this).parent().parent().next().find('input').removeAttr('disabled');
                    $(this).parent().parent().next().find('input').focus();
                    $(this).parent().parent().next().find('input').blur();
                },
                onClose: function (selectedDate) {
                    $(this).parent().parent().next().find('input').datepicker("option", "minDate", selectedDate);
                }
            });
        });

        $("body").delegate(".leaseto", "focusin", function () {
            $(this).datepicker({
                defaultDate: "+1w",
                changeMonth: true,
                changeYear: true,
                numberOfMonths: 1,
                  minDate: 0,
                onSelect: function (selectedDate) {
                    //   $(this).parent().prev().find('input').focus(); 
                    //   $(this).parent().prev().find('input').blur(); 
                },
                onClose: function (selectedDate) {
                    console.log($(this).parent().parent().prev().html());
                    $(this).parent().parent().prev().find('input').datepicker("option", "maxDate", selectedDate);
                }
            });
        });

        /*  $('.leasefrom').datetimepicker({
         viewMode: 'years',
         format: 'DD-MM-YYYY'
         });
         $('.leaseto').datetimepicker({
         viewMode: 'years',
         format: 'DD-MM-YYYY'
         });*/

    });
</script>
