<footer>
    <?php
    function addhttp($url) {
        if (!preg_match("~^(?:f|ht)tps?://~i", $url)) {
            $url = "http://" . $url;
        }
        return $url;
    }
    ?>
    <div class="container">
    <?php $links = getFooterLinks(); //echo '<pre>';print_r($links);die;  ?>
        <div class="fttr-block footer-link-block">
            <ul>
                <?php if (!empty($links)) { ?>
                    <?php foreach ($links as $link): ?>
                        <li class="footer-link-li">
                            <a href="<?= site_url('page') ?>/<?= $link->slug ?>"><?php echo $link->title; ?></a></li>
                    <?php endforeach; ?>
                <?php } ?>
            </ul>
        </div>
        <div class="fttr-block subscribe_ten">
            <a target="_blank" href="<?php echo "http://www.tenanttagblog.com/" ?>" class="drblog">Blog</a>
            <?php echo form_open('welcome/subscribe', array('class' => 'ajaxForm', 'id' => 'subscribe')) ?>

                <input type="email" name="email" class="dremail">
                <button class="subscribe">Subscribe</button>
                <div class="ajax_report alert display-hide" role="alert" style="float: left; width: 85%; padding: 5px; margin-top: 4px;">
                    <span class="close close_custom_popup"></span>
                    <span class="ajax_message" style="line-height:normal;margin: 0;padding: 3px;"></span>
                </div>
            <?php form_close(); ?>
        </div>	
        <div class="fttr-block right-pull">
            <h4>Follow Us</h4>
            <ul class="social-link">
                <?php $urls = getSocialUrls(); ?>
                <li><a id="drfb" target="_blank" href="<?php echo addhttp($urls->facebook_url); ?>"><i class="fa fa-facebook"></i></a></li>
                <li><a id="drtwt" target="_blank" href="<?php echo addhttp($urls->twitter_url); ?>"><i class="fa fa-twitter"></i></a></li>
                <li><a  target="_blank" href="<?php echo addhttp($urls->pinterest_url); ?>"><i class="fa fa-pinterest-p"></i></a></li>
            </ul>
            <img src="<?= $this->config->item('templateassets') ?>images/fttr-logo.png" alt=""/>
            <p><?php echo $urls->copyright_text; ?></p>
        </div>
    </div>
</footer>


<div class="popup-block" >
    <div class="my-popup save-later-account-setup" style="display: none">
        <div class="popup-cont">
            <div class="popup-head">
                <span class="close close_custom_popup"></span>
            </div>
            <div class="popup-body">
                <div class="center"><span class="t-icon"></span></div>

                <ul class="listing list-pop">
                    <li class="my-cross">Account Setup Incomplete</li>
                </ul>
            </div>
            <div class="popup-footer">
                <div class="center">
                    <a href="javascript:;" class="back back-popup close_custom_popup">Back</a>
                    <a href="javascript:;" class="save save-later-account-setup-process" style="width:150px;"> SAVE FOR LATER</a>
                </div>
            </div>
        </div>
    </div>
    <div class="my-popup save-later" style="display: none">
        <div class="popup-cont">
            <div class="popup-head">
                <span class="close close_custom_popup"></span>
            </div>
            <div class="popup-body">
                <div class="center"><span class="t-icon"></span></div>

                <ul class="listing list-pop">
                    <li class="my-cross">Property Information Incomplete</li>
                </ul>
            </div>
            <div class="popup-footer">
                <div class="center">
                    <a href="javascript:;" class="back back-popup close_custom_popup">Back</a>
                    <a href="javascript:;" class="save save-later-process" style="width:150px;"> SAVE FOR LATER</a>
                </div>
            </div>
        </div>
    </div>
    <div class="my-popup next-step checkout-confirmed" style="display: none">
        <div class="popup-cont">
            <div class="popup-head">
                <span class="close  close_custom_popup close-popup"></span>
            </div>
            <div class="popup-body">
                <h2 class="mr-top20">Confirm tServices</h2>
                <div class="center"><span class="t-icon"></span></div>

                <ul class="listing">
                    <li class="is_tMessaging">tText Confirmed</li>  
                    <li class="is_tMaintence">tMaintence Confirmed</li>
                    <li class="is_tPay">tPay Confirmed </li> 	
                </ul>
            </div>
            <div class="popup-footer">
                <div class="center">
                    <a href="javascript:;" class="back-popup my-link-third close_custom_popup">Back</a>
                    <a href="javascript:;" class="checkout save_property_checkout"> Checkout</a>
                    <!-- <a href="<?= site_url('add-property') ?>" class="add_property my-link-third"> Add Property</a> -->
                </div>
            </div>
        </div>
    </div>
    <div class="my-popup next-step-success-message" style="display: none">
        <div class="popup-cont">
            <div class="popup-head">
                <span class="close close_custom_popup close-popup"></span>
            </div>
            <div class="popup-body">
                <h2 class="mr-top20">THANK YOU FOR YOUR PAYMENT.</h2>
                <div class="center"><span class="t-icon"></span></div>

                <ul class="listing-center">
                    <!-- <li class="is_tMessaging">tText Active</li>  
                    <li class="is_tPay">tPay Active</li>    
                    <li class="is_tMaintence">tMaintence Active</li> -->
                    <li> Congratulations!</li>
                    <li> Your tServices are activated</li>
                    <li class="payment_received"> Payment Received: $7.95</li>
                    <li> Receipts sent to email.</li>
                </ul>
            </div>
            <div class="popup-footer">
                <div class="center">
                    <a href="javascript:;" id="<?php echo $this->session->userdata('stripe_payment_prop_id'); ?>" class="close-popup back tenant_welcome_text">OK</a>
                    <?php $this->session->unset_userdata('stripe_payment_prop_id'); ?>
                </div>
            </div>
        </div>
    </div>
     <div class="payment_error_popup" style="display: none">
        <div class="popup-cont">
            <div class="popup-head">
                <span class="close close_custom_popup close-popup"></span>
            </div>
            <div class="popup-body">
                <h2 class="mr-top20">YOUR PAYMENT WAS UNSUCCESSFUL</h2>
                <div class="center"><span class="t-icon"></span></div>

                <ul class="listing-center">
                    <li>  Please Review Your Payment Information!</li>
                    <li id="payment-error"></li>
                </ul>
            </div>
            <div class="popup-footer">
                <div class="center">
                    <a href="javascript:;" class="close-popup back close_custom_popup">BACK</a>
                </div>
            </div>
        </div>
    </div>
    <div class="checkout_confirm_box_pop" style="display: none">
        <div class="popup-cont">
            <div class="popup-head">
                <span class="close  close_custom_popup close-popup"></span>
            </div>
            <div class="popup-body">
                <h2 class="mr-top20">Confirm Order</h2>
                <div class="center"><span class="t-icon"></span></div>

                <ul class="listing-center">
                </ul>
                    <div class="order_info_pop"></div>
            </div>
            <div class="popup-footer">
                <div class="center">
                    <a href="javascript:;" class="close-popup back1 close_custom_popup">Back</a>
                    <a href="javascript:;" class="pay_button btn">Pay</a>
                </div>
            </div>
        </div>
    </div>
    <div class="delete_saved_card_info" style="display: none">
        <div class="popup-cont">           
                
                <div class="popup-head">
                    <span class="close close_custom_popup close-popup"></span>
                </div>
                <div class="popup-body">
                    <h2 class="mr-top20">Do you really want to disable this property?</h2>
                    <div class="center"><span class="t-icon"></span></div>

                    <ul class="listing-center">
                    </ul>
                        <div class="body_info_pop"></div>
                </div>
                <div class="popup-footer">
                    <div class="center">
                        <a href="javascript:;" class="btn-tenant btn delete_card_permanently">Disable</a>
                        <a href="javascript:;" class="close-popup btn-tenant back1 close_custom_popup">Cancel</a>
                    </div>
                </div>
        </div>
    </div>
    <div class="act_deact_saved_card_info" style="display: none">
        <div class="popup-cont">           
                
                <div class="popup-head">
                    <span class="close close_custom_popup close-popup"></span>
                </div>
                <div class="popup-body">
                    <h2 class="mr-top20 act_deact_text">Do you really want to deactivate this property?</h2>
                    <div class="center"><span class="t-icon"></span></div>

                    <ul class="listing-center">
                    </ul>
                        <div class="body_info_pop"></div>
                </div>
                <div class="popup-footer">
                    <div class="center">
                        <a href="javascript:;" class="btn-tenant btn act_deact_card_permanently">Deactivate</a>
                        <a href="javascript:;" class="close-popup btn-tenant back1 close_custom_popup">Cancel</a>
                    </div>
                </div>
        </div>
    </div>
    <div class="confirm_delete_acc" style="display: none">
        <div class="popup-cont">
            <div class="popup-head">
                <span class="close close_custom_popup close-popup"></span>
            </div>
            <div class="popup-body">
                <h2 class="mr-top20">Delete Account.</h2>                
                <div class="order_info_pop"></div>
                <ul class="delete_acc_con">
                    <li> Deleting this would no longer allow your tenants to pay you money online. Would you still like to proceed?</li>
                </ul>                   
            </div>
            <div class="popup-footer">
                <div class="center">
                    <a href="javascript:;" class="back back-popup close_custom_popup">Cancel</a>
                    <a href="javascript:;" class="save proceed_delete_account" style="width:150px;"> Delete</a>
                </div>
            </div>
        </div>
    </div>
    
</div>


<!--
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
-->
<script src="<?= $this->config->item('templateassets') ?>js/jquery-effects.js"></script>
<script>
    $('nav').menu({start: 979});
</script>
<script type="text/javascript" src="<?= $this->config->item('templateassets') ?>js/lightbox.min.js"></script>
<link href="<?= $this->config->item('templateassets') ?>css/lightbox.css" rel="stylesheet">
<script src="<?= $this->config->item('templateassets') ?>js/jquery-ui.js" type="text/javascript"></script>
<script>
    function check_flash_message(){
        if($('iframe').contents().find(".flash-block").html() != '' && $('iframe').contents().find(".flash-block").html() != undefined){
            $('iframe').css("display",'none');
            $(".mc-modal").find('.close_form').css("display",'none');
            $(".mc-modal").find('.mc-closeModal').css("display",'block');
            $(".mc-modal").find('.mc-closeModal').trigger('click');
        }
    }
     function closecustomFunc(){
       // alert('click');
        $('.mc-modal').parent().closest('div').hide(); 
    };

    $(document).ready(function () {
        $('.close_custom_popup').click(function(){
            $('body').removeClass('custom_modal_open');            
        });
        $('.open_cus_model').click(function(){
            $('body').addClass('custom_modal_open');            
        });
        setTimeout(function(){
        //$(".mc-modal").prepend('<div id="close_form" onclick="return closecustomFunc()" class="close_form">Close</div>');  
            //$('iframe').contents().find('form').attr("onsubmit","return check_flash_message()");
        },3000);  
         
       // setInterval(check_flash_message,200);
        $('.phone_us').mask('(000) 000-0000');
     //   $('.filtersize').mask('0ZZZZZZX0ZZZZZZX0ZZZZZZ', {translation:  {'Z': {optional: true}}});//.mask('00X00X00');
        $(document).on("click", function(event){
            var $trigger = $(".my-drop");
            if($trigger !== event.target && !$trigger.has(event.target).length){
                $(".my-dropdown").hide();
            }            
        });
        $(document).ready(function () {
            $(".minus").click(function () {
                $(".drproperty").slideToggle();
                $(this).toggleClass("drcollapse");
            });
            $(".secondminus").click(function () {
                $(".secondm").slideToggle();
                $(this).toggleClass("drcollapse");
            });


            $('.shwtmsg').click(function () {
                $('body').addClass('overlay');
                $('.tmntnce').hide();
                $('.tmsgs').toggle();

            });
            $(".shwtmntnce").click(function () {
                $('body').addClass('overlay');
                $('.tmsgs').hide();
                $('.tmntnce').toggle();
            });
            $(".shwtpay").click(function () {
                $('body').addClass('overlay');
                $('.tmsgs').hide();
                $('.tpay').toggle();
            });
            $('.my-drop').click(function () {
                $('.my-dropdown').toggle();
            })


            /*$('.save-property').click(function () {
                $('.popup-block').hide();
                $('.vendor-detail').submit();
            });*/
            
            $(".vendor-detail").validate({
                submitHandler: function (form) {
                    var posturl = siteUrl + 'add-property-step3';
                    var data = $('.vendor-detail').serialize();
                    var formClass = '.vendor-detail';
                    $.ajax({
                        url: posturl,
                        data: data,
                        type: "POST",
                        dataType: 'json',
                        beforeSend: function () {
                            $('#wait-div').show();
                        },
                        success: function (response) {
                            console.log(response);
                            if(typeof response.checkout !== 'undefined' && response.checkout == 'yes'){
                                window.location.href = response.url
                            }else if(typeof response.validation_failure !== 'undefined'){
                                $('#wait-div').hide();
                                $(formClass).find('.ajax_report').addClass('alert-danger').css("display",'block').children('.ajax_message').html(response.validation_error_message).css("display","block");
                                scrollToElement(formClass, 1000);
                            }else{
                                $('#wait-div').hide();
                                $(formClass).find('.ajax_report').removeClass('alert-success').removeClass('alert-danger').fadeIn(200);
                                $('#wait_div').hide();
                                if (response.success)
                                    $(formClass).find('.ajax_report').addClass('alert-success').children('.ajax_message').html(response.success_message);
                                else
                                    $(formClass).find('.ajax_report').addClass('alert-danger').children('.ajax_message').html(response.error_message);
                                if (response.url)
                                {
                                    setTimeout(function () {
                                        window.location.href = response.url;
                                    }, 700);
                                }
                                if (response.resetForm)
                                    $(formClass).resetForm();
                                if (response.selfReload)
                                {
                                    //location.reload();
                                    setTimeout(function () {
                                        location.reload();
                                    }, 700);
                                }
                                if (response.scrollToElement)
                                    scrollToElement(formClass, 1000);
                                if (response.ajaxCallBackFunction)
                                    ajaxCallBackFunction(response);
                                setTimeout(function () {
                                    $(formClass).find('.ajax_report').fadeOut(500);
                                }, 10000);
                            }
                        },
                        error: function (data) {
                            alert("Server Error.");
                            return false;
                        }
                    });

                    return false;
                }
            });


            $(".payment_form").validate({
                submitHandler: function (form) {
                    var posturl = siteUrl + 'account_setup';
                    var data = $('.payment_form').serialize();
                    var formData = new FormData($('.payment_form')[0]);
                    console.log(formData);
                    console.log(data);
                    //return false;
                    var formClass = '.payment_form';
                    $.ajax({
                        url: posturl,
                        data: formData,
                        type: "POST",
                        dataType: 'json',
                        cache: false,
                        contentType: false,
                        processData: false,
                        async: false,
                        beforeSend: function () {
                            $('#wait-div').show();
                        },
                        success: function (response) {
                            $('#wait-div').hide();
                            $(formClass).find('.ajax_report').removeClass('alert-success').removeClass('alert-danger').fadeIn(200);
                            $('wait_div').hide();
                            if (response.success)
                                $(formClass).find('.ajax_report').addClass('alert-success').children('.ajax_message').html(response.success_message);
                            else
                                $(formClass).find('.ajax_report').addClass('alert-danger').children('.ajax_message').html(response.error_message);
                            if (response.url)
                            {
                                setTimeout(function () {
                                    window.location.href = response.url;
                                }, 700);
                            }
                            if (response.resetForm)
                                $(formClass).resetForm();
                            if (response.selfReload)
                            {
                                //location.reload();
                                setTimeout(function () {
                                    location.reload();
                                }, 700);
                            }
                            if (response.scrollToElement)
                                scrollToElement(formClass, 1000);
                            if (response.ajaxCallBackFunction)
                                ajaxCallBackFunction(response);
                            setTimeout(function () {
                                $(formClass).find('.ajax_report').fadeOut(500);
                            }, 10000);
                        },
                        error: function (data) {
                            alert("Server Error.");
                            return false;
                        }
                    });

                    return false;
                }
            });

            $('.save-later-process').click(function () {
                $('.save-later').hide();
                $('.next-step').hide();
                $('.popup-block').hide();
                $('.action-type').val('save_later');
                $('.vendor-detail').submit();
            });
            $('.save-later-account-setup-process').click(function () {
                $('.save-later').hide();
                $('.next-step').hide();
                $('.popup-block').hide();
                $('.action-type').val('save-later');
                $('.payment_form').submit();
            });

            $('.close-popup').click(function (event) {
                $('.save-later').hide();
                $('.next-step').hide();
                $('.popup-block').hide();
            });

            $('.back-popup,.close').click(function (event) {
                $('.save-later').hide();
                $('.next-step').hide();
                $('.popup-block').hide();
            });
        });


        $(function () {
            $("#tabs").tabs();
        });
        //~ $(".togle").click(function(){
        //~ $(this).toggleClass("stop");
        //~ }); 
        $(".btn-part a").click(function () {

            //   $(this).toggleClass("active");
            //     $(this).siblings('a').toggleClass("unactive");
        });
        $('.close-message').click(function (event) {
            $('div .ajax_report').hide('slow');
            //$('div .ajax_report').removeClass('display-hide');
        });

        var type = window.location.hash.substr(1);
        if (type != '') {
            $(".all-tabs-panel-li").removeClass('active');
        }
        $(".late_fee_type_cls").change(function () {
            if ($(this).val() != '') {
                $(this).parent().next().show();
            } else {
                $(this).parent().next().hide();
            }
        });

    });



</script>
<script>
    function getStateByCountryId(con_id)
    {
        if (con_id)
        {
            var postUrl = siteUrl + 'getStateByCountryId/' + con_id;
            $.ajax({
                url: postUrl,
                dataType: 'json',
                success: function (response) {
                    $('.country_code').val(response.country_code);
                    $('.country_code').blur();
                    if (response.success)
                    {
                        $('#state').html('');
                        $('#regions').show('slow');
                        $('#state').append('<option value="">Select State</option>')
                        $.each(response.regions, function (key, value) {
                            $('#state').append('<option value="' + value.region_id + '">' + value.region_name + ' </option>');
                        });
                    }
                    else
                    {
                        $('#state').html('');
                        $('#state').append('<option value="">No state found under selected Country</option>');
                    }
                },
                error: function () {
                    alert('server error');
                },
            });
        }
    }

    function getCityByStateId(state_id)
    {
        if (state_id)
        {
            var postUrl = siteUrl + 'getCityByStateId/' + state_id;
            $.ajax({
                url: postUrl,
                dataType: 'json',
                success: function (response) {
                    if (response.success)
                    {
                        $('#city').html('');
                        $('#cities').show('slow');
                        $('#city').append('<option value="">Select City</option>')
                        $.each(response.cities, function (key, value) {
                            $('#city').append('<option value="' + value.city_id + '">' + value.name + ' </option>');
                        });
                    }
                    else
                    {
                        $('#city').html('');
                        $('#city').append('<option value="">No city found under selected State</option>');
                    }
                },
                error: function () {
                    alert('server error');
                },
            });
        }
    }

    /*$(document).ready(function () {
        $('.com_survey_question_link').click(function () {
            var id = $(this).attr('data-id');
            var postUrl = siteUrl + 'welcome/get_survey_question';
            $.ajax({
                url: postUrl,
                type: 'POST',
                data: {id: id},
                dataType: 'json',
                beforeSend: function () {
                    $('#wait-div').show();
                },
                success: function (response) {                        
                    if (response.success)
                    {
                        $("#survey_question").find('.survey_question_body').html(response.html);
                    }
                     $('#wait-div').hide();
                },
                error: function () {
                     $('#wait-div').hide();
                    alert('server error');
                },
            });
        });
    });*/

        $(document).on("click","#survey_submit",function(){
                var posturl = siteUrl + 'update_survey_question';
                var data = $('.survey_question_form').serialize();
                var formClass = '.survey_question_form';
                $.ajax({
                    url: posturl,
                    data: data,
                    type: "POST",
                    dataType: 'json',
                    beforeSend: function () {
                        $('#wait-div').show();
                        $('#wait-div').css("z-index",9999);
                    },
                    success: function (response) {
                        console.log(response);
                            $('#wait-div').hide();
                            $('#wait-div').css("z-index",'');
                           // alert(window.location)
                        if(typeof response.validation_failure !== 'undefined'){
                          //  alert(response.success_message)
                            $('#wait-div').hide();
                            $(formClass).find('.ajax_report').addClass('alert-danger').css("display",'block').children('.ajax_message').html(response.success_message);
                            scrollToElement(formClass, 1000);
                        }else{
                            $('#wait-div').hide();
                            $(formClass).find('.ajax_report').removeClass('alert-success').removeClass('alert-danger').fadeIn(200);
                            $('#wait_div').hide();
                            if (response.success)
                                $(formClass).find('.ajax_report').addClass('alert-success').children('.ajax_message').html(response.success_message);
                            else
                                $(formClass).find('.ajax_report').addClass('alert-danger').children('.ajax_message').html(response.success_message);
                            if (response.url)
                            {
                                setTimeout(function () {
                                    //window.location.href = response.url;
                                    window.location = window.location;
                                }, 700);
                            }
                            if (response.resetForm)
                                $(formClass).resetForm();
                            if (response.selfReload)
                            {
                                //location.reload();
                                setTimeout(function () {
                                    window.location = window.location;
                                    //location.reload();
                                }, 700);
                            }
                            if (response.scrollToElement)
                                scrollToElement(formClass, 1000);
                            if (response.ajaxCallBackFunction)
                                ajaxCallBackFunction(response);
                            setTimeout(function () {
                                $(formClass).find('.ajax_report').fadeOut(500);
                            }, 10000);
                        }
                    },
                    error: function (data) {
                        alert("Server Error.");
                          $('#wait-div').hide();
                            $('#wait-div').css("z-index",'');
                        return false;
                    }
                });
               
        });


</script>
<style>
    .my-tab.ui-tabs-nav .ui-state-active a {

        background-color: #ffffff !important;
        border-bottom-left-radius: 5px;
        border-color: #e2e2e4  #e2e2e4 #e2e2e4;
        border-image: none;
        border-style: solid none solid solid;
        border-top-left-radius: 5px;
        border-width: 1px 0 1px 1px;
        margin: 0 -15px -2px;
        padding: 22px 15px;
        position: relative;
        z-index: 9;
        background-color: #ffffff;
    }
</style>
<script type="text/javascript" src="//s3.amazonaws.com/downloads.mailchimp.com/js/signup-forms/popup/embed.js" data-dojo-config="usePlainJson: true, isDebug: false"></script>



<!-- Global Site Tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-78376704-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-78376704-1');
</script>


</body>
</html>
