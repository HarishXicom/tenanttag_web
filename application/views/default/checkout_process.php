<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js">
</script>

<script>
    $(document).ready(function() {
        $("#link-btn").click(function() {
            $("#linked-acc").slideDown("slow");
        });
    });
</script>
<style>
    [tooltip]:hover:before {
        background: #00659f;
        color: #fff;
        font-size: 14px;
        margin-left: 32px;
        margin-top: -5px;
        opacity: 1;
        z-index: 9999;
    }
    [tooltip]:before {
        border: 1px solid #00659f;
        box-shadow: 1px 1px 4px #777777;
        color: #333;
        content: attr(tooltip);
        opacity: 0;
        padding: 10px;
        position: absolute;
        transition: all 0.15s ease 0s;
        max-width: 640px;
    }
    [tooltip]:not([tooltip-persistent]):before {
        pointer-events: none;
    }
</style>
<div class="level">
    <div class="container">
        <div class="level-indicator">
            <img src="<?= $this->config->item('templateassets') ?>images/full-level.png" alt="" />
            <ul>
                <li>Property Info</li>
                <li>Tenants</li>
                <li class="blue-col1">tServices</li>
            </ul>
        </div>
    </div>
</div>

<section class="signup-section2">
    <div class="container">
        <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                var  checkTrialTimePeriod = '<?php echo checkTrialTimePeriod($this->session->userdata("MEM_ID")); ?>';
                $('.checkout_confirm_box').click(function() {
                    var PER_PROPERTY_AMOUNT = '<?php echo PER_PROPERTY_AMOUNT; ?>';
                    var checkboxes = document.getElementsByName('property[]');
                    var property_address = document.getElementsByName('property_address[]');
                    var checkboxesChecked = [];
                    var property_table = '';
                    var property_table = '';
                    for (var i = 0; i < checkboxes.length; i++) {
                        if (checkboxes[i].checked) {
                            checkboxesChecked.push(checkboxes[i].value);
                            property_table += '<tr>';
                            property_table += ' <td>' + property_address[i].value + '</td>';
                            property_table += ' <td>$<?php echo PER_PROPERTY_AMOUNT; ?>/month/unit</td>';
                            property_table += ' <td>$<?php echo PER_PROPERTY_AMOUNT; ?></td>';
                            property_table += '</tr>';

                        }
                    }
                    var total_amount_is = PER_PROPERTY_AMOUNT * checkboxesChecked.length
                    property_table += ' <tr>';
                    property_table += ' <td>Total</td>';
                    /*property_table += ' <td></td>';*/
                    /*property_table += '  <td></td>';*/
                    property_table += '  <td></td>';
                    property_table += '  <td>$' + total_amount_is.toFixed(2) + '</td>';
                    property_table += ' </tr>';
                    if(checkTrialTimePeriod != false){
                        property_table += ' <tr>';
                        property_table += ' <td>Trial Account</td>';
                        /*property_table += ' <td></td>';*/
                        /*property_table += '  <td></td>';*/
                        property_table += '  <td></td>';
                        property_table += '  <td>$0.00 (Trial)</td>';
                        property_table += ' </tr>'; 
                    }
                    //alert(checkboxesChecked.length)

                    var card_id = $('input[name=card_id]:checked', '#payment-form').val();
                    //alert(card_id);
                    $(".payment_error_popup").hide();
                    $(".popup-block").show();
                    $('body').addClass('custom_modal_open'); // add class in body
                    $(".checkout_confirm_box_pop").show();
                    if (card_id != 0) {
                        var card_num_text = $("#account_text_number_" + card_id).html();
                        var data_is = '<table border="1" cellpadding="0" cellspacing="0">';
                        data_is += '<thead>';
                        data_is += ' <tr>';
                        data_is += '<th>Property</th>';
                        /*  data_is += '<th>Services</th>';
                                                data_is += ' <th>Units </th>';*/
                        data_is += ' <th>Price</th>';
                        data_is += '<th>Total</th>';
                        data_is += '  </tr>';
                        data_is += ' </thead>';
                        data_is += '  <tbody>';
                        data_is += property_table;
                        data_is += '</tbody>';
                        data_is += ' </table>';

                        data_is += '<h4 style="color:#00659F;  font-family:robotobold;  font-size:16px;  text-align:left;padding-left: 28px;"> Checkout with saved card -  ' + card_num_text + '</h4>';
                        if (checkboxesChecked.length > 0) {
                            $(".pay_button").show();
                            $(".checkout_confirm_box_pop").find(".order_info_pop").html(data_is);
                        } else {
                            $(".pay_button").hide();
                            var cart_empty = '<div class="cart_empty_popup_message">';
                            cart_empty += 'There are no property selected. Please add or select a property.';
                            cart_empty += '</div>';
                            $(".checkout_confirm_box_pop").find(".order_info_pop").html(cart_empty);
                        }
                    } else {
                        var data_is = '<table border="1" cellpadding="0" cellspacing="0">';
                        data_is += '<thead>';
                        data_is += ' <tr>';
                        data_is += '<th>Property</th>';
                        /*data_is += '<th>Services</th>';
                                                data_is += ' <th>Units </th>';*/
                        data_is += ' <th>Price</th>';
                        data_is += '<th>Total</th>';
                        data_is += '  </tr>';
                        data_is += ' </thead>';
                        data_is += '  <tbody>';
                        data_is += property_table;
                        data_is += '</tbody>';
                        data_is += ' </table>';

                        data_is += '<h4 style="color:#00659F;  font-family:robotobold;  font-size:16px;  text-align:left;padding-left: 28px;"> CC Info</h4>';
                        data_is += '<div class="cc_info_area">';
                        data_is += '<div class="rowdata"><label> CC: </label> <span>' + $("#cc").val() + '</span></div>';
                        data_is += '<div class="rowdata"><label> Exp. Date: </label> <span>' + $("#month_year").val() + '</span></div>';
                        data_is += '<div class="rowdata"><label> Security Code: </label> <span>' + $("#cvc").val() + '</span></div>';
                        data_is += '<div class="rowdata"><label> Address: </label> <span>' + $("#address_line1").val() + '</span></div>';
                        data_is += '<div class="rowdata"><label> City: </label> <span>' + $("#address_city").val() + '</span></div>';
                        data_is += '<div class="rowdata"><label> State: </label> <span>' + $("#address_state").val() + '</span></div>';
                        data_is += '<div class="rowdata"><label> Zip: </label> <span>' + $("#address_zip").val() + '</span></div>';
                        data_is += '</div>';
                        if (checkboxesChecked.length > 0) {
                            if (card_id == 0) {
                                if ($("#month_year").val() == '' || $("#cvc").val() == '' || $("#cc").val() == '') {
                                    $(".payment_error_popup").hide();
                                    $(".popup-block").hide();
                                    $('body').removeClass('custom_modal_open'); // add class in body
                                    $(".checkout_confirm_box_pop").hide();
                                    var error_cu = '';
                                    if ($("#cc").val() == '') {
                                        error_cu += '<p>Please mention card number</p>';

                                    }
                                    if ($("#month_year").val() == '') {
                                        error_cu += '<p>Please mention exp. month & year</p>';
                                    }
                                    if ($("#cvc").val() == '') {
                                        error_cu += '<p>Please mention security Code - CVC</p>';
                                    }
                                    $("#payment-form").find('.custom_ajax_message').fadeIn(500);
                                    $("#payment-form").find('.custom_ajax_message').addClass('alert-danger').children('.ajax_message').html(error_cu);
                                    setTimeout(function() {
                                        $("#payment-form").find('.custom_ajax_message').fadeOut(500);
                                    }, 3000);
                                } else {
                                    $(".pay_button").show();
                                    $(".checkout_confirm_box_pop").find(".order_info_pop").html(data_is);
                                }

                            } else {
                                $(".pay_button").show();
                                $(".checkout_confirm_box_pop").find(".order_info_pop").html(data_is);
                            }
                        } else {
                            $(".pay_button").hide();
                            var cart_empty = '<div class="cart_empty_popup_message">';
                            cart_empty += 'There are no property selected. Please add or select any property';
                            cart_empty += '</div>';
                            $(".checkout_confirm_box_pop").find(".order_info_pop").html(cart_empty);
                        }
                    }
                });

                $('.pay_button').click(function() {
                    $(".popup-block").hide();
                    $(".checkout_confirm_box_pop").hide();
                    $("#payment-form").submit();
                });



            });
            Stripe.setPublishableKey('<?php echo STRIPE_PULBLISHABLE_KEY; ?>');

            function onSubmitDo() {
                $(".credit-cell-hi").val('');
                $(".credit-cell-hi").attr('placeholder', 'xxxx');
                $("#wait-div").show();
                var card_id = $('input[name=card_id]:checked', '#payment-form').val();
                if (card_id != 0) {
                    myStripeResponseHandlerCard();
                } else {
                    Stripe.card.createToken(document.getElementById('payment-form'), myStripeResponseHandler);

                }
                return false;
            };

            function myStripeResponseHandler(status, response) {
                console.log(status);
                console.log(response);
                if (response.error) {
                    //document.getElementById('payment-error').innerHTML = "<div style='padding: 8px;font-size: 14px;' class='alert-danger'>"+ response.error.message +"</div>";
                    document.getElementById('payment-error').innerHTML = response.error.message;
                    $("#wait-div").hide();
                    $(".popup-block").show();
                    $('body').addClass('custom_modal_open'); // add class in body
                    $(".payment_error_popup").show();
                    //$("#payment-form")[0].reset()

                } else {
                    var tokenInput = document.createElement("input");
                    tokenInput.type = "hidden";
                    tokenInput.name = "stripeToken";
                    tokenInput.value = response.id;
                    var paymentForm = document.getElementById('payment-form');
                    paymentForm.appendChild(tokenInput);
                    //console.log(paymentForm);
                    //console.log($("#payment-form").serialize());
                    var data = $("#payment-form").serialize();
                    //var posturl = siteUrl + 'property/charge_by_stripe';
                    var posturl = $('#payment-form').attr('action');
                    var formClass = '#payment-form';
                    $.ajax({
                        url: posturl,
                        data: data,
                        type: "POST",
                        dataType: 'json',
                        beforeSend: function() {
                            $('#wait-div').show();
                        },
                        success: function(response) {
                            $('#wait-div').hide();
                            $('body').removeClass('custom_modal_open');
                            if (response.success == true) {
                                $('#tabs-2').find('.custom_ajax_message_form').addClass('alert-success').css("display", 'block').children('.ajax_message').html(response.message).css("display", "block");
                                scrollToElement(formClass, 1000);
                                setTimeout(function() {
                                    window.location = response.redirect_url;
                                }, 700);
                            } else {
                                $(formClass).find('.custom_ajax_message_form').addClass('alert-danger').css("display", 'block').children('.ajax_message').html(response.message).css("display", "block");
                                scrollToElement(formClass, 1000);
                                setTimeout(function() {
                                    //window.location.href = response.redirect_url;
                                }, 700);
                            }
                        },
                        error: function(data) {
                            $('#wait-div').hide();
                            $('body').removeClass('custom_modal_open'); // add class in body
                            alert("Server Error.");
                            return false;
                        }
                    });

                }

            };

            function myStripeResponseHandlerCard() {
                var data = $("#payment-form").serialize();
                //var posturl = siteUrl + 'property/charge_by_stripe';
                var posturl = $('#payment-form').attr('action');
                var formClass = '#payment-form';
                $.ajax({
                    url: posturl,
                    data: data,
                    type: "POST",
                    dataType: 'json',
                    beforeSend: function() {
                        $('#wait-div').show();
                    },
                    success: function(response) {
                        $('#wait-div').hide();
                        $('body').removeClass('custom_modal_open');
                        if (response.success == true) {
                            $(formClass).find('.ajax_report').addClass('alert-success').css("display", 'block').children('.ajax_message').html(response.message).css("display", "block");
                            scrollToElement(formClass, 1000);
                            setTimeout(function() {
                                window.location.href = response.redirect_url;
                            }, 700);
                        } else {
                            $(formClass).find('.ajax_report').addClass('alert-danger').css("display", 'block').children('.ajax_message').html(response.message).css("display", "block");
                            scrollToElement(formClass, 1000);
                            setTimeout(function() {
                                //window.location.href = response.redirect_url;
                            }, 700);
                        }
                    },
                    error: function(data) {
                        $('#wait-div').hide();
                        $('body').removeClass('custom_modal_open');
                        alert("Server Error.");
                        return false;
                    }
                });
            };
        </script>
        <?php //echo "<pre>"; print_r($properties_info); exit; ?>
        <form action="<?php echo base_url('property/charge_by_stripe'); ?>" method="POST" id="payment-form" onsubmit="return onSubmitDo()">
            <div class="ajax_report alert custom_ajax_message_form display-hide" role="alert" style="margin-bottom: 10px; margin-left: 0px; width: 500px; position: relative; top: 100px;">
                <span class="close-message"></span>
                <div class="ajax_message">Hello Message</div>
            </div>
            <div class="tpay-acc-setup">
                <div class="shopping-cart-sec">
                    <h2>Checkout</h2>
                    <h2><a href="<?= site_url('add-property') ?>" class="add_property my-link-third"> Add Another Property</a></h2>
                    <div class="shopping-cart-sec-in">
                        <h4>Summary of Services</h4>
                        <table class="checkout_table" border="1" cellpadding="0" cellspacing="0">
                            <thead>
                                <tr>
                                    <th> </th>
                                    <th>Property</th>
                                    <th>Services</th>
                                    <th>Units
                                        <a href="#" data-toggle="tooltip" tooltip="A unit is 1 single family property or 1 unit from a multi-family property.">
                                            <span class="glyphicon glyphicon-info-sign"></span>
                                        </a>
                                    </th>
                                    <th>Price</th>
                                    <th>Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($properties_info as $key=> $value) { ?>
                                <tr>
                                    <td>
                                        <input checked class="select_property" type="checkbox" name="property[]" id="property_<?= $value->prop_id ?>" value="<?= $value->prop_id ?>">
                                        <label for="property_<?= $value->prop_id ?>"></label>
                                    </td>
                                    <td>
                                        <input type="hidden" name="property_address[]" id="property_address_<?= $value->prop_id ?>" value="<?= $value->address1 ?> <?= $value->address2 ?>, <?= $value->city ?> <?= $value->region_name ?> <?= $value->zip ?>">
                                        <?= $value->address1 ?>
                                            <?= $value->address2 ?>,
                                                <?= $value->city ?>
                                                    <?= $value->region_name ?>
                                                        <?= $value->zip ?></td>
                                    <td>tServices</td>
                                    <td>1</td>
                                    <td>$
                                        <?php echo amount_formated(PER_PROPERTY_AMOUNT); ?>/month/unit</td>
                                    <td>$
                                        <?php echo amount_formated(PER_PROPERTY_AMOUNT); ?>
                                    </td>
                                </tr>

                                <?php } ?>
                                <tr>
                                    <td colspan="5">Total</td>
                                    <td id="total_amount">$
                                        <?php echo amount_formated($total_amount); ?>
                                    </td>
                                </tr>

                                <?php if( checkTrialTimePeriod($this->session->userdata('MEM_ID')) != false){ ?>
                                <tr>
                                    <td colspan="5">Trial account</td>
                                    <td id="total_amount"> $0.00 (Trial)</td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <?php //echo "<pre>"; print_r($memberRecord); ?>
                    <h4 style="color:#00659F;  font-family:robotobold;  font-size:16px;  text-align:left;padding-left: 28px;"> Billing information</h4>
                    <div class="cart-form ch-form accordian-grp">
                        <div class="panel-group" id="accordion">
                            <?php foreach ($stripe_customer_card as $key=> $value) { ?>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                                            <input type="radio" <?php echo ($key == 0) ? 'checked' : ''; ?> name="card_id" class="card_cl" id="card_<?php echo $value->id; ?>" value="<?php echo $value->id; ?>"> <label for="card_<?php echo $value->id; ?>">Checkout with saved card XXXX - XXXX - XXXX - <?php echo $value->last_digits; ?></label><a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $value->id; ?>"></a>
                                                       </h4>
                                </div>
                                <div id="collapse<?php echo $value->id; ?>" class="panel-collapse collapse <?php echo ($key == 0) ? 'in' : ''; ?>">
                                    <div class="panel-body">
                                        <div class="saved_deb_card">
                                            <div class="saved_deb_card_left">
                                                <span class="acc-det" id="account_text_number_<?php echo $value->id; ?>">   XXXX - XXXX - XXXX - <?php echo $value->last_digits; ?></span>
                                                <span class="bs-Badge brandd"><?php echo $value->brand; ?></span>
                                                <span class="bs-Badge fundingg"><?php echo $value->funding; ?></span>
                                                <p style="display:none">
                                                    <input type="checkbox" checked name="recurring_<?php echo $value->id; ?>" value="recurring">
                                                    <label style="padding-left:30px">Recurring </label>
                                                </p>
                                            </div>
                                            <div class="saved_deb_card_right">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                            <div class="panel panel-default">
                                <?php if(!empty($stripe_customer_card)){ ?>
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                       <input type="radio" name="card_id" id="card_0"  class="card_cl" value="0"> <label for="card_0">Checkout with new card</label><a data-toggle="collapse" data-parent="#accordion" href="#collapse0"></a>
                                  </h4>
                                </div>
                                <?php }else{ ?>

                                <input style="display:none" checked type="radio" name="card_id" id="card_0" class="card_cl" value="0">
                                <label style="display:none" for="card_0">Checkout with new card</label>
                                <a style="display:none" data-toggle="collapse" data-parent="#accordion" href="#collapse0"></a>
                                <?php } ?>

                                <div id="collapse0" class="panel-collapse collapse <?php echo empty($stripe_customer_card) ? 'in' : ''; ?>">
                                    <div class="panel-body">
                                        <span style='color: red' class="error" id='payment-error1'></span>
                                        <div class="ajax_report custom_ajax_message alert display-hide" role="alert" style="margin-bottom: 10px; margin-left: 0px; width: 500px; position: relative; top: 100px;">
                                            <span class="close-message"></span>
                                            <div class="ajax_message">Hello Message</div>
                                        </div>
                                        <div class="cart-form-divide cart-form-section">
                                            <input type="hidden" name="amount" />
                                            <p>
                                                <label>First Name</label>
                                                <input type="text" value="<?php echo $memberRecord->first_name; ?>" data-stripe="name" id="name" name="name" placeholder="" />
                                            </p>
                                            <p class="last-name">
                                                <label>Last Name</label>
                                                <input type="text" value="<?php echo $memberRecord->last_name; ?>" name="last_name" id="last_name" placeholder="" />
                                            </p>
                                        </div>

                                        <div class="clearfix"></div>
                                        <div class="cart-form-section cart-form-sectionfour">
                                            <p class="ccc">
                                                <label>CC</label>
                                                <input type="text" name="" class="credit" style="width:49%;" data-stripe="number" id="cc" placeholder="" />
                                            </p>
                                            <p class="exp-date">
                                                <label>Exp. date</label>
                                                <input type="text" readonly name="month_year" id="month_year" style="" placeholder="MM/YY " />
                                            </p>
                                            <input type="hidden" id="exp_month" name="exp_month" style="" data-stripe="exp_month" />
                                            <input type="hidden" id="exp_year" name="exp_year" style="" data-stripe="exp_year" />
                                            <p class="security-code">
                                                <label>Security Code - CVC</label>
                                                <input type="text" class="reset" data-stripe="cvc" id="cvc" placeholder="">
                                            </p>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="cart-form-section cart-form-sectionfour cart-form-sectionfour2">
                                            <p class="add">
                                                <label>Address</label>
                                                <input type="text" value="<?php //echo $memberRecord->address; ?>" data-stripe="address_line1" name="address_line1" id="address_line1" placeholder="" />
                                            </p>
                                            <p class="city">
                                                <label>City</label>
                                                <input type="text" value="<?php // echo $memberRecord->city_id; ?>" name="address_city" style="width:21%;" data-stripe="address_city" id="address_city" placeholder="" />
                                            </p>
                                            <p class="state">
                                                <label>State</label>
                                                <input type="text" value="<?php //echo get_state_name($memberRecord->state_id); ?>" name="address_state" style="width:23.5%;" id="address_state" data-stripe="address_state" placeholder="" />
                                            </p>
                                            <p class="zip">
                                                <label>Zip</label>
                                                <input type="text" value="" class="reset" name="address_zip" data-stripe="address_zip" style="width:23.5%;" id="address_zip" placeholder="" />
                                            </p>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="cart-form-divide cart-form-section">
                                            <p style="display:none">
                                                <input type="checkbox" checked name="recurring_0" value="recurring">
                                                <label style="padding-left:30px">Recurring</label>
                                            </p>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="cart-btns">
                            <input type="hidden" value="<?php echo $memberRecord->email; ?>" name="emailAddress">
                            <input type="button" class="checkout_confirm_box" name="" value="Confirm Order" />
                            <br>
                            <br>
                            <a href="<?= site_url('dashboard') ?>" id="shoping-cart-cancel-btn" class="my-link">Cancel</a>
                        </div>

                    </div>
                </div>
            </div>
        </form>



        <script type="text/javascript">
            $(function() {
                $('#month_year').datepicker({
                    changeMonth: true,
                    changeYear: true,
                    showButtonPanel: true,
                    dateFormat: 'mm/yy',
                    yearRange: '2016:2100',
                    onClose: function(dateText, inst) {
                        $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
                        $('#exp_month').val(inst.selectedMonth + 1);
                        $('#exp_year').val(inst.selectedYear);
                    }
                });
            });
        </script>
        <style>
            .ui-datepicker-calendar {
                display: none;
            }
            .ui-datepicker-header {
                width: 200px;
            }
            .ui-datepicker-close.ui-state-default.ui-priority-primary.ui-corner-all {
                float: right;
            }
            .ui-state-default {
                margin: 7px 0 0;
                padding: 1px 6px;
            }
        </style>
    </div>
</section>





<script type="text/javascript">
    jQuery(function($) {
        $(".credit").credit();
    });

    jQuery(document).ready(function() {
        jQuery('.card_cl').click(function(e) {
            $('#accordion').find('.panel-collapse').removeClass('in');
            jQuery(this).parent().parent().next().addClass('in');
        });

        jQuery('.select_property').click(function() {
            var PER_PROPERTY_AMOUNT = '<?php echo PER_PROPERTY_AMOUNT; ?>';
            var checkboxes = document.getElementsByName('property[]');
            var checkboxesChecked = [];
            for (var i = 0; i < checkboxes.length; i++) {
                if (checkboxes[i].checked) {
                    checkboxesChecked.push(checkboxes[i].value);
                }
            }
            var total_amount_is = PER_PROPERTY_AMOUNT * checkboxesChecked.length
            $("#total_amount").html('$' + total_amount_is.toFixed(2));
        });
    });

    /**
     * Credit.js
     * Version: 1.0.0
     * Author: Ron Masas
     */

    (function($) {

        $.fn.extend({
            credit: function(args) {

                $(this).each(function() {


                    // Set defaults
                    var defaults = {
                        auto_select: true
                    }

                    // Init user arguments
                    var args = $.extend(defaults, args);

                    // global var for the orginal input
                    var credit_org = $(this);

                    // Hide input if css was not set
                    credit_org.css("display", "none");

                    // Create credit control holder
                    var credit_control = $('<div></div>', {
                        class: "credit-input"
                    });

                    // Add credit cell inputs to the holder
                    for (i = 0; i < 4; i++) {
                        if (i == 3) {
                            credit_control.append(
                                $("<input />", {
                                    class: "credit-cell",
                                    placeholder: "0000",
                                    maxlength: 4
                                })
                            );
                        } else {
                            credit_control.append(
                                $("<input />", {
                                    class: "credit-cell credit-cell-hi",
                                    placeholder: "0000",
                                    maxlength: 4
                                })
                            );
                        }
                    }

                    // Print the full credit input
                    credit_org.after(credit_control);

                    // Global var for credit cells
                    var cells = credit_control.children(".credit-cell");

                    /**
                     * Set key press event for all credit inputs
                     * this function will allow only to numbers to be inserted.
                     * @access public
                     * @return {bool} check if user input is only numbers
                     */
                    cells.keypress(function(event) {
                        // Check if key code is a number
                        if (event.keyCode > 31 && (event.keyCode < 48 || event.keyCode > 57)) {
                            // Key code is a number, the `keydown` event will fire next
                            return false;
                        }
                        // Key code is not a number return false, the `keydown` event will not fire
                        return true;
                    });

                    /**
                     * Set key down event for all credit inputs
                     * @access public
                     * @return {void}
                     */
                    cells.keydown(function(event) {
                        // Check if key is backspace
                        var backspace = (event.keyCode == 8);
                        // Switch credit text length
                        switch ($(this).val().length) {
                            case 4:
                                // If key is backspace do nothing
                                if (backspace) {
                                    return;
                                }
                                // Select next credit element
                                var n = $(this).next(".credit-cell");
                                // If found
                                if (n.length) {
                                    // Focus on it
                                    n.focus();
                                }
                                break;
                            case 0:
                                // Check if key down is backspace
                                if (!backspace) {
                                    // Key is not backspace, do nothing.
                                    return;
                                }
                                // Select previous credit element
                                var n = $(this).prev(".credit-cell");
                                // If found
                                if (n.length) {
                                    // Focus on it
                                    n.focus();
                                }
                                break;
                        }
                    });

                    // On cells focus
                    cells.focus(function() {
                        // Add focus class
                        credit_control.addClass('c-focus');
                    });

                    // On focus out
                    cells.blur(function() {
                        // Remove focus class
                        credit_control.removeClass('c-focus');
                    });

                    /**
                     * Update orginal input value to the credit card number
                     * @access public
                     * @return {void}
                     */
                    cells.keyup(function() {
                        // Init card number var
                        var card_number = '';
                        // For each of the credit card cells
                        cells.each(function() {
                            // Add current cell value
                            card_number = card_number + $(this).val();
                        });
                        // Set orginal input value
                        credit_org.val(card_number);
                    });


                    if (args["auto_select"] === true) {
                        // Focus on the first credit cell input
                        credit_control.children(".credit-cell:first").focus();
                    }

                });

            }
        });

    })(jQuery);
</script>