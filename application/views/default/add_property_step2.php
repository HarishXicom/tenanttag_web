<script>
    $(document).ready(function () {
        //~ var date = new Date();
        //~ date.setMonth(date.getYear() - 18);

        $(document).on('click', ".datepicker", function () {
            $(this).removeClass("hasDatepicker");
            $(this).datepicker({
                changeMonth: true,
                changeYear: true,
                yearRange: '1940:' + (new Date).getFullYear()
            });
            $(this).datepicker("show");
        });
        //$('#datepicker').datepicker('setDate', '12/31/2000');

        $(document).on('click', ".lease-start-date", function () {
            $(this).removeClass("hasDatepicker");
            $(this).datepicker({
               // minDate: 0,
                changeMonth: true,
                changeYear: true,
                onSelect: function (selected) {
                    $(".lease-end-date").datepicker("option", "minDate", selected)
                    //$(".due-date").datepicker("option","minDate", selected)
                }
            });
            $(this).datepicker("show");
        });

        $(document).on('click', ".lease-end-date", function () {
            $(this).removeClass("hasDatepicker");
            $(this).datepicker({
                minDate: 0,
                changeMonth: true,
                changeYear: true,
                onSelect: function (selected) {
                    $(".lease-start-date").datepicker("option", "maxDate", selected)
                    //$(".due-date").datepickwhosiner("option","maxDate", selected)
                }
            });
            $(this).datepicker("show");
        });

        $(document).on('click', ".due-date", function () {
            $(this).removeClass("hasDatepicker");
            $(this).datepicker();
            $(this).datepicker("show");
        });
        
         $(document).on('change', ".sel_lt_fee", function () {
             if($(this).val()!=''){
               $(this).parent().next().show();
           }else{
               $(this).parent().next().show();
           }
        });
        

    });
</script>
<div class="level">
    <div class="container">
        <div class="level-indicator">
            <img src="<?= $this->config->item('templateassets') ?>images/level.png" alt="" />
            <ul>
                <li> Property Info</li>
                <li class="blue-col1">Tenants</li>
                <li>tServices</li>
            </ul>
        </div>
    </div>
</div>
<section class="signup-section2">
    <div class="container">
        <h2 class="tenant-txt">Add Tenant</h2>



        <?php echo form_open('', array('class' => '', 'id' => 'property-add-step2')) ?>
        <div class="ajax_report alert display-hide" role="alert" style=" float:left; width:100%; margin-top: 10px; margin-bottom:10px;width:1169px;"><span class="close-message"></span><div class="ajax_message">Hello Message</div></div>

        <div class="my-panel mr-btm0 drsecondpanel tenanttag-steps">
            <div class="panel-head">
                <div class="panel-title">
                    <h1>Tenant</h1>
                </div>
            </div>
            <div class="sub-panel-head">
                <p>    <input type="checkbox" id="test1" class="is_vacant" name="is_vacant" value="Yes"/>    <label for="test1">Vacant</label>  </p>


            </div>
            <div class="panel-body all-tenants">
                <div id="tenant_list">
                    <div class="tenant-area">
                        <ul class="form-group" id="tenant-1">
                            <li class="marginbottom"><p class="labelp">First Name</p><input type="text" required="true" customvalidation="true" maxlength="20" class="form-control my-txt-fleid fname" placeholder="" name="first_name_1"/></li>
                            <li class="marginbottom"><p class="labelp">Middle Name</p><input type="text" maxlength="10" class="form-control my-txt-fleid fname" placeholder="" name="middle_name_1"/></li>
                            <li class="marginbottom"><p class="labelp">Last Name</p><input type="text" class="form-control my-txt-fleid" maxlength="10" placeholder="" name="last_name_1"/></li>
                            <li>  
                                <div class="my-col width50">
                                    <p class="labelp">DOB</p>
                                    <input readonly type="text" required="true" class="form-control my-txt-fleid width50 tenantdob" placeholder="" name="dob_1"/> 
                                </div>
                                <div class="my-col width50  right-pull mr-rite0">
                                    <p class="labelp">Mobile</p>
                                    <input type="text" class="form-control my-txt-fleid phone_us width50" required="true" placeholder="" name="mobile_1"/> 
                                </div>
                            </li>
                            <li class="marginbottom">
                                <p class="labelp">Email</p>
                                <input type="email" required="true" class="form-control my-txt-fleid" placeholder="" name="email_1" autocomplete="off"/>
                            </li>
                            <li>
                                <div class="my-col width50">
                                    <p class="labelp">Language</p>
                                    <select class="form-control my-txt-fleid width50 right-pull mr-rite0" required="true"  name="preferred_language_1">
                                        <option value="">Select Language</option>
                                        <option value="English">English</option>
                                        <option value="Spanish">Spanish</option>
                                    </select>
                                </div>
                                <div class="my-col width50 mr-rite0">
                                    <p class="labelp">Lease Start</p>
                                    <input type="text" readonly="true" class="form-control my-txt-fleid  leasefrom" required="true"  placeholder=""  name="lease_start_date_1"/>
                                </div>
                            </li>
                            <li>
                                <div class="my-col width50">
                                    <p class="labelp">Lease End</p>
                                    <input type="text" readonly="true" required="true" disabled="true" class="form-control my-txt-fleid  leaseto" placeholder="" name="lease_end_date_1"/>
                                </div>
                                <div class="my-col width50 right-pull mr-rite0 currency_text_box">
                                    <p class="labelp">Rent Amount</p>
                                    <span class=""><i class="fa fa-usd" style="font-size:14px"></i></span>
                                    <input type="text" required="true" number="true" class="form-control my-txt-fleid " digits="true" maxlength="6" placeholder="" name="rent_amount_1"/>
                                </div>
                            </li>
                            <li>
                                <div class="my-col width50">  
                                    <p class="labelp">Rent Due</p>                                  
                                    <select class="form-control my-txt-fleid due_date_start_class" required="true"  name="due_date_1">
                                        <option value="">Rent Due</option>
                                        <?php for ($j = 1; $j < 31; $j++) { ?>
                                            <option value="<?php echo $j; ?>"><?php echo ordinal($j); ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="my-col width50 right-pull mr-rite0">
                                <p class="labelp">Late Fee type</p>
                                    <select class="form-control my-txt-fleid width50 right-pull mr-rite0 sel_lt_fee" required="true"  name="late_fee_type_1">
                                        <option value="">Late Fee type</option>
                                        <option value="Daily charge">Daily charge</option>
                                        <option value="One time">One time</option>
                                    </select>
                                </div>
                            </li>
                            <li>
                                <div class="my-col width50 currency_text_box">
                                    <p class="labelp">Late Fee</p>
                                    <span class=""><i style="font-size:14px" class="fa fa-usd"></i></span>
                                    <input type="text" class="form-control my-txt-fleid " required="true" number='true' maxlength="6" digits="true" placeholder="" name="late_fee_1"/> 
                                </div>
                                <div class="my-col my-col width50 mr-rite0">
                                    <p class="labelp">Late Fee Start</p>
                                    <select class="form-control my-txt-fleid late_fee_start_class" required="true"  name="late_fee_start_1">
                                        <option value="">Late Fee Start</option>
                                        <?php for ($j = 1; $j < 31; $j++) { ?>
                                            <option value="<?php echo $j; ?>"><?php echo ordinal($j); ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </li>
                        </ul>
                        <div class="sub-panel-head">Pets Info</div>
                        <ul class="form-group">
                            <li class="full_li">
                                <div class="my-col width50 ">
                                    <input type="hidden" id="pets1" value="Yes"  name="pets[1][]" >
                                </div>
                                <div class="my-col width50">
                                    <p class="labelp">Pet type</p>
                                    <select class="form-control my-txt-fleid width50 right-pull mr-rite0 sel_lt_fee_up" id="pets_type1"  name="pets_type[1][]" >
                                        <option value="">None</option>
                                        <option value="dog">Dog</option>
                                        <option value="cat">Cat</option>
                                        <option value="other">Other</option>
                                    </select>
                                </div>
                                <div class="my-col width50">
                                    <p class="labelp">Pet Name</p>
                                    <input type="text" class="form-control my-txt-fleid valid" placeholder="" id="pets_name1"  name="pets_name[1][]" >                           
                                </div>
                                <div class="my-col width50 currency_text_box">
                                    <p class="labelp">Pet Fee</p> 
                                    <span class=""><i style="font-size:14px" class="fa fa-usd"></i></span>                               
                                    <input type="text" class="form-control my-txt-fleid valid"  number='true' maxlength="6" digits="true"  placeholder="" id="pets_fee1"  name="pets_fee[1][]" >                           
                                </div>
                                <div class="my-col width100">
                                    <p class="labelp">Pet Breed/Description</p>
                                    <input type="text" class="form-control my-txt-fleid valid" placeholder="" id="pets_breed1"  name="pets_breed[1][]" >                           
                                </div>
                                <div class="my-col width50">
                                    <input type="hidden" value="1" name="pets_no" class="pets_no">
                                    <a class="add-pet-btn" href="javascript:;"><i class="fa fa-plus"></i>Add Pet</a>
                                </div>
                            </li>
                        </ul>
                        <div class="sub-panel-head">Emergency Contact</div>
                        <ul  class="form-group">
                            <br>
                            <li>
                                <p class="labelp">Relationship</p>
                                <select class="form-control my-txt-fleid relationship" id="relationship"  name="relationship[1]" >
                                    <option value="">Select Relationship</option>
                                    <option value="Wife">Wife</option>
                                    <option value="Husband">Husband</option>
                                    <option value="Mother">Mother</option>
                                    <option value="Father">Father</option>
                                    <option value="Friend">Friend</option>
                                    <option value="Other">Other</option>
                                </select>
                            </li>                            
                            <li>
                                <div class="my-col width50 ">
                                    <p class="labelp">Name</p>
                                    <input type="text"  maxlength="20" class="form-control my-txt-fleid valid emergency_name" id="emergency_name1" placeholder="" name="emergency_name[1]" value=""/>
                                </div>
                                <div class="my-col width50 right-pull mr-rite0">
                                    <p class="labelp">Phone</p>
                                    <input type="text"  class="form-control my-txt-fleid valid phone_us emergency_phone" id="emergency_phone1" placeholder="" name="emergency_phone[1]" value=""/>
                                </div>
                            </li>
                            <li>
                                <p class="labelp">Email</p>
                                <input type="email" class="form-control my-txt-fleid valid emergency_email" id="emergency_email1" placeholder="" name="emergency_email[1]" value=""/>
                            </li>
                        </ul>
                        <ul   class="form-group">
                            <li class="full_li custom-full-li">
                                <div class="my-col width50 ">
                                    <span class="upload-btn">Upload Lease<input type="file" class="upl_lease" name="upload_lease_1"></span><div class="require_lease_doc"><!-- <em>*</em> -->You can add your lease now or later. However, your tenant will not be able to set up mobile rent payment without a lease on file.</div>
                                </div>
                                <!-- <div class="my-col width50 mr-rite0 req"> *Required for tPay </div> -->
                            </li>
                            <p class="btn_view_del 1ls_file_name upload_lease_files_cs"></p><br>
                        </ul>

                    </div>
                </div>
                <input type="hidden" value="1" name="total_tenant" class="tenant_no">

                <a href="javascript:;" class="edit-btn mr-btm30 add-tenant"><i class="fa fa-plus"></i>Add Tenant</a>
            </div>

        </div>
        <?php //echo '<pre>sss';print_r($memberRecord->mailing_address1);die;?>

        <?php if ($memberRecord->mailing_address1 == '') { ?>
            <div class="my-panel ">
                <div class="panel-head">
                    <div class="panel-title">
                        <h1>Add Your Mailing Address for Rent Payment</h1>
                    </div>
                </div>
                <div class="panel-body">
                    <ul class="address_mailing_ul form-group border0">

                        <li><input type="text" class="form-control my-txt-fleid" required="true" maxlength="30" placeholder="Address 1" name="address_1" value="<?php if (isset($memberRecord->mailing_address1) && $memberRecord->mailing_address1 != '') {
            echo $memberRecord->mailing_address1;
        } ?>"/></li>
                        <li><input type="text" class="form-control my-txt-fleid" placeholder="Address 2" maxlength="30" name="address_2" value="<?php if (isset($memberRecord->mailing_address2) && $memberRecord->mailing_address2 != '') {
            echo $memberRecord->mailing_address2;
        } ?>"/>

                        </li>
                        <li>
                            <input type="text" class="form-control my-txt-fleid " value="<?php if (isset($memberRecord->mailing_unit) && $memberRecord->mailing_unit != 0) {
            echo $memberRecord->mailing_unit;
        } ?>"  placeholder="Unit#" name="unit"/>
                        </li>
                        <li>
                            <input type="text" class="form-control my-txt-fleid " required="true" maxlength="20" value="<?php if (isset($memberRecord->mailing_city) && $memberRecord->mailing_city != '') {
                            echo $memberRecord->mailing_city;
                        } ?>" placeholder="City" name="city"/>
                        </li>
                        <li>
                            <?php
                            $data = array();
                            $state_id = $memberRecord->mailing_state_id;
                            $states = get_states('223');
                            if (!empty($states)) {
                                foreach ($states as $state) {
                                    $data[$state->region_id] = $state->region_name;
                                }
                            }
                            $list = "id='state'  class='form-control my-txt-fleid' required='1'";
                            echo form_dropdown('state', $data, $state_id, $list);
                            ?> 

                        </li>
                        <li>
                            <input type="text" class="form-control my-txt-fleid " required="true" maxlength="10" placeholder="Zipcode" name="zip" value="<?php if (isset($memberRecord->mailing_zip) && $memberRecord->mailing_zip != 0) {
                                echo $memberRecord->mailing_zip;
                            } ?>"/>
                        </li>
                    </ul>
                </div>

            </div>

<?php } ?>
        <div class="center">	
            <a href="<?= site_url('update-property-step1') ?>" class="my-link my-back">Back</a>
            <input type="submit" class="next" value="NEXt" style=""/></div>
<?php echo form_close(); ?>
    </div>
</section>



<style>
    .upload-btn > input {
        bottom: 0;
        cursor: pointer;
        font-size: 0;
        left: 0;
        opacity: 0;
        position: absolute;
        right: 0;
        top: 0;
        width: 100%;
    }
</style>
