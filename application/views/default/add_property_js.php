<script>

$(document).delegate('.del_photo', 'click', function () {
    var id = $(this).attr('id');
    var target = $(this).parent().parent();
    swal({
        title: "Are you sure?",
        text: 'Are you sure to delete this photo',
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false
    },
    function () {
        $.ajax({
            url: '<?php echo base_url('property/delete_photo'); ?>',
            type: 'POST',
            data: {id: id},
            beforeSend: function () {
                $('#wait-div').show();
            },
            success: function (data) {
                $("#wait-div").hide();
                var myArray = JSON.parse(data);
                if (data == 1) {
                    swal({
                        title: "Success",
                        text: 'Photo Deleted Successfully',
                        type: "success"
                    },
                    function () {
                        target.remove();
                    });
                } else {
                    swal("Error!", myArray.error_message, "error");

                }
            },
            error: function () {
                $("#wait-div").hide();
                swal("Error!", "Server not responding.", "error");
            }
        });
    });
});

function imageIsLoaded(e) {
    $('#preview_profile_pic').attr('href', e.target.result);
    $('#preview_profile_pic').show();
}
    $(document).ready(function () {
        //$('#wait-div').show();
        /*$(".property_img").change(function () {
            var path = $(this).val();
            var filename = path.replace(/^.*\\/, "");
            var html = filename + " <img class='clr_up_img del_up_img' src='<?php echo base_url() ?>assets/default/images/delete-icon.png'/>";
            $(".show_prp_img").html(html);            
           /* var reader = new FileReader();
            reader.onload = imageIsLoaded;
            reader.readAsDataURL(this.files[0]);
            */
           /* $(".show_prp_img").show();
        });*/

        $(".property_img").change(function () {
            var path = $(this).val();
            var filename = path.replace(/^.*\\/, "");
            //var html = filename + " <img class='clr_up_img del_up_img' src='<?php echo base_url() ?>assets/default/images/delete-icon.png'/>";
            var html = ' <a class="btn btn-edit btn-primary clr_up_img del_up_img" href="javascript:void(0);" >Delete</a> ';
            var reader = new FileReader();
            reader.onload = function(){
                var dataURL = reader.result;
                html_view = '<a href="'+ dataURL +'" target="_blank" class="btn preview_profile_pic" id="preview_profile_pic"> View </a> ';
                $(".show_prp_img").html(filename + ' ' + html_view + html );            
            };
            reader.readAsDataURL(this.files[0]);

            $(".show_prp_img").show();
        });

        /*$(".comm_docs").change(function () {
            var path = $(this).val();
            var filename = path.replace(/^.*\\/, "");
            var html = filename + " <img class='clr_up_img del_up_doc' src='<?php echo base_url() ?>assets/default/images/delete-icon.png'/>";
            $(".show_file_name2").html(html);
            $(".show_file_name2").show();
        });*/

        $(".comm_docs").change(function () {
            var path = $(this).val();
            var filename = path.replace(/^.*\\/, "");
            var html = " <a class='btn btn-edit btn-primary clr_up_img del_up_doc' href='javascript:;'> Delete</a>";

            var reader = new FileReader();
            reader.onload = function(){
                var dataURL = reader.result;
                html_view = '<a href="'+ dataURL +'" target="_blank" class="btn preview_doc" id="preview_doc"> View </a> ';
                $(".show_file_name2").html(filename + ' ' + html_view + html );            
            };
            reader.readAsDataURL(this.files[0]);

            //$(".show_file_name2").html(html);
            $(".show_file_name2").show();
        });

        $(document).delegate('.del_up_img', 'click', function () {
            $(".property_img").val('');
            $(".show_prp_img").html('');
            $(".show_prp_img").hide();
        });

        $(document).delegate('.del_up_doc', 'click', function () {
            $(".comm_docs").val('');
            $(".show_file_name2").html('');
            $(".show_file_name2").hide();
        });
        $(document).delegate('.del_lease_doc', 'click', function () {
        $(this).parent().find('.upl_lease').val('');    
        $(this).parent().html('');
          //  $(".show_file_name2").html('');
            //$(".show_file_name2").hide();
        });
        
        

        $(document).delegate('.del_prop_img', 'click', function () {
            var id = $(this).attr('id');
            var target = $(this).parent();
            $.ajax({
                url: '<?php echo base_url('property/delete_photo'); ?>',
                type: 'POST',
                data: {id: id},
                beforeSend: function () {
                    $('#wait-div').show();
                },
                success: function (data) {
                    $("#wait-div").hide();
                    $(".property_img").val('');
                    $(".show_prp_img").html('');
                    $(".show_prp_img").hide();

                },
                error: function () {
                    $("#wait-div").hide();
                    swal("Error!", "Server not responding.", "error");
                }
            });
        });

        $(document).delegate('.delu_comm_doc', 'click', function () {
            var id = $(this).attr('id');
            
            $.ajax({
                url: '<?php echo base_url('property/delete_community_doc'); ?>',
                type: 'POST',
                data: {id: id},
                beforeSend: function () {
                    $('#wait-div').show();
                },
                success: function (data) {
                    $("#wait-div").hide();
                    $(".comm_docs").val('');
                    $(".show_file_name2").html('');
                    $(".show_file_name2").hide();
                },
                error: function () {
                    $("#wait-div").hide();
                    swal("Error!", "Server not responding.", "error");
                }
            });
        });


/*        $(document).delegate('.upl_lease', 'change', function () {
            var path = $(this).val();
            var filename = path.replace(/^.*\\/, "");
            if(filename != ''){
                var html = filename + " <img class='clr_up_img del_lease_doc' src='<?php echo base_url() ?>assets/default/images/delete-icon.png'/>";
                $(this).parent().parent().parent().next().html(html);
                $(this).parent().parent().parent().next().show();
            }
        });*/

        $(document).delegate('.upl_lease', 'change', function () {
            var path = $(this).val();
            var d_this = $(this);
            var filename = path.replace(/^.*\\/, "");
            if(filename != ''){
                var html = " <a class='clr_up_img del_lease_doc' href='javascript:;'> Delete</a><br><br>";
                var reader = new FileReader();
                reader.onload = function(){
                    var dataURL = reader.result;
                    html_view = '<a href="'+ dataURL +'" target="_blank" class="btn preview_lease"> View </a> ';                    
                    d_this.parent().parent().parent().next().html(filename + ' ' + html_view + html);
                };
                reader.readAsDataURL(this.files[0]);

                $(this).parent().parent().parent().next().show();
            }
        });

        $("#country_id").change(function () {
            var countryId = $(this).val();
            if (countryId != 0) {
                $.ajax({
                    url: '<?php echo base_url(); ?>property/get_state',
                    type: 'POST',
                    data: {countryId: countryId},
                    beforeSend: function () {
                        $('#wait-div').show()
                    },
                    success: function (data) {
                        var dataArray = jQuery.parseJSON(data);
                        $("#state").html('<option value="0">Plese select</option>');
                        $.each(dataArray, function (idx, rec) {

                            $("#state").append('<option value="' + rec.region_id + '">' + rec.region_name + '</option>');
                        })

                        $('#wait-div').hide()

                    },
                    error: function () {
                        $('#wait-div').hide()
                        swal("error in form submission");
                    }
                });
            }
        });

        $("select[name='heat_type']").change(function () {
            if ($(this).val() != 'N/A') {
                $("select[name='heat_filter_size']").show();

            } else {
                $("select[name='heat_filter_size']").hide();
            }
        });

        $("select[name='heat_filter_size']").change(function () {
            if ($(this).val() == 'Other') {
                $("input[name='heat_filter_size_other']").show();

            } else {
                $("input[name='heat_filter_size_other']").val('');
                $("input[name='heat_filter_size_other']").hide();
            }
        });

        $("select[name='ac_filter_size']").change(function () {
            if ($(this).val() == 'Other') {
                $("input[name='ac_filter_size_other']").show();

            } else {
                $("input[name='ac_filter_size_other']").val('');
                $("input[name='ac_filter_size_other']").hide();
            }
        });

        $("select[name='ac_type']").change(function () {
            if ($(this).val() != 'N/A') {
                $("select[name='ac_filter_size']").show();
            } else {
                $("select[name='ac_filter_size']").hide();
            }
        });

        //  $('.tenantdob').datepicker({changeMonth:true,changeYear:true,maxDate: 0,defaultDate:"01/01/1985"});
        $("body").delegate(".tenantdob", "focusin", function () {
            $(this).datepicker({changeMonth: true, changeYear: true, maxDate: 0, defaultDate: "01/01/1985", yearRange: "-80:+0"});
        });
        $("body").delegate(".accountdob", "focusin", function () {
            $(this).datepicker({changeMonth: true, changeYear: true, maxDate: 0, defaultDate: "01/01/1985", yearRange: "-80:+0"});
        });

        $("body").delegate(".leasefrom", "focusin", function () {
            $(this).datepicker({
                defaultDate: "+1w",
                changeMonth: true,
              //  minDate: 0,
                changeYear: true,
                numberOfMonths: 1,
                onSelect: function (selectedDate) {
                    $(this).parent().parent().next().find('input').removeAttr('disabled');
                    $(this).parent().parent().next().find('input').focus();
                    $(this).parent().parent().next().find('input').blur();
                },
                onClose: function (selectedDate) {
                    $(this).parent().parent().next().find('input').datepicker("option", "minDate", selectedDate);
                }
            });
        });

        $("body").delegate(".leaseto", "focusin", function () {
            $(this).datepicker({
                defaultDate: "+1w",
                changeMonth: true,
                changeYear: true,
                minDate: 0,
                numberOfMonths: 1,
                onSelect: function (selectedDate) {
                    //   $(this).parent().prev().find('input').focus(); 
                    //   $(this).parent().prev().find('input').blur(); 
                },
                onClose: function (selectedDate) {
                    $(this).parent().prev().find('input').datepicker("option", "maxDate", selectedDate);
                }
            });
        });



        //   $('.leasefrom').datepicker({changeMonth: true, changeYear: true});
        //  $('.leaseto').datepicker({changeMonth: true, changeYear: true});
        $("#property-add-step1").validate({
            // Specify the validation rules
            rules: {
                property_type: {
                    valueNotEquals: ''
                },
                address1: {
                    required: true,
                    maxlength: 100,
                },
                state: {
                    valueNotEquals: '0'
                },
                city: {
                    required: true,
                    maxlength: 40,
                },
                /*zip: {
                    required: true,
                    number:true,
                    maxlength:6,
                },*/
                community: {
                    required: '#radio10:checked'
                },
                pest_service_provider: {
                    required: '#radio01_pest_control_home:checked'
                },
                pest_service_provider_number: {
                    required: '#radio01_pest_control_home:checked',
                    //   phoneUS: true
                },
                yard_service_provider: {
                    required: '#radio01_service_provided:checked'
                },
                yard_service_provider_number: {
                    required: '#radio01_service_provided:checked',
                    //    phoneUS: true
                }
            },
            // Specify the validation error messages
            messages: {
                property_type: {
                    valueNotEquals: 'Please Select Property Type',
                },
                state: {
                    valueNotEquals: 'Please Select State',
                },
                address1: {
                    required: 'Enter address'
                },
                city: {
                    required: 'Enter city'
                },
                /*zip: {
                    required: 'Enter valid zip code',
                    number: 'Enter valid zip code',
                },*/
                community: {
                    required: 'Enter Community name'
                },
                pest_service_provider: {
                    required: 'Enter Service Provider'
                },
                pest_service_provider_number: {
                    required: 'Enter Provider Number'
                },
                yard_service_provider: {
                    required: 'Enter Service Provider'
                },
                yard_service_provider_number: {
                    required: 'Enter Provider Number'
                }
            },
            submitHandler: function (form) {
            	$('#wait-div').show();
                submit_ajax_form('property-add-step1');
            }
        });


        $("#property-add-step2").validate({
            submitHandler: function (form) {
                submit_ajax_form('property-add-step2');
            }
        });

        $("#property-update-step2").validate({
            submitHandler: function (form) {
                submit_ajax_form('property-update-step2');
            }
        });

        jQuery.validator.addMethod("valueNotEquals", function (value, element, arg) {
            return arg != value;
        }, "Value must not equal arg.");
        jQuery.validator.addMethod("customvalidation", function (value, element) {
            return /^[A-Za-z_ -]+$/.test(value);
        }, "Alpha Characters Only.");

        jQuery.validator.addMethod('phoneUS', function (phone_number, element) {
            phone_number = phone_number.replace(/\s+/g, '');
            return this.optional(element) || phone_number.length > 9 &&
                    phone_number.match(/^(1-?)?(\([2-9]\d{2}\)|[2-9]\d{2})-?[2-9]\d{2}-?\d{4}$/);
        }, 'Enter a valid phone.');

        jQuery.validator.addMethod("valueNotEquals", function (value, element, arg) {
            return arg != value;
        }, "Value must not equal arg.");

    });

</script>
<script>
    $(document).on('click', '.is_community', function (event) {
        var value = $(this).val();
        if (value == 'Yes')
            $('.community_box').fadeIn();
        if (value == 'No')
            $('.community_box').fadeOut();
    });
    $("input:radio[name='pest_control_home']").click(function () {
        var value = $(this).val();
        if (value == 'yes')
            $('.service-provider').fadeIn();
        if (value == 'no')
            $('.service-provider').fadeOut();
    });

    $(document).on('click', '.add-new-service', function (event) {
        var assets = '<?= $this->config->item('templateassets') ?>';
        var new_created_services = $('.new_created_services').val();
        new_created_services = parseInt(new_created_services) + 1;

        var dataToAppend = '<div  class="form-group1 new-services-' + new_created_services + '"><div class="my-div width150"><label>Create</label></div>';
        dataToAppend += '<div class="my-div"><input type="text" class="form-control my-txt-fleid" placeholder=" Service" name="new_service_' + new_created_services + '"/></div>';
        dataToAppend += '<div class="my-div ">';
        dataToAppend += '<input type="text" class="form-control my-txt-fleid width50" placeholder="Company" name="new_service_provider_' + new_created_services + '"/>';
        dataToAppend += '<input type="text" class="form-control my-txt-fleid width50 right-pull mr-rite0 phone_us" placeholder="Contact no. " name="new_service_provider_number_' + new_created_services + '"/>';
        dataToAppend += '</div>';
        dataToAppend += '<div class="my-div width150"><a href="javascript:;" class="delete-row " id="cross' + new_created_services + '" data-value="' + new_created_services + '" style="margin-left:35px;"><img src="' + assets + 'images/cross.png" alt=""/></a></div></div>';
        $('.services-panel').append(dataToAppend);
        $('.new_created_services').val(new_created_services);
    });

    $(document).on('click', '.delete-row', function () {
        var id = $(this).attr('data-value');
        $('#cross' + id).remove();
        $('.new-services-' + id).remove();
        var new_created_services = $('.new_created_services').val();
        new_created_services = parseInt(new_created_services) - 1;
        $('.new_created_services').val(new_created_services);
    });

    $(document).on('click', '.delete-service', function () {
        var id = $(this).attr('data-value');
        $('#delete-service' + id).remove();
        $('.already-service-no-' + id).remove();
        var already_services = $('.already_services').val();
        already_services = parseInt(already_services) - 1;
        $('.already_services').val(already_services);
    });

    function show_service_provided(value)
    {
        if (value == 'yes')
        {
            $('.is_yard_service_provide').show();
        }
        if (value == 'no')
        {
            $('.is_yard_service_provide').hide();
        }
    }



    function submit_ajax_form(id) {
        var postUrl = $("#" + id).attr('action');
        var formClass = '#' + id;
        $('#' + id).ajaxSubmit({
            url: postUrl,
            dataType: 'json',
            beforeSend: function () {
                $('#wait-div').show();
            },
            success: function (response) {
                $(formClass).find('.ajax_report').removeClass('alert-success').removeClass('alert-danger').fadeIn(200);
                $('#wait-div').hide();
                if (response.success)
                    $(formClass).find('.ajax_report').addClass('alert-success').children('.ajax_message').html(response.success_message);
                else
                    $(formClass).find('.ajax_report').addClass('alert-danger').children('.ajax_message').html(response.error_message);
                if (response.url)
                {
                    setTimeout(function () {
                        window.location.href = response.url;
                    }, 700);
                }
                if (response.resetForm)
                    $(formClass).resetForm();
                if (response.selfReload)
                {
                    //location.reload();
                    setTimeout(function () {
                        location.reload();
                    }, 700);
                }
                if (response.scrollToElement)
                    scrollToElement(formClass, 1000);
                if (response.ajaxCallBackFunction)
                    ajaxCallBackFunction(response);
                setTimeout(function () {
                    $(formClass).find('.ajax_report').fadeOut(500);
                }, 10000);
            },
            error: function () {
                $('#wait-div').hide();
                alert('server error');
            }


        });
    }


</script>
<?php
$prefix_codes = $number = array();
$codes = get_prefix_code();
if (!empty($codes)) {
    foreach ($codes as $code) {
        if ($code->prefix_code != '') {
            $prefix_codes[$code->prefix_code] = $code->prefix_code;
        }
    }
}
for ($j = 1; $j < 31; $j++) {
    $number[$j] = ordinal($j);
}
?>
<script>
    $(document).on('click', '.add-tenant', function (event) {
        var assets = '<?= $this->config->item('templateassets') ?>';
        var tenant_no = $('.tenant_no').val();
        tenant_no = parseInt(tenant_no) + 1;
        var property_info = '<?php echo json_encode(getAnyTenantInfoByPropertyId()); ?>';
        if(property_info != 'null'){
        	var property_info = $.parseJSON(property_info);        	
        }
        var lease_start_date = (typeof property_info.lease_start_date === "undefined") ? '' : property_info.lease_start_date;
        var lease_end_date = (typeof property_info.lease_end_date === "undefined") ? '' : property_info.lease_end_date;
        var late_fee = (typeof property_info.late_fee === "undefined") ? '' : property_info.late_fee;
        var rent_amount = (typeof property_info.rent_amount === "undefined") ? '' : property_info.rent_amount;
        var late_fee_type = (typeof property_info.late_fee_type === "undefined") ? '' : property_info.late_fee_type;
        var due_date = (typeof property_info.due_date === "undefined") ? '' : property_info.due_date;
        var late_fee_start = (typeof property_info.late_fee_start === "undefined") ? '' : property_info.late_fee_start;


        var prefixCodes = '<?php echo json_encode($prefix_codes); ?>';
        prefixCodes = JSON.parse(prefixCodes);
        var numberDays = '<?php echo json_encode($number); ?>'; 
        numberDays = JSON.parse(numberDays);
        var dataToAppend = '<div class="tenant-area"><div class="panel-head"><div class="panel-title"><h1>Tenant</h1></div></div><ul class="form-group" id="tenant-' + tenant_no + '">';
        //var dataToAppend = '<ul class="form-group" id="tenant-' + tenant_no + '">';
            dataToAppend += '<li class="marginbottom">';
            dataToAppend += '<p class="labelp">First Name</p><input type="text" class="form-control my-txt-fleid" required="true" customvalidation="true" placeholder="" name="first_name_' + tenant_no + '"/>';
            dataToAppend += '</li>';

            dataToAppend += '<li class="marginbottom">';
            dataToAppend += '<p class="labelp">Middle Name</p><input type="text" class="form-control my-txt-fleid" placeholder="" name="middle_name_' + tenant_no + '"/>'
            dataToAppend += '</li>';

            dataToAppend += '<li>';
            dataToAppend += '<p class="labelp">Last Name</p><input type="text" class="form-control my-txt-fleid" placeholder="" name="last_name_' + tenant_no + '"/>';
            dataToAppend += '</li>';

            dataToAppend += '<li>';
                dataToAppend += '<div class="my-col width50">';
                    dataToAppend += '<p class="labelp">DOB</p><input type="text" class="form-control my-txt-fleid width50 tenantdob"  required="true" placeholder="" name="dob_' + tenant_no + '"/>';
                dataToAppend += '</div>';
                dataToAppend += '<div class="my-col width50 mr-rite0">';
                    dataToAppend += '<p class="labelp">Mobile</p><input type="text" class="form-control my-txt-fleid phone_us width50 " placeholder=""  required="true"  name="mobile_' + tenant_no + '"/>';
                dataToAppend += '</div>';
            dataToAppend += '</li>';

            dataToAppend += '<li class="marginbottom">';
            dataToAppend += '<p class="labelp">Email</p><input type="email" class="form-control my-txt-fleid" placeholder=""  required="true" email="true" name="email_' + tenant_no + '" autocomplete="off"/>';
            dataToAppend += '</li>';
            dataToAppend += '<a href="javascript:;" class="delete-tenant-row" id="cross' + tenant_no + '" data-value="' + tenant_no + '" style=""><img src="' + assets + 'images/cross.png" alt=""/></a>';

            dataToAppend += '<li>';
                dataToAppend += '<div class="my-col width50">';
                    dataToAppend += '<p class="labelp">Language</p><select class="form-control my-txt-fleid  width50 right-pull mr-rite0"  required="true" name="preferred_language_' + tenant_no + '"><option value="">Select Language</option><option value="English">English</option><option value="Spanish">Spanish</option></select>';
                dataToAppend += '</div>';
                dataToAppend += '<div class="my-col width50 mr-rite0">';
                    /*dataToAppend += '<span class="upload-btn">Upload Lease<input type="file" class="upl_lease" name="upload_lease_' + tenant_no + '"></span>';*/
                    dataToAppend += '<p class="labelp">Lease Start</p><input type="text" readonly="true" class="form-control my-txt-fleid width50 leasefrom" required="true" placeholder="" value="'+lease_start_date+'" name="lease_start_date_' + tenant_no + '"/>';
                dataToAppend += '</div>';                
            dataToAppend += '</li>';

            dataToAppend += '<li>';
                dataToAppend += '<div class="my-col width50">';
                    dataToAppend += '<p class="labelp">Lease End</p><input type="text" readonly="true"  required="true" class="form-control my-txt-fleid width50 right-pull mr-rite0 leaseto" value="'+lease_end_date+'"  placeholder="" name="lease_end_date_' + tenant_no + '"/>';
                dataToAppend += '</div>';
                dataToAppend += '<div class="my-col width50 right-pull mr-rite0 currency_text_box">';
                    dataToAppend += '<p class="labelp">Rent Amount</p><span class=""><i class="fa fa-usd" style="font-size:14px"></i></span>';
                    dataToAppend += '<input type="text" required="true" number="true" class="form-control my-txt-fleid width50" maxlength="6" digits="true" placeholder="" value="'+rent_amount+'"  name="rent_amount_' + tenant_no + '"/>';
                dataToAppend += '</div>';
            dataToAppend += '</li>';

            dataToAppend += '<li>';
                dataToAppend += '<div class="my-col width50">';
                    dataToAppend += '<p class="labelp">Rent Due</p><select  required="true" class="form-control my-txt-fleid width50 right-pull mr-rite0 due_date_start_class" name="due_date_' + tenant_no + '">';
                    dataToAppend += '<option value="">Rent Due</option>';
                    $.each(numberDays, function (key, value) {
                        if(due_date == key){
                            dataToAppend += '<option selected value="' + key + '">' + value + '</option>';
                        }else{
                            dataToAppend += '<option  value="' + key + '">' + value + '</option>';
                        }
                    });
                    dataToAppend += '</select>';
                dataToAppend += '</div>'; 
                dataToAppend += '<div class="my-col width50 right-pull mr-rite0">';
                    dataToAppend += '<p class="labelp">Late Fee type</p>';
                    dataToAppend += '<select class="form-control my-txt-fleid width50 right-pull mr-rite0 sel_lt_fee" name="late_fee_type_' + tenant_no + '">';
                    dataToAppend += '<option value="">Late Fee type</option>';
                    if(late_fee_type == "Daily charge"){
                        dataToAppend += '<option selected value="Daily charge">Daily charge</option>';
                    }else{
                        dataToAppend += '<option value="Daily charge">Daily charge</option>';
                    }
                     if(late_fee_type == "One time"){
                        dataToAppend += '<option selected value="One time">One time</option>';
                    }else{
                        dataToAppend += '<option value="One time">One time</option>';
                    }
                    dataToAppend += '</select>';
                dataToAppend += '</div>';
            dataToAppend += '</li>';

            dataToAppend += '<li>';
                dataToAppend += '<div class="my-col width50 currency_text_box" >';
                    dataToAppend += '<p class="labelp">Late Fee</p> <span class=""><i style="font-size:14px" class="fa fa-usd"></i></span><input type="text" class="form-control my-txt-fleid width50" maxlength="6" digits="true" placeholder="" value="'+late_fee+'"  name="late_fee_' + tenant_no + '"/>';
                dataToAppend += '</div> ';
                dataToAppend += '<div class="my-col my-col width50 mr-rite0 ">';
                    dataToAppend += '<p class="labelp">Late fee start</p><select valueNotEquals=""  required="true" class="form-control my-txt-fleid width50 right-pull mr-rite0 late_fee_start_class" name="late_fee_start_' + tenant_no + '">';
                    dataToAppend += '<option value="">Late fee start</option>';
                    $.each(numberDays, function (key, value) {
                        if(late_fee_start == key){
                            dataToAppend += '<option selected value="' + key + '">' + value + '</option>';
                        }else{
                            dataToAppend += '<option  value="' + key + '">' + value + '</option>';
                        }
                    });
                    dataToAppend += '</select>';
                dataToAppend += '</div>';
            dataToAppend += '</li>';
        dataToAppend += '</ul>';

        dataToAppend += '<div class="sub-panel-head">Pets Info</div>';
        dataToAppend += '<ul  class="form-group">';
            dataToAppend += '<li class="full_li">';
                dataToAppend += '<div class="my-col width50 ">';
                    dataToAppend += '<input type="hidden" name="pets['+tenant_no+'][]" valyue="Yes" id="pets'+tenant_no+'">';
                dataToAppend += '</div>';
                dataToAppend += '<div class="my-col width50">';
                    dataToAppend += '<p class="labelp">Pet type</p><select name="pets_type['+tenant_no+'][]" id="pets_type'+tenant_no+'" class="form-control my-txt-fleid width50 right-pull mr-rite0 sel_lt_fee_up"><option value="">None</option><option value="dog">Dog</option><option value="cat">Cat</option><option value="other">Other</option></select></div>';
                dataToAppend += '<div class="my-col width50 ">';
                    dataToAppend += '<p class="labelp">Pet Name</p><input type="text" placeholder="" name="pets_name['+tenant_no+'][]" value="" id="pets_name'+tenant_no+'" class="form-control my-txt-fleid valid">';
                dataToAppend += '</div>  ';
                dataToAppend += '<div class="my-col width50 ">';
                    dataToAppend += '<p class="labelp">Pet Fee</p><input type="text" placeholder="Pet Fee" name="pets_fee['+tenant_no+'][]" value="" id="pets_fee'+tenant_no+'" class="form-control my-txt-fleid valid">';
                dataToAppend += '</div>  ';
                dataToAppend += '<div class="my-col width100 ">';
                    dataToAppend += '<p class="labelp">Pet Breed/Description</p><input type="text" placeholder="" name="pets_breed['+tenant_no+'][]" value="" id="pets_breed'+tenant_no+'" class="form-control my-txt-fleid valid">';
                dataToAppend += '</div>  ';
                dataToAppend += ' <div class="my-col width50">';
                    dataToAppend += '<a href="javascript:;" class="add-pet-btn"><i class="fa fa-plus"></i>Add Pet</a>';
                dataToAppend += '</div>';
            dataToAppend += '</li>';
        dataToAppend += '</ul>';

        dataToAppend += '<div class="sub-panel-head">Emergency Contact</div>';
        dataToAppend += '<ul class="form-group">';
            dataToAppend += '<li>';
                dataToAppend += '<p class="labelp">Relationship</p><select class="form-control my-txt-fleid relationship" id="relationship'+tenant_no+'"  name="relationship['+tenant_no+']" ><option value="">Select Relationship</option><option value="Wife">Wife</option><option value="Husband">Husband</option><option value="Mother">Mother</option><option value="Father">Father</option><option value="Friend">Friend</option><option value="Other">Other</option></select>';
            dataToAppend += '</li>';
            dataToAppend += ' <li>';
                dataToAppend += '<div class="my-col width50 ">';
                    dataToAppend += '<p class="labelp">Name</p><input type="text" value=""  maxlength="20" name="emergency_name['+tenant_no+']" placeholder="" id="emergency_name'+tenant_no+'" class="form-control my-txt-fleid valid emergency_name">';
                dataToAppend += '</div>';
                dataToAppend += '<div class="my-col width50 right-pull mr-rite0">';
                    dataToAppend += '<p class="labelp">Phone</p><input type="text" value="" name="emergency_phone['+tenant_no+']" placeholder="" id="emergency_phone'+tenant_no+'" class="form-control my-txt-fleid valid emergency_phone phone_us">';
                dataToAppend += '</div>';
            dataToAppend += '</li>';
            dataToAppend += ' <li>';
                dataToAppend += '<p class="labelp">Email</p><input type="email" value="" name="emergency_email['+tenant_no+']" placeholder="" id="emergency_email'+tenant_no+'" class="form-control my-txt-fleid valid emergency_email">';
            dataToAppend += '</li>';
        dataToAppend += '</ul>';
        
        dataToAppend += '<ul class="form-group">';
            dataToAppend += '<li class="full_li custom-full-li">';
                dataToAppend += '<div class="my-col width50 ">';
                    dataToAppend += '<span class="upload-btn">Upload Lease<input type="file" class="upl_lease" name="upload_lease_' + tenant_no + '"></span><div class="require_lease_doc"><em>*</em>You can add your lease now or later. However, your tenant will not be able to set up mobile rent payment without a lease on file.</div>';
                dataToAppend += '</div>';
            dataToAppend += '</li>';
            dataToAppend += '<p class="btn_view_del upload_lease_files_cs"></p>';
        dataToAppend += '</ul>';
        dataToAppend += '</div>';
        
        $('#tenant_list').append(dataToAppend);
        $('.tenant_no').val(tenant_no);

    });

    $(document).on('click', '.is_vacant', function () {
        if (this.checked)
        {
            $('.all-tenants').hide();
        }
        else
        {
            $('.all-tenants').show();
        }
    });
    $(document).on('click', '.delete-tenant-row', function () {
        var id = $(this).attr('data-value');
        //$('#cross' + id).remove();
        /*$('#tenant-' + id).remove();*/
        //alert($(this).closest('ul').html());
        $(this).closest('ul').parent('.tenant-area').remove();
        //$(this).parent('.tenant-area').remove();
        var tenant_no = $('.tenant_no').val();
        tenant_no = parseInt(tenant_no) - 1;
        $('.tenant_no').val(tenant_no);
    });



    $(document).on('click', '.upadd-tenant', function (event) {
        var assets = '<?= $this->config->item('templateassets') ?>';
        var property_info = '<?php echo json_encode(getAnyTenantInfoByPropertyId()); ?>';
        if(property_info != 'null'){
        	var property_info = $.parseJSON(property_info);        	
        }
        var lease_start_date = (typeof property_info.lease_start_date === "undefined") ? '' : property_info.lease_start_date;
        var lease_end_date = (typeof property_info.lease_end_date === "undefined") ? '' : property_info.lease_end_date;
        var late_fee = (typeof property_info.late_fee === "undefined") ? '' : property_info.late_fee;
        var rent_amount = (typeof property_info.rent_amount === "undefined") ? '' : property_info.rent_amount;
        var late_fee_type = (typeof property_info.late_fee_type === "undefined") ? '' : property_info.late_fee_type;
        var due_date = (typeof property_info.due_date === "undefined") ? '' : property_info.due_date;
        var late_fee_start = (typeof property_info.late_fee_start === "undefined") ? '' : property_info.late_fee_start;
        var tenant_no = $('.tenant_no').val();
        tenant_no = parseInt(tenant_no) + 1;
        var pets_no = $('.pets_no').val();
        pets_no = parseInt(pets_no) + 1;


        var prefixCodes = '<?php echo json_encode($prefix_codes); ?>';
        prefixCodes = JSON.parse(prefixCodes);
        var numberDays = '<?php echo json_encode($number); ?>';
        numberDays = JSON.parse(numberDays);
        var dataToAppend = '<div class="tenant-area"><div class="panel-head"><div class="panel-title"><h1>Tenant</h1></div></div>';
        dataToAppend += '<ul class="form-group" id="tenant-' + tenant_no + '">';
        dataToAppend += '<li class="marginbottom">';
        dataToAppend += '<p class="labelp">First Name</p><input type="text" class="form-control my-txt-fleid" required="true" customvalidation="true" placeholder="" name="first_name[' + tenant_no + ']"/>';
        dataToAppend += '</li>';
        dataToAppend += '<li class="marginbottom">';
        dataToAppend += '<p class="labelp>Middle Name</p><input type="text" class="form-control my-txt-fleid" placeholder="" name="middle_name[' + tenant_no + ']"/>';
        dataToAppend += '</li>';
        dataToAppend += '<li class="marginbottom">';
        dataToAppend += '<p class="labelp">Last Name</p><input type="text" class="form-control my-txt-fleid" placeholder="" name="last_name[' + tenant_no + ']"/>';
        dataToAppend += '</li>';
        dataToAppend += '<a href="javascript:;" class="delete-tenant-row" id="cross' + tenant_no + '" data-value="' + tenant_no + '" style=""><img src="' + assets + 'images/cross.png" alt=""/></a>';


        dataToAppend += '<li>';
            dataToAppend += '<div class="my-col width50">';
                dataToAppend += '<p class="labelp">DOB</p><input type="text" class="form-control my-txt-fleid width50 tenantdob"  required="true" placeholder="" name="dob[' + tenant_no + ']"/>';
            dataToAppend += '</div>';
            dataToAppend += ' <div class="my-col width50 right-pull mr-rite0">';
                dataToAppend += '<p class="labelp">Mobile</p><input type="text" class="form-control my-txt-fleid phone_us width50 " placeholder=""  required="true"  name="mobile[' + tenant_no + ']"/>';
            dataToAppend += ' </div>';
        dataToAppend += '</li>';

        dataToAppend += '<li class="marginbottom">';
            dataToAppend += '<p class="labelp">Email</p><input type="email" class="form-control my-txt-fleid" placeholder=""  required="true" email="true" name="email[' + tenant_no + ']" autocomplete="off"/>';
        dataToAppend += '</li>';

        dataToAppend += '<li>';
            dataToAppend += '<div class="my-col width50">';
                dataToAppend += '<p class="labelp">Language</p><select class="form-control my-txt-fleid  width50 right-pull mr-rite0"  required="true" name="preferred_language[' + tenant_no + ']"><option value="">Select Language</option><option value="English">English</option><option value="Spanish">Spanish</option></select>';
            dataToAppend += '</div>';
            dataToAppend += '<div class="my-col width50 right-pull mr-rite0">';
                dataToAppend += '<p class="labelp">Lease Start</p><input type="text" readonly="true" class="form-control my-txt-fleid width50 leasefrom" required="true" placeholder="" value="' + lease_start_date + '" name="lease_start_date[' + tenant_no + ']"/>';
            dataToAppend += '</div>';
        dataToAppend += '</li>';

        dataToAppend += '<li>';
            dataToAppend += '<div class="my-col width50">';
                dataToAppend += '<p class="labelp">Lease End</p><input type="text" readonly="true" required="true" class="form-control my-txt-fleid width50 right-pull mr-rite0 leaseto"  value="' + lease_end_date + '" placeholder="" name="lease_end_date[' + tenant_no + ']"/>';
            dataToAppend += '</div>';
            dataToAppend += '<div class="my-col width50 right-pull mr-rite0 currency_text_box">';
                dataToAppend += '<p class="labelp">Rent Amount</p><span class=""><i class="fa fa-usd" style="font-size:14px"></i></span>';
                dataToAppend += '<input type="text" required="true" number="true" class="form-control my-txt-fleid width50" maxlength="6" placeholder="" digits="true"  value="' + rent_amount + '" name="rent_amount[' + tenant_no + ']"/>';
            dataToAppend += '</div>';
        dataToAppend += '</li>';

        dataToAppend += '<li>';
            dataToAppend += '<div class="my-col width50  ">';
                dataToAppend += '<p class="labelp">Rent Due</p><select valueNotEquals=""  required="true" class="form-control my-txt-fleid width50 right-pull mr-rite0 due_date_start_class"  name="due_date[' + tenant_no + ']">';
                dataToAppend += '<option value="">Rent Due</option>';
                $.each(numberDays, function (key, value) {
                    if(due_date == key){
                        dataToAppend += '<option selected value="' + key + '">' + value + '</option>';
                    }else{
                        dataToAppend += '<option  value="' + key + '">' + value + '</option>';
                    }
                });
                dataToAppend += '</select>';
            dataToAppend += '</div>';
            dataToAppend += '<div class="my-col width50 right-pull mr-rite0">';
                dataToAppend += '<p class="labelp">Late Fee Type</p><select class="form-control my-txt-fleid width50 sel_lt_fee_up right-pull mr-rite0" name="late_fee_type[' + tenant_no + ']">';
                dataToAppend += '<option value="">Late Fee type</option>';
                if(late_fee_type == "Daily charge"){
                    dataToAppend += '<option selected value="Daily charge">Daily charge</option>';
                }else{
                    dataToAppend += '<option value="Daily charge">Daily charge</option>';
                }
                 if(late_fee_type == "One time"){
                    dataToAppend += '<option selected value="One time">One time</option>';
                }else{
                    dataToAppend += '<option value="One time">One time</option>';
                }
                dataToAppend += '</select>';
            dataToAppend += '</div>';
        dataToAppend += '</li>';

        dataToAppend += '<li>';
            dataToAppend += '<div class="my-col width50 currency_text_box">';
                dataToAppend += '<p class="labelp">Late Fee</p><span class=""><i style="font-size:14px" class="fa fa-usd"></i></span>';
                dataToAppend += '<input type="text" class="form-control my-txt-fleid width50" maxlength="6" placeholder="Late Fee" digits="true" value="'+late_fee+'" name="late_fee[' + tenant_no + ']"/>';
            dataToAppend += '</div> ';
            dataToAppend += '<div class="my-col my-col width50 mr-rite0 ">';
                dataToAppend += '<p class="labelp">Late Fee Start</p><select valueNotEquals=""  required="true" class="form-control my-txt-fleid width50 right-pull mr-rite0 late_fee_start_class" name="late_fee_start[' + tenant_no + ']">';
                    dataToAppend += '<option value="">Late fee start</option>';
                    $.each(numberDays, function (key, value) {
                        if(late_fee_start == key){
                        dataToAppend += '<option selected value="' + key + '">' + value + '</option>';
                    }else{
                        dataToAppend += '<option  value="' + key + '">' + value + '</option>';
                    }
                    });
                    dataToAppend += '</select>';
            dataToAppend += '</div>';
        dataToAppend += '</li>';
        dataToAppend += '</ul>';

        dataToAppend += '<div class="sub-panel-head">Pets Info</div>';
            dataToAppend += '<ul  class="form-group">';
                dataToAppend += '<li class="full_li">';
                dataToAppend += '<div class="my-col width50 ">';
                    dataToAppend += '<input type="hidden" name="pets['+tenant_no+'][]" value="Yes" id="pets'+tenant_no+'">';
                dataToAppend += '</div>';
                dataToAppend += '<div class="my-col width50">';
                    dataToAppend += '<p class="labelp">Pet Type</p><select name="pets_type['+tenant_no+'][]" id="pets_type'+tenant_no+'" class="form-control my-txt-fleid width50 right-pull mr-rite0 sel_lt_fee_up"><option value="">None</option><option value="dog">Dog</option><option value="cat">Cat</option><option value="other">Other</option></select>';
                dataToAppend += '</div>';
                dataToAppend += '<div class="my-col width50 ">';
                    dataToAppend += '<p class="labelp">Pet Name</p><input type="text" placeholder="" name="pets_name['+tenant_no+'][]" value="" id="pets_name'+tenant_no+'" class="form-control my-txt-fleid valid">';
                dataToAppend += '</div>  ';
                dataToAppend += '<div class="my-col width50 ">';
                    dataToAppend += '<p class="labelp">Pet Fee</p><input type="text"  number="true" maxlength="6" digits="true"  placeholder="" name="pets_fee['+tenant_no+'][]" value="" id="pets_fee'+tenant_no+'" class="form-control my-txt-fleid valid">';
                dataToAppend += '</div>  ';
                dataToAppend += '<div class="my-col width100 ">';
                    dataToAppend += '<p class="labelp">Pet Breed/Description</p><input type="text" placeholder="" name="pets_breed['+tenant_no+'][]" value="" id="pets_breed'+tenant_no+'" class="form-control my-txt-fleid valid">';
                dataToAppend += '</div>  ';
                dataToAppend += ' <div class="my-col width50">';
                    dataToAppend += '<a href="javascript:;" class="add-pet-btn"><i class="fa fa-plus"></i>Add Pet</a>';
                dataToAppend += '</div>';
            dataToAppend += '</li>';
        dataToAppend += '</ul>';

        dataToAppend += '<div class="sub-panel-head">Emergency Contact</div>';
        dataToAppend += '<ul class="form-group">';
            dataToAppend += '<li>';
                dataToAppend += '<p class="labelp">Relationship</p><select class="form-control my-txt-fleid relationship" id="relationship'+tenant_no+'"  name="relationship['+tenant_no+']" ><option value="">Select Relationship</option><option value="Wife">Wife</option><option value="Husband">Husband</option><option value="Mother">Mother</option><option value="Father">Father</option><option value="Friend">Friend</option><option value="Other">Other</option></select>';
            dataToAppend += '</li>';
            dataToAppend += ' <li>';
                dataToAppend += '<div class="my-col width50 ">';
                    dataToAppend += '<p class="labelp">Name</p><input type="text" maxlength="20" value="" name="emergency_name['+tenant_no+']" placeholder="" id="emergency_name'+tenant_no+'" class="form-control my-txt-fleid valid emergency_name"></div><div class="my-col width50 right-pull mr-rite0"><p class="labelp">Phone</p><input type="text" value="" name="emergency_phone['+tenant_no+']" placeholder="" id="emergency_phone'+tenant_no+'" class="form-control my-txt-fleid valid emergency_phone phone_us">';
                dataToAppend += '</div>';
            dataToAppend += '</li>';
            dataToAppend += ' <li>';
                dataToAppend += '<p class="labelp">Email</p><input type="email" value="" name="emergency_email['+tenant_no+']" placeholder="" id="emergency_email'+tenant_no+'" class="form-control my-txt-fleid valid emergency_email">';
            dataToAppend += '</li>';
        dataToAppend += '</ul>';

        dataToAppend += '<ul class="form-group">';
            dataToAppend += ' <li class="full_li custom-full-li">';
                dataToAppend += '<div class="my-col width50 ">';
                    dataToAppend += '<span class="upload-btn">Upload Lease<input class="upl_lease" type="file" name="upload_lease[' + tenant_no + ']"></span><div class="require_lease_doc"><em>*</em>You can add your lease now or later. However, your tenant will not be able to set up mobile rent payment without a lease on file.</div>'
                dataToAppend += '</div>';
            dataToAppend += '<input type="hidden"  name="uploaded_lease_doc[' + tenant_no + ']" class="uploaded_lease_doc">';            
            dataToAppend += '</li><p class="btn_view_del upload_lease_files_cs"></p>';
        dataToAppend += '</ul>';
        dataToAppend += '</div>';
        $('#tenant_list').append(dataToAppend);
        $('.tenant_no').val(tenant_no);

    });


    $(document).on('click', '.add-pet-btn', function (event) {
        var assets = '<?= $this->config->item('templateassets') ?>';
        var pets_no = $('.pets_no').val();
        pets_no = parseInt(pets_no) + 1;
        var tenant_no = $('.tenant_no').val();
        tenant_no = parseInt(tenant_no);
        var assets = '<?= $this->config->item('templateassets') ?>';

        var dataToAppend = '<li class="full_li"><div class="my-col width50 "><input type="hidden" name="pets['+tenant_no+'][]"  value="Yes"  id="pets'+tenant_no+'"></div>';
        dataToAppend += '<div class="my-col width50"><select name="pets_type['+tenant_no+'][]" id="pets_type'+tenant_no+'" class="form-control my-txt-fleid width50 right-pull mr-rite0 sel_lt_fee_up"><option value="">None</option><option value="dog">Dog</option><option  value="cat">Cat</option><option value="other">Other</option></select></div>';
        dataToAppend += '<div class="my-col width50 "><input placeholder="Pet Name" type="text" name="pets_name['+tenant_no+'][]" value="" id="pets_name'+tenant_no+'" class="form-control my-txt-fleid valid"></div>  ';
        dataToAppend += '<div class="my-col width50 "><input placeholder="Pet Fee"  number="true" maxlength="6" digits="true"  type="text" name="pets_fee['+tenant_no+'][]" value="" id="pets_fee'+tenant_no+'" class="form-control my-txt-fleid valid"></div>  ';
        dataToAppend += '<div class="my-col width100 "><input placeholder="Pet Breed/Description" type="text" name="pets_breed['+tenant_no+'][]" value="" id="pets_breed'+tenant_no+'" class="form-control my-txt-fleid valid"></div>  ';
        dataToAppend += '<div class="my-col width50"><a href="javascript:;" class="delete-pets-row " id="pets_row' + tenant_no + '" data-value="' + tenant_no + '" style=""><img src="' + assets + 'images/cross.png" alt=""/></a></div></li>';
        
        $(this).closest('ul').append(dataToAppend);
        $('.pets_no').val(pets_no);

    });

    $(document).on('click', '.delete-pets-row', function () {
        $(this).closest('li').remove();
    });
    
    $(document).on('change', '.due_date_start_class', function () {
        var due_date_start_class = $(this).val();
        //alert(due_date_start_class)
        $(this).parent().parent().next().find('.late_fee_start_class').empty();
        for (var i = due_date_start_class; i <= 30; i++) {
            $(this).parent().parent().next().find('.late_fee_start_class').append('<option value="'+ i +'">'+i+'th</option>');
        };            
    });

    $(document).on('change', '.late_fee_start_class', function () {
        var late_fee_start_class = $(this).val();
        var due_date_start_class = $(this).parent().parent().prev().find('.due_date_start_class').val();
        //alert(due_date_start_class+' < '+late_fee_start_class);
        if(due_date_start_class > late_fee_start_class){
            //alert(due_date_start_class)
            $('.due_date_start_class option[value="'+late_fee_start_class+'"]').attr('selected', 'selected');        
            
        }
    });


</script>	


