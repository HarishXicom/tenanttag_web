
<script type="text/javascript" src="<?= $this->config->item('templateassets') ?>js/jquery-ui.js"></script>
<!-- <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script> -->
<script>
    /*$(document).ready(function () {
     //   $('.tMessaging').attr('checked', 'checked');
     //  $('.tMaintenace').attr('checked', 'checked');
     });*/
   
    
    
</script>
<style>
[tooltip]:hover:before {
    background: #00659f;
    color: #fff;
    font-size: 14px;
    margin-left: 32px;
    margin-top: -5px;
    opacity: 1;
    z-index: 9999;
}
[tooltip]:before {
    border: 1px solid #00659f;
    box-shadow: 1px 1px 4px #777777;
    color: #333;
    content: attr(tooltip);
    opacity: 0;
    padding: 10px;
    position: absolute;
    transition: all 0.15s ease 0s;
    max-width: 640px;
}
[tooltip]:not([tooltip-persistent]):before {pointer-events:none;}


</style>	
<div class="level">
    <div class="container">
        <div class="level-indicator">
            <img src="<?= $this->config->item('templateassets') ?>images/full-level.png" alt="" />
            <ul>
                <li> Property Info</li>
                <li>Tenants</li>
                <li  class="blue-col1">tServices</li>
            </ul>
        </div>
    </div>
</div>
<section class="signup-section2 wb-signup-section">
    <div class="container"> 
        <?php echo form_open('', array('class' => 'vendor-detail', 'id' => '')) ?>	
        <div class="ajax_report alert display-hide" role="alert" style="margin-bottom: 10px; margin-left: 0px; width: 500px; position: relative; top: 100px;">
            <span class="close-message"></span>
            <div class="ajax_message">Hello Message</div>
        </div>
        <input type="hidden" value="<?php echo $this->session->userdata('PROPERTY_ID'); ?>" class="prp_id"/>    
        <div class="step3-container">
            <div class="page_heading tServices_page_heading">tServices Set Up</div>
            <div class="signup-3" style="margin-top:5px">
                <?php $verification_status = $this->common_model->getSingleFieldFromAnyTable('verification_status', 'mem_id', $this->session->userdata('MEM_ID'), 'tbl_members');?>
                <?php if ($verification_status == 'No') { ?>
                     <p class="my-check">   
                         <input type="checkbox"  id="test2" <?php if($property_info->tmessaging != "No") { echo 'checked'; } ?> name="tMessaging" class="tMessaging" readonly="true" value="Yes"/>    
                        <label for="test2">
                            tText <a href="#" data-toggle="tooltip" tooltip="Custom message dynamically generated from property, lease and tenant information, You can always add more"><span class="glyphicon glyphicon-info-sign"></span></a></label> 
                            <span>
                                
                                <a href="javascript:;" id="sendeTesttText" class="ttextsetup">Test</a> Will be sent to your mobile #<br>
                                
                            </span>
                        
                    </p>
                    <script>
                    $(document).ready(function(){
                       $(".tMessaging,.tMaintenace").click(function(){
                            swal("Error!",'Please confirm your email address. tServices will not be active until confirmation is complete.', "error");
                            $('.tMessaging').attr('checked', false);
                            $('.tMaintenace').attr('checked', false);
                       }); 
                    });
                    </script>
                <?php } else { ?>
                    <p class="my-check">   
                        <input type="checkbox" <?php if($property_info->tmessaging != "No") { echo 'checked'; } ?> id="test2" name="tMessaging" class="tMessaging"  value="Yes"/>    
                         <label for="test2">
                            tText <a href="#" data-toggle="tooltip" tooltip="Custom message dynamically generated from property, lease and tenant info. You can always add more."><span class="glyphicon glyphicon-info-sign"></span> </a></label> 

                            <span>                                
                                <a href="javascript:;" id="sendeTesttText" class="ttextsetup">Test</a> Text sent to landlord's mobile#.<br>
                            </span>
                    </p>                
                <?php } ?>

            </div>
            <div class="signup-3  signup-tmaintenance">
                <?php $verification_status = $this->common_model->getSingleFieldFromAnyTable('verification_status', 'mem_id', $this->session->userdata('MEM_ID'), 'tbl_members');?>
                <?php if ($verification_status == 'No') { ?>
                    <p class="my-check mr-btm0">
                        <input type="checkbox"  <?php if($property_info->tmaintenance != "No") { echo 'checked'; } ?> id="test3" name="tMaintenace" class="tMaintenace" readonly="true" value="Yes"/>    
                        <label for="test3">tMaintenance <a href="#" data-toggle="tooltip" tooltip="Landlords will always receive maintenance requests. If vendors are selected, they will also receive requests and can go ahead and schedule an appointment. This save everyone's time."><span class="glyphicon glyphicon-info-sign"></span></a></label> 
                         
                    </p>
                <?php } else { ?>
                    <p class="my-check mr-btm0">
                        <input type="checkbox" id="test3" <?php if($property_info->tmaintenance != "No") { echo 'checked'; } ?> name="tMaintenace" class="tMaintenace"  value="Yes"/>    
                       <label for="test3">tMaintenance <a href="#" data-toggle="tooltip" tooltip="Landlords will always receive maintenance requests. If vendors are selected, they will also receive requests and can go ahead and schedule an appointment. This save everyone's time."><span class="glyphicon glyphicon-info-sign"></span></a></label> 
                        <div class="helpful-tips"><a style="text-decoration:underline" href="#" data-toggle="tooltip" tooltip="Establish a per call limit with your vendors, ex. $250, so they can complete general service calls without having to bother you or charge an additional service fee for a return trip.">Helpful tips</a></div>
                        
                    </p>
                <?php } ?>
                <div class="signup">
                    <h2>tMaintenance Vendors</h2> &nbsp;&nbsp;&nbsp;<span style="font-size:12px">*Please make sure that only 1 vendor is selected for each service category. If you want to use the service but do not have any vendors, you must add yourself as a vendor.</span>
                </div>
                <div class="table-content signup-table tabbb">
                    <table class="my-table vndrs_table">
                        <thead>
                            <tr>
                                <th>
                                    <?php if (!empty($allVendors)) { ?>
                                        <input type="checkbox" class="tnt_check_all" id="test025">    <label for="test025">&nbsp;</label>
                                    <?php } else { ?>
                                        &nbsp;
                                    <?php } ?>
                                </th>
                                <th>Company Name </th>
                                <th>Phone</th>
                                <th>Email</th>
                                <th>Service</th>

                            </tr>
                        </thead>
                        <tbody class="all-record"> 
                            <?php if (!empty($allVendors)) { ?>
                                <tr>

                                    <td colspan="5"> <h2 class="add_vndr">Select existing vendor</h2> </td>
                                </tr>
                                <?php //echo "<pre>"; print_R($allVendors); ?>
                                <?php //echo "<pre>"; print_R($allVendorsOfProperty); exit; ?>
                                <?php foreach ($allVendors as $value) { ?> 
                                    <?php //if ($value->property_id != $this->session->userdata('PROPERTY_ID')) { ?>
                                        <tr id="display_row_<?= $value->v_id ?>">
                                            <td class="width30">
                                                <?php if ($value->property_id == $this->session->userdata('PROPERTY_ID')) { ?>
                                                            <input type="checkbox" checked class="tnt_check" name="vendor_ids[]" value="<?= $value->v_id ?>" id="vendor_<?php echo $value->v_id; ?>">    
                                                    <?php } else{ ?>
                                                            <input type="checkbox" class="tnt_check" name="vendor_ids[]" value="<?= $value->v_id ?>" id="vendor_<?php echo $value->v_id; ?>">    
                                                <?php } ?>
                                                <label for="vendor_<?php echo $value->v_id; ?>">&nbsp;</label> 
                                            </td>

                                            <td> <?= $value->company_name ?>  </td>
                                            <td><?= $value->mobile_no ?></td>
                                            <td><?= $value->email ?></td>
                                            <td>
                                                <?php $exist_services = explode(',', $value->service); ?>
                                                <?php //echo "<pre>"; print_r($vendorServices); ?>
                                                    <?php foreach ($vendorServices as $service) { ?>
                                                            <?php if (in_array($service->t_id, $exist_services)) { ?>
                                                                    <?php echo $service->name .','; ?>
                                                            <?php } ?>
                                                    <?php } ?>
                                                <?php //echo $services[$vndr->service]; ?>
                                            </td>
                                        </tr>
                                    <?php //} ?>

                                <?php }
                            }
                            ?>
                            <!-- <tr id="" style="">
                                <td colspan="5"> 

                                    <h2 class="add_vndr">Add New Vendors</h2>

                                </td>
                            </tr> -->


                            <tr id="add-vendor-row">
                                <td colspan="5" class="center-cont">
                                    <a class="btn-tenant add-vendor-row" style="color:#ffffff;width:auto;margin-left: 0px; padding: 10px 13px 10px 20px;text-align: center; float:none; display:inline-block;text-decoration:none" href="javascript:;"  data-value="">Add New Vendor <i class="fa fa-plus" style="margin-left:5px; margin-right:0"></i></a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <span>                                
                        <a href="javascript:;" id="sendTesttMaintenanceEmail" class="tmainsetup">Test</a> <span style="color: #7b8185;">Message sent to landlord's email.</span><br>
                    </span>
                </div>
            </div>
            <div class="signup-3 signup-tpay">

                <div class="signup tpay-signup">
                    <p class="my-check mr-btm0">
                        
                        <span class="tpay_span">
                        <span class="ttpayy">						
						<input type="checkbox" <?php if($property_info->tpay != "No") { echo 'checked'; } ?> id="testPay" name="tPay" class="tPay <?php if($property_info->tpay == "No" && $tpay_account_status == 0){ ?> popup_check_account_disable <?php } ?>" value="Yes"/>    
                        <label for="testPay">tPay  
                            <?php //echo ($tpay_account_status == 0) ? "Please complete the information or you may deactivate tPay by unchecking the tPay box in tServices Set Up" : "Set-up the bank account where you want your tenant's rental payments deposited."; ?>
                           <!-- <a href="#" data-toggle="tooltip" tooltip="Set-up the bank account where you want your tenant's rental payments deposited.">-->
						   <a href="#" data-toggle="tooltip" tooltip="One-time set-up must be completed for Bank Account to be Activated.">
                                <span class="glyphicon glyphicon-info-sign"></span>
                            </a>
                        </label>  
						</span>
						
                           <span class="account_status_left"> Bank Account Status </span> <span class="account_status_left account_status_left_light_status">
                            <?php if($tpay_account_status == 0){ ?>
	                            <span class="togle" data-type="" data-value="" data-property="No">
	                                <span class="check"></span>
	                            </span>

	                        <?php }else{ ?>
                        		<span class="togle stop" data-type="" data-value="" data-property="Yes">
	                                <span class="check"></span>
	                            </span>
	                        <?php }  ?>
	                        </span> <br>
                            <input type="hidden" name="sy_account_status" id="sy_account_status" value="<?php echo ($tpay_account_status == 0) ? 'Inactive' : 'Active'; ?>">
                        <span class="one-step" style="line-height:18px;">    
                            <a style="padding-top:7px" href="<?= site_url('account_setup') ?>" type="button" class="accountsetup">SET UP/EDIT</a> 
                            <span style="line-height: 10px;">
                            One time set up required for activation.<br> 
                            <a style="margin-left:10px" href="javascript:;" data-toggle="modal" data-target="#how_do_ach_payment_work">How do ACH Payments work?</a>
                            </span>
                        </span>  <div class="setup_text">   </div>
                        </span>
                        <span class="tpay_span">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   </span>
                       
                    </p>
                </div>
            </div>
        </div>
        <input type="hidden" name="action-type" class="action-type" value="save">
        <span class="border-line1"></span>
        <div class="my-tenant-btn">  
            <!--
                              <input type="button" class="next my-nxt add-another-property" value="Add Property" style=""/>  
            -->	
            <a href="<?= site_url('update-property-step2') ?>" class="my-link my-back">Back</a>	
            <!--<a href="javascript:;" class="next my-nxt add-another-property"><i class="fa fa-plus"></i>Add Property</a>--> 	
            <input type="button" class="next submit-property save-property" value="Confirm" style=""/>
            <a href="javascript:;" class="my-link save-later-process">Save for Later</a>
        </div>
<?php echo form_close(); ?>

        <div class="htmlcontent" style="display:none">
            <table>
                <tr class="add-vendor-tbl">
                    <td style="font-size: 30px;"><i class="fa fa-times-circle-o"></i></td>
                    <td colspan="">
                        <div class="my-div">
                            <input class="form-control my-txt-fleid" required="true" type="text" placeholder="Company Name" name="company_name[]" id="company_name" >
                        </div>
                    </td>
                    <td>
                        <div class="my-div">
                            <input class="form-control my-txt-fleid phone_us" required="true" placeholder="Company Phone #" type="text" name="company_contact[]" id="company_contact">
                        </div>
                    </td>
                    <td>
                        <div class="my-div">
                            <input class="form-control my-txt-fleid" type="email" required="true" placeholder="Company Email" name="email[]" id="email">
                        </div>
                    </td>
                    <td> 
                        <select class="form-control width100" name="service[]" required="true" id="service">
                            <option value="">Select Service</option>
                            <?php foreach ($vendorServices as $vale) { ?>
                                <option value="<?= $vale->t_id ?>"><?= $vale->name ?></option>
                            <?php } ?>
                        </select>
                    </td>

                </tr>
            </table>
        </div>
    </div>    
</section>
<div class="end-block"></div>
<div class="payment-block" >
    <div class="my-popup checkout-form" style="display: none">
        <div class="popup-cont">
            <div class="popup-head">
                <span class="close"></span>
            </div>
            <div class="popup-body">
                <div class="center"><span class="t-icon"></span></div>

                <ul class="listing list-pop">
                    <li class="my-cross">Account Setup Incomplete</li>
                </ul>
            </div>
            <div class="popup-footer">
                <div class="center">
                    <a href="javascript:;" class="back back-popup">Back</a>
                    <a href="javascript:;" class="save save-later-account-setup-process" style="width:150px;"> SAVE FOR LATER</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
    <div id="how_do_ach_payment_work" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">ACH Payment Flow</h4>
          </div>
          <div class="modal-body">
            <p>
                Automated Clearing House (ACH) is an electronic network for financial transactions in the United States. 
                A transaction must be submitted before 7PM EST on a business day for processing to begin on the same day. 
                If not, processing will begin the following day at 7PM EST. 
                Payment to a landlord's, personal bank account is made within 4 business days after the processing of a tenant's rent payment begins as long as the tenant's payment clears.
                This cleared status is shown as PAID under the tenant's Payment History.
            </p>
            <p>
            <img src="<?php echo base_url().'assets/default/images/step-flow3.jpg'; ?>" style="width:100%">
            </p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>

      </div>
    </div>


