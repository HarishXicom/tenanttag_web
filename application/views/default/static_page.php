<section class="dr-banner bnr_pages">
    <div class="container">
        <div class="drwork-text" style="width:100%">
            <h5><?php echo $content->title; ?></h5>


        </div>
    </div>
</section>
<?php if ($content->slug == 'contact') { ?>
    <section class="howit-works contactus"  style="display: block !important;">
        <div class="container">
              <div class="cmn_style contacttxt">
                <?php echo $content->description; ?>
            </div> 
            <?php echo form_open('welcome/dosubmit', array('class' => 'ajaxForm contactus-section', 'id' => 'contact-us')) ?>
            <div class="ajax_report alert display-hide" role="alert" style="padding:10px;">
                <span class="close"></span>
                <span class="ajax_message" style="line-height:normal;margin: 0;padding: 3px;"></span>
            </div>
            <label>Name</label>  
            <input type="text" name="name"  required="true">
            <label>Mobile</label>  
            <input type="text" name="mobile"  required="true" class="phone_us">
            <label>Email</label>  
            <input type="text" name="cont_email" required="true">
            <label>Message</label>  
            <textarea name="message"></textarea>
            <div class="form-group1">
                <div class="my-div captcha">
                    <span><?php echo $math_captcha_question;?></span>
                    <input type="text" placeholder="Please answer to confirm you are a real user." class="form-control my-txt-fleid" name="math_captcha">
                </div>
            </div>
            <input type="hidden" value="<?php echo $this->session->userdata('mathcaptcha_answer'); ?>" name="mathcaptcha_answer">
            <input type="submit" value="Submit" class="drsubmitbttn"/>
            <?php form_close(); ?>
            
        </div>
    </section>
<?php } else { ?>
    <section class="howit-works" style="display: block !important;">
        <div class="container">
            <div class="cmn_style" style="width:100%">

                <?php echo $content->description; ?>
            </div>
        </div>
    </section>
<?php } ?>

<style>
.my-div.captcha{
    width: 100% !important;
    padding-right: 0px !important;
    padding-top: 16px !important;
}
.my-div.captcha > span {
    background: #00659f none repeat scroll 0 0;
    color: #fff;
    display: inline-block;
    float: left;
    font-family: "robotobold";
    font-size: 19px;
    padding: 7px 15px;
    text-align: center;
    width: 25%;
}
.my-div.captcha > input{
    margin-right: 0;
    width: 75%;
}
</style>