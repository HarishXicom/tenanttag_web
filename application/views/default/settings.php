<style>
    .my-list span{width:33% !important};
</style>

<?php echo validation_errors(); ?>
<section class="lease-part">
    <div class="container">
        <div id="tabs">
            <div class="my-tab1"> 
                <ul class="my-tab">
                    <li>
                        <a href="#tabs-1">Landlord Details</a>
                    </li>
                    <li>
                        <a href="#tabs-2">Change Password</a>
                    </li>
                     <li>
                        <a href="#tabs-3">Rent Payment Address</a>
                    </li>
                    <li>
                        <a href="#tabs-4">Allow 3rd party sharing</a>
                    </li>
                </ul>
            </div>
            <div class="tab-area">
                <div id="tabs-1">
                    <div class="tenant">
                        <ul class="tenant1">
                            <li><span class="my-span">My Profile</span>
                                <ul class="btn-group">
                                    <li><a href="javascript:void(0);" class="sky-blue-col editbinfo">Edit</a></li>
                                </ul>
                            </li>
                        </ul>
                         <?php echo form_open('', array('class' => 'ajaxForm', 'id' => "profile_form")) ?>
                        <div class="ajax_report alert display-hide" role="alert" style=" margin-top: 100px; margin-bottom:10px; "><span class="close-message"></span><div class="ajax_message">Hello Message</div></div>
                        <ul class="my-list border0 drsetting">

                            <li>
                                <span>
                                    <label class="label1">First Name</label>
                                    <input type="text" readonly="true" disabled="true" class="form-control tenant-txtfield binfo" name="first_name" value="<?php echo $info->first_name != '' ? $info->first_name : '' ?>"/>
                                    <label class="label2"></label>
                                </span>
                                <span> 
                                    <label class="label1">Last name</label>
                                    <input type="text" readonly="true" disabled="true" class="form-control tenant-txtfield binfo" name="last_name" value="<?php echo $info->last_name != '' ? $info->last_name : '' ?>"/>
                                </span>
                                <span> 
                                    <label class="label1">Company Name</label>
                                    <input type="text" readonly="true" disabled="true" class="form-control tenant-txtfield binfo" name="company_name" value="<?php echo $info->company_name != '' ? $info->company_name : '' ?>"/>
                                </span>
                            </li>

                            <li style="display:none">
                                <span>
                                    <label class="label1">Address</label>
                                    <input type="text" readonly="true" disabled="true" class="form-control tenant-txtfield binfo" name="address" value="<?php echo $info->address != '' ? $info->address : '' ?>"/>
                                    <label class="label2"></label>
                                </span>
                                <span> 
                                    <label class="label1">City</label>
                                    <input type="text" readonly="true" disabled="true" class="form-control tenant-txtfield binfo" name="city" value="<?php echo $info->city_id != '0' ? $info->city_id : '' ?>"/>
                                </span>
                                <span> 
                                    <label class="label1">State</label>
                                    <?php
                                    $state_id = $info->state_id != 0 ? $info->state_id : '';
                                    $data = array();
                                    $states = get_states('223');
                                    if (!empty($states)) {
                                        foreach ($states as $state) {
                                            $data[$state->region_id] = $state->region_name;
                                        }
                                    }
                                    $list = "id='state'  class='form-control my-txt-fleid binfo' readonly='1' disabled='true'";
                                    echo form_dropdown('state', $data, $state_id, $list);
                                    ?> 
                                </span>
                            </li>
                            <li>
                                <span>
                                    <label class="label1">Email</label>
                                     <input type="text"  readonly="true" disabled="true" class="remail form-control tenant-txtfield binfo" value="<?php echo $info->email != '' ? $info->email : '' ?>"/>
                                    <label class="label2"></label>
                                </span>
                                <span> 
                                    <label class="label1">Mobile</label>
                                    <input type="text" readonly="true" disabled="true" class="form-control tenant-txtfield phone_us binfo" name="mobile_no" value="<?php echo $info->mobile_no != '' ? $info->mobile_no : '' ?>"/>
                                    <label class="label2">  </label>
                                </span>
                            </li>
                            <li>
                                 <button style="display: none;" class="edit-btn  update_info cncl_btn">Cancel</button>
                                <button style="display: none;" class="edit-btn  update_info updt_btn">Update</button>
                            </li>
                        </ul>
                        <?php echo form_close(); ?>
                    </div>
                </div>
                <div id="tabs-2">
                    <ul class="tenant1">
                        <li><span class="my-span">Change Password</span>
                        </li>
                    </ul>
                    <?php echo form_open('', array('class' => 'ajaxForm', 'id' => "pass_form")) ?>
                    <div class="ajax_report alert display-hide" role="alert" style="clear:both; margin-top: 100px; margin-bottom:10px; "><span class="close-message"></span><div class="ajax_message">Hello Message</div></div>
                    <ul class="my-list border0 drsettingtab">
                        <li>
                            <span>
                                <label class="label1">Old Password</label>
                                <input type="password" name="old_pass"  class="form-control tenant-txtfield" value=""/>
                                <label class="label2"></label>
                            </span>
                        </li>
                        <li>
                            <span> 
                                <label class="label1">New Password</label>
                                <input type="password" name="new_pass" class="form-control tenant-txtfield" value=""/>
                            </span>
                        </li>
                        <li>
                            <span> 
                                <label class="label1">Confirm Password</label>
                                <input type="password" name="confirm_pass" class="form-control tenant-txtfield" value=""/>
                                <input type="hidden" name="change_pass" class="form-control tenant-txtfield" value="1"/>
                            </span>
                        </li>
                        <li>
                            <button  class="edit-btn edit form_status ">Update</button>
                        </li>

                    </ul>
                        <?php echo form_close(); ?>
                </div>
                <div id="tabs-3">
                    <div class="tenant">
                        <ul class="tenant1">
                            <li><span class="my-span">Mailing Address</span>
                                <ul class="btn-group">
                                    <li><a href="javascript:void(0);" class="sky-blue-col editminfo">Edit</a></li>
                                </ul>
                            </li>
                        </ul>
                         <?php echo form_open('', array('class' => 'ajaxForm', 'id' => "address_form")) ?>
                       <div class="ajax_report alert display-hide" role="alert" style=" margin-top: 100px; margin-bottom:10px; "><span class="close-message"></span><div class="ajax_message">Hello Message</div></div>
                        <ul class="my-list border0 drthirdtab">

                            <li>
                                <span>
                                    <label class="label1">Address 1</label>
                                    <input type="text" readonly="true" disabled="true" class="form-control tenant-txtfield minfo" name="mailing_address1" value="<?php echo $info->mailing_address1 != '' ? $info->mailing_address1 : '' ?>"/>
                                    <label class="label2"></label>
                                </span>
                                <span> 
                                    <label class="label1">Address 2</label>
                                    <input type="text" readonly="true" disabled="true" class="form-control tenant-txtfield minfo" name="mailing_address2" value="<?php echo $info->mailing_address2 != '' ? $info->mailing_address2 : '' ?>"/>
                                </span>
                                <span> 
                                    <label class="label1">Unit</label>
                                    <input type="text" readonly="true" disabled="true" class="form-control tenant-txtfield minfo" name="mailing_unit" value="<?php echo $info->mailing_unit != '' ? $info->mailing_unit : '' ?>"/>
                                </span>
                            </li>

                            <li>
                                
                                <span> 
                                    <label class="label1">City</label>
                                    <input type="text" readonly="true" disabled="true" class="form-control tenant-txtfield minfo" name="mailing_city" value="<?php echo $info->mailing_city != '' ? $info->mailing_city : '' ?>"/>
                                </span>
                                <span> 
                                    <label class="label1">State</label>
                                    <?php
                                    $state_id = $info->mailing_state_id != 0 ? $info->mailing_state_id : '';
                                    $data = array();
                                    $states = get_states('223');
                                    if (!empty($states)) {
                                        foreach ($states as $state) {
                                            $data[$state->region_id] = $state->region_name;
                                        }
                                    }
                                    $list = "id='state'  class='form-control my-txt-fleid minfo' readonly='1' disabled='true'";
                                    echo form_dropdown('mailing_state_id', $data, $state_id, $list);
                                    ?> 
                                </span>
                                 <span> 
                                    <label class="label1">Zip</label>
                                    <input type="text" readonly="true" disabled="true" class="form-control tenant-txtfield minfo" name="mailing_zip" value="<?php echo $info->mailing_zip != '' ? $info->mailing_zip : '' ?>"/>
                                </span>
                            </li>
                            <li>
                                 <button style="display: none;" class="edit-btn  update_minfo cncl_btn">Cancel</button>
                                <button style="display: none;" class="edit-btn  update_minfo updt_btn">Update</button>
                            </li>
                        </ul>
                        <?php echo form_close(); ?>
                    </div>
                </div>

                <div id="tabs-4">
                    <div class="tenant">
                        <ul class="tenant1">
                            <li><span class="my-span">3rd Party Sharing</span>
                                <ul class="btn-group">
                                </ul>
                            </li>
                        </ul>
                         <?php echo form_open('', array('class' => 'ajaxForm', 'id' => "sharing_form")) ?>
                           <div class="ajax_report alert display-hide" role="alert" style=" margin-top: 100px; margin-bottom:10px; "><span class="close-message"></span><div class="ajax_message">Hello Message</div></div>
                            <ul class="my-list border0 drthirdtab">

                                <li>
                                    <label class="label1">Allow 3rd Party Sharing</label>
                                    <p class="my-radio">  <input type="radio"  value="1" name="third_party_sharing" <?php echo $info->third_party_sharing == '1' ? 'checked' : ''; ?> id="third_party_sharing_y" class="all-radio2" >  <label for="third_party_sharing_y">Yes</label></p>
                                    <p class="my-radio">  <input type="radio"  value="0" name="third_party_sharing" <?php echo $info->third_party_sharing == '1' ? '' : 'checked'; ?> id="third_party_sharing_n" class="all-radio2">  <label for="third_party_sharing_n">No</label></p>
                                   
                                </li>                            
                                <li>
                                    <input type="hidden" name="sharing" value="sharing">
                                    <button style=""  name="updt_third_party_sharing" class="edit-btn  update_minfo1 updt_third_party_sharing">Update</button>
                                </li>
                            </ul>
                        <?php echo form_close(); ?>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>