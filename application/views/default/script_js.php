<script src="<?= $this->config->item('templateassets') ?>js/jquery-ui.js"></script>
<iframe id="synapse_iframe" style="top:0; height:0%;width:0%;position:fixed;z-index:100000;visibility:hidden;" frameborder="0"></iframe>
<script src="<?= $this->config->item('templateassets') ?>js/universal_iframe.js"></script>
<script>
    $(document).ready(function () {

        $(".tenant_welcome_text").click(function () {
            var prop_id = $(this).attr('id');
            //$("#addtenantSuccessModal").modal("hide");
            $.ajax({
                url: '<?php echo base_url('tenant/welcome_text_send_using_prop'); ?>',
                type: 'POST',
                data: {prop_id: prop_id},
                beforeSend: function () {
                    $('#wait-div').hide();
                    $('#next-step-success-message').hide();
                    $('.popup-block').hide();
                    $('body').removeClass('custom_modal_open');
                },
                success: function (data) {
                    $("#wait-div").hide();
                    if (data == 'success') {
                        swal({
                            title: "Success",
                            text: "Welcome message sent to tenant via sms",
                            type: "success"
                        },
                        function () {
                            location.reload();
                        });
                    } else {
                        //swal("Error!", data, "error");
                      /*  swal({
                            title: "Error!",
                            text: data,
                            type: "error"
                        },
                        function () {
                            location.reload();
                        });*/
                    }
                },
                error: function () {
                    $("#wait-div").hide();
                    swal("Error!", "Server not responding.", "error");
                }
            });
        });



        $(".chk_in_popup_btn").click(function () {
            var id = $(this).attr('id');
            $("#addtenantSuccessModal").modal("hide");
            $.ajax({
                url: '<?php echo base_url('tenant/check_in_using_popup'); ?>',
                type: 'POST',
                data: {id: id},
                beforeSend: function () {
                    $('#wait-div').show();
                },
                success: function (data) {
                    $("#wait-div").hide();
                    if (data == 'success') {
                        swal({
                            title: "Success",
                            text: "Credentials sent to tenant via sms",
                            type: "success"
                        },
                        function () {
                            location.reload();
                        });
                    } else {
                        //swal("Error!", data, "error");
                        swal({
                            title: "Error!",
                            text: data,
                            type: "error"
                        },
                        function () {
                            location.reload();
                        });
                    }
                },
                error: function () {
                    $("#wait-div").hide();
                    swal("Error!", "Server not responding.", "error");
                }
            });
        });


        

        //edit user
        $("#add_tenant_form").validate({
            // Specify the validation rules
            rules: {
                
                first_name: {
                    required: true,
                    customvalidation: true,
                    maxlength: 20,
                },
                last_name: {
                    required: true,
                    customvalidation: true,
                    maxlength: 20,
                },
                email: {
                    required: true,
                    email: true
                },
                password: {
                    required: true,
                    minlength: 6,
                },
                mobile_no: {
                    required: true,
               //     phoneUS: true
                },
                rent_amount: {
                    required: true,
                    number: true
                },
                late_fee: {
                    required: true,
                    number: true
                },
                late_fee_start: {
                    required: true,
                    number: true
                },
                lease_start_date: {
                    required: true,
                },
                dob: {
                    required: true,
                },
                lease_end_date: {
                    required: true,
                },
                due_date: {
                    required: true,
                },
                preferred_language: {
                    valueNotEquals: '',
                },
            },
            // Specify the validation error messages
            messages: {
                last_name: {
                    customvalidation: 'Only alphabets allowed'
                },
                first_name: {
                    customvalidation: 'Only alphabets allowed'
                },
                email: {
                    email: 'Enter valid email'
                },
                password:{
                    minlength:'Minimum 6 characters'
                },
                preferred_language: {
                    valueNotEquals: 'Please Select Language',
                }

            },
            submitHandler: function (form) {
               
                var posturl = siteUrl + 'add-tenant';
                var sessionId = '<?= $this->session->userdata('MEM_ID') ?>';
                var property_id = $("#property_id").val();
                if (sessionId != '')
                {
                    var data = $('#add_tenant_form').serialize();
                  
                    $.ajax({
                        url: posturl,
                        data: data,
                        dataType: 'json',
                        type: "POST",
                        beforeSend: function () {
                            $('#wait-div').show();
                        },
                        success: function (data) {
                            $('#wait-div').hide();
                             if (data.success){
                                 $('#addtenantModal').modal('toggle');
                                 $('#addtenantSuccessModal').modal('show');
                                 $(".chk_in_popup_btn").attr('id',data.mem_id);
                                 /*   BootstrapDialog.show({
                                    message: 'Tenant Saved Successfully',
                                    buttons: [{
                                        label: 'OK',
                                        action: function(dialog) {
                                              location.reload();
                                        } 
                                   }]
                                })*/ 
                             }else{
                                  //BootstrapDialog.alert(data.error_message);
                                  swal("Error!", data.error_message, "error");
                             }
                        },
                        error: function (data) {
                            alert("Server Error.");
                            return false;
                        }
                    });
                }
                else
                {
                    $.post(posturl, {id: value, vote: task});
                    $("#log-in").trigger("click");
                }
                return false;
            }
        });

        $("#change_password").validate({
            // Specify the validation rules
            rules: {
                password: {
                    required: true,
                },
                cpassword: {
                    required: true,
                    equalTo: "#password"
                }

            },
            // Specify the validation error messages
            messages: {
                password: {
                    required: "Please provide a password",
                },
                cpassword: {
                    required: "Please provide a password",
                    equalTo: "Please enter the same password as above",
                },
            },
            submitHandler: function (form) {
              
            }
        });


        jQuery.validator.addMethod("valueNotEquals", function (value, element, arg) {
            return arg != value;
        }, "Value must not equal arg.");
        jQuery.validator.addMethod("customvalidation", function (value, element) {
            return /^[A-Za-z_ -]+$/.test(value);
        }, "Alpha Characters Only.");

        jQuery.validator.addMethod('phoneUS', function (phone_number, element) {
            phone_number = phone_number.replace(/\s+/g, '');
            return this.optional(element) || phone_number.length > 9 &&
                    phone_number.match(/^(1-?)?(\([2-9]\d{2}\)|[2-9]\d{2})-?[2-9]\d{2}-?\d{4}$/);
        }, 'Enter a valid phone.');

        jQuery('.switch_options').each(function () {

            //This object
            var obj = jQuery(this);

            var enb = obj.children('.switch_enable'); //cache first element, this is equal to ON
            var dsb = obj.children('.switch_disable'); //cache first element, this is equal to OFF
            var input = obj.children('input'); //cache the element where we must set the value
            var input_val = obj.children('input').val(); //cache the element where we must set the value

            /* Check selected */
            if (0 == input_val) {
                dsb.addClass('selected');
            }
            else if (1 == input_val) {
                enb.addClass('selected');
            }

            //Action on user's click(ON)
            enb.on('click', function (e) {
                var b = $(".switch_val").val();
                if (0 == b) {
                    swal({
                        title: "Confirm?",
                        text: "Are you sure to put application under Maintenance Mode!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes!",
                        closeOnConfirm: false
                    },
                    function () {
                        $(dsb).removeClass('selected'); //remove "selected" from other elements in this object class(OFF)
                        $(e.target).addClass('selected'); //add "selected" to the element which was just clicked in this object class(ON) 
                        $(input).val(1).change(); //Finally change the value to 1
                        $.ajax({
                            url: '<?php echo base_url(); ?>administrator/change_app_mode',
                            type: 'POST',
                            data: {status: '1'},
                            beforeSend: function () {
                                $(".loading").show();
                            },
                            success: function (data) {
                                $(".loading").hide();
                                swal({
                                    title: "Success",
                                    text: 'Maintenance Mode On successfully',
                                    type: "success"
                                },
                                function () {
                                    //location.reload();
                                });

                            },
                            error: function () {
                                swal("Oops!", "error in form submission", "warning")
                                $(".loading").hide();
                            }
                        });
                    });
                }
            });

            //Action on user's click(OFF)
            dsb.on('click', function (e) {
                var a = $(".switch_val").val();
                if (1 == a) {
                    swal({
                        title: "Confirm?",
                        text: "Are you sure to put maintenance mode off!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes!",
                        closeOnConfirm: false
                    },
                    function () {
                        $(enb).removeClass('selected'); //remove "selected" from other elements in this object class(ON)
                        $(e.target).addClass('selected'); //add "selected" to the element which was just clicked in this object class(OFF) 
                        $(input).val(0).change(); // //Finally change the value to 0
                        $.ajax({
                            url: '<?php echo base_url(); ?>administrator/change_app_mode',
                            type: 'POST',
                            data: {status: '0'},
                            beforeSend: function () {
                                $(".loading").show();
                            },
                            success: function (data) {
                                $(".loading").hide();
                                swal({
                                    title: "Success",
                                    text: 'Maintenance Mode Off successfully',
                                    type: "success"
                                },
                                function () {
                                    //location.reload();
                                });

                            },
                            error: function () {
                                swal("Oops!", "error in form submission", "warning")
                                $(".loading").hide();
                            }
                        });
                    });
                }
            });

        });

        $(".cncl").click(function (e) {
            e.preventDefault();
            window.location.href = "<?php echo base_url('administrator/all_users'); ?>";
        });


        $("#add_main_cat").validate({
            // Specify the validation rules
            rules: {
                category_name: {
                    customvalidation: true,
                    maxlength: 20,
                    required: true,
                }
            },
            // Specify the validation error messages
            messages: {
                category_name: {
                    customvalidation: 'Only alphabets allowed'
                }
            },
            submitHandler: function (form) {
                $(".alert").hide();
                $("#add_main_cat").submit();
            }
        });

        $("#add_sub_cat").validate({
            // Specify the validation rules
            rules: {
                sub_category_name: {
                    customvalidation: true,
                    maxlength: 20,
                    required: true,
                },
                sel_main_category: {
                    valueNotEquals: "0",
                },
            },
            // Specify the validation error messages
            messages: {
                sub_category_name: {
                    customvalidation: 'Only alphabets allowed',
                    required: 'Please enter name'
                },
                sel_main_category: {
                    valueNotEquals: 'Please select main category',
                },
            },
            submitHandler: function (form) {
                $(".alert").hide();
                $("#add_sub_cat").submit();
            }
        });

        jQuery.validator.addMethod("valueNotEquals", function (value, element, arg) {
            return arg != value;
        }, "Value must not equal arg.");

    });

</script>
