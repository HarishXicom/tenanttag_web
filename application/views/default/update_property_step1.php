
<script>
    $(document).ready(function () {
        var yard_status = '<?= $propertyDetail->yard ?>';
        var community_status = '<?= $propertyDetail->community_status ?>';

        if (yard_status == 'yes')
        {
            $('.is_yard_service_provide').show('slow');
        }

        if (community_status == 'Yes')
            $('.community_box').show('slow');
    });
</script>

<div class="level">
    <div class="container">
        <div class="level-indicator">
            <img src="<?= $this->config->item('templateassets') ?>images/level-1.png" alt="" />
            <ul>
                <li  class="blue-col1"> Property Info</li>
                <li>Tenants</li>
                <li >tServices</li>
            </ul>
        </div>
    </div>
</div>

<section class="signup-section2">

    <div class="container">

        </br></br></br></br></br>
        <?php echo form_open('', array('class' => '', 'id' => 'property-add-step1', 'enctype' => 'multipart/form-data')) ?>
        <div class="ajax_report alert display-hide" role="alert" style=" margin-top: 100px; margin-bottom:10px;width:1169px; "><span class="close-message"></span><div class="ajax_message">Hello Message</div></div>
        <div >
            <div class="my-panel mr-btm0" >
                <h3 class="panel-head">
                    <div class="panel-title">
                        <h1>Property</h1>
                    </div>
                </h3>
                <div class="panel-body propertfirst">
                    <div class="form-group1" style=" margin-bottom: 19px;">
                        <h2 class="propphotos my-txt">Property Photos</h2>
                        <!--  <div class="my-col right-pull mr-rite0">
                                <input type="file[]" class="browse-btn property_img" name="profile_pic"> 
                                <button class="upload-btn my-btn">Upload Photo</button>
                            </div> -->

                        <div class="my-thumbs-list">
                                <ul id="formdiv">
                                <?php if (!empty($property_photo)) { ?>
                                <?php //print_r($property_photo); exit; ?>
                                    <?php foreach ($property_photo as $img) { ?>                                       

                                        <li id="filediv">
                                            <div id="abcd_<?php echo $img->id ?>" class="abcd">
                                                <img id="previewimg_<?php echo $img->id ?>" src="<?php echo $img->image ?>">
                                                <a class="clr_up_img del_photo" id="<?php echo $img->id ?>" href="javascript:void(0);" ><img alt="" style="width:25px; height:25px" src="https://tenanttag.com/assets/default/images/cross.png"></a> 
                                            </div>
                                            <input id="profile_pic" type="file" name="profile_pic[]" style="display: none;">                                          
                                        </li>
                               
                                <?php } ?>
                                <?php } ?>
                                    <li id="filediv">
                                       <!--  <input type="file" class="browse-btn 1property_img" id="profile_pic" name="profile_pic[]"> 
                                        <button class="upload-btn my-btn">Upload Photo</button> -->
                                        <div class="upload-file-cu upload-file" style="width:100px;"> 
                                             <div class="browse" style="width:100px; height:100px">
                                                <input type="file" id="profile_pic" class="profile_pic" accept="image/*" name="profile_pic[]">
                                            </div>
                                        </div>
                                    </li>

                                </ul>
                               <!--  <input type="button" id="add_more" class="upload" value="Add More"/> -->
                                    
                                    

                        </div>
                    </div>
                    <div  class="form-group1">
                        <div class="my-div">
                            <select class="form-control my-txt-fleid" name="property_type">
                                <option value="">Select Property Type</option>
                                <option value="SFR" <?php if ($propertyDetail->property_type == 'SFR') { ?>selected<?php } ?>>Single Family Home</option>
                                <option value="Condo" <?php if ($propertyDetail->property_type == 'Condo') { ?>selected<?php } ?>>Condominium</option>
                                <option value="Apt" <?php if ($propertyDetail->property_type == 'Apt') { ?>selected<?php } ?>>Apartment Building</option>
                            </select>
                        </div>
                        <div class="my-div"><input type="text" class="form-control my-txt-fleid" placeholder="Address 1" name="address1" value="<?= $propertyDetail->address1 ?>"/></div>
                        <div class="my-div"><input type="text" class="form-control my-txt-fleid" placeholder="Address 2" name="address2" value="<?= $propertyDetail->address2 ?>"/></div>
                        <input type="hidden" name="country" value="223"/>
                    </div>
                    <div class="form-group1">
                        <div class="my-div">
                            <div class="my-col">
                                <input type="text" class="form-control my-txt-fleid " placeholder="Unit" name="unit" value="<?= $propertyDetail->unit_number !='' ? $propertyDetail->unit_number : '' ?>"/>

                            </div>
                            <div class="my-col right-pull mr-rite0">
                                <input type="text" class="form-control my-txt-fleid width50 right-pull mr-rite0 " placeholder="City" name="city" value="<?= $propertyDetail->city ?>"/>
                            </div>
                        </div>
                        <div class="my-div">

                            <div class="">
                                <?php
                                $data = array();
                                $states = get_states(223);
                                if (!empty($states)) {
                                    foreach ($states as $state) {
                                        $data[$state->region_id] = $state->region_name;
                                    }
                                }
                                $list = "id='state'  class='form-control my-txt-fleid ' required='1'";
                                echo form_dropdown('state', $data, $propertyDetail->state_id, $list);
                                ?> 
                            </div>
                        </div>
<!-- 
                        <div class="my-div">
                            <div class="my-col">
                                <input type="text" class="form-control my-txt-fleid   " placeholder="Zip" name="zip" value="<?= $propertyDetail->zip ?>"/>
                            </div>
                            <div class="my-col right-pull mr-rite0">
                                <input type="file" class="browse-btn property_img" name="profile_pic"> 
                                <button class="upload-btn my-btn">Upload Photo</button>
                            </div>

                        </div> 
                        <p class="btn_view_del">
                            <span class="show_file_name show_prp_img" style="width:100%; <?php if (!empty($property_photo)) { echo 'display:block'; }?>">
                                <?php if (!empty($property_photo)) { ?>
                                        <?php $name = explode('/',$property_photo->image); echo end($name); ?>
                                    <a class="click_photo" target="_blank" href="<?php echo base_url() . $property_photo->image ?>" data-lightbox="roadtrip">
                                    View
                                    </a>
                                <a class="btn btn-edit btn-primary clr_up_img k" href="javascript:void(0);" >Delete</a> 
                                <?php } ?>
                            </span>
                        </p> -->
                    </div>
                    <div class="my-div custom-width">
                        <label><b>Property in Community</b></label>
                        <p class="my-radio">  <input type="radio" id="radio10" name="is_community" class="is_community" value="Yes" <?php if ($propertyDetail->community_status == 'Yes') { ?>checked<?php } ?>/>  <label for="radio10">Yes</label></p>
                        <p class="my-radio"> <input type="radio" id="radio11" name="is_community" class="is_community" value="No" <?php if ($propertyDetail->community_status == 'No') { ?>checked<?php } ?>/> <label for="radio11">No</label></p>
                    </div>
                    <div class="community_box" <?php
                    if ($propertyDetail->community_status == 'No') {
                        echo 'style="display:none"';
                    }
                    ?>>
                        <div class="form-group1">
                            <div class="my-div"><input type="text" class="form-control my-txt-fleid" placeholder="Community" name="community" value="<?= $propertyDetail->community ?>"></div>
                            <div class="my-div">
                                <div class="my-col width50">
                                    <input type="text" class="form-control my-txt-fleid width50 " placeholder="Gate Code" name="gate_code" value="<?= $propertyDetail->gate_code != '' ? $propertyDetail->gate_code : '' ?>"/>
                                </div>
                                <div class="my-col width50 right-pull mr-rite0">
                                    <input type="text" class="form-control my-txt-fleid width50 right-pull mr-rite0" placeholder="Unit PO Box #" name="po_box" value="<?= $propertyDetail->unit_po_box != '' ? $propertyDetail->unit_po_box : '' ?>"/>
                                </div>
                            </div>
                        </div>
                        <div class="form-group1">
                            <div class="my-div"><input type="text" class="form-control my-txt-fleid" placeholder="Community Mng Company" name="community_company" value="<?= $propertyDetail->community_managing_company ?>"/></div>
                            <div class="my-div"><input type="text" class="form-control my-txt-fleid phone_us" placeholder="Contact Number" name="company_number" value="<?= $propertyDetail->community_contact_number != '' ? $propertyDetail->community_contact_number : '' ?>"/></div>
                            <div class="my-div">

                                <span class="upload-btn" style="width:255px;">Upload Community Docs
                                    <input type="file" class="comm_docs" name="comm_docs">
                                </span>
                            </div>
                            <p class="btn_view_del">
                                <span class="show_file_name2" style="width:100%; <?php if($propertyDetail->community_doc!=''){ echo 'display:inline'; }?>">
                                    <?php if($propertyDetail->community_doc !=''){ ?>
                                        <?php echo $propertyDetail->community_doc; ?>
                                        <a target="_blank" href="<?php echo base_url() ?>assets/uploads/community_documents/<?php echo $propertyDetail->community_doc ?>">View
                                        </a>
                                        <a class='btn btn-edit btn-primary clr_up_img delu_comm_doc' id="<?php echo $propertyDetail->prop_id ?>" href='javascript:;'> Delete</a>
                                    <?php } ?>
                                </span>
                            </p>

                        </div>
                    </div>	 
                </div>

            </div>
            <div class="my-panel" >
                <h3 class="panel-head">
                    <div class="panel-title">
                        <h1>Amenities</h1>
                    </div>
                </h3>

                <div class="panel-body">
                    <div  class="form-group1">
                        <div class="my-div"><input type="text" class="form-control my-txt-fleid" placeholder="Garbage Day" readonly/></div>
                        <div class="my-div">
                            <select class="form-control my-txt-fleid width50" name="garbage_pick_day">
                                <option value="">Select Day</option>
                                <option value="Sunday" <?php if ($propertyDetail->garbage_day == 'Sunday') { ?>selected<?php } ?>>Sunday</option>
                                <option value="Monday" <?php if ($propertyDetail->garbage_day == 'Monday') { ?>selected<?php } ?>>Monday</option>
                                <option value="Tuesday" <?php if ($propertyDetail->garbage_day == 'Tuesday') { ?>selected<?php } ?>>Tuesday</option>
                                <option value="Wednesday" <?php if ($propertyDetail->garbage_day == 'Wednesday') { ?>selected<?php } ?>>Wednesday</option>
                                <option value="Thursday" <?php if ($propertyDetail->garbage_day == 'Thursday') { ?>selected<?php } ?>>Thursday</option>
                                <option value="Friday" <?php if ($propertyDetail->garbage_day == 'Friday') { ?>selected<?php } ?>>Friday</option>
                                <option value="Saturday" <?php if ($propertyDetail->garbage_day == 'Saturday') { ?>selected<?php } ?>>Saturday</option>
                                <option value="N/A" <?php if ($propertyDetail->garbage_day == 'N/A') { ?>selected<?php } ?>>N/A</option>
                            </select>
                        </div>
                    </div>
                    <div  class="form-group1">
                        <div class="my-div"><input type="text" class="form-control my-txt-fleid" placeholder="Recycle Day" readonly/></div>
                        <div class="my-div">
                            <select class="form-control my-txt-fleid width50" name="recycle_pick_day">
                                <option value="">Select Day</option>
                                <option value="Sunday" <?php if ($propertyDetail->recycle_day == 'Sunday') { ?>selected<?php } ?>>Sunday</option>
                                <option value="Monday" <?php if ($propertyDetail->recycle_day == 'Monday') { ?>selected<?php } ?>>Monday</option>
                                <option value="Tuesday" <?php if ($propertyDetail->recycle_day == 'Tuesday') { ?>selected<?php } ?>>Tuesday</option>
                                <option value="Wednesday" <?php if ($propertyDetail->recycle_day == 'Wednesday') { ?>selected<?php } ?>>Wednesday</option>
                                <option value="Thursday" <?php if ($propertyDetail->recycle_day == 'Thursday') { ?>selected<?php } ?>>Thursday</option>
                                <option value="Friday" <?php if ($propertyDetail->recycle_day == 'Friday') { ?>selected<?php } ?>>Friday</option>
                                <option value="Saturday" <?php if ($propertyDetail->recycle_day == 'Saturday') { ?>selected<?php } ?>>Saturday</option>
                                <option value="N/A" <?php if ($propertyDetail->recycle_day == 'N/A') { ?>selected<?php } ?>>N/A</option>
                            </select>
                        </div>
                    </div>
                    <div  class="form-group1">
                        <div class="my-div"><input type="text" class="form-control my-txt-fleid" placeholder="Yard Waste Day" readonly/></div>
                        <div class="my-div">
                            <select class="form-control my-txt-fleid width50" name="yard_waste_pick_day">
                                <option value="">Select Day</option>
                                <option value="Sunday" <?php if ($propertyDetail->yard_waste_day == 'Sunday') { ?>selected<?php } ?>>Sunday</option>
                                <option value="Monday" <?php if ($propertyDetail->yard_waste_day == 'Monday') { ?>selected<?php } ?>>Monday</option>
                                <option value="Tuesday" <?php if ($propertyDetail->yard_waste_day == 'Tuesday') { ?>selected<?php } ?>>Tuesday</option>
                                <option value="Wednesday" <?php if ($propertyDetail->yard_waste_day == 'Wednesday') { ?>selected<?php } ?>>Wednesday</option>
                                <option value="Thursday" <?php if ($propertyDetail->yard_waste_day == 'Thursday') { ?>selected<?php } ?>>Thursday</option>
                                <option value="Friday" <?php if ($propertyDetail->yard_waste_day == 'Friday') { ?>selected<?php } ?>>Friday</option>
                                <option value="Saturday" <?php if ($propertyDetail->yard_waste_day == 'Saturday') { ?>selected<?php } ?>>Saturday</option>
                                <option value="N/A" <?php if ($propertyDetail->yard_waste_day == 'N/A') { ?>selected<?php } ?>>N/A</option>
                            </select>
                        </div>
                    </div>
                    <div  class="form-group1 mr-top20">
                        <div class="my-div"><input type="text" class="form-control my-txt-fleid" placeholder="Smoke Detectors" readonly/></div>
                        <div class="my-div">
                            <select class="form-control my-txt-fleid width50" name="safety_equipment1">
                                <option value=""> Smoke Detectors:</option>
                                <option value="1" <?php if ($propertyDetail->safety_equipment1 == '1') { ?>selected<?php } ?>>1</option>
                                <option value="2" <?php if ($propertyDetail->safety_equipment1 == '2') { ?>selected<?php } ?>>2</option>
                                <option value="3" <?php if ($propertyDetail->safety_equipment1 == '3') { ?>selected<?php } ?>>3</option>
                                <option value="4" <?php if ($propertyDetail->safety_equipment1 == '4') { ?>selected<?php } ?>>4</option>
                                <option value="5" <?php if ($propertyDetail->safety_equipment1 == '5') { ?>selected<?php } ?>>5</option>
                                <option value="6" <?php if ($propertyDetail->safety_equipment1 == '6') { ?>selected<?php } ?>>6</option>
                                <option value="N/A" <?php if ($propertyDetail->safety_equipment1 == 'N/A') { ?>selected<?php } ?>>N/A</option>
                            </select>
                        </div>
                    </div>
                    <div  class="form-group1">
                        <div class="my-div"><input type="text" class="form-control my-txt-fleid" placeholder="Fire Extinguishers" readonly/></div>
                        <div class="my-div">
                            <select class="form-control my-txt-fleid width50" name="safety_equipment2">
                                <option value=""> Fire Extinguishers:</option>
                                <option value="1" <?php if ($propertyDetail->safety_equipment2 == '1') { ?>selected<?php } ?>>1</option>
                                <option value="2" <?php if ($propertyDetail->safety_equipment2 == '2') { ?>selected<?php } ?>>2</option>
                                <option value="3" <?php if ($propertyDetail->safety_equipment2 == '3') { ?>selected<?php } ?>>3</option>
                                <option value="4" <?php if ($propertyDetail->safety_equipment2 == '4') { ?>selected<?php } ?>>4</option>
                                <option value="N/A" <?php if ($propertyDetail->safety_equipment2 == 'N/A') { ?>selected<?php } ?>>N/A</option>
                            </select>
                        </div>
                    </div>
                    <div  class="form-group1">
                        <div class="my-div"><input type="text" class="form-control my-txt-fleid" placeholder="Heat Electric or Heat Furnace" readonly/></div>
                        <div class="my-div"> <?php //echo $propertyDetail->heat_type;   ?>
                            <select class="form-control my-txt-fleid width50" name="heat_type">
                                <option value="N/A" <?php if ($propertyDetail->heat_type == 'N/A') { ?>selected<?php } ?>>N/A</option>
                                <option value="Central" <?php if ($propertyDetail->heat_type == 'Central') { ?>selected<?php } ?>>Central</option>
                                <option value="Furnace" <?php if ($propertyDetail->heat_type == 'Furnace') { ?>selected<?php } ?>>Furnace</option>
                                <option value=" Room" <?php if ($propertyDetail->heat_type == 'Room') { ?>selected<?php } ?>>Room</option>
                            </select>
                        </div>
                        <div class="my-div">
                            <?php $chk = 0; ?>
                            <select style="float:left;<?php
                            if ($propertyDetail->heat_type == 'N/A') {
                                echo 'display:none;';
                            }
                            ?>" class="form-control my-txt-fleid width50" name="heat_filter_size" <?php ?>>       
                                <option <?php if ($propertyDetail->heat_filter_size == 'N/A') {
                                        $chk = 1;
                                ?>selected<?php } ?>>N/A</option>
                                <option <?php if ($propertyDetail->heat_filter_size == '20x20x1') {
                                    $chk = 1;
                                    ?>selected<?php } ?>>20x20x1</option>
                                <option <?php if ($propertyDetail->heat_filter_size == '20x25x1') {
                                        $chk = 1;
                                ?>selected<?php } ?>>20x25x1</option>
                                <option <?php if ($propertyDetail->heat_filter_size == '16x20x1') {
                                    $chk = 1;
                                    ?>selected<?php } ?>>16x20x1</option>
                                <option <?php if ($propertyDetail->heat_filter_size == '16x25x1') {
                                        $chk = 1;
                                ?>selected<?php } ?>>16x25x1</option>
                                <option <?php if ($propertyDetail->heat_filter_size == '14x25x1') {
                                    $chk = 1;
                                    ?>selected<?php } ?>>14x25x1</option>
                                <option <?php if ($propertyDetail->heat_filter_size == '12x12x1') {
                                        $chk = 1;
                                        ?>selected<?php } ?>>12x12x1</option>
                                <option <?php if ($propertyDetail->heat_filter_size == '12x16x1') {
                                        $chk = 1;
                                ?>selected<?php } ?>>12x16x1</option>
                                <option <?php if ($propertyDetail->heat_filter_size == '12x18x1') {
                                       $chk = 1;
                                       ?>selected<?php } ?>>12x18x1</option>
                                <option <?php if ($chk == 0) { ?>selected<?php } ?>>Other</option>
                            </select>
                            <input type="text" name="heat_filter_size_other" class="form-control my-txt-fleid filtersize" value="<?php
                                   if ($chk == 0) {
                                       echo $propertyDetail->heat_filter_size;
                                   }
                            ?>" required="true" style="<?php
                                   if ($chk == 1) {
                                       echo 'display:none;';
                                   }
                            ?>width: 90px;float: left"/>
                        </div>
                    </div>
                    <div  class="form-group1">
                        <div class="my-div"><input type="text" class="form-control my-txt-fleid" placeholder="AC Central or AC Room" readonly/></div>
                        <div class="my-div">
                            <select class="form-control my-txt-fleid width50" name="ac_type">
                                <option value="N/A" <?php if ($propertyDetail->ac_type == 'N/A') { ?>selected<?php } ?>>N/A</option>
                                <option value="Central" <?php if ($propertyDetail->ac_type == 'Central') { ?>selected<?php } ?>>Central</option>
                                <option value="Furnace" <?php if ($propertyDetail->ac_type == 'Furnace') { ?>selected<?php } ?>>Furnace</option>
                                <option value=" Room" <?php if ($propertyDetail->ac_type == 'Room') { ?>selected<?php } ?>>Room</option>
                            </select>
                        </div>
                        <div class="my-div"><?php $chk2 = 0; ?>
                            <select style="float:left;<?php
                                    if ($propertyDetail->ac_type == 'N/A') {
                                        echo 'display:none';
                                    }
                            ?>" class="form-control my-txt-fleid width50" name="ac_filter_size"  <?php ?>>

                                <option <?php if ($propertyDetail->ac_filter_size == 'N/A') {
                                        $chk2 = 1;
                                        ?>selected<?php } ?>>N/A</option>
                                <option <?php if ($propertyDetail->ac_filter_size == '20x20x1') {
                                        $chk2 = 1;
                                        ?>selected<?php } ?>>20x20x1</option>
                                <option <?php if ($propertyDetail->ac_filter_size == '20x25x1') {
                                        $chk2 = 1;
                                ?>selected<?php } ?>>20x25x1</option>
                                <option <?php if ($propertyDetail->ac_filter_size == '16x20x1') {
                                       $chk2 = 1;
                                ?>selected<?php } ?>>16x20x1</option>
                                <option <?php if ($propertyDetail->ac_filter_size == '16x25x1') {
                                   $chk2 = 1;
                                ?>selected<?php } ?>>16x25x1</option>
                                <option <?php if ($propertyDetail->ac_filter_size == '14x25x1') {
                            $chk2 = 1;
                                ?>selected<?php } ?>>14x25x1</option>
                                <option <?php if ($propertyDetail->ac_filter_size == '12x12x1') {
                        $chk2 = 1;
                        ?>selected<?php } ?>>12x12x1</option>
                                <option <?php if ($propertyDetail->ac_filter_size == '12x16x1') {
                        $chk2 = 1;
                        ?>selected<?php } ?>>12x16x1</option>
                                <option <?php if ($propertyDetail->ac_filter_size == '12x18x1') { ?>selected<?php } ?>>12x18x1</option>
                                <option <?php if ($chk2 == 0) { ?>selected<?php } ?>>Other</option>
                            </select>
                            <input type="text" name="ac_filter_size_other" class="form-control my-txt-fleid filtersize" value="<?php
                        if ($chk2 == 0) {
                            echo $propertyDetail->ac_filter_size;
                        }
                            ?>" required="true" style="<?php
                            if ($chk == 1) {
                                echo 'display:none;';
                            }
                            ?>width: 90px;float: left"/>

                        </div>
                        <?php
                        foreach ($allAmenties as $value) {
                            $amenties = explode(',', $propertyDetail->amenties);
                            ?>
                            <div  class="form-group1">
                                <div class="my-div"><input type="text" class="form-control my-txt-fleid" placeholder="<?= $value->name ?>" readonly/></div>
                                <div class="my-div">
                                    <p class="my-radio">  <input type="radio" id="radio01_<?= $value->id ?>" name="<?= $value->name ?>" value="<?= $value->id ?>" <?php if (in_array($value->id, $amenties)) { ?>checked<?php } ?>/>  <label for="radio01_<?= $value->id ?>">Yes</label></p>
                                    <p class="my-radio"> <input type="radio" id="radio02_<?= $value->id ?>" name="<?= $value->name ?>" value="" <?php if (!in_array($value->id, $amenties)) { ?>checked<?php } ?>/> <label for="radio02_<?= $value->id ?>">No</label></p>
                                </div>
                            </div>
                        <?php } ?>
                        <div  class="form-group1">
                            <div class="my-div"><input type="text" class="form-control my-txt-fleid" placeholder="Pest control home" readonly/></div>
                            <div class="my-div" style="width: 13%;">
                                <p class="my-radio">  <input type="radio" id="radio01_pest_control_home" name="pest_control_home" value="yes" <?php if ($propertyDetail->pest_control_home == 'yes') { ?>checked<?php } ?>/>  <label for="radio01_pest_control_home">Yes</label></p>
                                <p class="my-radio"> <input type="radio" id="radio02_pest_control_home" name="pest_control_home" value="no" <?php if ($propertyDetail->pest_control_home == 'no') { ?>checked<?php } ?>/> <label for="radio02_pest_control_home">No</label></p>
                            </div>
                            <div class="my-div service-provider"  <?php  if ($propertyDetail->pest_control_home == 'no') {    echo 'style="display:none;"'; }     ?>>
                                <div class="my-col width50">
                                    <input type="text" class="form-control my-txt-fleid width50" placeholder="Company" name="pest_service_provider" value="<?= $propertyDetail->pest_service_provider ?>"/>
                                </div>
                                <div class="my-col width50 right-pull mr-rite0">
                                    <input type="text" class="form-control my-txt-fleid width50 right-pull mr-rite0 phone_us" placeholder="Contact# " name="pest_service_provider_number" value="<?= $propertyDetail->pest_service_provider_number != '' ? $propertyDetail->pest_service_provider_number : '' ?>"/>
                                </div>
                            </div>

                        </div>
                        <div  class="form-group1">
                            <div class="my-div"><input type="text" class="form-control my-txt-fleid" placeholder="Yard" readonly/></div>
                            <div class="my-div" style="width:147px;">
                                <p class="my-radio">  <input type="radio" id="radio01_yard" name="yard" value="yes" onClick="show_service_provided(this.value)" <?php if ($propertyDetail->yard == 'yes') { ?>checked<?php } ?>/>  <label for="radio01_yard">Yes</label></p>
                                <p class="my-radio"> <input type="radio" id="radio02_yard" name="yard" value="no" onClick="show_service_provided(this.value)" <?php if ($propertyDetail->yard == 'no') { ?>checked<?php } ?>/> <label for="radio02_yard">No</label></p>
                            </div>
                            <div style="float: left; padding: 10px; <?php if ($propertyDetail->yard == 'no') {  echo 'display:none;';        } ?>" class="is_yard_service_provide">  <span>Is service included</span></div>



                            <div class="my-div is_yard_service_provide" style="width:147px; <?php   if ($propertyDetail->yard == 'no') { echo 'display:none;'; } ?> ">
                                <p class="my-radio">  <input type="radio" id="radio01_service_provided" name="yard_service_provided" value="yes" <?php if ($propertyDetail->yard_service_provided == 'yes') { ?>checked<?php } ?>/>  <label for="radio01_service_provided">Yes</label></p>
                                <p class="my-radio"> <input type="radio" id="radio02_service_provided" name="yard_service_provided" value="no" <?php if ($propertyDetail->yard_service_provided == 'no') { ?>checked<?php } ?>/> <label for="radio02_service_provided">No</label></p>
                            </div>

                            <div class="my-div is_yard_service_provide" style=" <?php
                                                if ($propertyDetail->yard == 'no') {
                                                    echo 'display:none;';
                                                }
                                                ?>">
                                <div class="my-col width50"><input type="text" class="form-control my-txt-fleid " placeholder="Company" name="yard_service_provider" value="<?= $propertyDetail->yard_service_provider ?>"/></div>
                                <div class="my-col width50 right-pull mr-rite0 div-50-50"><input type="text" class="form-control my-txt-fleid phone_us" placeholder="Contact# " name="yard_service_provider_number" value="<?= $propertyDetail->yard_service_provider_number != '' ? $propertyDetail->yard_service_provider_number : '' ?>"/></div>
                            </div>


                        </div>

                        <div class="my-panel   my-panel1 mr-btm0 morepanel" id="panel-4">
                            <div class="panel-head">
                                <div class="panel-title">
                                    <h1>More</h1>
                                </div>
                            </div>
                            <div class="panel-body">
                                <div class="already-services">
<?php foreach ($property_services as $key => $services) { ?>
                                        <div  class="form-group1 already-service-no-<?= $key ?>">
                                            <div class="my-div width150"><label>Select</label></div>
                                            <div class="my-div">
                                                <select  class="form-control my-txt-fleid" name="service[]" id="already-services-<?= $key ?>">
                                                    <option value="">Select Service</option>
    <?php foreach ($allServices as $value) { ?>
                                                        <option value="<?= $value->id ?>" <?php if ($services->service_id == $value->id) { ?>selected<?php } ?>><?= $value->service ?></option>
    <?php } ?>
                                                </select>
                                            </div>	
                                            <div class="my-div ">
                                                <input type="text" class="form-control my-txt-fleid width50" placeholder="Company" name="service_provider[]" value="<?= $services->company ?>"/>
                                                <input type="text" class="form-control my-txt-fleid width50 right-pull mr-rite0 phone_us" placeholder="Contact#" name="service_provider_number[]" value="<?= $services->contact ?>"/>
                                            </div>
                                            <div class="my-div width150">
                                                <a href="javascript:;" class="delete-service " id="delete-service<?= $key ?>" data-value="<?= $key ?>" style="margin-left:35px;"><img src="<?= $this->config->item('templateassets') ?>images/cross.png" alt=""/></a>
                                            </div>	

                                        </div>
                                        <input type="hidden" name="already_services" class="already_services" value="<?= $key ?>">
<?php } ?>
                                </div>
                                <div class="services-panel"></div>
                                <div  class="form-group1 new-services-0">
                                    <div class="my-div width150"><label>Create</label></div>
                                    <div class="my-div">
                                        <input type="text" class="form-control my-txt-fleid" placeholder=" Service" name="new_service_0"/>
                                    </div>
                                    <div class="my-div ">
                                        <input type="text" class="form-control my-txt-fleid width50" placeholder="Company" name="new_service_provider_0"/>
                                        <input type="text" class="form-control my-txt-fleid width50 right-pull mr-rite0 phone_us" placeholder="Contact# " name="new_service_provider_number_0"/>
                                    </div>
                                    <div class="my-div width150">
                                        <a href="javascript:;" class="delete-row " id="cross0" data-value="0" style="margin-left:35px;"><img src="<?= $this->config->item('templateassets') ?>images/cross.png" alt=""/></a>
                                    </div>

                                </div>
                                <div class="new-btn-part">
                                    <a href="javascript:;" class="add-btn add-more-service btn-block" style="position:unset;width:235px;"><i class="fa fa-plus"></i>Select More Services</a>
                                    <a href="javascript:;" class="add-btn add-new-service btn-block" style="position:unset;width:235px;"><i class="fa fa-plus"></i> Create New Service </a>
                                </div>
                                <input type="hidden" value="0" name="new_created_services" class="new_created_services">
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
        <div class="center">  

            <input type="submit" class="next" value="NEXt" style=""/>
        </div>
<?= form_close(); ?> 	
    </div>

</section>

<div class="end-block"></div>
<script>

    $('.add-more-service').on("click", function () {
        var assets = '<?= $this->config->item('templateassets') ?>';
        var already_services = $('.already_services').val();
        already_services = parseInt(already_services) + 1;
        $('.already_services').val(already_services);
        var allService = '<?= json_encode($allServices) ?>';
        allService = JSON.parse(allService);
        var dataToAppend = '';

        dataToAppend += '<div  class="form-group1 already-service-no-' + already_services + '"><div class="my-div width150"><label>Select</label></div>';
        dataToAppend += '<div class="my-div">';
        dataToAppend += '<select id="already-services-' + already_services + '" class="form-control my-txt-fleid" name="service[]">';
        dataToAppend += '<option value="">Select Service</option>';
        $.each(allService, function (key, value) {
            dataToAppend += '<option value="' + value.id + '">' + value.service + '</option>';
        });
        dataToAppend += '</select></div>	';
        dataToAppend += '<div class="my-div ">';
        dataToAppend += '<input type="text" class="form-control my-txt-fleid width50" placeholder="Company" name="service_provider[]"/>';
        dataToAppend += '<input type="text" class="form-control my-txt-fleid width50 right-pull mr-rite0 phone_us" placeholder="Contact#" name="service_provider_number[]"/>';
        dataToAppend += '</div>';
        dataToAppend += '<div class="my-div width150 "><a href="javascript:;" class="delete-service " id="delete-service' + already_services + '" data-value="' + already_services + '" style="margin-left:35px;"><img src="' + assets + 'images/cross.png" alt=""/></a></div></div>';

        $('.already-services').append(dataToAppend);
    });


</script>	
<!--
  <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
-->
<style>
    .upload-btn > input {
        bottom: 0;
        cursor: pointer;
        font-size: 0;
        left: 0;
        opacity: 0;
        position: absolute;
        right: 0;
        top: 0;
        width: 100%;
    }
    .panel-body.ui-accordion-content {height:auto !important;}
#formdiv{
    width:100%; 
    float:left; 
    text-align: center;
}
#formdiv li{
    float: left;
    height: 100px;
}
#error{
    color:red;
    text-align: left;
}
#filediv{
    margin: 10px;
}
#img{ 
    width: 17px;
    border: none; 
    height:17px;
    margin-left: -20px;
    margin-bottom: 91px;
}

.abcd{
    text-align: center;
}

.abcd img{
    height:100px;
    width:100px;
    padding: 5px;
    border: 1px solid rgb(232, 222, 189);
}
.upload{
    background-color:#00659f none repeat scroll 0 0;
    border:1px solid #00659f;
    color:#fff;
    border-radius:5px;
    padding:10px;
    float: right;
}

li#filediv .clr_up_img {
  background: #fff none repeat scroll 0 0;
  display: block;
  height: 25px;
  position: absolute;
  right: 1px;
  top: 0;
  width: 25px;
}
 #formdiv li {
  float: left;
  height: 100px;
  position: relative;
}
</style>
<script>
    var abc = 0; //Declaring and defining global increement variable

    $(document).ready(function() {

        $('#add_more').click(function() {
           /* $(this).parent().find('ul').before($("<li/>", {id: 'filediv'}).fadeIn('slow').append(
                    $("<input/>", {name: 'file[]', type: 'file', id: 'file'}),        
                    $("<br/><br/>")
                    ));*/
            $("#formdiv").append('<li id="filediv"><div class="upload-file-cu upload-file" style="width:100px;">   <div class="browse" style="width:100px; height:100px"> <input type="file" id="profile_pic" class="profile_pic" name="profile_pic[]"> </div>   </div></li>')
        });

                $('body').on('change', '#profile_pic', function(){
                if (this.files && this.files[0]) {
                     abc += 1; //increementing global variable by 1
                    
                    var z = abc - 1;
                    var x = $(this).parent().find('#previewimg' + z).remove();
                    $(this).before("<div id='abcd"+ abc +"' class='abcd'><img id='previewimg" + abc + "' src=''/></div>");
                   
                    var reader = new FileReader();
                    reader.onload = imageIsLoaded;
                    reader.readAsDataURL(this.files[0]);
                   $("#formdiv").append('<li id="filediv"><div class="upload-file-cu upload-file" style="width:100px;">   <div class="browse" style="width:100px; height:100px"> <input type="file" id="profile_pic" class="profile_pic" name="profile_pic[]"> </div>   </div></li>')
                    $(this).hide();

                    $(this).next().hide();
                    $("#abcd"+ abc).append($('<a class="clr_up_img del_up_img" href="javascript:void(0);" ><img alt="" style="width:25px; height:25px" src="https://tenanttag.com/assets/default/images/cross.png"></a> ').click(function() {
                        $(this).parent().parent().remove();
                    }));
                }
            });

    //To preview image     <a class="clr_up_img del_up_img" href="javascript:void(0);" ><img alt="" style="width:25px; height:25px" src="https://tenanttag.com/assets/default/images/cross.png"></a> 
        function imageIsLoaded(e) {
            $('#previewimg' + abc).attr('src', e.target.result);
        };

    });

</script>