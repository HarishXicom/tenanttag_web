<script>
    $(document).ready(function () {
        //~ var date = new Date();
        //~ date.setMonth(date.getYear() - 18);

        $(document).on('click', ".datepicker", function () {
            $(this).removeClass("hasDatepicker");
            $(this).datepicker({
                changeMonth: true,
                changeYear: true,
                yearRange: '1940:' + (new Date).getFullYear()
            });
            $(this).datepicker("show");
        });
        //$('#datepicker').datepicker('setDate', '12/31/2000');

        $(document).on('click', ".lease-start-date", function () {
            $(this).removeClass("hasDatepicker");
            $(this).datepicker({
               // minDate: 0,
                changeMonth: true,
                changeYear: true,
                onSelect: function (selected) {
                    $(".lease-end-date").datepicker("option", "minDate", selected)
                    //$(".due-date").datepicker("option","minDate", selected)
                }
            });
            $(this).datepicker("show");
        });

        $(document).on('click', ".lease-end-date", function () {
            $(this).removeClass("hasDatepicker");
            $(this).datepicker({
                minDate: 0,
                changeMonth: true,
                changeYear: true,
                onSelect: function (selected) {
                    $(".lease-start-date").datepicker("option", "maxDate", selected)
                    //$(".due-date").datepickwhosiner("option","maxDate", selected)
                }
            });
            $(this).datepicker("show");
        });

        $(document).on('click', ".due-date", function () {
            $(this).removeClass("hasDatepicker");
            $(this).datepicker();
            $(this).datepicker("show");
        });
        
         $(document).on('change', ".sel_lt_fee", function () {
             if($(this).val()!=''){
               $(this).parent().next().show();
           }else{
               $(this).parent().next().show();
           }
        });
        

    });
</script>
<div class="level">
    <div class="container">
        <div class="level-indicator">
            <img src="<?= $this->config->item('templateassets') ?>images/level.png" alt="" />
            <ul>
                <li> Property Info</li>
                <li class="blue-col1">Tenants</li>
                <li>tServices</li>
            </ul>
        </div>
    </div>
</div>
<section class="signup-section2 up2">
    <div class="container">
        <h2 class="tenant-txt">Add Tenant</h2>



        <?php echo form_open('', array('class' => '', 'id' => 'property-update-step2')) ?>
        <div class="ajax_report alert display-hide" role="alert" style=" float:left; width:100%; margin-top: 10px; margin-bottom:10px;width:1169px;"><span class="close-message"></span><div class="ajax_message">Hello Message</div></div>

        <div class="my-panel mr-btm0 drsecondpanel">
            <div class="panel-head">
                <div class="panel-title">
                    <h1>Tenant</h1>
                </div>
            </div>
            <div class="sub-panel-head">
                <p>    <input type="checkbox" id="test1" class="is_vacant" name="is_vacant" value="Yes"/>    
                <label for="test1">Vacant</label>  </p>

            </div>
            <div class="panel-body all-tenants">
                <div id="tenant_list">

                    <?php if (!empty($tenantDetails)) { //echo "<pre>"; print_r($tenantDetails); exit; 
                    ?>
                        <?php
                        $total_r = 1;
                        foreach ($tenantDetails as $key => $value) {
                            $tenantDetail = $value->tenantDetail;
                            $leaseDoc = $value->leaseDoc;?>	
                            <div class="tenant-area">
                                <?php
                                if($total_r != 1){
                                    echo '<div class="panel-head"><div class="panel-title"><h1>Tenant</h1></div></div>';
                                }
                                $total_r++;
                                ?>
                            <ul class="form-group" id="tenant-<?= $key + 1 ?>">
                                <li class="marginbottom"><p class="labelp">First Name</p><input type="text" class="form-control my-txt-fleid" required="true" customvalidation="true" maxlength="20" placeholder="First Name" id="first_name<?= $key + 1 ?>" name="first_name[<?= $key + 1 ?>]" value="<?= $tenantDetail->first_name ?>"/></li>
                                <li class="marginbottom"><p class="labelp">Middle Name</p><input type="text" class="form-control my-txt-fleid" placeholder="Middle Name" id="middle_name<?= $key + 1 ?>" name="middle_name[<?= $key + 1 ?>]" value="<?= $tenantDetail->middle_name ?>"/></li>
                                <li class="marginbottom"><p class="labelp">Last Name</p><input type="text" class="form-control my-txt-fleid" placeholder="" id="last_name<?= $key + 1 ?>" name="last_name[<?= $key + 1 ?>]" value="<?= $tenantDetail->last_name ?>"/></li>
                                <a href="javascript:;" class="delete-tenant-row" id="cross<?= $key + 1 ?>" data-value="<?= $key + 1 ?>" style=""><img src="<?= $this->config->item('templateassets') ?>images/cross.png" alt="Remove Selected"/></a>
                                <li>
                                    <div class="my-col width50">
                                        <p class="labelp">DOB</p>
                                        <input readonly type="text" class="form-control my-txt-fleid width50 tenantdob" placeholder="" id="dob<?= $key + 1 ?>" name="dob[<?= $key + 1 ?>]" value="<?= date('m/d/Y', $tenantDetail->dob) ?>"/> 
                                    </div>
                                    <div class="my-col width50 right-pull mr-rite0">
                                        <p class="labelp">Mobile</p>
                                        <input type="text" class="form-control my-txt-fleid phone_us " placeholder="" id="mobile<?= $key + 1 ?>" name="mobile[<?= $key + 1 ?>]" value="<?= $tenantDetail->mobile_no ?>"/> 
                                    </div>
                                </li>
                                <li class="marginbottom">
                                    <p class="labelp">Email</p>
                                    <input type="email" class="form-control my-txt-fleid" placeholder="" name="email[<?= $key + 1 ?>]" id="email<?= $key + 1 ?>" autocomplete="off" value="<?= $tenantDetail->email ?>"/>
                                </li>
                                <li class="drsecondlist">
                                    <div class="my-col width50">
                                        <p class="labelp">Language</p>
                                        <select class="form-control my-txt-fleid" id="lng<?= $key + 1 ?>" name="preferred_language[<?= $key + 1 ?>]">
                                            <option value="">Select Language</option>
                                            <option value="English" <?php if ($tenantDetail->preferred_language == 'English') { ?>selected<?php } ?>>English</option>
                                            <option value="Spanish" <?php if ($tenantDetail->preferred_language == 'Spanish') { ?>selected<?php } ?>>Spanish</option>                               
                                        </select> 
                                    </div>                               
                                    <div class="my-col width50 right-pull mr-rite0">
                                        <p class="labelp">Lease Start</p>
                                        <input type="text" readonly="true" class="form-control my-txt-fleid width50 leasefrom lease-start-date" id="lease_start_date<?= $key + 1 ?>" placeholder="" name="lease_start_date[<?= $key + 1 ?>]" value="<?= date('m/d/Y', strtotime($value->lease_start_date)) ?>"/>
                                    </div>                                 
                                </li>
                        
                                <li>
                                    <div class="my-col width50">
                                        <p class="labelp">Lease End</p>
                                        <input type="text" readonly="true"  class="form-control my-txt-fleid width50 right-pull mr-rite0 lease-end-date leaseto" id="lease_end_date<?= $key + 1 ?>"  placeholder="" name="lease_end_date[<?= $key + 1 ?>]" value="<?= date('m/d/Y', strtotime($value->lease_end_date)) ?>"/>
                                    </div>
                                    <div class="my-col width50 right-pull mr-rite0 currency_text_box">
                                        <p class="labelp">Rent Amount</p>
                                        <span class=""><i class="fa fa-usd" style="font-size:14px"></i></span>
                                        <input type="text" class="form-control my-txt-fleid width50" digits="true" id="rent_amount<?= $key + 1 ?>" placeholder="" name="rent_amount[<?= $key + 1 ?>]" value="<?= $value->rent_amount ?>"/>
                                    </div>
                                </li>
                                <li>
                                    <div class="my-col width50">  
                                        <p class="labelp">Rent Due<?php //echo $value->due_date; ?></p>                                  
                                        <select class="form-control my-txt-fleid width50 right-pull mr-rite0 due_date_start_class" id="due_date<?= $key + 1 ?>" name="due_date[<?= $key + 1 ?>]">
                                            <option value="">Rent Due</option>
                                             <?php for ($m = 1; $m < 31; $m++) { ?>
                                                <option value="<?php echo $m; ?>" <?php if ($value->due_date == $m) { ?>selected<?php } ?>><?php echo ordinal($m); ?></option>
                                            <?php } ?>                                           
                                        </select>
                                    </div>
                                    <div class="my-col width50 right-pull mr-rite0 ">
                                        <p class="labelp">Late Fee type</p>
                                        <select class="form-control my-txt-fleid width50 right-pull mr-rite0 sel_lt_fee_up" id="late_fee_type<?= $key + 1 ?>"  name="late_fee_type[<?= $key + 1 ?>]" >
                                            <option value="">Late Fee type</option>
                                            <option value="Daily charge" <?php if ($value->late_fee_type == 'Daily charge') { ?>selected<?php } ?>>Daily charge</option>
                                            <option value="One time" <?php if ($value->late_fee_type == 'One time') { ?>selected<?php } ?>>One time</option>
                                        </select>                                   
                                    </div>
                                </li>
                                <li>
                                    <div class="my-col width50 currency_text_box">
                                        <p class="labelp">Late Fee</p>
                                        <span class=""><i style="font-size:14px" class="fa fa-usd"></i></span>
                                        <input type="text" class="form-control my-txt-fleid width50" digits="true" placeholder="" id="late_fee<?= $key + 1 ?>"  name="late_fee[<?= $key + 1 ?>]" value="<?= $value->late_fee ?>"/> 
                                    </div>
                                   <div class="my-col my-col width50 mr-rite0 " <?php //if ($value->late_fee_type == '') { echo 'style="display:none"';}?> >
                                        <p class="labelp">Late Fee Start </p>
                                        <select class="form-control my-txt-fleid width50 right-pull mr-rite0 late_fee_start_class" id="late_fee_start<?= $key + 1 ?>" name="late_fee_start[<?= $key + 1 ?>]">
                                            <option value="">Late Fee Start</option>
                                             <?php for ($k = 1; $k < 31; $k++) { ?>
                                                <option value="<?php echo $k; ?>" <?php if ($value->late_fee_start == $k) { ?>selected<?php } ?>><?php echo ordinal($k); ?></option>
                                            <?php } ?>                                           
                                        </select>
                                    </div>                           
                                </li>
                            </ul>
                            <div class="sub-panel-head">Pets Info</div>
                            <ul  class="form-group pets_info_ul">
                            <?php $data_pets = $data_pets_type = $data_pets_breed = array();  ?>
                            <?php if(!empty($value->pets)) {
                                    $data_pets = explode(',',$value->pets);
                                    $data_pets_type = explode(',',$value->pets_type);
                                    $data_pets_name = explode(',',$value->pets_name);
                                    $data_pets_fee = explode(',',$value->pets_fee);
                                    $data_pets_breed = explode(',',$value->pets_breed);
                           
                                     for ($ij=0;$ij<count($data_pets);$ij++) { ?>
                                        <li class="full_li">
                                            <div class="my-col width50 ">
                                                <input type="hidden" id="pets<?= $key + 1 ?>" value="Yes"  name="pets[<?= $key + 1 ?>][]" >                                                   
                                            </div>
                                            <div class="my-col width50">
                                                <p class="labelp">Pet Type</p>
                                                <select class="form-control my-txt-fleid width50 right-pull mr-rite0 sel_lt_fee_up" id="pets_type<?= $key + 1 ?>"  name="pets_type[<?= $key + 1 ?>][]" >
                                                    <option value="">None</option>
                                                    <option value="dog" <?php if ($data_pets_type[$ij]== 'dog') { ?>selected<?php } ?>>Dog</option>
                                                    <option value="cat" <?php if ($data_pets_type[$ij] == 'cat') { ?>selected<?php } ?>>Cat</option>
                                                    <option value="other" <?php if ($data_pets_type[$ij] == 'other') { ?>selected<?php } ?>>Other</option>
                                                </select>
                                            </div>
                                            <div class="my-col width50">
                                                <p class="labelp">Pet Name</p>
                                                <input type="text" class="form-control my-txt-fleid valid"  placeholder="" id="pets_name<?= $key + 1 ?>" value="<?php echo isset($data_pets_name[$ij]) ? $data_pets_name[$ij] : ''; ?>" name="pets_name[<?= $key + 1 ?>][]" >                           
                                            </div> 
                                            <div class="my-col width50 currency_text_box">
                                                <p class="labelp">Pet Fee</p> 
                                                <span class=""><i style="font-size:14px" class="fa fa-usd"></i></span> 
                                                <input type="text" class="form-control my-txt-fleid valid"  number='true' maxlength="6" digits="true" placeholder="" id="pets_fee<?= $key + 1 ?>" value="<?php echo isset($data_pets_fee[$ij]) ? $data_pets_fee[$ij] : ''; ?>" name="pets_fee[<?= $key + 1 ?>][]" >                           
                                            </div> 
                                        
                                            <div class="my-col width100">
                                                <p class="labelp">Pet Breed/Description</p>
                                                <input type="text" class="form-control my-txt-fleid valid"  placeholder="" id="pets_breed<?= $key + 1 ?>" value="<?php echo isset($data_pets_breed[$ij]) ? $data_pets_breed[$ij] : ''; ?>" name="pets_breed[<?= $key + 1 ?>][]" >                           
                                            </div>        
                                        <?php if($ij==0){?>
                                                <div class="my-col width50">
                                                    <input type="hidden" value="<?php echo !empty($data_pets)?count($data_pets):'1'?>" name="pets_no" class="pets_no">
                                                    <a class="add-pet-btn" href="javascript:;"><i class="fa fa-plus"></i>Add Pet</a>
                                                </div>
                                        <?php }else{ ?>
                                                <div class="my-col width50">
                                                    <a href="javascript:;" class="delete-pets-row" id="pets_row<?= $key + 1 ?>" data-value="<?= $key + 1 ?>"><img src="<?= $this->config->item('templateassets') ?>images/cross.png" alt=""/></a>
                                                </div>
                                            </li>
                                        <?php } ?>
                                    <?php } ?>
                                <?php }else{ ?>
                                            <li class="full_li">
                                                <div class="my-col width50 ">
                                                    <input type="hidden" id="pets<?= $key + 1 ?>" value="Yes"  name="pets[<?= $key + 1 ?>][]" >  
                                                </div>
                                                <div class="my-col width50">
                                                    <p class="labelp">Pet Type</p>
                                                    <select class="form-control my-txt-fleid width50 right-pull mr-rite0 sel_lt_fee_up" id="pets_type1"  name="pets_type[1][1]" >
                                                        <option value="">None</option>
                                                        <option value="dog">Dog</option>
                                                        <option value="cat">Cat</option>
                                                        <option value="other">Other</option>
                                                    </select>
                                                </div>
                                                <div class="my-col width50">
                                                    <p class="labelp">Pet Name</p>
                                                    <input type="text" class="form-control my-txt-fleid valid" id="pets_name1" placeholder=""  name="pets_name[1][1]" >                           
                                                </div>
                                                <div class="my-col width50 currency_text_box">
                                                    <p class="labelp">Pet Fee</p> 
                                                    <span class=""><i style="font-size:14px" class="fa fa-usd"></i></span> 
                                                    <input type="text" class="form-control my-txt-fleid valid" id="pets_fee1"   number='true' maxlength="6" digits="true" placeholder=""  name="pets_fee[1][1]" >                           
                                                </div>
                                                <div class="my-col width100">
                                                    <p class="labelp">Pet Breed/Description</p>
                                                    <input type="text" class="form-control my-txt-fleid valid" id="pets_breed1" placeholder=""  name="pets_breed[1][1]" >                           
                                                </div>
                                                <div class="my-col width50">
                                                    <input type="hidden" value="1" name="pets_no" class="pets_no">
                                                    <a class="add-pet-btn" href="javascript:;"><i class="fa fa-plus"></i>Add Pet</a>
                                                </div>
                                            </li>
                                <?php  } ?>
                                </ul>
                                <div class="sub-panel-head">Emergency Contact</div>
                                <ul  class="form-group">
                                <li>
                                    <p class="labelp">Relationship</p>
                                    <select class="form-control my-txt-fleid relationship" id="relationship<?= $key + 1 ?>"  name="relationship[<?= $key + 1 ?>]" >
                                        <option value="">Select Relationship</option>
                                        <option  <?php if ($value->relationship == 'Wife') { ?>selected<?php } ?> value="Wife">Wife</option>
                                        <option <?php if ($value->relationship == 'Husband') { ?>selected<?php } ?> value="Husband">Husband</option>
                                        <option <?php if ($value->relationship == 'Mother') { ?>selected<?php } ?> value="Mother">Mother</option>
                                        <option <?php if ($value->relationship == 'Father') { ?>selected<?php } ?> value="Father">Father</option>
                                        <option <?php if ($value->relationship == 'Friend') { ?>selected<?php } ?> value="Friend">Friend</option>
                                        <option <?php if ($value->relationship == 'Other') { ?>selected<?php } ?> value="Other">Other</option>
                                    </select>
                                </li>
                                <li>
                                    <div class="my-col width50 ">
                                        <p class="labelp">Name</p>
                                        <input type="text" maxlength="20" class="form-control my-txt-fleid valid emergency_name" id="emergency_name<?= $key + 1 ?>" placeholder="" name="emergency_name[<?= $key + 1 ?>]" value="<?= $value->emergency_name ?>"/>
                                    </div>
                                    <div class="my-col width50 right-pull mr-rite0">
                                        <p class="labelp">Phone</p>
                                        <input type="text"  class="form-control my-txt-fleid valid phone_us emergency_phone" id="emergency_phone<?= $key + 1 ?>" placeholder="" name="emergency_phone[<?= $key + 1 ?>]" value="<?= $value->emergency_phone ?>"/>
                                    </div>
                                </li>
                                <li>
                                    <p class="labelp">Email</p>
                                    <input type="email" class="form-control my-txt-fleid valid emergency_email" id="emergency_email<?= $key + 1 ?>" placeholder="" name="emergency_email[<?= $key + 1 ?>]" value="<?= $value->emergency_email ?>"/>
                                </li>
                            </ul>
                            <ul   class="form-group">

                                <li class="cutom-width">
                                    <div class="my-col full-width "> 
                                        <span class="upload-btn cutom_bt">Upload Lease<input type="file" class="upl_lease" name="upload_lease[<?= $key + 1 ?>]"></span><!-- <em>*</em> -->You can add your lease now or later. However, your tenant will not be able to set up mobile rent payment without a lease on file.
                                        <input type="hidden"  name="uploaded_lease_doc[<?= $key + 1 ?>]" class="uploaded_lease_doc" value="<?php if(isset($leaseDoc->lease_doc) && $leaseDoc->lease_doc!=''){ echo $leaseDoc->lease_doc; } ?>">
                                    </div>
                                   <!--  <div class="my-col width50 right-pull mr-rite0 req">
                                        *Required for tPay
                                    </div> -->
                                </li>
                                 <p class="btn_view_del 1ls_file_name upload_lease_files_cs">
                                    <?php if(isset($leaseDoc->lease_doc) && $leaseDoc->lease_doc!=''){ ?>
                                        <a target="_blank" href="<?php base_url()?>assets/uploads/lease_documents/<?php echo $leaseDoc->lease_doc; ?>">View</a>
                                        <a href="javascript:;" class="clr_up_img del_lease_doc"> Delete</a><br><br>
                                    <?php } ?>
                                </p>                                
                            </ul>
                        </div>

                    <?php } ?>
                <?php } else { ?>
                    <div class="tenant-area">
                        <ul class="form-group" id="tenant-1">
                            <li class="marginbottom"><p class="labelp">First Name</p><input type="text" required="true" customvalidation="true" maxlength="10" class="form-control my-txt-fleid fname" id="fn1" placeholder="First Name" name="first_name[1]"/></li>
                            <li class="marginbottom"><p class="labelp">Middle Name</p><input type="text" class="form-control my-txt-fleid" maxlength="10" placeholder="" id="mn1" name="middle_name[1]"/></li>
                            <li class="marginbottom"><p class="labelp">Last Name</p><input type="text" class="form-control my-txt-fleid" maxlength="10" placeholder="" id="ln1" name="last_name[1]"/></li>
                            <li>  
                                <div class="my-col width50">
                                    <p class="labelp">DOB</p>
                                    <input readonly type="text" required="true" class="form-control my-txt-fleid width50 tenantdob" id="dbn1" placeholder="" name="dob[1]"/> 
                                </div>
                                <div class="my-col width50 right-pull mr-rite0">
                                    <p class="labelp">Mobile</p>
                                    <input type="text" class="form-control my-txt-fleid phone_us width50 " required="true" id="mbl1" placeholder="" name="mobile[1]"/>
                                </div>
                            </li>
                            <li class="marginbottom">
                                <p class="labelp">Email</p>
                                <input type="email" required="true" class="form-control my-txt-fleid" placeholder="" id="emld1" name="email[1]" autocomplete="off"/>
                                 
                            </li>
                            <li>   
                                <div class="my-col width50">
                                    <p class="labelp">Language</p>
                                    <select class="form-control my-txt-fleid width50 right-pull mr-rite0" required="true" id="lngl1" name="preferred_language[1]">
                                        <option value="">Select Language</option>
                                        <option value="English">English</option>
                                        <option value="Spanish">Spanish</option>
                                    </select>
                                </div>
                                <div class="my-col width50 right-pull mr-rite0">
                                    <p class="labelp">Lease Start</p>
                                    <input type="text" class="form-control my-txt-fleid  leasefrom lease-start-date" required="true"  placeholder="" id="lsrt1" name="lease_start_date[1]"/>
                                   
                                </div>
                            </li>
                            <p class="ls_file_name"></p>
                            <li>
                                <div class="my-col width50">
                                    <p class="labelp">Lease End</p>
                                    <input type="text" required="true"  class="form-control my-txt-fleid  lease-end-date leaseto" placeholder="" id="lsrt31" name="lease_end_date[1]"/>
                                </div>
                                <div class="my-col width50 right-pull mr-rite0 currency_text_box">
                                    <p class="labelp">Rent Amount</p>                            
                                    <span class=""><i class="fa fa-usd" style="font-size:14px"></i></span>
                                    <input type="text" required="true" number="true" class="form-control my-txt-fleid money2" digits="true"  maxlength="6" id="rnt1" placeholder="" name="rent_amount[1]"/>
                                </div>
                            </li>
                            <li>
                                <div class="my-col width50">        
                                    <p class="labelp">Rent Due</p>
                                    <select class="form-control my-txt-fleid due_date_start_class" required="true"  id="duda1"  name="due_date[1]">
                                        <option value="">Rent Due</option>
                                        <?php for ($j = 1; $j < 31; $j++) { ?>
                                            <option value="<?php echo $j; ?>"><?php echo ordinal($j); ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="my-col width50 right-pull mr-rite0">
                                    <p class="labelp">Late Fee type</p>
                                    <select class="form-control my-txt-fleid width50 right-pull mr-rite0" required="true" id="ltfet1"  name="late_fee_type[1]">
                                        <option value="">Late Fee type</option>
                                        <option value="Daily charge">Daily charge</option>
                                        <option value="One time">One time</option>
                                    </select>
                                </div>
                            </li>
                            <li>
                                <div class="my-col width50 currency_text_box">
                                    <p class="labelp">Late Fee</p>
                                    <span class=""><i style="font-size:14px" class="fa fa-usd"></i></span>
                                    <input type="text" class="form-control my-txt-fleid " required="true" number='true' digits="true" id="ltfe1" maxlength="6" placeholder="" name="late_fee[1]"/> 
                                </div>
                                <div class="my-col width50 right-pull mr-rite0 ">
                                    <p class="labelp">Late Fee Start</p>
                                     <select class="form-control my-txt-fleid late_fee_start_class" required="true"  id="late_fee_start1"  name="late_fee_start[1]">
                                        <option value="">Late Fee Start</option>
                                        <?php for ($j = 1; $j < 31; $j++) { ?>
                                            <option value="<?php echo $j; ?>"><?php echo ordinal($j); ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </li>
                        </ul>
                        <div class="sub-panel-head">Pets Info</div>
                        <ul class="form-group pets_info_ul">
                            <li  class="full_li">
                                <div class="my-col width50 ">
                                    <input type="hidden" id="pets1" value="Yes"  name="pets[1][1]" >                                                   
                                </div>
                                <div class="my-col width50">
                                    <p class="labelp">Pet type</p>
                                    <select class="form-control my-txt-fleid width50 right-pull mr-rite0 sel_lt_fee_up" id="pets_type1"  name="pets_type[1][1]" >
                                        <option value="">None</option>
                                        <option value="dog">Dog</option>
                                        <option value="cat">Cat</option>
                                        <option value="other">Other</option>
                                    </select>
                                </div>
                                <div class="my-col width50">  
                                    <p class="labelp">Pet Name</p>                      
                                    <input type="text" class="form-control my-txt-fleid valid" placeholder="Pet Name" id="pets_name1"  name="pets_name[1][1]" >                           
                                </div> 
                                <div class="my-col width50 currency_text_box">
                                    <p class="labelp">Pet Fee</p> 
                                    <span class=""><i style="font-size:14px" class="fa fa-usd"></i></span>                       
                                    <input type="text" class="form-control my-txt-fleid valid" number='true' maxlength="6" digits="true" placeholder="Pet Fee" id="pets_fee1"  name="pets_fee[1][1]" >                           
                                </div> 
                                <div class="my-col width100">
                                    <p class="labelp">Pet Breed/Description</p>                           
                                    <input type="text" class="form-control my-txt-fleid valid" placeholder="" id="pets_breed1"  name="pets_breed[1][1]" >                           
                                </div>  
                                <div class="my-col width50">
                                    <input type="hidden" value="1" name="pets_no" class="pets_no">
                                    <a class="add-pet-btn" href="javascript:;"><i class="fa fa-plus"></i>Add Pet</a>
                                </div>
                            </li>
                        </ul>
                        <div class="sub-panel-head">Emergency Contact</div>
                        <ul  class="form-group">
                            <br>
                            <li>
                                <p class="labelp">Relationship</p>   
                                <select class="form-control my-txt-fleid relationship" id="relationship1"  name="relationship[1]" >
                                    <option value="">Select Relationship</option>
                                    <option value="Wife">Wife</option>
                                    <option value="Husband">Husband</option>
                                    <option value="Mother">Mother</option>
                                    <option value="Father">Father</option>
                                    <option value="Friend">Friend</option>
                                    <option value="Other">Other</option>
                                </select>
                            </li>
                            <li>
                                <div class="my-col width50 ">
                                    <p class="labelp">Emergency Name</p>   
                                    <input type="text" maxlength="20" class="form-control my-txt-fleid valid emergency_name" id="emergency_name1" placeholder="" name="emergency_name[1]" value=""/>
                                </div>
                                <div class="my-col width50 right-pull mr-rite0">
                                    <p class="labelp">Phone</p>   
                                    <input type="text" class="form-control my-txt-fleid valid phone_us emergency_phone" id="emergency_phone1" placeholder="" name="emergency_phone[1]" value=""/>
                                </div>
                            </li>
                            <li>
                                <p class="labelp">Email</p>   
                                <input type="email" class="form-control my-txt-fleid valid emergency_email" id="emergency_email1" placeholder="" name="emergency_email[1]" value=""/>
                            </li>
                        </ul>
                        
                        <ul   class="form-group">

                                <li class="full_li custom-full-li">
                                    <div class="my-col width50 "> 
                                        <span class="upload-btn">Upload Lease<input type="file" class="upl_lease" name="upload_lease[1]"></span> <div class="require_lease_doc"><em>*</em>You can add your lease now or later. However, your tenant will not be able to set up mobile rent payment without a lease on file.</div>
                                    <input type="hidden" class="uploded_lease_doc" name="uploded_lease_doc[1]">
                                    </div>
                                    <!-- <div class="my-col width50 right-pull mr-rite0 req">
                                        *Required for tPay
                                    </div> -->
                                </li>
                                 <p class="ls_file_name upload_lease_files_cs">
                                   
                                </p>                                
                            </ul>

                    </div>
                <?php } ?>
            </div>
                <input type="hidden" value="<?php echo !empty($tenantDetails)?count($tenantDetails):'1'?>" name="total_tenant" class="tenant_no">
            <a href="javascript:;" class="edit-btn mr-btm30 upadd-tenant"><i class="fa fa-plus"></i>Add Tenant</a>
        </div>

    </div>

     <?php if($memberRecord->mailing_address1==''){?>
        <div class="my-panel ">
            <div class="panel-head">
                <div class="panel-title">
                    <h1>Add Your Mailing Address for Rent Payment</h1>
                </div>
            </div>
            <div class="panel-body">
                <ul class="address_mailing_ul form-group border0">

                    <li><input type="text" class="form-control my-txt-fleid" required="true" maxlength="30" placeholder="Address 1" name="address_1" value="<?php if(isset($memberRecord->mailing_address1) && $memberRecord->mailing_address1!=''){ echo $memberRecord->mailing_address1; }?>"/></li>
                    <li><input type="text" class="form-control my-txt-fleid" placeholder="Address 2" maxlength="30" name="address_2" value="<?php if(isset($memberRecord->mailing_address2) && $memberRecord->mailing_address2!=''){ echo $memberRecord->mailing_address2; }?>"/>

                    </li>
                    <li>
                        <input type="text" class="form-control my-txt-fleid " value="<?php if(isset($memberRecord->mailing_unit) && $memberRecord->mailing_unit!=0){ echo $memberRecord->mailing_unit; }?>"  placeholder="Unit#" name="unit"/>
                    </li>
                    <li>
                        <input type="text" class="form-control my-txt-fleid " required="true" maxlength="20" value="<?php if(isset($memberRecord->mailing_city) && $memberRecord->mailing_city!=''){ echo $memberRecord->mailing_city; }?>" placeholder="City" name="city"/>
                    </li>
                    <li>
                        <?php
                        $data = array();
                        $state_id = $memberRecord->mailing_state_id;
                        $states = get_states('223');
                        if (!empty($states)) {
                            foreach ($states as $state) {
                                $data[$state->region_id] = $state->region_name;
                            }
                        }
                        $list = "id='state'  class='form-control my-txt-fleid' required='1'";
                        echo form_dropdown('state', $data, $state_id, $list);
                        ?> 

                    </li>
                    <li>
                        <input type="text" class="form-control my-txt-fleid " required="true" maxlength="10" placeholder="Zipcode" name="zip" value="<?php if(isset($memberRecord->mailing_zip) && $memberRecord->mailing_zip!=0){ echo $memberRecord->mailing_zip; }?>"/>
                    </li>
                </ul>
            </div>

        </div>

        <?php } ?>


    <div class="center">	
        <a href="<?= site_url('update-property-step1') ?>" class="my-link my-back">Back</a>
        <input type="submit" class="next" value="NEXt" style=""/></div>
    <?php echo form_close(); ?>
</div>
</section>


<style>
    .upload-btn > input {
        bottom: 0;
        cursor: pointer;
        font-size: 0;
        left: 0;
        opacity: 0;
        position: absolute;
        right: 0;
        top: 0;
        width: 100%;
    }
</style>
<script>
  $(document).on('change', ".sel_lt_fee_up", function () {
             if($(this).val()!=''){
               $(this).parent().next().show();
           }else{
               $(this).parent().next().show();
           }
        });
    </script>