<?php //echo '<pre>';print_r($allProperties);die;            ?>
<section class="lease-part wb-my-properties-page">
    <style type="text/css">

.upload-btn > input {
    bottom: 0;
    cursor: pointer;
    font-size: 0;
    left: 0;
    opacity: 0;
    position: absolute;
    right: 0;
    top: 0;
    width: 100%;
}
</style>
    <div class="container">
        <div id="tabs">
            <div class="my-tab1"> 
                <a href="<?= site_url('add-property') ?>" class="btn-tenant"><i class="fa fa-plus"></i>Add Property</a>
                <ul class="my-tab ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">
                    <!--
                                            <li class="active all-tabs-panel-li"><a href="#summary" class="summary all-tabs-panel">Summary</a></li>
                    -->
                    <?php foreach ($allProperties as $key => $prop) { ?>
                        <li class="<?php if ($key == 0) { ?>active<?php } ?> all-tabs-panel-li"><a href="#tabs-<?= $prop->prop_id ?>" data-value="<?= $prop->prop_id ?>" class="all-tabs-panel"><?= $prop->city ?>, <?= $prop->address1 ?> <?= $prop->address2 ?></a></li>
                    <?php } ?>
                </ul>
            </div>
            <div class="tab-area properties-page">
                <?php
                foreach ($allProperties as $val) {
                    $prp_photos = $val->photos;
                    ?>
                    <div id="tabs-<?= $val->prop_id ?>">

                        <div class="tenant mr-btm0">

                            <div class="ajax_report alert display-hide" role="alert" style=" margin-top: -22px; margin-bottom:10px;width:871px; "><span class="close-message"></span><div class="ajax_message">Hello Message</div></div>

                            <ul class="tenant2">

                                <li>

                                    <span class="my-span"> <?= $val->property_type ?> <?= $val->city ?>, <?= $val->address1 ?> <?= $val->address2 ?><br>
                                        <?php if ($val->community_status == 'Yes') { ?><?= $val->community ?>, Gate: <?= $val->gate_code ?>,  PO Box <?= $val->unit_po_box ?> <br>
                                            <?= $val->community_managing_company ?>, <?= $val->community_contact_number ?><?php } else { ?> No community<?php } ?>
                                        <i class="fa fa-pencil-square-o edt_propert_i" id="<?= $val->prop_id ?>"></i>
                                    </span>
                                    <?php if ($val->community_status == 'Yes' && $val->community_doc != '') { ?>
                                   <!--  <a target="_blank" href="<?php echo base_url() ?>assets/uploads/community_documents/<?php echo $val->community_doc ?>" class="docname" title="click to view in new tab">
                                                    <img src="<?php echo base_url() ?>/assets/default/images/file-upload.png" style="float: right; width: 85px;">
                                                </a>
                                    <p class="del_comm_doc" id="<?php echo $val->prop_id ?>">Delete</p> -->
                                        </ul>
                                    <?php } ?>
                                    <div class="ajax_report alert display-hide" role="alert" style="padding:10px;">
                                        <i class="ajax_message"></i>
                                    </div>
                                    <?php echo form_open('update-address', array('class' => 'ajaxForm', 'id' => "form-" . $val->prop_id)) ?>
                                    <input type="hidden" name="property_id" value="<?= $val->prop_id ?>">
                                    <div class="edit-container drmyeditproperty" id="edit-cnt-<?= $val->prop_id ?>" style="display: none">
                                        <div id="wait-div"></div>
                                        <div  class="form-group1 edt_property" style="margin-top:10px">
                                            <div class="my-div">
                                                <select class="form-control " name="property_type">
                                                    <option value="">Select Property Type</option>
                                                    <option value="SFR" <?php if ($val->property_type == 'SFR') { ?>selected<?php } ?>>Single Family Homes</option>
                                                    <option value="Condo" <?php if ($val->property_type == 'Condo') { ?>selected<?php } ?>>Condominium</option>
                                                    <option value="Apt" <?php if ($val->property_type == 'Apt') { ?>selected<?php } ?>>Apartment Building</option>
                                                </select>
                                            </div>
                                            <div class="my-div"><input type="text" class="form-control" placeholder="Address 1" name="address1" value="<?php echo $val->address1; ?>"/></div>
                                            <div class="my-div"><input type="text" class="form-control" placeholder="Address 2" name="address2" value="<?php echo $val->address2; ?>"/></div>
                                        </div>
                                        <div class="form-group1 edt_property">
                                            <div class="my-div">
                                                <input type="text" class="form-control" style="width:100px" placeholder="Unit#" name="unit" value="<?php echo $val->unit_number != '0' ? $val->unit_number : ''; ?>"/>
                                                <input type="text" class="form-control width50 right-pull mr-rite0" placeholder="City" name="city" value="<?php echo $val->city; ?>"/>
                                            </div>

                                            <div class="my-div">
                                                <input type="hidden" name="country" value="223">
                                                <?php
                                                $data = array();
                                                $states = get_states(223);
                                                if (!empty($states)) {
                                                    foreach ($states as $state) {
                                                        $data[$state->region_id] = $state->region_name;
                                                    }
                                                }
                                                $list = "id='state_$val->prop_id'  class='form-control state' required='1'";
                                                echo form_dropdown('state', $data, $val->state_id, $list);
                                                ?> 
                                            </div>


                                            <div class="my-div">
                                                <input type="text" class="form-control  " placeholder="Zip" name="zip" value="<?php echo $val->zip; ?>"/>
                                                <input type="hidden" class="form-control  " name="pid" value="<?php echo $val->prop_id; ?>"/>
                                            </div>
                                        </div>

                                        <div class="my-div custom-width">
                                            <label><b>Property in Community</b></label>
                                            <p class="my-radio">  <input type="radio" id="radio10<?= $val->prop_id ?>" resp="<?= $val->prop_id ?>" name="is_community" class="is_community" value="Yes" <?php if ($val->community_status == 'Yes') { ?>checked="checked"<?php } ?>/> 
                                                <label for="radio10<?= $val->prop_id ?>">Yes</label></p>
                                            <p class="my-radio"> <input type="radio" id="radio11<?= $val->prop_id ?>" resp="<?= $val->prop_id ?>" name="is_community" class="is_community" value="No" <?php if ($val->community_status == 'No') { ?>checked="checked"<?php } ?>/> 
                                                <label for="radio11<?= $val->prop_id ?>">No</label></p>
                                        </div>

                                        <div class="community_box<?= $val->prop_id ?>" <?php
                                        if ($val->community_status == 'No') {
                                            echo 'style="display:none"';
                                        }
                                        ?>>
                                            <div class="form-group1">
                                                <div class="my-div"><input type="text" class="form-control my-txt-fleid" placeholder="Community" name="community" value="<?php echo $val->community; ?>"></div>
                                                <div class="my-div">
                                                    <div class="my-col width50">
                                                        <input type="text" class="form-control my-txt-fleid width50 " placeholder="Gate Code" name="gate_code" value="<?php echo $val->gate_code; ?>"/>
                                                    </div>
                                                    <div class="my-col width50 right-pull mr-rite0">
                                                        <input type="text" class="form-control my-txt-fleid width50 right-pull mr-rite0" placeholder="Unit PO Box #" name="po_box" value="<?php echo $val->unit_po_box; ?>" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group1">
                                                <div class="my-div"><input type="text" class="form-control my-txt-fleid" placeholder="Community Mng Company" name="community_company" value="<?php echo $val->community_managing_company; ?>"/></div>
                                                <div class="my-div"><input type="text" class="form-control my-txt-fleid phone_us" placeholder="Contact Number" name="company_number" value="<?php echo $val->community_contact_number; ?>"/></div>
                                            </div>
                                            <div class="form-group1">
                                                <div class="my-div">
                                                <span class="upload-btn" style="width:255px;">Upload Community Docs
                                                    <input type="file" class="comm_docs" name="comm_docs">
                                                </span>
                                                </div>
                                            </div>
                                            <p class="btn_view_del">
                                                <span class="show_file_name2" style="width:100%; font-size:14px;text-align:left; background-image:none; padding-left:0px; <?php if($val->community_doc!=''){ echo 'display:inline'; }?>">
                                                    <?php if($val->community_doc !=''){ ?>
                                                        <?php echo substr($val->community_doc, 0, 20).'..'; ?>
                                                        <a target="_blank" href="<?php echo base_url() ?>assets/uploads/community_documents/<?php echo $val->community_doc ?>">View
                                                        </a>
                                                        <a class='btn btn-edit btn-primary clr_up_img del_comm_doc' style="float:none; position:static;" id="<?php echo $val->prop_id ?>" href='javascript:;'> Delete</a>
                                                    <?php } ?>
                                                </span>
                                            </p>

                                        </div>
                                        <div class="form-group1">
                                        <button data-value="<?= $val->prop_id ?>"  onclick="show_loader();" class="edit-btn submit-form">Update</button>
                                        <button data-value="<?= $val->prop_id ?>"  class="edit-btn cncl_edit">Cancel</button>
                                        <!--<a href="javascript:void(0)" class="cncl_edit">Cancel</a>-->
                                        </div>
                                    </div>

                                </li>
                                <?php echo form_close(); ?>

                            </ul>
                            <br>
                            <div class="" style="width:100%">
                            <h2 class="propphotos my-txt">Property Photos</h2>
                            <ul class="btn-group mr-top10">

                                <li>
                                    <?php echo form_open_multipart('property/upload_property_photo', array('class' => 'ajaxForm', 'id' => "pform-" . $val->prop_id)) ?>
                                    <div class="ajax_report alert display-hide" role="alert" style=" position: absolute; margin-top: -45px;;"><div class="ajax_message"></div></div>
                                    <input class="browse-btn prop_photo"  type="file" id="<?php echo $val->prop_id; ?>" name="userfile[]">
                                    <input type="hidden" name="propty_id" value="<?php echo $val->prop_id ?>"/>
                                    <button class="upload-btn photo-up-btn blue-col">Upload Photos</button>
                                    <?php echo form_close(); ?>
                                </li>

                            </ul>
                            </div>
                            <div class="my-thumbs-list">
                                <ul class="upload-file">
                                    <?php if (!empty($prp_photos)) { ?>
                                        <?php foreach ($prp_photos as $img) { ?>
                                            <li>
                                                <span id="<?php echo $img->id ?>" class="crossicon del_photo">X</span>
                                                <a href="<?php echo base_url() . $img->image ?>" class="docname" data-lightbox="roadtrip<?php echo $img->property_id ?>">
                                                    <img src="<?php echo base_url() . $img->image ?>" alt="" style="width:120px;height:120px">
                                                </a>
                                            </li>
                                        <?php } ?>
                                    <?php } else { ?>
                                        <li>No photos</li>
                                    <?php } ?>

                                </ul>
                            </div>

                            <?php echo form_open('update-property', array('class' => 'ajaxForm', 'id' => "form-" . $val->prop_id)) ?>
                            <input type="hidden" name="property_id" value="<?= $val->prop_id ?>">
                            <ul class="select-grp">
                                <li><label class="label1">Garbage day</label>
                                    <select class="form-control pick-up-day<?= $val->prop_id ?>" name="garbage_pick_day" disabled>
                                        <option value="">Select Day</option>
                                        <option value="Sunday" <?php if ($val->garbage_day == 'Sunday') { ?>selected<?php } ?>>Sunday</option>
                                        <option value="Monday" <?php if ($val->garbage_day == 'Monday') { ?>selected<?php } ?>>Monday</option>
                                        <option value="Tuesday" <?php if ($val->garbage_day == 'Tuesday') { ?>selected<?php } ?>>Tuesday</option>
                                        <option value="Wednesday" <?php if ($val->garbage_day == 'Wednesday') { ?>selected<?php } ?>>Wednesday</option>
                                        <option value="Thursday" <?php if ($val->garbage_day == 'Thursday') { ?>selected<?php } ?>>Thursday</option>
                                        <option value="Friday" <?php if ($val->garbage_day == 'Friday') { ?>selected<?php } ?>>Friday</option>
                                        <option value="Saturday" <?php if ($val->garbage_day == 'Saturday') { ?>selected<?php } ?>>Saturday</option>
                                    </select>
                                </li>
                                <li><label class="label1">Recycle day</label>
                                    <select class="form-control pick-up-day<?= $val->prop_id ?>" name="recycle_pick_day" disabled>
                                        <option value="">Select Day</option>
                                        <option value="Sunday" <?php if ($val->recycle_day == 'Sunday') { ?>selected<?php } ?>>Sunday</option>
                                        <option value="Monday" <?php if ($val->recycle_day == 'Monday') { ?>selected<?php } ?>>Monday</option>
                                        <option value="Tuesday" <?php if ($val->recycle_day == 'Tuesday') { ?>selected<?php } ?>>Tuesday</option>
                                        <option value="Wednesday" <?php if ($val->recycle_day == 'Wednesday') { ?>selected<?php } ?>>Wednesday</option>
                                        <option value="Thursday" <?php if ($val->recycle_day == 'Thursday') { ?>selected<?php } ?>>Thursday</option>
                                        <option value="Friday" <?php if ($val->recycle_day == 'Friday') { ?>selected<?php } ?>>Friday</option>
                                        <option value="Saturday" <?php if ($val->recycle_day == 'Saturday') { ?>selected<?php } ?>>Saturday</option>
                                    </select>
                                </li>
                                <li><label class="label1">Yard waste day</label>
                                    <select class="form-control pick-up-day<?= $val->prop_id ?>" name="yard_waste_pick_day" disabled>
                                        <option value="">Select Day</option>
                                        <option value="Sunday" <?php if ($val->yard_waste_day == 'Sunday') { ?>selected<?php } ?>>Sunday</option>
                                        <option value="Monday" <?php if ($val->yard_waste_day == 'Monday') { ?>selected<?php } ?>>Monday</option>
                                        <option value="Tuesday" <?php if ($val->yard_waste_day == 'Tuesday') { ?>selected<?php } ?>>Tuesday</option>
                                        <option value="Wednesday" <?php if ($val->yard_waste_day == 'Wednesday') { ?>selected<?php } ?>>Wednesday</option>
                                        <option value="Thursday" <?php if ($val->yard_waste_day == 'Thursday') { ?>selected<?php } ?>>Thursday</option>
                                        <option value="Friday" <?php if ($val->yard_waste_day == 'Friday') { ?>selected<?php } ?>>Friday</option>
                                        <option value="Saturday" <?php if ($val->yard_waste_day == 'Saturday') { ?>selected<?php } ?>>Saturday</option>
                                    </select>
                                </li>
                            </ul>
                            <h2 class="my-txt">Amenities</h2>
                            <ul class="select-grp border-line">

                                <div  class="form-group1 mr-top20">
                                    <div class="my-div"><input type="text" class="form-control my-txt-fleid amem_input" placeholder="Smoke Detectors" readonly/></div>
                                    <div class="my-div">
                                        <select class="form-control my-txt-fleid width50 safety-equip<?= $val->prop_id ?>" name="safety_equipment1" disabled>
                                            <option value=""> Smoke Detectors:</option>
                                            <option value="1" <?php if ($val->safety_equipment1 == '1') { ?>selected<?php } ?>>1</option>
                                            <option value="2" <?php if ($val->safety_equipment1 == '2') { ?>selected<?php } ?>>2</option>
                                            <option value="3" <?php if ($val->safety_equipment1 == '3') { ?>selected<?php } ?>>3</option>
                                            <option value="4" <?php if ($val->safety_equipment1 == '4') { ?>selected<?php } ?>>4</option>
                                            <option value="5" <?php if ($val->safety_equipment1 == '5') { ?>selected<?php } ?>>5</option>
                                            <option value="6" <?php if ($val->safety_equipment1 == '6') { ?>selected<?php } ?>>6</option>
                                            <option value="N/A" <?php if ($val->safety_equipment1 == 'N/A') { ?>selected<?php } ?>>N/A</option>
                                        </select>
                                    </div>
                                </div>
                                <div  class="form-group1">
                                    <div class="my-div"><input type="text" class="form-control my-txt-fleid amem_input" placeholder="Fire Extinguishers" readonly/></div>
                                    <div class="my-div">
                                        <select class="form-control my-txt-fleid width50 safety-equip<?= $val->prop_id ?>" name="safety_equipment2" disabled>
                                            <option value=""> Fire Extinguishers:</option>
                                            <option value="1" <?php if ($val->safety_equipment2 == '1') { ?>selected<?php } ?>>1</option>
                                            <option value="2" <?php if ($val->safety_equipment2 == '2') { ?>selected<?php } ?>>2</option>
                                            <option value="3" <?php if ($val->safety_equipment2 == '3') { ?>selected<?php } ?>>3</option>
                                            <option value="4" <?php if ($val->safety_equipment2 == '4') { ?>selected<?php } ?>>4</option>
                                            <option value="N/A" <?php if ($val->safety_equipment2 == 'N/A') { ?>selected<?php } ?>>N/A</option>
                                        </select>
                                    </div>
                                </div>
                                <div  class="form-group1">
                                    <div class="my-div"><input type="text" class="form-control my-txt-fleid amem_input" placeholder="Heat Electric or Heat Furnace" readonly/></div>
                                    <div class="">
                                        <select class="form-control my-txt-fleid width50 safety-equip<?= $val->prop_id ?>" name="heat_type" disabled>
                                            <option value="N/A" <?php if ($val->heat_type == 'N/A') { ?>selected<?php } ?>>N/A</option>
                                            <option value="Central" <?php if ($val->heat_type == 'Central') { ?>selected<?php } ?>>Central</option>
                                            <option value="Furnace" <?php if ($val->heat_type == 'Furnace') { ?>selected<?php } ?>>Furnace</option>
                                            <option value="Room" <?php if ($val->heat_type == 'Room') { ?>selected<?php } ?>>Room</option>
                                        </select>

                                    </div>
                                    <div class=""  <?php
                                    if ($val->heat_type == 'N/A') {
                                        echo 'style="display:none;"';
                                    }
                                    ?>>
                                             <?php $chk = 0; ?>
                                        <select class="form-control my-txt-fleid width50 safety-equip<?= $val->prop_id ?>" name="heat_filter_size" disabled>
                                            <option <?php
                                            if ($val->heat_filter_size == 'N/A') {
                                                $chk = 1;
                                                ?>selected<?php } ?>>N/A</option>
                                            <option <?php
                                            if ($val->heat_filter_size == '20x20x1') {
                                                $chk = 1;
                                                ?>selected<?php } ?>>20x20x1</option>
                                            <option <?php
                                            if ($val->heat_filter_size == '20x25x1') {
                                                $chk = 1;
                                                ?>selected<?php } ?>>20x25x1</option>
                                            <option <?php
                                            if ($val->heat_filter_size == '16x20x1') {
                                                $chk = 1;
                                                ?>selected<?php } ?>>16x20x1</option>
                                            <option <?php
                                            if ($val->heat_filter_size == '16x25x1') {
                                                $chk = 1;
                                                ?>selected<?php } ?>>16x25x1</option>
                                            <option <?php
                                            if ($val->heat_filter_size == '14x25x1') {
                                                $chk = 1;
                                                ?>selected<?php } ?>>14x25x1</option>
                                            <option <?php
                                            if ($val->heat_filter_size == '12x12x1') {
                                                $chk = 1;
                                                ?>selected<?php } ?>>12x12x1</option>
                                            <option <?php
                                            if ($val->heat_filter_size == '12x16x1') {
                                                $chk = 1;
                                                ?>selected<?php } ?>>12x16x1</option>
                                            <option <?php
                                            if ($val->heat_filter_size == '12x18x1') {
                                                $chk = 1;
                                                ?>selected<?php } ?>>12x18x1</option>
                                            <option <?php if ($chk == 0) { ?>selected<?php } ?>>Other</option>
                                        </select>
                                        <input type="text" name="heat_filter_size_other" class="form-control my-txt-fleid filtersize heatfilter<?= $val->prop_id ?>" value="<?php
                                        if ($chk == 0) {
                                            echo $val->heat_filter_size;
                                        }
                                        ?>"  disabled="true" style="<?php
                                               if ($chk == 1) {
                                                   echo 'display:none;';
                                               }
                                               ?>width: 90px;float: left"/>

                                    </div>
                                </div>
                                <div  class="form-group1">
                                    <div class="my-div"><input type="text" class="form-control my-txt-fleid amem_input" placeholder="AC Central or AC Room" readonly/></div>
                                    <div class="">
                                        <select class="form-control my-txt-fleid width50 safety-equip<?= $val->prop_id ?>" name="ac_type" disabled>
                                            <option value="N/A" <?php if ($val->ac_type == 'N/A') { ?>selected<?php } ?>>N/A</option>
                                            <option value="Central" <?php if ($val->ac_type == 'Central') { ?>selected<?php } ?>>Central</option>
                                            <option value="Furnace" <?php if ($val->ac_type == 'Furnace') { ?>selected<?php } ?>>Furnace</option>
                                            <option value="Room" <?php if ($val->ac_type == 'Room') { ?>selected<?php } ?>>Room</option>
                                        </select>

                                    </div>
                                    <div class="">
                                        <?php $chk2 = 0; ?>
                                        <select class="form-control my-txt-fleid width50 safety-equip<?= $val->prop_id ?>" <?php
                                        if ($val->ac_type == 'N/A') {
                                            echo 'style="display:none;"';
                                        }
                                        ?> name="ac_filter_size" disabled>
                                            <option <?php
                                            if ($val->ac_filter_size == 'N/A') {
                                                $chk2 = 1;
                                                ?>selected<?php } ?>>N/A</option>
                                            <option <?php
                                            if ($val->ac_filter_size == '20x20x1') {
                                                $chk2 = 1;
                                                ?>selected<?php } ?>>20x20x1</option>
                                            <option <?php
                                            if ($val->ac_filter_size == '20x25x1') {
                                                $chk2 = 1;
                                                ?>selected<?php } ?>>20x25x1</option>
                                            <option <?php
                                            if ($val->ac_filter_size == '16x20x1') {
                                                $chk2 = 1;
                                                ?>selected<?php } ?>>16x20x1</option>
                                            <option <?php
                                            if ($val->ac_filter_size == '16x25x1') {
                                                $chk2 = 1;
                                                ?>selected<?php } ?>>16x25x1</option>
                                            <option <?php
                                            if ($val->ac_filter_size == '14x25x1') {
                                                $chk2 = 1;
                                                ?>selected<?php } ?>>14x25x1</option>
                                            <option <?php
                                            if ($val->ac_filter_size == '12x12x1') {
                                                $chk2 = 1;
                                                ?>selected<?php } ?>>12x12x1</option>
                                            <option <?php
                                            if ($val->ac_filter_size == '12x16x1') {
                                                $chk2 = 1;
                                                ?>selected<?php } ?>>12x16x1</option>
                                            <option <?php if ($val->ac_filter_size == '12x18x1') { ?>selected<?php } ?>>12x18x1</option>
                                            <option <?php if ($chk2 == 0) { ?>selected<?php } ?>>Other</option>
                                        </select>
                                        <input type="text" name="ac_filter_size_other" class="form-control my-txt-fleid filtersize acfilter<?= $val->prop_id ?>" value="<?php
                                        if ($chk2 == 0) {
                                            echo $val->ac_filter_size;
                                        }
                                        ?>" disabled="true" style="<?php
                                               if ($chk2 == 1 || $val->ac_type == 'N/A') {
                                                   echo 'display:none;';
                                               }
                                               ?>width: 90px;float: left"/>
                                    </div>
                                    <?php
                                    foreach ($allAmenties as $value) {
                                        $amenties = explode(',', $val->amenties);
                                        ?>
                                        <div  class="form-group1">
                                            <div class="my-div"><input type="text" class="form-control my-txt-fleid amem_input" placeholder="<?= $value->name ?>" readonly/></div>
                                            <div class="my-div">
                                                <p class="my-radio">  <input disabled class="all-radio<?= $val->prop_id ?>" type="radio" id="radio01_<?= $value->id ?><?= $val->prop_id ?>" name="<?= $value->name ?>" value="<?= $value->id ?>" <?php if (in_array($value->id, $amenties)) { ?>checked<?php } ?>/>  <label for="radio01_<?= $value->id ?><?= $val->prop_id ?>">Yes</label></p>
                                                <p class="my-radio"> <input disabled class="all-radio<?= $val->prop_id ?>" type="radio" id="radio02_<?= $value->id ?><?= $val->prop_id ?>" name="<?= $value->name ?>" value="" <?php if (!in_array($value->id, $amenties)) { ?>checked<?php } ?>/> <label for="radio02_<?= $value->id ?><?= $val->prop_id ?>">No</label></p>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <div  class="form-group1">
                                        <div class="my-div"><input type="text" class="form-control my-txt-fleid amem_input" placeholder="Pest control home" readonly/></div>
                                        <div class="control_div">
                                            <p class="my-radio">  <input disabled type="radio" id="radio01_pest_control_home<?= $val->prop_id ?>" name="pest_control_home" value="yes" <?php if ($val->pest_control_home == 'yes') { ?>checked<?php } ?>/>  <label for="radio01_pest_control_home<?= $val->prop_id ?>">Yes</label></p>
                                            <p class="my-radio"> <input disabled type="radio" id="radio02_pest_control_home<?= $val->prop_id ?>" name="pest_control_home" value="no" <?php if ($val->pest_control_home == 'no') { ?>checked<?php } ?>/> <label for="radio02_pest_control_home<?= $val->prop_id ?>">No</label></p>
                                        </div>
                                        <div class="service-provider" <?php
                                        if ($val->pest_control_home == 'no') {
                                            echo 'style="display:none"';
                                        }
                                        ?>>
                                            <input type="text" class="form-control my-txt-fleid width50" id="psp<?= $val->prop_id ?>" placeholder="Company" disabled="true" readonly="true" name="pest_service_provider" value="<?= $val->pest_service_provider ?>"/>
                                            <input type="text" class="form-control my-txt-fleid width50 mr-rite0 phone_us" id="pspno<?= $val->prop_id ?>" placeholder="Contact# " disabled="true" readonly="true" name="pest_service_provider_number" value="<?= $val->pest_service_provider_number != '' ? $val->pest_service_provider_number : '' ?>"/>
                                        </div>
                                    </div>
                                    <div  class="form-group1 is-serv-inc">
                                        <div class="my-div input-div"><input type="text" class="form-control my-txt-fleid amem_input" placeholder="Yard" readonly/></div>
                                        <div class="my-div midd-div" style="width:147px;">
                                            <p class="my-radio">  <input disabled type="radio" id="radio01_yard<?= $val->prop_id ?>" name="yard" value="yes" onClick="show_service_provided(this.value)" <?php if ($val->yard == 'yes') { ?>checked<?php } ?>/>  <label for="radio01_yard<?= $val->prop_id ?>">Yes</label></p>
                                            <p class="my-radio"> <input disabled type="radio" id="radio02_yard<?= $val->prop_id ?>" name="yard" value="no" onClick="show_service_provided(this.value)" <?php if ($val->yard == 'no') { ?>checked<?php } ?>/> <label for="radio02_yard<?= $val->prop_id ?>">No</label></p>
                                        </div>

                                        <div style="float: left; padding: 10px; width:100%" class="my-div is_yard_service_provide main-provider" style="width:147px; display:<?php if ($val->yard == 'yes') { ?> block;<?php } else { ?> none;<?php } ?>">
                                            <span class="is-sev-label" style="float: left; padding: 10px; width:39%;display:<?php if ($val->yard == 'yes') { ?> block;<?php } else { ?> none;<?php } ?>">Is service included</span>
                                            <div class="my-div is_yard_service_provide control_div" style=" display:<?php if ($val->yard == 'yes') { ?> block;<?php } else { ?> none;<?php } ?>">
                                                <p class="my-radio">  <input disabled type="radio" id="radio01_service_provided" name="yard_service_provided" value="yes" <?php if ($val->yard_service_provided == 'yes') { ?>checked<?php } ?>/>  <label for="radio01_service_provided">Yes</label></p>
                                                <p class="my-radio"> <input disabled type="radio" id="radio02_service_provided" name="yard_service_provided" value="no" <?php if ($val->yard_service_provided == 'no') { ?>checked<?php } ?>/> <label for="radio02_service_provided">No</label></p>
                                            </div>
                                            <div class="my-div is_yard_service_provide provide-last" style="display:<?php if ($val->yard == 'yes') { ?> block;<?php } else { ?> none;<?php } ?>">
                                                <input type="text" disabled="true" readonly="true" class="form-control my-txt-fleid width50" placeholder="Company" id="ysp<?= $val->prop_id ?>" name="yard_service_provider" value="<?= $val->yard_service_provider ?>"/>
                                                <input type="text" disabled="true" readonly="true" class="form-control my-txt-fleid width50 mr-rite0 phone_us"  id="yspno<?= $val->prop_id ?>" placeholder="Contact# " name="yard_service_provider_number" value="<?= $val->yard_service_provider_number != '' ? $val->yard_service_provider_number : '' ?>"/>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="my-panel   my-panel1 mr-btm0" id="panel-4">
                                        <div class="panel-head">
                                            <div class="panel-title">
                                                <h1>More</h1>
                                            </div>
                                        </div>
                                        <div class="panel-body">
                                            <div class="already-services<?= $val->prop_id ?>">
                                                <?php
                                                $property_service = $val->property_services;
                                                if (!empty($property_service)) {
                                                    foreach ($property_service as $key => $services) {
                                                        ?>
                                                        <div  class="form-group1 already-service-no-<?= $key + 1 ?>">
                                                            <div class="my-div">
                                                                <select  class="form-control my-txt-fleid services<?= $val->prop_id ?>" name="service[]" id="already-services-<?= $key ?>" disabled > 
                                                                    <option value="">Select Service</option>
                                                                    <?php foreach ($allServices as $value) { ?>
                                                                        <option value="<?= $value->id ?>" <?php if ($services->service_id == $value->id) { ?>selected<?php } ?>><?= $value->service ?></option>
                                                                    <?php } ?>
                                                                </select>
                                                            </div>	
                                                            <div class="my-div ">
                                                                <input readonly type="text" class="form-control my-txt-fleid width50 service_provider<?= $val->prop_id ?>" placeholder="Company" name="service_provider[]" value="<?= $services->company ?>"/>
                                                                <input readonly type="text" class="form-control my-txt-fleid width50 right-pull mr-rite0 phone_us service_provider_number<?= $val->prop_id ?>" placeholder="Contact#" name="service_provider_number[]" value="<?= $services->contact ?>"/>
                                                            </div>
                                                            <div class="my-div width150">
                                                                <a href="javascript:;" class="delete-service " id="delete-service<?= $key + 1 ?>" data-value="<?= $key + 1 ?>" style="margin-left:35px;display: none;"><img src="<?= $this->config->item('templateassets') ?>images/cross.png" alt=""/></a>
                                                            </div>	

                                                        </div>

                                                    <?php } ?>
                                                    <input type="hidden" name="already_services" class="already_services" value="<?= sizeof($property_service) ?>">
                                                <?php } ?>
                                            </div>

                                            <!--                                            <div  class="form-group1 new-services-0">
                                                                                            <div class="my-div">
                                                                                                <input type="text" class="form-control my-txt-fleid" placeholder=" Service" name="new-service[]" required/>
                                                                                            </div>
                                                                                            <div class="my-div ">
                                                                                                <input type="text" class="form-control my-txt-fleid width50" placeholder="Company" name="new-service_provider[]" required/>
                                                                                                <input type="text" class="form-control my-txt-fleid width50 right-pull mr-rite0" placeholder="Contact# " name="new-service_provider_number[]" required/>
                                                                                            </div>
                                                                                            <div class="my-div width150">
                                                                                                <a href="javascript:;" class="delete-row " id="cross0" data-value="0" style="margin-left:35px;"><img src="<?= $this->config->item('templateassets') ?>images/cross.png" alt=""/></a>
                                                                                            </div>
                                            
                                                                                        </div>-->
                                            <div class="services-panel<?= $val->prop_id ?>"></div>
                                            <div class="new-btn-part">

                                                <a href="javascript:;" class="edit-btn add-more-service btn-block" data-value="<?= $val->prop_id ?>" style="position:unset;width:235px;"><i class="fa fa-plus"></i>Select More Services</a>
                                                <a href="javascript:;" class="edit-btn add-new-service btn-block" data-value="<?= $val->prop_id ?>" style="position:unset;width:235px;"><i class="fa fa-plus"></i> Create New Service </a>
                                            </div>
                                            <input type="hidden" value="0" name="new_created_services" class="new_created_services">
                                        </div>
                                    </div>
                                </div>
                            </ul>
                            <a href="javascript:;" class="edit-btn mr-btm30 edit-amenties-servies" data-value="<?= $val->prop_id ?>"><span class="edit-icon"></span> Edit Property</a>
                            
                            <button class="edit-btn edit form_status " style="display:none;" data-value="<?= $val->prop_id ?>">Update</button>
                            <a href="javascript:;" class="edit-btn mr-btm30 cancel-amenties-servies" style="display:none" data-value="<?= $val->prop_id ?>"> Cancel</a>
                            <?php $cls = $val->occupied_status == 'occupied' ? 'alert_delete_property' : 'delete_property'; ?>
                            <a class="my-link <?php echo $cls; ?>" data-value="<?= $val->prop_id ?>" href="javascript:;">Delete</a>
                            <?php echo form_close(); ?>

                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>     
</section>



<div class="upload-photos-popup" style="display:none">
    <div class="popup-overlay">
        <div class="upp-inner">
            <div class="head-part">
                <h2></h2> <a href="javascript:void(0)" class="close_photo_viewr"> <img src="<?= $this->config->item('templateassets') ?>images/clsoe.png"/></a> <div class="clear"></div>
            </div>
            <div class="content-popup">
                <ul class="view_prop_photos">

                </ul>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).on("click", ".submit-form", function () {
        var id = $(this).attr("data-value");
        $('.services' + id).attr('disabled', false);
        $('.service_provider' + id).attr('readonly', false);
        $('.service_provider_number' + id).attr('readonly', false);
    });

    $(document).on("click", ".edit-amenties-servies", function () {
        var id = $(this).attr('data-value');
        $('.pick-up-day' + id).attr('disabled', false);
        $('.safety-equip' + id).attr('disabled', false);
        $('.services' + id).attr('disabled', false);
        $('.acfilter' + id).attr('disabled', false);
        $('.acfilter' + id).attr('readonly', false);
        $('.heatfilter' + id).attr('disabled', false);
        $('.heatfilter' + id).attr('readonly', false);
        $('#psp' + id + ',#ysp' + id + ',#yspno' + id).attr('disabled', false);
        $('#psp' + id + ',#ysp' + id + ',#yspno' + id).attr('readonly', false);
        $('#pspno' + id).attr('disabled', false);
        $('#pspno' + id).attr('readonly', false);
        $(':radio').attr('disabled', false);
        $('.service_provider' + id).attr('readonly', false);
        $('.service_provider' + id).attr('required', false);
        $('.service_provider_number' + id).attr('readonly', false);
        $('.service_provider_number' + id).attr('required', false);
        $(".form_status").addClass('submit-form');
        $(".submit-form").removeClass('form_status');
        $('.delete-service').show();
        $('.cancel-amenties-servies').show();
        $(this).hide();

        $(this).next().show();
    });

    $(document).on("click", ".cancel-amenties-servies", function () {
        var id = $(this).attr('data-value');
        $('.pick-up-day' + id).attr('disabled', true);
        $('.safety-equip' + id).attr('disabled', true);
        $('.services' + id).attr('disabled', true);
        $('.acfilter' + id).attr('disabled', true);
        $('.acfilter' + id).attr('readonly', true);
        $('.heatfilter' + id).attr('disabled', true);
        $('.heatfilter' + id).attr('readonly', false);
        $('#psp' + id + ',#ysp' + id + ',#yspno' + id).attr('disabled', true);
        $('#psp' + id + ',#ysp' + id + ',#yspno' + id).attr('readonly', true);
        $('#pspno' + id).attr('disabled', true);
        $('#pspno' + id).attr('readonly', true);
        $(':radio').attr('disabled', true);
        $('.service_provider' + id).attr('readonly', true);
        $('.service_provider' + id).attr('required', true);
        $('.service_provider_number' + id).attr('readonly', true);
        $('.service_provider_number' + id).attr('required', true);
        $(".form_status").removeClass('submit-form');
        $(".submit-form").addClass('form_status');
        $('.delete-service').hide();
        $(this).hide();
        $(this).prev().hide();
        $(this).prev().prev().show();
    });

function property_custom_js(id){
    //var id = $(this).attr('data-value');
    alert(id);
        $('.pick-up-day' + id).attr('disabled', true);
        $('.safety-equip' + id).attr('disabled', true);
        $('.services' + id).attr('disabled', true);
        $('.acfilter' + id).attr('disabled', true);
        $('.acfilter' + id).attr('readonly', true);
        $('.heatfilter' + id).attr('disabled', true);
        $('.heatfilter' + id).attr('readonly', false);
        $('#psp' + id + ',#ysp' + id + ',#yspno' + id).attr('disabled', true);
        $('#psp' + id + ',#ysp' + id + ',#yspno' + id).attr('readonly', true);
        $('#pspno' + id).attr('disabled', true);
        $('#pspno' + id).attr('readonly', true);
        $(':radio').attr('disabled', true);
        $('.service_provider' + id).attr('readonly', true);
        $('.service_provider' + id).attr('required', true);
        $('.service_provider_number' + id).attr('readonly', true);
        $('.service_provider_number' + id).attr('required', true);
        $(".form_status").removeClass('submit-form');
        $(".submit-form").addClass('form_status');
        $('.delete-service').hide();
        $(this).hide();
        $(this).prev().hide();
        $(this).prev().prev().show();
}

    $(".form_status").click(function () {

        if ($(this).hasClass('form_status')) {
            return false;
        } else {
            $('#wait-div').show();
        }
    });

    $(document).on("click", ".equipments", function () {
        var id = $(this).attr('data-value');
        $('.safety_equipment' + id).attr('disabled', false);
    });
    $(document).on("click", ".all-tabs-panel", function () {
        var id = $(this).attr('data-value');
        $('.all-tabs-panel-li').removeClass('active');
        $(this).parent().addClass('active');
    });
    $(document).on('click', '.add-new-service', function (event) {
        var id = $(this).attr('data-value');
        var assets = '<?= $this->config->item('templateassets') ?>';
        var new_created_services = $('.new_created_services').val();
        new_created_services = parseInt(new_created_services) + 1;

        var dataToAppend = '<div class="form-group1 new-services-' + new_created_services + '">';
        dataToAppend += '<div class="my-div">';
        dataToAppend += '<li>';
        dataToAppend += '<input type="text" class="form-control my-txt-fleid" placeholder="Service" name="new-service[]" /></li></div>';
        dataToAppend += '<div class="my-div">';
        dataToAppend += '<li>';
        dataToAppend += '<input type="text" class="form-control width50" placeholder="Company" name="new-service_provider[]" />';
        dataToAppend += '<input type="text" class="form-control width50 right-pull mr-rite0 phone_us" placeholder="Contact#" name="new-service_provider_number[]" />';
        dataToAppend += '</li></div>';


        dataToAppend += '<div class="my-div width150"><li"><a href="javascript:;" class="delete-row " id="cross' + new_created_services + '" data-value="' + new_created_services + '" style="margin-left:35px;"><img src="' + assets + 'images/cross.png" alt=""/></a></li></div></div>';

        $('.services-panel' + id).append(dataToAppend);
        $('.new_created_services').val(new_created_services);
    });

    $('.add-more-service').on("click", function () {
        var id = $(this).attr('data-value');
        var assets = '<?= $this->config->item('templateassets') ?>';
        var already_services = $('.already_services').val();
        already_services = parseInt(already_services) + 1;
        $('.already_services').val(already_services);
        var allService = '<?= json_encode($allServices) ?>';
        allService = JSON.parse(allService);
        var dataToAppend = '';

        dataToAppend += '<div  class="form-group1 already-service-no-' + already_services + '">';
        dataToAppend += '<div class="my-div">';
        dataToAppend += '<select id="already-services-' + already_services + '" class="form-control my-txt-fleid" name="service[]" >';
        dataToAppend += '<option value="">Select Service</option>';
        $.each(allService, function (key, value) {
            dataToAppend += '<option value="' + value.id + '">' + value.service + '</option>';
        });
        dataToAppend += '</select></div>	';
        dataToAppend += '<div class="my-div ">';
        dataToAppend += '<input type="text" class="form-control my-txt-fleid width50" placeholder="Company" name="service_provider[]" />';
        dataToAppend += '<input type="text" class="form-control my-txt-fleid width50 right-pull mr-rite0 phone_us" placeholder="Contact#" name="service_provider_number[]" />';
        dataToAppend += '</div>';
        dataToAppend += '<div class="my-div width150 "><a href="javascript:;" class="delete-service " id="delete-service' + already_services + '" data-value="' + already_services + '" style="margin-left:35px;"><img src="' + assets + 'images/cross.png" alt=""/></a></div></div>';

        $('.already-services' + id).append(dataToAppend);
    });

    $(document).on('click', '.delete-row', function () {
        var id = $(this).attr('data-value');
        $('#cross' + id).remove();
        $('.new-services-' + id).remove();
        var new_created_services = $('.new_created_services').val();
        new_created_services = parseInt(new_created_services) - 1;
        $('.new_created_services').val(new_created_services);
    });

    $(document).on('click', '.delete-service', function () {
        var id = $(this).attr('data-value');
        $('#delete-service' + id).remove();
        $('.already-service-no-' + id).remove();
        var already_services = $('.already_services').val();
        already_services = parseInt(already_services) - 1;
        $('.already_services').val(already_services);
    });

    function show_service_provided(value)
    {
        if (value == 'yes')
        {
            $('.is_yard_service_provide').show();
            $('.is_yard_service_provide span').show();
        }
        if (value == 'no')
        {
            $('.is_yard_service_provide').hide();
            $('.is_yard_service_provide span').hide();
        }
    }

    $("input:radio[name='pest_control_home']").click(function () {
        var value = $(this).val();
        if (value == 'yes')
            $('.service-provider').show();
        if (value == 'no')
            $('.service-provider').hide();
    });

    $("select[name='heat_type']").change(function () {
        if ($(this).val() != 'N/A') {
            $("select[name='heat_filter_size']").show();
        } else {
            $("select[name='heat_filter_size']").hide();
        }
    });

    $("select[name='ac_type']").change(function () {
        if ($(this).val() != 'N/A') {
            $("select[name='ac_filter_size']").show();
        } else {
            $("select[name='ac_filter_size']").hide();
        }
    });

    function show_loader() {
        $('#wait-div').show();
    }
</script>
<script>
    $(document).ready(function () {
        $(".comm_docs").change(function () {
            var path = $(this).val();
            var filename = path.replace(/^.*\\/, "");
            var html = " <a class='btn btn-edit btn-primary clr_up_img del_comm_doc'  style='float:none; position:static;' href='javascript:;'> Delete</a>";

            var reader = new FileReader();
            reader.onload = function(){
                var dataURL = reader.result;
                html_view = '<a href="'+ dataURL +'" target="_blank" class="btn preview_doc" id="preview_doc"> View </a> ';
                $(".show_file_name2").html(filename + ' ' + html_view + html );            
            };
            reader.readAsDataURL(this.files[0]);

            //$(".show_file_name2").html(html);
            $(".show_file_name2").show();
        });


        $(".close_photo_viewr").click(function () {
            $('.upload-photos-popup').hide();
            $(".view_prop_photos").html('');
        });
        //   $(".view_photos").colorbox({photo:true,rel:'gal'});

        $(".view_photos").click(function () {
            var prop_id = $(this).attr('id');
            $.ajax({
                url: '<?php echo base_url(); ?>property/get_photos/' + prop_id,
                type: 'POST',
                data: {prop_id: prop_id},
                beforeSend: function () {
                    $('#wait-div').show();
                },
                success: function (data) {
                    $('#wait-div').hide();
                    //  $('.upload-photos-popup').show()
                    $('.view_prop_photos').html(data);
                },
                error: function () {
                    $('#wait-div').hide();
                    swal("error in form submission");
                }
            });
        });

        $(".prop_photo").change(function () {
            var id = $(this).attr('id');
            $("#pform-" + id).submit();
        });

        $(document).delegate('.country_id', 'change', function () {
            //$(".country_id").change(function () {
            var countryId = $(this).val();
            var pid = $(this).attr('id');
            console.log(pid);
            if (countryId != 0) {
                $.ajax({
                    url: '<?php echo base_url(); ?>property/get_state',
                    type: 'POST',
                    data: {countryId: countryId},
                    beforeSend: function () {
                        $('#wait-div').show();
                    },
                    success: function (data) {
                        var dataArray = jQuery.parseJSON(data);
                        $("#state_" + pid).html('<option value="0">Plese select</option>');
                        $.each(dataArray, function (idx, rec) {

                            $("#state_" + pid).append('<option value="' + rec.region_id + '">' + rec.region_name + '</option>');
                        })

                        $('#wait-div').hide();

                    },
                    error: function () {
                        $('#wait-div').hide();
                        swal("error in form submission");
                    }
                });
            }
        });

        $(document).on('click', '.is_community', function (event) {
            var value = $(this).val();
            var pid = $(this).attr('resp');
            if (value == 'Yes')
                $('.community_box' + pid).show();
            if (value == 'No')
                $('.community_box' + pid).hide();
        });

        $(document).delegate('.edt_propert_i', 'click', function () {
            var pid = $(this).attr('id');
            $("#edit-cnt-" + pid).show();
        });

        $(".cncl_edit").click(function (e) {
            e.preventDefault();
            var pid = $(this).attr('data-value');
            $("#edit-cnt-" + pid).hide();
        });

        $("select[name='heat_type']").change(function () {
            if ($(this).val() != 'N/A') {
                $("select[name='heat_filter_size']").show();

            } else {
                $("select[name='heat_filter_size']").hide();
            }
        });

        $("select[name='heat_filter_size']").change(function () {
            if ($(this).val() == 'Other') {
                $("input[name='heat_filter_size_other']").show();

            } else {
                $("input[name='heat_filter_size_other']").val('');
                $("input[name='heat_filter_size_other']").hide();
            }
        });

        $("select[name='ac_filter_size']").change(function () {
            if ($(this).val() == 'Other') {
                $("input[name='ac_filter_size_other']").show();

            } else {
                $("input[name='ac_filter_size_other']").val('');
                $("input[name='ac_filter_size_other']").hide();
            }
        });

        $("select[name='ac_type']").change(function () {
            if ($(this).val() != 'N/A') {
                $("select[name='ac_filter_size']").show();
            } else {
                $("select[name='ac_filter_size']").hide();
            }
        });

        $(document).delegate('.delete_property,.alert_delete_property', 'click', function () {
            var id = $(this).attr('data-value');
            var alert_message = '';
            if ($(this).hasClass('alert_delete_property')) {
                alert_message = 'All the information related to this property including tenants info will be deleted. Would you like to proceed?';
            } else {
                alert_message = 'Are you sure to delete this property?';
            }
            swal({
                title: "Are you sure?",
                text: alert_message,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            },
            function () {
                $.ajax({
                    url: '<?php echo base_url('property/delete_property'); ?>',
                    type: 'POST',
                    data: {id: id},
                    beforeSend: function () {
                        $('#wait-div').show();
                    },
                    success: function (data) {
                        $("#wait-div").hide();
                        var myArray = JSON.parse(data);
                        if (myArray.success) {
                            swal({
                                title: "Success",
                                text: 'Property Deleted Successfully',
                                type: "success"
                            },
                            function () {
                                location.reload();
                            });
                        } else {
                            swal("Error!", myArray.error_message, "error");

                        }
                    },
                    error: function () {
                        $("#wait-div").hide();
                        swal("Error!", "Server not responding.", "error");
                    }
                });
            });
        });


        $(document).delegate('.del_photo', 'click', function () {
            var id = $(this).attr('id');
            var target = $(this).parent();
            swal({
                title: "Are you sure?",
                text: 'Are you sure to delete this photo',
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            },
            function () {
                $.ajax({
                    url: '<?php echo base_url('property/delete_photo'); ?>',
                    type: 'POST',
                    data: {id: id},
                    beforeSend: function () {
                        $('#wait-div').show();
                    },
                    success: function (data) {
                        $("#wait-div").hide();
                        var myArray = JSON.parse(data);
                        if (data == 1) {
                            swal({
                                title: "Success",
                                text: 'Photo Deleted Successfully',
                                type: "success"
                            },
                            function () {
                                target.remove();
                            });
                        } else {
                            swal("Error!", myArray.error_message, "error");

                        }
                    },
                    error: function () {
                        $("#wait-div").hide();
                        swal("Error!", "Server not responding.", "error");
                    }
                });
            });
        });
        
          $(document).delegate('.del_comm_doc', 'click', function () {
            var id = $(this).attr('id');
            var target = $(this).parent();
            swal({
                title: "Are you sure?",
                text: 'Are you sure to delete',
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            },
            function () {
                $.ajax({
                    url: '<?php echo base_url('property/delete_community_doc'); ?>',
                    type: 'POST',
                    data: {id: id},
                    beforeSend: function () {
                        $('#wait-div').show();
                    },
                    success: function (data) {
                        $("#wait-div").hide();
                            $(".comm_docs").val('');
                            $(".show_file_name2").html('');
                            $(".show_file_name2").hide();
                        
                        if (data == 1) {
                            swal({
                                title: "Success",
                                text: 'Deleted Successfully',
                                type: "success"
                            },
                            function () {
                               // target.remove();
                            });
                        } else {
                            swal("Error!", myArray.error_message, "error");

                        }
                    },
                    error: function () {
                        $("#wait-div").hide();
                        swal("Error!", "Server not responding.", "error");
                    }
                });
            });
        });
        //del_comm_doc
        
    });

    
</script>
<style>
    a.edit-btn {
        border: 1px solid;
        border-radius: 3px;
        color: #ffffff;
        font-family: "robotobold";
        font-size: 14px;
        height: 40px;
        padding: 10px 20px 10px 35px;
        position: relative;
        text-align: center;
        text-transform: uppercase;
        width: auto;
    }

    .add-btn {
        background: #00659f none repeat scroll 0 0;
        border: 1px solid #005586;
        border-radius: 3px;
        color: #ffffff;
        display: inline-block;
        font-family: "robotobold";
        font-size: 16px;
        height: 40px;
        padding: 10px;
        position: absolute;
        right: 0;
        text-align: center;
        text-transform: uppercase;

        width: 209px;
    }

    .li-div{
        float: left;
        margin-bottom: 20px;
        padding-right: 20px;
        width: 340px;
    }
    .my-thumbs-list{
        overflow: auto;
        width: 100%;;
        height: auto;
    }
    .my-thumbs-list{ background-color: transparent; }
    .my-thumbs-list .mTSButton{
        background-color: rgba(0,0,0,.7);
        -moz-border-radius: 48px; -webkit-border-radius: 48px; border-radius: 48px;
    }
    .my-thumbs-list .mTSButtonLeft{ left: 5px; }
    .my-thumbs-list .mTSButtonRight{ right: 5px; }
</style>	
<link href="<?= $this->config->item('templateassets') ?>css/jquery.mThumbnailScroller.css" rel="stylesheet">
<script type="text/javascript" src="<?= $this->config->item('templateassets') ?>js/jquery.mThumbnailScroller.js"></script>  
<script>
    (function ($) {
        $(window).load(function () {
            $(".my-thumbs-list").mThumbnailScroller({
                axis: "x", type: "click-25",
            });
        });
    })(jQuery);
</script>