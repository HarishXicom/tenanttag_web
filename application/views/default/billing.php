<style>
    .my-list span {
        width: 33% !important
    }
    ;
</style>
<!-- <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.5/css/bootstrap-dialog.min.css" rel="stylesheet" type="text/css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.5/js/bootstrap-dialog.min.js"></script> -->

<link href="<?= $this->config->item('templateassets') ?>css/bootstrap.min.css" rel="stylesheet">
<link href="<?= $this->config->item('templateassets') ?>css/bootstrap-dialog.min.css" rel="stylesheet">
<script type="text/javascript" src="<?= $this->config->item('templateassets') ?>js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?= $this->config->item('templateassets') ?>js/bootstrap-dialog.min.js"></script>


<?php echo validation_errors(); ?>
<section class="lease-part">
    <div class="container">
        <div id="tabs">
            <div class="my-tab1">
                <ul class="my-tab">
                    <li>
                        <a href="#tabs-2">Payment History</a>
                    </li>
                    <li>
                        <a href="#tabs-1">Subscribed Properties</a>
                    </li>
                    <!-- <li style="display:none">
                        <a href="#tabs-3">Subscription Billing</a>
                    </li> -->
                </ul>
            </div>
            <div class="tab-area">
                <div class="ajax_report alert display-hide" role="alert" style="margin-bottom: 10px; margin-left: 0px; width: 500px; position: relative; top: 100px;">
                    <span class="close-message"></span>
                    <div class="ajax_message"></div>
                </div>
                <br>

                <div id="tabs-2">
                    <div class="tenant">
                        <ul class="tenant2">
                            <li><span class="my-span">Payment History</span>
                                <ul class="btn-group">
                                    <li>
                                        <!-- <a href="javascript:void(0);" class="sky-blue-col editbinfo">Edit</a> -->
                                    </li>
                                </ul>
                            </li>
                        </ul>
                        <div class="checkout_accordin_container wc_checkout_accordin_container " style="margin-top:100px">
                            <select class="form-control my-txt-fleid" style="width:320px" onchange="this.options[this.selectedIndex].value && (window.location = this.options[this.selectedIndex].value);">
                                <option value="<?= site_url("account/billing") ?>/all#tabs-2">All Transactions</option>
                                <?php foreach ($tenantList as $key=> $value) { ?>
                                    <option <?php echo ($current_tenant == getSlugById($value['tenant_id'])) ? 'selected' : ''; ?> value="<?= site_url("account/billing") ?>/<?php echo getSlugById($value['tenant_id']); ?>#tabs-2"><?php echo getUserName($value['tenant_id']); ?></option>
                                    
                                <?php } ?>
                            </select>
                            <a class="" style="font-size: 15px;display:inline-table;margin-bottom: 3%;margin-top: 19px;text-decoration: underline; width: 5px;" href="<?php echo base_url() ?>account/billing#tabs-2" name="clear">Clear</a>
                            
                            <p  style="width:214px"  class="my-check-custom same_address_checkbox sac">   
                                <input value="1" onclick="return yousendit();" <?php echo ($current_tenant == $landlord_info->slug) ? 'checked' : ''; ?> name="subscriber_landlord" class="same_address" id="test2" type="checkbox">  
                                <label for="test2" style="font-size:14px;top:-22px">
                                    Subscription Payments   
                                </label>
                            </p>
                            <?php $landlord_url = site_url("account/billing").'/'.$landlord_info->slug.'#tabs-2';?>
                            <?php $tenants_url = site_url("account/billing").'/all#tabs-2';?>
                            <script>
                            function yousendit(){
                                if(document.getElementById('test2').checked){
                                    window.location='<?php echo $landlord_url; ?>';
                                }else{
                                    window.location='<?php echo $tenants_url; ?>';
                                    return false;
                                }
                                return true;

                            }
                            </script>
                            <div class="transaction-list" id="transactionList">
                                <a href="javascript:;" data-toggle="modal" data-target="#how_do_ach_payment_work">How do ACH Payments work?</a>
                                <p>* Rent payments will deposit into Landlord account 4 business day after Date.</p>
                                <table class="trans_table" border="1" style="width:100%">
                                    <tr>
                                        <th> Transaction Id</th>
                                        <th> User Name</th>
                                        <th> Property</th>
                                        <th> Transaction Amount</th>
                                        <th> Transaction Type *</th>
                                        <th> Status </th>
                                        <th> Date *</th>
                                    </tr>

                                    <?php if(!empty($transactions)) { ?>
                                        <?php foreach ($transactions as $key=> $value) { ?>
                                            <tr class="list-item">
                                                <td>
                                                    <?php echo $value[ 'transaction_id']; ?>
                                                </td>
                                                <td>
                                                    <?php echo getUserName($value[ 'mem_id']); ?>
                                                </td>
                                                <td>
                                                    <?php 
                                                        $pr_array = explode( ',',$value[ 'property_id']); 
                                                        foreach ($pr_array as $pr_array_key=> $pr_array_value) {
                                                            echo getPropertyAddress($value['property_id']).'
                                                            <br> '; 
                                                        } ?>
                                                </td>
                                                <td>
                                                    <?php echo amount_format_view($value[ 'amount']); ?>
                                                </td>
                                                <td>
                                                    <?php echo ($value[ 'user_type']=='Tenant' ) ? 'Rent' : 'Subscription'; ?> </td>
                                                <td>
                                                    <?php echo $value[ 'status']; ?>
                                                </td>
                                                <td>
                                                    <?php echo $value[ 'created_date']; ?>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    <?php }else{ ?> 
                                            <tr class="list-item">
                                                <td colspan="5">
                                                    No record found
                                                </td>
                                            </tr>
                                    <?php } ?>
                                    <div class="clearfix"></div>
                                </table>
                                <?php echo $this->ajax_pagination->create_links(); ?>
                            </div>
                            <div class="loading" style="display: none;">
                                <div class="content"><img src="<?php echo base_url().'assets/images/loading.gif'; ?>" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="tabs-1">
                    <div class="tenant">
                        <ul class="tenant2">
                            <li><span class="my-span">Subscribed Properties</span>
                                <ul class="btn-group">
                                    <li>
                                        <!-- <a href="javascript:void(0);" class="sky-blue-col editbinfo">Edit</a> -->
                                    </li>
                                </ul>
                            </li>
                        </ul>
                        <?php foreach ($stripe_customer_card as $key=> $value) { ?>
                            <?php //echo "<pre>"; print_r($stripe_customer_card); exit ?>
                            <div class="checkout_accordin_container wc_checkout_accordin_container ">
                                <form action="<?php echo base_url('account/deleteStripeCard'); ?>" method="POST" id="delete-form_<?= $value->id ?>">
                                    <div class="ajax_report alert display-hide custom_ajax_report" role="alert" style="margin-bottom: 10px; margin-left: 0px; width: 500px; position: relative; ">
                                        <span class="close-message"></span>
                                        <div class="ajax_message"></div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="saved_deb_card">
                                        <span style="color:#00659F"> Property: <?= $value->property_city ?>, <?= $value->property_address1 ?> <?= $value->property_address2 ?></span>
                                        <br>
                                        <div class="saved_deb_card_right">
                                            <!-- <button type="button" id="<?= $value->id ?>" class="btn-tenant delete_saved_card font-del" style="width:auto;margin-left: 0px; text-align: center; float:none; display:inline-block;text-decoration:none"> <i class="fa fa-trash-o" aria-hidden="true"></i></button> -->
                                           <!-- <button type="button" id="<?= $value->id ?>" class="btn-tenant delete_saved_card font-del" style="width:auto;margin-left: 0px; text-align: center; float:none; display:inline-block;text-decoration:none"> <img src="<?= $this->config->item('templateassets') ?>images/cross.png" alt="" />
                                            -->
                                        </div>
                                        <div class="saved_deb_card_left" style="margin-left:0px;">
                                            <span style="color:#00659F" class="acc-det">  XXXX - XXXX - XXXX - <?php echo $value->last_digits; ?></span>
                                            <span class="bs-Badge brandd"><?php echo $value->brand; ?></span>
                                            <span class="bs-Badge fundingg"><?php echo $value->funding; ?></span>
                                            <a href="javascript:void()" id="<?php echo $value->id; ?>" sky-blue-col editminfoclass="btn-tenant btn checkoutmodalclass btn-lg" data-toggle="modal" data-target="#checkoutModal"><i class="fa fa-pencil-square-o edt_propert_i" id="207"></i></a>
                                            <br>
                                            <input type="hidden" id="deleteStripeCard1" name="deleteStripeCard" value="<?= $value->id ?>">
                                            <input type="hidden" name="property_id" value="<?= $value->property_id ?>">
                                        </div>
                                        <?php if($value->recurring == 0){ ?>
                                            <span style="display:none" class="rec_span"><input type="checkbox"  class="recurring_status_change" value="<?= $value->id ?>" name="recurring"> <label>Recurring</label></span>
                                        <?php }else{ ?>
                                            <span style="display:none" class="rec_span"><input type="checkbox"  class="recurring_status_change" value="<?= $value->id ?>" checked name="recurring"> <label>Recurring</label></span>
                                        <?php } ?>
                                        <div class="clearfix"></div>
                                        <br>
                                        <?php if($value->status_by_landlord != 1){ ?>
                                            <!-- <div class="debit_card_status_p" style="float: left;padding-top: 10px"> -->
                                            <!-- <input type="checkbox" checked="checked" class="debit_card_status_text debit_card_deact_status" name="<?= $card_info->id ?>" id="debit_card_status">
                                                                      <label for="debit_card_status" class="debit_card_status_label">Deactivate Account</label> -->
                                            <input type="button" style="opacity:1;float:left" class="debit_card_status_text debit_card_act_status_subscribed btn-tenant" name="<?= $value->id ?>" value="Reactivate Account" id="debit_card_status">
                                            <!-- </div> -->
                                        <?php }else{ ?>
                                            <!-- <div class="debit_card_status_p"  style="float: left;padding-top: 10px"> -->
                                            <input type="button" style="opacity:1;float:left" class="debit_card_status_text debit_card_deact_status_subscribed btn-tenant" name="<?= $value->id ?>" value="Deactivate Account" id="debit_card_status">
                                            <!-- <input type="checkbox"  class="debit_card_status_text debit_card_act_status" name="<?= $card_info->id ?>" id="debit_card_status">
                                                                      <label for="debit_card_status" class="debit_card_status_label">Reactivate Account</label> -->
                                            <!-- </div>   -->
                                        <?php } ?>
                                        
                                        <div class="clearfix"></div>
                                        <br>
                                            <?php 
                                        if ( checkTrialTimePeriod($this->session->userdata('MEM_ID')) == false) {
                                            if(isset($value->subscription_info->last_subscription_date)){ ?>
                                                <span class="">Last subscription date: <?php  echo $value->subscription_info->last_subscription_date; ?></span>
                                                <span class="" style="float:right">Subscription renewal date: <?php  echo $value->subscription_info->next_subscription_date; ?></span>
                                                <?php 
                                            } 
                                        } else{ ?>
                                            <span class="" style="float:right">Subscription renewal date: <?php  echo checkTrialTimePeriod($this->session->userdata('MEM_ID')); ?></span>
                                            <?php 
                                        } ?>
                                    </div>
                                </form>

                            </div>
                            <div class="clearfix"></div>
                        <?php } ?>

                    </div>
                </div>

                

                <div id="tabs-3" style="display:none">
                    <div class="tenant">
                        <ul class="tenant1">
                            <li><span class="my-span">Subscription Billing</span>
                                <ul class="btn-group">
                                    <li>
                                        <!-- <a href="javascript:void(0);" class="sky-blue-col editbinfo">Edit</a> -->
                                    </li>
                                </ul>
                            </li>
                        </ul>

                        <?php if(!empty($card_info)) { ?>
                        <div class="tenant">
                            <form action="<?php echo base_url('account/deleteStripeCard'); ?>" method="POST" id="delete-form_<?= $card_info->id ?>">
                                <div class="saved_deb_card">
                                    <?php //echo '<pre>'; print_r($card_info); ?>
                                    <div class="saved_deb_card_left">
                                        <span class="acc-det" style="color:#00659F">   <?php echo 'XXXX - XXXX - XXXX - '.$card_info->last_digits; ?> </span>
                                        <span class="bs-Badge brandd"><?php echo $card_info->brand; ?></span>
                                        <span class="bs-Badge fundingg"><?php echo $card_info->funding; ?></span>
                                        <!-- <p><input type="checkbox"><label>Active </label> </p> -->
                                        <?php if($card_info->status != 'unverified'){ ?>
                                        <button type="button" id="<?= $card_info->id ?>" class="btn-tenant delete_saved_external_card font-del" style="width:auto;margin-left: 0px;text-align: center; float:none; display:inline-block;text-decoration:none"> <i class="fa fa-trash-o" aria-hidden="true"></i>
                                        </button>
                                        <input type="hidden" name="deleteStripeCard" value="<?= $card_info->id ?>">
                                        <br>
                                        <br>
                                        <?php if($card_info->status_by_landlord != 1){ ?>
                                        <!-- <div class="debit_card_status_p" style="float: left;padding-top: 10px"> -->
                                        <!-- <input type="checkbox" checked="checked" class="debit_card_status_text debit_card_deact_status" name="<?= $card_info->id ?>" id="debit_card_status">
                                                                    <label for="debit_card_status" class="debit_card_status_label">Deactivate Account</label> -->
                                        <input type="button" style="opacity:1;float:left" class="debit_card_status_text debit_card_act_status btn-tenant" name="<?= $card_info->id ?>" value="Reactivate Account" id="debit_card_status">
                                        <!-- </div> -->
                                        <?php }else{ ?>
                                        <!-- <div class="debit_card_status_p"  style="float: left;padding-top: 10px"> -->
                                        <input type="button" style="opacity:1;float:left" class="debit_card_status_text debit_card_deact_status btn-tenant" name="<?= $card_info->id ?>" value="Deactivate Account" id="debit_card_status">
                                        <!-- <input type="checkbox"  class="debit_card_status_text debit_card_act_status" name="<?= $card_info->id ?>" id="debit_card_status">
                                                                    <label for="debit_card_status" class="debit_card_status_label">Reactivate Account</label> -->
                                        <!-- </div>   -->
                                        <?php } ?>
                                        <?php } ?>
                                    </div>
                                    <div class="saved_deb_card_right">
                                        <?php if($card_info->status == 'unverified'){ ?>
                                        <!--<button type="button" data-toggle="modal" data-target="#varification_model" class="btn-tenant" style="color:#ffffff;width:auto;margin-left: 0px; padding: 10px 13px 10px 20px;text-align: center; float:none; display:inline-block;text-decoration:none"> Verify Account</button>
                                        <br>
                                        <br>
                                        <button type="button" id="<?= $card_info->id ?>" class="btn-tenant delete_saved_external_card font-del" style="float:right;width:auto;margin-left: 0px;text-align: center;  display:inline-block;text-decoration:none"> <i class="fa fa-trash-o" aria-hidden="true"></i>
                                            <input type="hidden" name="deleteStripeCard" value="<?= $card_info->id ?>">
                                        </button>-->
                                        <!-- <span class="bs-Badge stst"><?php //echo $card_info->status; ?></span> -->
                                        <?php }else{ ?>


                                        <?php } ?>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <?php } ?>
                        <div class="tenant">
                            <form style=" margin-top: 20px;" action="<?php echo base_url('account/update_stripe_billing_information'); ?>" method="POST" id="payment-form" onsubmit="return onSubmitDo()">
                                <div class="tpay-acc-setup" style="  margin: 22px 0;">
                                    <div class="shopping-cart-sec">
                                        <?php //echo "<pre>"; print_r($memberRecord); ?>
                                        <h4 style="color:#00659F;  font-family:robotobold;  font-size:16px;  text-align:left;padding-left: 28px;"> Add New Debit Card( Automatically replace the old one if exist)</h4>
                                        <div class="cart-form ch-form">
                                            <span style='color: red' class="error" id='payment-error1'></span>
                                            <div class="cart-form-divide cart-form-section">
                                                <input type="hidden" name="amount" />
                                                <p>
                                                    <label>First Name</label>
                                                    <input type="text" value="" data-stripe="name" id="name" name="name" placeholder="" />
                                                </p>
                                                <p class="last-name">
                                                    <label>Last Name</label>
                                                    <input type="text" value="" name="last_name" id="last_name" placeholder="" />
                                                </p>
                                            </div>

                                            <div class="clearfix"></div>
                                            <div class="cart-form-section cart-form-sectionfour">
                                                <p class="ccc">
                                                    <label>Debit Card Number.</label>
                                                    <input type="text" name="" class="credit" style="width:49%;" data-stripe="number" id="cc" placeholder="" />
                                                </p>
                                                <p class="exp-date">
                                                    <label>Exp. date</label>
                                                    <input type="text" readonly name="month_year" id="month_year" class="month_year" style="" placeholder="MM/YY " />
                                                </p>
                                                <input type="hidden" id="exp_month" id="exp_month" class="exp_month" style="" data-stripe="exp_month" />
                                                <input type="hidden" id="exp_year" name="exp_year"  class="exp_year" style="" data-stripe="exp_year" />
                                                <p class="security-code">
                                                    <label>Security Code - CVC</label>
                                                    <input type="text" class="reset" data-stripe="cvc" id="cvc" placeholder="">
                                                </p>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="cart-form-section cart-form-sectionfour cart-form-sectionfour2">
                                                <p class="add">
                                                    <label>Address</label>
                                                    <input type="text" value="<?php //echo $memberRecord->address; ?>" data-stripe="address_line1" name="address_line1" id="address_line1" placeholder="" />
                                                </p>
                                                <p class="city">
                                                    <label>City</label>
                                                    <input type="text" value="<?php // echo $memberRecord->city_id; ?>" name="address_city" style="width:21%;" data-stripe="address_city" id="address_city" placeholder="" />
                                                </p>
                                                <p class="state">
                                                    <label>State</label>
                                                    <input type="text" value="<?php //echo get_state_name($memberRecord->state_id); ?>" name="address_state" style="width:23.5%;" id="address_state" data-stripe="address_state" placeholder="" />
                                                </p>
                                                <p class="zip">
                                                    <label>Zip</label>
                                                    <input type="text" value="" class="reset" name="address_zip" data-stripe="address_zip" style="width:23.5%;" id="address_zip" placeholder="" />
                                                </p>
                                            </div>
                                            <div class="clearfix"></div>

                                            <div class="cart-btns">

                                                <input type="hidden" value="<?php echo $memberRecord->email; ?>" name="emailAddress">
                                                <input type="hidden" data-stripe="currency" value="usd" name="currency">
                                                <input type="submit" class="checkout_confirm_box" name="save_billing" value="Save" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>



            </div>
        </div>
    </div>
</section>
<!-- Modal -->

<div id="checkoutModal" class="modal  ch-form fade" role="dialog">
	<div class="modal-dialog" style="width: 900px;">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Update Card</h4>
			</div>
			<div class="modal-body">
                <div class="ch-form">
                	<form action="<?php echo base_url('property/updateCard'); ?>" method="POST" id="card-form" onsubmit="return onSubmitDoCard()">
                        <span style='color: red' class="error" id='payment-error1'></span>
                        <div class="ajax_report custom_ajax_message alert display-hide" role="alert" style="margin-bottom: 10px; margin-left: 0px; width: 500px; position: relative; top: 100px;">
                            <span class="close-message"></span>
                            <div class="ajax_message"></div>
                        </div>
                        <div class="cart-form-divide cart-form-section">
                            <input class="property_card_id" type="hidden" name="property_card_id" id="property_card_id" value="">
                            <p>
                                <label>First Name</label>
                                <input type="text" value="<?php echo $memberRecord->first_name; ?>" data-stripe="name" id="name" name="name" placeholder="" />
                            </p>
                            <p class="last-name">
                                <label>Last Name</label>
                                <input type="text" value="<?php echo $memberRecord->last_name; ?>" name="last_name" id="last_name" placeholder="" />
                            </p>
                        </div>

                        <div class="clearfix"></div>
                        <div class="cart-form-section cart-form-sectionfour">
                            <p class="ccc">
                                <label>CC</label>
                                <input type="text" name="" class="credit" style="width:49%;" data-stripe="number" id="cc" placeholder="" />
                            </p>
                            <p class="exp-date">
                                <label>Exp. date</label>
                                <input type="text" readonly name="month_year" id="month_year"  class="month_year"  style="" placeholder="MM/YY " />
                            </p>
                            <input type="hidden" id="exp_month" name="exp_month" style="" class="exp_month" data-stripe="exp_month" />
                            <input type="hidden" id="exp_year" name="exp_year" style=""class="exp_year"  data-stripe="exp_year" />
                            <p class="security-code">
                                <label>Security Code - CVC</label>
                                <input type="text" class="reset" data-stripe="cvc" id="cvc" placeholder="">
                            </p>
                        </div>
                        <div class="clearfix"></div>
                        <div class="cart-form-section cart-form-sectionfour cart-form-sectionfour2">
                            <p class="add">
                                <label>Address</label>
                                <input type="text" value="<?php //echo $memberRecord->address; ?>" data-stripe="address_line1" name="address_line1" id="address_line1" placeholder="" />
                            </p>
                            <p class="city">
                                <label>City</label>
                                <input type="text" value="<?php // echo $memberRecord->city_id; ?>" name="address_city" style="width:21%;" data-stripe="address_city" id="address_city" placeholder="" />
                            </p>
                            <p class="state">
                                <label>State</label>
                                <input type="text" value="<?php //echo get_state_name($memberRecord->state_id); ?>" name="address_state" style="width:23.5%;" id="address_state" data-stripe="address_state" placeholder="" />
                            </p>
                            <p class="zip">
                                <label>Zip</label>
                                <input type="text" value="" class="reset" name="address_zip" data-stripe="address_zip" style="width:23.5%;" id="address_zip" placeholder="" />
                            </p>
                        </div>
                        <div class="clearfix"></div>
                        <div class="cart-form-divide cart-form-section">
                            <p style="display:none">
                                <input type="checkbox" checked name="recurring_0" value="recurring">
                                <input type="hidden" value="<?php echo $memberRecord->email; ?>" name="emailAddress">
                                <label style="padding-left:30px">Recurring</label>
                            </p>
                        </div>
                        <div class="clearfix"></div>
                        <input type="button" class="checkout_confirm_box" name="" value="Update" />
                    </form>
                    
                        <div class="billing_note">*By changing your credit card information, all units billed to your old credit card will now be billed to your new credit card.</div>
                </div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<div id="varification_model" class="modal fade" role="dialog">
    <div class="modal-dialog" style="width: 900px;">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="add_vndr">Verify Account</h2>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form enctype='multipart/form-data' action="<?php echo base_url('account/verify_stripe_billing_information'); ?>" method="POST" id="verify-form" onsubmit="return onSubmitDo()">
                    <div class="ajax_report alert display-hide" role="alert" style="margin-bottom: 10px; margin-left: 0px; width: 500px; position: relative; top: 100px;">
                        <span class="close-message"></span>
                        <div class="ajax_message"></div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="clearfix"></div>
                    <div class="tpay-acc-setup">
                        <div class="shopping-cart-sec">
                            <div class="cart-form ch-form">
                                <span style='color: red' class="error" id='payment-error1'></span>
                                <div class="cart-form-divide cart-form-section">
                                    <span class="first-name_verify">
                                             <label>First Name</label>
                                             <input type="text" value="" style="" id="first_name" name="first_name" placeholder="" />
                                        </span>
                                    <span class="last-name_verify">
                                             <label>Last Name</label>
                                             <input type="text" value="" style="" name="last_name" id="last_name"  placeholder="" />
                                        </span>
                                </div>
                                <div class="clearfix"></div>
                                <div class="cart-form-section cart-form-sectionfour">
                                    <span class="varify_span_four dob_span"> 
                                             <label>DOB</label>
                                             <input type="text" name="dob" class="dob" readonly id="dob" placeholder=""  />
                                        </span>
                                    <span class="varify_span_four month_year_span"> 
                                             <label>Identity Document</label>
                   <input id="uploadFile" placeholder="" disabled="disabled" />
                  <div class="fileUpload btn btn-primary">
                    <span>Upload</span>
                                    <input id="uploadBtn" name="verify_doc" type="file" class="upload" />
                                </div>
                                <!-- <input type="file" readonly name="identity_document" id="identity_document"  style=""     />-->
                                </span>
                                <span class="varify_span_full"> 
                                             <label>Address Line 1</label>
                                             <input type="text" class="address_line_1" style=""  id="address_line_1" name="address_line_1">
                                        </span>
                            </div>
                            <div class="clearfix"></div>
                            <div class="cart-form-section cart-form-sectionfour">
                                <span class="varify_span_four postal_code_span"> 
                                             <label>Postal Code</label>
                                             <input type="text" value="" id="postal_code" name="postal_code" placeholder="" />
                                        </span>
                                <span class="varify_span_four city_span"> 
                                             <label>City</label>
                                             <input type="text" value="" name="city" id="city"  placeholder="" />
                                        </span>
                                <span class="varify_span_four state_span"> 
                                             <label>State</label>
                                             <input type="text" value="" id="state" name="state" placeholder="" />
                                        </span>
                                <span class="varify_span_four personal_id_number_span"> 
                                             <label>Personal id number</label>
                                             <input type="text" value="" name="personal_id_number" id="personal_id_number"  placeholder="" />
                                        </span>
                            </div>
                            <div class="clearfix"></div>
                            <div class="cart-btns">
                                <br>
                                <input type="button" class="verify_billing" name="verify_billing" value="Verify" />
                            </div>
                        </div>
                    </div>
            </div>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
    </div>
</div>
</div>

    <!-- Modal -->
    <div id="how_do_ach_payment_work" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">ACH Payment Flow</h4>
          </div>
          <div class="modal-body">
            <p>
                Automated Clearing House (ACH) is an electronic network for financial transactions in the United States. 
                A transaction must be submitted before 7PM EST on a business day for processing to begin on the same day. 
                If not, processing will begin the following day at 7PM EST. 
                Payment to a landlord's, personal bank account is made within 4 business days after the processing of a tenant's rent payment begins as long as the tenant's payment clears.
                This cleared status is shown as PAID under the tenant's Payment History.
            <p>
            <img src="<?php echo base_url().'assets/default/images/step-flow3.jpg'; ?>" style="width:100%">
            </p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>

      </div>
    </div>

<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
<script type="text/javascript">
	$(document).on("click", ".checkoutmodalclass", function () {
	     var card_id = $(this).attr('id');
	     $(".property_card_id").val( card_id );
	});

	$('.checkout_confirm_box').click(function() {
        $(".popup-block").hide();
        $(".checkout_confirm_box_pop").hide();
        $("#card-form").submit();
    });
    Stripe.setPublishableKey('<?php echo STRIPE_PULBLISHABLE_KEY; ?>');
	function onSubmitDoCard() {
        $(".credit-cell-hi").val('');
        $(".credit-cell-hi").attr('placeholder', 'xxxx');
        $("#wait-div").show();
        Stripe.card.createToken(document.getElementById('card-form'), myStripeResponseHandlerCard);
        return false;
    };

    function myStripeResponseHandlerCard(status, response) {
        console.log(status);
        console.log(response);
        if (response.error) {
            //document.getElementById('payment-error').innerHTML = "<div style='padding: 8px;font-size: 14px;' class='alert-danger'>"+ response.error.message +"</div>";
            document.getElementById('payment-error').innerHTML = response.error.message;
            $("#wait-div").hide();
            $(".popup-block").show();
            $('body').addClass('custom_modal_open'); // add class in body
            $(".payment_error_popup").show();
            //$("#payment-form")[0].reset()

        } else {
            var tokenInput = document.createElement("input");
            tokenInput.type = "hidden";
            tokenInput.name = "stripeToken";
            tokenInput.value = response.id;
            var paymentForm = document.getElementById('card-form');
            paymentForm.appendChild(tokenInput);
            var data = $("#card-form").serialize();
            var posturl = $('#card-form').attr('action');
            var formClass = '#card-form';
            $.ajax({
                url: posturl,
                data: data,
                type: "POST",
                dataType: 'json',
                beforeSend: function() {
                    $('#wait-div').show();
                },
                success: function(response) {
                    $('#wait-div').hide();
                    $('body').removeClass('custom_modal_open');
                    if (response.success == true) {
                        $('#tabs-2').find('.custom_ajax_message_form').addClass('alert-success').css("display", 'block').children('.ajax_message').html(response.message).css("display", "block");
                        scrollToElement(formClass, 1000);
                        setTimeout(function() {
                            window.location = response.redirect_url;
                        }, 700);
                    } else {
                        $(formClass).find('.custom_ajax_message_form').addClass('alert-danger').css("display", 'block').children('.ajax_message').html(response.message).css("display", "block");
                        scrollToElement(formClass, 1000);
                        setTimeout(function() {
                            //window.location.href = response.redirect_url;
                        }, 700);
                    }
                },
                error: function(data) {
                    $('#wait-div').hide();
                    $('body').removeClass('custom_modal_open'); // add class in body
                    alert("Server Error.");
                    return false;
                }
            });

        }

    };

    $(document).ready(function() {
        $('.recurring_status_change').click(function() {
            var id = $(this).val();
            if ($(this).prop("checked") == true) {
                var recurring = 1;
            } else {
                var recurring = 0;
            }
            var rec_posturl = '<?php echo base_url("account/change_stripe_recurring_status"); ?>';
            var data = {
                id: id,
                recurring: recurring
            };
            var formClass = '#delete-form_' + id;
            $.ajax({
                url: rec_posturl,
                data: data,
                type: "POST",
                dataType: 'json',
                beforeSend: function() {
                    $('#wait-div').show();
                },
                success: function(response) {
                    $('#wait-div').hide();
                    if (response.success == true) {
                        $(formClass).find('.ajax_report').addClass('alert-success').css("display", 'block').children('.ajax_message').html(response.success_message).css("display", "block");
                        setTimeout(function() {
                            $(formClass).find('.ajax_report').css("display", "none");
                        }, 2000);
                    } else {
                        $('.tab-area').find('.ajax_report').addClass('alert-danger').css("display", 'block').children('.ajax_message').html(response.success_message).css("display", "block");
                        setTimeout(function() {
                            $(formClass).find('.ajax_report').css("display", "none");
                        }, 2000);
                    }
                },
                error: function(data) {
                    alert("Server Error.");
                    return false;
                }
            });
        });
        $('.delete_saved_external_card').click(function() {
            var card_id = $(this).attr('id');
            $("#deleteStripeCard").val(card_id)
            $(".popup-block").show();
            $('body').addClass('custom_modal_open'); // add class in body
            $(".delete_saved_card_info").show();
            $(".delete_saved_card_info").find(".delete_card_permanently").attr('id', card_id);
            data_is = '<li>Tenants will not be able to pay by card if this card will be deleted.</li>';
            $(".delete_saved_card_info").find("ul").html(data_is);
        });

        $('.debit_card_deact_status').click(function() {
            var card_id = $(this).attr('name');
            $("#deleteStripeCard").val(card_id)
            $(".popup-block").show();
            $('body').addClass('custom_modal_open'); // add class in body
            $(".act_deact_saved_card_info").show();
            $(".act_deact_saved_card_info").find(".act_deact_card_permanently").attr('id', card_id);
            data_is = '<li>Tenants will not be able to pay by card if this card will be deactivate.</li>';
            $(".act_deact_saved_card_info").find("ul").html(data_is);
            data_is_act_deact_text = '<li>Do you really want to deactivate this property?</li>';
            $(".act_deact_text").html(data_is_act_deact_text);
            act_deact_card_permanently = 'Deactivate';
            $(".act_deact_card_permanently").html(act_deact_card_permanently);
        });

        $('.debit_card_act_status').click(function() {
            var card_id = $(this).attr('name');
            $("#deleteStripeCard").val(card_id)
            $(".popup-block").show();
            $('body').addClass('custom_modal_open'); // add class in body
            $(".act_deact_saved_card_info").show();
            $(".act_deact_saved_card_info").find(".act_deact_card_permanently").attr('id', card_id);
            data_is = '<li>Tenants will be able to pay by card if this card will be activate.</li>';
            $(".act_deact_saved_card_info").find("ul").html(data_is);
            data_is_act_deact_text = '<li>Do you really want to activate this property?</li>';
            $(".act_deact_text").html(data_is_act_deact_text);
            act_deact_card_permanently = 'Activate';
            $(".act_deact_card_permanently").html(act_deact_card_permanently);
        });

        $('.debit_card_deact_status_subscribed').click(function() {
            var card_id = $(this).attr('name');
            $("#deleteStripeCard").val(card_id)
            $(".popup-block").show();
            $('body').addClass('custom_modal_open'); // add class in body
            $(".act_deact_saved_card_info").show();
            $(".act_deact_saved_card_info").find(".act_deact_card_permanently").attr('id', card_id);
            data_is = '<li></li>';
            $(".act_deact_saved_card_info").find("ul").html(data_is);
            data_is_act_deact_text = '<li>Do you really want to deactivate this property?</li>';
            $(".act_deact_text").html(data_is_act_deact_text);
            act_deact_card_permanently = 'Deactivate';
            $(".act_deact_card_permanently").html(act_deact_card_permanently);
        });

        $('.debit_card_act_status_subscribed').click(function() {
            var card_id = $(this).attr('name');
            $("#deleteStripeCard").val(card_id)
            $(".popup-block").show();
            $('body').addClass('custom_modal_open'); // add class in body
            $(".act_deact_saved_card_info").show();
            $(".act_deact_saved_card_info").find(".act_deact_card_permanently").attr('id', card_id);
            data_is = '<li></li>';
            $(".act_deact_saved_card_info").find("ul").html(data_is);
            data_is_act_deact_text = '<li>Do you really want to activate this property?</li>';
            $(".act_deact_text").html(data_is_act_deact_text);
            act_deact_card_permanently = 'Activate';
            $(".act_deact_card_permanently").html(act_deact_card_permanently);
        });




        $('.delete_saved_card').click(function() {
            var card_id = $(this).attr('id');
            $("#deleteStripeCard").val(card_id)
            $(".popup-block").show();
            $('body').addClass('custom_modal_open'); // add class in body
            $(".delete_saved_card_info").show();
            $(".delete_saved_card_info").find(".delete_card_permanently").attr('id', card_id);
            data_is = '<li>Automatic subscription charges for this property will be disabled.</li>';
            $(".delete_saved_card_info").find("ul").html(data_is);
        });


        $('.act_deact_card_permanently').click(function() {
            var card_id = $(this).attr('id');
            //alert(card_id);              
            $(".popup-block").hide();
            $(".act_deact_saved_card_info").hide();
            //$("#delete-form_"+card_id).submit();

            //var data = $("#delete-form_"+card_id).serialize();
            var data = {
                card_id: card_id
            };
            //var posturl = siteUrl + 'property/charge_by_stripe';
            var posturl = '<?php echo base_url("account/updateStripeCardStatus"); ?>';
            var formClass = '#delete-form_' + card_id;
            $.ajax({
                url: posturl,
                data: data,
                type: "POST",
                dataType: 'json',
                beforeSend: function() {
                    $('#wait-div').show();
                },
                success: function(response) {
                    $('#wait-div').hide();
                    $('body').removeClass('custom_modal_open');
                    if (response.success == true) {
                        $(formClass).find('.ajax_report').addClass('alert-success').css("display", 'block').children('.ajax_message').html(response.message).css("display", "block");
                        setTimeout(function() {
                            $(formClass).remove();
                            $(formClass).find('.ajax_report').css("display", "none");
                            window.location.href = response.redirect_url;
                        }, 2000);
                    } else {
                        $(formClass).find('.ajax_report').addClass('alert-danger').css("display", 'block').children('.ajax_message').html(response.message).css("display", "block");
                        setTimeout(function() {
                            $(".tab-area").find('.ajax_report').css("display", "none");
                        }, 2000);
                    }
                },
                error: function(data) {
                    $('body').removeClass('custom_modal_open');
                    alert("Server Error.");
                    return false;
                }
            });
        });

        $('.delete_card_permanently').click(function() {
            var card_id = $(this).attr('id');
            $(".popup-block").hide();
            $(".delete_saved_card_info").hide();
            //$("#delete-form_"+card_id).submit();

            var data = $("#delete-form_" + card_id).serialize();
            //var posturl = siteUrl + 'property/charge_by_stripe';
            var posturl = $('#delete-form_' + card_id).attr('action');
            var formClass = '#delete-form_' + card_id;
            $.ajax({
                url: posturl,
                data: data,
                type: "POST",
                dataType: 'json',
                beforeSend: function() {
                    $('#wait-div').show();
                },
                success: function(response) {
                    $('#wait-div').hide();
                    $('body').removeClass('custom_modal_open');
                    if (response.success == true) {
                        $(formClass).find('.ajax_report').addClass('alert-success').css("display", 'block').children('.ajax_message').html(response.message).css("display", "block");
                        setTimeout(function() {
                            $(formClass).remove();
                            $(formClass).find('.ajax_report').css("display", "none");
                            window.location.href = response.redirect_url;
                        }, 2000);
                    } else {
                        $(formClass).find('.ajax_report').addClass('alert-danger').css("display", 'block').children('.ajax_message').html(response.message).css("display", "block");
                        setTimeout(function() {
                            $(".tab-area").find('.ajax_report').css("display", "none");
                        }, 2000);
                    }
                },
                error: function(data) {
                    $('body').removeClass('custom_modal_open');
                    alert("Server Error.");
                    return false;
                }
            });
        });
    });
    $(document).ready(function() {
        $('.verify_billing').click(function() {
            var data = $("#verify-form").serialize();
            //var posturl = siteUrl + 'property/charge_by_stripe';
            var posturl = $('#verify-form').attr('action');

            var formClass = '#verify-form';
            //$.ajax({
            $('#verify-form').ajaxSubmit({
                url: posturl,
                dataType: 'json',
                beforeSend: function() {
                    $('#wait-div').show();
                },
                success: function(response) {
                    $('body').removeClass('custom_modal_open');
                    $('#wait-div').hide();
                    if (response.success == true) {
                        $(formClass).find('.ajax_report').addClass('alert-success').css("display", 'block').children('.ajax_message').html(response.message).css("display", "block");
                        //scrollToElement(formClass, 1000);
                        setTimeout(function() {
                            //window.location = response.redirect_url;
                        }, 700);
                    } else {
                        $(formClass).find('.ajax_report').addClass('alert-danger').css("display", 'block').children('.ajax_message').html(response.message).css("display", "block");
                        //scrollToElement(formClass, 1000);
                        setTimeout(function() {
                            //window.location = window.location;
                        }, 700);
                    }
                },
                error: function(data) {
                    $('#wait-div').hide();
                    $('body').removeClass('custom_modal_open');
                    alert("Server Error.");
                    return false;
                }
            });
        });
    });
    Stripe.setPublishableKey('<?php echo STRIPE_PULBLISHABLE_KEY; ?>');

    function onSubmitDo() {
        $(".credit-cell-hi").val('');
        $(".credit-cell-hi").attr('placeholder', 'xxxx');
        $("#wait-div").show();
        Stripe.card.createToken(document.getElementById('payment-form'), myStripeResponseHandler);
        return false;
    };

    function myStripeResponseHandler(status, response) {
        console.log(status);
        console.log(response);
        if (response.error) {
            //document.getElementById('payment-error').innerHTML = "<div style='padding: 8px;font-size: 14px;' class='alert-danger'>"+ response.error.message +"</div>";
            document.getElementById('payment-error').innerHTML = response.error.message;
            $("#wait-div").hide();
            $(".popup-block").show();
            $('body').addClass('custom_modal_open'); // add class in body
            $(".payment_error_popup").show();
            //$("#payment-form")[0].reset()

        } else {
            var tokenInput = document.createElement("input");
            tokenInput.type = "hidden";
            tokenInput.name = "stripeToken";
            tokenInput.value = response.id;
            var paymentForm = document.getElementById('payment-form');
            paymentForm.appendChild(tokenInput);
            //console.log(paymentForm);
            //console.log($("#payment-form").serialize());
            var data = $("#payment-form").serialize();
            //var posturl = siteUrl + 'property/charge_by_stripe';
            var posturl = $('#payment-form').attr('action');
            var formClass = '#payment-form';
            $.ajax({
                url: posturl,
                data: data,
                type: "POST",
                dataType: 'json',
                beforeSend: function() {
                    $('#wait-div').show();
                },
                success: function(response) {
                    if (response.success == true) {
                        $(formClass).find('.ajax_report').addClass('alert-success').css("display", 'block').children('.ajax_message').html(response.message).css("display", "block");
                        scrollToElement(formClass, 1000);
                        setTimeout(function() {
                            window.location = window.location;
                        }, 700);
                    } else {
                        $(formClass).find('.ajax_report').addClass('alert-danger').css("display", 'block').children('.ajax_message').html(response.message).css("display", "block");
                        scrollToElement(formClass, 1000);
                        setTimeout(function() {
                            //window.location.href = response.redirect_url;
                        }, 700);
                    }
                },
                error: function(data) {
                    alert("Server Error.");
                    return false;
                }
            });

        }
    };


    $(function() {
        $('.month_year').datepicker({
            changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
            dateFormat: 'mm/yy',
            yearRange: '2016:2100',
            onClose: function(dateText, inst) {
                $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
                $('.exp_month').val(inst.selectedMonth + 1);
                $('.exp_year').val(inst.selectedYear);
            }
        });
        $('#dob').datepicker({
            dateFormat: 'dd/mm/yy'
        });
    });

    jQuery(function($) {
        $(".credit").credit();
    });

    /**
     * Credit.js
     * Version: 1.0.0
     * Author: Ron Masas
     */

    (function($) {

        $.fn.extend({
            credit: function(args) {

                $(this).each(function() {


                    // Set defaults
                    var defaults = {
                        auto_select: true
                    }

                    // Init user arguments
                    var args = $.extend(defaults, args);

                    // global var for the orginal input
                    var credit_org = $(this);

                    // Hide input if css was not set
                    credit_org.css("display", "none");

                    // Create credit control holder
                    var credit_control = $('<div></div>', {
                        class: "credit-input"
                    });

                    // Add credit cell inputs to the holder
                    for (i = 0; i < 4; i++) {
                        if (i == 3) {
                            credit_control.append(
                                $("<input />", {
                                    class: "credit-cell",
                                    placeholder: "0000",
                                    maxlength: 4
                                })
                            );
                        } else {
                            credit_control.append(
                                $("<input />", {
                                    class: "credit-cell credit-cell-hi",
                                    placeholder: "0000",
                                    maxlength: 4
                                })
                            );
                        }
                    }

                    // Print the full credit input
                    credit_org.after(credit_control);

                    // Global var for credit cells
                    var cells = credit_control.children(".credit-cell");

                    /**
                     * Set key press event for all credit inputs
                     * this function will allow only to numbers to be inserted.
                     * @access public
                     * @return {bool} check if user input is only numbers
                     */
                    cells.keypress(function(event) {
                        // Check if key code is a number
                        if (event.keyCode > 31 && (event.keyCode < 48 || event.keyCode > 57)) {
                            // Key code is a number, the `keydown` event will fire next
                            return false;
                        }
                        // Key code is not a number return false, the `keydown` event will not fire
                        return true;
                    });

                    /**
                     * Set key down event for all credit inputs
                     * @access public
                     * @return {void}
                     */
                    cells.keydown(function(event) {
                        // Check if key is backspace
                        var backspace = (event.keyCode == 8);
                        // Switch credit text length
                        switch ($(this).val().length) {
                            case 4:
                                // If key is backspace do nothing
                                if (backspace) {
                                    return;
                                }
                                // Select next credit element
                                var n = $(this).next(".credit-cell");
                                // If found
                                if (n.length) {
                                    // Focus on it
                                    n.focus();
                                }
                                break;
                            case 0:
                                // Check if key down is backspace
                                if (!backspace) {
                                    // Key is not backspace, do nothing.
                                    return;
                                }
                                // Select previous credit element
                                var n = $(this).prev(".credit-cell");
                                // If found
                                if (n.length) {
                                    // Focus on it
                                    n.focus();
                                }
                                break;
                        }
                    });

                    // On cells focus
                    cells.focus(function() {
                        // Add focus class
                        credit_control.addClass('c-focus');
                    });

                    // On focus out
                    cells.blur(function() {
                        // Remove focus class
                        credit_control.removeClass('c-focus');
                    });

                    /**
                     * Update orginal input value to the credit card number
                     * @access public
                     * @return {void}
                     */
                    cells.keyup(function() {
                        // Init card number var
                        var card_number = '';
                        // For each of the credit card cells
                        cells.each(function() {
                            // Add current cell value
                            card_number = card_number + $(this).val();
                        });
                        // Set orginal input value
                        credit_org.val(card_number);
                    });


                    if (args["auto_select"] === true) {
                        // Focus on the first credit cell input
                        credit_control.children(".credit-cell:first").focus();
                    }

                });

            }
        });

    })(jQuery);
    document.getElementById("uploadBtn").onchange = function() {
        document.getElementById("uploadFile").value = this.value;
    };

</script>

<!-- STyle CSS-------------------->

<style>
.row {
position: relative;
}
.post-list {
margin-bottom: 20px;
}
div.list-item {
border-left: 4px solid #7ad03a;
margin: 5px 15px 2px;
padding: 1px 12px;
background-color: #F1F1F1;
-webkit-box-shadow: 0 1px 1px 0 rgba(0, 0, 0, .1);
box-shadow: 0 1px 1px 0 rgba(0, 0, 0, .1);
height: 60px;
}
div.list-item p {
margin: .5em 0;
padding: 2px;
font-size: 13px;
line-height: 1.5;
}
.list-item a {
text-decoration: none;
padding-bottom: 2px;
color: #0074a2;
-webkit-transition-property: border, background, color;
transition-property: border, background, color;
-webkit-transition-duration: .05s;
transition-duration: .05s;
-webkit-transition-timing-function: ease-in-out;
transition-timing-function: ease-in-out;
}
.list-item a:hover {
text-decoration: underline;
}
.list-item h2 {
font-size: 25px;
font-weight: bold;
text-align: left;
}
/* Pagination */

div.pagination {
font-family: "Lucida Sans Unicode", "Lucida Grande", LucidaGrande, "Lucida Sans", Geneva, Verdana, sans-serif;
padding: 2px;
margin: 20px 10px;
float: right;
}
.trans_table td {
padding: 8px 5px;
}
.trans_table th {
background-color: #00659f;
color: white;
padding: 8px 5px;
}
div.pagination a {
margin: 2px;
padding: 0.5em 0.64em 0.43em 0.64em;
background-color: #00659F;
text-decoration: none;
/* no underline */

color: #fff;
}
div.pagination a:hover,
div.pagination a:active {
padding: 0.5em 0.64em 0.43em 0.64em;
margin: 2px;
background-color: #3d85bf;
color: #fff;
}
div.pagination span.current {
padding: 0.5em 0.64em 0.43em 0.64em;
margin: 2px;
background-color: #f6efcc;
color: #6d643c;
}
div.pagination span.disabled {
display: none;
}
.pagination ul li {
display: inline-block;
}
.pagination ul li a.active {
opacity: .5;
}
/* loading */

.loading {
position: absolute;
left: 0;
top: 0;
right: 0;
bottom: 0;
z-index: 2;
background: rgba(255, 255, 255, 0.7);
}
.loading .content {
position: absolute;
transform: translateY(-50%);
-webkit-transform: translateY(-50%);
-ms-transform: translateY(-50%);
top: 50%;
left: 0;
right: 0;
text-align: center;
color: #555;
}
.ui-datepicker-calendar {
display: none;
}
.ui-datepicker-header {
width: 200px;
}
.ui-datepicker-close.ui-state-default.ui-priority-primary.ui-corner-all {
float: right;
}
.ui-state-default {
margin: 7px 0 0;
padding: 1px 6px;
}
#wait-div{
	z-index: 9999999;
}
.trans_table td {
    border: 1px solid #ccc;
}
</style>