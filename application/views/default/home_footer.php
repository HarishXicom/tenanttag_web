<footer>
    <?php
    function addhttp($url) {
        if (!preg_match("~^(?:f|ht)tps?://~i", $url)) {
            $url = "http://" . $url;
        }
        return $url;
    }
    ?>
    <div class="container">
        <?php $links = getFooterLinks(); //echo '<pre>';print_r($links);die;  ?>
        <div class="fttr-block footer-link-block">
            <ul>
                <?php if (!empty($links)) { ?>
                    <?php foreach ($links as $link): ?>
                        <li class="footer-link-li">
                            <a href="<?= site_url('page') ?>/<?= $link->slug ?>"><?php echo $link->title; ?></a></li>
                    <?php endforeach; ?>
                <?php } ?>
            </ul>
        </div>
        <div class="fttr-block subscribe_ten">
            <a target="_blank" href="<?php echo "http://www.tenanttagblog.com/" ?>" class="drblog">Blog</a>
            <?php echo form_open('welcome/subscribe', array('class' => 'ajaxForm', 'id' => 'subscribe')) ?>

                <input type="email" name="email" class="dremail">
                <button class="subscribe">Subscribe</button>
                <div class="ajax_report alert display-hide" role="alert" style="float: left; width: 85%; padding: 5px; margin-top: 4px;">
                    <span class="close close_custom_popup"></span>
                    <span class="ajax_message" style="line-height:normal;margin: 0;padding: 3px;"></span>
                </div>
            <?php form_close(); ?>
        </div>	
        <div class="fttr-block right-pull">
            <h4>Follow Us</h4>
            <ul class="social-link">
                <?php $urls = getSocialUrls(); ?>
                <li><a id="drfb" target="_blank" href="<?php echo addhttp($urls->facebook_url); ?>"><i class="fa fa-facebook"></i></a></li>
                <li><a id="drtwt" target="_blank" href="<?php echo addhttp($urls->twitter_url); ?>"><i class="fa fa-twitter"></i></a></li>
                <li><a  target="_blank" href="<?php echo addhttp($urls->pinterest_url); ?>"><i class="fa fa-pinterest-p"></i></a></li>
            </ul>
            <img src="<?= $this->config->item('templateassets') ?>images/fttr-logo.png" alt=""/>
            <p><?php echo $urls->copyright_text; ?></p>
        </div>
    </div>
</footer>
<script src="<?= $this->config->item('templateassets') ?>js/jquery-effects.js"></script>
<script>
    $('nav').menu({start: 979});
</script>
<script type="text/javascript" src="<?= $this->config->item('templateassets') ?>js/lightbox.min.js"></script>
<link href="<?= $this->config->item('templateassets') ?>css/lightbox.css" rel="stylesheet">
<script src="<?= $this->config->item('templateassets') ?>js/jquery-ui.js" type="text/javascript"></script>
<style>
    .my-tab.ui-tabs-nav .ui-state-active a {

        background-color: #ffffff !important;
        border-bottom-left-radius: 5px;
        border-color: #e2e2e4  #e2e2e4 #e2e2e4;
        border-image: none;
        border-style: solid none solid solid;
        border-top-left-radius: 5px;
        border-width: 1px 0 1px 1px;
        margin: 0 -15px -2px;
        padding: 22px 15px;
        position: relative;
        z-index: 9;
        background-color: #ffffff;
    }
</style>
<script type="text/javascript" src="//s3.amazonaws.com/downloads.mailchimp.com/js/signup-forms/popup/embed.js" data-dojo-config="usePlainJson: true, isDebug: false"></script>



<!-- Global Site Tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-78376704-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-78376704-1');
</script>


</body>
</html>