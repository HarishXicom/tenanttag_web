<link href="<?= $this->config->item('templateassets') ?>css/bootstrap.min.css" rel="stylesheet">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.5/css/bootstrap-dialog.min.css" rel="stylesheet" type="text/css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.5/js/bootstrap-dialog.min.js"></script>
<?php $active_tenants = $inactive_tenants = array(); ?>

<section class="lease-part wb-tenant-page">
    <div class="container">

        <div class="ajax_report alert display-hide" role="alert" style=" margin-top: 50px; margin-bottom:10px;width:1169px;"><span class="close-message"></span><div class="ajax_message">Hello Message</div></div>
        <div id="tabs">
            <div class="my-tab1"> 
                <ul class="my-tab ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">
                    <li class="active all-tabs-panel-li"><a href="#summary" class="summary ">Summary</a></li>
                    <?php foreach ($allProperties as $prop) { ?>
                        <li class="all-tabs-panel-li"><a href="#tabs-<?= $prop->prop_id ?>"><?= $prop->city ?>, <?= $prop->address1 ?> <?= $prop->address2 ?></a></li>
                    <?php } ?>
                </ul>
            </div>
            <div class="tab-area">
                <div id="summary">
                    <div class="tenant">
                        <ul class="tenant1">
                            <?php
                            foreach ($allProperties as $val) {
                                $tenatnts = $val->tenants;
                               
                                ?>
                                <li>
                                    <span><?= $val->city ?>, <?= $val->address1 ?> <?= $val->address2 ?></span>
                                    <p class="tnt_label">Tenants</p>
                                    <?php
                                    $defaultWidth = 32;
                                    if (!empty($tenatnts)) {
                                        $width = $defaultWidth / sizeof($tenatnts);
                                        foreach ($tenatnts as $tenant) {
                                            // active tenant array to show on moveout popup
                                            if($tenant->move_status==1){
                                             	$active_tenants[] = $tenant;
                                            }
                                            if($tenant->move_status==0){
                                             	$active_tenants[] = $tenant;
                                            }
                                            if($tenant->move_status==2){
                                             	$inactive_tenants[] = $tenant;
                                            }
                                            ?>
                                            <a class="redrct" id="<?= $val->prop_id ?>" href="javascript:;"><?= $tenant->first_name." ".$tenant->last_name; ?></a>
                                            <?php
                                        }
                                    } else {
                                        ?>
                                    <a href="javascript:;" style="cursor: default;color:#000">No Tenants</a>	
                                    <?php } ?>	

                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
                <?php
                foreach ($allProperties as $val) {
                    $tenatnts = $val->tenants;
                    
                    ?>
                    <div id="tabs-<?= $val->prop_id ?>">
                        <?php if ($tenatnts) { ?>
                            <?php //echo "<pre>"; print_R($tenatnts); ?>
                            <?php foreach ($tenatnts as $tenant) { ?>
                                <div class="tenant tenant_tenant">
                                    <ul class="tenant1" id="edit-<?= $tenant->tenant_id ?>">
                                        <li><span class="my-span"><?= $val->property_code ?></span>
                                            <ul class="btn-group">
                                                  <!--   <li><?php //echo $tenant->move_status; ?> </li> -->
                                                <?php if ($tenant->move_status == 0) { ?>
                                                    <?php
                                                    $verification_status = $this->common_model->getSingleFieldFromAnyTable('verification_status', 'mem_id', $this->session->userdata('MEM_ID'), 'tbl_members');
                                                        if($verification_status=='No'){ $clss = 'intservices';}else{$clss = 'chk_in';}
                                                        ?>
                                                        <!-- <li><a href="javascript:;" id="<?= $tenant->tenant_id ?>" data-prop='<?= $val->prop_id ?>' class="red-col <?php echo $clss; ?>">Check In</a></li> -->
                                                        <li><a href="javascript:;" class="sky-blue-col edit-tenant" data-value="<?= $tenant->tenant_id ?>">Edit</a></li>
                                                        <li><a href="javascript:;" id="renew-<?= $tenant->tenant_id . "-" . $val->prop_id ?>" class="green-col rnw_rel" data-bind="<?= date('m/d/Y', strtotime($tenant->lease_end_date)) ?>" data-toggle="modal" data-target="#myModal">Renew</a></li>
                                                        <li><a href="javascript:;" class="red-col mvout_btn" data-toggle="modal" id="<?= $tenant->tenant_id ?>" data-target="#myModal2">Move Out</a></li>
                                                <?php } elseif ($tenant->move_status == 1) { ?>
                                                        <li><a href="javascript:;" class="sky-blue-col edit-tenant" data-value="<?= $tenant->tenant_id ?>">Edit</a></li>
                                                        <li><a href="javascript:;" id="renew-<?= $tenant->tenant_id . "-" . $val->prop_id ?>" class="green-col rnw_rel" data-bind="<?= date('m/d/Y', strtotime($tenant->lease_end_date)) ?>" data-toggle="modal" data-target="#myModal">Renew</a></li>
                                                        <li><a href="javascript:;" class="red-col mvout_btn" data-toggle="modal" id="<?= $tenant->tenant_id ?>" data-target="#myModal2">Move Out</a></li>
                                                <?php } else { ?>
                                                        <li><a href="javascript:;" class="red-col mvin_btn" data-toggle="modal" id="<?= $tenant->tenant_id ?>" data-target="#myModal3">Move in</a></li>
                                                <?php } ?>
                                                        <li><a href="javascript:;" class="red-col delte_btn" id="<?= $tenant->tenant_id ?>">Delete</a></li>

                                            </ul>
                                        </li>
                                    </ul>

                                    <ul class="tenant1" id="save-<?= $tenant->tenant_id ?>" style="display:none;">
                                        <li>
                                            <ul class="btn-group">
                                                <li><a href="javascript:;" class="red-col cancel-record" data-value="<?= $tenant->tenant_id ?>">Cancel</a></li>
                                                <li><a href="javascript:;" class="sky-blue-col save-record"  data-key="<?= $val->prop_id ?>" data-value="<?= $tenant->tenant_id ?>">Save</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                    <ul class="my-list border0 " id="show-tenant-<?= $tenant->tenant_id ?>" >
                                        <li>
                                            
                                            <span> <label class="label1">First name</label><label class="label2"><?= $tenant->first_name ?></label></span>
                                            <span> <label class="label1">Middle name</label><label class="label2"><?= $tenant->middle_name !=''? ucfirst($tenant->middle_name):'N/A'; ?></label></span>
                                            <span><label class="label1">Last Name</label><label class="label2"><?= $tenant->last_name ?></label></span>

                                            <span><label class="label1">DOB</label><label class="label2"><?= date('m/d/Y', $tenant->dob) ?></label></span>
                                            <span> <label class="label1">Mobile</label><label class="label2"><?= $tenant->mobile_no ?></label></span>
                                        </li>
                                        <li>
                                            <span><label class="label1">Language</label><label class="label2"><?= $tenant->preferred_language ?></label></span>
                                            <span><label class="label1">Email</label><label class="label2"><?= $tenant->email ?></label></span>
                                            <span><label class="label1">Lease Start  </label><label class="label2"><?= date('m/d/Y', strtotime($tenant->lease_start_date)) ?></label></span>
                                            <span> <label class="label1">Lease End</label><label class="label2"><?= date('m/d/Y', strtotime($tenant->lease_end_date)) ?></label></span>
                                            <span class="curr"><label class="label1">Rent</label><label class="label2">$<?= $tenant->rent_amount ?></label></span>
                                        </li>
                                        <li>
                                            <span><label class="label1">Due Date</label><label class="label2"><?= date('m/'.$tenant->due_date . '/Y') ?></label></span>
                                            <span><label class="label1">Late Fee Type</label><label class="label2"><?= $tenant->late_fee_type!=''?$tenant->late_fee_type:'N/A'; ?></label></span>
                                            <?php if($tenant->late_fee_type !=''){ ?>
                                               <span><label class="label1">Late Fee</label><label class="label2">$<?= $tenant->late_fee ?></label></span>
                                               <span><label class="label1">Late Fee Start</label><label class="label2"><?= $tenant->late_fee_start ?>th</label></span>
                                            <?php } ?>
                                             <span> <label class="label1">User Name</label><label class="label2"><?= $tenant->slug ?></label></span>
                                        </li>
                                        <div class="sub-panel-head">Pet Info</div>
                                        <?php if(!empty($tenant->pets_type)){ ?>
                                        
                                                <?php $pets_type_array = explode(',',$tenant->pets_type); ?>
                                                <?php $pets_name_array = explode(',',$tenant->pets_name); ?>
                                                <?php $pets_fee_array = explode(',',$tenant->pets_fee); ?>
                                                <?php $pets_breed_array = explode(',',$tenant->pets_breed); ?>
                                                <?php foreach ($pets_type_array as $key => $value) { ?>  
                                                    <?php if($key == 0){ ?>
                                                        <li class="per_info_cl">                                                  
                                                            <span>
                                                                <label class="label1">Pet Type</label>                                                                
                                                            </span>
                                                            <span>
                                                                <label class="label1">Pet Name</label>                                                                
                                                            </span>
                                                            <span>
                                                                <label class="label1">Pet Fee</label>                                                                
                                                            </span>
                                                             <span>
                                                                <label class="label1">Pet Breed/description</label>                                                                
                                                            </span>
                                                        </li>
                                                    <?php } ?>
                                                        <li class='<?= count($pets_type_array) == $key+1 ? "" : "per_info_cl"; ?>'>                                                  
                                                            <span>
                                                                <label class="label2">
                                                                    <?= $pets_type_array[$key]!=''? ucfirst($pets_type_array[$key]):'N/A'; ?>
                                                                </label>
                                                            </span>
                                                            <span>
                                                                <label class="label2">
                                                                    <?= $pets_name_array[$key]!=''? ucfirst($pets_name_array[$key]):'N/A'; ?>
                                                                </label>
                                                            </span>
                                                            <span>
                                                                <label class="label2">
                                                                    <?= $pets_fee_array[$key]!=''? ucfirst($pets_fee_array[$key]):'N/A'; ?>
                                                                </label>
                                                            </span>
                                                             <span>
                                                                <label class="label2">
                                                                    <?= $pets_breed_array[$key]!=''? ucfirst($pets_breed_array[$key]):'N/A'; ?>
                                                                </label>
                                                            </span>
                                                        </li>
                                                <?php } ?>
                                        <?php }else{ ?>
                                            <li>
                                                <span><label class="label1">Pet Type</label><label class="label2"><?= $tenant->pets_type!=''?$tenant->pets_type:'N/A'; ?></label></span>
                                                 <span><label class="label1">Pet Breed</label><label class="label2"><?= $tenant->pets_breed!=''?$tenant->pets_breed:'N/A'; ?></label></span>
                                            </li>
                                        <?php } ?>
                                        <div class="sub-panel-head">Emergency contact</div>
                                        <li>
                                            <span>
                                                <label class="label1">Emergency Name</label>
                                                <label class="label2">
                                                    <?= $tenant->emergency_name!=''?$tenant->emergency_name:'N/A'; ?>
                                                </label>
                                            </span>
                                             <span>
                                                <label class="label1">Emergency Phone</label>
                                                <label class="label2">
                                                    <?= $tenant->emergency_phone!=''?$tenant->emergency_phone:'N/A'; ?>
                                                </label>
                                            </span>
                                            <span>
                                                <label class="label1">Emergency Email</label>
                                                <label class="label2">
                                                    <?= $tenant->emergency_email!=''?$tenant->emergency_email:'N/A'; ?>
                                                </label>
                                            </span>
                                            <span>
                                                <label class="label1">Relationship</label>
                                                <label class="label2">
                                                    <?= $tenant->relationship!=''?$tenant->relationship:'N/A'; ?>
                                                </label>
                                            </span>
                                        </li>
                                    </ul>
                                    <?php echo form_open('', array('class' => 'tenant-detail update_teneant', 'name' => "tenant_edit_".$tenant->tenant_id,'id' => "tenant_edit_".$tenant->tenant_id)) ?>
                                    <!--<form id="form-<?//= $tenant->tenant_id ?>">-->
                                    <ul class="my-list border0 tenttt tttt2222" id="edit-tenant-<?= $tenant->tenant_id ?>" style="display:none;">
                                        <li>
                                            <span> <label class="label1">First name</label><input type="text" required="true" customvalidation="true" class="form-control tenant-txtfield" value="<?= $tenant->first_name ?>" name="first_name_<?= $tenant->tenant_id ?>" id="first_name_<?= $tenant->tenant_id ?>" /></span>
                                            <span><label class="label1">Middle Name</label><input type="text" class="form-control tenant-txtfield" value="<?= $tenant->middle_name ?>" name="middle_name_<?= $tenant->tenant_id ?>" id="middle_name_<?= $tenant->tenant_id ?>"/></span>
                                            <span><label class="label1">Last Name</label><input type="text" required="true" customvalidation="true" class="form-control tenant-txtfield" value="<?= $tenant->last_name ?>" name="last_name_<?= $tenant->tenant_id ?>" id="last_name_<?= $tenant->tenant_id ?>"/></span>
                                            <span class="dob1 span-up">
                                                <span><label class="label1">DOB</label><input type="text" required="true" class="form-control tenant-txtfield dob" value="<?= date('m/d/Y', $tenant->dob) ?>" name="dob_<?= $tenant->tenant_id ?>" id="dob_<?= $tenant->tenant_id ?>"/></span>
                                                <span> <label class="label1">Mobile</label><input type="text" required="true" phoneUS="true" class="form-control tenant-txtfield phone_us" value="<?= $tenant->mobile_no ?>" name="mobile_no_<?= $tenant->tenant_id ?>" id="mobile_no_<?= $tenant->tenant_id ?>" /></span>
                                            </span>                                        
                                            <span><label class="label1">Email</label><input type="text" required="true" email="true" class="form-control tenant-txtfield email_red" value="<?= $tenant->email ?>" name="email_<?= $tenant->tenant_id ?>" id="email_<?= $tenant->tenant_id ?>" readonly/></span>
                                        
                                            <span class="sel-lang-start span-up">
                                                <span>
                                                        <label class="label1">Language</label>
                                                        <select class="form-control my-txt-fleid" name="preferred_lang_<?= $tenant->tenant_id ?>" id="preferred_lang_<?= $tenant->tenant_id ?>" >
                                                            <option value="">Select Language</option>
                                                            <option value="English" <?php if ($tenant->preferred_language == 'English') { ?>selected<?php } ?>>English</option>
                                                            <option value="Spanish" <?php if ($tenant->preferred_language == 'Spanish') { ?>selected<?php } ?>>Spanish</option>
                                                        </select>
                                                </span>
                                                <span><label class="label1">Lease Start  </label><input required="true" type="text" class="form-control tenant-txtfield leasefrom" value="<?= date('m/d/y', strtotime($tenant->lease_start_date)) ?>" name="lease_start_date_<?= $tenant->tenant_id ?>" id="lease_start_date_<?= $tenant->tenant_id ?>"/></span>
                                            </span>
                                            <span class="rentt span-up">
                                                <span> <label class="label1">Lease End</label><input type="text" required="true" class="form-control tenant-txtfield leaseto" value="<?= date('m/d/y', strtotime($tenant->lease_end_date)) ?>" name="lease_end_date_<?= $tenant->tenant_id ?>" id="lease_end_date_<?= $tenant->tenant_id ?>"/></span>
                                                <span class="currency_text_box curr"><label class="label1">Rent</label><span class="dolar-sign"><i class="fa fa-usd" style="font-size:14px"></i></span><input type="text" required="1" class="form-control tenant-txtfield" value="<?= $tenant->rent_amount ?>" name="rent_amount_<?= $tenant->tenant_id ?>" id="rent_amount_<?= $tenant->tenant_id ?>"/></span>
                                            </span>
                                            <span class="rent-due span-up">
                                                <span>
                                                    <label class="label1">Due Date</label>
                                                    <select class="form-control my-txt-fleid due_date_start_class" required="true" name="due_date_<?= $tenant->tenant_id ?>" id="due_date_<?= $tenant->tenant_id ?>" >
                                                        <?php for ($j = 1; $j < 31; $j++) { ?>
                                                            <option  <?php
                                                            if ($tenant->due_date == $j) {
                                                                echo 'selected="selected"';
                                                            }
                                                            ?> value="<?php echo $j ?>"><?php echo ordinal($j); ?></option>
                                                            <?php } ?>
                                                    </select>
                                                </span>
                                                <span>
                                                    <label class="label1">Late Fee Type</label>
                                                    <select class="form-control my-txt-fleid late_fee_type_cls" id="late_fee_type_<?= $tenant->tenant_id ?>" required="true" name="late_fee_type_<?= $tenant->tenant_id ?>">
                                                        <option <?php if($tenant->late_fee_type==''){ echo 'selected="selected"';} ?> value="">Late Fee type</option>
                                                        <option <?php if($tenant->late_fee_type=='Daily charge'){ echo 'selected="selected"';} ?> value="Daily charge">Daily charge</option>
                                                        <option <?php if($tenant->late_fee_type=='One time'){ echo 'selected="selected"';} ?> value="One time">One time</option>
                                                    </select>
                                                </span>
                                            </span>
                                            <span class="late-fee span-up">
                                                <span class=" currency_text_box curr <?php //if($tenant->late_fee_type==''){ echo 'late_fee_sh';} ?>">
                                                    <label class="label1">Late Fee</label>
                                                    <span class="dolar-sign"><i class="fa fa-usd" style="font-size:14px"></i></span>
                                                    <input type="text" class="form-control tenant-txtfield" value="<?= $tenant->late_fee ?>" name="late_fee_<?= $tenant->tenant_id ?>" id="late_fee_<?= $tenant->tenant_id ?>"/>
                                                </span>
                                                <span>
                                                    <label class="label1">Late Fee Start</label>
                                                    <select class="form-control my-txt-fleid late_fee_start_class" required="true" name="late_fee_start_<?= $tenant->tenant_id ?>" id="late_fee_start_<?= $tenant->tenant_id ?>" class="">
                                                        <?php for ($j = 1; $j < 31; $j++) { ?>
                                                            <option  <?php
                                                            if ($tenant->due_date == $j) {
                                                                echo 'selected="selected"';
                                                            }
                                                            ?> value="<?php echo $j ?>"><?php echo ordinal($j); ?></option>
                                                            <?php } ?>
                                                    </select>
                                                </span>
                                            </span>
                                        <div class="sub-panel-head">Pets Info</div>
                               
                                        <?php $pets_type_array = explode(',',$tenant->pets_type); ?>
                                        <?php $pets_name_array = explode(',',$tenant->pets_name); ?>
                                        <?php $pets_fee_array = explode(',',$tenant->pets_fee); ?>
                                        <?php $pets_breed_array = explode(',',$tenant->pets_breed); ?>
                                        <?php foreach ($pets_type_array as $key => $value) { ?>  
                                                <li class="pet-info">
                                                    <span><label class="label1">Pet type</label>
                                                        <input type="hidden" id="pets1" value="Yes"  name="pets[]" >
                                                        <select class="form-control my-txt-fleid"  required="true"  id="pets_type[]"  name="pets_type[]" >
                                                            <option value="">Select Type</option>
                                                            <option >None</option>
                                                            <option <?php if ($pets_type_array[$key] == 'dog') { ?>selected<?php } ?> value="dog">Dog</option>
                                                            <option <?php if ($pets_type_array[$key] == 'cat') { ?>selected<?php } ?> value="cat">Cat</option>
                                                            <option <?php if ($pets_type_array[$key] == 'other') { ?>selected<?php } ?> value="other">Other</option>
                                                        </select>
                                                    </span>
                                                     <span>
                                                        <label>Pet Name</label>
                                                        <input type="text" class="form-control my-txt-fleid valid"   value="<?php echo isset($pets_name_array[$key]) ? $pets_name_array[$key] : ''; ?>"  placeholder="" id="pets_name[]"  name="pets_name[]" >  
                                                    </span>
                                                     <span class="currency_text_box curr">
                                                        <label>Pet Fee</label>
                                                        <span class="dolar-sign"><i class="fa fa-usd" style="font-size:14px"></i></span>
                                                        <input type="text" class="form-control my-txt-fleid valid"   value="<?php echo isset($pets_fee_array[$key]) ? $pets_fee_array[$key] : ''; ?>" number='true' maxlength="6" digits="true"  placeholder="" id="pets_fee[]"  name="pets_fee[]" >    
                                                    </span>
                                                    <span class="pet-bred">
                                                        <label class="label1">Pet breed/Description</label>
                                                        <input type="text" value="<?php echo isset($pets_breed_array[$key]) ? $pets_breed_array[$key] : ''; ?>" class="form-control my-txt-fleid" id="pets_breed[]"  name="pets_breed[]" >
                                                    </span>
                                                    <span>
                                                    <label class="label1"> &nbsp;&nbsp;&nbsp;&nbsp;</label>
                                                        <input type="hidden" class="pets_no" name="pets_no" value="1">
                                                        <?php if($key == 0){ ?>
                                                            <a href="javascript:;" class="add-pet-btn"><i class="fa fa-plus"></i>Add Pet</a>
                                                        <?php }else{ ?>
                                                            <a href="javascript:;" class="delete-pets-row" id="pets_row[]" data-value="" style="">
                                                            <img src="<?php echo $this->config->item('templateassets'); ?>images/cross.png" alt="">
                                                            </a>
                                                        <?php } ?>
                                                    </span>
                                                </li>
                                        <?php } ?>
                                    <div class="sub-panel-head">Emergency Contact</div>
                                        <li class="em-cont">
                                            <span>
                                                <label class="label1">Relationship</label>
                                            <select class="form-control my-txt-fleid relationship" id="relationship_<?= $tenant->tenant_id ?>" name="relationship_<?= $tenant->tenant_id ?>">
                                                <option value="">Select Relationship</option>
                                                <option <?php echo ($tenant->emergency_phone == 'Wife') ? 'selected' : ''; ?> value="Wife">Wife</option>
                                                <option <?php echo ($tenant->emergency_phone == 'Husband') ? 'selected' : ''; ?> value="Husband">Husband</option>
                                                <option <?php echo ($tenant->emergency_phone == 'Mother') ? 'selected' : ''; ?> value="Mother">Mother</option>
                                                <option <?php echo ($tenant->emergency_phone == 'Father') ? 'selected' : ''; ?> value="Father">Father</option>
                                                <option <?php echo ($tenant->emergency_phone == 'Friend') ? 'selected' : ''; ?> value="Friend">Friend</option>
                                                <option <?php echo ($tenant->emergency_phone == 'Other') ? 'selected' : ''; ?> value="Other">Other</option>
                                            </select>
                                            </span>
                                            <span class="em-name"><label class="label1">Emergency Name</label>
                                                <input type="text" required="true" customvalidation="true" maxlength="20" class="form-control tenant-txtfield emergency_name" value="<?= $tenant->emergency_name ?>" id="emergency_name_<?= $tenant->tenant_id ?>" name="emergency_name_<?= $tenant->tenant_id ?>"/>
                                            </span>
                                            <span class="em-ph">
                                                <label class="label1">Emergency Phone</label>
                                                <input type="text"  class="form-control my-txt-fleid valid phone_us emergency_phone"  value="<?= $tenant->emergency_phone ?>" id="emergency_phone_<?= $tenant->tenant_id ?>" name="emergency_phone_<?= $tenant->tenant_id ?>"/>
                                            </span>
                                            <span>
                                            <label class="label1">Emergency Email</label>
                                                <input type="email" class="form-control my-txt-fleid valid emergency_email"  value="<?= $tenant->emergency_email ?>" id="emergency_email_<?= $tenant->tenant_id ?>" name="emergency_email_<?= $tenant->tenant_id ?>"/>
                                            </span>
                                        </li>
                                        <input type="hidden" name="property_id_<?= $tenant->tenant_id ?>" id="property_id_<?= $tenant->tenant_id ?>" value="<?= $tenant->property_id ?>">
                                    </ul>
                                    <?php echo form_close(); ?>
                                    <div class="sub-panel-head">Rent Balance Due</div>
                                    <ul class="rent_bal_due_container my-list">
                                        <li> Rent Balance Due </li>
                                        <li> <a href="javascript:;" name="" data-toggle="modal" id="<?= $tenant->tenant_id ?>" data-target="#rent_due_log_model" class="rent_balance_due_is"> <?php echo $tenant->due_rent_amount; ?> </a> </li>
                                        <li> <a href="javascript:;" name="" data-toggle="modal" id="<?= $tenant->tenant_id ?>" data-target="#account_info_model" class="vendor_account_info_is"> Account info </a> </li>
                                        <li> <a  href="<?= site_url("account/billing") ?>/<?= $tenant->slug ?>#tabs-2"> Payment History </a> </li>
                                    </ul>

                                    <h2 class="lease-doc">Lease Document  <span class="h2_span">*You can add your lease now or later. However, your tenant will not be able to set up mobile rent payment without a lease on file.</span></h2>
                                    <ul class="upload-file-cu upload-file"> 
                                        <?php $doc = lease_docs($val->prop_id, $tenant->tenant_id, '0'); ?>
                                        <?php if (!empty($doc)) { ?>
                                            <?php foreach ($doc as $file) { ?>
                                                <li>
                                                    <span class="crossicon" id="<?php echo $file->id ?>">X</span>
                                                    <a class="docname" href="<?= $this->config->item('uploads') ?>lease_documents/<?php echo $file->lease_doc; ?>" target="_blank">
                                                        <img alt="" src="<?= $this->config->item('templateassets') ?>images/file-upload.png">
                                                        <span><?php echo $file->lease_doc; ?></a></span>
                                                </li>
                                            <?php } ?>
                                        <?php } ?>
                                        <li class="browse"><input type="file" name="file" class="lease-file" id="<?php echo $val->prop_id . "_" . $tenant->tenant_id ?>" /></li>
                                    </ul>
                                    <ul style="float:left" class="btn-group">
                                    <?php if ($tenant->move_status == 0) { ?>
                                        <li><a href="javascript:;" id="<?= $tenant->tenant_id ?>" data-prop='<?= $val->prop_id ?>' class="red-col chkin <?php echo $clss; ?>">Submit</a></li>
                                    <?php } ?>
                                    </ul>

                                </div>       
                                <?php
                            }
                        } else {
                            ?>
                            <!-- <h3 class="tnt"> No tenant found for this property. Click button below to add new tenant.</h3> -->
                        <?php } ?>	
                        <ul class="tenant1 tenant_add_new" id="add-<?= $val->prop_id ?>" style="display:none;">
                         <!--    <li>
                                <ul class="btn-group">
                                    <li></li>
                                </ul>
                            </li> -->
                        </ul>   
                        <?php echo form_open('', array('class' => 'tenant-detai ','action' => '"'.site_url("add-tenant").'"', 'id' => 'new-tenant-' . $val->prop_id)) ?>
                            <?php $tenant_property_info = getAnyTenantInfoByPropertyId($val->prop_id); ?>
                            <?php //echo isset($val->prop_id) ? $val->prop_id : 'Not exist'; ?>
                            <div id="add-tenant-<?= $val->prop_id ?>" style="display:none;">
                                <ul class="my-list border0 tttt2222">
                                    <li>
                                        <span> 
                                            <label class="label1">First name</label>
                                            <input type="text" class="form-control tenant-txtfield" value="" name="first_name" />
                                        </span>
                                        <span>
                                            <label class="label1">Middle Name</label><input type="text" class="form-control tenant-txtfield" value="" name="middle_name"/><label class="label2"></label></span>
                                        <span>
                                            <label class="label1">Last Name</label><input required="true" type="text" class="form-control tenant-txtfield" value="" name="last_name"/><label class="label2"></label></span>
                                        <span class="dob1 span-up">
                                            <span>
                                                <label class="label1">DOB</label>
                                                <input type="text" class="form-control tenant-txtfield dob" value="" name="dob"/>
                                            </span>
                                            <span> 
                                                <label class="label1">Mobile</label>
                                                <input type="text" class="form-control tenant-txtfield phone_us" value="" name="mobile_no"/>
                                                <label class="label2">  </label>
                                            </span> 
                                        </span>
                                         <span>
                                            <label class="label1">Email</label>
                                            <input type="text" class="form-control tenant-txtfield" value="" name="email"/>
                                            <label class="label2"></label>
                                        </span>
                                         <span class="sel-lang-start span-up">
                                            <span>
                                                <label class="label1">Language</label>
                                                <select class="form-control my-txt-fleid" name="preferred_language" >
                                                    <option value="">Select Language</option>
                                                    <option value="English">English</option>
                                                    <option value="Spanish">Spanish</option>
                                                </select>
                                            </span>                                 
                                           <span>
                                                <label class="label1">Lease Start  </label>
                                                <input type="text" class="form-control tenant-txtfield leasefrom"  value="<?php echo isset($tenant_property_info->lease_start_date) ? date('m/d/Y', strtotime($tenant_property_info->lease_start_date)) : ''; ?>" name="lease_start_date"/>
                                            </span>
                                         </span>
                                         
                                          <span class="rentt span-up">
                                                <span> 
                                                    <label class="label1">Lease End</label>
                                                    <input type="text" class="form-control tenant-txtfield leaseto" value="<?php echo isset($tenant_property_info->lease_end_date) ? date('m/d/Y', strtotime($tenant_property_info->lease_end_date)) : ''; ?>" name="lease_end_date"/>
                                                </span>
                                                <span class="currency_text_box curr">
                                                    <label class="label1">Rent</label>
                                                    <span class="dolar-sign"><i class="fa fa-usd" style="font-size:14px"></i></span>
                                                    <input type="text" class="form-control tenant-txtfield" value="<?php echo isset($tenant_property_info->rent_amount) ? $tenant_property_info->rent_amount : ''; ?>" name="rent_amount" />
                                                </span>
                                          </span>
                                          <span class="rent-due span-up">
                                             <span>
                                                <label class="label1">Rent Due</label>
                                                <select class="form-control my-txt-fleid due_date_start_class" required="true"  name="due_date">
                                                    <option value="">Rent Due</option>
                                                    <?php for ($j = 1; $j < 31; $j++) { ?>
                                                                <option <?php echo (isset($tenant_property_info->due_date) && $tenant_property_info->due_date == $j) ? 'selected' : ''; ?> value="<?php echo $j; ?>"><?php echo ordinal($j); ?></option>
                                                    <?php } ?>
                                                </select>
                                            </span>
                                            <span>
                                                <label class="label1">Late Fee Type</label>
                                                <select class="form-control my-txt-fleid late_fee_type_cls" required="true"   name="late_fee_type">
                                                    <option value="">Late Fee type</option>
                                                    <option <?php echo (isset($tenant_property_info->late_fee_type) && $tenant_property_info->late_fee_type == 'Daily charge') ? 'selected' : ''; ?> value="Daily charge">Daily charge</option>
                                                    <option <?php echo (isset($tenant_property_info->late_fee_type) && $tenant_property_info->late_fee_type == 'One time') ? 'selected' : ''; ?> value="One time">One time</option>
                                                </select>
                                            </span>
                                          </span>
                                          
                                           <span class="late-fee span-up">
                                               <span class="late_fee_sh1 currency_text_box curr">
                                                   <label class="label1">Late Fee</label>
                                                   <span class="dolar-sign"><i class="fa fa-usd" style="font-size:14px"></i></span>
                                                   <input type="text" class="form-control tenant-txtfield" value="<?php echo isset($tenant_property_info->late_fee) ? $tenant_property_info->late_fee : ''; ?>" name="late_fee"/>
                                               </span>
                                               <span><label class="label1">Late Fee Start</label>
                                                <select class="form-control my-txt-fleid late_fee_start_class" required="true"  name="late_fee_start">
                                                    <option value="">Late Fee Start</option>
                                                    <?php for ($j = 1; $j < 31; $j++) { ?>
                                                         <?php if(isset($tenant_property_info->late_fee_start) && $tenant_property_info->late_fee_start == $j){ ?> 
                                                                <option selected value="<?php echo $j; ?>"><?php echo ordinal($j); ?></option>
                                                        <?php }else{ ?>
                                                                <option value="<?php echo $j; ?>"><?php echo ordinal($j); ?></option>
                                                          <?php } ?>
                                                    <?php } ?>
                                                </select>
                                            </span>
                                           </span>
                                          
                                        
                                        </li>
                                            <!--<span><label class="label1">Password</label><input type="password" class="form-control tenant-txtfield" value="" name="password"/><label class="label2"></label></span>-->
                                        <div class="sub-panel-head">Pet Info</div>
                                       
                                        <li class="pet-info">
                                            <span><label class="label1">Pet type</label>
                                                <input type="hidden" id="pets1" value="Yes"  name="pets[]" >
                                                <select class="form-control my-txt-fleid"  required="true"  id="pets_type[]"  name="pets_type[]" >
                                                    <option >None</option>
                                                    <option value="dog">Dog</option>
                                                    <option value="cat">Cat</option>
                                                    <option value="other">Other</option>
                                                </select>
                                            </span>
                                            <span>
                                                <label>Pet Name</label>
                                                <input type="text" class="form-control my-txt-fleid valid" placeholder="" id="pets_name[]"  name="pets_name[]" >  
                                            </span>
                                             <span class="currency_text_box curr">
                                                <label>Pet Fee</label>
                                                <span class="dolar-sign"><i class="fa fa-usd" style="font-size:14px"></i></span>
                                                <input type="text" class="form-control my-txt-fleid valid"  number='true' maxlength="6" digits="true"  placeholder="" id="pets_fee[]"  name="pets_fee[]" >    
                                            </span>
                                            <span class="pet-bred">
                                                <label class="label1">Pet Breed/Description</label>
                                                <input type="text" class="form-control my-txt-fleid" id="pets_breed[]"  name="pets_breed[]" >
                                            </span>
                                            <span>
                                            <label class="label1"> &nbsp;&nbsp;&nbsp;&nbsp;</label>
                                                <input type="hidden" class="pets_no" name="pets_no" value="1">
                                                <a href="javascript:;" class="add-pet-btn"><i class="fa fa-plus"></i>Add Pet</a>
                                            </span>
                                        </li>
                                        <div class="sub-panel-head">Emergency Contact</div>
                                        <li class="em-cont">
                                            <span>
                                                <label>Relationship</label>
                                                <select class="form-control my-txt-fleid relationship" id="relationship" name="relationship">
                                                    <option value="">Select Relationship</option>
                                                    <option value="Wife">Wife</option>
                                                    <option value="Husband">Husband</option>
                                                    <option value="Mother">Mother</option>
                                                    <option value="Father">Father</option>
                                                    <option value="Friend">Friend</option>
                                                    <option value="Other">Other</option>
                                                </select>
                                            </span>
                                            <span class="em-name"><label class="label1">Emergency Name</label>
                                                <input type="text" required="true" customvalidation="true" maxlength="20" class="form-control my-txt-fleid valid emergency_name" id="emergency_name" placeholder="Name" name="emergency_name" value=""/>
                                            </span>
                                            <span class="em-ph">
                                                <label class="label1">Emergency Phone</label>
                                                <input type="text"  class="form-control my-txt-fleid valid phone_us emergency_phone" id="emergency_phone" placeholder="Phone" name="emergency_phone" value=""/>
                                            </span>
                                            <span>
                                            <label class="label1">Emergency Email</label>
                                                <input type="email" class="form-control my-txt-fleid valid emergency_email" id="emergency_email" placeholder="Email" name="emergency_email" value=""/>
                                            </span>
                                        </li>
                                        <input type="hidden" name="property_id" value="<?= $val->prop_id ?>">
                                </ul>
                                <ul class="form-group up-leaseee">  
                                    <li style="width:100%">
                                        <div class="my-col width50 "> 
                                            <span class="upload-btn">Upload Lease<input type="file" name="upload_lease" class="upl_lease"></span>
                                            <p>*You can add your lease now or later. However, your tenant will not be able to set up mobile rent payment without a lease on file.</p>
                                        </div>                              
                                    </li>
                                    <p class="btn_view_del ls_file_name dash_upload_lease_files_cs"></p>
                                </ul>
                                <a href="javascript:;" class="sky-blue-col save-new-record" data-value="<?= $val->prop_id ?>">Submit</a>
                                <ul class=" tenant1" style="float:left">
                                    <ul class="btn-group ">
                                        <li><a href="javascript:;" class="red-col cancel-new-record" data-value="<?= $val->prop_id ?>">Cancel</a></li>
                                    </ul>
                                </ul>                                
                            </div>
                        </form> 
                                
                        <a href="javascript:;" class="btn-tenant my-btn-tennt add-new-tenant" style="float:left;margin: 20px 0px 0px;" data-value="<?= $val->prop_id ?>"><i class="fa fa-plus"></i>Add New Tenant</a>
                    </div>
                <?php } ?>

            </div>

        </div>
        <?php echo form_close(); ?>
    </div>
</section>



<!--  Renew popup start -->
<div class="modal fade" id="myModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Renew Lease</h4>
            </div>
            <div class="modal-body">
                <?php echo form_open_multipart('', array('class' => 'renew-lease', 'id' => 'renew_lease')) ?>
              <span class="renew-lease-span">  <input type="text" required="true" name="lease_start_date" class="form-control mleaseto" placeholder="Lease Start"></span>
               <span class="renew-lease-span">   <input type="text" required="true" style="margin: 0px 18px;" name="lease_end_date" class="form-control mleasefrom" placeholder="Lease End" id="lease_end_date"> </span>
             <span class="renew-lease-span">     <input type="text" required="true" number="true" name="rent_amount" class="form-control" digits="true" placeholder="Rent Amount"> </span>
             <span class="renew-lease-span">     <select required="true" name="due_date" class="form-control my-txt-fleid width50 due_date_start_class">
                    <option value="">Rent due</option>
                    <?php for ($i = 1; $i < 31; $i++) { ?>
                        <option value="<?php echo $i ?>"><?php echo ordinal($i); ?></option>
                    <?php } ?>
                </select></span>
                <span class="renew-lease-span">   <select name="late_fee_type" required="true" class="form-control width50 my-txt-fleid">
                    <option value="">Late Fee type</option>
                    <option value="Daily charge">Daily charge</option>
                    <option value="One time">One time</option>
                </select></span>
               <span class="renew-lease-span">   <input type="text" required="true" number="true" name="late_fee" digits="true" placeholder="Late Fee" ></span>
               
                <input type="hidden" name="tennant_id" id="tennant_id" value="">
                <input type="hidden" name="properti_id" id="properti_id" value="">
                <div class="divider"></div>
                <span class="btn btn-default btn-file">
                    <span aria-hidden="true" class="glyphicon glyphicon-download-alt"></span> Upload Documents 
                    <input name="file" id="file" type="file">
                </span>

            </div>
            <div class="modal-footer" style="border:none !important">
                <button type="submit" class="btn btn-primary">Submit</button>

            </div>
            <?php echo form_close(); ?>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- renew popup end -->
<!--  Moveout popup start -->
<div class="modal fade" id="myModal2">
    <div class="modal-dialog move-model">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                <h4 class="modal-title moveout">Tenants(Move Out)
                    <a href="javascript:void(0);" class="select-all">
                        <label style="cursor:pointer" for="selalltnt">Select all</label> <input type="checkbox" class="select-all-check" id="selalltnt" /></a>
                </h4>
            </div>
            <div class="modal-body">
                <?php echo form_open_multipart('', array('class' => 'renew-lease', 'id' => 'move_out')) ?>

                <ul class="move-list">
                    <?php if(!empty($active_tenants)){ ?>
                        <?php foreach ($active_tenants as $active): ?>
                             <li>  
                                 <p>    
                                     <input type="checkbox" name="tenants[]" class="seltnt" id="atnt<?php echo $active->tenant_id ?>" value="<?php echo $active->tenant_id ?>" />    
                                     <label for="atnt<?php echo $active->tenant_id ?>"><?php echo ucfirst($active->first_name)." ".$active->last_name ?></label>  
                                 </p>
                             </li>
                        <?php endforeach; ?>
                    <?php } ?>
                </ul>
                <textarea class="my-txtarea-part" name="message" placeholder="Write Message..."></textarea>
                <p class="check_error"></p>
            </div>
            <div class="modal-footer" style="border:none !important">
                <button type="button" class="btn btn-primary mvouttnt">Submit</button>

            </div>
            <?php echo form_close(); ?>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!--  Movein popup start -->
<div class="modal fade" id="myModal3">
    <div class="modal-dialog move-model">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                <h4 class="modal-title moveout">Tenants(Move In)
                    <a href="javascript:void(0);" class="select-all">
                        <label style="cursor:pointer" for="selalltntm">Select all</label> <input type="checkbox" class="select-all-checkm" id="selalltnt" /></a>
                </h4>
            </div>
            <div class="modal-body">
                <?php echo form_open_multipart('', array('class' => 'renew-lease', 'id' => 'move_in')) ?>

                <ul class="move-list">
                    <?php if(!empty($inactive_tenants)){ ?>
                        <?php foreach ($inactive_tenants as $inactive): ?>
                             <li>  
                                 <p>    
                                     <input type="checkbox" name="tenants[]" class="seltntm" id="atntm<?php echo $inactive->tenant_id ?>" value="<?php echo $inactive->tenant_id ?>" />    
                                     <label for="atntm<?php echo $inactive->tenant_id ?>"><?php echo ucfirst($inactive->first_name)." ".$inactive->last_name ?></label>  
                                 </p>
                             </li>
                        <?php endforeach; ?>
                    <?php } ?>
                </ul>
                <textarea class="my-txtarea-part mtxt" name="message" placeholder="Write Message..."></textarea>
                <p class="check_errorm"></p>
            </div>
            <div class="modal-footer" style="border:none !important">
                <button type="button" class="btn btn-primary mvintnt">Submit</button>

            </div>
            <?php echo form_close(); ?>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script type="text/javascript">
    $(document).ready(function(){
       $(".rent_balance_due_is").click(function(){ // Click to only happen on announce links
         $("#issue_tenant_id").val($(this).data('id'));
         //$('#rent_due_log_model').modal('show');
       });
       });
       
</script>

<div class="modal fade" id="rent_due_log_model">
    <div class="modal-dialog move-model">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <div class="due_head">Rent Balance</div>
            </div>
            <div class="modal-body">
                <div class="rent_due_log_model_body">
                </div>
                <div class="account_info_model_body_issuse">
                    <form method="post" class="issue_memo_form"  'action'='<?php echo site_url("tenant/addmemo"); ?>' id="issue_memo_form"  name="issue_memo_form">
                        <label for="issue_memo">Issue Memo</label>
                        <select style="width:100%" name="issue_memo" class="issue_memo form-control">
                            <option value="credit_memo"> Issue Credit </option>
                            <option value="debit_memo"> Add Charge </option>
                        </select><br><br>
                        <label for="issue_amount">Amount</label>
                        <input type="hidden" name="issue_tenant_id" value="" id="issue_tenant_id" >
                        <input style="width:100%" class="form-control" type="issue_amount" name="issue_amount" required="true"><br><br><br><br><br><br><br><br><br><br><br><br>
                        <input  data-value="" style="width:100%" type="button" class="issue_submit btn-tenant" name="issue_submit"  value="update" class="issue_submit"><br><br>
                    </form>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="account_info_model">
    <div class="modal-dialog move-model">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <div class="account_info_head">Account Info</div>
            </div>
            <div class="modal-body">
                <div class="account_info_model_body">
                </div>

                
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
