<script type="text/javascript" src="<?= $this->config->item('templateassets') ?>js/jquery.validate.min.js"></script>  
<script>
    $(document).ready(function () {

        $(".rent_balance_due_is").click(function () {
            var id = $(this).attr('id');
            $("#issue_tenant_id").val(id);
            $("#wait-div").css('z-index',10000);
            //alert(id);
             $.ajax({
                    url: '<?php echo base_url('tenant/getRentDueAmount'); ?>',
                    type: 'POST',
                    data: {id: id},
                    beforeSend: function () {
                        $('#wait-div').show();
                    },
                    success: function (data) {
                        $("#wait-div").hide();
                        $(".rent_due_log_model_body").html(data);
                    },
                    error: function () {
                       
                        $("#wait-div").hide();
                        swal("Error!", "Server not responding.", "error");
                    }
                });
            });
        $(".vendor_account_info_is").click(function () {
            var id = $(this).attr('id');
             $.ajax({
                    url: '<?php echo base_url('tenant/getTenantAccountInfo'); ?>',
                    type: 'POST',
                    data: {id: id},
                    beforeSend: function () {
                        $('#wait-div').show();
                    },
                    success: function (data) { 
                        var data = JSON.parse(data);
                        $("#wait-div").hide();                       
                        $(".account_info_model_body").html(data.html);
                        $("#issue_tenant_id").val(data.tenant_id);
                    },
                    error: function () {
                        $("#wait-div").hide();
                        swal("Error!", "Server not responding.", "error");
                    }
                });
            });

        $(".redrct").click(function () {
            var prop = $(this).attr('id');
          
            window.location.href = '<?php echo base_url() ?>/tenants/#tabs-' + prop;
            var type = window.location.hash.substr(1);
            if (type != '') {
                location.reload();
            }
        });

        $(".mvout_btn").click(function () {
            var id = $(this).attr('id');
            $("#atnt" + id).prop('checked', true);
        });
        $('#myModal2').on('hidden.bs.modal', function () {
            $(".seltnt").prop('checked', false);
        })
        $(".select-all-check").click(function () {
            if ($(this).is(':checked')) {
                $(".seltnt").prop('checked', true);
            } else {
                $(".seltnt").prop('checked', false);
            }

        });

        $(".seltnt").click(function () {
            if ($(this).is(':checked')) {
                $(".check_error").text('');
                $(".check_error").hide();
            }
        });


        $(".mvouttnt").click(function () {
            var atLeastOneIsChecked = false;
            var sms = $.trim($(".my-txtarea-part").val());
            $('.seltnt').each(function () {
                if ($(this).is(':checked')) {
                    atLeastOneIsChecked = true;
                }
            });
            if (atLeastOneIsChecked == false) {
                $(".check_error").text('Please check at least one tenant');
                $(".check_error").show();
                return false;
            }
            else if (sms.length == 0) {
                $(".check_error").text('Please enter your messages');
                $(".check_error").show();
                return false;
            } else {
                $(".check_error").text('');
                $(".check_error").hide();
                $.ajax({
                    url: '<?php echo base_url(); ?>tenant/move_out',
                    type: 'POST',
                    data: $("#move_out").serialize(),
                    beforeSend: function () {
                        $('#myModal2').modal('toggle');
                        $('#wait-div').show();
                    },
                    success: function (data) {
                        $("#wait-div").hide();

                        swal({
                            title: "Success",
                            text: "Tenant(s) moved out successfully.",
                            type: "success"
                        },
                        function () {
                            location.reload();
                        });

                    },
                    error: function () {
                        $("#wait-div").hide();
                        alert("error in form submission");
                    }
                });
            }
        });



        $(".mvin_btn").click(function () {
            var id = $(this).attr('id');
            $("#atntm" + id).prop('checked', true);
        });
        $('#myModal3').on('hidden.bs.modal', function () {
            $(".seltntm").prop('checked', false);
        })
        $(".select-all-checmk").click(function () {
            if ($(this).is(':checked')) {
                $(".seltntm").prop('checked', true);
            } else {
                $(".seltntm").prop('checked', false);
            }

        });

        $(".seltntm").click(function () {
            if ($(this).is(':checked')) {
                $(".check_errorm").text('');
                $(".check_errorm").hide();
            }
        });


        $(".mvintnt").click(function () {
            var atLeastOneIsChecked = false;
            var sms = $.trim($(".mtxt").val());
            $('.seltntm').each(function () {
                if ($(this).is(':checked')) {
                    atLeastOneIsChecked = true;
                }
            });
            if (atLeastOneIsChecked == false) {
                $(".check_errorm").text('Please check at least one tenant');
                $(".check_errorm").show();
                return false;
            }
            else if (sms.length == 0) {
                $(".check_errorm").text('Please enter your messages');
                $(".check_errorm").show();
                return false;
            } else {
                $(".check_errorm").text('');
                $(".check_errorm").hide();
                $.ajax({
                    url: '<?php echo base_url(); ?>tenant/move_in',
                    type: 'POST',
                    data: $("#move_in").serialize(),
                    beforeSend: function () {
                        $('#myModal3').modal('toggle');
                        $('#wait-div').show();
                    },
                    success: function (data) {
                        $("#wait-div").hide();

                        swal({
                            title: "success",
                            text: "tenant(s) moved in sucsessfully ",
                            type: "success"
                        },
                        function () {
                            location.reload();
                        });

                    },
                    error: function () {
                        $("#wait-div").hide();
                        alert("error in form submission");
                    }
                });
            }
        });

        $('.lease-file').change(function (e) {
            var file_data = $(this).prop('files')[0];
            var prop_id = $(this).attr('id');
            var form_data = new FormData();
            form_data.append('file', file_data);
            form_data.append('prop_id', prop_id);
            $.ajax({
                url: '<?php echo base_url() ?>tenant/upload_doc', // Url to which the request is send
                dataType: 'text', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                type: 'post',
                beforeSend: function () {
                    $('#wait-div').show();
                },
                success: function (response) {
                    response = JSON.parse(response);
                    $('#wait-div').hide();
                    if (response.success == true) {
                        var html = '<li><a target="_blank" href="' + siteUrl + '/assets/uploads/lease_documents/' + response.success_message + '" class="docname">' +
                                '<img src="<?php echo base_url() ?>assets/default/images/file-upload.png" alt=""><span>' + response.success_message + '</span></a></li>';
                        $("#" + prop_id).parent().parent().prepend(html);
                    }else{
                        alert(response.error_message)
                    }
                },
                error: function () {
                    $('#wait-div').hide();
                    alert('server error');
                }
            });


        });

        $(document).on("click", ".all-tabs-panel-li", function () {

            $('.all-tabs-panel-li').removeClass('active');
            $(this).addClass('active');
        });

        $(document).on('click', '.crossicon', del_document);
        function del_document(e)
        {
            var id = $(this).attr('id');
            swal({
                title: "Alert?",
                text: "Are you sure to delete this document?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            },
            function () {
                $.ajax({
                    url: '<?php echo base_url('leases/delete_document'); ?>',
                    type: 'POST',
                    data: {id: id},
                    beforeSend: function () {
                        $('#wait-div').show();
                    },
                    success: function (data) {
                        $("#wait-div").hide();
                        swal({
                            title: "Deleted",
                            text: "Deleted Successfully",
                            type: "success"
                        },
                        function () {
                            $(e.target).closest('li').remove();
                        });
                    },
                    error: function () {
                        $("#wait-div").hide();
                        swal("Error!", "Server not responding.", "error");
                    }
                });
            });
        }

        $(".chk_in").click(function () {
            var id = $(this).attr('id');
            var prop = $(this).attr('data-prop');
            $.ajax({
                url: '<?php echo base_url('tenant/check_in'); ?>',
                type: 'POST',
                data: {id: id},
                beforeSend: function () {
                    $('#wait-div').show();
                },
                success: function (data) {
                    $("#wait-div").hide();
                    var data = JSON.parse(data);
                    if (data.success == true) {
                        if (data.redirect == true) {
                            swal({
                                title: "Property Pending",
                                text: "Please first complete the property.",
                                type: "success"
                            },
                            function () {
                                window.location.href = data.message;
                                //location.reload();
                            }); 
                        }else{
                            swal({
                                title: "Success",
                                text: "Tenant Checked In",
                                type: "success"
                            },
                            function () {
                                window.location.href = '<?php echo base_url() ?>/tenants/#tabs-' + prop;
                                location.reload();
                            });                            
                        }
                    } else {
                        swal("Error!", data.message, "error");
                    }
                },
                error: function () {
                    $("#wait-div").hide();
                    swal("Error!", "Server not responding.", "error");
                }
            });
        });


    });
</script>
<script>
    $(document).on("click", ".add-new-tenant", function () {
        var propId = $(this).attr('data-value');
        $('#add-tenant-' + propId).show('slow');
        $('#add-' + propId).show('slow');
        $('#s-add-' + propId).show('slow');
        $(this).hide('slow');

    });
    $(document).on("click", ".save-new-record", function () {
        var propId = $(this).attr('data-value');
        add(propId);

    });

    $(document).on("click", ".issue_submit", function () {
        addMemo();

    });

    $(document).on("click", ".cancel-new-record", function () {
        var propId = $(this).attr('data-value');
        $('#add-tenant-' + propId).hide('slow');
        $('.add-new-tenant').show('slow');
        $('#add-' + propId).hide('slow');
        $('#s-add-' + propId).hide('slow');
        //$('.new-tenant-'+propId).trigger('reset');
        $(this).closest("form").trigger('reset');

    });

    $(document).on("click", ".edit-tenant", function () {
        var tenantId = $(this).attr('data-value');
        $('#show-tenant-' + tenantId).hide('slow');
        $('#edit-tenant-' + tenantId).show('slow');
        $('#save-' + tenantId).show('slow');
        $('#edit-' + tenantId).hide('slow');

    });
    $(document).on("click", ".save-record", function () {
        var tenantId = $(this).attr('data-value');
        var prop_id = $(this).attr('data-key');
        update(tenantId,prop_id);

    });
    $(document).on("click", ".cancel-record", function () {
        var tenantId = $(this).attr('data-value');
        $('#show-tenant-' + tenantId).show('slow');
        $('#edit-tenant-' + tenantId).hide('slow');
        $('#save-' + tenantId).hide('slow');
        $('#edit-' + tenantId).show('slow');
        $('#form-' + tenantId).trigger('reset');

    });

    function update(id,prop_id)
    {
        var posturl = siteUrl + 'update-tenant/' + id;
        var sessionId = '<?= $this->session->userdata('MEM_ID') ?>';

        if (id != '' && sessionId != '')
        {
            var first_name = $('#first_name_' + id).val();
            var last_name = $('#last_name_' + id).val();
            var email = $('#email_' + id).val();
            var dob = $('#dob_' + id).val();
            var preferred_lang = $('#preferred_lang_' + id).val();
            var mobile_no = $('#mobile_no_' + id).val();
            var lease_start_date = $('#lease_start_date_' + id).val();
            var lease_end_date = $('#lease_end_date_' + id).val();
            var rent_amount = $('#rent_amount_' + id).val();
            var due_date = $('#due_date_' + id).val();
            var late_fee = $('#late_fee_' + id).val();
            var late_fee_type = $('#late_fee_type_' + id).val();
            var property_id = $('#property_id_' + id).val();
            var lease = $('#lease_' + id).val();
            var ccode = $('#ccode_' + id).val();
            //var form_data = $('#'+id).serialize();
            var data_info = $('#tenant_edit_' + id).serialize();
            
            $.ajax({
                url: posturl,
                //data: {first_name: first_name, last_name: last_name, email: email, dob: dob, preferred_lang: preferred_lang, mobile_no: mobile_no, lease_start_date: lease_start_date, lease_end_date: lease_end_date, rent_amount: rent_amount, due_date: due_date, late_fee: late_fee, property_id: property_id, lease: lease, ccode: ccode, late_fee_type: late_fee_type},
                data: data_info,
                dataType: 'json',
                type: "POST",
                beforeSend: function () {
                    //$('#wait-div').show();
                },
                success: function (data) {
                    $('#wait-div').hide();
                    $('.ajax_report').removeClass('alert-success').removeClass('alert-danger').fadeIn(200);
                    if (data.success)
                    {
                        $('.ajax_report').addClass('alert-success').children('.ajax_message').html(data.success_message);

                        var dataToAppend = '<li>';
                        dataToAppend += '<span> <label class="label1">First name</label><label class="label2">' + data.rec.first_name + '</label></span>';
                        dataToAppend += '<span> <label class="label1">Middle name</label><label class="label2">' + data.rec.middle_name + '</label></span>';
                        dataToAppend += '<span><label class="label1">Last Name</label><label class="label2">' + data.rec.last_name + '</label></span>';
                        dataToAppend += '<span><label class="label1">DOB</label><label class="label2">' + data.dob + '</label></span>';
                        dataToAppend += '<span> <label class="label1">Mobile</label><label class="label2">' + data.rec.mobile_no + '</label></span>';
                        dataToAppend += '</li>';
                        dataToAppend += '<li>';
                        dataToAppend += '<span><label class="label1">Language</label><label class="label2">' + data.rec.preferred_language + '</label></span>';
                        dataToAppend += '<span><label class="label1">Email</label><label class="label2">' + data.rec.email + '</label></span>';
                        dataToAppend += '<span><label class="label1">Lease Start  </label><label class="label2">' + data.lease_start_date + '</label></span>';
                        dataToAppend += '<span> <label class="label1">Lease End</label><label class="label2">' + data.lease_end_date + '</label></span>';
                        dataToAppend += '<span><label class="label1">Rent</label><label class="label2">' + data.leaseDeatil.rent_amount + '</label></span>';
                        dataToAppend += '</li>';
                        dataToAppend += '<li>';
                        dataToAppend += '<span><label class="label1">Due Date</label><label class="label2">' + data.due_date + '</label></span>';
                        if (data.leaseDeatil.late_fee_type != '') {
                            dataToAppend += '<span><label class="label1">Late Fee Type</label><label class="label2">' + data.leaseDeatil.late_fee_type + '</label></span>';
                            dataToAppend += '<span><label class="label1">Late Fee</label><label class="label2">$' + data.leaseDeatil.late_fee + '</label></span>';
                            dataToAppend += '<span><label class="label1">Late Fee Start</label><label class="label2">' + data.leaseDeatil.late_fee_start + 'th</label></span>';
                        }else{
                            dataToAppend += '<span><label class="label1">Late Fee Type</label><label class="label2">N/A</label></span>';
                        }
                        dataToAppend += '<span><label class="label1">User Name</label><label class="label2">' + data.rec.slug + '</label></span>';
                        dataToAppend += '</li>';
                        var pets_type_arr = data.leaseDeatil.pets_type;
                        var pets_name_arr = data.leaseDeatil.pets_name;
                        var pets_fee_arr = data.leaseDeatil.pets_fee;
                        var pets_breed_arr = data.leaseDeatil.pets_breed;
                        var pets_typeArr = pets_type_arr.split(",");
                        var pets_nameArr = pets_name_arr.split(",");
                        var pets_feeArr = pets_fee_arr.split(",");
                        var pets_breedArr = pets_breed_arr.split(",");
                        for (i = 0; i < pets_typeArr.length; i++) { 
                            dataToAppend += '<li><div class="sub-panel-head">Pets info</div>';
                            dataToAppend += '<span><label class="label1">Pet Type</label><label class="label2">' + pets_typeArr[i] + '</label></span>';
                            dataToAppend += '<span><label class="label1">Pet Name</label><label class="label2">' + pets_nameArr[i] + '</label></span>';
                            dataToAppend += '<span><label class="label1">Pet Fee</label><label class="label2">' + pets_feeArr[i] + '</label></span>';
                            dataToAppend += '<span><label class="label1">Pet Breed/Description</label><label class="label2">' + pets_breedArr[i] + '</label></span>';
                            dataToAppend += '</li>';
                        }

                        dataToAppend += '<li><div class="sub-panel-head">Emergency contact</div>';
                        dataToAppend += '<span><label class="label1">Emergency Name</label><label class="label2">' + data.leaseDeatil.emergency_name + '</label></span>';
                        dataToAppend += '<span><label class="label1">Emergency Phone</label><label class="label2">' + data.leaseDeatil.emergency_phone + '</label></span>';
                        dataToAppend += '<span><label class="label1">Emergency Email </label><label class="label2">' + data.leaseDeatil.emergency_email + '</label></span>';
                        dataToAppend += '</li>';

                        $('#show-tenant-' + id).html('');
                        $('#show-tenant-' + id).append(dataToAppend);


                        $('#save-' + id).hide('slow');
                        $('#edit-' + id).show('slow');

                        $('#show-tenant-' + id).show('slow');
                        $('#edit-tenant-' + id).hide('slow');
                        window.location.href = '<?php echo base_url() ?>/tenants/#tabs-' + prop_id;
                    }
                    else
                    {
                        $('.ajax_report').addClass('alert-danger').children('.ajax_message').html(data.error_message);
                    }
                    if (data.scrollToElement)
                        scrollToElement('.ajax_report', 1000);
                    setTimeout(function () {
                        $('.ajax_report').fadeOut(500);
                    }, 1000);
                },
                error: function (data) {
                    alert("Server Error.");
                    return false;
                }
            });
        }
        else
        {
            $.post(posturl, {id: value, vote: task});
            $("#log-in").trigger("click");
        }
        return false;
    }

    $(document).on('click', '.add-pet-btn', function (event) {
        var assets = '<?= $this->config->item('templateassets') ?>';
        var pets_no = $('.pets_no').val();
        pets_no = parseInt(pets_no) + 1;
        var assets = '<?= $this->config->item('templateassets') ?>';

        var dataToAppend = '<li class="pet-info">';
            /*dataToAppend += '<div class="my-col width50 ">';
                dataToAppend += '<input type="hidden" name="pets[]" id="pets[]">';
            dataToAppend += '</div>';
            dataToAppend += '<div class="my-col width50">';
                dataToAppend += '<select name="pets_type[]" id="pets_type[]" class="form-control my-txt-fleid width50 right-pull mr-rite0 sel_lt_fee_up"><option value="">Select Type</option><option value="dog">Dog</option><option  value="cat">Cat</option><option value="other">Other</option></select>';
            dataToAppend += '</div>';
            dataToAppend += '<div class="my-col width100 ">';
                dataToAppend += '<input type="text" name="pets_breed[]" value="" id="pets_breed[]" class="form-control my-txt-fleid valid">';
            dataToAppend += '</div>  ';
            dataToAppend += '<div class="my-col width50">';
                dataToAppend += '<a href="javascript:;" class="delete-pets-row " id="pets_row[]" data-value="" style=""><img src="' + assets + 'images/cross.png" alt=""/></a>';
            dataToAppend += '</div>';*/

          dataToAppend += '<span>';
            dataToAppend += '<label class="label1">Pet type</label>';
                dataToAppend += '<input type="hidden" id="pets1" value="Yes" name="pets[]">';
                dataToAppend += '<select class="form-control my-txt-fleid" required="true" id="pets_type[]" name="pets_type[]">';
                    dataToAppend += '<option>None</option>';
                    dataToAppend += '<option value="dog">Dog</option>';
                    dataToAppend += '<option value="cat">Cat</option>';
                    dataToAppend += '<option value="other">Other</option>';
                dataToAppend += '</select>';
            dataToAppend += '</span>';
            dataToAppend += '<span>';
                dataToAppend += '<label>Pet Name</label>';
                dataToAppend += '<input type="text" class="form-control my-txt-fleid valid" placeholder="" id="pets_name[]" name="pets_name[]">  ';
            dataToAppend += '</span>';
             dataToAppend += '<span class="currency_text_box curr">';
                dataToAppend += '<label>Pet Fee</label>';
                dataToAppend += '<span class="dolar-sign"><i class="fa fa-usd" style="font-size:14px"></i></span><input type="text" class="form-control my-txt-fleid valid" number="true" maxlength="6" digits="true" placeholder="" id="pets_fee[]" name="pets_fee[]">    ';
            dataToAppend += '</span>';
            dataToAppend += '<span class="pet-bred">';
                dataToAppend += '<label class="label1">Pet Breed/Description</label>';
                dataToAppend += '<input type="text" class="form-control my-txt-fleid" id="pets_breed[]" name="pets_breed[]">';
            dataToAppend += '</span>';
            dataToAppend += '<span class="">';
                dataToAppend += '<label class="label1"> </label>';
                dataToAppend += '<a href="javascript:;" class="delete-pets-row " id="pets_row[]" data-value="" style=""><img  style="padding-top:18px;" src="' + assets + 'images/cross.png" alt=""/></a>';
            dataToAppend += '</span>';
        dataToAppend += '</li>';
        

        $(this).closest('li').after(dataToAppend);
        $('.pets_no').val(pets_no);

    });

    $(document).on('click', '.delete-pets-row', function () {
        $(this).closest('li').remove();
    });

    function add(propId)
    {
        var posturl = siteUrl + 'add-tenant';
        var sessionId = '<?= $this->session->userdata('MEM_ID') ?>';
        var prop_id = propId;
        if (sessionId != '')
        {
            var data = $('#new-tenant-' + propId).serialize();
            $('#new-tenant-' + propId).ajaxSubmit({
                url: posturl,
                //data: data,
                dataType: 'json',
                //type: "POST",
                beforeSend: function () {
                    $('#wait-div').show();
                },
                success: function (data) {
                    $('#wait-div').hide();
                    $('.ajax_report').removeClass('alert-success').removeClass('alert-danger').fadeIn(200);
                    if (data.success)
                    {
                        swal({
                            title: "Success",
                            text: 'Tenant Checked In',
                            type: "success"
                        },
                        function () {
                            window.location.href = '<?php echo base_url() ?>/tenants/#tabs-' + prop_id;
                            location.reload();
                        });
                    }
                    else
                    {
                        swal("Error!", data.error_message, "error");
                    }
                },
                error: function (data) {
                    alert("Server Error.");
                    return false;
                }
            });
        }
        else
        {
            $.post(posturl, {id: value, vote: task});
            $("#log-in").trigger("click");
        }
        return false;
    }

    function addMemo()
    {
        var posturl = siteUrl + 'tenant/addmemo';
        var sessionId = '<?= $this->session->userdata('MEM_ID') ?>';
        if (sessionId != '')
        {
            $('#issue_memo_form').ajaxSubmit({
                url: posturl,
                //data: data,
                dataType: 'json',
                //type: "POST",
                beforeSend: function () {
                    $('#wait-div').show();
                },
                success: function (data) {
                    $('#wait-div').hide();
                    $('.ajax_report').removeClass('alert-success').removeClass('alert-danger').fadeIn(200);
                    if (data.success)
                    {
                        swal({
                            title: "Success",
                            text: 'Updated',
                            type: "success"
                        },
                        function () {
                            //window.location.href = '<?php echo base_url() ?>/tenants/#tabs-' + prop_id;
                            location.reload();
                        });
                    }
                    else
                    {
                        swal("Error!", data.error_message, "error");
                    }
                },
                error: function (data) {
                    alert("Server Error.");
                    return false;
                }
            });
        }
        else
        {
            $.post(posturl, {id: value, vote: task});
            $("#log-in").trigger("click");
        }
        return false;
    }

    jQuery.validator.addMethod("valueNotEquals", function (value, element, arg) {
        return arg != value;
    }, "Value must not equal arg.");
    jQuery.validator.addMethod("customvalidation", function (value, element) {
        return /^[A-Za-z_ -]+$/.test(value);
    }, "Alpha Characters Only.");

    jQuery.validator.addMethod('phoneUS', function (phone_number, element) {
        phone_number = phone_number.replace(/\s+/g, '');
        return this.optional(element) || phone_number.length > 9 &&
                phone_number.match(/^(1-?)?(\([2-9]\d{2}\)|[2-9]\d{2})-?[2-9]\d{2}-?\d{4}$/);
    }, 'Enter a valid phone.');

    jQuery.validator.addMethod("valueNotEquals", function (value, element, arg) {
        return arg != value;
    }, "Value must not equal arg.");


    $(document).on("click", ".all-tabs-panel-li", function () {

        $('.all-tabs-panel-li').removeClass('active');
        $(this).addClass('active');
    });
    $(document).on("click", ".intservices", function () {
        swal("Error!", 'Please confirm your email address. tServices will not be active until confirmation is complete.', "error");
    });

    $(document).ready(function () {

        $("body").delegate(".dob", "focusin", function () {
            $(this).datepicker({changeMonth: true, changeYear: true, maxDate: 0, defaultDate: "01/01/1985", yearRange: "-80:+0"});
        });

        $("body").delegate(".leasefrom", "focusin", function () {
            $(this).datepicker({
                defaultDate: "+1w",
                changeMonth: true,
              //  minDate: 0,
                changeYear: true,
                numberOfMonths: 1,
                onSelect: function (selectedDate) {
                    $(this).parent().parent().next().find('.leaseto').removeAttr('disabled');
                    $(this).parent().parent().next().find('.leaseto').focus();
                    $(this).parent().parent().next().find('.leaseto').blur();
                },
                onClose: function (selectedDate) {
                    $(this).parent().parent().next().find('.leaseto').datepicker("option", "minDate", selectedDate);
                }
            });
        });

        $("body").delegate(".leaseto", "focusin", function () {
            $(this).datepicker({
                defaultDate: "+1w",
                changeMonth: true,
                changeYear: true,
                minDate: 0,
                numberOfMonths: 1,
                onSelect: function (selectedDate) {
                    //   $(this).parent().prev().find('input').focus(); 
                    //   $(this).parent().prev().find('input').blur(); 
                },
                onClose: function (selectedDate) {
                    $(this).parent().parent().prev().find('.leaseto').datepicker("option", "maxDate", selectedDate);
                }
            });
        });

        $("body").delegate(".mleasefrom", "focusin", function () {
            $(this).datepicker({
                defaultDate: "+1w",
                changeMonth: true,
                minDate: $(".mleaseto").val(),
                changeYear: true,
                numberOfMonths: 1,
                onSelect: function (selectedDate) {
                    $(this).parent().next().find('input').removeAttr('disabled');
                    $(this).parent().next().find('input').focus();
                    $(this).parent().next().find('input').blur();
                },
                onClose: function (selectedDate) {
                    $(this).parent().next().find('input').datepicker("option", "minDate", selectedDate);
                }
            });
        });

        $("body").delegate(".mleaseto", "focusin", function () {
            $(this).datepicker({
                defaultDate: "+1w",
                changeMonth: true,
                changeYear: true,
                minDate: $(".mleaseto").val(),
                numberOfMonths: 1,
                onSelect: function (selectedDate) {
                    //   $(this).parent().prev().find('input').focus(); 
                    //   $(this).parent().prev().find('input').blur(); 
                },
                onClose: function (selectedDate) {
                    $(this).parent().prev().find('input').datepicker("option", "maxDate", selectedDate);
                }
            });
        });


        $(".rnw_rel").click(function () {
            var data = $(this).attr('id');
            var date = $(this).attr('data-bind');
            var arr = data.split('-');
            $("#tennant_id").val(arr[1]);
            $("#properti_id").val(arr[2]);
            $(".mleaseto").val(date);
        });
        $("#renew_lease").submit(function (event) {
            event.preventDefault();
        });

        $("#renew_lease").validate({
            submitHandler: function (form) {

                var posturl = siteUrl + 'tenant/renew_lease';
                var formData = new FormData(form);
                // formData.append('file', $('#file')[0].files[0]);

                $.ajax({
                    url: posturl,
                    data: formData,
                    type: "POST",
                    processData: false,
                    contentType: false,
                    beforeSend: function () {
                        $('#wait-div').show();
                    },
                    success: function (data) {
                        $('#wait-div').hide();
                        var myArray = JSON.parse(data);
                        if (myArray.success) {
                            BootstrapDialog.show({
                                message: myArray.message,
                                buttons: [{
                                        label: 'OK',
                                        action: function (dialog) {
                                            location.reload();
                                        }
                                    }]
                            })
                        } else {
                            BootstrapDialog.alert(myArray.message);
                        }
                    },
                    error: function (data) {
                        alert("Server Error.");
                        return false;
                    }
                });
                return false;
            }
        });

        $(".late_fee_type_cls").change(function () {
            if ($(this).val() != '') {
                $(this).parent().next().show();
            } else {
                $(this).parent().next().hide();
            }
        });



    });
    $(document).on('click', '.delte_btn', del_tenant);
    function del_tenant(e)
    {
        var id = $(this).attr('id');
        swal({
            title: "Alert?",
            text: "Are you sure that you want to delete this tenant?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
        },
        function () {
            $.ajax({
                url: '<?php echo base_url('tenant/delete_tenant'); ?>',
                type: 'POST',
                data: {id: id},
                beforeSend: function () {
                    $('#wait-div').show();
                },
                success: function (data) {
                    $("#wait-div").hide();
                    swal({
                        title: "Deleted",
                        text: "Deleted Successfully",
                        type: "success"
                    },
                    function () {
                        $(e.target).closest('.tenant').remove();
                    });
                },
                error: function () {
                    $("#wait-div").hide();
                    swal("Error!", "Server not responding.", "error");
                }
            });
        });
    }

    $(document).delegate('.del_lease_doc', 'click', function () {
        $(this).parent().find('.upl_lease').val('');    
        $(this).parent().html('');
          //  $(".show_file_name2").html('');
            //$(".show_file_name2").hide();
        });
    $(document).ready(function () {
        //$('#wait-div').show();
        $(".upl_lease").change(function () {
            var path = $(this).val();
            var filename = path.replace(/^.*\\/, "");
            //var html = filename + " <img class='clr_up_img del_lease_doc' src='<?php echo base_url() ?>assets/default/images/delete-icon.png'/>";
            //$(".dash_upload_lease_files_cs").html(html);
            var html = " <a class='clr_up_img del_lease_doc' href='javascript:;'> Delete</a><br><br>";
            var reader = new FileReader();
            reader.onload = function(){
                var dataURL = reader.result;
                html_view = '<a href="'+ dataURL +'" target="_blank" class="btn preview_lease"> View </a> ';                    
                $(".dash_upload_lease_files_cs").html(filename + ' ' + html_view + html);
            };
            reader.readAsDataURL(this.files[0]);

            $(".dash_upload_lease_files_cs").show();
        });

        
    });

    $(document).on('change', '.due_date_start_class', function () {
        var due_date_start_class = $(this).val();
        //alert(due_date_start_class)
        $(this).parent().parent().next().find('.late_fee_start_class').empty();
        for (var i = due_date_start_class; i <= 30; i++) {
            $(this).parent().parent().next().find('.late_fee_start_class').append('<option value="'+ i +'">'+i+'th</option>');
        };            
    });

    $(document).on('change', '.late_fee_start_class', function () {
        var late_fee_start_class = $(this).val();
        var due_date_start_class = $(this).parent().parent().prev().find('.due_date_start_class').val();
        //alert(due_date_start_class+' < '+late_fee_start_class);
        if(due_date_start_class > late_fee_start_class){
            //alert(due_date_start_class)
            $('.due_date_start_class option[value="'+late_fee_start_class+'"]').attr('selected', 'selected');        
        }
    });
</script>
