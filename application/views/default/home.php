<link href="<?= $this->config->item('templateassets') ?>css/bootstrap.min.css" rel="stylesheet">
<script type="text/javascript" src="<?= $this->config->item('templateassets') ?>js/bootstrap.min.js"></script> 
<link href="<?= $this->config->item('templateassets') ?>css/bootstrap-dialog.min.css" rel="stylesheet">
<script async type="text/javascript" src="<?= $this->config->item('templateassets') ?>js/bootstrap-dialog.min.js"></script> 

<section class="top-banner">
    <div class="container">
        <div class="row">
            <h2>A Better Way to Manage Properties and Keep Tenants</h2>
            <p class="banner-define">TenantTag’s property management software improves the rental experience for do-it-yourself landlords and their tenants while saving time and money.</p>
            <?php $feature_links = getFeatureLinks(); ?>
            <div class="inckudes">
                <h3>Includes</h3>
                <div class="single-include">
                    <img src="<?= $this->config->item('templateassets') ?>images/landlord-icon.png"> 
                    <div class="content">
                            <h4 class="top1">Landlord Dashboard</h4>
                        <p>One stop management interface </p>
                    </div>
                </div>
                <div class="single-include">
                    <img src="<?= $this->config->item('templateassets') ?>images/mob-app.png"> 
                    <div class="content">
                            <h4 class="top1">Tenant Mobile App</h4>
                        <p>Customized App for each tenant </p>
                    </div>
                </div>
                <div class="single-include">
                     <img src="<?= $this->config->item('templateassets') ?>images/t-text.png">
                    <div class="content">
                            <h4 class="top1">tText</h4>
                        <p>Automated text reminders  </p>
                    </div>
                </div>
                <div class="single-include">
                    <img src="<?= $this->config->item('templateassets') ?>images/maintaennace.png">
                    <div class="content">
                            <h4 class="top1">tMaintenance</h4>
                        <p>Automated maintenance processing </p>
                    </div>
                </div>
                <div class="single-include">
                    <img src="<?= $this->config->item('templateassets') ?>images/tpay.png"> 
                    <div class="content">
                            <h4 class="top1">tPay</h4>
                        <p>Mobile rent payment</p>
                    </div>
                </div>
                <div class="single-include">
                   <img src="<?= $this->config->item('templateassets') ?>images/features.png">
                    <div class="content" style="margin-top:12px">
                            <h4 class="top1">Other Features</h4>
                        <p> </p>
                    </div>
                </div>
            </div>

            <div class=" video-sec ">
                <h4 style="text-decoration:underline">How it Works</h4>
                <video controls width="100%" height="348px"   poster="assets/Tenant_Tag_Video_Final_Cut.png">
                  <source src="assets/Tenant_Tag_Video_Final_Cut.webm" type="video/webm" />
                  <source src="assets/Tenant_Tag_Video_Final_Cut.ogg" type="video/ogg" />
                  <source src="assets/Tenant_Tag_Video_Final_Cut.mp4" type="video/mp4" />
                </video>
            </div>
        </div>
    </div>
</section>


<section class="signup-sec" id="signup-sec">
    <div class="container">
        <div class="text-center">
            <h2>Sign up</h2>
            <p>60 Days Free Trial</p>
        </div>
        <div class="pro-mnth">
            <div class="pro-details">
                <h4>$<?php echo PER_PROPERTY_AMOUNT; ?>/Unit per month</h4>
                <ul>
                    <?php if (!empty($feature_links)) { ?>
                        <?php foreach ($feature_links as $link): ?>
                            <li><?php echo $link->title; ?><span>INCLUDED</span></li>
                        <?php endforeach; ?>
                    <?php } ?>
                </ul>
            </div>
            <div class="signup-form">
                <div class="right-banner right-signup-form">
                    <h4>Sign Up</h4>
                    <?php echo form_open('register', array('class' => '', 'id' => 'sign-up')) ?>
                    <div class="ajax_report alert display-hide" role="alert" style="margin-left:0px;width:320px;margin-bottom:10px;"><span class="close-message"></span><div class="ajax_message">Hello Message</div></div>
                    <div class="form-group1">
                        <div class="my-div"><input type="text" placeholder="First Name" class="form-control my-txt-fleid width50" name="first_name">
                        <input type="text" placeholder="Last Name" class="form-control my-txt-fleid width50 right-pull mr-rite0" name="last_name"></div>
                    </div>
                    <div class="form-group1">
                        <div class="my-div"><input type="text" placeholder="Company" class="form-control my-txt-fleid" name="company_name"></div>
                    </div>
                    <div class="form-group1">
                        <div class="my-div"><p class="check">    <input type="checkbox" id="test1" name="as_company">    <label for="test1">Use company name</label>  </p></div>
                    </div>
                    <div class="form-group1">
                        <div class="my-div"><input type="text" placeholder="Email address" class="form-control my-txt-fleid" name="email"></div>
                    </div>
                    <div class="form-group1">
                        <div class="my-div"><input type="password" id="signPassword" placeholder="Password" class="form-control my-txt-fleid width50" name="password"><input type="password" placeholder="Confirm Password" class="form-control my-txt-fleid width50 right-pull mr-rite0" name="confirm_password"></div>
                    </div>
                    <div id="regions" style="display:none">
                        <div  class="form-group1">
                            <div class="my-div">
                                <select class="form-control my-txt-fleid" name="state" id="state" onChange="getCityByStateId(this.value)">
                                    <option value="">Select State</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group1">
                        <div class="my-div">
                            <input type="text" placeholder="Mobile#" class="form-control my-txt-fleid phone_us" name="mobile" style=""></div>
                    </div>
                    <div class="form-group1">
                        <div class="my-div captcha">
                            <span><?php echo $math_captcha_question;?></span>
                            <input type="text" placeholder="Please answer to confirm you are a real user." class="form-control my-txt-fleid" name="math_captcha"></div>
                    </div>
                    <input type="hidden" value="<?php echo $this->session->userdata('mathcaptcha_answer'); ?>" name="mathcaptcha_answer">
                    <input type="button" class="formsign-bttn signup_btn_popup"  data-toggle="modal" data-target="#TemsAndConditionModal" value="SIGN UP"/> 
                    <div class="modal fade " id="TemsAndConditionModal">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title">Terms & Conditions</h4>
                                </div>
                                <div class="modal-body">  
                                    <div class="form-group1 hide1">
                                        <div class="my-div">
                                            <p class="mycheckbox">   
                                                <input type="checkbox" name="terms" id="" style="position: relative; left: 0px; top: 3px;">
                                                By using our services, you agree to Coastal Investment Group LLC's (dba as TenantTag) <a target="_blank" href="<?php echo base_url('page/terms'); ?>"><span class="changecolor">Term of Service</span></a> and <a target="_blank" href="<?php echo base_url('page/privacy'); ?>"><span class="changecolor">Privacy Policy </span></a>.
                                            
                                            </p>
                                        </div>
                                    </div>                 
                                    <div class="modal-footer terms-and-conditions-btn" style="border:none !important">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </div>
                               
                            </div><!-- /.modal-content -->
                        </div><!-- /.modal-dialog -->
                    </div>
                    <?= form_close(); ?>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="feature-sec">
    <div class="container">
        <h2 class="feat">Features</h2>
        <div class="slider-feature">
            <div class="slider-main">
                <div class="slider-for ">
            		<div class="slide_one">
                        <div class="autoplay slider-auto">
                            <div class="sliderr">
                                <div class="img-main custom">
                                    <img src="<?= $this->config->item('templateassets') ?>images/tab3.png">
                                </div>
                            </div>
                            <div class="sliderr">
                                <div class="img-main custom">
                                    <img src="<?= $this->config->item('templateassets') ?>images/tab2.png">
                                </div>
                            </div>
                            <div class="sliderr">
                                <div class="img-main custom">
                                    <img src="<?= $this->config->item('templateassets') ?>images/tab1.png">
                                </div>
                            </div>
                        </div>
            			<div class="cont-main custom-col-main">
                            <p>Have you needed details about your property, a copy of a lease, or vendor contact information but didn't have it with you? Our Landlord Dashboard puts your property, tenant, vendor, and other management information at your finger tips whenever you need it, wherever you need it.</p>
                            <a data-toggle="modal"  data-id="1" data-target="#common_question" class="com_question_link"  href="javascript:;">Common Questions <img src="<?= $this->config->item('templateassets') ?>images/arr.png">  </a>
                        </div>
                    </div>
                    <div class="slider_two">
                        <div class="autoplay slider-auto">
                            <div class="sliderr">
                            <div class="img-main">
                            <img src="<?= $this->config->item('templateassets') ?>images/App-1.png">
                            </div>

                            </div>
                            <div class="sliderr">
                            <div class="img-main">
                            <img src="<?= $this->config->item('templateassets') ?>images/App-3.png">
                            </div>
                          

                            </div>
                        </div>
            			 <div class="cont-main">
                            <p>Has a tenant ever asked you for information that they should have such as a lease copy, phone number, or garbage pick-up day? The Tenant Mobile App gives your tenants a customized portal with the information they need, when they need it, in order to minimize unnecessary phone calls, emails and texts to you.</p>
                            <a data-toggle="modal"  data-id="5" data-target="#common_question" class="com_question_link" href="javascript:;">Common Questions <img src="<?= $this->config->item('templateassets') ?>images/arr.png">  </a>
                        </div>
                    </div>
                    <div class="slider_tow">
                        <div class="autoplay slider-auto">
                            <div class="sliderr">
                                <div class="img-main">
                                    <img src="<?= $this->config->item('templateassets') ?>images/tText1.png">
                                </div>
                            </div>
                            <div class="sliderr">
                                <div class="img-main">
                                    <img src="<?= $this->config->item('templateassets') ?>images/tText2.png">
                                </div>
                            </div>
                            <div class="sliderr">
                                <div class="img-main">
                                    <img src="<?= $this->config->item('templateassets') ?>images/tText3.png">
                                </div>
                            </div>          
                        </div>
            			<div class="cont-main">
                            <p>Are you hesitant to communicate with a tenant because they may ask for other improvements?  tText improves communication with tenants while avoiding this situation by delivering automated text reminders whose content is either pulled from property, tenant, and lease information or is customized by you.</p>
                            <a data-toggle="modal" data-id="4"  data-target="#common_question" class="com_question_link"  href="javascript:;">Common Questions <img src="<?= $this->config->item('templateassets') ?>images/arr.png">  </a>
                        </div>
                    </div>
                    <div class="slider_tow">
                        <div class="autoplay slider-auto">
                            <div class="sliderr">
                                <div class="img-main">
                                    <img src="<?= $this->config->item('templateassets') ?>images/tMaint1.png">
                                </div>
                            </div>
                            <div class="sliderr">
                                <div class="img-main">
                                    <img src="<?= $this->config->item('templateassets') ?>images/tMaint2.png">
                                </div>
                            </div>      
                        </div>
        			    <div class="cont-main">
                            <p>Have you been busy or out town when your tenant requests maintenance? tMaintenance automatically sends maintenance requests to you and your preferred vendors when a request is made through the Tenant Mobile App.</p>
                            <a data-toggle="modal" data-id="2"  data-target="#common_question" class="com_question_link"  href="javascript:;">Common Questions <img src="<?= $this->config->item('templateassets') ?>images/arr.png">  </a>
                        </div>
                    </div>
                    <div class="slider_tow">
                        <div class="autoplay slider-auto">
                            <div class="sliderr">
                                <div class="img-main">
                                    <img src="<?= $this->config->item('templateassets') ?>images/tPay1.png">
                                </div>
                            </div> 
                            <div class="sliderr">
                                <div class="img-main">
                                    <img src="<?= $this->config->item('templateassets') ?>images/tPay3.png">
                                </div>
                            </div> 
                            <div class="sliderr">
                                <div class="img-main">
                                    <img src="<?= $this->config->item('templateassets') ?>images/tPay2.png">
                                </div>
                            </div> 
                        </div>
                            <div class="cont-main">
                                <p>Have you ever had to meet a tenant to pick-up rent or been told that rent is late because the tenant is out of town? tPay simplifies rent collection by allowing tenants to pay their rent through their Tenant Mobile App anywhere, at anytime.</p>
                                <a data-toggle="modal" data-id="6"  data-target="#common_question" class="com_question_link"  href="javascript:;">Common Questions <img src="<?= $this->config->item('templateassets') ?>images/arr.png">  </a>
                            </div>

                    </div>

                    <div class="autoplay slider-auto">
                        <div class="sliderr">
                            <div class="img-main">
                                <img src="<?= $this->config->item('templateassets') ?>images/OtherFeat1.png">
                            </div>
                            <div class="cont-main">
                                <p></p>
                            </div>
                        </div>       
                    </div>
                </div>
            </div>
            <div class="slider-sidebar">
                <div class="slider-nav ">
                    <div class="sliderr">
                        <a name="link1"></a> LANDLORD DASHBOARD 
                    </div>
                    <div class="sliderr">
                        <a name="link2"></a>TENANT MOBILE APP   
                    </div>
                    <div class="sliderr">
                        <a name="link3"></a>tTEXT
                    </div>
                    <div class="sliderr">
                        <a name="link4"></a>tMAINTENANCE
                    </div>
                    <div class="sliderr">
                        <a name="link5"></a>tPAY
                    </div>
                    <div class="sliderr">
                        <a name="link6"></a>+OTHER FEATURES
                    </div>
                </div>
            </div>
            <div class="sifnup-noww">
                <a href="#signup-sec">Sign up Now</a>
            </div>
        </div>
    </div>
</section>

<section class="dr-banner" >
    <div class="container">
        <div class="left-banner">
            <h3>Landlords, are you losing good tenants? TenantTag improves tenant communication and management for busy landlords.</h3>

            <ul>	
                <li class="message-img"><h4><span>t</span>Messaging<sup>*Free Trial*</sup></h4>
                    <p>Improves communications with tenants by using as an automated messaging system to deliver customized messages based on property, tenant, and lease information.</p>
                </li>

                <li class="mainstaince-img">
                    <h4><span>t</span>Maintenance<sup>*Free Trial*</sup></h4>
                    <p>Simplifies the submission, delivery, and approval of maintenance requests through an innovative, mobile solution.</p>
                </li>
                <li class="tpay-icon-img">
                    <h4><span>t</span>Pay<sup>*Free Trial*</sup></h4>
                    <p>Simplifies the submission, delivery, and approval of maintenance requests through an innovative, mobile solution.</p>
                </li>
            </ul>
        </div>

        <div class="right-banner right-signup-form">
            <h4>Sign Up</h4>

            <?php echo form_open('register', array('class' => '', 'id' => 'sign-up')) ?>
            <div class="ajax_report alert display-hide" role="alert" style="margin-left:0px;width:320px;margin-bottom:10px;"><span class="close-message"></span><div class="ajax_message">Hello Message</div></div>
            <div class="form-group1">
                <div class="my-div"><input type="text" placeholder="First Name" class="form-control my-txt-fleid width50" name="first_name"><input type="text" placeholder="Last Name" class="form-control my-txt-fleid width50 right-pull mr-rite0" name="last_name"></div>
            </div>
            <div class="form-group1">
                <div class="my-div"><input type="text" placeholder="Company" class="form-control my-txt-fleid" name="company_name"></div>
            </div>
            <div class="form-group1">
                <div class="my-div"><p class="check">    <input type="checkbox" id="test1" name="as_company">    <label for="test1">Use company name</label>  </p></div>
            </div>
            <div class="form-group1">
                <div class="my-div"><input type="text" placeholder="Email address" class="form-control my-txt-fleid" name="email"></div>
            </div>
            <div class="form-group1">
                <div class="my-div"><input type="password" id="signPassword" placeholder="Password" class="form-control my-txt-fleid width50" name="password"><input type="password" placeholder="Confirm Password" class="form-control my-txt-fleid width50 right-pull mr-rite0" name="confirm_password"></div>
            </div>
            <div id="regions" style="display:none">
                <div  class="form-group1">
                    <div class="my-div">
                        <select class="form-control my-txt-fleid" name="state" id="state" onChange="getCityByStateId(this.value)">
                            <option value="">Select State</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group1">
                <div class="my-div">
                    <input type="text" placeholder="Mobile#" class="form-control my-txt-fleid phone_us" name="mobile" style=""></div>
            </div>
            <div class="form-group1 hide">
                <div class="my-div">
                    <p class="">   
                        <input type="checkbox" name="terms" id="" style="position: relative; left: 0px; top: 3px;">
                        Agree to our <a href="<?php echo base_url('terms-and-conditions'); ?>"><span class="changecolor">Terms & Conditions</span></a>
                    </p>
                </div>
            </div>

            <input type="submit" class="formsign-bttn" value="SIGN UP"/> 
            <?= form_close(); ?>
        </div>
    </div>
</section>
<section class="dr-content ">
    <section class="howit-works">
        <div class="container">
            <div class="how-works-inner">
                <h2>How it works</h2>
                <div class="one-third-wrap">
                    <div class="one-third">
                        <div class="icon">
                            <img src="<?= $this->config->item('templateassets') ?>images/w1.png">
                        </div>
                        <p>ADD PROPERTY</p>
                    </div>
                    <div class="one-third">
                        <div class="icon">
                            <img src="<?= $this->config->item('templateassets') ?>images/w2.png">
                        </div>
                        <p>ADD TENANT</p>
                    </div>
                    <div class="one-third last">
                        <div class="icon">
                            <img src="<?= $this->config->item('templateassets') ?>images/w3.png">
                        </div>
                        <p>ADD tSERVICES</p>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="how-workd-checkout">
                    <div class="tick">
                        <img src="<?= $this->config->item('templateassets') ?>images/tick.png" alt="">
                    </div>
                    <span>Checkout</span>
                </div>
                <div class="download-app">
                    <label>Welcome message to Tenant</label>
                    <a href="javascript:void(0)" style="cursor:default;">Download your app</a>
                </div>
            </div>
            <div class="confirm-approve">
                <div class="one-half">
                    <div class="t-maintain">
                        <div class="t-left">
                        </div>
                        <div class="t-outer">
                            <div class="circle">
                                <img src="<?= $this->config->item('templateassets') ?>images/m1.png" alt="">
                            </div>
                            <p>tMaintenance</p>
                            <a href="<?php echo site_url('page/services'); ?>">More Info</a>
                        </div>
                    </div>
                </div>

                <div class="one-half last">
                    <div class="t-message">
                        <div class="t-outer">
                            <div class="circle">
                                <img src="<?= $this->config->item('templateassets') ?>images/m2.png" alt="">
                            </div>
                            <p>tMessaging</p>
                            <a href="<?php echo site_url('page/services'); ?>">More Info</a>
                        </div>
                        <div class="t-right">
                        </div>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </section>

    <section class="tantent-more">
        <div class="more-icon">
            <a href="javascript:void(0)">MORE</a> 
        </div>
        <div class="tantent-more-outer">
            <div class="container">
                <div class="tantent-more-inner">
                    <ul>
                        <li><img src="<?= $this->config->item('templateassets') ?>images/s1.png" alt=""></li>
                        <li><img src="<?= $this->config->item('templateassets') ?>images/s2.png" alt=""></li>
                        <li><img src="<?= $this->config->item('templateassets') ?>images/s3.png" alt=""></li>
                    </ul>
                    <div class="download-the-app">
                        <p>App for Tenants is available on</p>
                        <div class="icon_container">
                            <span>
                                <img src="<?= $this->config->item('templateassets') ?>images/appstore.png" alt="appStore">  
                            </span>
                            <span>
                                <img src="<?= $this->config->item('templateassets') ?>images/playstore.png" alt="playStore">
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="dr-testimonial">
        <div class="container test-part">
            <h2>Why Use <strong>TenantTag?</strong></h2>
			<ul class="usetenantag">
				<li><img src="<?= $this->config->item('templateassets') ?>images/efficently.png" alt="efficiently">To Manage More Efficiently!</li>
                <li><img src="<?= $this->config->item('templateassets') ?>images/tenaticon.png" alt="tenants">To Retain Tenants!</li>
				<li><img src="<?= $this->config->item('templateassets') ?>images/communication.png" alt="communication">To Improve Communication!</li>
                <li><img src="<?= $this->config->item('templateassets') ?>images/time.png" alt="time">To Save Time!</li>
                <li><img src="<?= $this->config->item('templateassets') ?>images/money.png" alt="money">To Save Money!</li>
                <li><img src="<?= $this->config->item('templateassets') ?>images/life.png" alt="easier">To Make Life Easier!</li>
			</ul>
			<div class="sifnup-noww">
                <a href="#signup-sec">Sign up Now</a>
            </div>
        </div>
    </section>

    <script>
        $(document).ready(function () {
            $(document).delegate('.more-icon a', 'click', function () {
                if ($(".more-icon").hasClass("arrow_up")) {
                    $('.more-icon').removeClass('arrow_up');
                } else {
                    $('.more-icon').addClass('arrow_up');
                }
                $(".tantent-more-outer").slideToggle("slow");
            });
        });
        		
        $('.top1').click(function (e) {
            $('html, body').animate({
                scrollTop: $('.top1-link').offset().top - 20
            }, 'slow');
        });
        $(document).ready(function () {
            $('.com_question_link').click(function () {
                var id = $(this).attr('data-id');
                var postUrl = siteUrl + 'welcome/get_common_question';
                $.ajax({
                    url: postUrl,
                    type: 'POST',
                    data: {id: id},
                    dataType: 'json',
                    beforeSend: function () {
                        $('#wait-div').show();
                    },
                    success: function (response) {                        
                        if (response.success)
                        {
                            $("#common_question").find('.common_question_body').html(response.html);
                        }
                         $('#wait-div').hide();
                    },
                    error: function () {
                         $('#wait-div').hide();
                        alert('server error');
                    },
                });
            });
        });
    </script>
</section>

<div class="modal fade " id="common_question">
    <div class="modal-dialog" style="width: 55%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Common Question</h4>
            </div>
            <div class="modal-body" style="   overflow-y:scroll;  min-height:200px;  max-height: 500px;">
                <div class="common_question_body">
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<script type="text/javascript">
require(["mojo/signup-forms/Loader"], function(L) {
 L.start({"baseUrl":"mc.us13.list-manage.com","uuid":"2baaccb1f2a2f459d49565401","lid":"bede308086"}) 
})
</script>
<script>
jQuery('.autoplay').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  autoplay: true,
  autoplaySpeed: 5000,
  dots:true,
  draggable:false
});

 jQuery('.slider-for').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  fade: true,
  asNavFor: '.slider-nav',
  draggable:false
});
jQuery('.slider-nav').slick({
  slidesToShow: 6,
  slidesToScroll: 1,
  asNavFor: '.slider-for',
  dots: true,
  centerMode: true,
    arrows: false,
  focusOnSelect: true,
  draggable:false
  
});

</script>




