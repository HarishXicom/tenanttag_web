<div id="properties-listing">
    <div class="signup">
        <div class="btn-part">
            <a href="javascript:;" class="<?php if ($type == 'all') { ?>active<?php } else { ?>unactive<?php } ?> left property-by-status" data-value="" style="width:108px;">All </a>
            <a href="javascript:;" class="<?php if ($type == 'vacant') { ?>active<?php } else { ?>unactive<?php } ?> left property-by-status" data-value="vacant" style="width:107px;">Vacant </a>
            <a href="javascript:;" class="<?php if ($type == 'occupied') { ?>active<?php } else { ?>unactive<?php } ?> right property-by-status" data-value="occupied" style="width:107px;">Occupied</a>
        </div>
        <a class="btn-tenant add-property" href="<?= site_url('add-property') ?>"><i class="fa fa-plus"></i>Add Property</a>
    </div>
    <div class="table-content signup-table dashboard-table">
       <!--  <table class="my-table">
            <thead>
                <tr>
                    <th>Address </th>
                    <th>Unit#  </th>
                    <th>Rent Balance</th>
                    <th>Tenants</th>
                    <th>Messages</th>
                    <th>Vendors</th>
                    <th><a href="javascript:;" class="dash-pop tpay">tPay</a></th>
                    <th><a href="javascript:;" class="dash-pop">tMaintenance</a></th>
                    <th><a href="javascript:;" class="dash-pop">tMessaging</a></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($allProperties as $value) { ?>
                            <tr>
                                <td> <a href="<?= site_url('my-properties') ?>#tabs-<?php echo $value->prop_id; ?>">  <?= $value->city ?>, <?= $value->address1 ?> <?= $value->address2 ?> </a> </td>
                                <td> <a href="<?= site_url('my-properties') ?>#tabs-<?php echo $value->prop_id; ?>"><?= $value->unit_number ?> </a> </td>
                                <td>  <?php echo $value->rent_amount; ?> </td> 
                                <td>
                                    <?php if ($value->tenants == 0) { ?>
                                        <a href="javascript:;" id="<?php echo $value->prop_id; ?>" data-toggle="modal" data-target="#addtenantModal" class="<?php if ($value->tenants == 0) { ?>zero<?php } ?>"> <?= $value->tenants ?> </a> 
                                    <?php } else { ?>
                                        <a href="<?= site_url('tenants') ?>#tabs-<?php echo $value->prop_id; ?>" class="<?php if ($value->expiry == 'Yes') { ?>expiring<?php } ?>"> <?= $value->tenants ?> </a> 
                                    <?php } ?>
                                </td>    
                                <td> <a href="<?= site_url('message') ?>#tabs-<?php echo $value->prop_id; ?>"> <?php echo $value->messages ?> </a> </td>
                                <td> <a href="<?= site_url('vendors') ?>#tabs-<?php echo $value->prop_id; ?>"> <?= $value->vendors ?> </a> </td>
                                <td>
                                    <div class="togle <?php if ($value->tpay != 'Yes') { ?>stop<?php } ?> tServices" id="tpay-<?= $value->prop_id ?>" data-type="tpay" data-value="<?= $value->prop_id ?>" data-property="<?php if ($value->tpay == 'Yes') { ?>No<?php } else { ?>Yes<?php } ?>">
                                        <span class="check"></span>
                                    </div>
                                </td>
                                <td> <div class="togle <?php if ($value->tmaintenance != 'Yes') { ?>stop<?php } ?> tServices" id="tmaintenance-<?= $value->prop_id ?>" data-type="tmaintenance" data-value="<?= $value->prop_id ?>" data-property="<?php if ($value->tmaintenance == 'Yes') { ?>No<?php } else { ?>Yes<?php } ?>"><span class="check"></span></div></td>
                                <td> 
                                    <div class="togle <?php if ($value->tmessaging != 'Yes') { ?>stop<?php } ?> tServices" id="tmessaging-<?= $value->prop_id ?>" data-type="tmessaging" data-value="<?= $value->prop_id ?>" data-property="<?php if ($value->tmessaging == 'Yes') { ?>No<?php } else { ?>Yes<?php } ?>">
                                        <span class="check"></span>
                                    </div>
                                </td>
                            </tr>
                        <?php } ?>
            </tbody>
        </table> -->
      <?php if ($allProperties) { ?>
        <table class="my-table ss">
                    <thead>
					<tr class="top-bar">
                            <th> </th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th class="bord4"></th>
                            <th class="bord"></th>
                            <th class="bord1"> tServices</th> 
                            <th class="bord2">
                               
                            </th>
                        </tr>
                        <tr>
                            <th>Address </th>
                            <th>Unit#  <a href="#" data-toggle="tooltip" tooltip="A single family property is 1 unit."><span class="glyphicon glyphicon-info-sign"></span></a></th>
                            <th>Rent Balance</th>
                            <th>Tenants</th>
                            <th>Messages</th>
                            <th>Vendors</th>
                            <th><a href="javascript:;" class="dash-pop shwtmsg">tText</a> <br><a href="javascript:;" style="text-align:center !important" id="sendeTesttText" class="">Test</a></th>
                            <th><a href="javascript:;" class="dash-pop shwtmntnce">tMaintenance</a><br><a href="javascript:;"  style="text-align:center !important"  id="sendTesttMaintenanceEmail" class="">Test</a></th>
                            <th>
                                <a  class="dash-pop shwtpay">tPay</a>                                 <?php if($tpay_account_status == 0){ ?>
                                        <span style="float: right;  border: none;padding-right: 18px;border-radius: 0px;height: 4px !important;width:30px" class="togle stop" data-type="" data-value="" data-property="No">
                                            <span class="check"></span>
                                        </span>
                                <?php }else{ ?>
                                        <span style="border: medium none;    border-radius: 0;    display: inline-block;    float: right;    height: 24px !important;    margin-top: -4px;    padding-right: 18px;    width: 30px;"  class="togle" data-type="" data-value="" data-property="Yes">
                                            <span class="check"></span>
                                        </span>
                                <?php }  ?>
                                <br>
                                <a href="<?= site_url('account_setup') ?>?dashboard=true"  style="text-align:center !importantl;"  id="" class="">Bank Acct.
                                </a>
								<!-- <a href="#" data-toggle="tooltip" tooltip=" Add status light next to Bank Account under tPay. Light will match status light from tServices page."><span class="glyphicon glyphicon-info-sign"></span></a> -->
                            </th>
                        </tr>
                    </thead>
                    <tbody>
 
                        <?php foreach ($allProperties as $value) { ?>
                            <tr>
                                <td> 
                                    <?php if($value->step_completed < 3){ ?>
                                            <a  class="address_info_left" href="<?= site_url('add-property') ?>">  <?= $value->city ?>, <?= $value->address1 ?> <?= $value->address2 ?> </a> 
                                            <a href="javascript:;" class="address_info_right red_indicator"  data-toggle="tooltip" tooltip="Property Setup Incomplete."><span class="glyphicon glyphicon-info-sign"></span></a> 
                                    <?php }else{ ?>
                                            <a  class="address_info_left" href="<?= site_url('my-properties') ?>#tabs-<?php echo $value->prop_id; ?>">  <?= $value->city ?>, <?= $value->address1 ?> <?= $value->address2 ?> </a> 
                                    <?php }  ?>
                                 </td>
                                 <?php if($value->step_completed < 3){ ?>
                                    <td> <a href="<?= site_url('add-property') ?>"><?= $value->unit_number ?> </a> </td>
                                <?php }else{ ?>
                                    <td> <a href="<?= site_url('my-properties') ?>#tabs-<?php echo $value->prop_id; ?>"><?= $value->unit_number ?> </a> </td>
                                <?php }  ?>
                                <td class="das_rent_ampount_td">  <?php echo $value->rent_amount; ?> </td> 
                                <td>
                                    <?php if($value->step_completed < 3){ ?>
                                            <?php if ($value->tenants == 0) { ?>
                                                <a href="<?= site_url('add-property') ?>" id="<?php echo $value->prop_id; ?>" class="<?php if ($value->tenants == 0) { ?>zero<?php } ?>"> <?= $value->tenants ?> </a> 
                                            <?php } else { ?>
                                                <a href="<?= site_url('add-property') ?>" class="<?php if ($value->expiry == 'Yes') { ?>expiring<?php } ?>"> <?= $value->tenants ?> </a> 
                                            <?php } ?>
                                    <?php }else{ ?>
                                             <?php if ($value->tenants == 0) { ?>
                                                <a href="javascript:;" id="<?php echo $value->prop_id; ?>" data-toggle="modal" data-target="#addtenantModal" class="<?php if ($value->tenants == 0) { ?>zero<?php } ?>"> <?= $value->tenants ?> </a> 
                                            <?php } else { ?>
                                                <a href="<?= site_url('tenants') ?>#tabs-<?php echo $value->prop_id; ?>" class="<?php if ($value->expiry == 'Yes') { ?>expiring<?php } ?>"> <?= $value->tenants ?> </a> 
                                            <?php } ?>
                                    <?php }  ?>
                                </td>    
                                <?php if($value->step_completed < 3){ ?>
                                        <td> <a href="<?= site_url('add-property') ?>"> <?php echo $value->messages ?> </a> </td>
                                        <td> <a href="<?= site_url('add-property') ?>"> <?= $value->vendors ?> </a> </td>
                                <?php }else{ ?>
                                        <td> <a href="<?= site_url('message') ?>#tabs-<?php echo $value->prop_id; ?>"> <?php echo $value->messages ?> </a> </td>
                                        <td> <a href="<?= site_url('vendors') ?>#tabs-<?php echo $value->prop_id; ?>"> <?= $value->vendors ?> </a> </td>
                                <?php }  ?>  
                                <?php if($value->subscription_status == FALSE || $value->step_completed < 4){ ?> 
                                            <?php
                                            if ($value->step_completed < 3) {
                                                $p_clss = 'tServicesPending';
                                            } else {
                                                $p_clss = 'tServices';
                                            }
                                            ?> 
                                            <td> 
                                                <div class="togle <?php echo $p_clss; ?>" id="tmessaging-<?= $value->prop_id ?>" data-type="tmessaging" data-value="<?= $value->prop_id ?>" data-property="not_subscribe">
                                                    <span class="check"></span>
                                                </div>
                                            </td>
                                            <td> 
                                                <div class="togle <?php echo $p_clss; ?>" id="tmaintenance-<?= $value->prop_id ?>" data-type="tmaintenance" data-value="<?= $value->prop_id ?>" data-property="not_subscribe">
                                                    <span class="check"></span>
                                                </div>
                                            </td>
                                            <td> 
                                                <div class="togle <?php echo $p_clss; ?>" id="tpay-<?= $value->prop_id ?>" data-type="tpay" data-value="<?= $value->prop_id ?>" data-property="not_subscribe">
                                                    <span class="check"></span>
                                                </div>
                                            </td>

                                <?php }else{ ?>
                                            <td> 
                                                <?php
                                                $verification_status = $this->common_model->getSingleFieldFromAnyTable('verification_status', 'mem_id', $this->session->userdata('MEM_ID'), 'tbl_members');
                                                if ($verification_status == 'No') {
                                                    $clss = 'intservices';
                                                } else {
                                                    $clss = 'tServices';
                                                }
                                                ?>
                                                <div class="togle <?php if ($value->tmessaging == 'Yes') { ?>stop<?php } ?> <?php echo $clss; ?>" id="tmessaging-<?= $value->prop_id ?>" data-type="tmessaging" data-value="<?= $value->prop_id ?>" data-property="<?php if ($value->tmessaging == 'Yes') { ?>No<?php } else { ?>Yes<?php } ?>">
                                                    <span class="check"></span>
                                                </div>
                                            </td>
                                            <td> 
                                                <div class="togle <?php if ($value->tmaintenance == 'Yes') { ?>stop<?php } ?> <?php echo $clss; ?>" id="tmaintenance-<?= $value->prop_id ?>" data-type="tmaintenance" data-value="<?= $value->prop_id ?>" data-property="<?php if ($value->tmaintenance == 'Yes') { ?>No<?php } else { ?>Yes<?php } ?>">
                                                    <span class="check"></span>
                                                </div>
                                            </td>
                                            <td> 
                                                <div class="togle <?php if ($value->tpay == 'Yes') { ?>stop<?php } ?> <?php echo $clss; ?>" id="tpay-<?= $value->prop_id ?>" data-type="tpay" data-value="<?= $value->prop_id ?>" data-property="<?php if ($value->tpay == 'Yes') { ?>No<?php } else { ?>Yes<?php } ?>">
                                                    <span class="check"></span>
                                                </div>
                                            </td>
                                        
                                <?php } ?>
                            </tr>
                <?php } ?>
                    </tbody>
        <?php }else{ ?>
            <center></br></br></br></br></br></br>
                <span class="no-record"> No Properties found...</span>
            </center>
        <?php } ?>
    </div>
</div>	  

