<section class="lease-part wb-leases-page">
    <div class="container">
        <?php
        $all_leases = array();
        if (!empty($properties)) {
            foreach ($properties as $details) {
                if (!empty($details['leases'])) {
                    foreach ($details['leases'] as $lease) {
                        $name = $details['details']->city . ", " . $details['details']->address1 . " " . $details['details']->address2;
                        $all_leases[] = array(
                            'name' => $name, 'start_date' => $lease->lease_start_date, 'end_date' => $lease->lease_end_date,
                            'member' => $lease->first_name . " " . $lease->last_name, 'prop_id' => $details['details']->prop_id, 'tenant_id' => $lease->mem_id,'rent_amount' => $lease->rent_amount
                        );
                    }
                }
            }
        }
        ?>
        <div id="tabs">
		<p class="notice">Notice : A copy of Tenant's Lease must be uploaded if the tenant will use tPay for rent payment  </p>
            <div class="my-tab1"> 
                <ul class="my-tab  ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">
                    <li class="active all-tabs-panel-li"><a href="#tabs-1" class="summary">Summary</a></li>
                    <?php
                    $i = 2;
                    $prp = array();
                    if (!empty($all_leases)) {
                        ?>
                        <?php foreach ($all_leases as $lease) { ?>
                            <?php if (!in_array($lease['prop_id'], $prp)) { ?>
                                    <li class="all-tabs-panel-li ui-state-default ui-corner-top"><a href="#tabs-<?php echo $i; ?>"><?php echo $lease['name']; ?></a></li>
                                    <?php
                                    $i++;
                            } $prp[] = $lease['prop_id'];
                            ?>
                        <?php } ?>
                    <?php } ?>
                </ul>
            </div>

            <div class="tab-area">
                <div id="tabs-1">
                    <div class="table-content">
                        <table class="my-table">
                            <thead>
                                <tr> 
                                    <th>Lease Start</th>
                                    <th>Lease End</th>
									<th>Rent Rate</th>
                                    <th>Name</th>
                                    <th>Property</th>
                                    <th>Lease Copy</th>
                                    <th>Upload</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php  if (!empty($all_leases)) { ?>
                                    <?php foreach ($all_leases as $lease) { //echo "<pre>"; print_r($all_leases); ?>
                                   <?php $docs = lease_docs($lease['prop_id'],$lease['tenant_id'],'0');?>
                                        <tr>                                                             

                                            <td><?php echo date('m/d/y', strtotime($lease['start_date'])); ?></td>
                                            <td><?php echo date('m/d/y', strtotime($lease['end_date'])); ?></td>
											<td> <?php echo '$'.$lease['rent_amount']; ?></td>
                                            <td><?php echo $lease['member']; ?></td>
                                            <td><a href="<?= site_url('my-properties') ?>#tabs-<?php echo $lease['prop_id']; ?>"><?php echo $lease['name']; ?></a></td>
                                            <td>
                                                <ul>
                                                    <li><a href="javascript:;" class="document"><?php echo $cnt=count($docs); ?><?php echo $cnt>1?' Documents':' Document' ?></a></li>
                                                    <!--<li><a href="#" class="upload">Upload</a></li>-->
                                                </ul>
                                            </td>
                                            <td>
                                                <!-- <input type="file" id="file" class="lease-file-list" name="<?php echo $lease['prop_id'] . "_" . $lease['tenant_id'] ?>" /> -->
                                                    <span class="upload-btn upload-btn_lease">Upload<input type="file" name="<?php echo $lease['prop_id'] . "_" . $lease['tenant_id'] ?>" class="upl_lease1 lease-file-list"></span>
                                            </td>

                                        </tr>
                                    <?php } ?>
                                <?php } else { ?>
                                    <tr><td colspan="5">No record found</td></tr>
                                <?php } ?>

                            </tbody>
                        </table>
                    </div>

                </div>
                <?php
                $all_leases = array();
                if (!empty($properties)) { 
                    ?>
                    <?php
                    $k = 2;
                    foreach ($properties as $data) {
                        ?>
                        <div id="tabs-<?php echo $k; ?>">
                            <div class="tenant">
                                <ul class="tenant1">
                                    <li><span><?php echo $pname = $data['details']->city . ", " . $data['details']->address1 . " " . $data['details']->address2; ?></span></li>
                                </ul>
                                <?php foreach ($data['leases'] as $ldata) { ?>
                                    <div class="tenant_div">
                                        <table class="table">
                                            <thead>
                                                <tr>                                                                                           
                                                    <th>Lease Start</th>
                                                    <th>Lease End </th>
													<th>Rent Rate </th>
                                                    <th>Name</th>
                                                    <th>Property</th>
                                                </tr>
                                                <tr>  
                                                    <td><?php //echo date('d/m/y', $ldata->lease_start_date); ?>
                                                         <div style="position: relative"> <input type="text" class="form-control width50 leasefrom" disabled="true" value="<?php echo date('m/d/Y', strtotime($ldata->lease_start_date)); ?>" /></div>
                                                        <!--<span id="<?php echo $ldata->lease_id; ?>" class="fa fa-pencil edt_lease_date frm_date"></span>-->
                                                    </td>
                                                    <td><?php //echo date('d/m/y', $ldata->lease_end_date); ?>
                                                        <div style="position: relative">  <input type="text" class="form-control width50 leaseto" value="<?php echo date('m/d/Y', strtotime($ldata->lease_end_date)); ?>" disabled="true"/></div>
                                                    <td>
                                                        <div style="position: relative">  <input type="text" style="width:75%" class="form-control width50 rent_amount" value="<?php echo $ldata->rent_amount; ?>"  disabled="true"/></div>
                                                        <span id="<?php echo $ldata->lease_id; ?>" class="fa fa-pencil edt_lease_date to_date"></span></td>
                                                    </td>
                                                    <td><?php echo $ldata->first_name . " " . $ldata->last_name; ?></td>
                                                    <td><a href="<?= site_url('my-properties') ?>#tabs-<?php echo $ldata->property_id; ?>"><?php echo $pname; ?></a></td>

                                                </tr>
                                            </thead>
                                        </table>
                                        <h2 class="lease-doc">Lease Document</h2>
                                        <ul class="upload-file" style="width:100%">
                                            <?php $doc = lease_docs($ldata->property_id, $ldata->mem_id,'0'); ?>
                                            <?php if (!empty($doc)) { ?>
                                                <?php foreach ($doc as $file) { ?>
                                                    <li>
                                                        <span class="crossicon" id="<?php echo $file->id ?>">X</span>
                                                        <a class="docname" href="<?= $this->config->item('uploads') ?>lease_documents/<?php echo $file->lease_doc; ?>" target="_blank">
                                                        <img alt="" src="<?= $this->config->item('templateassets') ?>images/file-upload.png">
                                                        <span><?php echo $file->lease_doc; ?></a></span>
                                                    </li>
                                                <?php } ?>
                                            <?php } ?>
                                            <li class="browse"><input type="file" name="file" class="lease-file" id="<?php echo $ldata->property_id . "_" . $ldata->mem_id ?>" /></li>
                                        </ul>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                        <?php
                        $k++;
                    }
                    ?>
                <?php } ?>


            </div>
        </div>
    </div>       
</section>
