
<style>
    .ms {
    background: rgba(0, 0, 0, 0) none repeat scroll 0 0 !important;
    height: auto !important;
    width: 99% !important;
}
</style>
<section class="lease-part add-new-vendor-popup">
    <div class="container">
        <?php
        $all_vendors = $all_property = array();
        if (!empty($properties)) {
            foreach ($properties as $details) {
                if (!empty($details['vendors'])) {
                    foreach ($details['vendors'] as $vendor) {
                        // $name = $details['details']->city . ", " . $details['details']->address1 . " " . $details['details']->address2;
                        $all_vendors[] = $vendor;
                    }
                }
                $all_property[] = $details['details'];
            }
        }
        $all_service = get_vendor_services();
        $services = array();

        if (!empty($all_service)) {
            foreach ($all_service as $ser) {
                $services[$ser->t_id] = $ser->name;
            }
        }
        $property_drop = array();
        if (!empty($all_properties)) {
            foreach ($all_properties as $propty) {
                $name = $propty->city . ", " . $propty->address1 . " " . $propty->address2;
                $property_drop[$propty->prop_id] = $name;
            }
        }
        ?>



        <div id="tabs">

            <div class="my-tab1"> 
                <a href="javascript:void(0)" class="btn-tenant my-btn-tennt my-vendor"><i class="fa fa-plus"></i>Add New Vendor</a>
                <ul class="my-tab ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">

                    <li class="active all-tabs-panel-li"><a href="#tabs-0" class="vendor">My Vendors</a></li>
                    <?php
                    if (!empty($all_properties)) {
                        //$k = 2;
                        ?>
                        <?php foreach ($all_properties as $all_prp) { ?>
                            <li class="all-tabs-panel-li"><a href="#tabs-<?php echo $all_prp->prop_id; ?>">
                                    <?php echo $all_prp->city . ", " . $all_prp->address1 . " " . $all_prp->address2; ?>
                                </a></li>
                            <?php
                            //$k++;
                        }
                        ?>
                    <?php } ?>
                </ul>
            </div>
            <div class="tab-area add_vndr_tab" style="display:none">
                <div class="table-content vendor-table">
                    <h2 class="my-txt" style="width:auto">Add New Vendor</h2>
                    <!--<span class="addmore_vendor">+Add New</span>-->

                    <form id="add_vendors" class="select-grp">
                        <div class="vndr_form sp-vendor">
                            <div class="frm_row">
                                <table id="filters" class="tablesorter">
                                    <thead>
                                        <tr>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if (isset($property_drop) && !empty($property_drop)) { ?>
                                            <tr>
                                                <td>
                                                    <div class="event">
                                                        <p class="my-event-checkbox">
                                                            <input type="checkbox" class="tnt_check_all" id="test025">    <label for="test025">All</label> </p>
                                                    </div>
                                                </td>
                                            </tr>
                                            <?php  foreach($property_drop as $key=>$prp_check): ?>
                                                <tr>
                                                    <td>
                                                        <div class="event">
                                                            <p class="my-event-checkbox"> 
                                                                <input type="checkbox" class="tnt_check" name="prop_id[]" value="<?php echo $key; ?>" id="ptp_<?php echo $key; ?>">    
                                                                <label for="ptp_<?php echo $key; ?>"><?php echo ucwords($prp_check); ?></label> 
                                                            </p>
                                                        </div> 
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>

                                        <?php } else { ?>
                                        <div class="event">
                                            <p class="my-event-checkbox">No record found. </p>
                                        </div>
                                    <?php } ?>
                                    </tbody>
                                </table>
                                <p class="note_vndr1"><b>*Please make sure that only 1 vendor is selected for each service category. If you want to use the service but do not have any vendors, you must add yourself as a vendor.</b></p><br>
                                  <label class="error check_error" style="display:none;"></label>

                                <?php
                               // $lists = "id='sevice' class='form-control add_vender_form width50' required='1'";
                               // echo form_dropdown('service_id[]', $services, '', $lists);
                                ?>
                            

                               <span class="sp sp1"> <input type="text" name="cname[]" maxlength="50" class="my-txtfleid add_vender_form" placeholder="Company Name" required="true"/></span>
                               <span class="sp sp2"> <input type="text" name="email[]" email="true" class="my-txtfleid add_vender_form" placeholder="Email" required="true"/></span>
                              <span class="sp sp3">  <input type="text" name="phone[]" class="my-txtfleid add_vender_form phone_us" placeholder="Phone" required="true"/></span>
								<span class="sp sp4"> 
								<select id='sevice' multiple name="service_id[0][]" class='service_dropdown form-control add_vender_form width50' required='1'>
                                    <?php foreach ($services as $service=>$val) { ?>
                                        <option value="<?= $service ?>"><?= $val ?></option>
                                    <?php } ?>
                                </select>
							<label id="sevice-error" class="error" for="sevice"></label>
								</span>
<!--                                <span  class="del-span">  <a class="del del_vendor_row" href="javascript:void(0)"><img src="images/cross.png" alt=""/></a></span>-->
                            </div>
                        </div>

                        <p class="select-grp">
                            
                            <a href="<?= site_url('vendors') ?>" class="my-link my-back">Back</a> 
                            <input type="submit" class="btn-tenant" value="Submit"/>
                        </p>

                    </form>
                </div>
            </div>
            <div class="tab-area">
                <div id="tabs-0">
                    <div class="table-content vendor-table">
                        <table class="my-table text-left">
                            <thead>
                                <tr> 
                                    <th>Service</th>
                                    <th>Company</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>

                                <?php if (!empty($summary)) { ?>
                                    <?php foreach ($summary as $vndr): ?>
                                        <tr class="hide_rec" style="display: none;">
                                            <td>
                                                <?php
                                                //$list = "id='sevice' class='form-control width50' required='1'";
                                                //echo form_dropdown('service', $services, $vndr->service, $list);
                                                ?>
                                                <?php $exist_services = explode(',', $vndr->service); ?> 
                                                <select id='sevice' multiple name="service_id[0][]" class='service_dropdown form-control add_vender_form width50' required='1'>
                                                    <?php foreach ($services as $service=>$val) { ?>
                                                            <?php if (in_array($service, $exist_services)) { ?>
                                                                <option value="<?= $service ?>" selected><?= $val ?></option>
                                                            <?php }else{ ?>
                                                                <option value="<?= $service ?>"><?= $val ?></option>
                                                            <?php } ?>
                                                    <?php } ?>
                                                </select>
                                            </td>
                                            <td><input type="text" name="cname" class="my-txtfleid" value="<?php echo $vndr->company_name; ?>"/></td>
                                            <td><input type="text" name="email" class="my-txtfleid" value="<?php echo $vndr->email; ?>"/></td>
                                            <td><input type="text" name="mobile" class="my-txtfleid phone_us" value="<?php echo $vndr->mobile_no; ?>"/></td>
                                            <td>
                                                <span class="edit-span"> <a class="downlod update_vendor" href="javascript:void(0);" id="<?php echo $vndr->vendor_id; ?>"></a></span>
                                                <span  class="del-span">  <a class="del del_vendor" href="javascript:void(0);"  id="<?php echo $vndr->vendor_id; ?>"></a></span>
                                            </td>   
                                        </tr>
                                        <tr class="show_rec">                                                             
                                            <td>
                                            <?php $exist_services = explode(',', $vndr->service); ?>
                                                <?php foreach ($services as $service=>$val) { ?>
                                                        <?php if (in_array($service, $exist_services)) { ?>
                                                                <?php echo rtrim($val .',',','); ?>
                                                        <?php } ?>
                                                <?php } ?>
                                            <?php //echo $services[$vndr->service]; ?>
                                            </td>
                                            <td><?php echo $vndr->company_name; ?></td>
                                            <td><?php echo $vndr->email; ?></td>
                                            <td><?php echo $vndr->mobile_no; ?></td>
                                            <td>
                                                <span class="edit-span"> <a class="edit edit_vendor" href="javascript:void(0);" id="<?php echo $vndr->vendor_id; ?>"></a></span>
                                                <span  class="del-span">  <a class="del del_vendor" href="javascript:void(0);"  id="<?php echo $vndr->vendor_id; ?>"></a></span>
                                            </td>
                                        </tr>

                                    <?php endforeach; ?>
                                <?php } else { ?>
                                    <tr>
                                        <td colspan="5">No record found</td>
                                    </tr>  
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>


                </div>
                <?php
                if (!empty($all_properties)) {
                   //echo "<pre>"; print_R($properties); exit;
                    foreach ($all_properties as $prop) {
                        ?>
                        <div id="tabs-<?php echo $prop->prop_id; ?>">
                            <?php $prop_vendor_list =  array(); ?>
                            <?php if (isset($properties[$prop->prop_id])) {
                                foreach ($properties as $key => $details) {

                                    ?>
                                    <?php if ($key == $prop->prop_id) { ?>
                                            <div class="table-content">
                                                <table class="my-table text-left">
                                                    <thead>
                                                        <tr> 
                                                            <th>Service</th>
                                                            <th>Company</th>
                                                            <th>Email</th>
                                                            <th>Phone</th>
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php if (!empty($details['vendors'])) { ?>
                                                            <?php foreach ($details['vendors'] as $vendr) { ?>
                                                                    <?php $prop_vendor_list[] =  $vendr['vendor_id']; ?>
                                                                    <tr class="hide_rec" style="display: none;">
                                                                        <td>
                                                                            <?php
                                                                            //$list = "id='sevice' class='form-control width50' required='1'";
                                                                            //echo form_dropdown('service', $services, $vendr['service'], $list);
                                                                            ?>
                                                                            <?php $exist_services = explode(',', $vendr['service']); ?> 
                                                                            <select id='sevice' multiple name="service_id[0][]" class='service_dropdown form-control add_vender_form width50' required='1'>
                                                                                <?php foreach ($services as $service=>$val) { ?>
                                                                                        <?php if (in_array($service, $exist_services)) { ?>
                                                                                            <option value="<?= $service ?>" selected><?= $val ?></option>
                                                                                        <?php }else{ ?>
                                                                                            <option value="<?= $service ?>"><?= $val ?></option>
                                                                                        <?php } ?>
                                                                                <?php } ?>
                                                                            </select>
                                                                        </td>
                                                                        <td><input type="text" name="cname" class="my-txtfleid" value="<?php echo $vendr['cname']; ?>"/></td>
                                                                        <td><input type="text" name="email" class="my-txtfleid" value="<?php echo $vendr['email']; ?>"/></td>
                                                                        <td><input type="text" name="mobile" class="my-txtfleid" value="<?php echo $vendr['mobile_no']; ?>"/></td>
                                                                        <td>
                                                                            <span class="edit-span"> <a class="downlod update_vendor" href="javascript:void(0);" id="<?php echo $vendr['vendor_id']; ?>"></a></span>
                                                                            <span  class="del-span">  <a class="del del_vendor" href="javascript:void(0);"  id="<?php echo $vendr['vendor_id']; ?>"></a></span>
                                                                        </td>   
                                                                    </tr>
                                                                    <tr class="show_rec">                                                             
                                                                        <td>
                                                                        <?php $exist_services = explode(',', $vendr['service']); ?>
                                                                            <?php foreach ($services as $service=>$val) { ?>
                                                                                    <?php if (in_array($service, $exist_services)) { ?>
                                                                                            <?php echo rtrim($val .',',','); ?>
                                                                                    <?php } ?>
                                                                            <?php } ?>
                                                                        <?php //echo $services[$vndr->service]; ?>
                                                                        </td>

                                                                        <td><?php echo $vendr['cname']; ?></td>
                                                                        <td><?php echo $vendr['email']; ?></td>
                                                                        <td><?php echo $vendr['mobile_no']; ?></td>
                                                                        <td>
                                                                            <span class="edit-span"> <a class="edit edit_vendor" href="javascript:void(0);" id="<?php echo $vendr['vendor_id']; ?>"></a></span>
                                                                            <span  class="del-span">  <a class="del del_vendor" href="javascript:void(0);"  id="<?php echo $vendr['vendor_id']; ?>"></a></span>
                                                                        </td>   
                                                                    </tr>
                                                            <?php } ?>
                                                        <?php } ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                    <?php } ?>
                                <?php }
                            }else {
                            ?>
                                <div class="table-content">
                                    <table class="my-table text-left">
                                        <thead>
                                            <tr> 
                                                <th>Service</th>
                                                <th>Company</th>
                                                <th>Email</th>
                                                <th>Phone</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr><td colspan="5">No vendor's for this property</td></tr>
                                        </tbody>
                                    </table>
                                </div>    
                            <?php } ?>


                            <?php // echo "<pre>"; print_R($prop); ?>

                            <div id="myModal_<?php echo $prop->prop_id; ?>" class="modal fade" role="dialog">
                                <div class="modal-dialog" style="width: 900px;">

                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h2 class="add_vndr">Select Existing Vendor or Add New Vendor</h2> 
                                        </div>
                                        <div class="modal-body">
                                            <div class="table-content signup-table tabbb">
                                                <br><br>
                                                <form name="vendor_form_<?php echo $prop->prop_id; ?>" id="vendor_form_<?php echo $prop->prop_id; ?>" class="addVendorInProperty" action="#" method="post">
                                                    <table class="my-table vndrs_table" style="margin-bottom: 10px;">
                                                        <thead>
                                                            <tr>
                                                            <th>
                                                            <?php if (!empty($summary)) { ?>
                                                            <input type="checkbox" class="tnt_check_all" id="test_<?php echo $prop->prop_id; ?>">    <label for="test_<?php echo $prop->prop_id; ?>">&nbsp;</label>
                                                            <?php } else { ?>
                                                            &nbsp;
                                                            <?php } ?>
                                                            </th>
                                                            <th>Service</th>
                                                            <th>Company Name </th>
                                                            <th>Email</th>
                                                            <th>Phone</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody class="all-record"> 
                                                            <?php if (!empty($summary)) { ?>
                                                            <tr>
                                                                <td colspan="5"> </td>
                                                            </tr>
                                                            <?php //echo "<pre>"; print_R($summary); exit; ?>
                                                            <?php foreach ($summary as $value_s) { ?> 
                                                                <?php if (!in_array($value_s->vendor_id, $prop_vendor_list)) { ?>
                                                                    <tr class="display-custom-row" id="display_row_<?= $value_s->vendor_id ?>_<?php echo $prop->prop_id; ?>" >
                                                                        <td class="width30">
                                                                            <input type="checkbox" class="tnt_check" name="vendor_ids[]" value="<?= $value_s->vendor_id ?>" id="vendor_<?php echo $value_s->vendor_id; ?>_<?php echo $prop->prop_id; ?>">    
                                                                            <label for="vendor_<?php echo $value_s->vendor_id; ?>_<?php echo $prop->prop_id; ?>">&nbsp;</label> 
                                                                        </td>

                                                                        <td>
                                                                            <?php $exist_services = explode(',', $value_s->service); ?>
                                                                            <?php foreach ($services as $service_key => $service_val) { ?>
                                                                                <?php // print_r($services);  ?>
                                                                                <?php if (in_array($service_key, $exist_services)) { ?>
                                                                                    <?php echo rtrim($service_val .',',','); ?>
                                                                                <?php } ?>
                                                                            <?php } ?>
                                                                            <?php //echo $services[$vndr->service]; ?>
                                                                        </td>
                                                                        <td> <?= $value_s->company_name ?>  </td>
                                                                        <td><?= $value_s->email ?></td>
                                                                        <td><?= $value_s->mobile_no ?></td>
                                                                    </tr>
                                                                <?php } ?>

                                                            <?php } ?>
                                                            <input type="hidden" name="property_id" value="<?php echo $prop->prop_id; ?>">
                                                            <?php
                                                            }
                                                            ?>
                                                            <tr id="add-vendor-row">
                                                                <td colspan="5" class="center-cont">
                                                                    <a class="btn-tenant add-vendor-row" style="color:#ffffff;width:auto;margin-left: 0px; padding: 10px 13px 10px 20px;text-align: center; float:none; display:inline-block;text-decoration:none" href="javascript:;"  data-value="<?php echo isset($value->v_id) ? $value->v_id : ''; ?>">Add Vendor <i class="fa fa-plus" style="margin-left:5px; margin-right:0"></i></a> Please make sure that only 1 vendor is selected for each service.
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    <div class="button_vendor_s" style="margin-left: 45%;"><input type="button" class="update_vendors_of_property" id="<?php echo $prop->prop_id; ?>" value="Save" style=""/></div>
                                                </form>
                                            </div>

                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <button type="button" data-toggle="modal" data-target="#myModal_<?php echo $prop->prop_id; ?>" class="btn-tenant" style="color:#ffffff;width:auto;margin-left: 0px; padding: 10px 13px 10px 20px;text-align: center; float:none; display:inline-block;text-decoration:none">Add Vendor </button>

                        </div>


                        <?php
                    }
                    ?>
                </div>
        <?php } ?>


        </div>

</section>
<div class="cp_vndr_form" style="display: none;">
    <div class="frm_row">
        <?php
     //   $lists = "id='ms' multiple='multiple' class='ms form-control add_vender_form width50' required='1'";
     //   echo form_dropdown('prop_id[]', $property_drop, '', $lists);
        
        if(!empty($property_drop)){
            foreach($property_drop as $key=>$prp_check){ ?>
        <input type="checkbox" name="prop_id[]" value="<?php echo $key ?>"/>
        <?php echo $prp_check; ?>
            <?php }
        }
        ?>

        <?php
        $lists = "id='sevice' class='form-control add_vender_form  width50' required='1'";
        echo form_dropdown('service_id[]', $services, '', $lists);
        ?>

        <input type="text" name="cname[]" maxlength="50" class="my-txtfleid add_vender_form" placeholder="Company Name" required="true"/>
        <input type="email" name="email[]" class="my-txtfleid add_vender_form" placeholder="Email" email="true" required="true"/>
        <input type="text"  name="phone[]" class="my-txtfleid add_vender_form phone_us" placeholder="Phone"  required="true"/>

        <span  class="del-span">  <a class="del del_vendor_row" href="javascript:void(0)"></a></span>
    </div>
</div>
<script type="text/javascript">
    $(document).on('click', '.btn-group', function (event) {
       // alert('hhhhhhhhhhhhhhh')
    });
    $(document).on('click', '.multiselect-native-select', function (event) {
        //alert('11111111')
    });
    
</script>
<link href="<?= $this->config->item('templateassets') ?>css/bootstrap.min.css" rel="stylesheet">
<link href="<?= $this->config->item('templateassets') ?>css/bootstrap-dialog.min.css" rel="stylesheet">
<script type="text/javascript" src="<?= $this->config->item('templateassets') ?>js/bootstrap-dialog.min.js"></script>
