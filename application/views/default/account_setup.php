<script src="<?= $this->config->item('templateassets') ?>js/jquery-ui.js"></script>
<iframe id="synapse_iframe" style="top:0; height:0%;width:0%;position:fixed;z-index:100000;visibility:hidden;" frameborder="0"></iframe>
<script src="<?= $this->config->item('templateassets') ?>js/universal_iframe.js"></script>
<script type="text/javascript" src="<?= $this->config->item('templateassets') ?>js/fingerprint2.js"></script>
<?php //$a=1; $b=.5; echo $a * $b; exit; ?>
<script type="text/javascript">
    $(document).ready(function () {
        $('#verfication_model').modal({backdrop: 'static', keyboard: false})  
        new Fingerprint2().get(function(result){
        // this will use all available fingerprinting sources
        $.ajax({
                url: '<?php echo base_url('property/update_fingerprint'); ?>',
                type: 'POST',
                data: {fingerprint: result},
                beforeSend: function () {
                    $('#wait-div').hide();
                },
                success: function (data) {
                    //console.log(data);
                    $("#wait-div").hide();                     
                },
                error: function () {
                    $("#wait-div").hide();                   
                }
            });
        });
    });
    
     $(document).ready(function () {       
        //$('#wait-div').show();
        $("#recent_bank_statment").change(function () {
            var path = $(this).val();
            var filename = path.replace(/^.*\\/, "");
            var nw_filename = '<span class="recent_bank_statment_file_name ls_file_name">'+filename+'</span> ';
            var html = '<a class="btn btn-del btn-primary" href="javascript:;" id="delete_recent_bank_statment">Delete</a>';
            var reader = new FileReader();
            reader.onload = function(){
                var dataURL = reader.result;
                html_view = '<a href="'+ dataURL +'" target="_blank" class="btn btn btn-edit btn-primary" id="edit_recent_bank_statment"> View </a> ';
                $(".recent_bank_span").html(nw_filename + ' '  + html + ' '+ html_view);
            };
            reader.readAsDataURL(this.files[0]);
            //$(".recent_bank_statment_file_name").html(html);
            $(".recent_bank_span").show();
        });
        $("#driving_document").change(function () {
            var path = $(this).val();
            var filename = path.replace(/^.*\\/, "");
            var nw_filename = '<span class="driving_document_file_name ls_file_name">'+filename+'</span> ';
            var html = '<a class="btn btn-del btn-primary" href="javascript:;" id="delete_driving_document">Delete</a>';
            var reader = new FileReader();
            reader.onload = function(){
                var dataURL = reader.result;
                html_view = '<a href="'+ dataURL +'" target="_blank" class="btn btn btn-edit btn-primary" id="edit_driving_document"> View </a> ';
                $(".document_span").html(nw_filename + ' '  + html + ' '+ html_view);
            };
            reader.readAsDataURL(this.files[0]);
    
            $(".document_span").show();
        });
    
        
        $(document).on("click",'#delete_driving_document', function(){ 
            var id = $(this).attr('name');        
    
            $.ajax({
                url: '<?php echo base_url('property/delete_driving_document'); ?>',
                type: 'POST',
                data: {id: id},
                beforeSend: function () {
                    $('#wait-div').show();
                },
                success: function (data) {
                    $("#wait-div").hide(); 
                    $(".driving_document_file_name").html(' ')
                    $("#delete_driving_document").remove();
                    $("#edit_driving_document").remove();
                },
                error: function () {
                    $("#wait-div").hide();
                    swal("Error!", "Server not responding.", "error");
                }
            });
        });
    
          //$("#delete_recent_bank_statment").click(function(){
        $(document).on("click",'#delete_recent_bank_statment', function(){ 
            var id = $(this).attr('name');        
    
            $.ajax({
                url: '<?php echo base_url('property/delete_recent_bank_statment'); ?>',
                type: 'POST',
                data: {id: id},
                beforeSend: function () {
                    $('#wait-div').show();
                },
                success: function (data) {
                    $("#wait-div").hide();
                    $(".recent_bank_statment_file_name").html(' ')
                    $("#edit_recent_bank_statment").remove();
                    $("#delete_recent_bank_statment").remove();
                },
                error: function () {
                    $("#wait-div").hide();
                    swal("Error!", "Server not responding.", "error");
                }
            });
          });
        
      });
    $(document).ready(function(){
    
    
      $("#same_address").click(function(){
          var same_address_type = $(this).attr('name');         
    
          if(same_address_type == "same_address_acc"){
            if($(this).is(":checked")) {
              $.ajax({
                  url: '<?php echo base_url('property/get_same_address_as_rent'); ?>',
                  type: 'POST',
                  data: {same_address_type: same_address_type},
                  /*beforeSend: function () {
                      $('#wait-div').show();
                  },*/
                  success: function (data) {
                      $("#wait-div").hide();
                      var dataArray = $.parseJSON(data);
                      console.log(dataArray);
                      $("#first_name").val(dataArray.first_name);
                      $("#middle_name").val(dataArray.middle_name);
                      $("#last_name").val(dataArray.last_name);
                      $("#phone_us").val(dataArray.mobile_no);
                      $("#email").val(dataArray.email);
                      $("#address_1").val(dataArray.mailing_address1);
                      $("#address_2").val(dataArray.mailing_address2);
                      $("#unit").val(dataArray.mailing_unit);
                      $("#city").val(dataArray.mailing_city);
                      $("#zip").val(dataArray.mailing_zip);
                      $("#state").val(dataArray.mailing_state_id);
                      $("#dob").val(dataArray.dob);  
                      $("#social_security").val('');                    
                  },
                  error: function () {
                      $("#wait-div").hide();
                      swal("Error!", "Server not responding.", "error");
                  }
              });
    
    }else{
     $.ajax({
                  url: '<?php echo base_url('property/get_saved_address_linked'); ?>',
                  type: 'POST',
                  data: {same_address_type: same_address_type},
                  /*beforeSend: function () {
                      $('#wait-div').show();
                  },*/
                  success: function (data) {
                      $("#wait-div").hide();
                      console.log(data);
                      var dataArray = $.parseJSON(data);
                      //console.log(dataArray);
                      $("#first_name").val(dataArray.first_name);
                      $("#middle_name").val(dataArray.middle_name);
                      $("#last_name").val(dataArray.last_name);
                      $("#phone_us").val(dataArray.phone);
                      $("#email").val(dataArray.email);
                      $("#address_1").val(dataArray.address_1);
                      $("#address_2").val(dataArray.address_2);
                      $("#unit").val(dataArray.unit);
                      $("#city").val(dataArray.city);
                      $("#zip").val(dataArray.zip);
                      $("#state").val(dataArray.state);
                      $("#dob").val(dataArray.dob);                    
                      $("#social_security").val(dataArray.social_security);  
                    
                  },
                  error: function () {
                      $("#wait-div").hide();
                      swal("Error!", "Server not responding.", "error");
                  }
              });
            }  
          }else{        //  if($(this).is(":not(:checked)"))
              //alert("Checkbox is unchecked.");
            if($(this).is(":checked")  && same_address_type == "same_address_mem"){
                $.ajax({
                  url: '<?php echo base_url('property/get_same_address_as_rent'); ?>',
                  type: 'POST',
                  data: {same_address_type: same_address_type},
                  /*beforeSend: function () {
                      $('#wait-div').show();
                  },*/
                  success: function (data) {
                      $("#wait-div").hide();
                      var dataArray = $.parseJSON(data);
                      console.log(dataArray);
                      $("#first_name").val(dataArray.first_name);
                      $("#middle_name").val(dataArray.middle_name);
                      $("#last_name").val(dataArray.last_name);
                      $("#phone_us").val(dataArray.mobile_no);
                      $("#email").val(dataArray.email);
                      $("#address_1").val(dataArray.mailing_address1);
                      $("#address_2").val(dataArray.mailing_address2);
                      $("#unit").val(dataArray.mailing_unit);
                      $("#city").val(dataArray.mailing_city);
                      $("#zip").val(dataArray.mailing_zip);
                      $("#state").val(dataArray.mailing_state_id);
                      $("#dob").val(dataArray.dob);  
                      $("#social_security").val('');                    
                  },
                  error: function () {
                      $("#wait-div").hide();
                      swal("Error!", "Server not responding.", "error");
                  }
              });
            }else{
    //var dataArray = jQuery.parseJSON(data);
    //console.log(dataArray);
    $("#first_name").val('');
    $("#middle_name").val('');
    $("#last_name").val('');
    $("#phone_us").val('');
    $("#email").val('');
    $("#address_1").val('');
    $("#address_2").val('');
    $("#unit").val('');
    $("#city").val('');
    $("#zip").val('');
    $("#state").val('');
    $("#dob").val('');   
    $("#social_security").val('');   
            }            
          }
      });
    
    
      $("#bank_account").change(function(){
          var selectedValue = $(this).val();
          if(selectedValue != ''){
              $("#remove_sel_acc").css("display","inline-block");
          }else{
              $("#remove_sel_acc").css("display","none");            
          }
      });
      
      $("#remove_sel_acc").click(function(){
          var selectedAccount = $('#bank_account').val();
          $.ajax({
              url: '<?php echo base_url('property/delete_linked_account_number'); ?>',
              type: 'POST',
              data: {id: selectedAccount},
              beforeSend: function () {
                  $('#wait-div').show();
              },
              success: function (data) {
                $("#wait-div").hide();
                if(data == 'success'){                  
                  $("#wait-div").hide();
                  window.location =window.location;
                  $("#bank_account option[value='"+selectedAccount+"']").remove();
                  $("#remove_sel_acc").css("display","none");
                }
                window.location =window.location;
              },
              error: function () {
                  $("#wait-div").hide();
                  swal("Error!", "Server not responding.", "error");
              } 
          });
      });
    
      $("#bank_account").change(function(){
          var selectedAccount = $('#bank_account').val();
          $.ajax({
              url: '<?php echo base_url('property/check_micro_deposit'); ?>',
              type: 'POST',
              data: {id: selectedAccount},
              beforeSend: function () {
                  $('#wait-div').show();
              },
              global:false,
              success: function (data) {
        					var data = $.parseJSON(data);
        					$("#wait-div").hide();
        					if(data.success == true){                  
        						$("#wait-div").hide();
                    if(data.html_status == 'verified'){
                      $("#micro_deposit_form_div").html(data.html);
                      $("#red_green_toogle").addClass('stop');
                      $("#red_green_toogle").css('left','0px');
                    }else if(data.html_status == 'not_exist'){
                      $("#micro_deposit_form_div").html(' ');
                      $("#red_green_toogle").removeClass('stop');
                    }else{
                      $("#micro_deposit_form_div").html(data.html);
                      $("#red_green_toogle").removeClass('stop');
        						}
        					}
              },
              error: function () {
                  $("#wait-div").hide();
                  swal("Error!", "Server not responding.", "error");
              }
          });
      });
      $(document).on("click",'#update_micro_deposit', function(){ 
            var data = $("#payment_form").serialize();
            var formClass = '#payment_form';   
            $.ajax({
                url: '<?php echo base_url('property/synapseMicroDepositVerify'); ?>',
                type: 'POST',
                data: data,
                beforeSend: function () {
                    $('#wait-div').show();
                },
                success: function (data) {
                    var data = $.parseJSON(data);
                    if(data.success){
                      $("#wait-div").hide();
                      $("#micro_deposit_message").removeClass('alert-warning').removeClass('micro_deposit_message').addClass('micro_deposit_message').addClass('alert').addClass('alert-success').html(data.notify_message);
                      setTimeout(function(){ 
                        window.location=window.location; 
                      }, 1000); 
                    }else{
                      $("#wait-div").hide();
                      $("#micro_deposit_message").addClass('micro_deposit_message').addClass('alert').addClass('alert-warning').html(data.notify_message);
                      setTimeout(function(){ 
                        //window.location=window.location; 
                      }, 1000); 
                      
                    }
                },
                error: function () {
                    $("#wait-div").hide();
                    swal("Error!", "Server not responding.", "error");
                }
            });
          });
      
        $("#linked_bank_account").click(function(){
          var iframeInfo = {
            physical_id:{
                //set it as true if you wish to collect Physical IDs
                collect:true,
                //Tweak physical ID message
                message:'To continue, please attach a state or federally-issued photo ID. The image of your ID must be in color with all four corners visible.',
                //Set true to open up user's webcame. False just opens up a file uploader, not webcam.
                no_webcam:true
              },
              //on sanbdox or production?
              development_mode: true,
              //should the user be prompted to do KYC?
              do_kyc: false,
              //show the user be prompted to link a bank account?
              do_banks: true,
              //if set to false, other options (payments and link bank will be disabled until KYC is finished.)
              kyc_done: true,
              userInfo: {
                //user's credentials
                oauth_key: 'rWEu6W4NfPwPpzgE7qfoNatMOQH9572qZfJoriWC',
                fingerprint: 'suasusau21324redakufejfjsf'
              },
              colors: {
                //color prefrences
                'trim':'#059db1',
                'unfocused':'UNFOCUSED_COLOR',
                'text':'#059db1',
              },
              messages:{
                //messages to display below the buttons
                'kyc_message':'Please click on the button above to verify your Identity before creating a transaction.',
                'bank_message':'Please click on the button above to link a bank account with your profile.',
                'trans_mesage':'Please click on the button above to make a transaction.'
              },
              //your logo
              //receiverLogo: '<?= $this->config->item("uploads") ?>site_logo/<?= $this->config->item("site_logo") ?>'   
              receiverLogo: '<?= $this->config->item('uploads') ?>site_logo/<?= $this->config->item('site_logo') ?>'
          };
          setupSynapseiFrame(iframeInfo);
          $("#synapse_iframe").css("visibility","visible");
          $("#synapse_iframe").css("height","100%");
          $("#synapse_iframe").css("width","100%");
          $("#synapse_iframe").css("height","100%");
          $("#synapse_iframe").css("width","100%");
          expressReciver = function (e) {
              var json = JSON.parse(e.data);
              var json = json.json;
              if(json === undefined){
                  try{
                    var json = JSON.parse(e.data);
                    if (json.cancel || json.close) {
                      $("#synapse_iframe").css("visibility","hidden");
                      $("#synapse_iframe").css("height","0%");
                      $("#synapse_iframe").css("width","0%");
                      $("#synapse_iframe").prop("src","");
                      //self.set('enableButton', true);
                    };
                  }catch (e){
                     console.log(e);
                  };
              }else{
                  try{
                    if(json.success){
                        var nodes_arr = json.nodes;
                        var fd = new FormData();
                        for (var j = 0; j < nodes_arr.length; j++) {
                           var account_number = nodes_arr[j].info.account_num;
                           var class_is = nodes_arr[j].info.class;
                           var bank_name = nodes_arr[j].info.bank_name;
                           var oid = nodes_arr[j]._id.$oid;
                           fd.append("bank_name[]", bank_name);
                           fd.append("class[]", class_is);                     
                           fd.append("account_number[]", account_number);
                           fd.append("oid[]", oid);
                        }
                        $.ajax({
                              url: '<?php echo base_url('property/update_linked_account'); ?>',
                              type: 'POST',
                              data: fd,
                              processData: false,
                              contentType: false,
                              success: function (data) {                            
                              }
                          });
                    };
                    if (json.cancel || json.close) {
                      $("#synapse_iframe").css("visibility","hidden");
                      $("#synapse_iframe").css("height","0%");
                      $("#synapse_iframe").css("width","0%");
                      $("#synapse_iframe").prop("src","");
                      self.set('enableButton', true);
                    };
                  }catch (e){
                     console.log(e);
                  };
              }
            
          };
        window.addEventListener('message', expressReciver, false);
      });
    //$(".close icon").click(function(){ 
    $(document).on('click', '.close icon', function (event) {
       /* alert("hi");*/
        $("#synapse_iframe").css("visibility","hidden");
        $("#synapse_iframe").css("height","0%");
        $("#synapse_iframe").css("width","0%");
        $("#synapse_iframe").prop("src","");
    });
    });
</script>
<?php
    //}
    ?>
<div class="level">
    <div class="container">
        <div class="level-indicator">
            <img src="<?= $this->config->item('templateassets') ?>images/full-level.png" alt="" />
            <ul>
                <li> Property Info</li>
                <li>Tenants</li>
                <li class="blue-col1">tServices</li>
            </ul>
        </div>
    </div>
</div>
<section class="signup-section2">
    <div class="container">
        <?php echo form_open_multipart('', array('class' => 'payment_form', 'id' => 'payment_form')) ?>     
        <div class="ajax_report alert display-hide" role="alert" style="margin-bottom: 10px; margin-left: 0px; width: 500px; position: relative; top: 100px;">
            <span class="close-message"></span>
            <div class="ajax_message">Saved</div>
        </div>
        <div class="tpay-acc-setup">
            <div class="page_heading tpay_setup_heading">tPay Set Up/Edit</div>
            <div clas="notify_message" style="color:#9F713D">
                <?php 
                    if(isset($account_info->kyc_permission) && $account_info->kyc_permission == 'UNVERIFIED'){
                         echo "Please wait while we are verify your account.";
                    }
                    ?>
            </div>
            <div class="tpay-acc-setup-left settipp">
                <h3>Step 1 : General Information </h3>
                <div class="step-in">
                    <?php if(!empty($account_info)){ ?>
                    <div class="same_address_checkbox sac"> 
                        <input type="checkbox" value="Yes" name="same_address_acc" class="same_address" id="same_address"> Same as account information
                    </div>
                    <?php }else{ ?>
                    <div class="same_address_checkbox sac"> 
                        <input type="checkbox" value="Yes" name="same_address_mem" class="same_address" id="same_address"> Same as account information
                    </div>
                    <?php } ?> 
                    <div class="tpay-field-part">
                        <span class="req-field">
                        <label class="com_label">First Name</label>
                        <input type="text" name="first_name" id="first_name" class="form-control my-txt-fleid1 error"  maxlength="20" value="<?php echo isset($account_info->first_name) ? $account_info->first_name : ''; ?>" placeholder="First name"><em>*</em>
                        </span>
                    </div>
                    <div class="tpay-field-part tpay-field-divide">
                        <span class="req-field wh"> 
                        <label class="com_label">Middle Name</label>
                        <input type="text" name="middle_name" id="middle_name"  class="form-control1 my-txt-fleid1 error" maxlength="15" value="<?php echo isset($account_info->middle_name) ? $account_info->middle_name : ''; ?>" placeholder="Middle name">
                        <em>*</em>
                        </span>
                        <span class="req-field"> 
                        <label class="com_label">Last Name</label>
                        <input type="text" name="last_name" id="last_name"  class="form-control1 my-txt-fleid1 error"  maxlength="15" value="<?php echo isset($account_info->last_name) ? $account_info->last_name : ''; ?>" placeholder="Last name">
                        <em>*</em>
                        </span>
                        <label id="middle_name-error" style="width:40%" class="error" for="middle_name"></label>
                        <label id="last_name-error" style="width:40%" class="error" for="last_name"></label>
                    </div>
                    <div class="tpay-field-part">
                        <span class="req-field"> 
                        <label class="com_label">Phone</label>
                        <input type="text" class="phone_us" id="phone_us"  class="form-control my-txt-fleid1 error"  name="phone" value="<?php echo isset($account_info->phone) ? $account_info->phone : ''; ?>" placeholder="Phone">
                        <em>*</em>
                        </span>
                    </div>
                    <div class="tpay-field-part">
                        <span class="req-field">  
                        <label class="com_label">Email</label>
                        <input type="email" name="email" id="email"  class="form-control my-txt-fleid1 error"  value="<?php echo isset($account_info->email) ? $account_info->email : ''; ?>" placeholder="Email">
                        <em>*</em>
                        </span>
                    </div>
                    <div class="tpay-field-part">
                        <span class="req-field ">  
                        <label class="com_label">Country</label>
                        <input type="text" name="address_country_code" id="address_country_code"  class="form-control my-txt-fleid1 error" readonly="readonly" value="US" placeholder="Address country code">
                        <input type="hidden" name="address_line_1" id="address_line_1"  class="form-control my-txt-fleid1 error" style="display:none" value="<?php //echo isset($account_info->address_line_1) ? $account_info->address_line_1 : ''; ?>" placeholder="Address  ">
                        <!-- <em>*</em> -->
                        </span>
                    </div>
                    <div class="tpay-field-part">
                        <label class="com_label">Address</label>
                        <span class="req-field">
                        <input type="text" name="address_street" id="address_street"  class="form-control my-txt-fleid1 error"  value="<?php echo isset($account_info->address_street) ? $account_info->address_street : ''; ?>" placeholder="Address">
                        <em>*</em>
                        </span>
                    </div>
                    <div class="clearfix"></div>
                    <div class="tpay-field-part">
                        <span class="req-field"> 
                        <label class="com_label">City</label>
                        <input type="text" name="address_city" id="address_city"  class="form-control1 my-txt-fleid1 error"  value="<?php echo isset($account_info->address_city) ? $account_info->address_city : ''; ?>" placeholder="City">
                        <em>*</em>
                        </span>
                        <label id="city-error" style="width:40%" class="error" for="city"></label>
                    </div>
                    <div class="tpay-field-part tpay-field-divide">
                        <span class="req-field address_subdivision_cont">
                            <label class="com_label">State</label>
                            <?php $usa_state= array(    'AL' => 'Alabama',    'AK' => 'Alaska',  'AZ' => 'Arizona',     'AR' => 'Arkansas',   'CA' => 'California', 'CO' => 'Colorado',                         'CT' => 'Connecticut',                         'DE' => 'Delaware',                         'DC' => 'District Of Columbia',                         'FL' => 'Florida',                         'GA' => 'Georgia',                         'HI' => 'Hawaii',                         'ID' => 'Idaho',                         'IL' => 'Illinois',                         'IN' => 'Indiana',                         'IA' => 'Iowa',                         'KS' => 'Kansas',                         'KY' => 'Kentucky',                         'LA' => 'Louisiana',                         'ME' => 'Maine',                         'MD' => 'Maryland',                         'MA' => 'Massachusetts',                         'MI' => 'Michigan',                         'MN' => 'Minnesota',                         'MS' => 'Mississippi',                         'MO' => 'Missouri',                         'MT' => 'Montana',                         'NE' => 'Nebraska',                         'NV' => 'Nevada',                         'NH' => 'New Hampshire',                         'NJ' => 'New Jersey',                         'NM' => 'New Mexico',                         'NY' => 'New York',                         'NC' => 'North Carolina',                         'ND' => 'North Dakota',                         'OH' => 'Ohio',                         'OK' => 'Oklahoma',                         'OR' => 'Oregon',                         'PA' => 'Pennsylvania',                         'RI' => 'Rhode Island',                         'SC' => 'South Carolina',                         'SD' => 'South Dakota',                         'TN' => 'Tennessee',                         'TX' => 'Texas', 'UT' => 'Utah',  'VT' => 'Vermont',  'VA' => 'Virginia', 'WA' => 'Washington', 'WV' => 'West Virginia',   'WI' => 'Wisconsin','WY' => 'Wyoming',);  ?>
                            <select name="address_subdivision" id="address_subdivision"  class="form-control1 my-txt-fleid1 error">
                                <?php foreach ($usa_state as $key => $value) { ?>
                                <option <?php echo (isset($account_info->address_subdivision) && $account_info->address_subdivision == $key) ? 'selected' : ''; ?> value="<?php echo $key; ?>"><?php echo $key; ?></option>
                                <?php } ?>
                            </select>
                            <!-- <input type="text" name="address_subdivision" id="address_subdivision"  class="form-control1 my-txt-fleid1 error"  value="<?php //echo isset($account_info->address_subdivision) ? $account_info->address_subdivision : ''; ?>" placeholder="Address subdivision"> -->
                            <em>*</em>
                        </span>
                        <span class="req-field">  
                        <label class="com_label">Zip</label>
                        <input type="text" name="address_zip_code" id="address_zip_code"  class="form-control1 my-txt-fleid1 error"  value="<?php echo isset($account_info->address_zip_code) ? $account_info->address_zip_code : ''; ?>" placeholder="Zip">
                        <em>*</em>
                        </span>
                        <label id="address_subdivision-error" style="width:40%" class="error" for="address_subdivision"></label>
                        <label id="address_zip_code-error" style="width:40%" class="error" for="address_zip_code"></label>
                    </div>
                    <div class="tpay-field-part tpay-field-divide">
                        <span class="req-field"> 
                        <label class="com_label">DOB</label>
                        <input type="text" readonly name="dob" id="dob"  readonly class="form-control1 my-txt-fleid1 error accountdob" value="<?php echo (isset($account_info->dob) && $account_info->dob !=0 ) ? getDateByFormat( $account_info->dob,'m/d/Y' ) : ''; ?>"  placeholder="DOB">
                        <em>*</em>
                        </span>
                        <span class="req-field"> 
                        <label class="com_label">Social Security</label>
                        <input type="text" name="social_security" id="social_security"  class="form-control1 my-txt-fleid1 error" value="<?php echo isset($account_info->social_security) ? $account_info->social_security : ''; ?>" placeholder="Last 4 S.S">
                        <em>*</em>
                        </span>
                        <label id="dob-error" style="width:40%" class="error" for="dob"></label>
                        <label id="social_security-error" style="width:40%" class="error" for="social_security"></label>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <!--<h3>Step 3 : Documentation ( one time only )</h3>-->
                <div class="step-in2">
                    <div class="acc-contact-left">
                      <p><em>*</em> Please upload a copy of your photo id. If unable to upload contact us at support@tenanttag.com. </p>
                        <!-- <div>
                            <h4>Email : 123@gmail.com</h4>
                            <h4>Fax : 888-888-8888</h4>
                        </div> -->
                        <p class="dr-li">
                            <span class="lab">Driver's Licence or Photo ID :  <a class="ach_href" data-toggle="modal" data-target="#id_page_modal">Example</a></span>
                            <span class="upload-file">
                            <img src="<?= $this->config->item('templateassets') ?>images/text-file-5-xxl.png" alt="" />
                            Upload ID
                            <input type="file" name="driving_document" id="driving_document"  value="Upload">
                            </span>
                            <span class="document_span">
                            <span class="driving_document_file_name ls_file_name">
                            <?php if(isset($account_info->driving_document) && $account_info->driving_document != ''){ ?>
                            <?php echo $account_info->driving_document; ?>
                            <?php } ?>
                            </span>
                            <?php if(isset($account_info->driving_document) && $account_info->driving_document != ''){ ?>
                            <a href="javascript:void(0);"  name="<?php echo $account_info->id; ?>"  id="delete_driving_document" class="btn btn-del btn-primary">Delete</a>
                            <a target="_blank"   id="edit_driving_document" href="<?php base_url()?>assets/uploads/user_document/user_<?php echo $account_info->user_id; ?>/<?php echo $account_info->driving_document; ?>"  class="btn btn-edit btn-primary">View</a>
                            <?php } ?>                                          
                            </span>
                        </p>
                        
                        <p class="dr-li add-ma" style="display:none">
                            <span class="lab">ACH authorization form :</span>
                            <span class="browse-dr-li">
                            <span class="upload-file">
                            <img src="<?= $this->config->item('templateassets') ?>images/text-file-5-xxl.png" alt="" />
                            Upload File
                            <input type="file" name="recent_bank_statment" id="recent_bank_statment" value="Upload">
                            </span>
                            </span>
                            <span class="recent_bank_span">
                            <span class="recent_bank_statment_file_name ls_file_name">
                            <?php if(isset($account_info->recent_bank_statment) && $account_info->recent_bank_statment != ''){ ?>
                            <?php echo $account_info->recent_bank_statment; ?>
                            <?php } ?>
                            </span>
                            <?php if(isset($account_info->recent_bank_statment) && $account_info->recent_bank_statment != ''){ ?>
                            <a href="javascript:void(0);" name="<?php echo $account_info->id; ?>"  id="delete_recent_bank_statment" class="btn btn-del btn-primary">Delete</a>
                            <a id="edit_recent_bank_statment" target="_blank" href="<?php base_url()?>assets/uploads/user_document/user_<?php echo $account_info->user_id; ?>/<?php echo $account_info->recent_bank_statment; ?>"  class="btn btn-edit btn-primary">View</a>
                            <?php } ?>
                            </span>
                        </p>
                    </div>
                    <div class="clearfix"> </div>
                    <div class="policy-agreement">
                        <?php //print_r($account_info); ?>
                        <!--<input checked  style="" type="checkbox" name="ach_aggrement"  value="yes" />-->
                        <p class="check-design"><em>*</em> Please review the ACH Authorization Form and confirm your agreement at the bottom of the form.  </p>
                        <div class="agreement1 ">
                            <span class="ach_scroll"> 
                            Direct Payment via ACH is the transfer of funds from a consumer account for the purpose of making a payment. I (we) authorize Coastal Investment Group, LLC, dba TenantTag, and Coastal Investment Group, LLC's payment processing partner, SynapseFi, to electronically credit ACH transactions into my (our) Linked Bank Account(s) (and, if necessary, debit ACH transactions out of my (our) Linked Bank account(s) to correct for erroneous credits) for the purpose of rent collection. 
                            <p class="check-design"><input  class="form-control1 my-txt-fleid1 error"  <?php echo (isset($account_info->ach_aggrement) && $account_info->ach_aggrement == 'Yes') ? 'checked' : ''; ?> type="checkbox" name="ach_aggrement"  value="yes" />  
                            <label id="ach_aggrement-error" class="" for="ach_aggrement">I (we) Agree</label> 
                            <!--<a class="ach_href" data-toggle="modal" data-target="#ach_form_modal"> See ACH Form </a></p>-->
                            </span>
                            
                        </div>
                        <div style="  color: #585858; position:relative; margin-left:3.5%;  display: block;    font-size: 14px;    line-height: 36px;" class="check-design">
                            <!--Terms and Conditions -->
                            <input style=" left: 0;    margin-left: -17px !important;    position: absolute; top: -21px;" class="form-control1 my-txt-fleid1 error" <?php echo (isset($account_info->terms_conditions) && $account_info->terms_conditions == 'Yes') ? 'checked' : ''; ?> type="checkbox" name="terms_conditions" value="yes" />
                            <label id="terms_conditions-error" class="" for="terms_conditions">
                            <em>*</em>
                                
                                I (we) acknowledge that I (we) have read through and agree to the <a href="https://tenanttag.com/page/terms" target="_blank">Terms Of Service</a>
                                and <a href="https://tenanttag.com/page/privacy" target="_blank">Privacy Policy</a> of Coastal Investment Group, LLC dba as TenantTag and to 
                                <a href="https://synapsefi.com/tos-evolve" target="_blank">Terms Of Service</a>
                                and <a href="https://synapsefi.com/privacy" target="_blank">Privacy Policy</a> of SynapseFi, Coastal Investment Group LLC.'s payment partner.
                            </label>
                        </div>
                         
                    </div>
                    <div class="bank-center thy">
                        <p>GENERAL INFORMATION MUST BE APPROVED BEFORE LINKING A BANK ACCOUNT</p>
                        <p>CHECK EMAIL FOR STATUS UPDATE</p>
                    </div>
                    <div class="bank-center save-center" style="margin-bottom:20px;">
                        <?php if(isset($_REQUEST['dashboard'])){ ?>
	                        <input type="hidden" name="redirect" value="dashboard">
	                        <a  class="my-link" href="<?= site_url('dashboard') ?>"  name=""  />Back</a>
	                        <?php if(getSynapseKycStatus($this->session->userdata('MEM_ID')) != true){ ?>
		                          <span class="save-rtrn"><span class="btn-save">  <span style="padding:8px 0px">Submit for Approval</span>  </span> 
		                          <!--<span class="save-rtrn"><span class="btn-save">Save <span>Submit for Approval</span>  </span> -->
		                          <input type="submit" name="" class="save_return_dashboard" value=" Return to Dashboard" /> </span>
	                        <?php }else{ ?> 
		                          <span class="save-rtrn"><span class="btn-save">Approved <span>Submit Update</span>  </span> 
		                          <input type="submit" name="" class="save_return_dashboard" value=" Return to Dashboard" /> </span>
	                        <?php } ?>
	                        <br>
                        <?php } else{ ?>
	                        <input type="hidden" name="redirect" value="">
	                        <a  class="my-link" href="<?= site_url('add-property-step3') ?>"  name=""  />Back</a>
	                        <?php if(getSynapseKycStatus($this->session->userdata('MEM_ID')) != true){ ?>
	                        	<input type="submit" name="" class="save_prop_return_tservices" value="Submit for Approval" />
	                        <?php }else{ ?> 
	                        	<!-- <input type="submit" name="" class="save_prop_return_tservices" value="Submit for Approval" /> -->
	                        	<span class="save-rtrn"><span class="btn-save">Approved <span>Submit Update</span>  </span> 
		                        <input type="submit" name="" class="save_return_dashboard" value=" Return to Dashboard" /> </span>
	                        <?php } ?> 
	                        <br>
                        <?php } ?>
                        <!--  <a href="javascript:;" class="my-link save-later-account-setup-process">Save for Later</a> -->
                    </div>
                    <div class="clearfix"></div>
                </div>

                <h3>Step 2 : Bank Information</h3>
                <div class="step-in linked-btn-sec">
                    <div class="linkedBankAccount_div" style="width:22%;float:left">
                        <!-- <input type="button" name="linked_bank_account" id="linked_bank_account" value="Link Bank Account" > -->
                        <?php if(getSynapseKycStatus($this->session->userdata('MEM_ID')) == true){ ?>
                        <input type="button" name="linked_bank_account" id="custom_linked_bank_account" data-toggle="modal" data-target="#custom_linked_bank_account_model" value="Link Bank Account" >
                        <?php }else{ ?>
                        <input type="button" name="linked_bank_account" id="custom_linked_bank_account" data-toggle="modal" data-target="#custom_linked_bank_account_unverfied_model" value="Link Bank Account" >
                        <?php } ?>
                    </div>
                    <div id="linked-acc"  style="width:72%;float:right;padding-top: 0;">
                        <label style="width: 20%;padding-top: 10px;">Bank Account</label>                       
                        <!-- <h4>Linked Bank Account</h4> -->
                        <div class="linked-acc-part wb-linkedacc-part"  style="float: left;">
                            <?php //print_r($account_info); exit; ?>
                            <p>
                            <select name="bank_account" id="bank_account"  class="form-control1 my-txt-fleid1 error">
                                <option value="">Select Account... </option>
                                <?php foreach ($linked_account_number as $key => $value) { ?>
	                                <?php  if(isset($value->status) && $value->status == 1){ ?> 
	                                	<option selected value="<?php echo $value->id; ?>"><?php echo $value->bank_name.' '.$value->class.' xxxx-xxxx-xxxx-'.$value->account_number; ?> </option>
	                                <?php }else{ ?>
	                                	<option value="<?php echo $value->id; ?>"><?php echo $value->bank_name.' '.$value->class.' xxxx-xxxx-xxxx-'.$value->account_number; ?> </option>
	                                <?php } ?>
                                <?php } ?>
                            </select>
                            <?php //if(getSynapseKycStatus($this->session->userdata('MEM_ID')) != true){ ?>
                            <?php if($tpay_account_status == 0){ ?>
                                    <span style="border:none;border-radius:0;display:inline-block;float:left;height:35px;top:4px;width:40px;left:8px;" id="red_green_toogle" class="togle" data-type="" data-value="" data-property="No">
                                        <span class="check"></span>
                                    </span>
                            <?php }else{ ?>
                                    <span style="border:none;border-radius:0;display:inline-block;float:left;height:35px;top:4px;width:40px;" id="red_green_toogle" class="togle stop" data-type="" data-value="" data-property="Yes">
                                        <span class="check"></span>
                                    </span>
                                    
                            <?php }  ?>
                                    <a href="#" data-toggle="tooltip" class="href_tooltip" tooltip="Account is Linked when Green"><span class="glyphicon glyphicon-info-sign"></span></a>
                            </p>
                            <div class="micro_deposit_form_div" id="micro_deposit_form_div">
                                <?php //print_r($selected_linked_account_number); exit; ?>
                                <?php if(!empty($selected_linked_account_number) &&  $selected_linked_account_number->bank_code == 'other' &&  $selected_linked_account_number->synapse_micro_permission != 'CREDIT-AND-DEBIT') { ?>
                                <div class="micro_deposit">
                                    <span class="dolar-sign"><i class="fa fa-usd" style="font-size:14px"></i></span> <input type="text" id="deposit1" name="deposit1" placeholder="Micro Amount 1" aria-invalid="false" class="valid">
                                    <span class="dolar-sign"><i class="fa fa-usd" style="font-size:14px"></i></span>  <input type="text" id="deposit2" name="deposit2" placeholder="Micro Amount 2" aria-invalid="false" class="valid">
                                    <input type="button" id="update_micro_deposit" name="update_micro_deposit" value="Update" class="btn">
                                    <a href="#" data-toggle="tooltip" tooltip="Verify micro deposit">
                                    <span class="glyphicon glyphicon-info-sign">    </span>
                                    </a>
                                </div>
                                    <div id="micro_deposit_message" name="micro_deposit_message" class="">
                                    </div>
                                <p class="micro_note">Please confirm the 2 microdeposits placed into your Bank Account.  Once confirmed, the account will be active. Microdeposits appear within 2 business days.</p>
                                <?php } ?>
                            </div>
                        </div>
                        <a  style="width: 7%;  display: none;padding-top: 10px;" href="javascript:void(0)" style="display:none" id="remove_sel_acc" class="remove_sel_acc"><img src="<?= $this->config->item('templateassets') ?>images/cross.png" alt=""/></a>
                        <!--<span class="acc-show">Routing </span>
                            <span class="acc-show">Account xxxx-xxxx-xxxx </span> -->                  
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="tpay-acc-setup-right">
                <h4>Commonly Asked Questions</h4>
                <?php echo $getCommonQuestionByCategory; ?>
                <!-- <span>Lorem ipsum dolor sit amet, consec tetur adipi scing elit, sed do eius mod tempor incididunt ut labore et dolore magna aliqua.</span>
                    <span>sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris.</span>
                    
                    <span>Ut enim ad minim veniam, quis nostrud exer citation ullamco laboris nisi ut aliquip ex ea commodo consequat.</span>-->
            </div>
            <div class="clearfix"></div>
            <input type="hidden" value="save" class="action-type" name="action-type">
        </div>
        <?php echo form_close(); ?>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function(){
         $(".bank_anchor_class").click(function(){
              var bank_code = $(this).attr('id'); 
              var bank_img = $('img', this).attr('src'); 
              $.ajax({
                   url: '<?php echo base_url('property/synapse_bank_linked'); ?>',
                   type: 'POST',
                   data: {bank_code: bank_code,bank_img: bank_img},
                   beforeSend: function () {
                        $('#wait-div').addClass('zindex');
                        $('#wait-div').show();
                   },
                   success: function (response) {
                        var response = jQuery.parseJSON(response);
                        $('#wait-div').removeClass('zindex');
                        $("#wait-div").hide(); 
                        //alert(response.success);
                        if(response.success){
                             $('#custom_linked_bank_account_model').find('.select_bank_c').removeClass('active');
                             $('#custom_linked_bank_account_model').find('.login_bank_cl').addClass('active');
    
                             $('#custom_linked_bank_account_model').find(".bank_list").hide()
                             $('#custom_linked_bank_account_model').find(".bank_login_view").html(response.html)
                        }else{
                             $('#custom_linked_bank_account_model').find(".linked_bank_model_body").html(response.error_message)                              
                        }
                   },
                   error: function () {
                             $('#wait-div').removeClass('zindex');
                             $("#wait-div").hide();
                             swal("Error!", "Server not responding.", "error");
                        }
              });
         });
    });
    $(document).on('click', '.go_back_to_home', function(){
              //window.location=window.location;  
              $('#custom_linked_bank_account_model').find(".bank_list").show()
             $('#custom_linked_bank_account_model').find(".bank_login_view").html(' ')        
    });
    
    $(document).ready(function () {
         $('#custom_linked_bank_account_model').on('hidden.bs.modal', function () {
             //location.reload();
             $('#custom_linked_bank_account_model').find(".bank_list").show()
             $('#custom_linked_bank_account_model').find(".bank_login_view").html(' ')
         });
    });
    $(document).on('click', '.go_back_to_first_step', function(){
         $('#custom_linked_bank_account_model').find(".bank_list").show()
         $('#custom_linked_bank_account_model').find(".bank_login_view").html(' ')
         //window.location=window.location;          
    });
    $(document).on('click', '.submit_login_form', function(){
         var submit_but = $(this);
         submit_but.attr("disabled",true);
         var bank_code = $(this).attr('id'); 
         var data = $("#bank_login_form").serialize();
         var formClass = '#bank_login_form';
         $.ajax({
              url: '<?php echo base_url('property/getSynapseBankDetail'); ?>',
              type: 'POST',
              data: data,
              beforeSend: function () {
                   $('#wait-div').addClass('zindex');
                   $('#wait-div').show();
    
              },
              success: function (response) {
                   var response = jQuery.parseJSON(response);
                   $('#wait-div').removeClass('zindex');
                   $("#wait-div").hide();
                   console.log(response);
                   submit_but.attr("disabled",false);
                   if(response.success){
                     if(response.data.http_code == '202'){
                        $('#custom_linked_bank_account_model').find(".header_container_linked").html('<div class="header_success">Verify Account</div>');
                     }else{
                        $('#custom_linked_bank_account_model').find(".header_container_linked").html('<div class="header_success">Select account number</div>');
                     }
                        //$(formClass).find('.ajax_report').addClass('alert-success').css("display",'block').children('.ajax_message').html(response.message).css("display","block");
                        $('#custom_linked_bank_account_model').find(".bank_login_view").html(response.account_list_html);
                   }else{
                        $(formClass).find('.ajax_report').addClass('alert-danger').css("display",'block').children('.ajax_message').html(response.error_message).css("display","block");
                        //$('#custom_linked_bank_account_model').find(".bank_login_view").html(response.error_message)                              
                   }
              },
              error: function () {
                        $('#wait-div').removeClass('zindex');
                        $("#wait-div").hide();
                        submit_but.attr("disabled",false);
                        $(formClass).find('.ajax_report').addClass('alert-danger').css("display",'block').children('.ajax_message').html(response.error_message).css("display","block");
                        swal("Error!", "Server not responding.", "error");
                   }
         });
    });
    
    $(document).on('click', '.mfa_submit', function(){
       var submit_but = $(this);
       submit_but.attr("disabled",true);
       //var bank_code = $(this).attr('id'); 
       var data = $("#mfa_form").serialize();
       var formClass = '#mfa_form';
       $.ajax({
           url: '<?php echo base_url('property/getSynapseBankDetail'); ?>',
           type: 'POST',
           data: data,
           beforeSend: function () {
               $('#wait-div').addClass('zindex');
               $('#wait-div').show();
    
           },
           success: function (response) {
               var response = jQuery.parseJSON(response);
               $('#wait-div').removeClass('zindex');
               $("#wait-div").hide();
               console.log(response);
               submit_but.attr("disabled",false);
               if(response.success){
                   if(response.data.http_code == '202'){
                       $('#custom_linked_bank_account_model').find(".header_container_linked").html('<div class="header_success">Verify Account</div>');
                   }else{
                       $('#custom_linked_bank_account_model').find(".header_container_linked").html('<div class="header_success">Select account number</div>');
                   }
                       //$(formClass).find('.ajax_report').addClass('alert-success').css("display",'block').children('.ajax_message').html(response.message).css("display","block");
                   $('#custom_linked_bank_account_model').find(".bank_login_view").html(response.account_list_html);
               }else{
                   $(formClass).find('.ajax_report').addClass('alert-danger').css("display",'block').children('.ajax_message').html(response.error_message).css("display","block");
                   setTimeout(function () {
                       //window.location = window.location;                             
                   }, 4000);
                       //$('#custom_linked_bank_account_model').find(".bank_login_view").html(response.error_message)                              
               }
           },
           error: function () {
                $('#wait-div').removeClass('zindex');
                $("#wait-div").hide();
                submit_but.attr("disabled",false);
                $(formClass).find('.ajax_report').addClass('alert-danger').css("display",'block').children('.ajax_message').html(response.error_message).css("display","block");
                swal("Error!", "Server not responding.", "error");
           }
       });
    });
    
    $(document).on('click', '.select_account_submit', function(){
         var submit_but = $(this);
         submit_but.attr("disabled",true);
         var data = $("#select_account_number_form").serialize();
         var dropdown_bank_account = $('#bank_account');
         var id_option = $('#bank_account option');
         var formClass = '#select_account_number_form';
         $.ajax({
              url: '<?php echo base_url('property/updateSynapseLinkedAccounts'); ?>',
              type: 'POST',
              data: data,
              beforeSend: function () {
                   $('#wait-div').addClass('zindex');
                   $('#wait-div').show();
    
              },
              success: function (response) {
                   var response = jQuery.parseJSON(response);
                   $('#wait-div').removeClass('zindex');
                   $("#wait-div").hide();
                   console.log(response);
                   submit_but.attr("disabled",false);
                   if(response.success){
                        $(formClass).find('.ajax_report').addClass('alert-success').css("display",'block').children('.ajax_message').html(response.success_message).css("display","block");
                        setTimeout(function () {
                            window.location = window.location;
                            if(response.typeis == 'add'){
                                  dropdown_bank_account.append(response.html);
                            }else{
                                  id_option.each(function(){
                                       dropdown_bank_account.removeAttr("selected");
                                       if($(this).val() == response.html){
                                            dropdown_bank_account.attr('selected','selected');                                                                        
                                       }
                                  });
                            }
                            $('#custom_linked_bank_account_model').modal('hide');
                        }, 2000);
                   }else{
                        $(formClass).find('.ajax_report').addClass('alert-danger').css("display",'block').children('.ajax_message').html(response.error_message).css("display","block");
                   }
              },
              error: function () {
                        $('#wait-div').removeClass('zindex');
                        $("#wait-div").hide();
                        submit_but.attr("disabled",false);
                        $(formClass).find('.ajax_report').addClass('alert-danger').css("display",'block').children('.ajax_message').html(response.error_message).css("display","block");
                        swal("Error!", "Server not responding.", "error");
                   }
         });
    });
    
    
     
</script>
<?php if(getSynapseKycStatus($this->session->userdata('MEM_ID')) == true){ ?>
<div id="custom_linked_bank_account_model" width="70%" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close go_back_to_home" data-dismiss="modal">&times;</button>
                <div class="header_container_linked">
                    <div class="ui ordered steps" >
                        <div class="active step select_bank_c go_back_to_first_step">
                            <span>1 </span> 
                            <h4>Bank Select <span>Select your bank</span> </h4>
                        </div>
                        <div class=" step login_bank_cl">
                            <span>2 </span> 
                            <h4>Bank Login <span>Login to your account</span> </h4>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-body linked_bank_model_body">
                <div class="bank_list">
                    <ul>
                        <li class="btn-anybank" style="width:98%; text-align:center"><a style="font-size:14px" href="javascript:;" class="bank_anchor_class" alt="Bank" id="other">Confirm Any Bank Account Using Account and Routing #s</a></li>
                    </ul>
                    <ul>
                        <?php
                            $json = file_get_contents('https://synapsepay.com/api/v3/institutions/show');
                            $array = json_decode($json);
                            $urlPoster=array();
                            $bank_lists = $array->banks;
                            foreach ($bank_lists as $key => $value) { 
                                 ?>
                        <li><a href="javascript:;" class="bank_anchor_class" alt="Bank" id="<?php echo $value->bank_code; ?>" > <img class="bank_img_class" src="<?php echo $value->logo; ?>"><?php echo $value->bank_name; ?> </a></li>
                        <?php
                            }
                            ?>
                    </ul>
                </div>
                <div class="bank_login_view">
                </div>
            </div>
        </div>
    </div>
</div>
<?php } ?>
<?php if(isset($fingerprint_auth_key) && $fingerprint_auth_key == '') { ?>
<div id="verfication_model" width="70%" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <!--<button type="button" class="close go_back_to_home" data-dismiss="modal">&times;</button>-->
                <?php //print_R($fingerprint_auth_key);  ?>
                Verify the PIN sent to the 2FA device <?php //print_r($mem_info); ?>
            </div>
            <div class="modal-body">
                <form name="varify_form" id="verify_form" method="post">
                    <div class="ajax_report alert display-hide" role="alert" style="margin-bottom: 10px; margin-left: 0px; width: 500px; position: relative; top: 5px !important;">
                        <span class="close-message"></span>
                        <div class="ajax_message">Saved</div>
                    </div>
                    <div class="tpay-field-part" style="height:auto">
                        <span class="req-field">
                        <label class="com_label">Phone Number</label>
                        <input type="text" name="phone_number" id="phone_number"   class="form-control phone_us my-txt-fleid1 error" value="<?php echo $mem_info->mobile_no; ?>">
                        </span>
                    </div>
                    <div class="tpay-field-part" style="height:auto">
                        <span class="req-field">
                        <label class="com_label"></label>
                        <input type="button" class="btn-tenant" name="send_verifcation_code" value="Send Verification code" id="send_verifcation_code" style="background: #00659f none repeat scroll 0 0;    color: #fff;    font-weight: normal;    margin-top: 21px;">
                        </span>
                    </div>
                    <div class="clearfix"></div>
                    <div class="tpay-field-part"  style="width:100%;height:auto">
                        <br>
                        <span class="req-field">
                        <label class="com_label">Validation Pin</label>
                        <input type="text" name="validation_pin" class="form-control my-txt-fleid1 error"  maxlength="20" value="" id="validation_pin">
                        </span>
                    </div>
                    <div class="clearfix"></div>
                    <input type="button" class="btn-tenant" name="submit_verify_code" value="submit" id="submit_verify_code">
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(window).on('load',function(){
        $('#verfication_model').modal('show');
    });
    $('#verfication_model').on('hidden.bs.modal', function () {
         location.reload();
    })
    $(document).ready(function () {
        $("#submit_verify_code").click(function () {
            // this will use all available fingerprinting sources
             var verify_form = $("#verify_form");
             var data = $("#verify_form").serialize();
            $.ajax({
                url: '<?php echo base_url("property/verifySynapseMFA"); ?>',
                type: 'POST',
                data: data,
                beforeSend: function () {
                    $('#wait-div').hide();
                },
                success: function (data) {
                    console.log(data);
                    var data = $.parseJSON(data);
                    if(data.status == 'success'){
                        verify_form.find(".ajax_report").addClass('alert-success').removeClass('alert-danger').css("display",'block').children('.ajax_message').html(data.message).css("display","block");
                        setTimeout(function () {
                            window.location = window.location;                             
                        }, 4000);
                    }else{
                        verify_form.find(".ajax_report").addClass('alert-danger').removeClass('alert-success').css("display",'block').children('.ajax_message').html("Invalid MFA PIN supplied.").css("display","block");                            
                    }
    
                    $("#wait-div").hide();                     
                },
                error: function () {
                    $("#wait-div").hide();                   
                }
            });
        });
    });
    
    $(document).ready(function () {
        $("#send_verifcation_code").click(function () {
            // this will use all available fingerprinting sources
             var verify_form = $("#verify_form");
             var data = $("#verify_form").serialize();
            $.ajax({
                url: '<?php echo base_url("property/sendVerificationCodeByPost"); ?>',
                type: 'POST',
                data: data,
                beforeSend: function () {
                    $('#wait-div').show();
                    $("#wait-div").css("z-index","9999");
                },
                success: function (data) {
                    $('#wait-div').hide();
                    $("#wait-div").css("z-index","1");
                    console.log(data);
                    var data = $.parseJSON(data);
                    if(data.status == 'success'){
                        verify_form.find(".ajax_report").addClass('alert-success').removeClass('alert-danger').css("display",'block').children('.ajax_message').html(data.message).css("display","block");
                        setTimeout(function () {
                           // window.location = window.location;                             
                        }, 4000);
                    }else{
                        verify_form.find(".ajax_report").addClass('alert-danger').removeClass('alert-success').css("display",'block').children('.ajax_message').html(data.message).css("display","block");                            
                    }
    
                    $("#wait-div").hide();                     
                },
                error: function () {
                    $("#wait-div").hide();                   
                }
            });
        });
    });
</script>
<?php } ?>
<div id="custom_linked_bank_account_unverfied_model" width="70%" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close go_back_to_home" data-dismiss="modal">&times;</button>
                <div class="header_container_linked">
                    <div class="ui ordered steps" >
                        Please fill the document            
                    </div>
                </div>
            </div>
            <div class="modal-body linked_bank_model_body">
                Unable to link bank account since document are not verify.
            </div>
        </div>
    </div>
</div>

<div id="ach_form_modal" width="70%" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close go_back_to_home" data-dismiss="modal">&times;</button>
                ACH Form
            </div>
            <div class="modal-body ach_form_body">
                <ul>
                    <li>
                        Direct Payment via ACH transfer of funds from a consumer account for the purpose of making the payment
                    </li>
                    <li>
                        I (we) authorize TenantTag and TenantTag's  payment partner, SynapseFi to electronically credit ACG transactions into my(our) linked bank account(s) 
                        (and if necesssary, debit ACH transactions ot of my(our) Linked Bank Account(s) to correct erroneous credits) for the purpose of rent collection.</li>
                    <li>
                        I (we) understand that this autorization will remain in full force and effect until  
                        I (we) cancel the rent collection service through my (our) account with TenantTag at www.tenanttag.com .
                    </li>
                </ul>

            </div>
        </div>
    </div>
</div>
<div id="id_page_modal" width="50%" class="modal fade" role="dialog">
    <div class="modal-dialog modal-sm">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close go_back_to_home" data-dismiss="modal">&times;</button>
                <b>Photo Tips</b>
            </div>
            <div class="modal-body id_page_body">
                <center><h2 style="margin-top: 0px; margin-bottom: 0px; font-size: 28px;">ID</h2></center>
                <ul>
                    <li> Place ID on dark background.</li>
                    <li> Well lit area, no shadows.</li>
                    <li> 4 Corners of ID visible.</li>
                    <li> Close-up of ID, No thumbnails.</li>
                    <li> JPEG</li>
                    <li> 500 KB to 1MB is fine.</li>
                </ul>
            </div>
        </div>
    </div>
</div>

<style>
    [tooltip]:hover:before {
    background: #00659f;
    color: #fff;
    font-size: 14px;
    margin-left: 32px;
    margin-top: -5px;
    opacity: 1;
    z-index: 9999;
    }
    [tooltip]:before {
    border: 1px solid #00659f;
    box-shadow: 1px 1px 4px #777777;
    color: #333;
    content: attr(tooltip);
    opacity: 0;
    padding: 10px;
    position: absolute;
    transition: all 0.15s ease 0s;
    max-width: 640px;
    }
    [tooltip]:not([tooltip-persistent]):before {pointer-events:none;}
</style>