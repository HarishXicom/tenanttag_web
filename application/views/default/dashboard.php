
<link href="<?= $this->config->item('templateassets') ?>css/bootstrap.min.css" rel="stylesheet">
<link href="<?= $this->config->item('templateassets') ?>css/bootstrap-dialog.min.css" rel="stylesheet">
<script type="text/javascript" src="<?= $this->config->item('templateassets') ?>js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?= $this->config->item('templateassets') ?>js/jquery.validate.min.js"></script>  
<script type="text/javascript" src="<?= $this->config->item('templateassets') ?>js/bootstrap-dialog.min.js"></script>  

<script type="text/javascript" src="<?= $this->config->item('templateassets') ?>js/fingerprint2.js"></script>
<?php if ($this->session->flashdata('addmoreprop')) { ?>
    <script>
        swal({
            title: "Alert",
            text: 'Do you want to add another property?',
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, add property!",
            cancelButtonText: "No",
            closeOnConfirm: false
        },
        function () {
            window.location.href = "<?php echo base_url('add-property'); ?>"
        });
        
    </script>
    <?php
}
?>
<style>
[tooltip]:hover:before {
    background: #00659f;
    color: #fff;
    font-size: 14px;
    margin-left: 32px;
    margin-top: -5px;
    opacity: 1;
    z-index: 9999;
}
[tooltip]:before {
    border: 1px solid #00659f;
    box-shadow: 1px 1px 4px #777777;
    color: #333;
    content: attr(tooltip);
    opacity: 0;
    padding: 10px;
    position: absolute;
    transition: all 0.15s ease 0s;
    max-width: 640px;
}
[tooltip]:not([tooltip-persistent]):before {pointer-events:none;}


</style>    
<script>

$(document).on('click', '.add-pet-btn', function (event) {
    var assets = '<?= $this->config->item('templateassets') ?>';
    var pets_no = $('.pets_no').val();
    pets_no = parseInt(pets_no) + 1;
    var assets = '<?= $this->config->item('templateassets') ?>';

    var dataToAppend = '<li class="pet-info">';
    dataToAppend += '<li class="pet-info">';
                dataToAppend += '<input type="hidden" id="pets1" value="Yes" name="pets[]">';
           dataToAppend += ' <span>';
                dataToAppend += '<select class="form-control valid" id="pets_type" name="pets_type[]" aria-invalid="false">';
                    dataToAppend += '<option value="">None</option>';
                    dataToAppend += '<option value="dog">Dog</option>';
                    dataToAppend += '<option value="cat">Cat</option>';
                    dataToAppend += '<option value="other">Other</option>';
                dataToAppend += '</select>';
            dataToAppend += '</span>';
            dataToAppend += '<span>';
                dataToAppend += '<input type="text" class="form-control tenant-txtfield" placeholder="Pet Name" id="pets_name" name="pets_name[]">';                           
            dataToAppend += '</span>';
            dataToAppend += '<span class="">';
                dataToAppend += '<input type="text" class="form-control tenant-txtfield" number="true" maxlength="6" digits="true" placeholder="Pet Fee" id="pets_fee" name="pets_fee[]"> ';                          
            dataToAppend += '</span>';
            dataToAppend += '<span class="pet-bred">';
                dataToAppend += '<input type="text" class="form-control tenant-txtfield" placeholder="Pet Breed/Description" id="pets_breed" name="pets_breed[]"> ';                          
            dataToAppend += '</span>';
            dataToAppend += '<span class="">';
                dataToAppend += '<a href="javascript:;" class="delete-pets-row " id="pets_row[]" data-value="" style=""><img  style="padding-top:18px;" src="' + assets + 'images/cross.png" alt=""/></a>';                          
            dataToAppend += '</span>';
        dataToAppend += '</li>';
    $(this).closest('li').after(dataToAppend);
    $('.pets_no').val(pets_no);

});
$(document).on('click', '.delete-pets-row', function () {
        $(this).closest('li').remove();
    });
$(document).delegate('.del_lease_doc', 'click', function () {
        $(this).parent().find('.upl_lease').val('');    
        $(this).parent().html('');
          //  $(".show_file_name2").html('');
            //$(".show_file_name2").hide();
        });
$(document).ready(function () {

    //$('#wait-div').show();
    $(".upl_lease").change(function () {
        var path = $(this).val();
        var filename = path.replace(/^.*\\/, "");
        //var html = filename + " <img class='clr_up_img del_lease_doc' src='<?php echo base_url() ?>assets/default/images/delete-icon.png'/>";

        var html = " <a class='clr_up_img del_lease_doc' href='javascript:;'> Delete</a><br><br>";
        var reader = new FileReader();
        reader.onload = function(){
            var dataURL = reader.result;
            html_view = '<a href="'+ dataURL +'" target="_blank" class="btn preview_lease"> View </a> ';                    
            $(".dash_upload_lease_files_cs").html(filename + ' ' + html_view + html);
        };
        reader.readAsDataURL(this.files[0]);
        //$(".dash_upload_lease_files_cs").html(html);
        $(".dash_upload_lease_files_cs").show();
    });

    $(document).on('click', '#sendeTesttText', function () {
        var posturl = siteUrl + 'property/send_test_etext_message';
        $.ajax({
                url: posturl,
                dataType: 'json',
                type: "POST",
                beforeSend: function () {
                    $('#wait-div').show();
                },
                success: function (response) {
                    $('#wait-div').hide();
                    if (response.success == true)
                    {
                        BootstrapDialog.show({
                            title: 'Success',
                            message: response.msg
                        });
                    }else{
                        BootstrapDialog.show({
                            title: 'Error!',
                            message: response.msg
                        });
                    }
                    
                },
                error: function (data) {
                    alert("Server Error.");
                    return false;
                }
            });
    });

    $(document).on('click', '#sendTesttMaintenanceEmail', function () {
        var posturl = siteUrl + 'property/send_test_tmaintenance_email';
        $.ajax({
                url: posturl,
                dataType: 'json',
                type: "POST",
                beforeSend: function () {
                    $('#wait-div').show();
                },
                success: function (response) {
                    $('#wait-div').hide();
                    if (response.success == true)
                    {
                        BootstrapDialog.show({
                            title: 'Success',
                            message: response.msg
                        });
                    }else{
                        BootstrapDialog.show({
                            title: 'Error!',
                            message: response.msg
                        });
                    }                    
                },
                error: function (data) {
                    alert("Server Error.");
                    return false;
                }
            });
    });
});
</script>
<section class="signup-section2 dashborad-page wb-dash-page">
    <div class="container">
        <!--div class="block-area blk_cnt">
            <a class="my-block" href="javascript:;">
                <h1><?php //echo $countAllProperties ?></h1>
                <p>Properties </p>
            </a>
            <a class="my-block" href="javascript:;">
                <h1><?php //echo $countOccupiedProperties ?></h1>
                <p>Occupied</p>
            </a>
            <a class="my-block" href="javascript:;">
                <h1><?php //echo $countVacantProperties ?></h1>
                <p>vacant</p>
            </a>
            <a class="my-block mr-rite0" href="javascript:;">
                <h1><?= $totalTenants ?></h1>
                <p>Tenants</p>
            </a>
        </div-->
                    <!-- <div class="ajax_report alert alert-success" role="alert" style="margin-bottom: 10px; margin-left: 0px; width: 500px; position: relative; top: 100px;">
                        <span class="close-message"></span>
                        <div class="ajax_message"><?php echo $this->session->userdata('stripe_payment'); ?></div>
                    </div><br> -->

            <?php 
                $userdata = $this->session->userdata(); 
                if(isset($userdata['stripe_payment'])){                   ?>
                    <script> 
                    $(document).ready(function(){
                        var current_tmaintenance = '<?php echo $this->session->userdata("current_tmaintenance"); ?>';
                        var current_tmessaging = '<?php echo $this->session->userdata("current_tmessaging"); ?>';
                        var current_tpay = '<?php echo $this->session->userdata("current_tpay"); ?>';
                        var payment_received = '<?php echo $this->session->userdata("payment_received"); ?>';

                        if(payment_received){
                            $('.payment_received').html(payment_received);
                        }

                        if (current_tmessaging != 'Yes'){
                            $('.is_tMessaging').html('');
                            $('.is_tMessaging').html('tText Unconfirmed');
                            $('.is_tMessaging').addClass('my-cross');
                        }else{
                            $('.is_tMessaging').html('');
                            $('.is_tMessaging').html('tText Confirmed');
                            $('.is_tMessaging').removeClass('my-cross');
                        }
                        if (current_tmaintenance != 'Yes'){
                            $('.is_tMaintence').html('');
                            $('.is_tMaintence').html('tMaintence Unconfirmed');
                            $('.is_tMaintence').addClass('my-cross');
                        }else{
                            $('.is_tMaintence').html('');
                            $('.is_tMaintence').html('tMaintence Confirmed');
                            $('.is_tMaintence').removeClass('my-cross');
                        }
                        if (current_tpay != 'Yes'){
                            $('.is_tPay').html('');
                            $('.is_tPay').html('is_tPay Unconfirmed');
                            $('.is_tPay').addClass('my-cross');
                        }else{
                            $('.is_tPay').html('');
                            $('.is_tPay').html('is_tPay Confirmed');
                            $('.is_tPay').removeClass('my-cross');
                        }
                        $(".popup-block").show();
                        $(".next-step-success-message").show();
                    });
                    </script>
                    <?php $this->session->unset_userdata('stripe_payment'); ?>
                    <?php //$this->session->unset_userdata('stripe_payment_prop_id'); // Remove after popup ?>
                    <?php $this->session->unset_userdata('current_tmessaging'); ?>
                    <?php $this->session->unset_userdata('current_tmaintenance'); ?>
                    <?php $this->session->unset_userdata('current_tpay'); ?>
            <?php } ?>
        
        <div id="properties-listing">
            <div class="signup">
                <div class="btn-part">
                    <a href="javascript:;" class="active left property-by-status" data-value="" style="width:108px;;">All </a>
                    <a href="javascript:;" class="unactive left property-by-status" data-value="vacant" style="width:107px;">Vacant </a>
                    <a href="javascript:;" class="unactive right property-by-status" data-value="occupied" style="width:107px;">Occupied</a>
                </div>
                <a class="btn-tenant add-property" href="<?= site_url('add-property') ?>"><i class="fa fa-plus"></i>Add Property</a>
            </div>
            <div class="table-content signup-table dashboard-table">
			  
                <table class="my-table ss">
                    <thead>
					  <tr class="top-bar">
                            <th> </th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th class="bord4"></th>
                            <th class="bord"></th>
                            <th class="bord1"> tServices</th> 
                            <th class="bord2">
                               
                            </th>
                        </tr>
                        <tr>
                            <th>Address </th>
                            <th>Unit# <a href="#" data-toggle="tooltip" tooltip="A single family property is 1 unit."><span class="glyphicon glyphicon-info-sign"></span></a></th>
                            <th>Rent Balance</th>
                            <th>Tenants</th>
                            <th>Messages</th>
                            <th>Vendors</th>
                            <th><a href="javascript:;" class="dash-pop shwtmsg open_cus_model">tText</a> <br><a href="javascript:;" style="text-align:center !important" id="sendeTesttText" class="">Test</a></th>
                            <th><a href="javascript:;" class="dash-pop shwtmntnce open_cus_model">tMaintenance</a><br><a href="javascript:;"  style="text-align:center !important"  id="sendTesttMaintenanceEmail" class="">Test</a></th>
                            <th>
                                <a  class="dash-pop shwtpay open_cus_model">tPay</a> 
                                <br>
                                <a href="<?= site_url('account_setup') ?>?dashboard=true"  style="text-align:center !importantl;"  id="" class="">Bank Acct.
                                </a>
                                    <?php if($tpay_account_status == 0){ ?>
                                            <span style=" border: none;padding-right: 18px;border-radius: 0px;height: 4px !important;width:30px" class="togle" data-type="" data-value="" data-property="No">
                                                <span class="check"></span>
                                            </span>
                                    <?php }else{ ?>
                                            <span style="  border: none;padding-right: 18px;border-radius: 0px;height: 4px !important;width:75px"  class="togle stop" data-type="" data-value="" data-property="Yes">
                                                <span class="check"></span>
                                            </span>
                                    <?php }  ?>
                                 <!-- <a href="#" data-toggle="tooltip" tooltip=" Add status light next to Bank Account under tPay. Light will match status light from tServices page."><span class="glyphicon glyphicon-info-sign"></span></a> -->
                            </th>
                        </tr>
                    </thead>
                    <tbody>
 
                        <?php foreach ($allProperties as $value) {   //print_R($allProperties); ?>
                            <tr>
                                <td> 
                                    <?php if($value->step_completed < 3){ ?>
                                            <a  class="address_info_left" href="<?= site_url('add-property') ?>">  <?= $value->city ?>, <?= $value->address1 ?> <?= $value->address2 ?> </a> 
                                            <a href="javascript:;" class="address_info_right red_indicator"  data-toggle="tooltip" tooltip="Property Setup Incomplete."><span class="glyphicon glyphicon-info-sign"></span></a> 
                                    <?php }else{ ?>
                                            <a  class="address_info_left" href="<?= site_url('my-properties') ?>#tabs-<?php echo $value->prop_id; ?>">  <?= $value->city ?>, <?= $value->address1 ?> <?= $value->address2 ?> </a> 
                                    <?php }  ?>
                                </td>
                                <?php if($value->step_completed < 3){ ?>
                                    <td> <a href="<?= site_url('add-property') ?>"><?= $value->unit_number ?> </a> </td>
                                <?php }else{ ?>
                                    <td> <a href="<?= site_url('my-properties') ?>#tabs-<?php echo $value->prop_id; ?>"><?= $value->unit_number ?> </a> </td>
                                <?php }  ?>
                                <td class="das_rent_ampount_td">  <?php echo $value->rent_amount; ?> </td> 
                                <td>
                                    <?php if($value->step_completed < 3){ ?>
                                            <?php if ($value->tenants == 0) { ?>
                                                <a href="<?= site_url('add-property') ?>" id="<?php echo $value->prop_id; ?>" class="<?php if ($value->tenants == 0) { ?>zero<?php } ?>"> <?= $value->tenants ?> </a> 
                                            <?php } else { ?>
                                                <a href="<?= site_url('add-property') ?>" class="<?php if ($value->expiry == 'Yes') { ?>expiring<?php } ?>"> <?= $value->tenants ?> </a> 
                                            <?php } ?>
                                    <?php }else{ ?>
                                             <?php if ($value->tenants == 0) { ?>
                                                <a href="javascript:;" id="<?php echo $value->prop_id; ?>" data-toggle="modal" data-target="#addtenantModal" class="<?php if ($value->tenants == 0) { ?>zero<?php } ?>"> <?= $value->tenants ?> </a> 
                                            <?php } else { ?>
                                                <a href="<?= site_url('tenants') ?>#tabs-<?php echo $value->prop_id; ?>" class="<?php if ($value->expiry == 'Yes') { ?>expiring<?php } ?>"> <?= $value->tenants ?> </a> 
                                            <?php } ?>
                                    <?php }  ?>
                                </td> 
                                <?php if($value->step_completed < 3){ ?>
                                        <td> <a href="<?= site_url('add-property') ?>"> <?php echo $value->messages ?> </a> </td>
                                        <td> <a href="<?= site_url('add-property') ?>"> <?= $value->vendors ?> </a> </td>
                                <?php }else{ ?>
                                        <td> <a href="<?= site_url('message') ?>#tabs-<?php echo $value->prop_id; ?>"> <?php echo $value->messages ?> </a> </td>
                                        <td> <a href="<?= site_url('vendors') ?>#tabs-<?php echo $value->prop_id; ?>"> <?= $value->vendors ?> </a> </td>
                                <?php }  ?>   
                                <?php if($value->subscription_status == FALSE || $value->step_completed < 4){ ?>
                                            <?php
                                            if ($value->step_completed < 3) {
                                                $p_clss = 'tServicesPending';
                                            } else {
                                                $p_clss = 'tServices';
                                            }
                                            ?> 
                                            <td> 
                                                <div class="togle <?php echo $p_clss; ?>" id="tmessaging-<?= $value->prop_id ?>" data-type="tmessaging" data-value="<?= $value->prop_id ?>" data-property="not_subscribe">
                                                    <span class="check"></span>
                                                </div>
                                            </td>
                                            <td> 
                                                <div class="togle <?php echo $p_clss; ?>" id="tmaintenance-<?= $value->prop_id ?>" data-type="tmaintenance" data-value="<?= $value->prop_id ?>" data-property="not_subscribe">
                                                    <span class="check"></span>
                                                </div>
                                            </td>
                                            <td> 
                                                <div class="togle <?php echo $p_clss; ?>" id="tpay-<?= $value->prop_id ?>" data-type="tpay" data-value="<?= $value->prop_id ?>" data-property="not_subscribe">
                                                    <span class="check"></span>
                                                </div>
                                            </td>

                                <?php }else{ ?>
                                            <td> 
                                                <?php
                                                $verification_status = $this->common_model->getSingleFieldFromAnyTable('verification_status', 'mem_id', $this->session->userdata('MEM_ID'), 'tbl_members');
                                                if ($verification_status == 'No') {
                                                    $clss = 'intservices';
                                                } else {
                                                    $clss = 'tServices';
                                                }
                                                ?>
                                                <div class="togle <?php if ($value->tmessaging == 'Yes') { ?>stop<?php } ?> <?php echo $clss; ?>" id="tmessaging-<?= $value->prop_id ?>" data-type="tmessaging" data-value="<?= $value->prop_id ?>" data-property="<?php if ($value->tmessaging == 'Yes') { ?>No<?php } else { ?>Yes<?php } ?>">
                                                    <span class="check"></span>
                                                </div>
                                            </td>
                                            <td> 
                                                <div class="togle <?php if ($value->tmaintenance == 'Yes') { ?>stop<?php } ?> <?php echo $clss; ?>" id="tmaintenance-<?= $value->prop_id ?>" data-type="tmaintenance" data-value="<?= $value->prop_id ?>" data-property="<?php if ($value->tmaintenance == 'Yes') { ?>No<?php } else { ?>Yes<?php } ?>">
                                                    <span class="check"></span>
                                                </div>
                                            </td>
                                            <td> 
                                                <div class="togle <?php if ($value->tpay == 'Yes') { ?>stop<?php } ?> <?php echo $clss; ?>" id="tpay-<?= $value->prop_id ?>" data-type="tpay" data-value="<?= $value->prop_id ?>" data-property="<?php if ($value->tpay == 'Yes') { ?>No<?php } else { ?>Yes<?php } ?>">
                                                    <span class="check"></span>
                                                </div>
                                            </td>
                                        
                                <?php } ?>
                            </tr>
<?php } ?>
                    </tbody>
                </table>
<?php if (!$allProperties) { ?>
                    <center></br></br></br></br></br></br>
                        <span class="no-record"> No Properties found...</span>
                    </center>
<?php } ?>
            </div>
        </div>	  
    </div>
</section>
<div class="modal fade " id="addtenantSuccessModal">
    <div class="modal-dialog" style="width: 55%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <img src='<?php echo base_url() ?>assets/default/images/yes_icon.jpg'>
                <h4 class="modal-title">Success</h4>
            </div>
            <div class="modal-body">
               <div class="tenant_success"> Please check-in your tenant.</div>
            </div>
            <div class="modal-footer" style="border:none !important">
                <a href="javascript:;" id="" data-prop='' class="red-col btn btn-primary chk_in_popup_btn">Check In</a>
            </div>
        </div>
    </div>
</div>

<div class="modal fade " id="addtenantModal">
    <div class="modal-dialog" style="width: 55%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Add New Tenant</h4>
            </div>
            <div class="modal-body">
                <?php echo form_open('', array('class' => 'tenant-detai', 'id' => 'add_tenant_form')) ?>
                <ul class="my-list tttt2222 ttttextra border0">
                    <li>
                        <span> <label>First Name</label><input placeholder="" type="text" class="form-control tenant-txtfield" value="" name="first_name"  /></span>
                        <span><label>Middle Name</label><input placeholder="" type="text" class="form-control tenant-txtfield" value="" name="middle_name"/><label class="label2"></label></span>
                        <span><label>Last Name</label><input placeholder="" type="text" class="form-control tenant-txtfield" value="" name="last_name"/><label class="label2"></label></span>
                        
						<span class="dob span-up">
						<span><label>DOB</label><input placeholder="" type='text' class="form-control tenant-txtfield" id='datetimepicker4' readonly="true" name="dob"/></span>
                        <span> <label>Mobile</label><input  placeholder="" type="text" class="form-control tenant-txtfield phone_us" value="" name="mobile_no"/><label class="label2">  </label></span>
						</span>
					<span><label>Email</label><input type="text"  placeholder="" class="form-control tenant-txtfield" value="" name="email"/><label class="label2"></label></span>
					<span class="sel-lang-start span-up">
					 <span><label>Lamnguage</label>
                            <select class="form-control my-txt-fleid" name="preferred_language" >
                                <option value="">Select Language</option>
                                <option value="English">English</option>
                                <option value="Spanish">Spanish</option>
                            </select>
                        </span>
						 <span>  <label>Lease Start</label><input  placeholder="" type="text" readonly id="lease_from" class="form-control tenant-txtfield" value="" name="lease_start_date"/></span>
						
					</span>
					
					<span class="rentt span-up">
					<span>  <label>Lease End</label><input  placeholder="" readonly type="text" id="lease_to" class="form-control tenant-txtfield" value="" name="lease_end_date"/></span>
                        <span class="currency_text_box curr"> <label>Rent Amount</label><span class="dolar-sign"><i class="fa fa-usd" style="font-size:14px"></i></span><input  placeholder="" type="text" class="form-control tenant-txtfield" value="" name="rent_amount" /></span>
					</span>
					<span class="rent-due span-up">
					  <span><label>Rent Due</label>
                            <select class="form-control my-txt-fleid due_date_start_class" required="true"  name="due_date">
                                <option value="">Rent Due</option>
                                    <?php for ($j = 1; $j < 31; $j++) { ?>
                                    <option value="<?php echo $j; ?>"><?php echo ordinal($j); ?></option>
                                    <?php } ?>
                            </select>
                        </span>
						<span><label>Late Fee type</label>
                            <select class="form-control my-txt-fleid late_fee_type_cls" required="true"   name="late_fee_type">
                                <option value="">Late Fee type</option>
                                <option value="Daily charge">Daily charge</option>
                                <option value="One time">One time</option>
                            </select>
                        </span>
						
					</span>
					
					<span class="late-fee span-up">
					<span class="late_fee_sh1 currency_text_box curr"><label>Late Fee</label><span class="dolar-sign"><i class="fa fa-usd" style="font-size:14px"></i></span><input  placeholder="" type="text" class="form-control tenant-txtfield" value="" name="late_fee"/></span>
                        <span class="late_fee_start"><label>Late fee start</label>
                         <select class="form-control my-txt-fleid late_fee_start_class" required="true"  name="late_fee_start">
                                <option value="">Late fee start</option>
                                    <?php for ($j = 1; $j < 31; $j++) { ?>
                                    <option value="<?php echo $j; ?>"><?php echo ordinal($j); ?></option>
                                    <?php } ?>
                            </select>
					</span>
					
                    </li>

                   <div class="sub-panel-head">Pet Info</div>
                    <li class="pet-info mrg">
                      <!--  <span >
                        </span>-->
                        <input type="hidden" id="pets1" value="Yes"  name="pets[]" >
                        <span><label>Pet type</label>
                            <select class="form-control" id="pets_type"  name="pets_type[]" >
                                <option value="">None</option>
                                <option value="dog">Dog</option>
                                <option value="cat">Cat</option>
                                <option value="other">Other</option>
                            </select>
                        </span>
                        <span><label>Pet Name</label>
                            <input type="text" class="form-control tenant-txtfield" placeholder="" id="pets_name"  name="pets_name[]" >                           
                        </span>
                        <span class="currency_text_box curr"><label>Pet Fee</label>
                          <span class="dolar-sign"><i class="fa fa-usd" style="font-size:14px"></i></span>  <input type="text" class="form-control tenant-txtfield"  number='true' maxlength="6" digits="true"  placeholder="" id="pets_fee"  name="pets_fee[]" >                           
                        </span>
                        <span class="pet-bred"><label>Pet Breed/Description</label>
                            <input type="text" class="form-control tenant-txtfield" placeholder="" id="pets_breed"  name="pets_breed[]" >                           
                        </span>
                        <span class="">
						
                            <input type="hidden" value="1" name="pets_no" class="pets_no">
                            <a class="add-pet-btn" href="javascript:;"><i class="fa fa-plus"></i>Add Pet</a>
                        </span>
                    </li>
                    <div class="sub-panel-head">Emergency Contact</div>
                    <li class="em-cont">
                        <span class=""><label>Relationship</label>
                            <select class="form-control" id="relationship"  name="relationship" >
                                <option value="">Select Relationship</option>
                                <option value="Wife">Wife</option>
                                <option value="Husband">Husband</option>
                                <option value="Mother">Mother</option>
                                <option value="Father">Father</option>
                                <option value="Friend">Friend</option>
                                <option value="Other">Other</option>
                            </select>
                        </span>
						 <span class="em-name"><label>Name</label>
                            <input type="text"  maxlength="20" class="form-control my-txt-fleid valid emergency_name" id="emergency_name" placeholder="" name="emergency_name" value=""/>
                        </span>
                        <span class="span em-ph"><label>Phone</label>
                            <input type="text"  class="form-control my-txt-fleid valid phone_us emergency_phone" id="emergency_phone" placeholder="" name="emergency_phone" value=""/>
                        </span>
						<span>
                        <label>Email</label>

                        <input type="email" class="form-control my-txt-fleid valid emergency_email" id="emergency_email" placeholder="" name="emergency_email" value=""/></span>
						
						
						
						
                    </li>                            
                   </ul>
				 <ul class="form-group up-leaseee">
                  
				   
                    <li  class="full_li custom-full-li" >
    					<div class="my-col width50 "> 
                            <span class=" ">
                                <span class="upload-btn">Upload Lease<input type="file" class="upl_lease" name="upload_lease"></span>
                            </span>
                            <div class="require_lease_doc"><em>*</em>You can add your lease now or later. However, your tenant will not be able to set up mobile rent payment without a lease on file.</div>
                        </div>
                       
                    </li>
                    <p class="btn_view_del ls_file_name dash_upload_lease_files_cs"></p>
                </ul>
            </div>

                <input type="hidden" name="property_id" value="" id="property_id">
                <input type="hidden" name="homepage" id="homepage" value="yes">
                <!--                <h2 class="lease-doc">Tenant Document</h2>
                                <ul class="upload-file">
                                    <li class="browse"><input type="file" name="lease"></li>
                                </ul>-->

                <div class="modal-footer" style="border:none !important">
                    <div class="" style="float:left">
                        <button type="submit" class="btn btn-primary">Submit</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
           
<?php echo form_close(); ?>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


    <div class="my-popup save-later incomplete_pop tmsgs" style="display:none">
        <div class="popup-cont">
            <div class="popup-head">
                <span class="close close_custom_popup"></span>
            </div>
            <div class="popup-body">
               <ul>	
                    <li class="message-img"><h4><span>t</span>Text<sup></sup></h4>
                        <?php echo getFeatureBySlug('ttext'); ?>
                        <!--<p>Improves communications with tenants by using as an automated texting system to deliver customized messages based on property, tenant, and lease information.</p>-->
                    </li>                
                </ul>              
            </div>
            <div class="popup-footer">
                <div class="center">
                </div>
            </div>
        </div>
    </div>
    <div class="my-popup save-later incomplete_pop tmntnce" style="display:none">
        <div class="popup-cont">
            <div class="popup-head">
                <span class="close close_custom_popup"></span>
            </div>
            <div class="popup-body">

               <ul>	              

                <li class="mainstaince-img">
                    <h4><span>t</span>Maintenance<sup></sup></h4>
                    <?php echo getFeatureBySlug('tmaintenance'); ?>
                    <!--<p>Simplifies the submission, delivery, and approval of maintenance requests through an innovative, mobile solution.</p>-->
                </li>
            </ul>
              
            </div>
            <div class="popup-footer">
                <div class="center">
                </div>
            </div>
        </div>
    </div>
    <div class="my-popup save-later incomplete_pop tpay" style="display:none">
        <div class="popup-cont">
            <div class="popup-head">
                <span class="close close_custom_popup"></span>
            </div>
            <div class="popup-body">

               <ul> 
              

                <li class="mainstaince-img">
                    <h4><span>t</span>Pay<sup></sup></h4>
                    <?php echo getFeatureBySlug('tpay'); ?>
                    <!--<p>Make rent collection more efficent by allowing tenants to pay their rent on their mobile device.</p>-->
                </li>
            </ul>
              
            </div>
            <div class="popup-footer">
                <div class="center">
                </div>
            </div>
        </div>
    </div>

    
<script>
    $(document).ready(function () {
        $('#datetimepicker4').datepicker({maxDate: 0, defaultDate: "01/01/1985", changeMonth: true,
            changeYear: true,yearRange: "-80:+0" });
        $("#lease_from").datepicker({
            defaultDate: "+1w",
         //   minDate: 0,
            changeMonth: true,
            changeYear: true,
            onClose: function (selectedDate) {
                $("#lease_to").datepicker("option", "minDate", selectedDate);
            }
        });
        $("#lease_to").datepicker({
            defaultDate: "+1w",
            changeMonth: true,
            changeYear: true,
            minDate: 0,
            onClose: function (selectedDate) {
                $("#lease_from").datepicker("option", "maxDate", selectedDate);
            }
        });
        $('#duedate').datepicker();
    });
    $(document).on("click", ".zero", function () {
        $("#property_id").val($(this).attr('id'));
    });
    $(document).on("click", ".property-by-status", function () {
        var type = $(this).attr('data-value');
        if (type == '' || type == null)
            type = "NULL";
        var postUrl = siteUrl + 'dashboard/' + type;
        $.ajax({
            url: postUrl,
            dataType: 'json',
            beforeSend: function () {
                $('#wait-div').show();
            },
            success: function (response) {
                if (response.success)
                {
                    $('#properties-listing').html('');
                    $('#properties-listing').html(response.html);
                }
                 $('#wait-div').hide();
            },
            error: function () {
                 $('#wait-div').hide();
                alert('server error');
            },
        });
    });


    $(document).on("click", ".intservices", function () {
        swal("Error!", 'Please confirm your email address. tServices will not be active until confirmation is complete.', "error");
    });

    $(document).on("click", ".tServicesPending", function () {
        window.location = siteUrl + 'add-property';
    });
    

    $(document).on("click", ".tServices", function () {
        var property_id = $(this).attr('data-value');
        var value = $(this).attr('data-property');
        var type = $(this).attr('data-type');
        var postUrl = siteUrl + 'change-tservices-status/' + property_id + '/' + type + '/' + value;
        var message = '';
        if (type == 'tmessaging' && value == "No")
            message = 'Turning off tText will not allow you to send automated text messages to tenants. Do you wish to continue?';
        if (type == 'tmessaging' && value == "Yes")
            message = 'Turn on tText?';
        if (type == 'tmaintenance' && value == "No")
            message = 'Turning off tMaintenance will not allow the Tenants to request maintenance on their mobile devices. Do you wish  to continue?';
        if (type == 'tmaintenance' && value == "Yes")
            message = 'Turn on tMaintenance?';
        if (type == 'tpay' && value == "No")
            message = 'Turning off tPay will not allow  Tenants to pay their rent on their mobile devices. Do you wish  to continue?';
        if (type == 'tpay' && value == "Yes")
            message = 'Turn on tPay?';
        if(value == 'not_subscribe')
            message = 'Please subscribe the property to start the tservices?';
        BootstrapDialog.confirm({
            title: 'Confirm',
            message: message,
            callback: function (result) {
                if (result)
                {
                    $.ajax({
                        url: postUrl,
                        dataType: 'json',
                        success: function (response) {
                            if (response.success)
                            {
                                if (response.tmaintenance_message == 'vendor_not_exist')
                                {
                                    BootstrapDialog.confirm({
                                        title: 'Confirm',
                                        message: 'Please add a vendor to turn on the tmaintenance.',
                                        callback: function (resultis) {
                                            if (resultis)
                                            {
                                                window.location = response.redirect_to;
                                            }else{
                                                return false;
                                            }
                                        }
                                    });
                                }
                                else
                                {
                                    if(response.redirect_to != ''){
                                        window.location = response.redirect_to;
                                    }else{
                                        if (value == 'Yes')
                                        {
                                            $('#' + type + '-' + property_id).addClass('stop');
                                            $('#' + type + '-' + property_id).attr('data-property', 'No');
                                        }
                                        else
                                        {
                                            $('#' + type + '-' + property_id).removeClass('stop');
                                            $('#' + type + '-' + property_id).attr('data-property', 'Yes');
                                        }
                                    }
                                }
                            }
                        },
                        error: function () {
                            alert('server error');
                        },
                    });
                }
                else
                {
                    return false;
                }
            }
        });


    });



</script>	

<script type="text/javascript">
    $(document).ready(function () {
        new Fingerprint2().get(function(result){
        // this will use all available fingerprinting sources
        $.ajax({
                url: '<?php echo base_url('property/update_fingerprint'); ?>',
                type: 'POST',
                data: {fingerprint: result},
                beforeSend: function () {
                    $('#wait-div').hide();
                },
                success: function (data) {
                    //console.log(data);
                    $("#wait-div").hide();                     
                },
                error: function () {
                    $("#wait-div").hide();                   
                }
            });
        });
    });
</script>
