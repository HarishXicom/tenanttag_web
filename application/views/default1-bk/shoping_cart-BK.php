
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script>
    $(document).ready(function(){
        $("#link-btn").click(function(){
            $("#linked-acc").slideDown("slow");
        });
    });
</script>	
<div class="level">
    <div class="container">
        <div class="level-indicator">
            <img src="<?= $this->config->item('templateassets') ?>images/full-level.png" alt="" />
            <ul>
                <li> Property Info</li>
                <li>Tenants</li>
                <li  class="blue-col1">tServices</li>
            </ul>
        </div>
    </div>
</div>
<section class="signup-section2">
    <div class="container">
        <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
        <script type="text/javascript">  
          Stripe.setPublishableKey('pk_test_mo7Ze4S2ZLdYooQx7vl1fC9a');    
          function onSubmitDo () {      
            Stripe.card.createToken( document.getElementById('payment-form'), myStripeResponseHandler );          
            return false;      
          };

          function myStripeResponseHandler ( status, response ) {      
            console.log( status );
            console.log( response );    
            if ( response.error ) {
              document.getElementById('payment-error').innerHTML = response.error.message;
            } else {
              var tokenInput = document.createElement("input");
              tokenInput.type = "hidden";
              tokenInput.name = "stripeToken";
              tokenInput.value = response.id;
              var paymentForm = document.getElementById('payment-form');
              paymentForm.appendChild(tokenInput);
              paymentForm.submit();
            }      
         };      
        </script>
        <form action="<?php echo base_url('property/charge_by_stripe'); ?>" method="POST" id="payment-form" onsubmit="return onSubmitDo()">   
            <div class="ajax_report alert display-hide" role="alert" style="margin-bottom: 10px; margin-left: 0px; width: 500px; position: relative; top: 100px;">
              <span class="close-message"></span>
              <div class="ajax_message">Hello Message</div>
            </div>
            <div class="tpay-acc-setup">
                 <div class="shopping-cart-sec">
                  <h2>Shopping Cart</h2>
                  <div class="shopping-cart-sec-in">
                      <h4>Summary of Services</h4>
                      <table border="1" cellpadding="0" cellspacing="0">
                          <thead>
                              <tr>
                                  <th>Services</th>
                                  <th>Properties</th>
                                  <th>Status</th>
                                  <th>Price</th>
                                  <th>Total</th>
                              </tr>
                          </thead>
                          <tbody>
                              <tr>
                                  <td>tMaintenance / tText</td>
                                  <td>1-5</td>
                                  <td>
                                  <?php //print_r($property_info); exit; ?>
                                    <div class="togle tShhopingCarttServices <?php if ($property_info->tmessaging != 'Yes') { ?>stop<?php } ?>" id="tmessaging" data-type="tmessaging" data-value="<?= $property_info->prop_id ?>" data-property="<?php if ($property_info->tmessaging == 'Yes') { ?>No<?php } else { ?>Yes<?php } ?>">
                                        <span class="check"></span>
                                    </div>
                                  </td>
                                  <td>$5/mth</td>
                                  <td>$5</td>
                              </tr>
                              <tr>
                                  <td>tRent</td>
                                  <td>1</td>
                                  <td>
                                    <div class="togle  tShhopingCarttServices <?php if ($property_info->trent != 'Yes') { ?>stop<?php } ?>" id="trent" data-type="trent" data-value="<?= $property_info->prop_id ?>" data-property="<?php if ($property_info->trent == 'Yes') { ?>No<?php } else { ?>Yes<?php } ?>">
                                        <span class="check"></span>
                                    </div>
                                  </td>
                                  <td>$5.95/mth/prop</td>
                                  <td>$5.95</td>
                              </tr>
                              <tr>
                                  <td>Total</td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td>$10.95</td>
                              </tr>
                          </tbody>
                      </table>
                  </div>  
                  <span style='color: red' class="error" id='payment-error'></span>                      
                  <div class="cart-form">                      
                          <div class="cart-form-divide cart-form-section">
                              <input type="hidden" name="amount"/>
                              <input type="text" data-stripe="name" name="first_name" placeholder="First Name" />
                              <input type="text" name="last_name" placeholder="Last Name" />
                          </div>
                          
                          <div class="clearfix"></div>
                          <div class="cart-form-section cart-form-sectionfour">
                              <input type="text" name="" style="width:49%;" data-stripe="number" placeholder="CC"  />
                              <input type="text" readonly name="month_year" id="month_year"  style=""  placeholder="MM/YY "  />
                              <input type="hidden"  id="exp_month" name="exp_month" style="" data-stripe="exp_month"   />
                              <input type="hidden"  id="exp_year" name="exp_year" style="" data-stripe="exp_year"  />
                              <input type="text" class="reset"  data-stripe="cvc" placeholder="Security Code - CVC">
                             
                             
                          </div>
                          <div class="clearfix"></div>
                          <div class="cart-form-section cart-form-sectionfour cart-form-sectionfour2">
                              <input type="text" name="" data-stripe="billingAddress" placeholder="Billing Address" />
                               <input type="text" name="BillingAddressCity" style="width:21%;" data-stripe="BillingAddressCity" placeholder="City" />
                               <input type="text" name="BillingAddressState" style="width:23.5%;" data-stripe="BillingAddressState" placeholder="State" />
                              <input type="text" class="reset" data-stripe="address_zip" style="width:23.5%;" placeholder="Zip" />
                          </div>
                          <div class="clearfix"></div>
                          
                          <div class="cart-btns">
                              <a href="javascript;;" id="shoping-cart-cancel-btn" class="shoping-cart-cancel-btn">Cancel</a>
                              <input type="hidden" name="emailAddress">
                              <input type="submit" name="" value="Confirm" />
                          </div>
                  </div>    
              </div>      
            </div>          
        </form>
        <script type="text/javascript">
        $(function() {
          $('#month_year').datepicker( {
              changeMonth: true,
              changeYear: true,
              showButtonPanel: true,
              dateFormat: 'mm/yy',
              yearRange: '2016:2100',
              onClose: function(dateText, inst) { 
                  $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
                  $('#exp_month').val(inst.selectedMonth);
                  $('#exp_year').val(inst.selectedYear);
              }
          });
      });
        </script>
        <style>
          .ui-datepicker-calendar {display: none;}
          .ui-datepicker-header{ width: 200px;}
          .ui-datepicker-close.ui-state-default.ui-priority-primary.ui-corner-all {float: right;}
          .ui-state-default {    margin: 7px 0 0;    padding: 1px 6px;}
        </style>
    </div>
    <script type="text/javascript">
        $(document).on("click", ".tShhopingCarttServices", function () {
            var property_id = $(this).attr('data-value');
            var value = $(this).attr('data-property');
            var type = $(this).attr('data-type');
            var postUrl = siteUrl + 'property/changeShopingCartTservicesStatus/' + property_id + '/' + type + '/' + value;
            var message = '';
            if (type == 'tmessaging' && value == "No")
                message = 'Turing off tMaintenance / tText will not allow you to send message to tenants. Do you wish to continue?';
            if (type == 'tmessaging' && value == "Yes")
                message = 'Are you sure to Turning On tMaintenance / tText?';
            if (type == 'trent' && value == "No")
                message = 'Turning off tRent will not allow the Tenants to contact Vendors. Do you wish  to continue?';
            if (type == 'trent' && value == "Yes")
                message = 'Are you sure to Turning On tRent?';
            BootstrapDialog.confirm(message, function (result) {
                if (result)
                {
                    $.ajax({
                        url: postUrl,
                        dataType: 'json',
                        success: function (response) {
                            if (response.success)
                            {
                                if (value == 'Yes')
                                {
                                    $('#' + type).removeClass('stop');
                                    $('#' + type).attr('data-property', 'No');
                                }
                                else
                                {
                                    $('#' + type).addClass('stop');
                                    $('#' + type).attr('data-property', 'Yes');
                                }
                            }
                        },
                        error: function () {
                            alert('server error');
                        },
                    });
                }
                else
                {
                    return false;
                }
            });

        });
    </script>
</section>