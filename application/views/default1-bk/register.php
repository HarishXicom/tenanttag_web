<section class="dr-banner">
	<div class="container">
		<div class="right-banner" style="width: 654px !important;margin-right:330px;">
			<h4>Sign Up</h4>
			<?php echo form_open('register',array('class'=>'ajaxForm','id'=>'sign-up'))?>
			<div class="ajax_report alert display-hide" role="alert" style="margin-left:0px;margin-bottom:10px;width:500px;"><span class="close-message"></span><div class="ajax_message">Hello Message</div></div>
			<div class="form-group1">
				<div class="my-div"><input type="text" placeholder="First Name" class="form-control my-txt-fleid " name="first_name" style="width:500px;"></div>
			</div>
			<div class="form-group1">
				<div class="my-div"><input type="text" placeholder="Last Name" class="form-control my-txt-fleid " name="last_name"  style="width:500px;"></div>
			</div>
			<div class="form-group1">
				<div class="my-div"><input type="text" placeholder="Company" class="form-control my-txt-fleid" name="company_name" style="width:500px;"></div>
			</div>
			<div class="form-group1">
				<div class="my-div"><p class="check">    <input type="checkbox" id="test1" name="as_company">    <label for="test1">Use company name</label>  </p></div>
			</div>
			<div class="form-group1">
				<div class="my-div"><input type="text" placeholder="Email address" class="form-control my-txt-fleid" name="email"  style="width:500px;"></div>
			</div>
			<div class="form-group1">
				<div class="my-div"><input type="password" placeholder="Password" class="form-control my-txt-fleid" name="password"  style="width:500px;"></div>
			</div>
			<div class="form-group1">
				<div class="my-div"><input type="password" placeholder="Confirm Password" class="form-control my-txt-fleid " name="confirm_password"  style="width:500px;"></div>
			</div>
			<div  class="form-group1">
				<div class="my-div">
					<select class="form-control my-txt-fleid" name="country" id="country" onChange="getStateByCountryId(this.value)"  style="width:500px;">
						<option value="">Select Country</option>
						<?php foreach($allcountry as $value) { ?>
							<option value="<?=$value->id?>"><?=$value->country_name?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div id="regions" style="display:none">
			<div  class="form-group1">
				<div class="my-div">
					<select class="form-control my-txt-fleid" name="state" id="state" onChange="getCityByStateId(this.value)"  style="width:500px;">
						<option value="">Select State</option>
					</select>
				</div>
			</div>
			</div>
			
			<div class="form-group1">
				<div class="my-div"><input type="text" placeholder="code#" class="form-control my-txt-fleid country_code" name="country_code" style="width:100px"><input type="text" placeholder="Mobile#" class="form-control my-txt-fleid right-pull mr-rite0" name="mobile" style="width:373px;"></div>
			</div>
			<div class="form-group1">
                            <div class="my-div"><p class="check">    <input type="checkbox" id="test2" name="terms">    <label for="test2">Agree to our <a href="#"><span class="changecolor">Terms & Conditions</span></a></label>  </p></div>
			</div>
			
			<input type="submit" class="formsign-bttn" value="SIGN UP" style="width:74%"/> 
			<?=form_close();?>
		</div>
	</div>
</section>
<section class="dr-content">
	<section class="howit-works">
		<div class="container">
			<div class="drwork-text">
				<h5>How it works</h5>
				<p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio.</p>
				<a href="#">Learn More</a>
			</div>
			<div class="dr-rightvideo">
				<a href="#"><img src="<?=$this->config->item('templateassets')?>images/video-img.png" alt=""/></a>
			</div>
		</div>
	</section>
	<section class="dr-testimonial">
		<div class="container test-part">
			<h2>Why Use TenantTag?</h2>
			<div class="testimonial">
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit<br>
in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
				<span class="client-name">- Jeffrey Zeldman</span>
			</div>
			<div class="testimonial1">
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim<br> veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit <br>esse cillum dolore eu, fugiat nulla pariatur.</p>
				<span class="client-name">- Paul Boag</span>
			</div>
		</div>
	</section>
</section>
<style>
	.mr-rite0 {
    margin-right: -180px !important;
}
</style>
