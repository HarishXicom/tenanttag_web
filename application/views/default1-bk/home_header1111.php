<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>TenantTag</title>
        <link rel="shortcut icon" href="<?php echo base_url() ?>/favicon.ico" type="image/x-icon">
        <link rel="icon" href="<?php echo base_url() ?>/favicon.ico" type="image/x-icon">
        <link href="<?= $this->config->item('templateassets') ?>css/font-awesome.css" rel="stylesheet">
        <link href="<?= $this->config->item('templateassets') ?>css/style.css" rel="stylesheet">
        <link href="<?= $this->config->item('templateassets') ?>css/responsive.css" rel="stylesheet">

        <script type="text/javascript" src="<?= $this->config->item('templateassets') ?>js/jquery-1.11.0.js"></script>
        <script type="text/javascript" src="<?= $this->config->item('templateassets') ?>js/jquery.form.js"></script>
        <script type="text/javascript" src="<?= $this->config->item('templateassets') ?>js/formClass.js"></script>
        <script type="text/javascript" src="<?= $this->config->item('templateassets') ?>js/jquery.validate.min.js"></script>
        <script type="text/javascript" src="<?= $this->config->item('templateassets') ?>js/jquery.mask.js"></script>         
    </head>

    <body>
        <header>
            <div class="container">
                <div class="dr-logo"><a href="<?= site_url() ?>"><img src="<?= $this->config->item('uploads') ?>site_logo/<?= $this->config->item('site_logo') ?>" alt="logo"/></a></div>	
                <?php if (!$this->session->userdata('MEM_ID')) { ?>
                    <?php echo form_open('login', array('class' => 'ajaxForm', 'id' => 'login')) ?>
                    <div class="ajax_report alert display-hide" role="alert" style="margin-left:788px;width:320px;margin-bottom:10px;"><span class="close-message"></span><div class="ajax_message">Hello Message</div></div>
                    <div class="right-header">
                        <div class="righthead-col">
                            <label>Email</label>
                            <input type="email" placeholder="Email" name="email" class="head-field" required/>
                        </div>
                        <div class="righthead-col right-fld">
                            <label>Password</label>
                            <input type="password" placeholder="Password" name="password" class="head-field" required/>
                        </div>
                        <input type="submit" value="LOGIN" class="loginbttn">
                        <input type="checkbox" name="keeplogin" style="position: relative; left: 0px; top: 3px; right: 0px;">
                        <span style="font-size: 12px; margin-left: 1px;">Remember me</span>
                        <a href="<?= site_url('forget_password') ?>">
                            <span class="changecolor front-forgat">Forgot password?</span>
                        </a>
                    </div>
                    <?= form_close(); ?>
                <?php } ?>
            </div>
        </header>
        <div id="wait-div"></div>
        <script>
            var siteUrl = '<?= site_url() ?>';
        </script>	
