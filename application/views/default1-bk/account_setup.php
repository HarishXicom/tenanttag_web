<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<iframe id="synapse_iframe" style="top:0; height:0%;width:0%;position:fixed;z-index:100000;visibility:hidden;" frameborder="0"></iframe>
<script src="https://cdn.synapsepay.com/static_assets/v3/iframes/universal_iframe.js"></script>

<?php 
//if(isset($_REQUEST['linked_bank_account'])){ ?>

  <script>
    $(document).ready(function(){
      $("#bank_account").change(function(){
          var selectedValue = $(this).val();
          if(selectedValue != ''){
              $("#remove_sel_acc").css("display","inline-block");
          }else{
              $("#remove_sel_acc").css("display","none");            
          }
      });
      $("#remove_sel_acc").click(function(){
          var selectedAccount = $('#bank_account').val();
          $.ajax({
              url: '<?php echo base_url('property/delete_linked_account_number'); ?>',
              type: 'POST',
              data: {accountnumber: selectedAccount},
              beforeSend: function () {
                  $('#wait-div').show();
              },
              success: function (data) {
                  $("#wait-div").hide();
                  $("#bank_account option[value='"+selectedAccount+"']").remove();
                  $("#remove_sel_acc").css("display","none");
              },
              error: function () {
                  $("#wait-div").hide();
                  swal("Error!", "Server not responding.", "error");
              }
          });;
      });
        $("#linked_bank_account").click(function(){
          var iframeInfo = {
            physical_id:{
                //set it as true if you wish to collect Physical IDs
                collect:true,
                //Tweak physical ID message
                message:'To continue, please attach a state or federally-issued photo ID. The image of your ID must be in color with all four corners visible.',
                //Set true to open up user's webcame. False just opens up a file uploader, not webcam.
                no_webcam:true
              },
              //on sanbdox or production?
              development_mode: true,
              //should the user be prompted to do KYC?
              do_kyc: false,
              //show the user be prompted to link a bank account?
              do_banks: true,
              //if set to false, other options (payments and link bank will be disabled until KYC is finished.)
              kyc_done: true,
              userInfo: {
                //user's credentials
                oauth_key: 'rWEu6W4NfPwPpzgE7qfoNatMOQH9572qZfJoriWC',
                fingerprint: 'suasusau21324redakufejfjsf'
                /*oauth_key: 'oauth-6943beaa-0a59-4f0b-87b0-f0ccf2c98a79',
                fingerprint: '84fd65b1e8e40b80a44204b963cd636b'*/
              },
              colors: {
                //color prefrences
                'trim':'#059db1',
                'unfocused':'UNFOCUSED_COLOR',
                'text':'#059db1',
              },
              messages:{
                //messages to display below the buttons
                'kyc_message':'Please click on the button above to verify your Identity before creating a transaction.',
                'bank_message':'Please click on the button above to link a bank account with your profile.',
                'trans_mesage':'Please click on the button above to make a transaction.'
              },
              //your logo
              //receiverLogo: '<?= $this->config->item("uploads") ?>site_logo/<?= $this->config->item("site_logo") ?>'   
              receiverLogo: '<?= $this->config->item('uploads') ?>site_logo/<?= $this->config->item('site_logo') ?>'
          };
          setupSynapseiFrame(iframeInfo);
          $("#synapse_iframe").css("visibility","visible");
          $("#synapse_iframe").css("height","100%");
          $("#synapse_iframe").css("width","100%");
          $("#synapse_iframe").css("height","100%");
          $("#synapse_iframe").css("width","100%");
          expressReciver = function (e) {
              var json = JSON.parse(e.data);
              var json = json.json;
              if(json === undefined){
                  try{
                    var json = JSON.parse(e.data);
                    if (json.cancel || json.close) {
                      $("#synapse_iframe").css("visibility","hidden");
                      $("#synapse_iframe").css("height","0%");
                      $("#synapse_iframe").css("width","0%");
                      $("#synapse_iframe").prop("src","");
                      //self.set('enableButton', true);
                    };
                  }catch (e){
                     console.log(e);
                  };
              }else{
                  try{
                    if(json.success){
                        var nodes_arr = json.nodes;
                        var fd = new FormData();
                        for (var j = 0; j < nodes_arr.length; j++) {
                           var account_number = nodes_arr[j].info.account_num;
                           var class_is = nodes_arr[j].info.class;
                           var bank_name = nodes_arr[j].info.bank_name;
                           var oid = nodes_arr[j]._id.$oid;
                           fd.append("bank_name[]", bank_name);
                           fd.append("class[]", class_is);                     
                           fd.append("account_number[]", account_number);
                           fd.append("oid[]", oid);
                        }
                        $.ajax({
                              url: '<?php echo base_url('property/update_linked_account'); ?>',
                              type: 'POST',
                              data: fd,
                              processData: false,
                              contentType: false,
                              success: function (data) {                            
                              }
                          });
                    };
                    if (json.cancel || json.close) {
                      $("#synapse_iframe").css("visibility","hidden");
                      $("#synapse_iframe").css("height","0%");
                      $("#synapse_iframe").css("width","0%");
                      $("#synapse_iframe").prop("src","");
                      self.set('enableButton', true);
                    };
                  }catch (e){
                     console.log(e);
                  };
              }
            
          };
        window.addEventListener('message', expressReciver, false);
      });
    //$(".close icon").click(function(){
    $(document).on('click', '.close icon', function (event) {
        alert("hi");
        $("#synapse_iframe").css("visibility","hidden");
        $("#synapse_iframe").css("height","0%");
        $("#synapse_iframe").css("width","0%");
        $("#synapse_iframe").prop("src","");
    });
  });
</script>
  <?php
//}
?>
<div class="level">
    <div class="container">
        <div class="level-indicator">
            <img src="<?= $this->config->item('templateassets') ?>images/full-level.png" alt="" />
            <ul>
                <li> Property Info</li>
                <li>Tenants</li>
                <li class="blue-col1">tServices</li>
            </ul>
        </div>
    </div>
</div>
<section class="signup-section2">
    <div class="container">
        <?php echo form_open_multipart('', array('class' => 'payment_form', 'id' => 'payment_form')) ?>	    
            <div class="ajax_report alert display-hide" role="alert" style="margin-bottom: 10px; margin-left: 0px; width: 500px; position: relative; top: 100px;">
              <span class="close-message"></span>
              <div class="ajax_message">Hello Message</div>
          </div>
            <div class="tpay-acc-setup">
              <div class="tpay-acc-setup-left">
                <h3>Step 1 : General Information</h3>
                <?php if(!empty($account_info)){ ?>
                      <div class="step-in">
                        <div class="tpay-field-part">
                          <input type="text" name="first_name" value="<?php echo isset($account_info->first_name) ? $account_info->first_name : ''; ?>" placeholder="First name">
                        </div>
                        <div class="tpay-field-part">
                          <input type="text" name="last_name" value="<?php echo isset($account_info->last_name) ? $account_info->last_name : ''; ?>" placeholder="Last name">
                        </div>
                        <div class="tpay-field-part">
                          <input type="text" class="phone_us" name="phone" value="<?php echo isset($account_info->phone) ? $account_info->phone : ''; ?>" placeholder="Phone">
                        </div>
                        <div class="tpay-field-part">
                          <input type="email" name="email" value="<?php echo isset($account_info->email) ? $account_info->email : ''; ?>" placeholder="Email">
                        </div>
                        <div class="tpay-field-part">
                          <input type="text" name="address_1" value="<?php echo isset($account_info->address_1) ? $account_info->address_1 : ''; ?>" placeholder="Address 1">
                        </div>
                        <div class="tpay-field-part">
                          <input type="text" name="address_2" value="<?php echo isset($account_info->address_2) ? $account_info->address_2 : ''; ?>" placeholder="Address 2">
                        </div>
                        <div class="clearfix"></div>
                        <div class="tpay-field-part tpay-field-divide">
                          <input type="text" name="unit" value="<?php echo isset($account_info->unit) ? $account_info->unit : ''; ?>" placeholder="Unit">
                          <input type="text" name="city" value="<?php echo isset($account_info->city) ? $account_info->city : ''; ?>" placeholder="City">
                        </div>  
                        <div class="tpay-field-part tpay-field-divide">
                          <input type="text" name="state" value="<?php echo isset($account_info->state) ? $account_info->state : ''; ?>" placeholder="State">
                          <input type="text" name="zip" value="<?php echo isset($account_info->zip) ? $account_info->zip : ''; ?>" placeholder="Zip">
                        </div>
                        <div class="tpay-field-part tpay-field-divide">
                          <input type="text" name="dob" class="accountdob" value="<?php echo (isset($account_info->dob) && $account_info->dob !=0 ) ? date( 'm/d/Y', $account_info->dob ) : ''; ?>"  placeholder="DOB">
                        </div>
                      <div class="clearfix"></div>
                   </div>   

                <?php }else{ ?>
                    <div class="step-in">
                      <div class="tpay-field-part">
                        <input type="text" name="first_name" value="<?php echo isset($mem_info->first_name) ? $mem_info->first_name : ''; ?>" placeholder="First name">
                      </div>
                      <div class="tpay-field-part">
                        <input type="text" name="last_name" value="<?php echo isset($mem_info->last_name) ? $mem_info->last_name : ''; ?>" placeholder="Last name">
                      </div>
                      <div class="tpay-field-part">
                        <input type="text" name="phone" value="<?php echo isset($mem_info->mobile_no) ? $mem_info->mobile_no : ''; ?>" placeholder="Phone">
                      </div>
                      <div class="tpay-field-part">
                        <input type="email" name="email" value="<?php echo isset($mem_info->email) ? $mem_info->email : ''; ?>" placeholder="Email">
                      </div>
                      <div class="tpay-field-part">
                        <input type="text" name="address_1" value="<?php echo isset($mem_info->mailing_address1) ? $mem_info->mailing_address1 : ''; ?>" placeholder="Address 1">
                      </div>
                      <div class="tpay-field-part">
                        <input type="text" name="address_2" value="<?php echo isset($mem_info->mailing_address2) ? $mem_info->mailing_address2 : ''; ?>" placeholder="Address 2">
                      </div>
                      <div class="clearfix"></div>
                      <div class="tpay-field-part tpay-field-divide">
                        <input type="text" name="unit" value="<?php echo isset($mem_info->mailing_unit) ? $mem_info->mailing_unit : ''; ?>" placeholder="Unit">
                        <input type="text" name="city" value="<?php echo isset($mem_info->mailing_city) ? $mem_info->mailing_city : ''; ?>" placeholder="City">
                      </div>  
                      <div class="tpay-field-part tpay-field-divide">
                        <input type="text" name="state" value="<?php echo isset($mem_info->mailing_state_id) ? $mem_info->mailing_state_id : ''; ?>" placeholder="State">
                        <input type="text" name="zip" value="<?php echo isset($mem_info->mailing_zip) ? $mem_info->mailing_zip : ''; ?>" placeholder="Zip">
                      </div>
                      <div class="tpay-field-part tpay-field-divide">
                        <input type="text" name="dob" class="accountdob" value="<?php echo (isset($mem_info->dob) && $mem_info->dob !=0 ) ? date( 'm/d/Y', $mem_info->dob ) : ''; ?>"  placeholder="DOB">
                      </div>
                      <div class="clearfix"></div>
                   </div>   

                <?php } ?> 
                   <h3>Step 2 : Bank Information</h3> 
                  <div class="step-in linked-btn-sec">
                    <input type="button" name="linked_bank_account" id="linked_bank_account" value="Link Bank Account" >
                    <div class="clearfix"></div>
                    <div id="linked-acc">
                      <!-- <h4>Linked Bank Account</h4> -->
                      <div class="linked-acc-part">  
                        <label>Bank Account</label>                       
                          <select name="bank_account" id="bank_account">
                            <option value="">Select Account... </option>
                            <?php foreach ($linked_account_number as $key => $value) { ?>
                                    <?php if($account_info->linked_account_id == $value->id){ ?> >
                                              <option selected value="<?php echo $value->id; ?>"><?php echo $value->bank_name.' '.$value->class.' xxxx-xxxx-xxxx-'.$value->account_number; ?> </option>
                                    <?php }else{ ?>
                                              <option value="<?php echo $value->id; ?>"><?php echo $value->bank_name.' '.$value->class.' xxxx-xxxx-xxxx-'.$value->account_number; ?> </option>
                                    <?php } ?>
                            <?php } ?>
                          </select>
                          <a href="javascript:void(0)" style="display:none" id="remove_sel_acc" class="remove_sel_acc"><img src="<?= $this->config->item('templateassets') ?>images/cross.png" alt=""/></a>
                      </div>
                      <div class="clearfix"></div>                         
                        <!--<span class="acc-show">Routing </span>
                        <span class="acc-show">Account xxxx-xxxx-xxxx </span> -->                  
                    </div>
                    <div class="clearfix"></div>
                  </div>
                  
                <h3>Step 3 : Documentation ( one time only )</h3> 
                  <div class="step-in2">

                      <p>Please upload a copyof each document if unable to upload, you can email or fax them to us </p>
                        <div class="acc-contact-left">
                          <div>
                            <h4>Email : 123@gmail.com</h4>
                            <h4>Fax : 888-888-8888</h4>
                          </div>

                      <p>
                        <span>Driver's Licence or Photo ID </span>
                        <input type="file" name="driving_document" value="Upload">
                        <span class="uploaded_doc_file">
                          <?php if(isset($account_info->recent_bank_statment) && $account_info->recent_bank_statment != ''){ ?>
                                    <?php echo $account_info->recent_bank_statment; ?>
                          <?php } ?>
                        </span>
                        </span>
                      </p>
                      <p>
                        <span>Recent bank attachment with matching address </span>
                        <input type="file" name="recent_bank_statment"  value="Upload">
                        <span class="uploaded_doc_file">
                          <?php if(isset($account_info->driving_document) && $account_info->driving_document != ''){ ?>
                                    <?php echo $account_info->driving_document; ?>
                          <?php } ?>
                        </span>
                      </p>
                      </div>
                      
                      <div class="clearfix"> </div>
                      <hr>
                      <div class="policy-agreement">
                      <p>ACH Agreement <input <?php echo (isset($account_info->ach_aggrement) && $account_info->ach_aggrement == 'yes') ? 'checked' : ''; ?> type="checkbox" name="ach_aggrement"  value="yes" /> I Agree</p>

                      <span class="agreement">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</span>

                      <p>Terms and Conditions <input <?php echo (isset($account_info->terms_conditions) && $account_info->terms_conditions == 'yes') ? 'checked' : ''; ?> type="checkbox" name="terms_conditions" value="yes" /> I Agree</p>

                      <span class="agreement">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident.</span>
                      </div>
                      <div class="bank-center">
                       <p>Thank You, You will be  notified of  approval by xxxxxxxx</p>
                      </div>
                      <div class="bank-center">
                        <a  class="my-link" href="<?= site_url('add-property-step3') ?>"  name=""  />BACK</a>
                        <input type="submit" name="" value="SUBMIT" />
                        <a href="javascript:;" class="my-link save-for-later-account-setup">Save for Later</a>
                      </div>
                      <div class="clearfix"></div>
                    </div>  
              </div>

              <div class="tpay-acc-setup-right">
                <h4>Commonly Asked Questions</h4>
                
                <span>Lorem ipsum dolor sit amet, consec tetur adipi scing elit, sed do eius mod tempor incididunt ut labore et dolore magna aliqua.</span>

                <span>sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris.</span>

                <span>Ut enim ad minim veniam, quis nostrud exer citation ullamco laboris nisi ut aliquip ex ea commodo consequat.</span>
                </div>
              
              <div class="clearfix"></div>
              <input type="hidden" value="save" class="action-type" name="action-type">
            </div>          
        <?php echo form_close(); ?>
    </div>
</section>