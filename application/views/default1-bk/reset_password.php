<section class="dr-banner" style="min-height:600px;">
	<div class="container">
		<div class="right-banner" style="width: 654px !important; margin-right:330px;">
			<h4>Reset Password</h4>
			<?php if(isset($check) && !empty($check)){ ?>
			<?php echo form_open('',array('class'=>'ajaxForm','id'=>'reset-password'))?>
			<div class="ajax_report alert display-hide" role="alert" style="margin-left:0px;margin-bottom:10px;width:500px;"><span class="close-message"></span><div class="ajax_message">Hello Message</div></div>
			<div class="form-group1">
				<div class="my-div"><input type="password" placeholder="New Password" class="form-control my-txt-fleid " name="password" style="width:500px;"></div>
				<div class="my-div"><input type="password" placeholder="Confirm password" class="form-control my-txt-fleid " name="confirm_password" style="width:500px;"></div>
			</div>
			<input type="submit" class="formsign-bttn" value="SUBMIT" style="width:50%"/> 
			<?=form_close();?>
                        <?php }else{
                            echo '<h5>Reset Password Link expired</h5>';
                        } ?>
		</div>
	</div>
</section>
<section class="dr-content">
	<section class="howit-works">
		<div class="container">
			<div class="drwork-text">
				<h5>How it works</h5>
				<p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio.</p>
				<a href="#">Learn More</a>
			</div>
			<div class="dr-rightvideo">
				<a href="#"><img src="<?=$this->config->item('templateassets')?>images/video-img.png" alt=""/></a>
			</div>
		</div>
	</section>
	<section class="dr-testimonial">
		<div class="container test-part">
			<h2>Why Use TenantTag?</h2>
			<div class="testimonial">
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit<br>
in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
				<span class="client-name">- Jeffrey Zeldman</span>
			</div>
			<div class="testimonial1">
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim<br> veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit <br>esse cillum dolore eu, fugiat nulla pariatur.</p>
				<span class="client-name">- Paul Boag</span>
			</div>
		</div>
	</section>
</section>

