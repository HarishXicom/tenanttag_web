
<link href="<?= $this->config->item('templateassets') ?>css/theme.blue.css" rel="stylesheet">
<script type="text/javascript" src="<?= $this->config->item('templateassets') ?>js/jquery.validate.min.js"></script>   
<script type="text/javascript" src="<?= $this->config->item('templateassets') ?>js/jquery.tablesorter.js"></script>   
<script type="text/javascript" src="<?= $this->config->item('templateassets') ?>js/jquery.tablesorter.widgets.js"></script>   

<script id="js">// *** Filter search type function arguments ***
// data.filter = filter input value for a column;
// data.iFilter = same as filter, except lowercase (if wo.filter_ignoreCase is true)
// data.exact = table cell text (or parsed data, if  save_sch_smscolumn parser enabled)
// data.iExact = same as exact, except lowercase (if wo.filter_ignoreCase is true)

// search for a match from the beginning of a string
// "^l" matches "lion" but not "koala"
    $.tablesorter.filter.types.start = function (config, data) {
        if (/^\^/.test(data.iFilter)) {
            return data.iExact.indexOf(data.iFilter.substring(1)) === 0;
        }
        return null;
    };

// search for a match at the end of a string
// "a$" matches "Llama" but not "aardvark"
    $.tablesorter.filter.types.end = function (config, data) {
        if (/\$$/.test(data.iFilter)) {
            var filter = data.iFilter,
                    filterLength = filter.length - 1,
                    removedSymbol = filter.substring(0, filterLength),
                    exactLength = data.iExact.length;
            return data.iExact.lastIndexOf(removedSymbol) + filterLength === exactLength;
        }
        return null;
    };

    $(function () {

        $('#filters')
                .on('filterEnd filterReset', function (e, table) {
                    var c = this.config,
                            fr = c.filteredRows;
                    if (fr === 0) {
                        c.$table.append([
                            '<tr class="noData remove-me" role="alert" aria-live="assertive">',
                            '<td colspan="' + c.columns + '">No Data Found</td>',
                            '</tr>'
                        ].join(''));
                    } else {
                        c.$table.find('.noData').remove();
                    }
                })
                .tablesorter({
                    theme: 'blue',
                    widthFixed: false,
                    widgets: ["zebra", "filter"],
                    widgetOptions: {
                        filter_reset: '.reset'
                    }
                });
        $('#filters2')
                .on('filterEnd filterReset', function (e, table) {
                    var c = this.config,
                            fr = c.filteredRows;
                    if (fr === 0) {
                        c.$table.append([
                            '<tr class="noData remove-me" role="alert" aria-live="assertive">',
                            '<td colspan="' + c.columns + '">No Data Found</td>',
                            '</tr>'
                        ].join(''));
                    } else {
                        c.$table.find('.noData').remove();
                    }
                })
                .tablesorter({
                    theme: 'blue',
                    widthFixed: false,
                    widgets: ["zebra", "filter"],
                    widgetOptions: {
                        filter_reset: '.reset'
                    }
                });
    });

</script>
<script>
    $(document).on("click", ".my-tab li", function () {
        $('.my-tab li').removeClass('active');
        $(this).addClass('active');
        $('.tab-area').show();
        $(".add_vndr_tab").hide();
        var pid = $(this).find('a').attr('data-value');
        $.ajax({
            url: '<?php echo base_url(); ?>message/getSinglePropertySMS',
            type: 'POST',
            data: {id: pid},
            beforeSend: function () {
                $('#wait-div').show();
            },
            success: function (data) {
                $("#wait-div").hide();
                if (pid) {
                    $("#tabs-" + pid).html(data);
                } else {
                    $(".msg-block").html(data);
                }
            },
            error: function () {
                $("#wait-div").hide();
                alert("error in form submission");
            }
        });

    });

    $(document).on('click', '.my-vendor', show_add_vendor);

    function show_add_vendor()
    {
        $('.my-tab li').removeClass('active');
        $('.tab-area').hide();
        $(".add_vndr_tab").show();

    }
    jQuery.validator.addMethod('phoneUS', function (phone_number, element) {
        phone_number = phone_number.replace(/\s+/g, '');
        return this.optional(element) || phone_number.length > 9 &&
                phone_number.match(/^(1-?)?(\([2-9]\d{2}\)|[2-9]\d{2})-?[2-9]\d{2}-?\d{4}$/);
    }, 'Enter a valid phone.');

</script>
<?php
$weeks = array('1' => '1 week', '2' => '2 weeks', '3' => '3 weeks', '4' => '4 weeks');



for ($j = 1; $j < 31; $j++) {
    $days[$j] = ordinal($j);
}
?>
<script>
    $(document).ready(function () {
        $(".minus").click(function () {
            $(".drproperty").slideToggle("slow");
            $(this).toggleClass("drcollapse");
        });
        $(".secondminus").click(function () {
            $(".secondm").slideToggle("slow");
            $(this).toggleClass("drcollapse");
        });

        $(".crt_btn").click(function () {
            $(".wrte_msg").val('');
            $(".select_msg_div").hide();
            $(".create_msg_div").show();
        });
        $(".sel_btn").click(function () {
            $(".create_msg_div").hide();
            $(".select_msg_div").show();
        });

        $(".sch_msg").click(function () {
            var value = $(this).val();
            if (value == 'Yes') {
                $('.sch-container').show();
                $(".final_btn").val('Save');
            }
            if (value == 'No') {
                $('.sch-container').hide();
                $(".final_btn").val('Send');
            }
        });

        $(".tnt_check_all").click(function () {
            if ($(this).is(':checked')) {
                $(".tnt_check").prop('checked', true);
                $(".shw_tnt").show();
            } else {
                $(".tnt_check").prop('checked', false);
                $(".shw_tnt").hide();
            }
            $(".check_error").hide();

        });

        $(".tnt_check").click(function () {
            var id = $(this).val();
            if ($(this).is(':checked')) {
                $("#shw_tnt_" + id).show();
            } else {
                $("#shw_tnt_" + id).hide();
            }
            if ($('.tnt_check:checked').length == $('.tnt_check').length) {
                $(".tnt_check_all").prop('checked', true);
            } else {
                $(".tnt_check_all").prop('checked', false);
            }
            $(".check_error").hide();
        });

        $(".remove_tnt").click(function () {
            var id = $(this).attr('id');
            $("#tenant_" + id).prop('checked', false);
            $(this).parent().parent().hide();
            $(".check_error").hide();
        });


        $(".prp_check_all").click(function () {
            if ($(this).is(':checked')) {
                $(".prp_check").prop('checked', true);
                $(".shw_prp").show();
            } else {
                $(".prp_check").prop('checked', false);
                $(".shw_prp").hide();
            }
            $(".check_error").hide();

        });

        $(".prp_check").click(function () {
            var id = $(this).val();
            if ($(this).is(':checked')) {
                $("#shw_prp_" + id).show();
            } else {
                $("#shw_prp_" + id).hide();
            }
            if ($('.prp_check:checked').length == $('.prp_check').length) {
                $(".prp_check_all").prop('checked', true);
            } else {
                $(".prp_check_all").prop('checked', false);
            }
            $(".check_error").hide();
        });

        $(".remove_prp").click(function () {
            var id = $(this).attr('id');
            $("#property_" + id).prop('checked', false);
            $(this).parent().parent().hide();
            $(".check_error").hide();
        });

        $(".sel_prp_tnt").click(function () {

            if ($(this).val() == 'property') {
                if ($('.tnt_check:checked').length > 0) {
                    swal({
                        title: "Alert?",
                        text: "Changes made will be lost, would you like to proceed??",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes",
                        closeOnConfirm: true,
                        closeOnCancel: true
                    },
                    function (isConfirm) {
                        if (isConfirm) {
                            $(".add_edit_tnt").hide();
                            $(".shw_prp").hide();
                            $(".prp_check_all").prop('checked', false);
                            $(".prp_check").prop('checked', false);
                            $(".add_edit_prp").show();
                        } else {
                            $("#radio006").prop('checked', false);
                            $("#radio005").prop('checked', true);
                        }
                    });
                }else{
                     $(".add_edit_tnt").hide();
                            $(".shw_prp").hide();
                            $(".prp_check_all").prop('checked', false);
                            $(".prp_check").prop('checked', false);
                            $(".add_edit_prp").show();
                }
            } else {
                if ($('.prp_check:checked').length > 0) {
                    swal({
                        title: "Alert?",
                        text: "Changes made will be lost, would you like to proceed?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes",
                        closeOnConfirm: true,
                        closeOnCancel: true
                    },
                    function (isConfirm) {
                        if (isConfirm) {
                            $(".add_edit_prp").hide();
                            $(".shw_tnt").hide();
                            $(".tnt_check_all").prop('checked', false);
                            $(".tnt_check").prop('checked', false);
                            $(".add_edit_tnt").show();
                        } else {
                            $("#radio006").prop('checked', true);
                            $("#radio005").prop('checked', false);
                        }
                    });

                }else{
                     $(".add_edit_prp").hide();
                            $(".shw_tnt").hide();
                            $(".tnt_check_all").prop('checked', false);
                            $(".tnt_check").prop('checked', false);
                            $(".add_edit_tnt").show();
                }


            }
        });
//datepicker({changeMonth: true, changeYear: true, maxDate: 0, defaultDate: "01/01/1945"})
        $('.startFrom').datepicker({dateFormat: 'mm/dd/y', minDate: 0});
        $(document).delegate('.repeat_msg', 'change', function () {
            if ($(this).val() == 'week') {
                $("#repeat_every").html('');
                var weeks = '<?php echo json_encode($weeks); ?>';
                weeks = JSON.parse(weeks);
                $.each(weeks, function (key, value) {
                    $("#repeat_every").append('<option value="' + key + '">' + value + '</option>');
                });
                $(".repeat-every").show();
                $(".repeat_on").show();
            }
            else if ($(this).val() == 'month') {
                $("#repeat_every").html('');
                var dayz = '<?php echo json_encode($days); ?>';
                dayz = JSON.parse(dayz);
                $.each(dayz, function (key, value) {
                    $("#repeat_every").append('<option value="' + key + '">' + value + '</option>');
                });
                $(".repeat-every").show();
                $(".repeat_on").hide();
            } else {
                $("#repeat_every").html('');
                $(".repeat-every").hide();
                $(".repeat_on").hide();
            }
        });

        $(document).delegate('.edit_repeat_msg', 'change', function () {
            var id = $(this).attr('resp');
            if ($(this).val() == 'week') {
                $("#erv_" + id).html('');
                var weeks = '<?php echo json_encode($weeks); ?>';
                weeks = JSON.parse(weeks);
                $.each(weeks, function (key, value) {
                    $("#erv_" + id).append('<option value="' + key + '">' + value + '</option>');
                });
                $("#erv_" + id).show();
                $("#ero_" + id).show();
            }
            else if ($(this).val() == 'month') {
                $("#erv_" + id).html('');
                var dayz = '<?php echo json_encode($days); ?>';
                dayz = JSON.parse(dayz);
                $.each(dayz, function (key, value) {
                    $("#erv_" + id).append('<option value="' + key + '">' + value + '</option>');
                });
                $("#erv_" + id).show();
                $("#ero_" + id).hide();
            } else {
                $("#erv_" + id).html('');
                $("#erv_" + id).hide();
                $("#ero_" + id).hide();
            }
        });

        $("#radio008").click(function () {
            $(".repeat-everyoc").hide();
        });
        $("#radio009").click(function () {
            $(".repeat-everyoc").show();
        });

        $("#send_sch_message").validate({
            // Specify the validation rules
            rules: {
                custom_message: {
                    required: true,
                    //  maxlength: 140,
                },
                sch_date: {
                    required: '#radio007:checked'
                }
            },
            // Specify the validation error messages
            messages: {
                custom_message: {
                    required: 'Enter your message'
                },
                sch_date: {
                    required: 'Select start date'
                }
            },
            submitHandler: function (form) {
                if ($(".sel_prp_tnt:checked").val() == 'property') {
                    var atLeastOneIsChecked = false;
                    $('.prp_check').each(function () {
                        if ($(this).is(':checked')) {
                            atLeastOneIsChecked = true;
                        }
                    });
                    if (atLeastOneIsChecked == false) {
                        $(".check_error").text('Please check at least one property');
                        $(".check_error").show();
                        return false;
                    }
                } else {
                    var atLeastOneIsChecked = false;
                    $('.tnt_check').each(function () {
                        if ($(this).is(':checked')) {
                            atLeastOneIsChecked = true;
                        }
                    });
                    if (atLeastOneIsChecked == false) {
                        $(".check_error").text('Please check at least one tenant');
                        $(".check_error").show();
                        return false;
                    }
                }
                $.ajax({
                    url: '<?php echo base_url(); ?>message/send_save_msg',
                    type: 'POST',
                    data: $("#send_sch_message").serialize(),
                    beforeSend: function () {
                        $('#wait-div').show();
                    },
                    success: function (data) {
                        $("#wait-div").hide();

                        var myArray = JSON.parse(data);
                        if (myArray.success) {
                            swal({
                                title: "Sucess",
                                text: myArray.success_message,
                                type: "success"
                            },
                            function () {
                                location.reload();
                            });
                        } else {
                            swal("Error!", myArray.success_message, "error");
                        }

                    },
                    error: function () {
                        $("#wait-div").hide();
                        alert("error in form submission");
                    }
                });
            }
        });

        $(".read_more").click(function () {
            $(this).parent().next().show();
            $(this).parent().hide();
        });


        $(document).on('click', '.del', del_sms);
        function del_sms(e)
        {
            var id = $(this).attr('id');
            var url = "<?php echo base_url('messages/delete_message'); ?>";

            swal({
                title: "Alert?",
                text: "Are you sure to delete this message?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            },
            function () {
                $.ajax({
                    url: '<?php echo base_url('message/delete_message'); ?>',
                    type: 'POST',
                    data: {id: id},
                    beforeSend: function () {
                        $('#wait-div').show();
                    },
                    success: function (data) {
                        $("#wait-div").hide();
                        swal({
                            title: "Deleted",
                            text: "Deleted Successfully",
                            type: "success"
                        },
                        function () {
                            $(e.target).parent().parent().parent().remove();
                        });
                    },
                    error: function () {
                        $("#wait-div").hide();
                        swal("Error!", "Server not responding.", "error");
                    }
                });
            });
        }
        
         $(document).on('click', '.dellog', del_sms);
        function del_sms(e)
        {
            var id = $(this).attr('id');
            var url = "<?php echo base_url('messages/delete_log'); ?>";

            swal({
                title: "Alert?",
                text: "Are you sure to delete this message?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            },
            function () {
                $.ajax({
                    url: '<?php echo base_url('message/delete_log'); ?>',
                    type: 'POST',
                    data: {id: id},
                    beforeSend: function () {
                        $('#wait-div').show();
                    },
                    success: function (data) {
                        $("#wait-div").hide();
                        swal({
                            title: "Deleted",
                            text: "Deleted Successfully",
                            type: "success"
                        },
                        function () {
                            $(e.target).parent().parent().parent().remove();
                        });
                    },
                    error: function () {
                        $("#wait-div").hide();
                        swal("Error!", "Server not responding.", "error");
                    }
                });
            });
        }

        $(document).delegate('.tedit_repeat_msg', 'change', function () {
            var id = $(this).attr('resp');
            if ($(this).val() == 'week') {
                $("#terv_" + id).html('');
                var weeks = '<?php echo json_encode($weeks); ?>';
                weeks = JSON.parse(weeks);
                $.each(weeks, function (key, value) {
                    $("#terv_" + id).append('<option value="' + key + '">' + value + '</option>');
                });
                $("#terv_" + id).show();
                $("#tero_" + id).show();
            }
            else if ($(this).val() == 'month') {
                $("#terv_" + id).html('');
                var dayz = '<?php echo json_encode($days); ?>';
                dayz = JSON.parse(dayz);
                $.each(dayz, function (key, value) {
                    $("#terv_" + id).append('<option value="' + key + '">' + value + '</option>');
                });
                $("#terv_" + id).show();
                $("#tero_" + id).hide();
            } else {
                $("#terv_" + id).html('');
                $("#terv_" + id).hide();
                $("#tero_" + id).hide();
            }
        });

        $(document).on('click', '.tsave_sch_sms', tsave_sch_sms);
        function tsave_sch_sms(e)
        {
            var id = $(this).attr('id');
            var scheduled_on = $("#tsch_" + id).val();
            var repeat_type = $("#tetype_" + id).val();
            var repeat_every = $("#terv_" + id).val();
            var repeat_on = $("#tero_" + id).val();
            var occurences = $("#tert_" + id).val();
             var sms = $("#tschta_" + id).val();
            $.ajax({
                url: '<?php echo base_url('message/update_message'); ?>',
                type: 'POST',
                data: {id: id, scheduled_on: scheduled_on, repeat_type: repeat_type, repeat_every: repeat_every, repeat_on: repeat_on, occurences: occurences,sms:sms},
                beforeSend: function () {
                    $('#wait-div').show();
                },
                success: function (data) {
                    $("#wait-div").hide();
                    swal({
                        title: "Sucess",
                        text: "Updated Successfully",
                        type: "success"
                    },
                    function () {
                        $("#tnca_" + id).hide();
                        $("#tnca_" + id).prev().prev().prev().find(".date-msg a").text(scheduled_on);
                        $("#tnca_" + id).prev().prev().text(sms);
                    });
                },
                error: function () {
                    $("#wait-div").hide();
                    swal("Error!", "Server not responding.", "error");
                }
            });
        }

        $(document).on('click', '.edit_sch_sms', show_nca);
        function show_nca(e) {
            $(".new-cont-area").hide();
            var id = $(this).attr('id');
            $("#nca_" + id).toggle();
        }

        $(document).on('click', '.tedit_sch_sms', tshow_nca);
        function tshow_nca(e) {

            $(".new-cont-area").hide();
            var id = $(this).attr('id');
            $("#tnca_" + id).toggle();
        }

        $(document).on('click', '.save_sch_sms', save_sch_sms);
        function save_sch_sms(e)
        {
            var id = $(this).attr('id');
            var scheduled_on = $("#sch_" + id).val();
            var repeat_type = $("#etype_" + id).val();
            var repeat_every = $("#erv_" + id).val();
            var repeat_on = $("#ero_" + id).val();
            var occurences = $("#ert_" + id).val();
            var sms = $("#schta_" + id).val();
            $.ajax({
                url: '<?php echo base_url('message/update_message'); ?>',
                type: 'POST',
                data: {id: id, scheduled_on: scheduled_on, repeat_type: repeat_type, repeat_every: repeat_every, repeat_on: repeat_on, occurences: occurences,sms:sms},
                beforeSend: function () {
                    $('#wait-div').show();
                },
                success: function (data) {
                    $("#wait-div").hide();
                    swal({
                        title: "Sucess",
                        text: "Updated Successfully",
                        type: "success"
                    },
                    function () {
                        $("#nca_" + id).hide();
                        $("#nca_" + id).prev().prev().find(".date-msg a").text(scheduled_on);
                        $("#nca_" + id).prev().prev().prev().text(sms);
                    });
                },
                error: function () {
                    $("#wait-div").hide();
                    swal("Error!", "Server not responding.", "error");
                }
            });
        }

    });


    $(function () {
        $("#tabs").tabs();
    });
    $(function () {
        $(".twotabs").tabs();
    });
    $(function () {
        //  $("#tabs2").tabs();
    });


</script>

