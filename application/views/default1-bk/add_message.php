<?php echo form_open('message/send_save_msg', array('class' => '', 'id' => 'send_sch_message')) ?>

<div class="tab-area add_vndr_tab" style="display:none">
    <div class="ajax_report alert display-hide" role="alert" style="padding:10px;">
        <span class="close"></span>
        <span class="ajax_message">Hello Message</span>
    </div>
    <div class="table-content vendor-table">
        <div class="edit-msg">
            <!--Create Message Div-->
            <div class="panel panle-brown create_msg_div" >
                <div class="panel-head">
                    <div class="panel-title-1">
                        <h1>Create New</h1>
                        <!--<button class="btn create-btn sel_btn">Select</button>-->
                    </div>
                </div>
                <div class="panel-body padd0">
                    <div class="event">
                        <textarea name="custom_message" class="form-control wrte_msg" placeholder="Write your message here..."></textarea>
                    </div>
                </div>
            </div>
            <!--Create Message Div END-->

            <div class="panel panle-brown">
                <div class="panel-head">
                    <div class="panel-title-1">
                        <div class="my-div">
                            <p class="my-radio blue-radio ">  <input type="radio" class="sel_prp_tnt" name="message_for" value="tenant" id="radio005" checked="true">  <label for="radio005">Tenants</label></p>
                            <p class="my-radio blue-radio "> <input type="radio"  class="sel_prp_tnt" name="message_for" value="property" id="radio006"> <label for="radio006">Property</label></p>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="add_edit_tnt">
                        <div class="search-area-part">
                            <!--                            <div class="search-portion">
                                                            <input type="search" class="my-search" placeholder="Seach Tenants"/>
                                                            <i class="fa fa-search"></i>
                                                        </div>-->
                            <div class="search-option">
                                <table id="filters" class="tablesorter">
                                    <thead>
                                        <tr>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if (isset($tenants) && !empty($tenants)) { ?>
                                            <tr>
                                                <td>
                                                    <div class="event">
                                                        <p class="my-event-checkbox">
                                                            <input type="checkbox" class="tnt_check_all" id="test025">    <label for="test025">All</label> </p>
                                                    </div>
                                                </td>
                                            </tr>
                                            <?php foreach ($tenants as $tenant): ?>
                                                <tr>
                                                    <td>
                                                        <div class="event">
                                                            <p class="my-event-checkbox"> 
                                                                <input type="checkbox" class="tnt_check" name="tenant_ids[]" value="<?php echo $tenant->tenant_id; ?>" id="tenant_<?php echo $tenant->tenant_id; ?>">    
                                                                <label for="tenant_<?php echo $tenant->tenant_id; ?>"><?php echo ucwords($tenant->first_name . " " . $tenant->last_name); ?></label> 
                                                            </p>
                                                        </div> 
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>

                                        <?php } else { ?>
                                        <div class="event">
                                            <p class="my-event-checkbox">No record found. </p>
                                        </div>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div> 
                        </div>
                        <div class="selected-item">
                            <div class="header-part">
                                <h1>Added Tenants</h1>
                            </div>
                            <?php ?>
                            <div class="content-part1">
                                <?php if (isset($tenants) && !empty($tenants)) { ?>
                                    <?php foreach ($tenants as $tenant): ?>
                                        <div class="event shw_tnt" id="shw_tnt_<?php echo $tenant->tenant_id; ?>" style="display: none">
                                            <p class="my-event-checkbox cross2"> 
                                                <img id="<?php echo $tenant->tenant_id; ?>" class="remove_tnt" src="<?php echo base_url() ?>/assets/default/images/deselect-tenant.png"/>
                                                <label><?php echo ucwords($tenant->first_name . " " . $tenant->last_name); ?></label> 
                                            </p>
                                        </div>
                                    <?php endforeach; ?>
                                <?php } ?>

                            </div>
                        </div>
                    </div>
                    <div class="add_edit_prp" style="display:none;">
                        <div class="search-area-part">

                            <div class="search-option">
                                <table id="filters2" class="tablesorter">
                                    <thead>
                                        <tr>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if (isset($properties) && !empty($properties)) { ?>
                                            <tr>
                                                <td>
                                                    <div class="event">
                                                        <p class="my-event-checkbox">
                                                            <input type="checkbox" class="prp_check_all" id="test0255">    <label for="test0255">All</label> </p>
                                                    </div>
                                                </td>
                                            </tr>
                                            <?php foreach ($properties as $property): ?>
                                            <tr>
                                                <td><div class="event">
                                                        <p class="my-event-checkbox"> 
                                                            <input type="checkbox" class="prp_check" name="property_ids[]" value="<?php echo $property->prop_id; ?>" id="property_<?php echo $property->prop_id; ?>">    
                                                            <label for="property_<?php echo $property->prop_id; ?>"><?php echo ucwords($property->city . " " . $property->address1 . " " . $property->address2); ?></label> 
                                                        </p>
                                                    </div>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                </div>
                            <?php }else { ?>
                                <div class="event">
                                    <p class="my-event-checkbox">No record found. </p>
                                </div>
                            <?php } ?>
                            </tbody>
                            </table>
                        </div> 
                    </div>
                    <div class="selected-item">
                        <div class="header-part">
                            <h1>Added Property</h1>
                        </div>
                        <div class="content-part1">
                            <?php if (isset($properties) && !empty($properties)) { ?>
                                <?php foreach ($properties as $property): ?>
                                    <div class="event shw_prp" id="shw_prp_<?php echo $property->prop_id; ?>" style="display: none">
                                        <p class="my-event-checkbox cross2"> 
                                            <img id="<?php echo $property->prop_id; ?>" class="remove_prp" src="<?php echo base_url() ?>/assets/default/images/deselect-tenant.png"/>
                                            <label><?php echo ucwords($property->city . " " . $property->address1 . " " . $property->address2); ?></label> 
                                        </p>
                                    </div>
                                <?php endforeach; ?>
                            <?php } ?>

                        </div>
                    </div>

                </div>
                <label class="error check_error" style="display:none;"></label>
                <div  class="new-info-cont drnewaddmsg">
                    <div class="bock-area">
                        <label>Schedule Message</label>
                        <div class="my-div">
                            <p class="my-radio blue-radio sch_message">  <input type="radio" name="sch_msg" class="sch_msg"  id="radio007" value="Yes">  <label for="radio007">Yes</label></p>
                            <p class="my-radio blue-radio sch_message"> <input type="radio" name="sch_msg" class="sch_msg" id="radio0008" value="No" checked="true"> <label for="radio0008">No</label></p>
                        </div>
                    </div>

                    <div class="sch-container" style="display: none">
                        <div class="bock-area">
                            <label>Start Date :</label>
                            <input type="text" name="sch_date" class="form-control calender1 startFrom" placeholder="MM/DD/YY"/>
                        </div>
                        <div class="bock-area">
                            <label>Repeat:</label>
                            <select name="repeat_msg" class="form-control repeat repeat_msg">
                                <option value="week">Weekly</option>
                                <option value="month">Monthly</option>
                                <option value="year" selected="selected">Yearly</option>

                            </select>
                            <div class="repeat-every" style="display:none;">
                                <label class="every">Repeat Every :</label>
                                <select name="repeat_every" id="repeat_every" class="form-control repeat w65 " >

                                </select>
                            </div>

                            <label class="repeaton repeat_on"  style="display:none;">Repeat On :</label>
                            <select name="repeat_on" class="form-control repeat w repeat_on"  style="display:none;">
                                <option>Monday</option>
                                <option>Tuesday</option>
                                <option>Wednesday</option>
                                <option>Thursday</option>
                                <option>Friday</option>
                                <option>Saturday</option>
                                <option>Sunday</option>
                            </select>

                        </div>
                        <div class="bock-area">
                            <label>End:</label>
                            <div class="my-div end-time">
                                <p class="my-radio blue-radio ">  
                                    <input type="radio" name="end_msg" checked="true" value="infinte" id="radio008"> 
                                    <label for="radio008">Never</label>
                                </p>
                                <p class="my-radio blue-radio "> 
                                    <input type="radio" name="end_msg" value="finite" id="radio009">
                                    <label for="radio009">After</label>
                                </p>
                            </div>
                            <div class="repeat-everyoc" style="display: none">

                                <select name="msg_occurence" class="form-control repeat mr-lft0">
                                    <option value='1'>1 Occurence</option>
                                    <option value='2'>2 Occurences</option>
                                    <option value='3'>3 Occurences</option>
                               
                                </select>
                                
                            </div>
                        </div>
                    </div>
                </div>

                <input type="submit" class="btn btn-save final_btn" value="Send">

            </div>

        </div>

    </div>
</div>
</div>

<?= form_close(); ?> 	


