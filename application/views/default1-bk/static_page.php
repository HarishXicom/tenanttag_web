<section class="dr-banner bnr_pages">
    <div class="container">
        <div class="drwork-text" style="width:100%">
            <h5><?php echo $content->title; ?></h5>


        </div>
    </div>
</section>
<?php if ($content->slug == 'contact') { ?>
    <section class="howit-works contactus">
        <div class="container">
              <div class="cmn_style contacttxt">
                <?php echo $content->description; ?>
            </div> 
            <?php echo form_open('welcome/dosubmit', array('class' => 'ajaxForm contactus-section', 'id' => 'contact-us')) ?>
            <div class="ajax_report alert display-hide" role="alert" style="padding:10px;">
                <span class="close"></span>
                <span class="ajax_message" style="line-height:normal;margin: 0;padding: 3px;"></span>
            </div>
            <label>Name</label>  
            <input type="text" name="name"  required="true">
            <label>Mobile</label>  
            <input type="text" name="mobile"  required="true" class="phone_us">
            <label>Email</label>  
            <input type="text" name="cont_email" required="true">
            <label>Message</label>  
            <textarea name="message"></textarea>
            <input type="submit" value="Submit" class="drsubmitbttn"/>
            <?php form_close(); ?>
            
        </div>
    </section>
<?php } else { ?>
    <section class="howit-works">
        <div class="container">
            <div class="cmn_style" style="width:100%">

                <?php echo $content->description; ?>
            </div>
        </div>
    </section>
<?php } ?>

