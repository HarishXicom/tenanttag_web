<style>
    .ms {
    background: rgba(0, 0, 0, 0) none repeat scroll 0 0 !important;
    height: auto !important;
    width: 99% !important;
}
</style>
<section class="lease-part">
    <div class="container">
        <?php
        $all_vendors = $all_property = array();
        if (!empty($properties)) {
            foreach ($properties as $details) {
                if (!empty($details['vendors'])) {
                    foreach ($details['vendors'] as $vendor) {
                        // $name = $details['details']->city . ", " . $details['details']->address1 . " " . $details['details']->address2;
                        $all_vendors[] = $vendor;
                    }
                }
                $all_property[] = $details['details'];
            }
        }
        $all_service = get_vendor_services();
        $services = array();

        if (!empty($all_service)) {
            foreach ($all_service as $ser) {
                $services[$ser->t_id] = $ser->name;
            }
        }
        $property_drop = array();
        if (!empty($all_properties)) {
            foreach ($all_properties as $propty) {
                $name = $propty->city . ", " . $propty->address1 . " " . $propty->address2;
                $property_drop[$propty->prop_id] = $name;
            }
        }
        ?>



        <div id="tabs">

            <div class="my-tab1"> 
                <a href="javascript:void(0)" class="btn-tenant my-btn-tennt my-vendor"><i class="fa fa-plus"></i>Add Vendor</a>
                <ul class="my-tab">

                    <li class="active all-tabs-panel-li"><a href="#tabs-1" class="vendor">My Vendors</a></li>
                    <?php
                    if (!empty($all_properties)) {
                        //$k = 2;
                        ?>
                        <?php foreach ($all_properties as $all_prp) { ?>
                            <li class="all-tabs-panel-li"><a href="#tabs-<?php echo $all_prp->prop_id; ?>">
                                    <?php echo $all_prp->city . ", " . $all_prp->address1 . " " . $all_prp->address2; ?>
                                </a></li>
                            <?php
                            //$k++;
                        }
                        ?>
                    <?php } ?>
                </ul>
            </div>
            <div class="tab-area add_vndr_tab" style="display:none">
                <div class="table-content vendor-table">
                    <h2 class="my-txt" style="width:auto">Add Vendor</h2>
                    <!--<span class="addmore_vendor">+Add New</span>-->

                    <form id="add_vendors" class="select-grp">
                        <div class="vndr_form">
                            <div class="frm_row">
                                <table id="filters" class="tablesorter">
                                    <thead>
                                        <tr>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if (isset($property_drop) && !empty($property_drop)) { ?>
                                            <tr>
                                                <td>
                                                    <div class="event">
                                                        <p class="my-event-checkbox">
                                                            <input type="checkbox" class="tnt_check_all" id="test025">    <label for="test025">All</label> </p>
                                                    </div>
                                                </td>
                                            </tr>
                                            <?php  foreach($property_drop as $key=>$prp_check): ?>
                                                <tr>
                                                    <td>
                                                        <div class="event">
                                                            <p class="my-event-checkbox"> 
                                                                <input type="checkbox" class="tnt_check" name="prop_id[]" value="<?php echo $key; ?>" id="ptp_<?php echo $key; ?>">    
                                                                <label for="ptp_<?php echo $key; ?>"><?php echo ucwords($prp_check); ?></label> 
                                                            </p>
                                                        </div> 
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>

                                        <?php } else { ?>
                                        <div class="event">
                                            <p class="my-event-checkbox">No record found. </p>
                                        </div>
                                    <?php } ?>
                                    </tbody>
                                </table>
                                  <label class="error check_error" style="display:none;"></label>

                                <?php
                               // $lists = "id='sevice' class='form-control add_vender_form width50' required='1'";
                               // echo form_dropdown('service_id[]', $services, '', $lists);
                                ?>
                                <select id='sevice' multiple name="service_id[0][]" class='service_dropdown form-control add_vender_form width50' required='1'>
                                    <?php foreach ($services as $service=>$val) { ?>
                                        <option value="<?= $service ?>"><?= $val ?></option>
                                    <?php } ?>
                                </select>

                                <input type="text" name="cname[]" maxlength="50" class="my-txtfleid add_vender_form" placeholder="Company Name" required="true"/>
                                <input type="text" name="email[]" email="true" class="my-txtfleid add_vender_form" placeholder="Email" required="true"/>
                                <input type="text" name="phone[]" class="my-txtfleid add_vender_form phone_us" placeholder="Phone" required="true"/>

<!--                                <span  class="del-span">  <a class="del del_vendor_row" href="javascript:void(0)"><img src="images/cross.png" alt=""/></a></span>-->
                            </div>
                        </div>

                        <p class="select-grp">
                            
                            <input type="submit" class="btn-tenant" value="Submit"/>
                            <span class="note_vndr">*Please make sure that only 1 vendor is selected for each service category</span>
                        </p>

                    </form>
                </div>
            </div>
            <div class="tab-area">
                <div id="tabs-1">
                    <div class="table-content vendor-table">
                        <table class="my-table text-left">
                            <thead>
                                <tr> 
                                    <th>Service</th>
                                    <th>Company</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>

                                <?php if (!empty($summary)) { ?>
                                    <?php foreach ($summary as $vndr): ?>
                                        <tr class="hide_rec" style="display: none;">
                                            <td>
                                                <?php
                                                //$list = "id='sevice' class='form-control width50' required='1'";
                                                //echo form_dropdown('service', $services, $vndr->service, $list);
                                                ?>
                                                <?php $exist_services = explode(',', $vndr->service); ?> 
                                                <select id='sevice' multiple name="service_id[0][]" class='service_dropdown form-control add_vender_form width50' required='1'>
                                                    <?php foreach ($services as $service=>$val) { ?>
                                                            <?php if (in_array($service, $exist_services)) { ?>
                                                                <option value="<?= $service ?>" selected><?= $val ?></option>
                                                            <?php }else{ ?>
                                                                <option value="<?= $service ?>"><?= $val ?></option>
                                                            <?php } ?>
                                                    <?php } ?>
                                                </select>
                                            </td>
                                            <td><input type="text" name="cname" class="my-txtfleid" value="<?php echo $vndr->company_name; ?>"/></td>
                                            <td><input type="text" name="email" class="my-txtfleid" value="<?php echo $vndr->email; ?>"/></td>
                                            <td><input type="text" name="mobile" class="my-txtfleid phone_us" value="<?php echo $vndr->mobile_no; ?>"/></td>
                                            <td>
                                                <span class="edit-span"> <a class="downlod update_vendor" href="javascript:void(0);" id="<?php echo $vndr->vendor_id; ?>"></a></span>
                                                <span  class="del-span">  <a class="del del_vendor" href="javascript:void(0);"  id="<?php echo $vndr->vendor_id; ?>"></a></span>
                                            </td>   
                                        </tr>
                                        <tr class="show_rec">                                                             
                                            <td>
                                            <?php $exist_services = explode(',', $vndr->service); ?>
                                                <?php foreach ($services as $service=>$val) { ?>
                                                        <?php if (in_array($service, $exist_services)) { ?>
                                                                <?php echo $val .','; ?>
                                                        <?php } ?>
                                                <?php } ?>
                                            <?php //echo $services[$vndr->service]; ?>
                                            </td>
                                            <td><?php echo $vndr->company_name; ?></td>
                                            <td><?php echo $vndr->email; ?></td>
                                            <td><?php echo $vndr->mobile_no; ?></td>
                                            <td>
                                                <span class="edit-span"> <a class="edit edit_vendor" href="javascript:void(0);" id="<?php echo $vndr->vendor_id; ?>"></a></span>
                                                <span  class="del-span">  <a class="del del_vendor" href="javascript:void(0);"  id="<?php echo $vndr->vendor_id; ?>"></a></span>
                                            </td>
                                        </tr>

                                    <?php endforeach; ?>
                                <?php } else { ?>
                                    <tr>
                                        <td colspan="5">No record found</td>
                                    </tr>  
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>


                </div>
                <?php
                if (!empty($all_properties)) {
                    foreach ($all_properties as $prop) {
                        ?>
                        <div id="tabs-<?php echo $prop->prop_id; ?>">
                            
                            <?php if (isset($properties[$prop->prop_id])) {
                                foreach ($properties as $key => $details) {
                                    ?>
                <?php if ($key == $prop->prop_id) { ?>
                                        <div class="table-content">
                                            <table class="my-table text-left">
                                                <thead>
                                                    <tr> 
                                                        <th>Service</th>
                                                        <th>Company</th>
                                                        <th>Email</th>
                                                        <th>Phone</th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php if (!empty($details['vendors'])) { ?>
                                                    <?php foreach ($details['vendors'] as $vendr) { ?>
                                                            <tr class="hide_rec" style="display: none;">
                                                                <td>
                                                                    <?php
                                                                    //$list = "id='sevice' class='form-control width50' required='1'";
                                                                    //echo form_dropdown('service', $services, $vendr['service'], $list);
                                                                    ?>
                                                                    <?php $exist_services = explode(',', $vendr['service']); ?> 
                                                                    <select id='sevice' multiple name="service_id[0][]" class='service_dropdown form-control add_vender_form width50' required='1'>
                                                                        <?php foreach ($services as $service=>$val) { ?>
                                                                                <?php if (in_array($service, $exist_services)) { ?>
                                                                                    <option value="<?= $service ?>" selected><?= $val ?></option>
                                                                                <?php }else{ ?>
                                                                                    <option value="<?= $service ?>"><?= $val ?></option>
                                                                                <?php } ?>
                                                                        <?php } ?>
                                                                    </select>
                                                                </td>
                                                                <td><input type="text" name="cname" class="my-txtfleid" value="<?php echo $vendr['cname']; ?>"/></td>
                                                                <td><input type="text" name="email" class="my-txtfleid" value="<?php echo $vendr['email']; ?>"/></td>
                                                                <td><input type="text" name="mobile" class="my-txtfleid" value="<?php echo $vendr['mobile_no']; ?>"/></td>
                                                                <td>
                                                                    <span class="edit-span"> <a class="downlod update_vendor" href="javascript:void(0);" id="<?php echo $vendr['vendor_id']; ?>"></a></span>
                                                                    <span  class="del-span">  <a class="del del_vendor" href="javascript:void(0);"  id="<?php echo $vendr['vendor_id']; ?>"></a></span>
                                                                </td>   
                                                            </tr>
                                                            <tr class="show_rec">                                                             
                                                                <td>
                                                                <?php $exist_services = explode(',', $vendr['service']); ?>
                                                                    <?php foreach ($services as $service=>$val) { ?>
                                                                            <?php if (in_array($service, $exist_services)) { ?>
                                                                                    <?php echo $val .','; ?>
                                                                            <?php } ?>
                                                                    <?php } ?>
                                                                <?php //echo $services[$vndr->service]; ?>
                                                                </td>

                                                                <td><?php echo $vendr['cname']; ?></td>
                                                                <td><?php echo $vendr['email']; ?></td>
                                                                <td><?php echo $vendr['mobile_no']; ?></td>
                                                                <td>
                                                                    <span class="edit-span"> <a class="edit edit_vendor" href="javascript:void(0);" id="<?php echo $vendr['vendor_id']; ?>"></a></span>
                                                                    <span  class="del-span">  <a class="del del_vendor" href="javascript:void(0);"  id="<?php echo $vendr['vendor_id']; ?>"></a></span>
                                                                </td>   
                                                            </tr>

                                                        <?php } ?>
                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    <?php } ?>
                                <?php }
                            }else {
                            ?>
                             <div class="table-content">
                                    <table class="my-table text-left">
                                        <thead>
                                            <tr> 
                                                <th>Service</th>
                                                <th>Company</th>
                                                <th>Email</th>
                                                <th>Phone</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr><td colspan="5">No vendor's for this property</td></tr>
                                        </tbody>
                                    </table>
                                </div>    
                            <?php } ?>
                        </div>
                        <?php
                    }
                    ?>
                </div>
<?php } ?>


        </div>

</section>
<div class="cp_vndr_form" style="display: none;">
    <div class="frm_row">
        <?php
     //   $lists = "id='ms' multiple='multiple' class='ms form-control add_vender_form width50' required='1'";
     //   echo form_dropdown('prop_id[]', $property_drop, '', $lists);
        
        if(!empty($property_drop)){
            foreach($property_drop as $key=>$prp_check){ ?>
        <input type="checkbox" name="prop_id[]" value="<?php echo $key ?>"/>
        <?php echo $prp_check; ?>
            <?php }
        }
        ?>

        <?php
        $lists = "id='sevice' class='form-control add_vender_form  width50' required='1'";
        echo form_dropdown('service_id[]', $services, '', $lists);
        ?>

        <input type="text" name="cname[]" maxlength="50" class="my-txtfleid add_vender_form" placeholder="Company Name" required="true"/>
        <input type="email" name="email[]" class="my-txtfleid add_vender_form" placeholder="Email" email="true" required="true"/>
        <input type="text"  name="phone[]" class="my-txtfleid add_vender_form phone_us" placeholder="Phone"  required="true"/>

        <span  class="del-span">  <a class="del del_vendor_row" href="javascript:void(0)"></a></span>
    </div>
</div>
