<link href="http://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/css/bootstrap.min.css"
rel="stylesheet" type="text/css" />
<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/js/bootstrap.min.js"></script>
<link href="http://cdn.rawgit.com/davidstutz/bootstrap-multiselect/master/dist/css/bootstrap-multiselect.css"
rel="stylesheet" type="text/css" />
<script src="http://cdn.rawgit.com/davidstutz/bootstrap-multiselect/master/dist/js/bootstrap-multiselect.js"
type="text/javascript"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.5/css/bootstrap-dialog.min.css" rel="stylesheet" type="text/css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.5/js/bootstrap-dialog.min.js"></script>
<script>
    
    $(document).ready(function (){
        $('.service_dropdown').multiselect({
            includeSelectAllOption: true,
            selectAllText:'Select All',
            nonSelectedText: 'Select Service'
        });
       $(".add-vendor-row").click(function (){
           var cnt = $(".add-vendor-tbl").length;
           var imgcrossurl = '<?php echo $this->config->item("templateassets"); ?>';
           var content = '<tr class="add-vendor-tbl">'+
                     '<td style="font-size: 30px;"><img src="'+imgcrossurl+'images/cross.png" class="fa fa-times-circle-o faclosepointer" alt=""/></td>'+
                    '<td colspan="">'+
                        '<div class="my-div">'+
                            '<input class="form-control my-txt-fleid" required="true" type="text" placeholder="Company Name" name="company_name['+cnt+']" id="company_name'+cnt+'" >'+
                        '</div>'+
                    '</td>'+
                    '<td>'+
                        '<div class="my-div">'+
                            '<input class="form-control my-txt-fleid phone_us" required="true" placeholder="Company Phone #" type="text" name="company_contact['+cnt+']" id="company_contact'+cnt+'">'+
                        '</div>'+
                    '</td>'+
                    '<td>'+
                       ' <div class="my-div">'+
                            '<input class="form-control my-txt-fleid" type="email" required="true" placeholder="Company Email" name="email['+cnt+']" id="email'+cnt+'">'+
                       ' </div>'+
                   ' </td>'+
                    '<td>'+ 
                        '<select multiple class="form-control width100 service_dropdown" name="service['+cnt+'][]" required="true" id="service'+cnt+'"">'+
                            +
                            <?php foreach ($vendorServices as $vale) { ?>
                                '<option value="<?= $vale->t_id ?>"><?= $vale->name ?></option>'+
                            <?php } ?>
                        '</select>'+
                    '</td>'+

                '</tr>';
           //var content = $(".add-vendor-tbl").html();
           $('#add-vendor-row').before(content);
           $('.service_dropdown').multiselect({
                includeSelectAllOption: true,
                selectAllText:'Select All',
                nonSelectedText: 'Select Service'
            });
       }); 
       
       $(document).delegate('.fa-times-circle-o','click',function(){
          $(this).parent().parent().remove(); 
       });
    });
    
    $('.save-for-later').on('click', function () {
        $('.popup-block').show();
        $('.save-later').show();
        $('.action-type').val('save_later');
    });

    $('.save-for-later-account-setup').on('click', function () {
        $('.popup-block').show();
        $('.save-later-account-setup').show();
        $('.action-type').val('save-later-account-setup');
    });

    

    $('.add-another-property').on('click', function () {
        if (!$('.tMessaging').is(':checked'))
        {
            $('.is_tMessaging').html('');
            $('.is_tMessaging').html('tText Inactive');
            $('.is_tMessaging').addClass('my-cross');
        }
        else
        {
            $('.is_tMessaging').html('');
            $('.is_tMessaging').html('tText Active');
            $('.is_tMessaging').removeClass('my-cross');
        }
        if (!$('.tMaintenace').is(':checked'))
        {
            $('.is_tMaintence').html('');
            $('.is_tMaintence').html('tMaintence Inactive');
            $('.is_tMaintence').addClass('my-cross');
        }
        else
        {
            $('.is_tMaintence').html('');
            $('.is_tMaintence').html('tMaintence Active');
            $('.is_tMaintence').removeClass('my-cross');
        }
        if (!$('.tPay').is(':checked'))
        {
            $('.is_tPay').html('');
            $('.is_tPay').html('is_tPay Inactive');
            $('.is_tPay').addClass('my-cross');
        }
        else
        {
            $('.is_tPay').html('');
            $('.is_tPay').html('is_tPay Active');
            $('.is_tPay').removeClass('my-cross');
        }
        
        $('.popup-block').show();
        $('.next-step').show();
        $('.action-type').val('add-more-property');
    });

    $('.save-property').on('click', function () {
        if (!$('.tMessaging').is(':checked'))
        {
            $('.is_tMessaging').html('');
            $('.is_tMessaging').html('tText Incomplete');
            $('.is_tMessaging').addClass('my-cross');
        }
        else
        {
            $('.is_tMessaging').html('');
            $('.is_tMessaging').html('tText Complete');
            $('.is_tMessaging').removeClass('my-cross');
        }
        if (!$('.tMaintenace').is(':checked'))
        {
            $('.is_tMaintence').html('');
            $('.is_tMaintence').html('tMaintence Incomplete');
            $('.is_tMaintence').addClass('my-cross');
        }
        else
        {
            $('.is_tMaintence').html('');
            $('.is_tMaintence').html('tMaintence Complete');
            $('.is_tMaintence').removeClass('my-cross');
        }
        if (!$('.tPay').is(':checked'))
        {
            $('.is_tPay').html('');
            $('.is_tPay').html('tPay Information Incomplete');
            $('.is_tPay').addClass('my-cross');
        }
        else
        {
            $('.is_tPay').html('');
            $('.is_tPay').html('tPay Active');
            $('.is_tPay').removeClass('my-cross');
        }
        $('.popup-block').show();
        $('.next-step').show();
        $('.action-type').val('save');
    });

    //$('.save_property_checkout').click(function () {
    $(document).on('click', '.save_property_checkout', function () {
        $('.popup-block').hide();
        $('.vendor-detail').submit();
    });

    $(document).on('click', '#add-vendor', function () {
        $('#add-vendor-row').show();
    });

    $(document).on('click', '.edit-record', function () {
        var value = $(this).attr('data-value');
        $('#edit_row_' + value).show();
        $('#display_row_' + value).hide();
    });

    $(document).on('click', '.cancel', function () {
        var value = $(this).attr('data-value');
        $('#edit_row_' + value).hide();
        $('#display_row_' + value).show();
        $(".vendor-detail").trigger('reset');
    });

    $(document).on('click', '.reset', function () {
        $('#add-vendor-row').hide();
        $(".vendor-detail").trigger('reset');
    });

    $(document).on('click', '.delete-record', function () {
        var value = $(this).attr('data-value');
        deleteRecord(value);
    });

    $(document).on('click', '.add-record', function () {
        add_vendor();
    });

    $(document).on('click', '.add-vendor-to', function () {
        var id = $(this).attr('data-value');
        //$(this).parent().parent().hide();
        add_vendor_to(id);
    });

    $(document).ready(function () {
        $(".tnt_check_all").change(function () {
            $(".tnt_check").prop('checked', $(this).prop("checked"));
        });
    });
    $(document).on('click', '#sendeTesttText', function () {
        var posturl = siteUrl + 'property/send_test_etext_message';
        $.ajax({
                url: posturl,
                dataType: 'json',
                type: "POST",
                beforeSend: function () {
                    $('#wait-div').show();
                },
                success: function (response) {
                    $('.vendor-detail').find('.ajax_report').removeClass('alert-success').removeClass('alert-danger').fadeIn(200);
                    $('#wait-div').hide();
                    if (response.success == true)
                    {
                        $('.vendor-detail').find('.ajax_report').addClass('alert-success').children('.ajax_message').html(response.msg);
                    }else{
                        $('.vendor-detail').find('.ajax_report').addClass('alert-danger').children('.ajax_message').html(response.msg);
                    }
                    /*if (response.scrollToElement)
                        scrollToElement('.ajaxForm', 1000);
                    setTimeout(function () {
                        $('.ajaxForm').find('.ajax_report').fadeOut(500);
                    }, 1000);*/
                },
                error: function (data) {
                    alert("Server Error.");
                    return false;
                }
            });
    });

    $(document).on('click', '#sendTesttMaintenanceEmail', function () {
        var posturl = siteUrl + 'property/send_test_tmaintenance_email';
        $.ajax({
                url: posturl,
                dataType: 'json',
                type: "POST",
                beforeSend: function () {
                    $('#wait-div').show();
                },
                success: function (response) {
                    $('.vendor-detail').find('.ajax_report').removeClass('alert-success').removeClass('alert-danger').fadeIn(200);
                    $('#wait-div').hide();
                    if (response.success == true)
                    {
                        $('.vendor-detail').find('.ajax_report').addClass('alert-success').children('.ajax_message').html(response.msg);
                    }else{
                        $('.vendor-detail').find('.ajax_report').addClass('alert-danger').children('.ajax_message').html(response.msg);
                    }
                    /*if (response.scrollToElement)
                        scrollToElement('.ajaxForm', 1000);
                    setTimeout(function () {
                        $('.ajaxForm').find('.ajax_report').fadeOut(500);
                    }, 1000);*/
                },
                error: function (data) {
                    alert("Server Error.");
                    return false;
                }
            });
    });


    
    function deleteRecord(id)
    {
        var posturl = siteUrl + 'delete-vendor/' + id;
        var sessionId = '<?= $this->session->userdata('MEM_ID') ?>';

        if (id != '' && sessionId != '')
        {

            $.ajax({
                url: posturl,
                dataType: 'json',
                type: "POST",
                beforeSend: function () {
                    $('#wait-div').show();
                },
                success: function (response) {
                    $('.ajaxForm').find('.ajax_report').removeClass('alert-success').removeClass('alert-danger').fadeIn(200);
                    $('#wait-div').hide();
                    if (response.success)
                    {
                        $('#edit_row_' + id).remove();
                        $('#display_row_' + id).remove();
                        $('.ajaxForm').find('.ajax_report').addClass('alert-success').children('.ajax_message').html(response.success_message);
                    }
                    if (response.scrollToElement)
                        scrollToElement('.ajaxForm', 1000);
                    setTimeout(function () {
                        $('.ajaxForm').find('.ajax_report').fadeOut(500);
                    }, 1000);
                },
                error: function (data) {
                    alert("Server Error.");
                    return false;
                }
            });
        }
        else
        {
            $.post(posturl, {id: value, vote: task});
            $("#log-in").trigger("click");
        }
        return false;
    }

    function update(id)
    {
        var posturl = siteUrl + 'update-vendor/' + id;
        var sessionId = '<?= $this->session->userdata('MEM_ID') ?>';

        if (id != '' && sessionId != '')
        {
            var company_name = $('#company_name_' + id).val();
            var company_contact = $('#company_contact_' + id).val();
            var email = $('#email_' + id).val();
            var service = $('#service_' + id).val();

            $.ajax({
                url: posturl,
                data: {company_name: company_name, company_contact: company_contact, email: email, service: service},
                dataType: 'json',
                type: "POST",
                beforeSend: function () {
                    $('#wait-div').show();
                },
                success: function (data) {
                    $('#wait-div').hide();
                    $('.ajaxForm').find('.ajax_report').removeClass('alert-success').removeClass('alert-danger').fadeIn(200);
                    if (data.success)
                    {
                        $('.ajaxForm').find('.ajax_report').addClass('alert-success').children('.ajax_message').html(data.success_message);
                        $('#display_row_' + id).html('');
                        var dataToAppend = '<td>' + data.row.company_name + '</td>';
                        dataToAppend += '<td>' + data.row.mobile_no + '</td>';
                        dataToAppend += '<td>' + data.row.email + '</td>';
                        dataToAppend += '<td><select class="form-control " disabled><option value="">' + data.service_name + '</option></select></td>';
                        dataToAppend += '<td><a class="btn-tenant edit-record" style="color:#ffffff;width:38px;" href="javascript:;" title="Edit" data-value="' + id + '"><i class="fa fa-pencil"></i></a><a class="btn-tenant delete-record" style="color:#ffffff;width:38px;margin-left:2px" href="javascript:;" title="Delete" data-value="' + id + '"><i class="fa fa-trash"></i></a></td>';
                        $('#display_row_' + id).html(dataToAppend);
                        $('#edit_row_' + id).hide();
                        $('#display_row_' + id).show();
                    }
                    else
                    {
                        $('.ajaxForm').find('.ajax_report').addClass('alert-danger').children('.ajax_message').html(data.error_message);
                    }
                    if (data.scrollToElement)
                        scrollToElement('.ajaxForm', 1000);
                    setTimeout(function () {
                        $('.ajaxForm').find('.ajax_report').fadeOut(500);
                    }, 1000);
                },
                error: function (data) {
                    alert("Server Error.");
                    return false;
                }
            });
        }
        else
        {
            $.post(posturl, {id: value, vote: task});
            $("#log-in").trigger("click");
        }
        return false;
    }

    function add_vendor()
    {
        var posturl = siteUrl + 'add-vendor';
        var sessionId = '<?= $this->session->userdata('MEM_ID') ?>';

        if (sessionId != '')
        {
            var company_name = $('#company_name').val();
            var company_contact = $('#company_contact').val();
            var email = $('#email').val();
            var service = $('#service').val();

            $.ajax({
                url: posturl,
                data: {company_name: company_name, company_contact: company_contact, email: email, service: service},
                dataType: 'json',
                type: "POST",
                beforeSend: function () {
                    $('#wait-div').show();
                },
                success: function (data) {
                    $('#wait-div').hide();
                    $('.ajaxForm').find('.ajax_report').removeClass('alert-success').removeClass('alert-danger').fadeIn(200);
                    if (data.success)
                    {
                        $('.ajaxForm').find('.ajax_report').addClass('alert-success').children('.ajax_message').html(data.success_message);
                        var dataToAppend = '<tr id="display_row_' + data.row.v_id + '">';
                        dataToAppend += '<input type="hidden" name="add-to-property[]" value="' + data.row.v_id + '" />';
                        dataToAppend += '<td>' + data.row.company_name + '</td>';
                        dataToAppend += '<td>' + data.row.mobile_no + '</td>';
                        dataToAppend += '<td>' + data.row.email + '</td>';
                        dataToAppend += '<td><select class="form-control " disabled><option value="">' + data.service_name + '</option></select></td>';
                        dataToAppend += '<td><a class="btn-tenant edit-record" style="color:#ffffff;width:38px;" href="javascript:;" title="Edit" data-value="' + data.row.v_id + '"><i class="fa fa-pencil"></i></a><a class="btn-tenant delete-record" style="color:#ffffff;width:38px;" href="javascript:;" title="Delete" data-value="' + data.row.v_id + '"><i class="fa fa-trash"></i></a></td>';
                        dataToAppend += '</tr>';

                        dataToAppend += '<tr id="edit_row_' + data.row.v_id + '" style="display:none;">';
                        dataToAppend += '<td><div class="my-div">';
                        dataToAppend += '<input class="form-control my-txt-fleid" type="text" id="company_name_' + data.row.v_id + '" value="' + data.row.company_name + '">';
                        dataToAppend += '</div></td>';
                        dataToAppend += '<td><div class="my-div">';
                        dataToAppend += '<input class="form-control my-txt-fleid phone_us" type="text" id="company_contact_' + data.row.v_id + '" value="' + data.row.mobile_no + '">';
                        dataToAppend += '</div></td>';
                        dataToAppend += '<td><div class="my-div">';
                        dataToAppend += '<input class="form-control my-txt-fleid" type="email" id="email_' + data.row.v_id + '" value="' + data.row.email + '">';
                        dataToAppend += '</div></td>';
                        dataToAppend += '<td><select class="form-control " id="service_' + data.row.v_id + '"><option value="">Select Service</option>';
                        var selected = '';
                        $.each(data.vendorServices, function (key, value) {
                            if (value.t_id == data.row.name)
                                selected = 'selected';
                            dataToAppend += '<option value="' + value.t_id + '" selected="' + selected + '">' + value.name + '</option>';
                        });
                        dataToAppend += '</select></td>';
                        dataToAppend += '<td><a class="btn-tenant" style="color:#ffffff;width:38px;" href="javascript:;" title="Save" onClick="update(' + data.row.v_id + ')"><i class="fa fa-floppy-o"></i></a><a class="btn-tenant cancel" style="color:#ffffff;width:38px;" href="javascript:;" title="Cancel" data-value="' + data.row.v_id + '"><i class="fa fa-times"></a></td>';
                        dataToAppend += '</tr>';
                        //$('#add-vendor-row').hide();
                        $('.all-records').prepend(dataToAppend);
                        $('#company_name').val('');
                        $("#company_contact").val('');
                        $("#email").val('');
                        $("#service").val('');
                        // $(".vendor-detail").trigger('reset');
                    }
                    else
                    {
                        $('.ajaxForm').find('.ajax_report').addClass('alert-danger').children('.ajax_message').html(data.error_message);
                    }
                    if (data.scrollToElement)
                        scrollToElement('.ajaxForm', 1000);
                    setTimeout(function () {
                        $('.ajaxForm').find('.ajax_report').fadeOut(500);
                    }, 1000);
                },
                error: function (data) {
                    alert("Server Error.");
                    return false;
                }
            });
        }
        else
        {
            $.post(posturl, {id: value, vote: task});
            $("#log-in").trigger("click");
        }
        return false;
    }

    function add_vendor_to(id)
    {
        // var id = $(this).attr('id');
        var posturl = siteUrl + 'vendors/assign_vendor';
        var sessionId = '<?= $this->session->userdata('MEM_ID') ?>';
        var i = 0;
        if (sessionId != '')
        {
            var vendors = [];
            $('.tnt_check').each(function (i) { //loop through each checkbox
                if ($(this).is(':checked')) {
                    vendors[i] = $(this).val();
                    i++;
                }
            });
            $.ajax({
                url: posturl,
                data: {vendors: vendors, prp_id: $(".prp_id").val()},
                type: "POST",
                
                beforeSend: function () {
                    $('#wait-div').show();
                },
                success: function (data) {
                    $('#wait-div').hide();
                    var myArray = JSON.parse(data);
                        if (myArray.success) {
                            swal({
                                title: "Sucess",
                                text: myArray.success_message,
                                type: "success"
                            },
                            function () {
                                location.reload();
                            });
                        } else {
                            swal("Error!", myArray.success_message, "error");
                        }
                },
                error: function (data) {
                    alert("Server Error.");
                    $('#wait-div').hide();
                    return false;
                }
            });
        }
        else
        {
            $.post(posturl, {id: value, vote: task});
            $("#log-in").trigger("click");
        }
        return false;
    }

</script>
<style>
    .vendor_ids_check{
        visibility:hidden;
    }
</style>	
