<div class="level">
    <div class="container">
        <div class="level-indicator">
            <img src="<?= $this->config->item('templateassets') ?>images/level-1.png" alt="" />
            <ul>
                <li  class="blue-col1"> Property Info</li>
                <li>Tenants</li>
                <li >tServices</li>
            </ul>
        </div>
    </div>
    <div class="my-popup save-later incomplete_pop" style="display:none;">
        <div class="popup-cont">
            <div class="popup-head">
                <span class="close"></span>
            </div>
            <div class="popup-body">

                <h2 class="alert_incomlete">There is/are properties with pending information, would you like to complete those?</h2></li>

                <ul class="move-list">
                    <?php if (!empty($incomplete)) { ?>
                        <script>
                            $(document).ready(function () {
                                $(".my-popup").show();
                            });
                        </script>
                        <?php foreach ($incomplete as $inactive): ?>
                            <li>  
                                <p> 
                                    <?php if($inactive->step_completed==2){ ?>
                                        <a href="<?php echo base_url() ?>add-property-step<?= $inactive->step_completed+1 ?>?propid=<?= $inactive->prop_id ?>">

                                        <?= $inactive->address1 ?> <?= $inactive->address2 ?>, <?= $inactive->city ?> <?= $inactive->region_name ?> <?= $inactive->zip ?>
                                    </a>  
                                    <?php }else{ ?>
                                     <a href="<?php echo base_url() ?>update-property-step<?= $inactive->step_completed+1 ?>?propid=<?= $inactive->prop_id ?>">

                                        <?= $inactive->address1 ?> <?= $inactive->address2 ?>, <?= $inactive->city ?> <?= $inactive->region_name ?> <?= $inactive->zip ?>
                                    </a>  
                                    <?php } ?>
                                    


                                </p>
                            </li>
                        <?php endforeach; ?>
                    <?php } ?>
                </ul>
            </div>
            <div class="popup-footer">
                <div class="center">
                </div>
            </div>
        </div>
    </div>
</div> 

<section class="signup-section2">

    <div class="container">

        </br></br></br></br></br>
        <?php echo form_open('', array('class' => '', 'id' => 'property-add-step1')) ?>
        <div class="ajax_report alert display-hide" role="alert" style=" margin-top: 100px; margin-bottom:10px;width:1169px; "><span class="close-message"></span><div class="ajax_message">Hello Message</div></div>
        <div >
            <div class="my-panel mr-btm0" >
                <h3 class="panel-head">
                    <div class="panel-title">
                        <h1>Property</h1>
                    </div>
                </h3>
                <div class="panel-body propertfirst">
                    <div  class="form-group1">
                        <div class="my-div">
                            <select class="form-control my-txt-fleid" name="property_type">
                                <option value="">Select Property Type</option>
                                <option value="SFR">Single Family Home</option>
                                <option value="Condo">Condominium</option>
                                <option value="Apt">Apartment Building</option>
                            </select>
                        </div>
                        <div class="my-div"><input type="text" class="form-control my-txt-fleid" placeholder="Address 1" name="address1"/></div>
                        <div class="my-div"><input type="text" class="form-control my-txt-fleid " placeholder="Address 2" name="address2"/></div>
                        <input type="hidden" name="country" value="223"/>
                    </div>
                    <div class="form-group1">
                        <div class="my-div">
                            <div class="my-col">
                                <input type="text" class="form-control my-txt-fleid width50 right-pull mr-rite0 " placeholder="Unit" name="unit"/>
                            </div>
                            <div class="my-col right-pull mr-rite0">
                                <input type="text" class="form-control my-txt-fleid " placeholder="CIty" name="city"/>
                            </div>


                        </div>
                        <div class="my-div">

                            <div class="mr-rite0">
                                <?php
                                $data = array();
                                $states = get_states('223');
                                if (!empty($states)) {
                                    foreach ($states as $state) {
                                        $data[$state->region_id] = $state->region_name;
                                    }
                                }
                                $list = "id='state'  class='form-control my-txt-fleid' required='1'";
                                echo form_dropdown('state', $data, $state_id, $list);
                                ?> 
                            </div>
                        </div>

                        <div class="my-div">
                            <div class="my-col">
                                <input type="text" class="form-control my-txt-fleid   " number="true" digits="true" maxlength="7"  placeholder="Zip" name="zip"/>
                            </div>
                            <div class="my-col right-pull mr-rite0">
                                <input type="file" class="browse-btn property_img" name="profile_pic"> 
                                <button class="upload-btn my-btn">Upload Photo</button>
                               
                            </div>
                             
                        </div>
                         <span class="show_file_name show_prp_img"></span>
                    </div>

                    <div class="my-div custom-width">
                        <label><b>Property in Community</b></label>
                        <p class="my-radio ">  <input type="radio" id="radio10" name="is_community" class="is_community" value="Yes"/>  <label for="radio10">Yes</label></p>
                        <p class="my-radio"> <input type="radio" id="radio11" name="is_community" class="is_community" checked="true" value="No"/> <label for="radio11">No</label></p>
                  
                    </div>
                    <div class="community_box" style="display:none">
                        <div class="form-group1">
                            <div class="my-div"><input type="text" class="form-control my-txt-fleid " placeholder="Community" name="community" required></div>
                            <div class="my-div">
                                <div class="my-col width50">
                                    <input type="text" class="form-control my-txt-fleid  " placeholder="Gate Code" name="gate_code"/>
                                </div>
                                <div class="my-col width50 right-pull mr-rite0">
                                    <input type="text" class="form-control my-txt-fleid " placeholder="Unit PO Box #" name="po_box"/>
                                </div>
                            </div>
                            
                        </div>
                        <div class="form-group1">
                            <div class="my-div"><input type="text" class="form-control my-txt-fleid" placeholder="Community Mng Company" name="community_company"/></div>
                            <div class="my-div"><input type="text" class="form-control my-txt-fleid phone_us" placeholder="Contact Number" name="company_number"/></div>
                            <div class="my-div">

                                <span class="upload-btn" style="width:255px;">Upload Community Docs
                                    <input type="file" name="comm_docs" class="comm_docs"></span>
                            <span class="show_file_name2"></span>
                            </div>
                            
                        </div>
                    </div>	 
                </div>

            </div>
            <div class="my-panel" >
                <h3 class="panel-head">
                    <div class="panel-title">
                        <h1>Amenities</h1>
                    </div>
                </h3>

                <div class="panel-body">
                    <div  class="form-group1">
                        <div class="my-div"><input type="text" class="form-control my-txt-fleid" disabled="true" placeholder="Garbage Pick-up Day" readonly/></div>
                        <div class="my-div">
                            <select class="form-control my-txt-fleid width50" name="garbage_pick_day">
                                <option value="N/A">N/A</option>
                                <option value="Sunday">Sunday</option>
                                <option value="Monday">Monday</option>
                                <option value="Tuesday">Tuesday</option>
                                <option value="Wednesday">Wednesday</option>
                                <option value="Thursday">Thursday</option>
                                <option value="Friday">Friday</option>
                                <option value="Saturday">Saturday</option>

                            </select>
                        </div>
                    </div>
                    <div  class="form-group1">
                        <div class="my-div"><input type="text" class="form-control my-txt-fleid" disabled="true" placeholder="Recycle Pick-up Day" readonly/></div>
                        <div class="my-div">
                            <select class="form-control my-txt-fleid width50" name="recycle_pick_day">
                                <option value="N/A">N/A</option>
                                <option value="Sunday">Sunday</option>
                                <option value="Monday">Monday</option>
                                <option value="Tuesday">Tuesday</option>
                                <option value="Wednesday">Wednesday</option>
                                <option value="Thursday">Thursday</option>
                                <option value="Friday">Friday</option>
                                <option value="Saturday">Saturday</option>

                            </select>
                        </div>
                    </div>
                    <div  class="form-group1">
                        <div class="my-div"><input type="text" class="form-control my-txt-fleid" disabled="true" placeholder="Yard Waste Pick-up Day" readonly/></div>
                        <div class="my-div">
                            <select class="form-control my-txt-fleid width50" name="yard_waste_pick_day">
                                <option value="N/A">N/A</option>
                                <option value="Sunday">Sunday</option>
                                <option value="Monday">Monday</option>
                                <option value="Tuesday">Tuesday</option>
                                <option value="Wednesday">Wednesday</option>
                                <option value="Thursday">Thursday</option>
                                <option value="Friday">Friday</option>
                                <option value="Saturday">Saturday</option>

                            </select>
                        </div>
                    </div>
                    <div  class="form-group1 ">
                        <div class="my-div"><input type="text" class="form-control my-txt-fleid" disabled="true" placeholder="Smoke Detectors" readonly/></div>
                        <div class="my-div">
                            <select class="form-control my-txt-fleid width50" name="safety_equipment1">
                                <option value="N/A">N/A</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>

                            </select>
                        </div>
                    </div>
                    <div  class="form-group1">
                        <div class="my-div"><input type="text" class="form-control my-txt-fleid" disabled="true" placeholder="Fire Extinguishors" readonly/></div>
                        <div class="my-div">
                            <select class="form-control my-txt-fleid width50" name="safety_equipment2">
                                <option value="N/A">N/A</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>

                            </select>
                        </div>
                    </div>
                    <div  class="form-group1">
                        <div class="my-div"><input type="text" class="form-control my-txt-fleid" disabled="true" placeholder="Heat Electric or Heat Furnace" readonly/></div>
                        <div class="my-div">
                            <select class="form-control my-txt-fleid width50" name="heat_type">
                                <option value="N/A">N/A</option>
                                <option value="Central">Central</option>
                                <option value="Furnace">Furnace</option>
                                <option value=" Room">Room</option>

                            </select>
                        </div>
                        <div class="my-div">
                            <select class="form-control my-txt-fleid width50"  name="heat_filter_size" style="display:none;float: left">
                                <option>N/A</option>
                                <option>20x20x1</option>
                                <option>20x25x1</option>
                                <option>16x20x1</option>
                                <option>16x25x1</option>
                                <option>14x25x1</option>
                                <option>12x12x1</option>
                                <option>12x16x1</option>
                                <option>12x18x1</option>
                                <option>Other</option>
                            </select>
                            <input type="text" name="heat_filter_size_other" class="form-control my-txt-fleid filtersize" required="true" style="display:none;width: 90px;float: left"/>

                        </div>
                    </div>
                    <div  class="form-group1">
                        <div class="my-div"><input type="text" disabled="true" class="form-control my-txt-fleid" placeholder="AC Central or AC Room" readonly/></div>
                        <div class="my-div">
                            <select class="form-control my-txt-fleid width50" name="ac_type">
                                <option value="N/A">N/A</option>
                                <option value="Central">Central</option>
                                <option value="Room">Room</option>
                                <option value=" Window">Window</option>
                            </select>
                        </div>
                        <div class="my-div">
                            <select class="form-control my-txt-fleid width50" name="ac_filter_size" style="display:none;float: left">
                                <option>N/A</option>
                                <option>20x20x1</option>
                                <option>20x25x1</option>
                                <option>16x20x1</option>
                                <option>16x25x1</option>
                                <option>14x25x1</option>
                                <option>12x12x1</option>
                                <option>12x16x1</option>
                                <option>12x18x1</option>
                                <option>Other</option>
                            </select>
                            <input type="text" name="ac_filter_size_other" class="form-control my-txt-fleid filtersize" required="true" style="display:none;width: 90px;float: left"/>
                        </div>
                        <?php foreach ($allAmenties as $value) { ?>
                            <div  class="form-group1">
                                <div class="my-div"><input type="text" disabled="true" class="form-control my-txt-fleid" placeholder="<?= $value->name ?>" readonly/></div>
                                <div class="my-div">
                                    <p class="my-radio">  <input type="radio" checked="true" id="radio01_<?= $value->id ?>" name="<?= $value->name ?>" value="<?= $value->id ?>"/>  <label for="radio01_<?= $value->id ?>">Yes</label></p>
                                    <p class="my-radio"> <input type="radio" id="radio02_<?= $value->id ?>" name="<?= $value->name ?>" value=""/> <label for="radio02_<?= $value->id ?>">No</label></p>
                                </div>
                            </div>
                        <?php } ?>
                        <div  class="form-group1">
                            <div class="my-div"><input type="text" disabled="true" class="form-control my-txt-fleid" placeholder="Pest control home" readonly/></div>
                            <div class="my-div" style="width: 15%;">
                                <p class="my-radio">  <input type="radio" id="radio01_pest_control_home" checked="true" name="pest_control_home" value="yes"/>  <label for="radio01_pest_control_home">Yes</label></p>
                                <p class="my-radio"> <input type="radio" id="radio02_pest_control_home"  name="pest_control_home" value="no"/> <label for="radio02_pest_control_home">No</label></p>
                            </div>
                            <div class="my-div service-provider">
                                <div class="my-col width50">
                                    <input type="text" class="form-control my-txt-fleid "  placeholder="Company" name="pest_service_provider"/>
                                </div>
                                <div class="my-col width50 right-pull mr-rite0">
                                    <input type="text" class="form-control my-txt-fleid phone_us"  placeholder="Contact no. " name="pest_service_provider_number"/>
                                </div>
                            </div>
                        </div>
                        <div  class="form-group1">
                            <div class="my-div"><input type="text" class="form-control my-txt-fleid" disabled="true" placeholder="Yard" readonly/></div>
                            <div class="my-div" style="width:147px;">
                                <p class="my-radio">  <input type="radio" id="radio01_yard" checked="true"  name="yard" value="yes" onClick="show_service_provided(this.value)"/>  <label for="radio01_yard">Yes</label></p>
                                <p class="my-radio"> <input type="radio" id="radio02_yard" name="yard" value="no"  onClick="show_service_provided(this.value)"/> <label for="radio02_yard">No</label></p>
                            </div>
                            <div style="float: left; padding: 10px;" class="is_yard_service_provide drnext">  <span>Is service included</span></div>



                            <div class="my-div is_yard_service_provide" style="width:147px; ">
                                <p class="my-radio">  <input type="radio" id="radio01_service_provided" name="yard_service_provided" value="yes"/>  <label for="radio01_service_provided">Yes</label></p>
                                <p class="my-radio"> <input type="radio" id="radio02_service_provided" name="yard_service_provided" checked="true" value="no"/> <label for="radio02_service_provided">No</label></p>
                            </div>

                            <div class="my-div is_yard_service_provide" style="">
                                <input type="text" class="form-control my-txt-fleid width50"  placeholder="Company" name="yard_service_provider"/>
                                <input type="text" class="form-control my-txt-fleid width50 right-pull mr-rite0 phone_us" placeholder="Contact no. " name="yard_service_provider_number"/>
                            </div>


                        </div>
                        <div class="my-panel   my-panel1 mr-btm0 morepanel" id="panel-4">
                            <div class="panel-head">
                                <div class="panel-title">
                                    <h1>More</h1>
                                </div>
                            </div>
                            <div class="panel-body">

                                <div class="already-services">
                                    <div  class="form-group1 already-service-no-0">
                                        <div class="my-div width150"><label>Select</label></div>
                                        <div class="my-div">
                                            <select  class="form-control my-txt-fleid" name="service[]" id="already-services-0">
                                                <option value="">Select Service</option>
                                                <?php foreach ($allServices as $value) { ?>
                                                    <option value="<?= $value->id ?>"><?= $value->service ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>	
                                        <div class="my-div ">
                                            <input type="text" class="form-control my-txt-fleid width50 " placeholder="Company" name="service_provider[]"/>
                                            <input type="text" class="form-control my-txt-fleid width50 right-pull mr-rite0 phone_us" placeholder="Contact no." name="service_provider_number[]"/>
                                        </div>
                                        <div class="my-div width150 ">
                                            <a href="javascript:;" class="delete-service " id="delete-service0" data-value="0" style="margin-left:35px;"><img src="<?= $this->config->item('templateassets') ?>images/cross.png" alt=""/></a>
                                        </div>	

                                    </div>
                                    <input type="hidden" name="already_services" class="already_services" value="0">
                                </div>
                                <div class="services-panel"></div>
                                <div  class="form-group1 new-services-0">
                                    <div class="my-div width150"><label>Create</label></div>
                                    <div class="my-div">
                                        <input type="text" class="form-control my-txt-fleid" placeholder=" Service" name="new_service_0"/>
                                    </div>
                                    <div class="my-div ">
                                        <input type="text" class="form-control my-txt-fleid width50" placeholder="Company" name="new_service_provider_0"/>
                                        <input type="text" class="form-control my-txt-fleid width50 right-pull mr-rite0 phone_us" placeholder="Contact no. " name="new_service_provider_number_0"/>
                                    </div>
                                    <div class="my-div width150 ">
                                        <a href="javascript:;" class="delete-row " id="cross0" data-value="0" style="margin-left:35px;"><img src="<?= $this->config->item('templateassets') ?>images/cross.png" alt=""/></a>
                                    </div>

                                </div>
                                <div class="new-btn-part">
                                    <a href="javascript:;" class="add-btn add-more-service btn-block" style="position:unset;width:235px;"><i class="fa fa-plus"></i> Select More Services</a>
                                    <a href="javascript:;" class="add-btn add-new-service btn-block" style="position:unset;width:235px;"><i class="fa fa-plus"></i> Create New Service </a>
                                </div>



                                <input type="hidden" value="0" name="new_created_services" class="new_created_services">
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
        <div class="center">  

            <input type="submit" class="next" value="NEXT "style=""/>
        </div>
        <?= form_close(); ?> 	
    </div>

</section>



<div class="end-block"></div>
<script>
    $(document).ready(function () {
        $('.add-more-service').on("click", function () {
            var assets = '<?= $this->config->item('templateassets') ?>';
            var already_services = $('.already_services').val();
            already_services = parseInt(already_services) + 1;
            $('.already_services').val(already_services);
            var allService = '<?= json_encode($allServices) ?>';
            allService = JSON.parse(allService);
            var dataToAppend = '';

            dataToAppend += '<div  class="form-group1 already-service-no-' + already_services + '"><div class="my-div width150"><label>Select</label></div>';
            dataToAppend += '<div class="my-div">';
            dataToAppend += '<select id="already-services-' + already_services + '" class="form-control my-txt-fleid" name="service[]">';
            dataToAppend += '<option value="">Select Service</option>';
            $.each(allService, function (key, value) {
                dataToAppend += '<option value="' + value.id + '">' + value.service + '</option>';
            });
            dataToAppend += '</select></div>	';
            dataToAppend += '<div class="my-div ">';
            dataToAppend += '<input type="text" class="form-control my-txt-fleid width50" placeholder="Company" name="service_provider[]"/>';
            dataToAppend += '<input type="text" class="form-control my-txt-fleid width50 right-pull mr-rite0 phone_us" placeholder="Contact no." name="service_provider_number[]"/>';
            dataToAppend += '</div>';
            dataToAppend += '<div class="my-div width150 "><a href="javascript:;" class="delete-service " id="delete-service' + already_services + '" data-value="' + already_services + '" style="margin-left:35px;"><img src="' + assets + 'images/cross.png" alt=""/></a></div></div>';

            $('.already-services').append(dataToAppend);
        });
    });
</script>
<style>
    .upload-btn > input {
        bottom: 0;
        cursor: pointer;
        font-size: 0;
        left: 0;
        opacity: 0;
        position: absolute;
        right: 0;
        top: 0;
        width: 100%;
    }
    .panel-body.ui-accordion-content {height:auto !important;}
</style>
