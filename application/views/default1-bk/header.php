<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>TenantTag</title>
        <link rel="shortcut icon" href="<?php echo base_url() ?>/favicon.ico" type="image/x-icon">
        <link rel="icon" href="<?php echo base_url() ?>/favicon.ico" type="image/x-icon">
        <link href="<?= $this->config->item('templateassets') ?>css/font-awesome.css" rel="stylesheet">
        <link href="<?= $this->config->item('templateassets') ?>css/style.css" rel="stylesheet">
        <link href="<?= $this->config->item('templateassets') ?>js/sweetalert.css" rel="stylesheet">
        <link href="<?= $this->config->item('templateassets') ?>css/responsive.css" rel="stylesheet">
        <script type="text/javascript" src="<?= $this->config->item('templateassets') ?>js/jquery-1.11.0.js"></script>
        <script type="text/javascript" src="<?= $this->config->item('templateassets') ?>js/jquery.form.js"></script>
        <script type="text/javascript" src="<?= $this->config->item('templateassets') ?>js/formClass.js"></script>
        <script type="text/javascript" src="<?= $this->config->item('templateassets') ?>js/jquery.validate.min.js"></script>
        <script type="text/javascript" src="<?= $this->config->item('templateassets') ?>js/sweetalert.min.js"></script>
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300italic,300,400italic,600italic,600,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
        <link href="<?= $this->config->item('templateassets') ?>css/bootstrap-datetimepicker.css" rel="stylesheet">
        <script type="text/javascript" src="<?= $this->config->item('templateassets') ?>js/moment.js"></script>
        <script type="text/javascript" src="<?= $this->config->item('templateassets') ?>js/bootstrap-datetimepicker.min.js"></script>
        <script type="text/javascript" src="<?= $this->config->item('templateassets') ?>js/jquery.mask.js"></script>     


    </head>

    <body >
        <header class="my-header">
            <div class="container-fulid">
                <div class="dr-logo my-logo"><a href="<?= site_url('dashboard') ?>"><img src="<?= $this->config->item('uploads') ?>site_logo/<?= $this->config->item('site_logo') ?>" alt="logo"/></a></div>
                <?php if ($this->session->userdata('MEM_ID')) { ?>
                    <?php
                    $verification_status = $this->common_model->getSingleFieldFromAnyTable('verification_status', 'mem_id', $this->session->userdata('MEM_ID'), 'tbl_members');
                    if ($verification_status == 'No') {
                        ?>
                        <div class="my-alert" ><div class="alert alert-danger" role="alert" style="display:inline-block; margin:0 auto; width:100%"><div class="ajax_message" style="padding:0px;"><i class="fa fa-exclamation-triangle"></i>
                                    Please confirm your email address. tServices will not be active until confirmation is complete.</div></div></div>
                    <?php } ?>

                    <div class="right-header inner-header">
                        <ul class="profile">
                            <li><img src="<?= $this->config->item('templateassets') ?>images/user-pic.png" alt=""/></li>
                            <li class="my-drop"><a href="javascript:;">My Profile</a>
                                <ul class="my-dropdown">
                                    <!--
                                      <li><a href="<?= site_url('dashboard') ?>">Dashboard</a></li>
                                    -->
                                    <?php if ($selected_tab != 'first-property') { ?> 
                                        <li><a href="<?= site_url('account/settings') ?>">Settings</a></li>
                                    <?php } ?>
                                    <li><a href="<?= site_url('logout') ?>">Logout</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <?php if ($selected_tab != 'first-property') { ?>  
                    </div>

                    <div class="navbar">
                        <div class="container">
                            <nav>
                                <ul class="my-navbar">
                                    <li class="<?php if ($selected_tab == 'dashboard') { ?>active<?php } ?>"><a href="<?= site_url('dashboard') ?>">Dashboard</a></li>
                                    <li class="<?php if ($selected_tab == 'properties') { ?>active<?php } ?>"><a href="<?= site_url('my-properties') ?>">Properties</a></li>
                                    <li class="<?php if ($selected_tab == 'tenants') { ?>active<?php } ?>"><a href="<?= site_url('tenants') ?>">Tenants</a></li>
                                    <li class="<?php if ($selected_tab == 'leases') { ?>active<?php } ?>"><a href="<?= site_url('leases') ?>">Leases</a></li>
                                    <li class="<?php if ($selected_tab == 'messages') { ?>active<?php } ?>"><a href="<?= site_url('message') ?>"><span style="text-transform: lowercase;">t</span>Messages</a></li>
                                    <li class="<?php if ($selected_tab == 'vendors') { ?>active<?php } ?>"><a href="<?= site_url('vendors') ?>">Vendors</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                <?php
                }
            }
            ?>
        </header>
        <?php if ($this->session->userdata('MEM_ID')) { ?>
    <?php if (isset($tPayment) && $tPayment) { ?>  
                <div class="breadcrumb">
                    <div class="container">
                        <div class="heading">
                            <label>Welcome, <span><?= $this->common_model->getSingleFieldFRomAnyTable('first_name', 'mem_id', $this->session->userdata('MEM_ID'), 'tbl_members') ?>!</span></label>
                            <span><span> Survey Question</span></span>
                        </div>
                    </div>
                </div>
            <?php
            }
        }
        ?> 	
        <div id="wait-div"></div>
        <script>
            var siteUrl = '<?= site_url('') ?>';
        </script>
