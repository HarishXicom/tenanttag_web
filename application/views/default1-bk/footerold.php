<footer>
    <div class="container">
        <div class="fttr-block">
            <ul>
                <li><a href="#">Services   </a></li>
                <li><a href="#">Pricing</a></li>
                <li><a href="#">How it works</a></li>
                <li><a href="#">Stories</a></li>
                <li><a href="#">Blog</a></li>
            </ul>
        </div>
        <div class="fttr-block">
            <ul>
                <li><a href="#">Contact</a></li>
                <li><a href="#">FAQ</a></li>
                <li><a href="#">Privacy</a></li>
                <li><a href="#">Terms</a></li>
            </ul>
        </div>
        <div class="fttr-block">
            <h4>Subscribe</h4>
            <p>Occaecati cupiditate non provident, similique sunt in culpa qui officia.</p>
            <input type="text" class="dremail">
            <a href="#" class="subscribe">Subscribe</a>
        </div>	
        <div class="fttr-block">
            <h4>Connect</h4>
            <ul class="social-link">
                <li><a id="drfb" href="#"><i class="fa fa-facebook"></i></a></li>
                <li><a id="drtwt" href="#"><i class="fa fa-twitter"></i></a></li>
                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                <li><a id="drtube" href="#"><i class="fa fa-youtube"></i></a></li>
            </ul>
            <a href="#"><img src="<?= $this->config->item('templateassets') ?>images/fttr-logo.png" alt=""/></a>
            <p>© 2015 Tenant. All rights reserved.</p>
        </div>
    </div>
</footer>


<div class="popup-block" >
    <div class="my-popup save-later" style="display: none">
        <div class="popup-cont">
            <div class="popup-head">
                <span class="close"></span>
            </div>
            <div class="popup-body">
                <div class="center"><span class="t-icon"></span></div>

                <ul class="listing list-pop">
                    <li class="my-cross">Property Information Incomplete</li>
                </ul>
            </div>
            <div class="popup-footer">
                <div class="center">
                    <a href="javascript:;" class="back back-popup">Back</a>
                    <a href="javascript:;" class="save save-later-process" style="width:150px;"> SAVE FOR LATER</a>
                </div>
            </div>
        </div>
    </div>
    <div class="my-popup next-step" style="display: none">
        <div class="popup-cont">
            <div class="popup-head">
                <span class="close close-popup"></span>
            </div>
            <div class="popup-body">
                <h2 class="mr-top20">Click <span>checkout</span> and property is <?= $this->common_model->getSingleFieldFromAnyTable('occupied_status', 'prop_id', $this->session->userdata('PROPERTY_ID'), 'tbl_properties') ?></h2>
                <div class="center"><span class="t-icon"></span></div>

                <ul class="listing">
                    <li class="is_tMessaging">tMessaging Active</li> 	
                    <li class="is_tMaintence">tMaintence Active</li>
                </ul>
            </div>
            <div class="popup-footer">
                <div class="center">
                    <a href="javascript:;" class="back back-popup">Back</a>
                    <a href="javascript:;" class="checkout"> Checkout</a>

                </div>
            </div>
        </div>
    </div>
</div>


<!--
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
-->
<script src="<?= $this->config->item('templateassets') ?>js/jquery-ui.js" type="text/javascript"></script>
<script>
    $(document).ready(function () {
        $('.phone_us').mask('(000) 000-0000');
        $('.filtersize').mask('00X00X00');
        $(document).ready(function () {
            $(".minus").click(function () {
                $(".drproperty").slideToggle();
                $(this).toggleClass("drcollapse");
            });
            $(".secondminus").click(function () {
                $(".secondm").slideToggle();
                $(this).toggleClass("drcollapse");
            });


            $('.dash-pop').click(function () {
                $('body').addClass('overlay');
                $('.popup-block').toggle();

            });
            $('.my-drop').click(function () {
                $('.my-dropdown').toggle();
            })

            $('.checkout').click(function () {
                $('.popup-block').hide();
                $('.vendor-detail').submit();
            });

            $('.save-later-process').click(function () {
                $('.save-later').hide();
                $('.next-step').hide();
                $('.popup-block').hide();
                $('.vendor-detail').submit();
            });

            $('.close-popup').click(function (event) {
                $('.save-later').hide();
                $('.next-step').hide();
                $('.popup-block').hide();
            });

            $('.back-popup,.close').click(function (event) {
                $('.save-later').hide();
                $('.next-step').hide();
                $('.popup-block').hide();
            });
        });


        $(function () {
            $("#tabs").tabs();
        });
        //~ $(".togle").click(function(){
        //~ $(this).toggleClass("stop");
        //~ }); 
        $(".btn-part a").click(function () {

            $(this).toggleClass("active");
            $(this).siblings('a').toggleClass("unactive");
        });
        $('.close-message').click(function (event) {
            $('div .ajax_report').hide('slow');
            //$('div .ajax_report').removeClass('display-hide');
        });

    });



</script>
<script>
    function getStateByCountryId(con_id)
    {
        if (con_id)
        {
            var postUrl = siteUrl + 'getStateByCountryId/' + con_id;
            $.ajax({
                url: postUrl,
                dataType: 'json',
                success: function (response) {
                    $('.country_code').val(response.country_code);
                    $('.country_code').blur();
                    if (response.success)
                    {
                        $('#state').html('');
                        $('#regions').show('slow');
                        $('#state').append('<option value="">Select State</option>')
                        $.each(response.regions, function (key, value) {
                            $('#state').append('<option value="' + value.region_id + '">' + value.region_name + ' </option>');
                        });
                    }
                    else
                    {
                        $('#state').html('');
                        $('#state').append('<option value="">No state found under selected Country</option>');
                    }
                },
                error: function () {
                    alert('server error');
                },
            });
        }
    }

    function getCityByStateId(state_id)
    {
        if (state_id)
        {
            var postUrl = siteUrl + 'getCityByStateId/' + state_id;
            $.ajax({
                url: postUrl,
                dataType: 'json',
                success: function (response) {
                    if (response.success)
                    {
                        $('#city').html('');
                        $('#cities').show('slow');
                        $('#city').append('<option value="">Select City</option>')
                        $.each(response.cities, function (key, value) {
                            $('#city').append('<option value="' + value.city_id + '">' + value.name + ' </option>');
                        });
                    }
                    else
                    {
                        $('#city').html('');
                        $('#city').append('<option value="">No city found under selected State</option>');
                    }
                },
                error: function () {
                    alert('server error');
                },
            });
        }
    }

</script>
</body>
</html>
