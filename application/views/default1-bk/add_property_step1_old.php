
<!--
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
-->
<div class="level">
  <div class="container">
	  <div class="level-indicator">
		 <img src="<?=$this->config->item('templateassets')?>images/level-1.png" alt="" />
		 <ul>
			 <li  class="blue-col1"> Property Info</li>
			 <li>Tenants</li>
			   <li >tServices</li>
		 </ul>
	  </div>
  </div>
</div>

<section class="signup-section2">
	
	<div class="container">
		<h2 class="tenant-txt">Set up: Property 1</h2></br></br></br></br></br>
		<?php echo form_open('',array('class'=>'ajaxForm','id'=>'property-add-step1'))?>
		<div class="ajax_report alert display-hide" role="alert" style=" margin-top: 100px; margin-bottom:10px;width:1169px; "><span class="close-message"></span><div class="ajax_message">Hello Message</div></div>
		<div >
		<div class="my-panel"id="accordion" > 
<!--
			<h3 class="panel-head">
				<div class="panel-title ">
					<h1>Add Profile Picture</h1>
				</div>
			</h3>
			<div class="panel-body">
			    <ul class="upload-file">
				  <li class=""><input type="file" name="profile_pic"></li>
				</ul>
                            
			</div>
-->
        
			<h3 class="panel-head">
				<div class="panel-title">
					<h1>Property</h1>
				</div>
			</h3>
			<div class="panel-body">
				<div  class="form-group1">
					<div class="my-div">
						<select class="form-control my-txt-fleid" name="property_type">
							<option value="">Select Property Type</option>
                                <option value="SFR">Single Family Home</option>
                                <option value="Condo">Condominium</option>
                                <option value="Apt">Apartment Building</option>
						</select>
					</div>
					<div class="my-div"><input type="text" class="form-control my-txt-fleid" placeholder="Address 1" name="address1"/></div>
					<div class="my-div"><input type="text" class="form-control my-txt-fleid" placeholder="Address 2" name="address2"/></div>
				</div>
				<div class="form-group1">
					<div class="my-div">
						<input type="text" class="form-control my-txt-fleid width50" placeholder="Unit" name="unit"/>
						<input type="text" class="form-control my-txt-fleid width50 right-pull mr-rite0" placeholder="City" name="city"/>
					</div>
					<div class="my-div">
						<select class="form-control my-txt-fleid width50" name="state">
							<option value="">Select State</option>
							<?php foreach($allRegion as $value) { ?>
								<option value="<?=$value->region_id?>"><?=$value->region_name?></option>
							<?php } ?>
						</select>
						<input type="text" class="form-control my-txt-fleid width50 right-pull mr-rite0" placeholder="Zip" name="zip"/>
					</div>
					<div class="my-div">
						<input type="file" name="profile_pic" >
					</div>	
				</div>
<!--
				<div class="form-group1 ">
					<div class="my-div">
						<input type="text" class="form-control my-txt-fleid width50" placeholder="Bedrooms" name="bedrooms"/>
						<input type="text" class="form-control my-txt-fleid width50 right-pull mr-rite0" placeholder="Bathrooms" name="bathrooms"/>
					</div>
					<div class="my-div">
						<select class="form-control my-txt-fleid width50" name="parking">
							<option value="">Parking</option>
							<option value="Yes">Yes</option>
							<option value="No">No</option>
						</select>
						<input type="text" class="form-control my-txt-fleid width50 right-pull mr-rite0" placeholder="1000sf" name="area"/>
					</div>
					<div class="my-div">
						<select class="form-control my-txt-fleid width50" name="build_year">
							<option value="">Year</option>
							<?php for($i=1950;$i<=date('Y',time());$i++){ ?>
								<option value="<?=$i?>"><?=$i?></option>
							<?php } ?>	
						</select>
					</div>
				</div>
-->
				<div class="my-div custom-width">
					<label><b>Property in Community</b></label>
					<p class="my-radio">  <input type="radio" id="radio10" name="is_community" class="is_community" value="Yes"/>  <label for="radio10">Yes</label></p>
					<p class="my-radio"> <input type="radio" id="radio11" name="is_community" class="is_community" value="No"/> <label for="radio11">No</label></p>
				</div>
				<div class="community_box" style="display:none">
				<div class="form-group1">
					<div class="my-div"><input type="text" class="form-control my-txt-fleid" placeholder="Community"/ name="community"></div>
					<div class="my-div">
						<input type="text" class="form-control my-txt-fleid width50 " placeholder="Gate Code" name="gate_code"/>
						<input type="text" class="form-control my-txt-fleid width50 right-pull mr-rite0" placeholder="Unit PO Box #" name="po_box"/>
					</div>
				</div>
				<div class="form-group1">
					<div class="my-div"><input type="text" class="form-control my-txt-fleid" placeholder="Communiy Mng Company" name="community_company"/></div>
					<div class="my-div"><input type="text" class="form-control my-txt-fleid" placeholder="Contact Number" name="company_number"/></div>
					<div class="my-div">
						
						<span class="upload-btn" style="width:255px;">Upload Community Docs<input type="file" name="comm_docs"></span>
					</div>
				</div>
				</div>	 
		   </div>
     
			<h3 class="panel-head">
				<div class="panel-title">
					<h1>Amenities and Services</h1>
				</div>
			</h3>
                  
            <div class="panel-body">
				<div  class="form-group1">
				   <div class="my-div"><input type="text" class="form-control my-txt-fleid" placeholder="Garbage Pick-up" readonly/></div>
				   <div class="my-div">
					   <select class="form-control my-txt-fleid width50" name="garbage_pick_day">
						   <option value="">Select Day</option>
						   <option value="Sunday">Sunday</option>
						   <option value="Monday">Monday</option>
						   <option value="Tuesday">Tuesday</option>
						   <option value="Wednesday">Wednesday</option>
						   <option value="Thursday">Thursday</option>
						   <option value="Friday">Friday</option>
						   <option value="Saturday">Saturday</option>
						   <option value="N/A">N/A</option>
					   </select>
				   </div>
				</div>
				<div  class="form-group1">
				   <div class="my-div"><input type="text" class="form-control my-txt-fleid" placeholder="Recycle Pick-up" readonly/></div>
				   <div class="my-div">
					   <select class="form-control my-txt-fleid width50" name="recycle_pick_day">
						   <option value="">Select Day</option>
						   <option value="Sunday">Sunday</option>
						   <option value="Monday">Monday</option>
						   <option value="Tuesday">Tuesday</option>
						   <option value="Wednesday">Wednesday</option>
						   <option value="Thursday">Thursday</option>
						   <option value="Friday">Friday</option>
						   <option value="Saturday">Saturday</option>
						   <option value="N/A">N/A</option>
					   </select>
				   </div>
				</div>
				<div  class="form-group1">
				   <div class="my-div"><input type="text" class="form-control my-txt-fleid" placeholder="Yard waste Pick-up" readonly/></div>
				   <div class="my-div">
					   <select class="form-control my-txt-fleid width50" name="yard_waste_pick_day">
						   <option value="">Select Day</option>
						   <option value="Sunday">Sunday</option>
						   <option value="Monday">Monday</option>
						   <option value="Tuesday">Tuesday</option>
						   <option value="Wednesday">Wednesday</option>
						   <option value="Thursday">Thursday</option>
						   <option value="Friday">Friday</option>
						   <option value="Saturday">Saturday</option>
						   <option value="N/A">N/A</option>
					   </select>
				   </div>
				</div>
				<div  class="form-group1 mr-top20">
					<div class="my-div">
						<select class="form-control my-txt-fleid" name="safety_equipment1">
							<option value=""> Select Smoke Detectors</option>
							<option value="1">1</option>
							<option value="2">2</option>
							<option value="3">3</option>
							<option value="4">4</option>
							<option value="N/A">N/A</option>
						</select>
					</div>
				</div>
				<div  class="form-group1">
					<div class="my-div">
						<select class="form-control my-txt-fleid" name="safety_equipment2">
							<option value=""> Select Fire Extinguishors</option>
							<option value="1">1</option>
							<option value="2">2</option>
							<option value="3">3</option>
							<option value="4">4</option>
							<option value="N/A">N/A</option>
						</select>
					</div>
				</div>
				<div  class="form-group1">
				   <div class="my-div"><input type="text" class="form-control my-txt-fleid" placeholder="Heat Electric or Heat Furnace" readonly/></div>
				   <div class="my-div">
					   <select class="form-control my-txt-fleid width50" name="heat_type">
						   <option value="">Select</option>
						   <option value="Heat Electric">Heat Electric</option>
						   <option value="Heat Furnace">Heat Furnace</option>
						   <option value="N/A">N/A</option>
					   </select>
				   </div>
				   <div class="my-div">
					   <select class="form-control my-txt-fleid width50" name="heat_filter_size">
						   <option value="">Select Filter size</option>
						   <option value="1">1</option>
						   <option value="2">2</option>
						   <option value="3">3</option>
					   </select>
				   </div>
				</div>
				<div  class="form-group1">
				   <div class="my-div"><input type="text" class="form-control my-txt-fleid" placeholder="AC Central or AC Room" readonly/></div>
				   <div class="my-div">
					   <select class="form-control my-txt-fleid width50" name="ac_type">
						   <option value="">Select</option>
						   <option value="AC Central">AC Central</option>
						   <option value="AC Room">AC Room</option>
						   <option value="N/A">N/A</option>
					   </select>
				   </div>
				   <div class="my-div">
					   <select class="form-control my-txt-fleid width50" name="ac_filter_size">
						   <option value="">Select Filter size</option>
						   <option value="1">1</option>
						   <option value="2">2</option>
						   <option value="3">3</option>
					   </select>
				   </div>
                <?php foreach($allAmenties as $value) { ?>
                <div  class="form-group1">
					<div class="my-div"><input type="text" class="form-control my-txt-fleid" placeholder="<?=$value->name?>" readonly/></div>
					<div class="my-div">
						<p class="my-radio">  <input type="radio" id="radio01_<?=$value->id?>" name="<?=$value->name?>" value="<?=$value->id?>"/>  <label for="radio01_<?=$value->id?>">Yes</label></p>
						<p class="my-radio"> <input type="radio" id="radio02_<?=$value->id?>" name="<?=$value->name?>" value=""/> <label for="radio02_<?=$value->id?>">No</label></p>
					</div>
                </div>
                <?php } ?>
                <div  class="form-group1">
					<div class="my-div"><input type="text" class="form-control my-txt-fleid" placeholder="Pest control home" readonly/></div>
					<div class="my-div">
						<p class="my-radio">  <input type="radio" id="radio01_pest_control_home" name="pest_control_home" value="yes"/>  <label for="radio01_pest_control_home">Yes</label></p>
						<p class="my-radio"> <input type="radio" id="radio02_pest_control_home" name="pest_control_home" value="no"/> <label for="radio02_pest_control_home">No</label></p>
					</div>
					<div class="my-div">
						<input type="text" class="form-control my-txt-fleid width50" placeholder="Company" name="pest_service_provider"/>
						<input type="text" class="form-control my-txt-fleid width50 right-pull mr-rite0" placeholder="Contact# " name="pest_service_provider_number"/>
					</div>
                </div>
                <div  class="form-group1">
					<div class="my-div"><input type="text" class="form-control my-txt-fleid" placeholder="Yard" readonly/></div>
					<div class="my-div" style="width:147px;">
						<p class="my-radio">  <input type="radio" id="radio01_yard" name="yard" value="yes" onClick="show_service_provided(this.value)"/>  <label for="radio01_yard">Yes</label></p>
						<p class="my-radio"> <input type="radio" id="radio02_yard" name="yard" value="no" onClick="show_service_provided(this.value)"/> <label for="radio02_yard">No</label></p>
					</div>
					
					<div class="my-div is_yard_service_provide" style="width:147px; display:none"><input type="text" class="form-control my-txt-fleid" placeholder="Service provided" readonly/></div>
					<div class="my-div is_yard_service_provide" style="width:147px; display:none">
						<p class="my-radio">  <input type="radio" id="radio01_service_provided" name="yard_service_provided" value="yes"/>  <label for="radio01_service_provided">Yes</label></p>
						<p class="my-radio"> <input type="radio" id="radio02_service_provided" name="yard_service_provided" value="no"/> <label for="radio02_service_provided">No</label></p>
					</div>
					<div class="my-div is_yard_service_provide" style="display:none">
						<input type="text" class="form-control my-txt-fleid width50" placeholder="Company" name="yard_service_provider"/>
						<input type="text" class="form-control my-txt-fleid width50 right-pull mr-rite0" placeholder="Contact# " name="yard_service_provider_number"/>
					</div>
					
					
                </div>
                <div class="my-panel   my-panel1 mr-btm0" id="panel-4">
					  <div class="panel-head">
						  <div class="panel-title">
							  <h1>More</h1>
						  </div>
					  </div>
                      <div class="panel-body">
                          <a href="javascript:;" class="add-btn add-new-service"><i class="fa fa-plus"></i>Add</a>
                          <div  class="form-group1 ">
							  <div class="my-div width150"><label>Select</label></div>
                              <div class="my-div">
								  <select  class="form-control my-txt-fleid" name="service">
									<option value="">Select Service</option>
									<?php foreach($allServices as $value) { ?>
										<option value="<?=$value->id?>"><?=$value->service?></option>
									<?php } ?>
								  </select>
							  </div>	
                             <div class="my-div ">
								  <input type="text" class="form-control my-txt-fleid width50" placeholder="Company" name="service_provider"/>
								  <input type="text" class="form-control my-txt-fleid width50 right-pull mr-rite0" placeholder="Contact#" name="service_provider_number"/>
							  </div>
                         
                        
					  </div>
						<div class="services-panel"></div>
						<div  class="form-group1 new-services-0">
							<div class="my-div width150"><label>Create</label></div>
                            <div class="my-div">
								<input type="text" class="form-control my-txt-fleid" placeholder=" Service" name="new_service_0"/>
							</div>
							<div class="my-div ">
								<input type="text" class="form-control my-txt-fleid width50" placeholder="Company" name="new_service_provider_0"/>
								<input type="text" class="form-control my-txt-fleid width50 right-pull mr-rite0" placeholder="Contact# " name="new_service_provider_number_0"/>
							</div>
							<div class="my-div width150">
								<a href="javascript:;" class="delete-row " id="cross0" data-value="0" style="margin-left:35px;"><img src="<?=$this->config->item('templateassets')?>images/cross.png" alt=""/></a>
							</div>
							
						</div>
						<input type="hidden" value="0" name="new_created_services" class="new_created_services">
						</div>
                </div>
     </div>
		

<!--
			<h3 class="panel-head">
				<div class="panel-title">
					<h1>Safety Equipment</h1>
				</div>
			</h3>
			<div class="panel-body">

				<div  class="form-group1 mr-top20">
					<div class="my-div">
						<select class="form-control my-txt-fleid" name="safety_equipment1">
							<option value=""> Select Smoke Detectors</option>
							<option value="1">1</option>
							<option value="2">2</option>
							<option value="3">3</option>
							<option value="4">4</option>
							<option value="N/A">N/A</option>
						</select>
					</div>
				</div>
				<div  class="form-group1">
					<div class="my-div">
						<select class="form-control my-txt-fleid" name="safety_equipment2">
							<option value=""> Select Fire Extinguishors</option>
							<option value="1">1</option>
							<option value="2">2</option>
							<option value="3">3</option>
							<option value="4">4</option>
							<option value="N/A">N/A</option>
						</select>
					</div>
				</div>
			</div>
		
-->
		</div>
		
		</div>
	<div class="center">  
			<input type="submit" class="next" value="NEXt" style=""/>
		</div>
		<?=form_close();?> 	
	</div>
	
</section>

<div class="end-block"></div>
<script>
	$(document).on('click','.is_community',function(event) { 
		var value	=	$(this).val();
		if(value=='Yes')
			$('.community_box').show('slow');
		if(value=='No')
			$('.community_box').hide('slow');
	});
	$(document).on('click','.add-new-service',function(event) { 
		 var assets	=	'<?=$this->config->item('templateassets')?>';
		 var new_created_services	=	$('.new_created_services').val();
		 new_created_services	=	parseInt(new_created_services)+1;
		 
		 var dataToAppend	=	'<div  class="form-group1 new-services-'+new_created_services+'"><div class="my-div width150"><label>Create</label></div>';    
		 dataToAppend+='<div class="my-div"><input type="text" class="form-control my-txt-fleid" placeholder=" Service" name="new_service_'+new_created_services+'"/></div>';    
		 dataToAppend+='<div class="my-div ">';    
		 dataToAppend+='<input type="text" class="form-control my-txt-fleid width50" placeholder="Company" name="new_service_provider_'+new_created_services+'"/>';    
		 dataToAppend+='<input type="text" class="form-control my-txt-fleid width50 right-pull mr-rite0" placeholder="Contact# " name="new_service_provider_number_'+new_created_services+'"/>';    
		 dataToAppend+='</div>';
		 dataToAppend+='<div class="my-div width150"><a href="javascript:;" class="delete-row " id="cross'+new_created_services+'" data-value="'+new_created_services+'" style="margin-left:35px;"><img src="'+assets+'images/cross.png" alt=""/></a></div></div>';
		 $('.services-panel').append(dataToAppend);
		 $('.new_created_services').val(new_created_services);
	});
	
	$(document).on('click','.delete-row',function(){
	var id	=	$(this).attr('data-value');
	$('#cross'+id).remove();
	$('.new-services-'+id).remove();
	var new_created_services	=	$('.new_created_services').val();
	new_created_services	=	parseInt(new_created_services)-1;
	$('.new_created_services').val(new_created_services);
	});

	function show_service_provided(value)
	{
		if(value == 'yes')
		{
			$('.is_yard_service_provide').show('slow');
		}
		if(value == 'no')
		{
			$('.is_yard_service_provide').hide('slow');
		}
	}
	
</script>	
  <script>
  $(document).on('click','.panel-head',function(){
		$(this).children().toggleClass('open');
		$(this).next().slideToggle();
	});
	$(document).ready(function(){
		$('.panel-body').hide();
		$('.panel-body').first().show();
		});
  </script>


<!--
  <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
-->
<style>
	.upload-btn > input {
    bottom: 0;
    cursor: pointer;
    font-size: 0;
    left: 0;
    opacity: 0;
    position: absolute;
    right: 0;
    top: 0;
    width: 100%;
}
.panel-body.ui-accordion-content {height:auto !important;}
</style>
