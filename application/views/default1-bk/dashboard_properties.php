<div id="properties-listing">
    <div class="signup">
        <div class="btn-part">
            <a href="javascript:;" class="<?php if ($type == 'all') { ?>active<?php } else { ?>unactive<?php } ?> left property-by-status" data-value="" style="width:108px;">All </a>
            <a href="javascript:;" class="<?php if ($type == 'vacant') { ?>active<?php } else { ?>unactive<?php } ?> left property-by-status" data-value="vacant" style="width:107px;">Vacant </a>
            <a href="javascript:;" class="<?php if ($type == 'occupied') { ?>active<?php } else { ?>unactive<?php } ?> right property-by-status" data-value="occupied" style="width:107px;">Occupied</a>
        </div>
        <a class="btn-tenant add-property" href="<?= site_url('add-property') ?>"><i class="fa fa-plus"></i>Add Property</a>
    </div>
    <div class="table-content signup-table dashboard-table">
        <table class="my-table">
            <thead>
                <tr>
                    <th>Address </th>
                    <th>Unit#  </th>
                    <th>Rent Balance</th>
                    <th>Tenants</th>
                    <th>Messages</th>
                    <th>Vendors</th>
                    <th><a href="javascript:;" class="dash-pop tpay">tPay</a></th>
                    <th><a href="javascript:;" class="dash-pop">tMaintenance</a></th>
                    <th><a href="javascript:;" class="dash-pop">tMessaging</a></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($allProperties as $value) { ?>
                            <tr>
                                <td> <a href="<?= site_url('my-properties') ?>#tabs-<?php echo $value->prop_id; ?>">  <?= $value->city ?>, <?= $value->address1 ?> <?= $value->address2 ?> </a> </td>
                                <td> <a href="<?= site_url('my-properties') ?>#tabs-<?php echo $value->prop_id; ?>"><?= $value->unit_number ?> </a> </td>
                                <td>  <?php echo $value->rent_amount; ?> </td> 
                                <td>
                                    <?php if ($value->tenants == 0) { ?>
                                        <a href="javascript:;" id="<?php echo $value->prop_id; ?>" data-toggle="modal" data-target="#addtenantModal" class="<?php if ($value->tenants == 0) { ?>zero<?php } ?>"> <?= $value->tenants ?> </a> 
                                    <?php } else { ?>
                                        <a href="<?= site_url('tenants') ?>#tabs-<?php echo $value->prop_id; ?>" class="<?php if ($value->expiry == 'Yes') { ?>expiring<?php } ?>"> <?= $value->tenants ?> </a> 
                                    <?php } ?>
                                </td>    
                                <td> <a href="<?= site_url('message') ?>#tabs-<?php echo $value->prop_id; ?>"> <?php echo $value->messages ?> </a> </td>
                                <td> <a href="<?= site_url('vendors') ?>#tabs-<?php echo $value->prop_id; ?>"> <?= $value->vendors ?> </a> </td>
                                <td>
                                    <div class="togle <?php if ($value->tpay != 'Yes') { ?>stop<?php } ?> tServices" id="tpay-<?= $value->prop_id ?>" data-type="tpay" data-value="<?= $value->prop_id ?>" data-property="<?php if ($value->tpay == 'Yes') { ?>No<?php } else { ?>Yes<?php } ?>">
                                        <span class="check"></span>
                                    </div>
                                </td>
                                <td> <div class="togle <?php if ($value->tmaintenance != 'Yes') { ?>stop<?php } ?> tServices" id="tmaintenance-<?= $value->prop_id ?>" data-type="tmaintenance" data-value="<?= $value->prop_id ?>" data-property="<?php if ($value->tmaintenance == 'Yes') { ?>No<?php } else { ?>Yes<?php } ?>"><span class="check"></span></div></td>
                                <td> 
                                    <div class="togle <?php if ($value->tmessaging != 'Yes') { ?>stop<?php } ?> tServices" id="tmessaging-<?= $value->prop_id ?>" data-type="tmessaging" data-value="<?= $value->prop_id ?>" data-property="<?php if ($value->tmessaging == 'Yes') { ?>No<?php } else { ?>Yes<?php } ?>">
                                        <span class="check"></span>
                                    </div>
                                </td>
                            </tr>
                        <?php } ?>
            </tbody>
        </table>
        <?php if (!$allProperties) { ?>
            <center></br></br></br></br></br></br>
                <span class="no-record"> No Properties found...</span>
            </center>
        <?php } ?>
    </div>
</div>	  

