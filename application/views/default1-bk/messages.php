<?php
if (!empty($sch_msgs)) {
    foreach ($sch_msgs as $sc_msg) {
        $property_wise_sch[$sc_msg->prop_id][] = $sc_msg;
    }
}
if (!empty($sent_msgs)) {
    foreach ($sent_msgs as $snt_msg) {
        $property_wise_sent[$snt_msg->prop_id][] = $snt_msg;
    }
}


//echo '<pre>';print_r($sch_msgs);die;
?>
<section class="lease-part">
    <div class="container">



        <div id="tabs">

            <div class="my-tab1"> 

                <a href="javascript:void(0)" class="btn-tenant my-btn-tennt my-vendor"><i class="fa fa-plus"></i>Add Messages</a>
                <ul class="my-tab">

                    <li class="active "><a href="#tabs-1" class="calender">Scheduled Messages</a></li>
                    <?php if (!empty($properties)) {
                        foreach ($properties as $key => $prop) {
                            ?>
                            <li class="all-tabs-panel-li">
                                <a href="#tabs-<?= $prop->prop_id ?>" data-value="<?= $prop->prop_id ?>" class="all-tabs-panel"><?= $prop->city ?>, <?= $prop->address1 ?> <?= $prop->address2 ?></a></li>
                        <?php }
                    } ?>
                    <!--<li><a href="#tabs-2">456 Southeast Blvd #123</a></li>-->
                </ul>
            </div>

<?php $this->load->view('default/add_message'); ?>

            <div class="tab-area" >
                <div id="tabs-1">
                    <div class="tenant mr-btm0">
                        <ul class="tenant2 msg-block">
                            <?php if (!empty($sch_msgs)) { ?>
                                <?php foreach ($sch_msgs as $sc_sms): ?>
                                    <li>
                                        <p class="my-messg">
                                            <?php
                                                    if (mb_strlen($sc_sms->message) > 180) {
                                                        echo substr($sc_sms->message, 0, 180) . "...<u class='col-red read_more'>Read More</u><p class='my-messg' style='display:none'>$sc_sms->message</p>";
                                                    } else {
                                                        echo $sc_sms->message;
                                                    }
                                             ?>
                                           
                                        </p>
                                        <ul class="msg-sender-info">
                                            <li class="user-msg"><a href="#" ><?php echo $sc_sms->first_name . " " . $sc_sms->last_name ?></a></li>
                                            <li class="loc-msg"><a href="#" ><?= $sc_sms->city ?>, <?= $sc_sms->address1 ?> <?= $sc_sms->address2 ?></a></li> 
                                            <li class="date-msg"><a href="#" ><?php echo date('m/d/y', strtotime($sc_sms->scheduled_on)); ?></a></li>
                                        </ul>
                                        <ul  class="msg-icon-part">
                                            <li class="edit-span"> <a class="edit edit_sch_sms" id="<?php echo $sc_sms->id; ?>" href="javascript:void(0)"></a></li>
                                            <li  class="del-span">  <a class="del" id="<?php echo $sc_sms->id; ?>" href="javascript:void(0)"></a></li>
                                        </ul>
                                        <div class="new-cont-area" id="nca_<?php echo $sc_sms->id; ?>" style="display:none;">
                                            <textarea class="form-control tarea" id="schta_<?php echo $sc_sms->id; ?>" name=""><?php echo $sc_sms->message; ?></textarea>
                                            <input type="text" id="schta_<?php echo $sc_sms->id; ?>" placeholder="MM-DD-YY" value="<?php echo date('m/d/y', strtotime($sc_sms->scheduled_on)); ?>" class="form-control startFrom" name="sch_date_edit">
                                            <select id="etype_<?php echo $sc_sms->id; ?>" name="edit_repeat_msg" resp="<?php echo $sc_sms->id; ?>"  class="form-control my-select-1 edit_repeat_msg">
                                                <option value="week" <?php if ($sc_sms->repeat_type == 'week') {echo 'selected="selected"';} ?>>Weekly</option>
                                                <option value="month" <?php if ($sc_sms->repeat_type == 'month') {echo 'selected="selected"';} ?>>Monthly</option>
                                                <option value="year" <?php if ($sc_sms->repeat_type == 'year') {echo 'selected="selected"';} ?>>Yearly</option>
                                            </select>
                                          
                                            <select id="erv_<?php echo $sc_sms->id; ?>" name="edit_repeat_every" class="form-control my-select-1 shw_rpt_type"  <?php if($sc_sms->repeat_type == 'year'){ echo 'style="display:none"';} ?>>
                                               <?php if($sc_sms->repeat_type == 'month'){?>
                                                <?php for ($j = 1; $j < 31; $j++) {
                                                    if($sc_sms->repeat_every==$j){
                                                        echo '<option selected="selected" value="'.$j.'">'.ordinal($j).'</option>';
                                                    }else{
                                                         echo '<option value="'.$j.'">'.ordinal($j).'</option>';
                                                    }
                                                } ?>
                                               <?php } else { ?>
                                              <?php 
                                              $weeks = array('1' => '1 week', '2' => '2 weeks', '3' => '3 weeks', '4' => '4 weeks');
                                              foreach ($weeks as $key=>$weekday){
                                                   if($sc_sms->repeat_on==$key){
                                                        echo '<option selected="selected" value="'.$key.'">'.$weekday.'</option>';
                                                    }else{
                                                         echo '<option value="'.$key.'">'.$weekday.'</option>';
                                                    }
                                              }
                                              ?>
                                               <?php } ?>
                                            </select>
                                         
                                            <select id="ero_<?php echo $sc_sms->id; ?>" name="edit_repeat_on" class="form-control my-select-1 shw_rpt_on" <?php if($sc_sms->repeat_type == 'year' || $sc_sms->repeat_type == 'month'){ echo 'style="display:none"';} ?>>
                                                <option <?php if($sc_sms->repeat_on=='Monday'){ echo 'selected="selected"';} ?> >Monday</option>
                                                <option <?php if($sc_sms->repeat_on=='Tuesday'){ echo 'selected="selected"';} ?>>Tuesday</option>
                                                <option <?php if($sc_sms->repeat_on=='Wednesday'){ echo 'selected="selected"';} ?>>Wednesday</option>
                                                <option <?php if($sc_sms->repeat_on=='Thursday'){ echo 'selected="selected"';} ?>>Thursday</option>
                                                <option <?php if($sc_sms->repeat_on=='Friday'){ echo 'selected="selected"';} ?>>Friday</option>
                                                <option <?php if($sc_sms->repeat_on=='Saturday'){ echo 'selected="selected"';} ?>>Saturday</option>
                                                <option <?php if($sc_sms->repeat_on=='Sunday'){ echo 'selected="selected"';} ?>>Sunday</option>
                                            </select>
                                            <select id="ert_<?php echo $sc_sms->id; ?>" name="repeat_occurence" class="form-control my-select-1 repeat-time">
                                                    <?php for($k=1;$k<13;$k++){ ?>
                                                        <option <?php if($sc_sms->occurences==$k){ echo 'selected="selected"';} ?> value="<?php echo $k ?>">Repeat <?php echo $k; ?> Times</option>
                                                    <?php } ?>
                                                        <option <?php if($sc_sms->end_repeat=='infinte'){ echo 'selected="selected"';} ?> value="noend">Repeat Infinite</option>
                                            </select>
                                            <a class="save save_sch_sms" id="<?php echo $sc_sms->id; ?>" href="javascript:void(0)">Save</a>
                                            
                                        </div>
                                    </li>
                                    <?php endforeach; ?>
                            <?php }else{ ?>
                                    <li>No messages scheduled.</li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
                <?php
                if (!empty($properties)) {
                    foreach ($properties as $key => $prop) {
                        ?>
                        <div id="tabs-<?= $prop->prop_id ?>">
                            <div id="tabs1" class="my-msg-inner-tab twotabs">
                                <ul>
                                    <li><a href="#tabs-sch<?= $prop->prop_id ?>">Scheduled </a></li>
                                    <li><a href="#tabs-log<?= $prop->prop_id ?>">  Message Log</a></li>

                                </ul>
                                <div id="tabs-sch<?= $prop->prop_id ?>">
                                    <div class="Scheduled-cont">
                                        <ul class="Scheduled-cont-data-1">
                                            <?php if (isset($property_wise_sch[$prop->prop_id]) && !empty($property_wise_sch[$prop->prop_id])) { ?>
                                                <?php foreach ($property_wise_sch[$prop->prop_id] as $sms) { ?>
                                                    <li>
                                                        <ul class="msg-sender-info-1">
                                                            <li class="user-msg"><a href="#" ><?php echo ucwords($sms->first_name . " " . $sms->last_name); ?></a></li>
                                                            <li class="date-msg"><a href="#" ><?php echo date("m/d/y", strtotime($sms->scheduled_on)); ?></a></li>
                                                        </ul>
                                                        <p><?php echo $sms->message; ?></p>
                                                        <ul class="msg-icon-part-1">
                                                              <li class="edit-span"> <a class="edit tedit_sch_sms" id="<?php echo $sms->id; ?>" href="javascript:void(0)"></a></li>
                                                              <li  class="del-span">  <a class="del" id="<?php echo $sms->id; ?>" href="javascript:void(0)"></a></li>
                                                        </ul>
                                                        <div class="new-cont-area tb_area" id="tnca_<?php echo $sms->id; ?>" style="display:none;">
                                            
                                            <input type="text" id="tsch_<?php echo $sms->id; ?>" placeholder="MM-DD-YY" value="<?php echo date('m/d/y', strtotime($sms->scheduled_on)); ?>" class="form-control startFrom" name="sch_date_edit">
                                            <select id="tetype_<?php echo $sms->id; ?>" name="edit_repeat_msg" resp="<?php echo $sms->id; ?>"  class="form-control my-select-1 tedit_repeat_msg">
                                                <option value="week" <?php if ($sms->repeat_type == 'week') {echo 'selected="selected"';} ?>>Weekly</option>
                                                <option value="month" <?php if ($sms->repeat_type == 'month') {echo 'selected="selected"';} ?>>Monthly</option>
                                                <option value="year" <?php if ($sms->repeat_type == 'year') {echo 'selected="selected"';} ?>>Yearly</option>
                                            </select>
                                          
                                            <select id="terv_<?php echo $sms->id; ?>" name="edit_repeat_every" class="form-control my-select-1 shw_rpt_type"  <?php if($sms->repeat_type == 'year'){ echo 'style="display:none"';} ?>>
                                               <?php if($sms->repeat_type == 'month'){?>
                                                <?php for ($j = 1; $j < 31; $j++) {
                                                    if($sms->repeat_every==$j){
                                                        echo '<option selected="selected" value="'.$j.'">'.ordinal($j).'</option>';
                                                    }else{
                                                         echo '<option value="'.$j.'">'.ordinal($j).'</option>';
                                                    }
                                                } ?>
                                               <?php } else { ?>
                                              <?php 
                                              $weeks = array('1' => '1 week', '2' => '2 weeks', '3' => '3 weeks', '4' => '4 weeks');
                                              foreach ($weeks as $key=>$weekday){
                                                   if($sms->repeat_on==$key){
                                                        echo '<option selected="selected" value="'.$key.'">'.$weekday.'</option>';
                                                    }else{
                                                         echo '<option value="'.$key.'">'.$weekday.'</option>';
                                                    }
                                              }
                                              ?>
                                               <?php } ?>
                                            </select>
                                         
                                            <select id="tero_<?php echo $sms->id; ?>" name="edit_repeat_on" class="form-control my-select-1 shw_rpt_on" <?php if($sms->repeat_type == 'year' || $sms->repeat_type == 'month'){ echo 'style="display:none"';} ?>>
                                                <option <?php if($sms->repeat_on=='Monday'){ echo 'selected="selected"';} ?> >Monday</option>
                                                <option <?php if($sms->repeat_on=='Tuesday'){ echo 'selected="selected"';} ?>>Tuesday</option>
                                                <option <?php if($sms->repeat_on=='Wednesday'){ echo 'selected="selected"';} ?>>Wednesday</option>
                                                <option <?php if($sms->repeat_on=='Thursday'){ echo 'selected="selected"';} ?>>Thursday</option>
                                                <option <?php if($sms->repeat_on=='Friday'){ echo 'selected="selected"';} ?>>Friday</option>
                                                <option <?php if($sms->repeat_on=='Saturday'){ echo 'selected="selected"';} ?>>Saturday</option>
                                                <option <?php if($sms->repeat_on=='Sunday'){ echo 'selected="selected"';} ?>>Sunday</option>
                                            </select>
                                            <select id="tert_<?php echo $sms->id; ?>" name="repeat_occurence" class="form-control my-select-1 repeat-time">
                                                    <?php for($k=1;$k<13;$k++){ ?>
                                                        <option <?php if($sms->occurences==$k){ echo 'selected="selected"';} ?> value="<?php echo $k ?>">Repeat <?php echo $k; ?> Times</option>
                                                    <?php } ?>
                                                        <option <?php if($sms->end_repeat=='infinte'){ echo 'selected="selected"';} ?> value="noend">Repeat Infinite</option>
                                            </select>
                                            <a class="save tsave_sch_sms" id="<?php echo $sms->id; ?>" href="javascript:void(0)">Save</a>
                                            
                                        </div>
                                                    </li>
                                                <?php } ?>
                                            <?php } else { ?>
                                                <li>No messages scheduled.</li>
                                            <?php } ?>


                                        </ul>
                                    </div>
                                </div>
                                <div id="tabs-log<?= $prop->prop_id ?>">
                                    <div class="Scheduled-cont">
                                        <ul class="Scheduled-cont-data">
                                            <?php if (isset($property_wise_sent[$prop->prop_id]) && !empty($property_wise_sent[$prop->prop_id])) { ?>
                                                <?php foreach ($property_wise_sent[$prop->prop_id] as $smz) { ?>
                                                    <li>
                                                        <p><?php echo $smz->message; ?></p>
                                                        <span class="date"><?php echo date("m/d/y", strtotime($smz->date_created)); ?></span>
                                                    </li>
                                                <?php } ?>
                                            <?php } else { ?>
                                                <li>No sent messages.</li>
                                            <?php } ?>

                                        </ul>
                                    </div>
                                </div>

                            </div>
                        </div>
                    <?php } ?>
                <?php } ?>



            </div>

        </div>
    </div>     
</section>