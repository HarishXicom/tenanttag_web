
<section class="signup-section2 dashborad-page">
  <div class="container">
	  <div class="block-area">
		  <a class="my-block" href="javascript:;">
			  <h1><?=$countAllProperties?></h1>
			  <p>Properties </p>
		  </a>
			 <a class="my-block" href="javascript:;">
			  <h1><?=$countOccupiedProperties?></h1>
			  <p>Occupied</p>
		  </a>
			 <a class="my-block" href="javascript:;">
			   <h1><?=$countVacantProperties?></h1>
			  <p>vacant</p>
		  </a>
			 <a class="my-block mr-rite0" href="javascript:;">
			   <h1><?=$totalTenants?></h1>
			  <p>Tenants</p>
		  </a>
	  </div>
	  <div id="properties-listing">
	  <div class="signup">
		  <div class="btn-part">
			  <a href="javascript:;" class="unactive left property-by-status" data-value="vacant">Vacant </a>
			  <a href="javascript:;" class="unactive right property-by-status" data-value="occupied">Occupied</a>
		  </div>
		 <a class="btn-tenant add-property" href="<?=site_url('add-property')?>"><i class="fa fa-plus"></i>Add Property</a>
	  </div>
	  <div class="table-content signup-table dashboard-table">
		  <table class="my-table">
		  <thead>
			  <tr>
				  <th>Address </th>
				  <th>Unit#  </th>
				  <th>Tenants</th>
				  <th>Messages</th>
				  <th>Vendors</th>
				  <th><a href="javascript:;" class="dash-pop">tMessaging</a></th>
				  <th><a href="javascript:;" class="dash-pop">tMaintenance</a></th>
			  </tr>
		  </thead>
		  <tbody>
			  <?php foreach($allProperties as $value) { ?>
			   <tr>
					<td> <a href="javascript:;"> <?=$this->common_model->getSingleFieldFromAnyTable('region_name','region_id',$value->city,'tbl_region')?> <?=$value->city?>,<?=$value->address1?> <?=$value->address2?> </a> </td>
					<td> <a href="javascript:;"><?=$value->unit_number?> </a> </td>
					<td> <a href="<?=site_url('tenants')?>" class="<?php if($value->tenants== 0){?>zero<?php } ?>"> <?=$value->tenants?> </a> </td>
					<td> <a href="javascript:;"> 0 </a> </td>
					<td> <a href="javascript:;"> <?=$value->vendors?> </a> </td>
					<td> <div class="togle <?php if($value->tmessaging != 'Yes') { ?>stop<?php } ?> tServices" id="tmessaging-<?=$value->prop_id?>" data-type="tmessaging" data-value="<?=$value->prop_id?>" data-property="<?php if($value->tmessaging=='Yes') { ?>No<?php } else { ?>Yes<?php } ?>"><span class="check"></span></div></td>
					<td> <div class="togle <?php if($value->tmaintenance != 'Yes') { ?>stop<?php } ?> tServices" id="tmaintenance-<?=$value->prop_id?>" data-type="tmaintenance" data-value="<?=$value->prop_id?>" data-property="<?php if($value->tmaintenance=='Yes') { ?>No<?php } else { ?>Yes<?php } ?>"><span class="check"></span></div></td>
			  </tr>
			  <?php } ?>
		  </tbody>
	  </table>
	  <?php if(!$allProperties) { ?>
		 <center></br></br></br></br></br></br>
		 <span class="no-record"> No Properties found...</span>
		 </center>
	  <?php } ?>
	  </div>
	  </div>	  
		  </div>
</section>

<script>
	$(document).on("click",".property-by-status",function(){
	var type	=	$(this).attr('data-value');
	var postUrl	=	siteUrl+'dashboard/'+type;
		$.ajax({
			url:postUrl,
			dataType:'json',
			success:function(response){
				if(response.success)
				{
					$('#properties-listing').html('');
					$('#properties-listing').html(response.html);
				}
			},
			error:function(){
				alert('server error');
			},
		});
});

$(document).on("click",".tServices",function(){
var property_id		=	$(this).attr('data-value');
var	value		=	$(this).attr('data-property');
var	type			=	$(this).attr('data-type');
var postUrl	=	siteUrl+'change-tservices-status/'+property_id+'/'+type+'/'+value;
		var message='';
		if(type == 'tmessaging' && value=="No")
			message	=	'Turing off tmessaging will not allow you to send message to tenants. Do you wish to continue?';
		if(type == 'tmessaging' && value=="Yes")
			message	=	'Are you sure to Turning On tmessaging?';
		if(type == 'tmaintenance' && value=="No")
			message	=	'Turning off tmaintenance will not allow the Tenants to contact Vendors. Do you wish  to continue?';
		if(type == 'tmaintenance' && value=="Yes")
			message	=	'Are you sure to Turning On tmaintenance?';
		BootstrapDialog.confirm(message, function(result){
		if(result)
		{
			$.ajax({
			url:postUrl,
			dataType:'json',
			success:function(response){
				if(response.success)
				{
					if(value == 'Yes')
					{
						$('#'+type+'-'+property_id).removeClass('stop');
						$('#'+type+'-'+property_id).attr('data-property','No');
					}	
					else
					{
						$('#'+type+'-'+property_id).addClass('stop');
						$('#'+type+'-'+property_id).attr('data-property','Yes');
					}	
				}
			},
			error:function(){
				alert('server error');
			},
			});
		}
		else 
		{
			return false;
		}
		});
		
		
});

</script>	
