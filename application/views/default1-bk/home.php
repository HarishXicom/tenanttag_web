<section class="top-banner">
<div class="container">
<div class="row">
<h2>A Better Way for a Landlord to Manage Property and Keep Tenants</h2>
<p class="banner-define">TenantTag’s property management software improves
the rental experience for landlords and their tenants 
while saving time and money.</p>

<div class="inckudes">
<h3>Includes</h3>
<div class="single-include">
<a href="#"> <img src="<?= $this->config->item('templateassets') ?>images/landlord-icon.png">  </a>
<div class="content">
<a href="#"><h4>Landlord Dashboard</h4>  </a>
<p>One stop management interface <a href="#">more</a> </p>
</div>
</div>

<div class="single-include">
<a href="#"> <img src="<?= $this->config->item('templateassets') ?>images/mob-app.png">  </a>
<div class="content">
<a href="#"><h4>Tenant Mobile App</h4>  </a>
<p>Customized for each tenant <a href="#">more</a> </p>
</div>
</div>
<div class="single-include">
<a href="#"> <img src="<?= $this->config->item('templateassets') ?>images/t-text.png"> </a>
<div class="content">
<a href="#"><h4>tText</h4>  </a>
<p>Automated text message reminders  <a href="#">more</a> </p>
</div>
</div>
<div class="single-include">
<a href="#"> <img src="<?= $this->config->item('templateassets') ?>images/maintaennace.png"> </a>
<div class="content">
<a href="#"><h4>tMaintenance</h4>  </a>
<p>Automated request routing <a href="#">more</a> </p>
</div>
</div>
<div class="single-include">
<a href="#"> <img src="<?= $this->config->item('templateassets') ?>images/tpay.png"> </a>
<div class="content">
<a href="#"><h4>tPay</h4>  </a>
<p>Mobile rent payment <a href="#">more</a> </p>
</div>
</div>

<div class="single-include">
<a href="#"> <img src="<?= $this->config->item('templateassets') ?>images/features.png"> </a>
<div class="content">
<a href="#"><h4>Other Features</h4>  </a>

</div>
</div>

</div>
<div class=" video-sec ">

<video controls width="100%" height="320px"  poster="images/video-cover.jpg">
  <source src="https://youtu.be/uisBaTkQAE" type="video/ogg" />

</video>

<a href="#" class="comm-ques">Common Questions  </a>
</div>

</div>
</div>
</section>


<section class="signup-sec">
<div class="container">
<div class="text-center">
<h2>Sign up</h2>
<p>60 Days Free Trial</p>
</div>
<div class="pro-mnth">
<div class="pro-details">
<h4>$7.95/Property per month</h4>
<ul>
<li>Landlord Dashboard<span>INCLUDED</span>  </li>
<li>Tenant Portal<span>INCLUDED</span>  </li>
<li>tText<span>INCLUDED</span>  </li>
<li>tMaintenance  <span>INCLUDED</span>  </li>
<li>tPay  <span>INCLUDED</span>  </li>
<li>Other Features <span>INCLUDED</span>  </li>

</ul>
</div>

<div class="signup-form">
  <div class="right-banner right-signup-form">
            <h4>Sign Up</h4>
            <?php echo form_open('register', array('class' => '', 'id' => 'sign-up')) ?>
            <div class="ajax_report alert display-hide" role="alert" style="margin-left:0px;width:320px;margin-bottom:10px;"><span class="close-message"></span><div class="ajax_message">Hello Message</div></div>
            <div class="form-group1">
                <div class="my-div"><input type="text" placeholder="First Name" class="form-control my-txt-fleid width50" name="first_name"><input type="text" placeholder="Last Name" class="form-control my-txt-fleid width50 right-pull mr-rite0" name="last_name"></div>
            </div>
            <div class="form-group1">
                <div class="my-div"><input type="text" placeholder="Company" class="form-control my-txt-fleid" name="company_name"></div>
            </div>
            <div class="form-group1">
                <div class="my-div"><p class="check">    <input type="checkbox" id="test1" name="as_company">    <label for="test1">Use company name</label>  </p></div>
            </div>
            <div class="form-group1">
                <div class="my-div"><input type="text" placeholder="Email address" class="form-control my-txt-fleid" name="email"></div>
            </div>
            <div class="form-group1">
                <div class="my-div"><input type="password" id="signPassword" placeholder="Password" class="form-control my-txt-fleid width50" name="password"><input type="password" placeholder="Confirm Password" class="form-control my-txt-fleid width50 right-pull mr-rite0" name="confirm_password"></div>
            </div>
            <!--div  class="form-group1">
                <div class="my-div">
                    <select class="form-control my-txt-fleid" name="country" id="country" onChange="getStateByCountryId(this.value)">
                        <option value="">Select Country</option>
            <?php //foreach ($allcountry as $value) { ?>
                            <option value="<?//= $value->id ?>"><?//= $value->country_name ?></option>
            <?php //} ?>
                    </select>
                </div>
            </div-->
            <div id="regions" style="display:none">
                <div  class="form-group1">
                    <div class="my-div">
                        <select class="form-control my-txt-fleid" name="state" id="state" onChange="getCityByStateId(this.value)">
                            <option value="">Select State</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group1">
                <div class="my-div">
                    <!--<input type="text" placeholder="code#" class="form-control my-txt-fleid country_code" name="country_code" style="width:100px">-->
                    <input type="text" placeholder="Mobile#" class="form-control my-txt-fleid phone_us" name="mobile" style=""></div>
            </div>
            <div class="form-group1" style="display:none;">
                <div class="my-div">
                    <p class="">   
                        <input type="checkbox" name="terms" id="" style="position: relative; left: 0px; top: 3px;">
                        Agree to our <a href="<?php echo base_url('terms-and-conditions'); ?>"><span class="changecolor">Terms & Conditions</span></a>
                    </p>
                </div>
            </div>

            <input type="submit" class="formsign-bttn" value="SIGN UP"/> 
            <?= form_close(); ?>
        </div>
</div>

</div></div>

</section>

<section class="feature-sec">
<div class="container">

<h2 class="feat">Features</h2>


<div class="slider-feature">
<div class="slider-main">
<div class="slider-for ">
<div class="autoplay slider-auto">
<div class="sliderr">
<div class="img-main">
<img src="<?= $this->config->item('templateassets') ?>images/mob-ten.png">
</div>
<div class="cont-main">
<h3>Tenant Portal</h3>
<p>Give your tenants the information they need 
when they need it!</p>
<a href="#">Common Questions <img src="<?= $this->config->item('templateassets') ?>images/arr.png">  </a>
</div>

</div>
<div class="sliderr">
<div class="img-main">
<img src="<?= $this->config->item('templateassets') ?>images/mob-ten.png">
</div>
<div class="cont-main">
<h3>Tenant Portal2</h3>
<p>Give your tenants the information they need 
when they need it!</p>
<a href="#">Common Questions <img src="<?= $this->config->item('templateassets') ?>images/arr.png">  </a>
</div>

</div>
<div class="sliderr">
<div class="img-main">
<img src="<?= $this->config->item('templateassets') ?>images/mob-ten.png">
</div>
<div class="cont-main">
<h3>Tenant Portal3</h3>
<p>Give your tenants the information they need 
when they need it!</p>
<a href="#">Common Questions <img src="<?= $this->config->item('templateassets') ?>images/arr.png">  </a>
</div>

</div>

</div>
<div class="autoplay slider-auto">
<div class="sliderr">
<div class="img-main">
<img src="<?= $this->config->item('templateassets') ?>images/mob-ten.png">
</div>
<div class="cont-main">
<h3>Tenant Portal4</h3>
<p>Give your tenants the information they need 
when they need it!</p>
<a href="#">Common Questions <img src="<?= $this->config->item('templateassets') ?>images/arr.png">  </a>
</div>

</div>
<div class="sliderr">
<div class="img-main">
<img src="<?= $this->config->item('templateassets') ?>images/mob-ten.png">
</div>
<div class="cont-main">
<h3>Tenant Portal5</h3>
<p>Give your tenants the information they need 
when they need it!</p>
<a href="#">Common Questions <img src="<?= $this->config->item('templateassets') ?>images/arr.png">  </a>
</div>

</div>
<div class="sliderr">
<div class="img-main">
<img src="<?= $this->config->item('templateassets') ?>images/mob-ten.png">
</div>
<div class="cont-main">
<h3>Tenant Portal6</h3>
<p>Give your tenants the information they need 
when they need it!</p>
<a href="#">Common Questions <img src="<?= $this->config->item('templateassets') ?>images/arr.png">  </a>
</div>

</div>

</div>
<div class="autoplay slider-auto">
<div class="sliderr">
<div class="img-main">
<img src="<?= $this->config->item('templateassets') ?>images/mob-ten.png">
</div>
<div class="cont-main">
<h3>Tenant Portal7</h3>
<p>Give your tenants the information they need 
when they need it!</p>
<a href="#">Common Questions <img src="<?= $this->config->item('templateassets') ?>images/arr.png">  </a>
</div>

</div>
<div class="sliderr">
<div class="img-main">
<img src="<?= $this->config->item('templateassets') ?>images/mob-ten.png">
</div>
<div class="cont-main">
<h3>Tenant Portal8</h3>
<p>Give your tenants the information they need 
when they need it!</p>
<a href="#">Common Questions <img src="<?= $this->config->item('templateassets') ?>images/arr.png">  </a>
</div>

</div>
<div class="sliderr">
<div class="img-main">
<img src="<?= $this->config->item('templateassets') ?>images/mob-ten.png">
</div>
<div class="cont-main">
<h3>Tenant Portal9</h3>
<p>Give your tenants the information they need 
when they need it!</p>
<a href="#">Common Questions <img src="<?= $this->config->item('templateassets') ?>images/arr.png">  </a>
</div>

</div>

</div>
<div class="autoplay slider-auto">
<div class="sliderr">
<div class="img-main">
<img src="<?= $this->config->item('templateassets') ?>images/mob-ten.png">
</div>
<div class="cont-main">
<h3>Tenant Portal10</h3>
<p>Give your tenants the information they need 
when they need it!</p>
<a href="#">Common Questions <img src="<?= $this->config->item('templateassets') ?>images/arr.png">  </a>
</div>

</div>
<div class="sliderr">
<div class="img-main">
<img src="<?= $this->config->item('templateassets') ?>images/mob-ten.png">
</div>
<div class="cont-main">
<h3>Tenant Portal11</h3>
<p>Give your tenants the information they need 
when they need it!</p>
<a href="#">Common Questions <img src="<?= $this->config->item('templateassets') ?>images/arr.png">  </a>
</div>

</div>
<div class="sliderr">
<div class="img-main">
<img src="<?= $this->config->item('templateassets') ?>images/mob-ten.png">
</div>
<div class="cont-main">
<h3>Tenant Portal12</h3>
<p>Give your tenants the information they need 
when they need it!</p>
<a href="#">Common Questions <img src="<?= $this->config->item('templateassets') ?>images/arr.png">  </a>
</div>

</div>

</div>
<div class="autoplay slider-auto">
<div class="sliderr">
<div class="img-main">
<img src="<?= $this->config->item('templateassets') ?>images/mob-ten.png">
</div>
<div class="cont-main">
<h3>Tenant Portal13</h3>
<p>Give your tenants the information they need 
when they need it!</p>
<a href="#">Common Questions <img src="<?= $this->config->item('templateassets') ?>images/arr.png">  </a>
</div>

</div>
<div class="sliderr">
<div class="img-main">
<img src="<?= $this->config->item('templateassets') ?>images/mob-ten.png">
</div>
<div class="cont-main">
<h3>Tenant Portal14</h3>
<p>Give your tenants the information they need 
when they need it!</p>
<a href="#">Common Questions <img src="<?= $this->config->item('templateassets') ?>images/arr.png">  </a>
</div>

</div>
<div class="sliderr">
<div class="img-main">
<img src="<?= $this->config->item('templateassets') ?>images/mob-ten.png">
</div>
<div class="cont-main">
<h3>Tenant Portal15</h3>
<p>Give your tenants the information they need 
when they need it!</p>
<a href="#">Common Questions <img src="<?= $this->config->item('templateassets') ?>images/arr.png">  </a>
</div>

</div>

</div>
<div class="autoplay slider-auto">
<div class="sliderr">
<div class="img-main">
<img src="<?= $this->config->item('templateassets') ?>images/mob-ten.png">
</div>
<div class="cont-main">
<h3>Tenant Portal16</h3>
<p>Give your tenants the information they need 
when they need it!</p>
<a href="#">Common Questions <img src="<?= $this->config->item('templateassets') ?>images/arr.png">  </a>
</div>

</div>
<div class="sliderr">
<div class="img-main">
<img src="<?= $this->config->item('templateassets') ?>images/mob-ten.png">
</div>
<div class="cont-main">
<h3>Tenant Portal17</h3>
<p>Give your tenants the information they need 
when they need it!</p>
<a href="#">Common Questions <img src="<?= $this->config->item('templateassets') ?>images/arr.png">  </a>
</div>

</div>
<div class="sliderr">
<div class="img-main">
<img src="<?= $this->config->item('templateassets') ?>images/mob-ten.png">
</div>
<div class="cont-main">
<h3>Tenant Portal18</h3>
<p>Give your tenants the information they need 
when they need it!</p>
<a href="#">Common Questions <img src="<?= $this->config->item('templateassets') ?>images/arr.png">  </a>
</div>

</div>

</div>
</div>
</div>


<div class="slider-sidebar">
<div class="slider-nav ">
<div class="sliderr">
LANDLORD DASHBOARD 
</div>
<div class="sliderr">
TENANT MOBILE APP  
</div>
<div class="sliderr">
tTEXT
</div>
<div class="sliderr">
tMAINTENANCE  
</div>
<div class="sliderr">
tPAY
</div>
<div class="sliderr">
+OTHER FEATURES
</div>
</div>

</div>


<div class="sifnup-noww">
<a href="#">Sign up Now</a>
</div>

</div>


</div>
</section>

<section class="dr-banner" >
    <div class="container">
        <div class="left-banner">
            <h3>Landlords, are you losing good tenants? TenantTag improves tenant communication and management for busy landlords.</h3>

            <ul>	
                <li class="message-img"><h4><span>t</span>Messaging<sup>*Free Trial*</sup></h4>
                    <p>Improves communications with tenants by using as an automated messaging system to deliver customized messages based on property, tenant, and lease information.</p>
                </li>

                <li class="mainstaince-img">
                    <h4><span>t</span>Maintenance<sup>*Free Trial*</sup></h4>
                    <p>Simplifies the submission, delivery, and approval of maintenance requests through an innovative, mobile solution.</p>
                </li>
                <li class="tpay-icon-img">
                    <h4><span>t</span>Pay<sup>*Free Trial*</sup></h4>
                    <p>Simplifies the submission, delivery, and approval of maintenance requests through an innovative, mobile solution.</p>
                </li>
            </ul>
        </div>

        <div class="right-banner right-signup-form">
            <h4>Sign Up</h4>
            <?php echo form_open('register', array('class' => '', 'id' => 'sign-up')) ?>
            <div class="ajax_report alert display-hide" role="alert" style="margin-left:0px;width:320px;margin-bottom:10px;"><span class="close-message"></span><div class="ajax_message">Hello Message</div></div>
            <div class="form-group1">
                <div class="my-div"><input type="text" placeholder="First Name" class="form-control my-txt-fleid width50" name="first_name"><input type="text" placeholder="Last Name" class="form-control my-txt-fleid width50 right-pull mr-rite0" name="last_name"></div>
            </div>
            <div class="form-group1">
                <div class="my-div"><input type="text" placeholder="Company" class="form-control my-txt-fleid" name="company_name"></div>
            </div>
            <div class="form-group1">
                <div class="my-div"><p class="check">    <input type="checkbox" id="test1" name="as_company">    <label for="test1">Use company name</label>  </p></div>
            </div>
            <div class="form-group1">
                <div class="my-div"><input type="text" placeholder="Email address" class="form-control my-txt-fleid" name="email"></div>
            </div>
            <div class="form-group1">
                <div class="my-div"><input type="password" id="signPassword" placeholder="Password" class="form-control my-txt-fleid width50" name="password"><input type="password" placeholder="Confirm Password" class="form-control my-txt-fleid width50 right-pull mr-rite0" name="confirm_password"></div>
            </div>
            <!--div  class="form-group1">
                <div class="my-div">
                    <select class="form-control my-txt-fleid" name="country" id="country" onChange="getStateByCountryId(this.value)">
                        <option value="">Select Country</option>
            <?php //foreach ($allcountry as $value) { ?>
                            <option value="<?//= $value->id ?>"><?//= $value->country_name ?></option>
            <?php //} ?>
                    </select>
                </div>
            </div-->
            <div id="regions" style="display:none">
                <div  class="form-group1">
                    <div class="my-div">
                        <select class="form-control my-txt-fleid" name="state" id="state" onChange="getCityByStateId(this.value)">
                            <option value="">Select State</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group1">
                <div class="my-div">
                    <!--<input type="text" placeholder="code#" class="form-control my-txt-fleid country_code" name="country_code" style="width:100px">-->
                    <input type="text" placeholder="Mobile#" class="form-control my-txt-fleid phone_us" name="mobile" style=""></div>
            </div>
            <div class="form-group1">
                <div class="my-div">
                    <p class="">   
                        <input type="checkbox" name="terms" id="" style="position: relative; left: 0px; top: 3px;">
                        Agree to our <a href="<?php echo base_url('terms-and-conditions'); ?>"><span class="changecolor">Terms & Conditions</span></a>
                    </p>
                </div>
            </div>

            <input type="submit" class="formsign-bttn" value="SIGN UP"/> 
            <?= form_close(); ?>
        </div>
    </div>
</section>
<section class="dr-content ">
    <section class="howit-works">
        <div class="container">

            <div class="how-works-inner">
                <h2>How it works</h2>
                <div class="one-third-wrap">
                    <div class="one-third">
                        <div class="icon">
                            <img src="<?= $this->config->item('templateassets') ?>images/w1.png">
                        </div>
                        <p>ADD PROPERTY</p>
                    </div>
                    <div class="one-third">
                        <div class="icon">
                            <img src="<?= $this->config->item('templateassets') ?>images/w2.png">
                        </div>
                        <p>ADD TENANT</p>
                    </div>
                    <div class="one-third last">
                        <div class="icon">
                            <img src="<?= $this->config->item('templateassets') ?>images/w3.png">
                        </div>
                        <p>ADD tSERVICES</p>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="how-workd-checkout">
                    <div class="tick">
                        <img src="<?= $this->config->item('templateassets') ?>images/tick.png" alt="">
                    </div>
                    <span>Checkout</span>
                </div>

                <div class="download-app">
                    <label>Welcome message to Tenant</label>
                    <a href="javascript:void(0)" style="cursor:default;">Download your app</a>
                </div>

            </div>


            <div class="confirm-approve">
                <div class="one-half">
                    <div class="t-maintain">
                        <div class="t-left">
                        </div>
                        <div class="t-outer">
                            <div class="circle">
                                <img src="<?= $this->config->item('templateassets') ?>images/m1.png" alt="">
                            </div>
                            <p>tMaintenance</p>
                            <a href="<?php echo site_url('page/services'); ?>">More Info</a>
                        </div>
                    </div>
                </div>

                <div class="one-half last">
                    <div class="t-message">
                        <div class="t-outer">
                            <div class="circle">
                                <img src="<?= $this->config->item('templateassets') ?>images/m2.png" alt="">
                            </div>
                            <p>tMessaging</p>
                            <a href="<?php echo site_url('page/services'); ?>">More Info</a>
                        </div>
                        <div class="t-right">
                        </div>
                    </div>
                </div>
                <div class="clear"></div>
            </div>

        </div>
    </section>



    <section class="tantent-more">
        <div class="more-icon">
            <a href="javascript:void(0)">MORE</a> 
        </div>
        <div class="tantent-more-outer">
            <div class="container">
                <div class="tantent-more-inner">
                    <ul>
                        <li><img src="<?= $this->config->item('templateassets') ?>images/s1.png" alt=""></li>
                        <li><img src="<?= $this->config->item('templateassets') ?>images/s2.png" alt=""></li>
                        <li><img src="<?= $this->config->item('templateassets') ?>images/s3.png" alt=""></li>
                    </ul>

                    <div class="download-the-app">
                        <p>App for Tenants is available on</p>
                        <div class="icon_container">
                            <span>
                                <img src="<?= $this->config->item('templateassets') ?>images/appstore.png" alt="appStore">  
                            </span>
                            <span>
                                <img src="<?= $this->config->item('templateassets') ?>images/playstore.png" alt="playStore">
                            </span>
                        </div>
                           <!-- <h2>Download the app</h2>
                            <a href="#" class="app-store">app store</a>
                            <a href="#" class="google-play">google play</a> -->
                    </div>


                </div>
            </div>
        </div>
    </section>




    <section class="dr-testimonial">
        <div class="container test-part">
            <h2>Why Use <strong>TenantTag?</strong></h2>
			<ul class="usetenantag">
				<li><img src="<?= $this->config->item('templateassets') ?>images/money.png" alt="money">To Save Money!</li>
				<li><img src="<?= $this->config->item('templateassets') ?>images/tenaticon.png" alt="tenants">To Retain Tenants!</li>
				<li><img src="<?= $this->config->item('templateassets') ?>images/life.png" alt="easier">To Make Life Easier!</li>
				<li><img src="<?= $this->config->item('templateassets') ?>images/time.png" alt="time">To Save Time!</li>
				<li><img src="<?= $this->config->item('templateassets') ?>images/efficently.png" alt="efficiently">To Manage More Efficiently!</li>
				<li><img src="<?= $this->config->item('templateassets') ?>images/communication.png" alt="communication">To Improve Communication!</li>
			</ul>
			<div class="sifnup-noww">
<a href="#">Sign up Now</a>
</div>
			
            <!--<div class="testimonial">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit<br>
                    in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                <span class="client-name">- Jeffrey Zeldman</span>
            </div>
            <div class="testimonial1">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim<br> veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit <br>esse cillum dolore eu, fugiat nulla pariatur.</p>
                <span class="client-name">- Paul Boag</span>
            </div>-->
			
			
			
        </div>
    </section>

    <script>
        $(document).ready(function () {
            $(document).delegate('.more-icon a', 'click', function () {
                if ($(".more-icon").hasClass("arrow_up")) {
                    $('.more-icon').removeClass('arrow_up');
                } else {
                    $('.more-icon').addClass('arrow_up');
                }
                $(".tantent-more-outer").slideToggle("slow");
            });
        });
    </script>




</section>

