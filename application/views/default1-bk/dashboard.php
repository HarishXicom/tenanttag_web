<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.5/css/bootstrap-dialog.min.css" rel="stylesheet" type="text/css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.5/js/bootstrap-dialog.min.js"></script>
<script type="text/javascript" src="<?= $this->config->item('templateassets') ?>js/jquery.validate.min.js"></script>   
<?php if ($this->session->flashdata('addmoreprop')) { ?>
    <script>
        swal({
            title: "Alert",
            text: 'Do you want to add another property?',
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, add property!",
            cancelButtonText: "No",
            closeOnConfirm: false
        },
        function () {
            window.location.href = "<?php echo base_url('add-property'); ?>"
        });
    </script>
    <?php
}
?>
<section class="signup-section2 dashborad-page">
    <div class="container">
        <!--div class="block-area blk_cnt">
            <a class="my-block" href="javascript:;">
                <h1><?php //echo $countAllProperties ?></h1>
                <p>Properties </p>
            </a>
            <a class="my-block" href="javascript:;">
                <h1><?php //echo $countOccupiedProperties ?></h1>
                <p>Occupied</p>
            </a>
            <a class="my-block" href="javascript:;">
                <h1><?php //echo $countVacantProperties ?></h1>
                <p>vacant</p>
            </a>
            <a class="my-block mr-rite0" href="javascript:;">
                <h1><?= $totalTenants ?></h1>
                <p>Tenants</p>
            </a>
        </div-->
                    <!-- <div class="ajax_report alert alert-success" role="alert" style="margin-bottom: 10px; margin-left: 0px; width: 500px; position: relative; top: 100px;">
                        <span class="close-message"></span>
                        <div class="ajax_message"><?php echo $this->session->userdata('stripe_payment'); ?></div>
                    </div><br> -->

            <?php 
                $userdata = $this->session->userdata(); 
                if(isset($userdata['stripe_payment'])){                   ?>
                    <script> 
                    $(document).ready(function(){
                        var current_tmaintenance = '<?php echo $this->session->userdata("current_tmaintenance"); ?>';
                        var current_tmessaging = '<?php echo $this->session->userdata("current_tmessaging"); ?>';
                        var current_tpay = '<?php echo $this->session->userdata("current_tpay"); ?>';

                        if (current_tmessaging != 'Yes'){
                            $('.is_tMessaging').html('');
                            $('.is_tMessaging').html('tText Inactive');
                            $('.is_tMessaging').addClass('my-cross');
                        }else{
                            $('.is_tMessaging').html('');
                            $('.is_tMessaging').html('tText Active');
                            $('.is_tMessaging').removeClass('my-cross');
                        }
                        if (current_tmaintenance != 'Yes'){
                            $('.is_tMaintence').html('');
                            $('.is_tMaintence').html('tMaintence Inactive');
                            $('.is_tMaintence').addClass('my-cross');
                        }else{
                            $('.is_tMaintence').html('');
                            $('.is_tMaintence').html('tMaintence Active');
                            $('.is_tMaintence').removeClass('my-cross');
                        }
                        if (current_tpay != 'Yes'){
                            $('.is_tPay').html('');
                            $('.is_tPay').html('is_tPay Inactive');
                            $('.is_tPay').addClass('my-cross');
                        }else{
                            $('.is_tPay').html('');
                            $('.is_tPay').html('is_tPay Active');
                            $('.is_tPay').removeClass('my-cross');
                        }
                        $(".popup-block").show();
                        $(".next-step-success-message").show();
                    });
                    </script>
                    <?php $this->session->unset_userdata('stripe_payment'); ?>
                    <?php $this->session->unset_userdata('current_tmessaging'); ?>
                    <?php $this->session->unset_userdata('current_tmaintenance'); ?>
                    <?php $this->session->unset_userdata('current_tpay'); ?>
            <?php } ?>
        
        <div id="properties-listing">
            <div class="signup">
                <div class="btn-part">
                    <a href="javascript:;" class="active left property-by-status" data-value="" style="width:108px;;">All </a>
                    <a href="javascript:;" class="unactive left property-by-status" data-value="vacant" style="width:107px;">Vacant </a>
                    <a href="javascript:;" class="unactive right property-by-status" data-value="occupied" style="width:107px;">Occupied</a>
                </div>
                <a class="btn-tenant add-property" href="<?= site_url('add-property') ?>"><i class="fa fa-plus"></i>Add Property</a>
            </div>
            <div class="table-content signup-table dashboard-table">
                <table class="my-table">
                    <thead>
                        <tr>
                            <th>Address </th>
                            <th>Unit#  </th>
                            <th>Rent Balance</th>
                            <th>Tenants</th>
                            <th>Messages</th>
                            <th>Vendors</th>
                            <th><a href="javascript:;" class="dash-pop shwtmsg">tText</a></th>
                            <th><a href="javascript:;" class="dash-pop shwtmntnce">tMaintenance</a></th>
                            <th><a href="javascript:;" class="dash-pop shwtpay">tPay</a></th>
                        </tr>
                    </thead>
                    <tbody>

                        <?php foreach ($allProperties as $value) { ?>
                            <tr>
                                <td> <a href="<?= site_url('my-properties') ?>#tabs-<?php echo $value->prop_id; ?>">  <?= $value->city ?>, <?= $value->address1 ?> <?= $value->address2 ?> </a> </td>
                                <td> <a href="<?= site_url('my-properties') ?>#tabs-<?php echo $value->prop_id; ?>"><?= $value->unit_number ?> </a> </td>
                                <td>  <?php echo $value->rent_amount; ?> </td> 
                                <td>
                                    <?php if ($value->tenants == 0) { ?>
                                        <a href="javascript:;" id="<?php echo $value->prop_id; ?>" data-toggle="modal" data-target="#addtenantModal" class="<?php if ($value->tenants == 0) { ?>zero<?php } ?>"> <?= $value->tenants ?> </a> 
                                    <?php } else { ?>
                                        <a href="<?= site_url('tenants') ?>#tabs-<?php echo $value->prop_id; ?>" class="<?php if ($value->expiry == 'Yes') { ?>expiring<?php } ?>"> <?= $value->tenants ?> </a> 
                                    <?php } ?>
                                </td>    
                                <td> <a href="<?= site_url('message') ?>#tabs-<?php echo $value->prop_id; ?>"> <?php echo $value->messages ?> </a> </td>
                                <td> <a href="<?= site_url('vendors') ?>#tabs-<?php echo $value->prop_id; ?>"> <?= $value->vendors ?> </a> </td>
                                <td> 
                                    <?php
                                    $verification_status = $this->common_model->getSingleFieldFromAnyTable('verification_status', 'mem_id', $this->session->userdata('MEM_ID'), 'tbl_members');
                                    if ($verification_status == 'No') {
                                        $clss = 'intservices';
                                    } else {
                                        $clss = 'tServices';
                                    }
                                    ?>
                                    <div class="togle <?php if ($value->tmessaging == 'Yes') { ?>stop<?php } ?> <?php echo $clss; ?>" id="tmessaging-<?= $value->prop_id ?>" data-type="tmessaging" data-value="<?= $value->prop_id ?>" data-property="<?php if ($value->tmessaging == 'Yes') { ?>No<?php } else { ?>Yes<?php } ?>">
                                        <span class="check"></span>
                                    </div>
                                </td>
                                <td> 
                                    <div class="togle <?php if ($value->tmaintenance == 'Yes') { ?>stop<?php } ?> <?php echo $clss; ?>" id="tmaintenance-<?= $value->prop_id ?>" data-type="tmaintenance" data-value="<?= $value->prop_id ?>" data-property="<?php if ($value->tmaintenance == 'Yes') { ?>No<?php } else { ?>Yes<?php } ?>">
                                        <span class="check"></span>
                                    </div>
                                </td>
                                <td> 
                                    <div class="togle <?php if ($value->tpay == 'Yes') { ?>stop<?php } ?> <?php echo $clss; ?>" id="tpay-<?= $value->prop_id ?>" data-type="tpay" data-value="<?= $value->prop_id ?>" data-property="<?php if ($value->tpay == 'Yes') { ?>No<?php } else { ?>Yes<?php } ?>">
                                        <span class="check"></span>
                                    </div>
                                </td>
                            </tr>
<?php } ?>
                    </tbody>
                </table>
<?php if (!$allProperties) { ?>
                    <center></br></br></br></br></br></br>
                        <span class="no-record"> No Properties found...</span>
                    </center>
<?php } ?>
            </div>
        </div>	  
    </div>
</section>
<div class="modal fade" id="addtenantModal">
    <div class="modal-dialog" style="width: 55%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Add New Tenant</h4>
            </div>
            <div class="modal-body">
<?php echo form_open('', array('class' => 'tenant-detai', 'id' => 'add_tenant_form')) ?>
                <ul class="my-list border0">
                    <li>

                        <span> <label class="label1">First name</label><input type="text" class="form-control tenant-txtfield" value="" name="first_name"  /></span>
                        <span><label class="label1">Last Name</label><input type="text" class="form-control tenant-txtfield" value="" name="last_name"/><label class="label2"></label></span>
                        <span><label class="label1">DOB</label>
                            <input type='text' class="form-control tenant-txtfield" id='datetimepicker4' readonly="true" name="dob"/>
                        </span>
                        <span> <label class="label1">Mobile</label><input type="text" class="form-control tenant-txtfield phone_us" value="" name="mobile_no"/><label class="label2">  </label></span>
                        <span><label class="label1">Language</label>
                            <select class="form-control my-txt-fleid" name="preferred_language" >
                                <option value="">Select Language</option>
                                <option value="English">English</option>
                                <option value="Spanish">Spanish</option>
                            </select>
                        </span>
                    </li>
                    <li>

                        <span><label class="label1">Email</label><input type="text" class="form-control tenant-txtfield" value="" name="email"/><label class="label2"></label></span>
                        <!--<span><label class="label1">Password</label><input type="password" class="form-control tenant-txtfield" value="" name="password"/><label class="label2"></label></span>-->
                        <span><label class="label1">Lease Start  </label>
                            <input type="text" id="lease_from" class="form-control tenant-txtfield" value="" name="lease_start_date"/></span>
                        <span> <label class="label1">Lease End</label>
                            <input type="text" id="lease_to" class="form-control tenant-txtfield" value="" name="lease_end_date"/></span>
                        <span><label class="label1">Rent</label><input type="text" class="form-control tenant-txtfield" value="" name="rent_amount" /></span>
                        <span><label class="label1">Rent Due</label>
                            <select class="form-control my-txt-fleid " required="true"  name="due_date">
                                <option value="">Rent Due</option>
<?php for ($j = 1; $j < 31; $j++) { ?>
                                    <option value="<?php echo $j; ?>"><?php echo ordinal($j); ?></option>
<?php } ?>

                            </select>

                        </span>

                    </li>
                    <li>
                        <span><label class="label1">Late Fee Type</label>
                            <select class="form-control my-txt-fleid late_fee_type_cls" required="true"   name="late_fee_type">
                                <option value="">Late Fee type</option>
                                <option value="Daily charge">Daily charge</option>
                                <option value="One time">One time</option>
                            </select>

                        </span>
                        <span class="late_fee_sh"><label class="label1">Late Fee</label><input type="text" class="form-control tenant-txtfield" value="" name="late_fee"/></span>

                    </li>

                </ul>
                <input type="hidden" name="property_id" value="" id="property_id">
                <!--                <h2 class="lease-doc">Tenant Document</h2>
                                <ul class="upload-file">
                                    <li class="browse"><input type="file" name="lease"></li>
                                </ul>-->



            </div>
            <div class="modal-footer" style="border:none !important">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Add Tenant</button>

            </div>
<?php echo form_close(); ?>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div class="my-popup save-later incomplete_pop tmsgs" style="display:none">
        <div class="popup-cont">
            <div class="popup-head">
                <span class="close"></span>
            </div>
            <div class="popup-body">

               <ul>	
                <li class="message-img"><h4><span>t</span>Text<sup></sup></h4>
                    <p>Improves communications with tenants by using as an automated texting system to deliver customized messages based on property, tenant, and lease information.</p>
                </li>

                
            </ul>
              
            </div>
            <div class="popup-footer">
                <div class="center">
                </div>
            </div>
        </div>
    </div>
    <div class="my-popup save-later incomplete_pop tmntnce" style="display:none">
        <div class="popup-cont">
            <div class="popup-head">
                <span class="close"></span>
            </div>
            <div class="popup-body">

               <ul>	
              

                <li class="mainstaince-img">
                    <h4><span>t</span>Maintenance<sup>*Free*</sup></h4>
                    <p>Simplifies the submission, delivery, and approval of maintenance requests through an innovative, mobile solution.</p>
                </li>
            </ul>
              
            </div>
            <div class="popup-footer">
                <div class="center">
                </div>
            </div>
        </div>
    </div>
    <div class="my-popup save-later incomplete_pop tpay" style="display:none">
        <div class="popup-cont">
            <div class="popup-head">
                <span class="close"></span>
            </div>
            <div class="popup-body">

               <ul> 
              

                <li class="mainstaince-img">
                    <h4><span>t</span>Pay<sup></sup></h4>
                    <p>Make rent collection more efficent by allowing tenants to pay their rent on their mobile device.</p>
                </li>
            </ul>
              
            </div>
            <div class="popup-footer">
                <div class="center">
                </div>
            </div>
        </div>
    </div>

<script>
    $(document).ready(function () {
        $('#datetimepicker4').datepicker({maxDate: 0, defaultDate: "01/01/1985", changeMonth: true,
            changeYear: true,yearRange: "-80:+0" });
        $("#lease_from").datepicker({
            defaultDate: "+1w",
         //   minDate: 0,
            changeMonth: true,
            changeYear: true,
            onClose: function (selectedDate) {
                $("#lease_to").datepicker("option", "minDate", selectedDate);
            }
        });
        $("#lease_to").datepicker({
            defaultDate: "+1w",
            changeMonth: true,
            changeYear: true,
            minDate: 0,
            onClose: function (selectedDate) {
                $("#lease_from").datepicker("option", "maxDate", selectedDate);
            }
        });
        $('#duedate').datepicker();
    });
    $(document).on("click", ".zero", function () {
        $("#property_id").val($(this).attr('id'));
    });
    $(document).on("click", ".property-by-status", function () {
        var type = $(this).attr('data-value');
        if (type == '' || type == null)
            type = "NULL";
        var postUrl = siteUrl + 'dashboard/' + type;
        $.ajax({
            url: postUrl,
            dataType: 'json',
            beforeSend: function () {
                $('#wait-div').show();
            },
            success: function (response) {
                if (response.success)
                {
                    $('#properties-listing').html('');
                    $('#properties-listing').html(response.html);
                }
                 $('#wait-div').hide();
            },
            error: function () {
                 $('#wait-div').hide();
                alert('server error');
            },
        });
    });


    $(document).on("click", ".intservices", function () {
        swal("Error!", 'Please confirm your email address. tServices will not be active until confirmation is complete.', "error");
    });

    $(document).on("click", ".tServices", function () {
        var property_id = $(this).attr('data-value');
        var value = $(this).attr('data-property');
        var type = $(this).attr('data-type');
        var postUrl = siteUrl + 'change-tservices-status/' + property_id + '/' + type + '/' + value;
        var message = '';
        if (type == 'tmessaging' && value == "No")
            message = 'Turing off tText will not allow you to send automated text messages to tenants. Do you wish to continue?';
        if (type == 'tmessaging' && value == "Yes")
            message = 'Turn on tText?';
        if (type == 'tmaintenance' && value == "No")
            message = 'Turning off tMaintenance will not allow the Tenants to request maintenance on their mobile devices.Do you wish  to continue?';
        if (type == 'tmaintenance' && value == "Yes")
            message = 'Turn on tMaintenance?';
        if (type == 'tpay' && value == "No")
            message = 'Turning off tPay will not allow  Tenants to pay their rent on their mobile devices. Do you wish  to continue?';
        if (type == 'tpay' && value == "Yes")
            message = 'Turn on tPay?';
        BootstrapDialog.confirm({
            title: 'Confirm',
            message: message,
            callback: function (result) {
                if (result)
                {
                    $.ajax({
                        url: postUrl,
                        dataType: 'json',
                        success: function (response) {
                            if (response.success)
                            {
                                if(response.redirect_to != ''){
                                    window.location = response.redirect_to;
                                }else{
                                    if (value == 'Yes')
                                    {
                                        $('#' + type + '-' + property_id).addClass('stop');
                                        $('#' + type + '-' + property_id).attr('data-property', 'No');
                                    }
                                    else
                                    {
                                        $('#' + type + '-' + property_id).removeClass('stop');
                                        $('#' + type + '-' + property_id).attr('data-property', 'Yes');
                                    }
                                }
                            }
                        },
                        error: function () {
                            alert('server error');
                        },
                    });
                }
                else
                {
                    return false;
                }
            }
        });


    });



</script>	
