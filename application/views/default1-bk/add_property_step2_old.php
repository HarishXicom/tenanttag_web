
<script>
$(document).ready(function(){
	//~ var date = new Date();
	//~ date.setMonth(date.getYear() - 18);
	
	 $(document).on('click',".datepicker", function(){
		$(this).removeClass("hasDatepicker");
		$(this).datepicker();
		$(this).datepicker("show");
	 });
	//$('#datepicker').datepicker('setDate', '12/31/2000');
	
	$(document).on('click',".lease-start-date", function(){
		$(this).removeClass("hasDatepicker");
		$(this).datepicker({
	      //  minDate: 0,
	        onSelect: function(selected) {
	          $(".lease-end-date").datepicker("option","minDate", selected)
	          $(".due-date").datepicker("option","minDate", selected)
	        }
	    });
	    $(this).datepicker("show");
	});
	
	$(document).on('click',".lease-end-date", function(){
		$(this).removeClass("hasDatepicker");
		$(this).datepicker({
	        minDate: 0,
	        onSelect: function(selected) {
	           $(".lease-start-date").datepicker("option","maxDate", selected)
	           $(".due-date").datepicker("option","maxDate", selected)
	        }
	    });
	    $(this).datepicker("show");
	});
	
	$(document).on('click',".due-date", function(){
		$(this).removeClass("hasDatepicker");
		$(this).datepicker();
		$(this).datepicker("show");
	});
	     
});
</script>
<div class="level">
   <div class="container">
              <div class="level-indicator">
                 <img src="<?=$this->config->item('templateassets')?>images/level.png" alt="" />
                 <ul>
                     <li> Property Info</li>
                     <li class="blue-col1">Tenants</li>
                       <li>tServices</li>
                 </ul>
              </div>
          </div>
      </div>
      <section class="signup-section2">
          <div class="container">
              <h2 class="tenant-txt">Add Tenant</h2></br></br></br></br></br>
              <?php echo form_open('',array('class'=>'ajaxForm','id'=>'property-add-step1'))?>
				<div class="ajax_report alert display-hide" role="alert" style=" margin-top: 100px; margin-bottom:10px;width:1169px;"><span class="close-message"></span><div class="ajax_message">Hello Message</div></div>
              
              <div class="my-panel">
                  <div class="panel-head">
                      <div class="panel-title">
                          <h1>Tenant</h1>
                      </div>
                  </div>
                  <div class="sub-panel-head">
                      <p>    <input type="checkbox" id="test1" class="is_vacant" name="is_vacant" value="Yes"/>    <label for="test1">Vacant</label>  </p>
                         
                     
                  </div>
                  <div class="panel-body all-tenants">
                      <div id="tenant_list">

                      

                      <ul class="form-group" id="tenant-1">
                          <li><input type="text" class="form-control my-txt-fleid" placeholder="Tenants First Name" name="first_name_1"/></li>
                            <li><input type="text" class="form-control my-txt-fleid" placeholder="Tenants Last Name" name="last_name_1"/></li>
                            <li><input readonly type="text" class="form-control my-txt-fleid width50 datepicker" placeholder="DOB" name="dob_1" data-date-format="dd-mm-yyyy"/> 
                            <span class="upload-btn">Upload Lease<input type="file" name="upload_lease_1"></span>
                            </li>
                            
                            <li><input type="email" class="form-control my-txt-fleid" placeholder="Email" name="email_1" autocomplete="off"/></li>
                            <li><input type="password" class="form-control my-txt-fleid width50" placeholder="Pasword" name="password_1" autocomplete="off"/>
                            <input type="text" class="form-control my-txt-fleid width50 right-pull mr-rite0" placeholder="Mobile" name="mobile_1"/> </li>
                            <li><select class="form-control my-txt-fleid" name="preferred_language_1">
								<option value="">Select Language</option>
								<option value="English">English</option>
								<option value="Spanish">Spanish</option>
								
							</select>
							</li>
                            <li>
								<input type="text" class="form-control my-txt-fleid width50 lease-start-date" placeholder="Lease Start" name="lease_start_date_1"/>
								<input type="text" class="form-control my-txt-fleid width50 right-pull mr-rite0 lease-end-date" placeholder="Lease End" name="lease_end_date_1"/>
							</li>
                            <li>
								<input type="text" class="form-control my-txt-fleid width50" placeholder="Rent Amount" name="rent_amount_1"/>
								<input type="text" class="form-control my-txt-fleid width50 right-pull mr-rite0 due-date" placeholder="Due Date" name="due_date_1"/>
							</li>
                            <li><input type="text" class="form-control my-txt-fleid width50" placeholder="Late Fee" name="late_fee_1"/> </li>
                          
                      </ul>
                      </div>
                      <input type="hidden" value="1" name="total_tenant" class="tenant_no">
                       <a href="javascript:;" class="edit-btn mr-btm30 add-tenant"><i class="fa fa-plus"></i>Add Tenant</a>
                  </div>
                  
              </div>
              
              <div class="my-panel">
                  <div class="panel-head">
                      <div class="panel-title">
                          <h1>Add Your Mailing Address for Rent Payment</h1>
                      </div>
                  </div>
                  <div class="panel-body">
                      <ul class="form-group">
                          <li><input type="text" class="form-control my-txt-fleid" placeholder="Address 1" name="address_1" value="<?=$propertyDetail->address1?>"/></li>
                            <li><input type="text" class="form-control my-txt-fleid" placeholder="Address 2" name="address_2" value="<?=$propertyDetail->address2?>"/></li>
                            <li>
								<input type="text" class="form-control my-txt-fleid width50" placeholder="Unit" name="" readonly/>
								<input type="text" class="form-control my-txt-fleid right-pull mr-rite0" placeholder="Unit" name="unit" value="<?=$propertyDetail->unit_number?>" style="width:150px;"/>
							</li>
							<li><input type="text" class="form-control my-txt-fleid" placeholder="city" name="city" value="<?=$propertyDetail->city?>"/></li>
							<li>
							<select class="form-control my-txt-fleid" name="state">
								<option value="">Select State</option>
								<?php foreach($allRegion as $value) { ?>
									<option value="<?=$value->region_id?>" <?php if($propertyDetail->state_id == $value->region_id) { ?> selected<?php } ?>><?=$value->region_name?></option>
								<?php } ?>
							</select>
							</li>
							<li>
							<input type="text" class="form-control my-txt-fleid right-pull mr-rite0" placeholder="Zip" name="zip" value="<?=$propertyDetail->zip?>"/>
							</li>
                      </ul>
                  </div>
                  
              </div>
              
              <div class="center">  <input type="submit" class="next" value="NEXT" style=""/></div>
              <?php echo form_close();?>
          </div>
      </section>

<script>
	$(document).on('click','.add-tenant',function(event) { 
		 var assets	=	'<?=$this->config->item('templateassets')?>';
		 var tenant_no	=	$('.tenant_no').val();
		 tenant_no	=	parseInt(tenant_no)+1;
		 var dataToAppend	=	'<ul class="form-group" id="tenant-'+tenant_no+'">';    
		 dataToAppend+=	'<li><input type="text" class="form-control my-txt-fleid" placeholder="Tenants First Name" name="first_name_'+tenant_no+'"/></li>';    
		 dataToAppend+=	'<li><input type="text" class="form-control my-txt-fleid" placeholder="Tenants Last Name" name="last_name_'+tenant_no+'"/></li>';    
		 dataToAppend+=	'<li><input type="text" class="form-control my-txt-fleid width50 datepicker" placeholder="DOB" name="dob_'+tenant_no+'"/> <span class="upload-btn">Upload Lease<input type="file" name="upload_lease_'+tenant_no+'"></span><a href="javascript:;" class="delete-row" id="cross'+tenant_no+'" data-value="'+tenant_no+'" style="margin-left:445px;"><img src="'+assets+'images/cross.png" alt=""/></a></li>';    
		 dataToAppend+=	'<li><input type="email" class="form-control my-txt-fleid" placeholder="Email" name="email_'+tenant_no+'" autocomplete="off"/></li>';    
		 dataToAppend+=	'<li><input type="password" class="form-control my-txt-fleid width50" placeholder="Pasword" name="password_'+tenant_no+'" autocomplete="off"/>';    
		 dataToAppend+=	'<input type="text" class="form-control my-txt-fleid width50 right-pull mr-rite0" placeholder="Mobile" name="mobile_'+tenant_no+'"/> </li>';    
		 dataToAppend+=	'<li><select class="form-control my-txt-fleid" name="preferred_language_'+tenant_no+'"><option value="">Select Language</option><option value="English">German</option><option value="German">English</option></select></li>';    
		 dataToAppend+=	'<li><input type="text" class="form-control my-txt-fleid width50 lease-start-date" placeholder="Lease Start" name="lease_start_date_'+tenant_no+'"/>';    
		 dataToAppend+=	'<input type="text" class="form-control my-txt-fleid width50 right-pull mr-rite0 lease-end-date" placeholder="Lease End" name="lease_end_date_'+tenant_no+'"/></li>';    
		 dataToAppend+=	'<li><input type="text" class="form-control my-txt-fleid width50" placeholder="Rent Amount" name="rent_amount_'+tenant_no+'"/><input type="text" class="form-control my-txt-fleid width50 right-pull mr-rite0 due-date" placeholder="Due Date" name="due_date_'+tenant_no+'"/></li>';    
		 dataToAppend+=	'<li><input type="text" class="form-control my-txt-fleid width50" placeholder="Late Fee" name="late_fee_'+tenant_no+'"/> </li>';    
		 dataToAppend+=	'</ul>';    
		 $('#tenant_list').append(dataToAppend);
		 $('.tenant_no').val(tenant_no);
		 
	});
	
	$(document).on('click','.is_vacant',function(){
		if(this.checked) 
		{
			$('.all-tenants').hide('slow');
		}
		else 
		{
			$('.all-tenants').show('slow');
		}
	});
	$(document).on('click','.delete-row',function(){
	var id	=	$(this).attr('data-value');
	$('#cross'+id).remove();
	$('#tenant-'+id).remove();
	 var tenant_no	=	$('.tenant_no').val();
	tenant_no	=	parseInt(tenant_no)-1;
	$('.tenant_no').val(tenant_no);
	});
</script>	
<script>
  $(document).on('click','.panel-head',function(){
		$(this).children().toggleClass('open');
		$(this).next().slideToggle();
	});
	$(document).ready(function(){
		//$('.panel-body').hide();
		$('.panel-body').first().show();
		});
  </script>
<style>
	.upload-btn > input {
    bottom: 0;
    cursor: pointer;
    font-size: 0;
    left: 0;
    opacity: 0;
    position: absolute;
    right: 0;
    top: 0;
    width: 100%;
}
</style>
