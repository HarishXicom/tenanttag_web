
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script>
    $(document).ready(function(){
        $("#link-btn").click(function(){
            $("#linked-acc").slideDown("slow");
        });
    });
</script>	
<div class="level">
    <div class="container">
        <div class="level-indicator">
            <img src="<?= $this->config->item('templateassets') ?>images/full-level.png" alt="" />
            <ul>
                <li> Property Info</li>
                <li>Tenants</li>
                <li  class="blue-col1">tServices</li>
            </ul>
        </div>
    </div>
</div>
<section class="signup-section2">
    <div class="container">
        <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
        <script type="text/javascript">  
          Stripe.setPublishableKey('pk_test_mo7Ze4S2ZLdYooQx7vl1fC9a');    
          function onSubmitDo () {      
            //$("#wait-div").show();
            Stripe.card.createToken( document.getElementById('payment-form'), myStripeResponseHandler );          
            return false;      
          };

          function myStripeResponseHandler ( status, response ) {      
            console.log( status );
            console.log( response );    
            if ( response.error ) {
              document.getElementById('payment-error').innerHTML = response.error.message;
              //$("#wait-div").hide();
            } else {
              var tokenInput = document.createElement("input");
              tokenInput.type = "hidden";
              tokenInput.name = "stripeToken";
              tokenInput.value = response.id;
              var paymentForm = document.getElementById('payment-form');
              paymentForm.appendChild(tokenInput);
              //console.log(paymentForm);
              //console.log($("#payment-form").serialize());
              var data = $("#payment-form").serialize();
              //var posturl = siteUrl + 'property/charge_by_stripe';
              var posturl = $( '#payment-form' ).attr( 'action' );
              var formClass = '#payment-form';
              $.ajax({
                        url: posturl,
                        data: data,
                        type: "POST",
                        dataType: 'json',
                        beforeSend: function () {
                            $('#wait-div').show();
                        },
                        success: function (response) {
                          if(response.success == true){
                            $(formClass).find('.ajax_report').addClass('alert-success').css("display",'block').children('.ajax_message').html(response.message).css("display","block");
                                scrollToElement(formClass, 1000);
                                setTimeout(function () {
                                    window.location.href = response.redirect_url;
                                }, 700);
                          }else{
                            $(formClass).find('.ajax_report').addClass('alert-danger').css("display",'block').children('.ajax_message').html(response.message).css("display","block");
                                scrollToElement(formClass, 1000);
                            setTimeout(function () {
                                    //window.location.href = response.redirect_url;
                                }, 700);
                          }
                        },
                        error: function (data) {
                            alert("Server Error.");
                            return false;
                        }
                    });

            }      
         };      
        </script>

        <?php if(isset($_REQUEST['propid']) && isset($_REQUEST['field'])){ ?>


                <form action="<?php echo base_url('property/tservices_charge_by_stripe'); ?>" method="POST" id="payment-form" onsubmit="return onSubmitDo()">   
                    <div class="ajax_report alert display-hide" role="alert" style="margin-bottom: 10px; margin-left: 0px; width: 500px; position: relative; top: 100px;">
                      <span class="close-message"></span>
                      <div class="ajax_message">Hello Message</div>
                    </div>
                    <div class="tpay-acc-setup">
                         <div class="shopping-cart-sec">
                          <h2>Shopping Cart</h2>
                          <div class="shopping-cart-sec-in">
                              <h4>Summary of Services</h4>
                                  <?php $field = $_REQUEST['field']; ?>
                                  <table border="1" cellpadding="0" cellspacing="0">
                                      <thead>
                                          <tr>
                                              <th>Services</th>
                                              <th>Properties</th>
                                              <th>Status</th>
                                              <th>Price</th>
                                              <th>Total</th>
                                          </tr>
                                      </thead>
                                      <tbody>
                                        <?php if($_REQUEST['field'] == 'tmessaging' || $_REQUEST['field'] == 'tmaintenance'){ ?>
                                                <?php $price = amount_formated(TMAINTENANCE_TTEXT_PRICE); ?>
                                                <tr>
                                                    <td>tMaintenance / tText</td>
                                                    <td>1-5</td>
                                                    <td>
                                                    Active
                                                    </td>
                                                    <td>$<?php echo amount_formated(TMAINTENANCE_TTEXT_PRICE); ?>/mth</td>
                                                    <td>$<?php echo amount_formated(TMAINTENANCE_TTEXT_PRICE); ?></td>
                                                </tr>
                                                <tr>
                                                    <td>Total</td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>-</td>
                                                </tr>
                                        <?php }elseif($_REQUEST['field'] == 'tpay'){ ?>
                                                <?php $price = amount_formated(TMAINTENANCE_TTEXT_PRICE); ?>
                                              <tr>
                                                  <td>tPay</td>
                                                  <td>1</td>
                                                  <td>
                                                    Active
                                                  </td>
                                                  <td>$<?php echo amount_formated(PER_PROPERTY_AMOUNT); ?>/mth/prop</td>
                                                  <td>$<?php echo amount_formated(PER_PROPERTY_AMOUNT); ?></td>
                                              </tr>
                                              <tr>
                                                  <td>Total</td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td><?php echo amount_formated(PER_PROPERTY_AMOUNT); ?></td>
                                              </tr>
                                        <?php } ?>
                                      </tbody>
                                  </table>
                                  </div>  
                          <?php //echo "<pre>"; print_r($memberRecord); ?>
                          <span style='color: red' class="error" id='payment-error'></span>                      
                          <div class="cart-form">                      
                                  <div class="cart-form-divide cart-form-section">
                                      <input type="hidden" value="<?php echo $price; ?>" name="amount"/>
                                      <input type="hidden" value="<?php echo $field; ?>" name="tservice"/>
                                      <input type="text" value="<?php echo $memberRecord->first_name; ?>" data-stripe="name" name="name" placeholder="First Name" />
                                      <input type="text" value="<?php echo $memberRecord->last_name; ?>" name="last_name" placeholder="Last Name" />
                                  </div>
                                  
                                  <div class="clearfix"></div>
                                  <div class="cart-form-section cart-form-sectionfour">
                                      <input type="text" name="" class="credit" style="width:49%;" data-stripe="number" placeholder="CC"  />
                                      <input type="text" readonly name="month_year" id="month_year"  style=""  placeholder="MM/YY "  />
                                      <input type="hidden"  id="exp_month" name="exp_month" style="" data-stripe="exp_month"   />
                                      <input type="hidden"  id="exp_year" name="exp_year" style="" data-stripe="exp_year"  />
                                      <input type="text" class="reset"  data-stripe="cvc" placeholder="Security Code - CVC">
                                  </div>
                                  <div class="clearfix"></div>
                                  <div class="cart-form-section cart-form-sectionfour cart-form-sectionfour2">
                                      <input type="text" value="<?php echo $memberRecord->address; ?>" data-stripe="address_line1" name="address_line1" placeholder="Address" />
                                       <input type="text" value="<?php echo $memberRecord->city_id; ?>"name="address_city" style="width:21%;" data-stripe="address_city" placeholder="City" />
                                       <input type="text"  value="<?php echo get_state_name($memberRecord->state_id); ?>" name="address_state" style="width:23.5%;" data-stripe="address_state" placeholder="State" />
                                      <input type="text" value="" class="reset" name="address_zip" data-stripe="address_zip" style="width:23.5%;" placeholder="Zip" />
                                  </div>
                                  <div class="clearfix"></div>
                                  
                                  <div class="cart-btns">                                      
                                      <input type="hidden" value="<?php echo $memberRecord->email; ?>" name="emailAddress">
                                      <input type="submit" name="" value="Confirm" />
                                  </div>
                          </div>    
                      </div>      
                    </div>          
                </form>



        <?php }else{ ?>



                    <form action="<?php echo base_url('property/charge_by_stripe'); ?>" method="POST" id="payment-form" onsubmit="return onSubmitDo()">   
                      <div class="ajax_report alert display-hide" role="alert" style="margin-bottom: 10px; margin-left: 0px; width: 500px; position: relative; top: 100px;">
                        <span class="close-message"></span>
                        <div class="ajax_message">Hello Message</div>
                      </div>
                      <div class="tpay-acc-setup">
                           <div class="shopping-cart-sec">
                            <h2>Shopping Cart</h2>
                            <div class="shopping-cart-sec-in">
                                <h4>Summary of Services</h4>
                                      <table border="1" cellpadding="0" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>Services</th>
                                            <th>Properties</th>
                                            <th>Status</th>
                                            <th>Price</th>
                                            <th>Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                      <?php $tServicesTmainTmess = array($property_info->tmaintenance,$property_info->tmessaging); ?>
                                      <?php if(in_array("Yes", $tServicesTmainTmess)){ ?>
                                        <tr>
                                            <td>tMaintenance / tText</td>
                                            <td>1-5</td>
                                            <td>
                                            <?php 
                                            echo !empty($property_info->tmaintenance) ? 'Active' : 'Inactive'; 
                                            echo ' / ';
                                            echo !empty($property_info->tmessaging) ? 'Active' : 'Inactive'; 
                                            ?>
                                            </td>
                                            <td>$<?php echo amount_formated(TMAINTENANCE_TTEXT_PRICE); ?>/mth</td>
                                            <td>$<?php echo amount_formated(TMAINTENANCE_TTEXT_PRICE); ?></td>
                                        </tr>
                                      <?php } ?>
                                      <?php if($property_info->tpay == 'Yes'){ ?>
                                        <tr>
                                            <td>tPay</td>
                                            <td>1</td>
                                            <td>
                                              <?php  echo !empty($property_info->tpay) ? 'Active' : 'Inactive'; ?>
                                            </td>
                                            <td><?php echo amount_formated(PER_PROPERTY_AMOUNT); ?>/mth/prop</td>
                                            <td>$<?php echo amount_formated(PER_PROPERTY_AMOUNT); ?></td>
                                        </tr>
                                      <?php } ?>
                                        <tr>
                                            <td>Total</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>$<?php echo amount_formated($total_amount); ?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>  
                            <?php //echo "<pre>"; print_r($memberRecord); ?>
                            <span style='color: red' class="error" id='payment-error'></span>                      
                            <div class="cart-form">                      
                                    <div class="cart-form-divide cart-form-section">
                                        <input type="hidden" name="amount"/>
                                        <input type="text" value="<?php echo $memberRecord->first_name; ?>" data-stripe="name" name="name" placeholder="First Name" />
                                        <input type="text" value="<?php echo $memberRecord->last_name; ?>" name="last_name" placeholder="Last Name" />
                                    </div>
                                    
                                    <div class="clearfix"></div>
                                    <div class="cart-form-section cart-form-sectionfour">
                                        <input type="text" name="" class="credit" style="width:49%;" data-stripe="number" placeholder="CC"  />
                                        <input type="text" readonly name="month_year" id="month_year"  style=""  placeholder="MM/YY "  />
                                        <input type="hidden"  id="exp_month" name="exp_month" style="" data-stripe="exp_month"   />
                                        <input type="hidden"  id="exp_year" name="exp_year" style="" data-stripe="exp_year"  />
                                        <input type="text" class="reset"  data-stripe="cvc" placeholder="Security Code - CVC">
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="cart-form-section cart-form-sectionfour cart-form-sectionfour2">
                                        <input type="text" value="<?php echo $memberRecord->address; ?>" data-stripe="address_line1" name="address_line1" placeholder="Address" />
                                         <input type="text" value="<?php echo $memberRecord->city_id; ?>"name="address_city" style="width:21%;" data-stripe="address_city" placeholder="City" />
                                         <input type="text"  value="<?php echo get_state_name($memberRecord->state_id); ?>" name="address_state" style="width:23.5%;" data-stripe="address_state" placeholder="State" />
                                        <input type="text" value="" class="reset" name="address_zip" data-stripe="address_zip" style="width:23.5%;" placeholder="Zip" />
                                    </div>
                                    <div class="clearfix"></div>
                                    
                                    <div class="cart-btns">                                        
                                        <a href="<?= site_url('add-property-step3') ?>" id="shoping-cart-cancel-btn" class="my-link">Back</a> &nbsp;&nbsp;&nbsp;&nbsp;
                                        
                                        <input type="hidden" value="<?php echo $memberRecord->email; ?>" name="emailAddress">
                                        <input type="submit" name="" value="Confirm" />
                                    </div>
                            </div>    
                        </div>      
                      </div>          
                  </form>
        <?php } ?>


        <script type="text/javascript">
        $(function() {
          $('#month_year').datepicker( {
              changeMonth: true,
              changeYear: true,
              showButtonPanel: true,
              dateFormat: 'mm/yy',
              yearRange: '2016:2100',
              onClose: function(dateText, inst) { 
                  $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
                  $('#exp_month').val(inst.selectedMonth);
                  $('#exp_year').val(inst.selectedYear);
              }
          });
      });
        </script>
        <style>
          .ui-datepicker-calendar {display: none;}
          .ui-datepicker-header{ width: 200px;}
          .ui-datepicker-close.ui-state-default.ui-priority-primary.ui-corner-all {float: right;}
          .ui-state-default {    margin: 7px 0 0;    padding: 1px 6px;}
        </style>
    </div>
</section>





<script type="text/javascript">
    jQuery(function ( $ ){
      $(".credit").credit();
    });
  </script>


<script>
/**
 * Credit.js
 * Version: 1.0.0
 * Author: Ron Masas
 */

(function( $ ){

    $.fn.extend({
        credit: function ( args ) {

          $(this).each(function (){


          // Set defaults
      var defaults = {
        auto_select:true
      }

      // Init user arguments
      var args = $.extend(defaults,args);

          // global var for the orginal input
          var credit_org = $(this);

            // Hide input if css was not set
            credit_org.css("display","none");

            // Create credit control holder
          var credit_control = $('<div></div>',{
            class: "credit-input"
          });

          // Add credit cell inputs to the holder
          for ( i = 0; i < 4; i++ ) {
              credit_control.append(
                  $("<input />",{
                    class: "credit-cell",
                    placeholder: "0000",
                    maxlength: 4
                  })
                );
          }

          // Print the full credit input
          credit_org.after( credit_control );

          // Global var for credit cells
          var cells = credit_control.children(".credit-cell");

          /**
       * Set key press event for all credit inputs
       * this function will allow only to numbers to be inserted.
       * @access public
       * @return {bool} check if user input is only numbers
       */
      cells.keypress(function ( event ) {
        // Check if key code is a number
        if ( event.keyCode > 31 && (event.keyCode < 48 || event.keyCode > 57) ) {
              // Key code is a number, the `keydown` event will fire next
              return false;
          }
          // Key code is not a number return false, the `keydown` event will not fire
          return true;
      });

      /**
       * Set key down event for all credit inputs
       * @access public
       * @return {void}
       */
      cells.keydown(function ( event ) {
        // Check if key is backspace
        var backspace = ( event.keyCode == 8 );
        // Switch credit text length
        switch( $(this).val().length ) {
          case 4:
            // If key is backspace do nothing
            if ( backspace ) {
              return;
            }
            // Select next credit element
            var n = $(this).next(".credit-cell");
            // If found
            if (n.length) {
              // Focus on it
              n.focus();
            }
          break;
          case 0:
            // Check if key down is backspace
            if ( !backspace ) {
              // Key is not backspace, do nothing.
              return;
            }
            // Select previous credit element
            var n = $(this).prev(".credit-cell");
            // If found
            if (n.length) {
              // Focus on it
              n.focus();
            }
          break;
        }
      });

      // On cells focus
      cells.focus( function() {
          // Add focus class
          credit_control.addClass('c-focus');
      });

      // On focus out
      cells.blur( function() {
          // Remove focus class
          credit_control.removeClass('c-focus');
      });

      /**
       * Update orginal input value to the credit card number
       * @access public
       * @return {void}
       */
      cells.keyup(function (){
        // Init card number var
        var card_number = '';
        // For each of the credit card cells
        cells.each(function (){
          // Add current cell value
          card_number = card_number + $(this).val();
        });
        // Set orginal input value
        credit_org.val( card_number );
      });


      if ( args["auto_select"] === true ) {
        // Focus on the first credit cell input
        credit_control.children(".credit-cell:first").focus();
      }

      });
              
        }
    });

})(jQuery);

</script>