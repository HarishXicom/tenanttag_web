<script>
    $(document).ready(function () {

        $('.editbinfo').click(function () {
            $('.binfo').attr('disabled', false);
            $('.binfo').attr('readonly', false);
            $(".remail").attr('disabled', true);
            $('.minfo').attr('disabled', true);
            $('.minfo').attr('readonly', true);
            $('.update_minfo').hide();
            $('.update_info').show();
        });
        
        $('.editminfo').click(function () {
            $('.minfo').attr('disabled', false);
            $('.minfo').attr('readonly', false);
            $(".remail").attr('disabled', true);
            $('.binfo').attr('disabled', true);
            $('.binfo').attr('readonly', true);
            $('.update_info').hide();
            $('.update_minfo').show();
        });

        $(".cncl_btn").click(function () {
            $('input,select').attr('disabled', true);
            $('input,select').attr('readonly', true);
            $('.update_info').hide();
            $('.update_minfo').hide();
            $('label.error').hide();
            $("#profile_form").resetForm();
            return false;
        });

        /*    $(".updt_btn").click(function (e) {
         return false;
         });*/




        $("#change_password").validate({
            // Specify the validation rules
            rules: {
                password: {
                    required: true,
                },
                cpassword: {
                    required: true,
                    equalTo: "#password"
                }

            },
            // Specify the validation error messages
            messages: {
                password: {
                    required: "Please provide a password",
                },
                cpassword: {
                    required: "Please provide a password",
                    equalTo: "Please enter the same password as above",
                },
            },
            submitHandler: function (form) {

            }
        });


        jQuery.validator.addMethod("valueNotEquals", function (value, element, arg) {
            return arg != value;
        }, "Value must not equal arg.");
        jQuery.validator.addMethod("customvalidation", function (value, element) {
            return /^[A-Za-z_ -]+$/.test(value);
        }, "Alpha Characters Only.");

        jQuery.validator.addMethod('phoneUS', function (phone_number, element) {
            phone_number = phone_number.replace(/\s+/g, '');
            return this.optional(element) || phone_number.length > 9 &&
                    phone_number.match(/^(1-?)?(\([2-9]\d{2}\)|[2-9]\d{2})-?[2-9]\d{2}-?\d{4}$/);
        }, 'Enter a valid phone.');

        jQuery.validator.addMethod("valueNotEquals", function (value, element, arg) {
            return arg != value;
        }, "Value must not equal arg.");

    });

</script>
