<link href="http://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/css/bootstrap.min.css"
rel="stylesheet" type="text/css" />
<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/js/bootstrap.min.js"></script>
<link href="http://cdn.rawgit.com/davidstutz/bootstrap-multiselect/master/dist/css/bootstrap-multiselect.css"
rel="stylesheet" type="text/css" />
<script src="http://cdn.rawgit.com/davidstutz/bootstrap-multiselect/master/dist/js/bootstrap-multiselect.js"
type="text/javascript"></script>


<script type="text/javascript" src="<?= $this->config->item('templateassets') ?>js/jquery.validate.min.js"></script>   
<script>
    $(document).on("click", ".all-tabs-panel-li", function () {
        $('.all-tabs-panel-li').removeClass('active');
        $(this).addClass('active');
        $('.tab-area').show();
        $(".add_vndr_tab").hide();
    });

    $(".tnt_check_all").click(function () {
        if ($(this).is(':checked')) {
            $(".tnt_check").prop('checked', true);
        } else {
            $(".tnt_check").prop('checked', false);
        }

    });


    $(document).on('click', '.edit_vendor', edit_vendor);

    function edit_vendor()
    {
        $(".hide_rec").hide();
        $(".show_rec").show();
        $(this).parent().parent().parent().hide();
        $(this).parent().parent().parent().prev().show();

    }

    $(document).on('click', '.update_vendor', update_vendor);

    function update_vendor(e)
    {
        var id = $(this).attr('id');
        var values = [];
        var url = "<?php echo base_url('vendors/update_vendor'); ?>";
        $(this).closest('tr').find('input,select').each(function (key, value) {
            values[key] = $(this).val();
            values[4] = id
            //console.log(values);
        });

        var update_cat = jQuery.post(url, {'data': values});
        update_cat.done(function (data) {
            var myArray = JSON.parse(data);
            if (myArray.success) {
                swal({
                    title: "Success",
                    text: 'Vendor Updated Successfully',
                    type: "success"
                },
                function () {
                    location.reload();
                });
            } else {
                swal("Error!", myArray.message, "error");

            }
        });

    }

    $(document).on('click', '.del_vendor', del_vendor);

    function del_vendor(e)
    {
        var id = $(this).attr('id');
        swal({
            title: "Alert?",
            text: "Are you sure to delete this vendor?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
        },
        function () {
            $.ajax({
                url: "<?php echo base_url('vendors/delete_vendor'); ?>/" + id,
                type: 'POST',
                data: {id: id},
                beforeSend: function () {
                    $('#wait-div').show();
                },
                success: function (data) {
                    $("#wait-div").hide();
                    var myArray = JSON.parse(data);
                    if (myArray.success) {
                        swal({
                            title: "Deleted",
                            text: myArray.success_message,
                            type: "success"
                        },
                        function () {
                            location.reload();
                        });
                    } else {
                        swal("Error!", myArray.success_message, "error");
                    }
                },
                error: function () {
                    $("#wait-div").hide();
                    swal("Error!", "Server not responding.", "error");
                }
            });
        });

    }

    $(document).on('click', '.my-vendor', show_add_vendor);

    function show_add_vendor()
    {
        $('.all-tabs-panel-li').removeClass('active');
        $('.tab-area').hide();
        $(".add_vndr_tab").show();

    }

    $(document).on('click', '.addmore_vendor', row_add_vendor);

    function row_add_vendor()
    {
        var html = $(".cp_vndr_form").html();
        $(".vndr_form").append(html);
    }

    $(document).on('click', '.del_vendor_row', del_vendor_row);

    function del_vendor_row()
    {
        $(this).parent().parent().remove();
    }

    $("#add_vendors").validate({
        submitHandler: function (form) {
            var atLeastOneIsChecked = false;
            $('.tnt_check').each(function () {
                if ($(this).is(':checked')) {
                    atLeastOneIsChecked = true;
                }
            });
            if (atLeastOneIsChecked == false) {
                $(".check_error").text('Please check at least one property');
                $(".check_error").show();
                return false;
            }
            $.ajax({
                url: '<?php echo base_url(); ?>vendors/new_vendor',
                type: 'POST',
                data: $("#add_vendors").serializeArray(),
                beforeSend: function () {
                    $(".loader").show();
                },
                success: function (data) {
                    var myArray = JSON.parse(data);
                    if (myArray.success) {
                        swal({
                            title: "Success",
                            text: myArray.success_message,
                            type: "success"
                        },
                        function () {
                            location.reload();
                        });
                    } else {
                        swal("Error!", myArray.success_message, "error");
                    }
                },
                error: function () {
                    $(".loader").hide();
                    swal("error in form submission");
                }
            });
        }
    });

    jQuery.validator.addMethod('phoneUS', function (phone_number, element) {
        phone_number = phone_number.replace(/\s+/g, '');
        return this.optional(element) || phone_number.length > 9 &&
                phone_number.match(/^(1-?)?(\([2-9]\d{2}\)|[2-9]\d{2})-?[2-9]\d{2}-?\d{4}$/);
    }, 'Enter a valid phone.');

    $(document).ready(function () {
        $('.service_dropdown').multiselect({
            includeSelectAllOption: true,
            selectAllText:'Select All',
            nonSelectedText: 'Select Service'
        });
    });
</script>
