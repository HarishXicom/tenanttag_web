<script type="text/javascript" src="<?= $this->config->item('templateassets') ?>js/jquery.validate.min.js"></script>  
<script>
    $(document).ready(function () {

        $(".redrct").click(function () {
            var prop = $(this).attr('id');
          
            window.location.href = '<?php echo base_url() ?>/tenants/#tabs-' + prop;
            var type = window.location.hash.substr(1);
            if (type != '') {
                location.reload();
            }
        });

        $(".mvout_btn").click(function () {
            var id = $(this).attr('id');
            $("#atnt" + id).prop('checked', true);
        });
        $('#myModal2').on('hidden.bs.modal', function () {
            $(".seltnt").prop('checked', false);
        })
        $(".select-all-check").click(function () {
            if ($(this).is(':checked')) {
                $(".seltnt").prop('checked', true);
            } else {
                $(".seltnt").prop('checked', false);
            }

        });

        $(".seltnt").click(function () {
            if ($(this).is(':checked')) {
                $(".check_error").text('');
                $(".check_error").hide();
            }
        });


        $(".mvouttnt").click(function () {
            var atLeastOneIsChecked = false;
            var sms = $.trim($(".my-txtarea-part").val());
            $('.seltnt').each(function () {
                if ($(this).is(':checked')) {
                    atLeastOneIsChecked = true;
                }
            });
            if (atLeastOneIsChecked == false) {
                $(".check_error").text('Please check at least one tenant');
                $(".check_error").show();
                return false;
            }
            else if (sms.length == 0) {
                $(".check_error").text('Please enter your messages');
                $(".check_error").show();
                return false;
            } else {
                $(".check_error").text('');
                $(".check_error").hide();
                $.ajax({
                    url: '<?php echo base_url(); ?>tenant/move_out',
                    type: 'POST',
                    data: $("#move_out").serialize(),
                    beforeSend: function () {
                        $('#myModal2').modal('toggle');
                        $('#wait-div').show();
                    },
                    success: function (data) {
                        $("#wait-div").hide();

                        swal({
                            title: "success",
                            text: "tenant(s) moved out sucsessfully ",
                            type: "success"
                        },
                        function () {
                            location.reload();
                        });

                    },
                    error: function () {
                        $("#wait-div").hide();
                        alert("error in form submission");
                    }
                });
            }
        });



        $(".mvin_btn").click(function () {
            var id = $(this).attr('id');
            $("#atntm" + id).prop('checked', true);
        });
        $('#myModal3').on('hidden.bs.modal', function () {
            $(".seltntm").prop('checked', false);
        })
        $(".select-all-checmk").click(function () {
            if ($(this).is(':checked')) {
                $(".seltntm").prop('checked', true);
            } else {
                $(".seltntm").prop('checked', false);
            }

        });

        $(".seltntm").click(function () {
            if ($(this).is(':checked')) {
                $(".check_errorm").text('');
                $(".check_errorm").hide();
            }
        });


        $(".mvintnt").click(function () {
            var atLeastOneIsChecked = false;
            var sms = $.trim($(".mtxt").val());
            $('.seltntm').each(function () {
                if ($(this).is(':checked')) {
                    atLeastOneIsChecked = true;
                }
            });
            if (atLeastOneIsChecked == false) {
                $(".check_errorm").text('Please check at least one tenant');
                $(".check_errorm").show();
                return false;
            }
            else if (sms.length == 0) {
                $(".check_errorm").text('Please enter your messages');
                $(".check_errorm").show();
                return false;
            } else {
                $(".check_errorm").text('');
                $(".check_errorm").hide();
                $.ajax({
                    url: '<?php echo base_url(); ?>tenant/move_in',
                    type: 'POST',
                    data: $("#move_in").serialize(),
                    beforeSend: function () {
                        $('#myModal3').modal('toggle');
                        $('#wait-div').show();
                    },
                    success: function (data) {
                        $("#wait-div").hide();

                        swal({
                            title: "success",
                            text: "tenant(s) moved in sucsessfully ",
                            type: "success"
                        },
                        function () {
                            location.reload();
                        });

                    },
                    error: function () {
                        $("#wait-div").hide();
                        alert("error in form submission");
                    }
                });
            }
        });

        $('.lease-file').change(function (e) {
            var file_data = $(this).prop('files')[0];
            var prop_id = $(this).attr('id');
            var form_data = new FormData();
            form_data.append('file', file_data);
            form_data.append('prop_id', prop_id);
            $.ajax({
                url: '<?php echo base_url() ?>tenant/upload_doc', // Url to which the request is send
                dataType: 'text', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                type: 'post',
                beforeSend: function () {
                    $('#wait-div').show();
                },
                success: function (response) {
                    $('#wait-div').hide();
                    if (response != 'error') {
                        var html = '<li><a target="_blank" href="' + siteUrl + '/assets/uploads/lease_documents/' + response + '" class="docname">' +
                                '<img src="<?php echo base_url() ?>assets/default/images/file-upload.png" alt=""><span>' + response + '</span></a></li>';
                        $("#" + prop_id).parent().parent().prepend(html);
                    }
                },
                error: function () {
                    $('#wait-div').hide();
                    alert('server error');
                }
            });


        });

        $(document).on("click", ".all-tabs-panel-li", function () {

            $('.all-tabs-panel-li').removeClass('active');
            $(this).addClass('active');
        });

        $(document).on('click', '.crossicon', del_document);
        function del_document(e)
        {
            var id = $(this).attr('id');
            swal({
                title: "Alert?",
                text: "Are you sure to delete this record?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            },
            function () {
                $.ajax({
                    url: '<?php echo base_url('leases/delete_document'); ?>',
                    type: 'POST',
                    data: {id: id},
                    beforeSend: function () {
                        $('#wait-div').show();
                    },
                    success: function (data) {
                        $("#wait-div").hide();
                        swal({
                            title: "Deleted",
                            text: "Deleted Successfully",
                            type: "success"
                        },
                        function () {
                            $(e.target).closest('li').remove();
                        });
                    },
                    error: function () {
                        $("#wait-div").hide();
                        swal("Error!", "Server not responding.", "error");
                    }
                });
            });
        }

        $(".chk_in").click(function () {
            var id = $(this).attr('id');
            var prop = $(this).attr('data-prop');
            $.ajax({
                url: '<?php echo base_url('tenant/check_in'); ?>',
                type: 'POST',
                data: {id: id},
                beforeSend: function () {
                    $('#wait-div').show();
                },
                success: function (data) {
                    $("#wait-div").hide();
                    if (data == 'sucess') {
                        swal({
                            title: "Sucess",
                            text: "Credentials sent to tenant via sms",
                            type: "success"
                        },
                        function () {
                            window.location.href = '<?php echo base_url() ?>/tenants/#tabs-' + prop;
                            location.reload();
                        });
                    } else {
                        swal("Error!", data, "error");
                    }
                },
                error: function () {
                    $("#wait-div").hide();
                    swal("Error!", "Server not responding.", "error");
                }
            });
        });


    });
</script>
<script>
    $(document).on("click", ".add-new-tenant", function () {
        var propId = $(this).attr('data-value');
        $('#add-tenant-' + propId).show('slow');
        $('#add-' + propId).show('slow');
        $(this).hide('slow');

    });
    $(document).on("click", ".save-new-record", function () {
        var propId = $(this).attr('data-value');
        add(propId);

    });
    $(document).on("click", ".cancel-new-record", function () {
        var propId = $(this).attr('data-value');
        $('#add-tenant-' + propId).hide('slow');
        $('.add-new-tenant').show('slow');
        $('#add-' + propId).hide('slow');
        //$('.new-tenant-'+propId).trigger('reset');
        $(this).closest("form").trigger('reset');

    });

    $(document).on("click", ".edit-tenant", function () {
        var tenantId = $(this).attr('data-value');
        $('#show-tenant-' + tenantId).hide('slow');
        $('#edit-tenant-' + tenantId).show('slow');
        $('#save-' + tenantId).show('slow');
        $('#edit-' + tenantId).hide('slow');

    });
    $(document).on("click", ".save-record", function () {
        var tenantId = $(this).attr('data-value');
        update(tenantId);

    });
    $(document).on("click", ".cancel-record", function () {
        var tenantId = $(this).attr('data-value');
        $('#show-tenant-' + tenantId).show('slow');
        $('#edit-tenant-' + tenantId).hide('slow');
        $('#save-' + tenantId).hide('slow');
        $('#edit-' + tenantId).show('slow');
        $('#form-' + tenantId).trigger('reset');

    });

    function update(id)
    {
        var posturl = siteUrl + 'update-tenant/' + id;
        var sessionId = '<?= $this->session->userdata('MEM_ID') ?>';

        if (id != '' && sessionId != '')
        {
            var first_name = $('#first_name_' + id).val();
            var last_name = $('#last_name_' + id).val();
            var email = $('#email_' + id).val();
            var dob = $('#dob_' + id).val();
            var preferred_lang = $('#preferred_lang_' + id).val();
            var mobile_no = $('#mobile_no_' + id).val();
            var lease_start_date = $('#lease_start_date_' + id).val();
            var lease_end_date = $('#lease_end_date_' + id).val();
            var rent_amount = $('#rent_amount_' + id).val();
            var due_date = $('#due_date_' + id).val();
            var late_fee = $('#late_fee_' + id).val();
            var late_fee_type = $('#late_fee_type_' + id).val();
            var property_id = $('#property_id_' + id).val();
            var lease = $('#lease_' + id).val();
            var ccode = $('#ccode_' + id).val();
            //var form_data = $('#'+id).serialize();
            var data_info = $('#tenant_edit_' + id).serialize();
            
            $.ajax({
                url: posturl,
                //data: {first_name: first_name, last_name: last_name, email: email, dob: dob, preferred_lang: preferred_lang, mobile_no: mobile_no, lease_start_date: lease_start_date, lease_end_date: lease_end_date, rent_amount: rent_amount, due_date: due_date, late_fee: late_fee, property_id: property_id, lease: lease, ccode: ccode, late_fee_type: late_fee_type},
                data: data_info,
                dataType: 'json',
                type: "POST",
                beforeSend: function () {
                    //$('#wait-div').show();
                },
                success: function (data) {
                    $('#wait-div').hide();
                    $('.ajax_report').removeClass('alert-success').removeClass('alert-danger').fadeIn(200);
                    if (data.success)
                    {
                        $('.ajax_report').addClass('alert-success').children('.ajax_message').html(data.success_message);

                        var dataToAppend = '<li>';
                        dataToAppend += '<span> <label class="label1">First name</label><label class="label2">' + data.rec.first_name + '</label></span>';
                        dataToAppend += '<span> <label class="label1">Middle name</label><label class="label2">' + data.rec.middle_name + '</label></span>';
                        dataToAppend += '<span><label class="label1">Last Name</label><label class="label2">' + data.rec.last_name + '</label></span>';
                        dataToAppend += '<span><label class="label1">DOB</label><label class="label2">' + data.dob + '</label></span>';
                        dataToAppend += '<span> <label class="label1">Mobile</label><label class="label2">' + data.rec.mobile_no + '</label></span>';
                        dataToAppend += '</li>';
                        dataToAppend += '<li>';
                        dataToAppend += '<span><label class="label1">Language</label><label class="label2">' + data.rec.preferred_language + '</label></span>';
                        dataToAppend += '<span><label class="label1">Email</label><label class="label2">' + data.rec.email + '</label></span>';
                        dataToAppend += '<span><label class="label1">Lease Start  </label><label class="label2">' + data.lease_start_date + '</label></span>';
                        dataToAppend += '<span> <label class="label1">Lease End</label><label class="label2">' + data.lease_end_date + '</label></span>';
                        dataToAppend += '<span><label class="label1">Rent</label><label class="label2">' + data.leaseDeatil.rent_amount + '</label></span>';
                        dataToAppend += '</li>';
                        dataToAppend += '<li>';
                        dataToAppend += '<span><label class="label1">Due Date</label><label class="label2">' + data.due_date + '</label></span>';
                        if (data.leaseDeatil.late_fee_type != '') {
                            dataToAppend += '<span><label class="label1">Late Fee Type</label><label class="label2">' + data.leaseDeatil.late_fee_type + '</label></span>';
                            dataToAppend += '<span><label class="label1">Late Fee</label><label class="label2">$' + data.leaseDeatil.late_fee + '</label></span>';
                        }
                        dataToAppend += '<span><label class="label1">User Name</label><label class="label2">' + data.rec.slug + '</label></span>';
                        dataToAppend += '</li>';
                        var pets_type_arr = data.leaseDeatil.pets_type;
                        var pets_breed_arr = data.leaseDeatil.pets_breed;
                        var pets_typeArr = pets_type_arr.split(",");
                        var pets_breedArr = pets_breed_arr.split(",");
                        for (i = 0; i < pets_typeArr.length; i++) { 
                            dataToAppend += '<li>';
                            dataToAppend += '<span><label class="label1">Pet Type</label><label class="label2">' + pets_typeArr[i] + '</label></span>';
                            dataToAppend += '<span><label class="label1">Pet Breed</label><label class="label2">' + pets_breedArr[i] + '</label></span>';
                            dataToAppend += '</li>';
                        }

                        dataToAppend += '<li>';
                        dataToAppend += '<span><label class="label1">Emergency Name</label><label class="label2">' + data.leaseDeatil.emergency_name + '</label></span>';
                        dataToAppend += '<span><label class="label1">Emergency Phone</label><label class="label2">' + data.leaseDeatil.emergency_phone + '</label></span>';
                        dataToAppend += '<span><label class="label1">Emergency Email </label><label class="label2">' + data.leaseDeatil.emergency_email + '</label></span>';
                        dataToAppend += '</li>';

                        $('#show-tenant-' + id).html('');
                        $('#show-tenant-' + id).append(dataToAppend);


                        $('#save-' + id).hide('slow');
                        $('#edit-' + id).show('slow');

                        $('#show-tenant-' + id).show('slow');
                        $('#edit-tenant-' + id).hide('slow');
                    }
                    else
                    {
                        $('.ajax_report').addClass('alert-danger').children('.ajax_message').html(data.error_message);
                    }
                    if (data.scrollToElement)
                        scrollToElement('.ajax_report', 1000);
                    setTimeout(function () {
                        $('.ajax_report').fadeOut(500);
                    }, 1000);
                },
                error: function (data) {
                    alert("Server Error.");
                    return false;
                }
            });
        }
        else
        {
            $.post(posturl, {id: value, vote: task});
            $("#log-in").trigger("click");
        }
        return false;
    }

    $(document).on('click', '.add-pet-btn', function (event) {
        var assets = '<?= $this->config->item('templateassets') ?>';
        var pets_no = $('.pets_no').val();
        pets_no = parseInt(pets_no) + 1;
        var assets = '<?= $this->config->item('templateassets') ?>';

        var dataToAppend = '<li><div class="my-col width50 "><input type="hidden" name="pets[]" id="pets[]"></div>';
        dataToAppend += '<div class="my-col width50"><select name="pets_type[]" id="pets_type[]" class="form-control my-txt-fleid width50 right-pull mr-rite0 sel_lt_fee_up"><option value="">Select Type</option><option value="dog">Dog</option><option  value="cat">Cat</option><option value="other">Other</option></select></div>';
        dataToAppend += '<div class="my-col width100 "><input type="text" name="pets_breed[]" value="" id="pets_breed[]" class="form-control my-txt-fleid valid"></div>  ';
        dataToAppend += '<div class="my-col width50"><a href="javascript:;" class="delete-pets-row " id="pets_row[]" data-value="" style=""><img src="' + assets + 'images/cross.png" alt=""/></a></div></li>';
        
        $(this).closest('li').after(dataToAppend);
        $('.pets_no').val(pets_no);

    });

    $(document).on('click', '.delete-pets-row', function () {
        $(this).closest('li').remove();
    });

    function add(propId)
    {
        var posturl = siteUrl + 'add-tenant';
        var sessionId = '<?= $this->session->userdata('MEM_ID') ?>';
        var prop_id = propId;
        if (sessionId != '')
        {
            var data = $('#new-tenant-' + propId).serialize();
            $.ajax({
                url: posturl,
                data: data,
                dataType: 'json',
                type: "POST",
                beforeSend: function () {
                    $('#wait-div').show();
                },
                success: function (data) {
                    $('#wait-div').hide();
                    $('.ajax_report').removeClass('alert-success').removeClass('alert-danger').fadeIn(200);
                    if (data.success)
                    {
                        swal({
                            title: "Sucess",
                            text: data.success_message,
                            type: "success"
                        },
                        function () {
                            window.location.href = '<?php echo base_url() ?>/tenants/#tabs-' + prop_id;
                            location.reload();
                        });
                    }
                    else
                    {
                        swal("Error!", data.error_message, "error");
                    }
                },
                error: function (data) {
                    alert("Server Error.");
                    return false;
                }
            });
        }
        else
        {
            $.post(posturl, {id: value, vote: task});
            $("#log-in").trigger("click");
        }
        return false;
    }

    jQuery.validator.addMethod("valueNotEquals", function (value, element, arg) {
        return arg != value;
    }, "Value must not equal arg.");
    jQuery.validator.addMethod("customvalidation", function (value, element) {
        return /^[A-Za-z_ -]+$/.test(value);
    }, "Alpha Characters Only.");

    jQuery.validator.addMethod('phoneUS', function (phone_number, element) {
        phone_number = phone_number.replace(/\s+/g, '');
        return this.optional(element) || phone_number.length > 9 &&
                phone_number.match(/^(1-?)?(\([2-9]\d{2}\)|[2-9]\d{2})-?[2-9]\d{2}-?\d{4}$/);
    }, 'Enter a valid phone.');

    jQuery.validator.addMethod("valueNotEquals", function (value, element, arg) {
        return arg != value;
    }, "Value must not equal arg.");


    $(document).on("click", ".all-tabs-panel-li", function () {

        $('.all-tabs-panel-li').removeClass('active');
        $(this).addClass('active');
    });
    $(document).on("click", ".intservices", function () {
        swal("Error!", 'Please confirm your email address. tServices will not be active until confirmation is complete.', "error");
    });

    $(document).ready(function () {

        $("body").delegate(".dob", "focusin", function () {
            $(this).datepicker({changeMonth: true, changeYear: true, maxDate: 0, defaultDate: "01/01/1985", yearRange: "-80:+0"});
        });

        $("body").delegate(".leasefrom", "focusin", function () {
            $(this).datepicker({
                defaultDate: "+1w",
                changeMonth: true,
              //  minDate: 0,
                changeYear: true,
                numberOfMonths: 1,
                onSelect: function (selectedDate) {
                    $(this).parent().next().find('input').removeAttr('disabled');
                    $(this).parent().next().find('input').focus();
                    $(this).parent().next().find('input').blur();
                },
                onClose: function (selectedDate) {
                    $(this).parent().next().find('input').datepicker("option", "minDate", selectedDate);
                }
            });
        });

        $("body").delegate(".leaseto", "focusin", function () {
            $(this).datepicker({
                defaultDate: "+1w",
                changeMonth: true,
                changeYear: true,
                minDate: 0,
                numberOfMonths: 1,
                onSelect: function (selectedDate) {
                    //   $(this).parent().prev().find('input').focus(); 
                    //   $(this).parent().prev().find('input').blur(); 
                },
                onClose: function (selectedDate) {
                    $(this).parent().prev().find('input').datepicker("option", "maxDate", selectedDate);
                }
            });
        });

        $("body").delegate(".mleasefrom", "focusin", function () {
            $(this).datepicker({
                defaultDate: "+1w",
                changeMonth: true,
                minDate: $(".mleaseto").val(),
                changeYear: true,
                numberOfMonths: 1,
                onSelect: function (selectedDate) {
                    $(this).parent().next().find('input').removeAttr('disabled');
                    $(this).parent().next().find('input').focus();
                    $(this).parent().next().find('input').blur();
                },
                onClose: function (selectedDate) {
                    $(this).parent().next().find('input').datepicker("option", "minDate", selectedDate);
                }
            });
        });

        $("body").delegate(".mleaseto", "focusin", function () {
            $(this).datepicker({
                defaultDate: "+1w",
                changeMonth: true,
                changeYear: true,
                minDate: $(".mleaseto").val(),
                numberOfMonths: 1,
                onSelect: function (selectedDate) {
                    //   $(this).parent().prev().find('input').focus(); 
                    //   $(this).parent().prev().find('input').blur(); 
                },
                onClose: function (selectedDate) {
                    $(this).parent().prev().find('input').datepicker("option", "maxDate", selectedDate);
                }
            });
        });


        $(".rnw_rel").click(function () {
            var data = $(this).attr('id');
            var date = $(this).attr('data-bind');
            var arr = data.split('-');
            $("#tennant_id").val(arr[1]);
            $("#properti_id").val(arr[2]);
            $(".mleaseto").val(date);
        });
        $("#renew_lease").submit(function (event) {
            event.preventDefault();
        });

        $("#renew_lease").validate({
            submitHandler: function (form) {

                var posturl = siteUrl + 'tenant/renew_lease';
                var formData = new FormData(form);
                // formData.append('file', $('#file')[0].files[0]);

                $.ajax({
                    url: posturl,
                    data: formData,
                    type: "POST",
                    processData: false,
                    contentType: false,
                    beforeSend: function () {
                        $('#wait-div').show();
                    },
                    success: function (data) {
                        $('#wait-div').hide();
                        var myArray = JSON.parse(data);
                        if (myArray.success) {
                            BootstrapDialog.show({
                                message: myArray.message,
                                buttons: [{
                                        label: 'OK',
                                        action: function (dialog) {
                                            location.reload();
                                        }
                                    }]
                            })
                        } else {
                            BootstrapDialog.alert(myArray.message);
                        }
                    },
                    error: function (data) {
                        alert("Server Error.");
                        return false;
                    }
                });
                return false;
            }
        });

        $(".late_fee_type_cls").change(function () {
            if ($(this).val() != '') {
                $(this).parent().next().show();
            } else {
                $(this).parent().next().hide();
            }
        });



    });
    $(document).on('click', '.delte_btn', del_tenant);
    function del_tenant(e)
    {
        var id = $(this).attr('id');
        swal({
            title: "Alert?",
            text: "Are you sure to delete this tenant?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
        },
        function () {
            $.ajax({
                url: '<?php echo base_url('tenant/delete_tenant'); ?>',
                type: 'POST',
                data: {id: id},
                beforeSend: function () {
                    $('#wait-div').show();
                },
                success: function (data) {
                    $("#wait-div").hide();
                    swal({
                        title: "Deleted",
                        text: "Deleted Successfully",
                        type: "success"
                    },
                    function () {
                        $(e.target).closest('.tenant').remove();
                    });
                },
                error: function () {
                    $("#wait-div").hide();
                    swal("Error!", "Server not responding.", "error");
                }
            });
        });
    }
</script>
