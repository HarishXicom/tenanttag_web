<link href="<?= $this->config->item('templateassets') ?>css/bootstrap.min.css" rel="stylesheet">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.5/css/bootstrap-dialog.min.css" rel="stylesheet" type="text/css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.5/js/bootstrap-dialog.min.js"></script>
<?php $active_tenants = $inactive_tenants = array(); ?>

<section class="lease-part">
    <div class="container">

        <div class="ajax_report alert display-hide" role="alert" style=" margin-top: 50px; margin-bottom:10px;width:1169px;"><span class="close-message"></span><div class="ajax_message">Hello Message</div></div>
        <div id="tabs">
            <div class="my-tab1"> 
                <ul class="my-tab">
                    <li class="active all-tabs-panel-li"><a href="#summary" class="summary ">Summary</a></li>
                    <?php foreach ($allProperties as $prop) { ?>
                        <li class="all-tabs-panel-li"><a href="#tabs-<?= $prop->prop_id ?>"><?= $prop->city ?>, <?= $prop->address1 ?> <?= $prop->address2 ?></a></li>
                    <?php } ?>
                </ul>
            </div>
            <div class="tab-area">
                <div id="summary">
                    <div class="tenant">
                        <ul class="tenant1">
                            <?php
                            foreach ($allProperties as $val) {
                                $tenatnts = $val->tenants;
                               
                                ?>
                                <li>
                                    <span><?= $val->city ?>, <?= $val->address1 ?> <?= $val->address2 ?></span>
                                    <p class="tnt_label">Tenants</p>
                                    <?php
                                    $defaultWidth = 32;
                                    if (!empty($tenatnts)) {
                                        $width = $defaultWidth / sizeof($tenatnts);
                                        foreach ($tenatnts as $tenant) {
// active tenant array to show on moveout popup
                                            if($tenant->move_status==1){
                                             $active_tenants[] = $tenant;
                                            }
                                             if($tenant->move_status==2){
                                             $inactive_tenants[] = $tenant;
                                            }
                                            ?>
                                    <a class="redrct" id="<?= $val->prop_id ?>" href="javascript:;"><?= $tenant->first_name." ".$tenant->last_name; ?></a>
                                            <?php
                                        }
                                    } else {
                                        ?>
                                    <a href="javascript:;" style="cursor: default;color:#000">No Tenants</a>	
                                    <?php } ?>	

                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
                <?php
                foreach ($allProperties as $val) {
                    $tenatnts = $val->tenants;
                    
                    ?>
                    <div id="tabs-<?= $val->prop_id ?>">
                        <?php if ($tenatnts) { ?>
                            <?php //echo "<pre>"; print_R($tenatnts); ?>
                            <?php foreach ($tenatnts as $tenant) { ?>
                                <div class="tenant">
                                    <ul class="tenant1" id="edit-<?= $tenant->tenant_id ?>">
                                        <li><span class="my-span"><?= $val->property_code ?></span>
                                            <ul class="btn-group">
                                                <?php if ($tenant->move_status == 0) { ?>
                                                <?php $verification_status = $this->common_model->getSingleFieldFromAnyTable('verification_status', 'mem_id', $this->session->userdata('MEM_ID'), 'tbl_members');
                                        if($verification_status=='No'){ $clss = 'intservices';}else{$clss = 'chk_in';}
                                    ?>
                                                    <li><a href="javascript:;" id="<?= $tenant->tenant_id ?>" data-prop='<?= $val->prop_id ?>' class="red-col <?php echo $clss; ?>">Check In</a></li>
                                                    <li><a href="javascript:;" class="sky-blue-col edit-tenant" data-value="<?= $tenant->tenant_id ?>">Edit</a></li>
                                                <?php } elseif ($tenant->move_status == 1) { ?>
                                                    <li><a href="javascript:;" class="sky-blue-col edit-tenant" data-value="<?= $tenant->tenant_id ?>">Edit</a></li>
                                                    <li><a href="javascript:;" id="renew-<?= $tenant->tenant_id . "-" . $val->prop_id ?>" class="green-col rnw_rel" data-bind="<?= date('m/d/Y', strtotime($tenant->lease_end_date)) ?>" data-toggle="modal" data-target="#myModal">Renew</a></li>
                                                    <li><a href="javascript:;" class="red-col mvout_btn" data-toggle="modal" id="<?= $tenant->tenant_id ?>" data-target="#myModal2">Move Out</a></li>
                                                <?php } else { ?>
                                                    <li><a href="javascript:;" class="red-col mvin_btn" data-toggle="modal" id="<?= $tenant->tenant_id ?>" data-target="#myModal3">Move in</a></li>
                                                <?php } ?>
                                                    <li><a href="javascript:;" class="red-col delte_btn" id="<?= $tenant->tenant_id ?>">Delete</a></li>

                                            </ul>
                                        </li>
                                    </ul>

                                    <ul class="tenant1" id="save-<?= $tenant->tenant_id ?>" style="display:none;">
                                        <li>
                                            <ul class="btn-group">
                                                <li><a href="javascript:;" class="red-col cancel-record" data-value="<?= $tenant->tenant_id ?>">Cancel</a></li>
                                                <li><a href="javascript:;" class="sky-blue-col save-record" data-value="<?= $tenant->tenant_id ?>">Save</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                    <ul class="my-list border0" id="show-tenant-<?= $tenant->tenant_id ?>" >
                                        <li>
                                            
                                            <span> <label class="label1">First name</label><label class="label2"><?= $tenant->first_name ?></label></span>
                                            <span><label class="label1">Last Name</label><label class="label2"><?= $tenant->last_name ?></label></span>

                                            <span><label class="label1">DOB</label><label class="label2"><?= date('m/d/Y', $tenant->dob) ?></label></span>
                                            <span> <label class="label1">Mobile</label><label class="label2"><?= $tenant->mobile_no ?></label></span>
                                            <span><label class="label1">Language</label><label class="label2"><?= $tenant->preferred_language ?></label></span>
                                        </li>
                                        <li>
                                            <span><label class="label1">Email</label><label class="label2"><?= $tenant->email ?></label></span>
                                            <span><label class="label1">Lease Start  </label><label class="label2"><?= date('m/d/Y', strtotime($tenant->lease_start_date)) ?></label></span>
                                            <span> <label class="label1">Lease End</label><label class="label2"><?= date('m/d/Y', strtotime($tenant->lease_end_date)) ?></label></span>
                                            <span><label class="label1">Rent</label><label class="label2">$<?= $tenant->rent_amount ?></label></span>
                                            <span><label class="label1">Due Date</label><label class="label2"><?= date('m/'.$tenant->due_date . '/Y') ?></label></span>


                                        </li>
                                        <li>
                                            <span><label class="label1">Late Fee Type</label><label class="label2"><?= $tenant->late_fee_type!=''?$tenant->late_fee_type:'N/A'; ?></label></span>
                                            <?php if($tenant->late_fee_type !=''){ ?>
                                               <span><label class="label1">Late Fee</label><label class="label2">$<?= $tenant->late_fee ?></label></span>
                                            <?php } ?>
                                             <span> <label class="label1">User Name</label><label class="label2"><?= $tenant->slug ?></label></span>
                                        </li>

                                        <?php if(!empty($tenant->pets_type)){ ?>
                                        
                                                <?php $pets_type_array = explode(',',$tenant->pets_type); ?>
                                                <?php $pets_breed_array = explode(',',$tenant->pets_breed); ?>
                                                <?php foreach ($pets_type_array as $key => $value) { ?>  
                                                    <?php if($key == 0){ ?>
                                                        <li class="per_info_cl">                                                  
                                                            <span>
                                                                <label class="label1">Pet Type</label>                                                                
                                                            </span>
                                                             <span>
                                                                <label class="label1">Pet Breed</label>                                                                
                                                            </span>
                                                        </li>
                                                    <?php } ?>
                                                        <li class='<?= count($pets_type_array) == $key+1 ? "" : "per_info_cl"; ?>'>                                                  
                                                            <span>
                                                                <label class="label2">
                                                                    <?= $pets_type_array[$key]!=''? ucfirst($pets_type_array[$key]):'N/A'; ?>
                                                                </label>
                                                            </span>
                                                             <span>
                                                                <label class="label2">
                                                                    <?= $pets_breed_array[$key]!=''? ucfirst($pets_breed_array[$key]):'N/A'; ?>
                                                                </label>
                                                            </span>
                                                        </li>
                                                <?php } ?>
                                        <?php }else{ ?>
                                            <li>
                                                <span><label class="label1">Pet Type</label><label class="label2"><?= $tenant->pets_type!=''?$tenant->pets_type:'N/A'; ?></label></span>
                                                 <span><label class="label1">Pet Breed</label><label class="label2"><?= $tenant->pets_breed!=''?$tenant->pets_breed:'N/A'; ?></label></span>
                                            </li>
                                        <?php } ?>
                                        <li>
                                            <span>
                                                <label class="label1">Emergency Name</label>
                                                <label class="label2">
                                                    <?= $tenant->emergency_name!=''?$tenant->emergency_name:'N/A'; ?>
                                                </label>
                                            </span>
                                             <span>
                                                <label class="label1">Emergency Phone</label>
                                                <label class="label2">
                                                    <?= $tenant->emergency_phone!=''?$tenant->emergency_phone:'N/A'; ?>
                                                </label>
                                            </span>
                                            <span>
                                                <label class="label1">Emergency Email</label>
                                                <label class="label2">
                                                    <?= $tenant->emergency_email!=''?$tenant->emergency_email:'N/A'; ?>
                                                </label>
                                            </span>
                                        </li>
                                    </ul>
                                    <?php echo form_open('', array('class' => 'tenant-detail update_teneant', 'name' => "tenant_edit_".$tenant->tenant_id,'id' => "tenant_edit_".$tenant->tenant_id)) ?>
                                    <!--<form id="form-<?//= $tenant->tenant_id ?>">-->
                                    <ul class="my-list border0" id="edit-tenant-<?= $tenant->tenant_id ?>" style="display:none;">
                                        <li>
                                            <span> <label class="label1">First name</label><input type="text" required="true" customvalidation="true" class="form-control tenant-txtfield" value="<?= $tenant->first_name ?>" name="first_name_<?= $tenant->tenant_id ?>" id="first_name_<?= $tenant->tenant_id ?>" /></span>
                                            <span><label class="label1">Middle Name</label><input type="text" required="true" customvalidation="true" class="form-control tenant-txtfield" value="<?= $tenant->middle_name ?>" name="middle_name_<?= $tenant->tenant_id ?>" id="middle_name_<?= $tenant->tenant_id ?>"/></span>
                                            <span><label class="label1">Last Name</label><input type="text" required="true" customvalidation="true" class="form-control tenant-txtfield" value="<?= $tenant->last_name ?>" name="last_name_<?= $tenant->tenant_id ?>" id="last_name_<?= $tenant->tenant_id ?>"/></span>

                                            <span><label class="label1">DOB</label><input type="text" required="true" class="form-control tenant-txtfield dob" value="<?= date('m/d/Y', $tenant->dob) ?>" name="dob_<?= $tenant->tenant_id ?>" id="dob_<?= $tenant->tenant_id ?>"/></span>
                                            <span> <label class="label1">Mobile</label><input type="text" required="true" phoneUS="true" class="form-control tenant-txtfield phone_us" value="<?= $tenant->mobile_no ?>" name="mobile_no_<?= $tenant->tenant_id ?>" id="mobile_no_<?= $tenant->tenant_id ?>" /></span>

                                        </li>
                                        <li>

                                            <span><label class="label1">Language</label>
                                                <select class="form-control my-txt-fleid" name="preferred_lang_<?= $tenant->tenant_id ?>" id="preferred_lang_<?= $tenant->tenant_id ?>" >
                                                    <option value="">Select Language</option>
                                                    <option value="English" <?php if ($tenant->preferred_language == 'English') { ?>selected<?php } ?>>English</option>
                                                    <option value="Spanish" <?php if ($tenant->preferred_language == 'Spanish') { ?>selected<?php } ?>>Spanish</option>
                                                </select>
                                            </span>
                                            <span><label class="label1">Email</label><input type="text" required="true" email="true" class="form-control tenant-txtfield email_red" value="<?= $tenant->email ?>" name="email_<?= $tenant->tenant_id ?>" id="email_<?= $tenant->tenant_id ?>" readonly/></span>
                                            <span><label class="label1">Lease Start  </label><input required="true" type="text" class="form-control tenant-txtfield leasefrom" value="<?= date('m/d/y', strtotime($tenant->lease_start_date)) ?>" name="lease_start_date_<?= $tenant->tenant_id ?>" id="lease_start_date_<?= $tenant->tenant_id ?>"/></span>
                                            <span> <label class="label1">Lease End</label><input type="text" required="true" class="form-control tenant-txtfield leaseto" value="<?= date('m/d/y', strtotime($tenant->lease_end_date)) ?>" name="lease_end_date_<?= $tenant->tenant_id ?>" id="lease_end_date_<?= $tenant->tenant_id ?>"/></span>
                                            <span><label class="label1">Rent</label><input type="text" required="1" class="form-control tenant-txtfield" value="<?= $tenant->rent_amount ?>" name="rent_amount_<?= $tenant->tenant_id ?>" id="rent_amount_<?= $tenant->tenant_id ?>"/></span>
                                        </li>
                                        <li>
                                            <span><label class="label1">Due Date</label>
                                                <select class="form-control my-txt-fleid " required="true" name="due_date_<?= $tenant->tenant_id ?>" id="due_date_<?= $tenant->tenant_id ?>" class="">
                                                    <?php for ($j = 1; $j < 31; $j++) { ?>
                                                        <option  <?php
                                                        if ($tenant->due_date == $j) {
                                                            echo 'selected="selected"';
                                                        }
                                                        ?> value="<?php echo $j ?>"><?php echo ordinal($j); ?></option>
                                                        <?php } ?>
                                                </select>

                                            </span>
                                            <span><label class="label1">Late Fee Type</label>
                                                <select class="form-control my-txt-fleid late_fee_type_cls" id="late_fee_type_<?= $tenant->tenant_id ?>" required="true" name="late_fee_type_<?= $tenant->tenant_id ?>">
                                                    <option <?php if($tenant->late_fee_type==''){ echo 'selected="selected"';} ?> value="">Late Fee type</option>
                                                    <option <?php if($tenant->late_fee_type=='Daily charge'){ echo 'selected="selected"';} ?> value="Daily charge">Daily charge</option>
                                                    <option <?php if($tenant->late_fee_type=='One time'){ echo 'selected="selected"';} ?> value="One time">One time</option>
                                                </select>
                                            </span>
                                            <span class="<?php if($tenant->late_fee_type==''){ echo 'late_fee_sh';} ?>"><label class="label1">Late Fee</label><input type="text" class="form-control tenant-txtfield" value="<?= $tenant->late_fee ?>" name="late_fee_<?= $tenant->tenant_id ?>" id="late_fee_<?= $tenant->tenant_id ?>"/></span>
                                            
                                        </li>

                                        <?php $pets_type_array = explode(',',$tenant->pets_type); ?>
                                        <?php $pets_breed_array = explode(',',$tenant->pets_breed); ?>
                                        <?php foreach ($pets_type_array as $key => $value) { ?>  
                                                <li>
                                                    <span><label class="label1">Pet type</label>
                                                        <input type="hidden" id="pets1" value="Yes"  name="pets[]" >
                                                        <select class="form-control my-txt-fleid"  required="true"  id="pets_type[]"  name="pets_type[]" >
                                                            <option value="">Select Type</option>
                                                            <option <?php if ($pets_type_array[$key] == 'dog') { ?>selected<?php } ?> value="dog">Dog</option>
                                                            <option <?php if ($pets_type_array[$key] == 'cat') { ?>selected<?php } ?> value="cat">Cat</option>
                                                            <option <?php if ($pets_type_array[$key] == 'other') { ?>selected<?php } ?> value="other">Other</option>
                                                        </select>
                                                    </span>
                                                    <span>
                                                        <label class="label1">Pet breed</label>
                                                        <input type="text" value="<?php echo $pets_breed_array[$key]; ?>" class="form-control my-txt-fleid" id="pets_breed[]"  name="pets_breed[]" >
                                                    </span>
                                                    <span>
                                                    <label class="label1"> &nbsp;&nbsp;&nbsp;&nbsp;</label>
                                                        <input type="hidden" class="pets_no" name="pets_no" value="1">
                                                        <?php if($key == 0){ ?>
                                                            <a href="javascript:;" class="add-pet-btn"><i class="fa fa-plus"></i>Add Pet</a>
                                                        <?php }else{ ?>
                                                            <a href="javascript:;" class="delete-pets-row" id="pets_row[]" data-value="" style="">
                                                            <img src="<?php echo $this->config->item('templateassets'); ?>images/cross.png" alt="">
                                                            </a>
                                                        <?php } ?>
                                                    </span>
                                                </li>
                                        <?php } ?>
                                        <li>
                                            <span><label class="label1">Emergency Name</label>
                                                <input type="text" required="true" customvalidation="true" maxlength="20" class="form-control tenant-txtfield emergency_name" value="<?= $tenant->emergency_name ?>" id="emergency_name_<?= $tenant->tenant_id ?>" name="emergency_name_<?= $tenant->tenant_id ?>"/>
                                            </span>
                                            <span>
                                                <label class="label1">Emergency Phone</label>
                                                <input type="text"  class="form-control my-txt-fleid valid phone_us emergency_phone"  value="<?= $tenant->emergency_phone ?>" id="emergency_phone_<?= $tenant->tenant_id ?>" name="emergency_phone_<?= $tenant->tenant_id ?>"/>
                                            </span>
                                            <span>
                                            <label class="label1">Emergency Email</label>
                                                <input type="email" class="form-control my-txt-fleid valid emergency_email"  value="<?= $tenant->emergency_email ?>" id="emergency_email_<?= $tenant->tenant_id ?>" name="emergency_email_<?= $tenant->tenant_id ?>"/>
                                            </span>
                                        </li>
                                        <input type="hidden" name="property_id_<?= $tenant->tenant_id ?>" id="property_id_<?= $tenant->tenant_id ?>" value="<?= $tenant->property_id ?>">
                                    </ul>
                                    <?php echo form_close(); ?>
                                    <h2 class="lease-doc">Lease Document</h2>
                                    <ul class="upload-file">
                                        <?php $doc = lease_docs($val->prop_id, $tenant->tenant_id, '0'); ?>
                                        <?php if (!empty($doc)) { ?>
                                            <?php foreach ($doc as $file) { ?>
                                                <li>
                                                    <span class="crossicon" id="<?php echo $file->id ?>">X</span>
                                                    <a class="docname" href="<?= $this->config->item('uploads') ?>lease_documents/<?php echo $file->lease_doc; ?>" target="_blank">
                                                        <img alt="" src="<?= $this->config->item('templateassets') ?>images/file-upload.png">
                                                        <span><?php echo $file->lease_doc; ?></a></span>
                                                </li>
                                            <?php } ?>
                                        <?php } ?>
                                        <li class="browse"><input type="file" name="file" class="lease-file" id="<?php echo $val->prop_id . "_" . $tenant->tenant_id ?>" /></li>
                                    </ul>

                                </div>       
                                <?php
                            }
                        } else {
                            ?>
                            <h3 class="tnt"> No tenant found for this property. Click button below to add new tenant.</h3>
                        <?php } ?>	

                        <ul class="tenant1" id="add-<?= $val->prop_id ?>" style="display:none;">
                            <li>
                                <ul class="btn-group">
                                    <li><a href="javascript:;" class="red-col cancel-new-record" data-value="<?= $val->prop_id ?>">Cancel</a></li>
                                    <li><a href="javascript:;" class="sky-blue-col save-new-record" data-value="<?= $val->prop_id ?>">Save</a></li>
                                </ul>
                            </li>
                        </ul>	
                        <?php echo form_open('', array('class' => 'tenant-detai ', 'id' => 'new-tenant-' . $val->prop_id)) ?>

                        <div id="add-tenant-<?= $val->prop_id ?>" style="display:none;">
                            <ul class="my-list border0">
                                <li>
                                    <span> <label class="label1">First name</label><input type="text" class="form-control tenant-txtfield" value="" name="first_name" /></span>
                                    <span><label class="label1">Middle Name</label><input type="text" class="form-control tenant-txtfield" value="" name="middle_name"/><label class="label2"></label></span>
                                    <span><label class="label1">Last Name</label><input required="true" type="text" class="form-control tenant-txtfield" value="" name="last_name"/><label class="label2"></label></span>
                                    <span><label class="label1">DOB</label><input type="text" class="form-control tenant-txtfield dob" value="" name="dob"/></span>
                                    <span> <label class="label1">Mobile</label><input type="text" class="form-control tenant-txtfield phone_us" value="" name="mobile_no"/><label class="label2">  </label></span> 
                                </li>
                                <li>
                                    <span><label class="label1">Language</label>
                                        <select class="form-control my-txt-fleid" name="preferred_language" >
                                            <option value="">Select Language</option>
                                            <option value="English">English</option>
                                            <option value="Spanish">Spanish</option>
                                        </select>
                                    </span>

                                    <span><label class="label1">Email</label><input type="text" class="form-control tenant-txtfield" value="" name="email"/><label class="label2"></label></span>
                                    <!--<span><label class="label1">Password</label><input type="password" class="form-control tenant-txtfield" value="" name="password"/><label class="label2"></label></span>-->


                                    <span><label class="label1">Lease Start  </label><input type="text" class="form-control tenant-txtfield leasefrom"  value="" name="lease_start_date"/></span>
                                    <span> <label class="label1">Lease End</label><input type="text" class="form-control tenant-txtfield leaseto" value="" name="lease_end_date"/></span>
                                    <span><label class="label1">Rent</label><input type="text" class="form-control tenant-txtfield" value="" name="rent_amount" /></span>
                                </li>
                                <li>
                                    <span><label class="label1">Rent Due</label>
                                        <select class="form-control my-txt-fleid " required="true"  name="due_date">
                                            <option value="">Rent Due</option>
                                            <?php for ($j = 1; $j < 31; $j++) { ?>
                                                <option value="<?php echo $j; ?>"><?php echo ordinal($j); ?></option>
                                            <?php } ?>

                                        </select>

                                    </span>
                                    <span><label class="label1">Late Fee Type</label>
                                        <select class="form-control my-txt-fleid late_fee_type_cls" required="true"   name="late_fee_type_1">
                                            <option value="">Late Fee type</option>
                                            <option value="Daily charge">Daily charge</option>
                                            <option value="One time">One time</option>
                                        </select>
                                    </span>

                                    <span class="late_fee_sh"><label class="label1">Late Fee</label><input type="text" class="form-control tenant-txtfield" value="" name="late_fee"/></span>
                                    
                                </li>
                                <li>
                                    <span><label class="label1">Pet type</label>
                                        <input type="hidden" id="pets1" value="Yes"  name="pets[]" >
                                        <select class="form-control my-txt-fleid"  required="true"  id="pets_type[]"  name="pets_type[]" >
                                            <option value="">Select Type</option>
                                            <option value="dog">Dog</option>
                                            <option value="cat">Cat</option>
                                            <option value="other">Other</option>
                                        </select>
                                    </span>
                                    <span>
                                        <label class="label1">Pet breed</label>
                                        <input type="text" class="form-control my-txt-fleid" id="pets_breed[]"  name="pets_breed[]" >
                                    </span>
                                    <span>
                                    <label class="label1"> &nbsp;&nbsp;&nbsp;&nbsp;</label>
                                        <input type="hidden" class="pets_no" name="pets_no" value="1">
                                        <a href="javascript:;" class="add-pet-btn"><i class="fa fa-plus"></i>Add Pet</a>
                                    </span>
                                </li>
                                <li>
                                    <span><label class="label1">Emergency Name</label>
                                        <input type="text" required="true" customvalidation="true" maxlength="20" class="form-control my-txt-fleid valid emergency_name" id="emergency_name" placeholder="Name" name="emergency_name" value=""/>
                                    </span>
                                    <span>
                                        <label class="label1">Emergency Phone</label>
                                        <input type="text"  class="form-control my-txt-fleid valid phone_us emergency_phone" id="emergency_phone" placeholder="Phone" name="emergency_phone" value=""/>
                                    </span>
                                    <span>
                                    <label class="label1">Emergency Email</label>
                                        <input type="email" class="form-control my-txt-fleid valid emergency_email" id="emergency_email" placeholder="Email" name="emergency_email" value=""/>
                                    </span>
                                </li>
                                <input type="hidden" name="property_id" value="<?= $val->prop_id ?>">
                            </ul>
<!--                            <h2 class="lease-doc">Tenant Document</h2>
                            <ul class="upload-file">
                                <li class="browse"><input type="file" name="lease"></li>
                            </ul>-->
                        </div>
                        </form>	
                        <a href="javascript:;" class="btn-tenant my-btn-tennt add-new-tenant" style="float:left;margin: 20px 0px 0px;" data-value="<?= $val->prop_id ?>"><i class="fa fa-plus"></i>Add New Tenant</a>
                    </div>
                <?php } ?>

            </div>

        </div>
        <?php echo form_close(); ?>
    </div>
</section>



<!--  Renew popup start -->
<div class="modal fade" id="myModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Renew Lease</h4>
            </div>
            <div class="modal-body">
                <?php echo form_open_multipart('', array('class' => 'renew-lease', 'id' => 'renew_lease')) ?>
                <input type="text" required="true" name="lease_start_date" class="form-control mleaseto" placeholder="Lease Start">
                <input type="text" required="true" style="margin: 0px 18px;" name="lease_end_date" class="form-control mleasefrom" placeholder="Lease End">
                <input type="text" required="true" number="true" name="rent_amount" class="form-control" digits="true" placeholder="Rent Amount">
                <select required="true" name="due_date" class="form-control my-txt-fleid width50">
                    <option value="">Rent due</option>
                    <?php for ($i = 1; $i < 31; $i++) { ?>
                        <option value="<?php echo $i ?>"><?php echo ordinal($i); ?></option>
                    <?php } ?>
                </select>
                 <select name="late_fee_type" required="true" class="form-control width50 my-txt-fleid">
                    <option value="">Late Fee type</option>
                    <option value="Daily charge">Daily charge</option>
                    <option value="One time">One time</option>
                </select>
                <input type="text" required="true" number="true" name="late_fee" digits="true" placeholder="Late Fee" >
               
                <input type="hidden" name="tennant_id" id="tennant_id" value="">
                <input type="hidden" name="properti_id" id="properti_id" value="">
                <div class="divider"></div>
                <span class="btn btn-default btn-file">
                    <span aria-hidden="true" class="glyphicon glyphicon-download-alt"></span> Upload Documents 
                    <input name="file" id="file" type="file">
                </span>

            </div>
            <div class="modal-footer" style="border:none !important">
                <button type="submit" class="btn btn-primary">Submit</button>

            </div>
            <?php echo form_close(); ?>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- renew popup end -->
<!--  Moveout popup start -->
<div class="modal fade" id="myModal2">
    <div class="modal-dialog move-model">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                <h4 class="modal-title moveout">Tenants(Move Out)
                    <a href="javascript:void(0);" class="select-all">
                        <label style="cursor:pointer" for="selalltnt">Select all</label> <input type="checkbox" class="select-all-check" id="selalltnt" /></a>
                </h4>
            </div>
            <div class="modal-body">
                <?php echo form_open_multipart('', array('class' => 'renew-lease', 'id' => 'move_out')) ?>

                <ul class="move-list">
                    <?php if(!empty($active_tenants)){ ?>
                        <?php foreach ($active_tenants as $active): ?>
                             <li>  
                                 <p>    
                                     <input type="checkbox" name="tenants[]" class="seltnt" id="atnt<?php echo $active->tenant_id ?>" value="<?php echo $active->tenant_id ?>" />    
                                     <label for="atnt<?php echo $active->tenant_id ?>"><?php echo ucfirst($active->first_name)." ".$active->last_name ?></label>  
                                 </p>
                             </li>
                        <?php endforeach; ?>
                    <?php } ?>
                </ul>
                <textarea class="my-txtarea-part" name="message" placeholder="Write Message..."></textarea>
                <p class="check_error"></p>
            </div>
            <div class="modal-footer" style="border:none !important">
                <button type="button" class="btn btn-primary mvouttnt">Submit</button>

            </div>
            <?php echo form_close(); ?>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!--  Movein popup start -->
<div class="modal fade" id="myModal3">
    <div class="modal-dialog move-model">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                <h4 class="modal-title moveout">Tenants(Move In)
                    <a href="javascript:void(0);" class="select-all">
                        <label style="cursor:pointer" for="selalltntm">Select all</label> <input type="checkbox" class="select-all-checkm" id="selalltnt" /></a>
                </h4>
            </div>
            <div class="modal-body">
                <?php echo form_open_multipart('', array('class' => 'renew-lease', 'id' => 'move_in')) ?>

                <ul class="move-list">
                    <?php if(!empty($inactive_tenants)){ ?>
                        <?php foreach ($inactive_tenants as $inactive): ?>
                             <li>  
                                 <p>    
                                     <input type="checkbox" name="tenants[]" class="seltntm" id="atntm<?php echo $inactive->tenant_id ?>" value="<?php echo $inactive->tenant_id ?>" />    
                                     <label for="atntm<?php echo $inactive->tenant_id ?>"><?php echo ucfirst($inactive->first_name)." ".$inactive->last_name ?></label>  
                                 </p>
                             </li>
                        <?php endforeach; ?>
                    <?php } ?>
                </ul>
                <textarea class="my-txtarea-part mtxt" name="message" placeholder="Write Message..."></textarea>
                <p class="check_errorm"></p>
            </div>
            <div class="modal-footer" style="border:none !important">
                <button type="button" class="btn btn-primary mvintnt">Submit</button>

            </div>
            <?php echo form_close(); ?>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
