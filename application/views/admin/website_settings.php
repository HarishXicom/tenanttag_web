<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->

        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <i class="fa fa-home"></i>
                    <a href="<?= site_url('admin') ?>">Home</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li><a href="<?php if (isset($breadcrum1_url)) {
    echo $breadcrum1_url;
} else { ?>javascript:;<?php } ?>"><?= isset($breadcrum1) ? $breadcrum1 : ''; ?></a><?php if (isset($breadcrum2)) { ?><i class="fa fa-angle-right"></i><?php } ?></li>
                <li><a href="<?php if (isset($breadcrum2_url)) {
    echo $breadcrum2_url;
} else { ?>javascript:;<?php } ?>"><?= isset($breadcrum2) ? $breadcrum2 : '';
; ?></a><?php if (isset($breadcrum3)) { ?><i class="fa fa-angle-right"></i><?php } ?></li>
                <li><a href="<?php if (isset($breadcrum3_url)) {
    echo $breadcrum3_url;
} else { ?>javascript:;<?php } ?>"><?= isset($breadcrum3) ? $breadcrum3 : '';
; ?></a><?php if (isset($breadcrum4)) { ?><i class="fa fa-angle-right"></i><?php } ?></li>
                <li><a href="<?php if (isset($breadcrum4_url)) {
    echo $breadcrum4_url;
} else { ?>javascript:;<?php } ?>"><?= isset($breadcrum4) ? $breadcrum4 : '';
; ?></a><?php if (isset($breadcrum5)) { ?><i class="fa fa-angle-right"></i><?php } ?></li>
            </ul>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <div class="col-md-12 ">
                <!-- BEGIN SAMPLE FORM PORTLET-->
                <div class="portlet box blue ">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-gift"></i> <?= isset($subpageName) ? $subpageName : '' ?>
                        </div>
                        <div class="tools">

                        </div>
                    </div>
                    <div class="portlet-body form">
<?php echo form_open_multipart('', array('class' => 'ajaxForm form-horizontal', 'id' => 'website-settings')) ?>
                        <div class="ajax_report alert display-hide" role="alert" style="margin-left:232px;width:956px;"><span class="close"></span><span class="ajax_message">Hello Message</span></div>
                        <div class="form-body">
                            <div class="form-group">
                                <label class="col-md-2 control-label">Site Name <span class="required" aria-required="true"> * </span></label>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                        </span>
                                        <input type="text" class="form-control" name="site_name" placeholder="Site Name" value="<?= $recordDetail->site_name ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">Site Title <span class="required" aria-required="true"> * </span></label>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                        </span>
                                        <input type="text" class="form-control" name="site_title" placeholder="Site Title" value="<?= $recordDetail->site_title ?>">
                                    </div>
                                </div>
                            </div>
<!--                            <div class="form-group">
                                <label class="col-md-2 control-label">Page Size Front <span class="required" aria-required="true"> * </span></label>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                        </span>
                                        <input type="text" class="form-control" name="page_size_front" placeholder="Page Size Front" value="<?//= $recordDetail->page_size_front ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">Page Size Admin <span class="required" aria-required="true"> * </span></label>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                        </span>
                                        <input type="text" class="form-control" name="page_size_admin" placeholder="Page Size Admin" value="<?//= $recordDetail->page_size_admin ?>">
                                    </div>
                                </div>
                            </div>-->
                            <div class="form-group">
                                <label class="col-md-2 control-label">Copyright Text </label>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                        </span>
                                        <input type="text" class="form-control" name="copyright_text" placeholder="copyright text" value="<?= $recordDetail->copyright_text ?>">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label">Contact No. </label>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                        </span>
                                        <input type="text" class="form-control" name="contact_number" placeholder="Contact Number" value="<?= $recordDetail->contact_number ?>">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label">Contact Address </label>
                                <div class="col-md-9">
                                    <textarea class="form-control" rows="3" name="contact_address"><?= $recordDetail->contact_address ?></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label"> </label>
                                <div class="col-md-9">
                                    <img src="<?= $this->config->item('uploads') ?>site_logo/<?= $recordDetail->site_logo ?>" hieght="100" width="100">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">Site Logo </label>
                                <div class="col-md-9">
                                    <input type="file" id="exampleInputFile" name="site_logo">
                                    <p class="help-block">
                                        Select logo for site...
                                    </p>
                                </div>
                            </div>


                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn green">Submit</button>
                                    <button type="reset" class="btn default">Reset</button>
                                </div>
                            </div>
                        </div>
<?= form_close(); ?>
                    </div>
                </div>
                <!-- END SAMPLE FORM PORTLET-->
            </div>
            <!-- END PAGE CONTENT-->

        </div>
    </div>
</div>

