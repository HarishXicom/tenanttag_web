<script>
    $(document).ready(function () {
        $('.date-picker').datepicker({
            // startDate: '-2m',
            endDate: '-18y'
        });
    });
</script>	
<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->

        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <i class="fa fa-home"></i>
                    <a href="<?= site_url('admin') ?>">Home</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li><a href="<?php
                    if (isset($breadcrum1_url)) {
                        echo $breadcrum1_url;
                    } else {
                        ?>javascript:;<?php } ?>"><?= isset($breadcrum1) ? $breadcrum1 : ''; ?></a><?php if (isset($breadcrum2)) { ?><i class="fa fa-angle-right"></i><?php } ?></li>
                <li><a href="<?php
                    if (isset($breadcrum2_url)) {
                        echo $breadcrum2_url;
                    } else {
                        ?>javascript:;<?php } ?>"><?=
                           isset($breadcrum2) ? $breadcrum2 : '';
                           ;
                           ?></a><?php if (isset($breadcrum3)) { ?><i class="fa fa-angle-right"></i><?php } ?></li>
                <li><a href="<?php
                    if (isset($breadcrum3_url)) {
                        echo $breadcrum3_url;
                    } else {
                        ?>javascript:;<?php } ?>"><?=
                        isset($breadcrum3) ? $breadcrum3 : '';
                        ;
                        ?></a><?php if (isset($breadcrum4)) { ?><i class="fa fa-angle-right"></i><?php } ?></li>
                <li><a href="<?php
                    if (isset($breadcrum4_url)) {
                        echo $breadcrum4_url;
                    } else {
                        ?>javascript:;<?php } ?>"><?=
                        isset($breadcrum4) ? $breadcrum4 : '';
                        ;
                    ?></a><?php if (isset($breadcrum5)) { ?><i class="fa fa-angle-right"></i><?php } ?></li>
            </ul>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <div class="col-md-12 ">
                <!-- BEGIN SAMPLE FORM PORTLET-->
                <div class="portlet box blue ">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-gift"></i> <?= $subpageName ?>
                        </div>
                        <div class="tools">

                        </div>
                    </div>
                    <div class="portlet-body form">
<?php echo form_open_multipart('', array('class' => 'ajaxForm form-horizontal', 'id' => 'admin_login')) ?>
                        <div class="ajax_report alert display-hide" role="alert" style="margin-left:232px;width:956px;"><span class="close"></span><span class="ajax_message">Hello Message</span></div>
                        <div class="form-body">
                            <input type="text" name="user_type" value="landlord" hidden>
                            <div class="form-group">
                                <label class="col-md-2 control-label">First Name <span class="required" aria-required="true"> * </span></label>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                        </span>
                                        <input type="text" class="form-control" name="first_name" placeholder="First Name" value="<?= isset($memberDetail->first_name) ? $memberDetail->first_name : ''; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">Last Name <span class="required" aria-required="true"> * </span></label>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                        </span>
                                        <input type="text" class="form-control" name="last_name" placeholder="Last Name" value="<?= isset($memberDetail->last_name) ? $memberDetail->last_name : ''; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">Company Name </label>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                        </span>
                                        <input type="company_name" class="form-control" name="company_name" placeholder="Company Name" value="<?= isset($memberDetail->company_name) ? $memberDetail->company_name : ''; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">Email <span class="required" aria-required="true"> * </span></label>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-envelope"></i>
                                        </span>
                                        <input type="email" class="form-control" name="email" placeholder="Email Address" value="<?= isset($memberDetail->email) ? $memberDetail->email : ''; ?>" <?php if (isset($action) && $action == 'edit') { ?> readonly<?php } ?>>
                                    </div>
                                </div>
                            </div>
<?php if (!isset($action)) { ?>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Password <span class="required" aria-required="true"> * </span></label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                            </span>
                                            <input type="password" class="form-control" name="password" placeholder="Password">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Confirm Password <span class="required" aria-required="true"> * </span></label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                            </span>
                                            <input type="password" class="form-control" name="confirm_password" placeholder="Confirm Password">
                                        </div>
                                    </div>
                                </div>
<?php } ?>
                            <div class="form-group">
                                <label class="col-md-2 control-label">Mobile No. <span class="required" aria-required="true"> * </span></label>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                        </span>
                                        <input type="text" class="form-control phone_us" name="mobile" placeholder="Mobile No." value="<?= isset($memberDetail->mobile_no) ? $memberDetail->mobile_no : ''; ?>">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label">Address <span class="required" aria-required="true"> * </span></label>
                                <div class="col-md-9">
                                    <textarea class="form-control" rows="3" name="mailing_address1"><?= isset($memberDetail->mailing_address1    ) ? $memberDetail->mailing_address1   : ''; ?></textarea>
                                </div>
                            </div>

                            <div id="regions" style="">
                                <div class="form-group">
                                    <label class="col-md-2 control-label">State <span class="required" aria-required="true"> * </span></label>
                                    <div class="col-md-9">

                                        <?php
                                        $state_id = isset($memberDetail->mailing_state_id) && $memberDetail->mailing_state_id != 0 ? $memberDetail->mailing_state_id : '';
                                        $data = array();
                                        $states = get_states('223');
                                        if (!empty($states)) {
                                            foreach ($states as $state) {
                                                $data[$state->region_id] = $state->region_name;
                                            }
                                        }
                                        $list = "id='mailing_state_id'  class='form-control'";
                                        echo form_dropdown('mailing_state_id', $data, $state_id, $list);
                                        ?> 

                                    </div>
                                </div>
                            </div>
                            <div id="cities" style="">
                                <div class="form-group">
                                    <label class="col-md-2 control-label">City <span class="required" aria-required="true"> * </span></label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="mailing_city" placeholder="City" value="<?= isset($memberDetail->mailing_city) ? $memberDetail->mailing_city : ''; ?>">
                                    </div>
                                </div>
                            </div>
                            <div id="cities" style="">
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Unit <span class="required" aria-required="true"> * </span></label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="mailing_unit" placeholder="Unit" value="<?= isset($memberDetail->mailing_unit) ? $memberDetail->mailing_unit : ''; ?>">
                                    </div>
                                </div>
                            </div>
<?php if (isset($action) && $action == 'edit') { ?>
                                <div class="form-group">
                                    <label class="col-md-2 control-label"> </label>
                                    <div class="col-md-9">
                                        <img src="<?= $this->config->item('uploads') ?>member/<?= $memberDetail->profile_image ?>" hieght="100" width="100">

                                    </div>
                                </div>
<?php } ?>
                            <div class="form-group">
                                <label class="col-md-2 control-label">Profile Image <span class="required" aria-required="true"> * </span></label>
                                <div class="col-md-9">
                                    <input type="file" id="exampleInputFile" name="profile_pic">
                                    <p class="help-block">
                                        Select profile Image...
                                    </p>
                                </div>
                            </div>


                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn green">Submit</button>
                                    <button type="reset" class="btn default">Reset</button>
                                </div>
                            </div>
                        </div>
<?= form_close(); ?>
                    </div>
                </div>
                <!-- END SAMPLE FORM PORTLET-->
                <!-- END PAGE CONTENT
                
               

 

                
                
                -->
            </div>

            <script>
                function getStateByCountryId(con_id)
                {
                    if (con_id)
                    {
                        var postUrl = siteUrl + 'getStateByCountryId/' + con_id;
                        $.ajax({
                            url: postUrl,
                            dataType: 'json',
                            beforeSend: function () {
                                $('#wait-div').show();
                            },
                            success: function (response) {
                                if (response.success)
                                {
                                    $('#state').html('');
                                    $('#regions').show('slow');
                                    $('#state').append('<option value="">Select State</option>')
                                    $.each(response.regions, function (key, value) {
                                        $('#state').append('<option value="' + value.region_id + '">' + value.region_name + ' </option>');
                                    });
                                }
                                else
                                {
                                    $('#state').html('');
                                    $('#state').append('<option value="">No state found under selected Country</option>');
                                }
                                 $('#wait-div').hide();
                            },
                            error: function () {
                                alert('server error');
                                $('#wait-div').hide();
                            },
                        });
                    }
                }

                function getCityByStateId(state_id)
                {
                    if (state_id)
                    {
                        var postUrl = siteUrl + 'getCityByStateId/' + state_id;
                        $.ajax({
                            url: postUrl,
                            dataType: 'json',
                             beforeSend: function () {
                                $('#wait-div').show();
                            },
                            success: function (response) {
                                if (response.success)
                                {
                                    $('#city').html('');
                                    $('#cities').show('slow');
                                    $('#city').append('<option value="">Select City</option>')
                                    $.each(response.cities, function (key, value) {
                                        $('#city').append('<option value="' + value.city_id + '">' + value.name + ' </option>');
                                    });
                                }
                                else
                                {
                                    $('#city').html('');
                                    $('#city').append('<option value="">No city found under selected State</option>');
                                }
                                 $('#wait-div').hide();
                            },
                            error: function () {
                                 $('#wait-div').hide();
                                alert('server error');
                            },
                        });
                    }
                }
            </script>
