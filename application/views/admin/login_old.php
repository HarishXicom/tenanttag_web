<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.4
Version: 3.3.0
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title><?php if($title) { echo $title.' | '.$this->config->item('site_title'); } else { ?>TenantTag<?php } ?></title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<meta content="" name="description"/>
<meta content="" name="author"/>
<script type="text/javascript" src="<?=$this->config->item('admintemplateassets')?>js/jquery-1.11.0.js"></script>
<script src="<?=$this->config->item('admintemplateassets')?>js/jquery.form.js"></script>
<script type="text/javascript" src="<?=$this->config->item('admintemplateassets')?>js/formClass.js"></script>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
<link href="<?=$this->config->item('admintemplateassets')?>font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="<?=$this->config->item('admintemplateassets')?>simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
<link href="<?=$this->config->item('admintemplateassets')?>bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="<?=$this->config->item('admintemplateassets')?>uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL STYLES -->
<link href="<?=$this->config->item('admintemplateassets')?>css/login.css" rel="stylesheet" type="text/css"/>
<!-- END PAGE LEVEL SCRIPTS -->
<!-- BEGIN THEME STYLES -->
<link href="<?=$this->config->item('admintemplateassets')?>css/components.css" id="style_components" rel="stylesheet" type="text/css"/>
<link href="<?=$this->config->item('admintemplateassets')?>css/plugins.css" rel="stylesheet" type="text/css"/>
<link href="<?=$this->config->item('admintemplateassets')?>css/layout.css" rel="stylesheet" type="text/css"/>
<link href="<?=$this->config->item('admintemplateassets')?>themes/darkblue.css" rel="stylesheet" type="text/css" id="style_color"/>
<link href="<?=$this->config->item('admintemplateassets')?>css/custom.css" rel="stylesheet" type="text/css"/>
<!-- END THEME STYLES -->
<link rel="shortcut icon" href="favicon.ico"/>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="login">
<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
<div class="menu-toggler sidebar-toggler">
</div>
<!-- END SIDEBAR TOGGLER BUTTON -->
<!-- BEGIN LOGO -->
<div class="logo">
	<a href="javascript:;">
	<img src="<?=$this->config->item('admintemplateassets')?>images/logo-big.png" alt="logo" class="logo-default"/>	
	</a>
</div>
<!-- END LOGO -->
<!-- BEGIN LOGIN -->
<div class="content">
	<!-- BEGIN LOGIN FORM -->
	<div class="">
	<?php echo form_open('admin/login',array('class'=>'ajaxForm','id'=>'admin_login'))?>
		<h3 class="form-title">Sign In</h3>
		<div class="ajax_report alert display-hide" role="alert"><span class="close"></span><span class="ajax_message">Hello Message</span></div>
		
		<div class="form-group">
			<!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
			<label class="control-label visible-ie8 visible-ie9">Username</label>
			<input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="Username" name="username"/>
		</div>
		<div class="form-group">
			<label class="control-label visible-ie8 visible-ie9">Password</label>
			<input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="password"/>
		</div>
		<div class="form-actions">
			<button type="submit" class="btn btn-success uppercase">Login</button>
			<label class="rememberme check">
			<input type="checkbox" name="remember_me" value="Yes"/>Remember </label>
			<a href="javascript:;" id="forget-password" class="forget-password">Forgot Password?</a>
		</div>
	<?php echo form_close();?>
	</div>
	<!-- END LOGIN FORM -->
	<!-- BEGIN FORGOT PASSWORD FORM -->
	<div class="forget-form">
	<?php echo form_open('admin/forget_password',array('class'=>'ajaxForm','id'=>'admin_forget_password'))?>
		<h3>Forgot Password ?</h3>
		<div class="ajax_report alert display-hide" role="alert"><span class="close"></span><span class="ajax_message">Hello Message</span></div>
		<p>
			 Enter your e-mail address below to reset your password.
		</p>
		<div class="form-group">
			<input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="email"/>
		</div>
		<div class="form-actions">
			<button type="button" id="back-btn" class="btn btn-default">Back</button>
			<button type="submit" class="btn btn-success uppercase pull-right">Submit</button>
		</div>
	<?php echo form_close();?>
	</div>
	<!-- END FORGOT PASSWORD FORM -->
	
</div>
<div class="copyright">
	 2014 © Metronic. Admin Dashboard Template.
</div>
<!-- END LOGIN -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="assets/global/plugins/respond.min.js"></script>
<script src="assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
<script src="<?=$this->config->item('admintemplateassets')?>js/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="<?=$this->config->item('admintemplateassets')?>bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?=$this->config->item('admintemplateassets')?>js/jquery.blockui.min.js" type="text/javascript"></script>
<script src="<?=$this->config->item('admintemplateassets')?>js/jquery.cokie.min.js" type="text/javascript"></script>
<script src="<?=$this->config->item('admintemplateassets')?>uniform/jquery.uniform.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?=$this->config->item('admintemplateassets')?>js/metronic.js" type="text/javascript"></script>
<script src="<?=$this->config->item('admintemplateassets')?>js/layout.js" type="text/javascript"></script>
<script src="<?=$this->config->item('admintemplateassets')?>js/demo.js" type="text/javascript"></script>

<!-- END PAGE LEVEL SCRIPTS -->
<script>
jQuery(document).ready(function() {     
Metronic.init(); // init metronic core components
Layout.init(); // init current layout

Demo.init();
});

$(document).on("click",".forget-password",function(){
	$('.forget-form').show();
	$('.login-form').hide();
});
$(document).on("click","#back-btn",function(){
	$('.forget-form').hide();
	$('.login-form').show();
});
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
