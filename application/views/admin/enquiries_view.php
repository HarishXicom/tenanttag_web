<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h4 class="modal-title"> Detail</h4>
</div>
		<div class="modal-body">
			<div class="row">
				<div class="col-md-12">
					<table class="table table-striped table-bordered table-hover" id="sample_1">
								<thead>
									<tr>
									<th>Field Name</th>
									<th>Data</th>
									</tr>
								</thead>
							<tbody>
								
									<tr class="odd gradeX">
									<td>ID </td>
									<td><?=$records->id?></td>
									</tr>
									<tr class="odd gradeX">
									<td>Name</td>
									<td><?=$records->name?></td>
									</tr>
									<tr class="odd gradeX">
									<td>Email</td>
									<td><?=$records->email?></td>
									</tr>
									<tr class="odd gradeX">
									<td>Message</td>
									<td><?=$records->message?></td>
									</tr>
									<tr class="odd gradeX">
									<td>Reply Status</td>
									<td><?=$records->reply_status?></td>
									</tr>
									<tr>
									<td>Add Date</td>
									<td><?=date('m/d/y',$records->add_date)?></td>
									</tr>
									
							</tbody>
					</table>
				</div>
			</div>
		</div>

<div class="modal-footer">
	<button type="button" class="btn default" data-dismiss="modal">Close</button>
</div>
