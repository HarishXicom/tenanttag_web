<script>
 pageckeditor=true;
 
</script>
<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			
			<div class="page-bar">
				       <ul class="page-breadcrumb">
                                <li>
                                        <i class="fa fa-home"></i>
                                        <a href="<?=site_url('admin')?>">Home</a>
                                        <i class="fa fa-angle-right"></i>
                                </li>
                                <li><a href="<?php if(isset($breadcrum1_url)) { echo $breadcrum1_url;} else { ?>javascript:;<?php } ?>"><?=isset($breadcrum1)?$breadcrum1:'';?></a><?php if(isset($breadcrum2)) { ?><i class="fa fa-angle-right"></i><?php } ?></li>
                                <li><a href="<?php if(isset($breadcrum2_url)) { echo $breadcrum2_url;} else { ?>javascript:;<?php } ?>"><?=isset($breadcrum2)?$breadcrum2:'';;?></a><?php if(isset($breadcrum3)) { ?><i class="fa fa-angle-right"></i><?php } ?></li>
                                <li><a href="<?php if(isset($breadcrum3_url)) { echo $breadcrum3_url;} else { ?>javascript:;<?php } ?>"><?=isset($breadcrum3)?$breadcrum3:'';;?></a><?php if(isset($breadcrum4)) { ?><i class="fa fa-angle-right"></i><?php } ?></li>
                                <li><a href="<?php if(isset($breadcrum4_url)) { echo $breadcrum4_url;} else { ?>javascript:;<?php } ?>"><?=isset($breadcrum4)?$breadcrum4:'';;?></a><?php if(isset($breadcrum5)) { ?><i class="fa fa-angle-right"></i><?php } ?></li>
                            </ul>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12 ">
					<!-- BEGIN SAMPLE FORM PORTLET-->
					<div class="portlet box blue ">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-gift"></i> <?=$subpageName?>
							</div>
							<div class="tools">
								<a href="javascript:location.reload();" class="collapse">
								</a>
								<a href="" class="reload">
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<?php echo form_open_multipart('',array('class'=>'ajaxForm form-horizontal','id'=>'add-amenties'))?>
								<div class="ajax_report alert display-hide" role="alert" style="margin-left:232px;width:956px;"><span class="close"></span><span class="ajax_message">Hello Message</span></div>
								<div class="form-body">
									
									<div class="form-group">
										<label class="col-md-2 control-label">Query </label>
										<div class="col-md-9">
											<div class="input-group">
												<?=$enquiryDetail->message?>
											</div>
										</div>
									</div>
                                                                    <div class="form-group">
										<label class="col-md-2 control-label">To </label>
										<div class="col-md-9">
											<div class="input-group">
												<?php echo $enquiryDetail->name." &lt".$enquiryDetail->email."&gt"?>
											</div>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-2 control-label">Reply <span class="required" aria-required="true"> * </span></label>
										<div class="col-md-9">
											<div class="input-group">
                                                <textarea id="ckeditor<?=  isset($key)?$key:''?>" name="message" class="ckeditor">
                                                	<!-- <table border="0" style="width:100%">
														<tbody>
															<tr>
																<td>
																<table align="center" border="0" cellpadding="0" cellspacing="0" style="background:#fff; box-shadow:0 0 10px #ccc; width:600px">
																	<tbody>
																		<tr>
																			<td>&nbsp;</td>
																		</tr>
																		<tr>
																			<td>
																			<table cellpadding="0" cellspacing="0" style="width:100%">
																				<tbody>
																					<tr>
																						<td>&nbsp;</td>
																						<td>
																						<table cellpadding="0" cellspacing="0" style="width:100%">
																							<tbody>
																								<tr>
																									<td>&nbsp;</td>
																								</tr>
																								<tr>
																									<td>
																									<table cellpadding="0" cellspacing="0" style="width:100%">
																										<tbody>
																											<tr>
																												<td>
																												<table border="0" cellpadding="0" cellspacing="0" style="width:100%">
																													<tbody>
																														<tr>
																															<td><strong>Dear <?php echo $enquiryDetail->name?>,</strong></td>
																														</tr>
																														<tr>
																															<td style="background-color:#1f7cae; height:40px; text-align:center">Title</td>
																														</tr>
																														<tr>
																															<td>&nbsp;</td>
																														</tr>
																														<tr>
																															<td>
																															<p><strong>Message :</strong></p>

																															<p>&nbsp;</p>

																															<p>&nbsp;</p>
																															</td>
																														</tr>
																														<tr>
																															<td>Regards,<br />
																															TenantTag Team</td>
																														</tr>
																													</tbody>
																												</table>
																												</td>
																											</tr>
																										</tbody>
																									</table>
																									</td>
																								</tr>
																								<tr>
																									<td>
																									<p>*This email account is not monitored. Please do not reply to this email as we will not be able to read and respond to your messages.</p>
																									</td>
																								</tr>
																							</tbody>
																						</table>
																						</td>
																						<td>&nbsp;</td>
																					</tr>
																				</tbody>
																			</table>
																			</td>
																		</tr>
																		<tr>
																			<td>
																			<p>You have received this message by auto generated e-mail.</p>

																			<p>2016 &copy; Tenant.</p>
																			</td>
																		</tr>
																	</tbody>
																</table>
																</td>
															</tr>
														</tbody>
													</table> -->
												</textarea>
											</div>
										</div>
									</div>
									<input type="hidden" name="name" value="<?=$enquiryDetail->name?>">
									<input type="hidden" name="email" value="<?=$enquiryDetail->email?>">
									<input type="hidden" name="user_enquiry" value="<?=$enquiryDetail->message?>">
								</div>
								<div class="form-actions">
									<div class="row">
										<div class="col-md-offset-3 col-md-9">
											<button type="submit" class="btn green">Send</button>
											<button type="button" class="btn default">Cancel</button>
										</div>
									</div>
								</div>
							<?=form_close();?>
						</div>
					</div>
					<!-- END SAMPLE FORM PORTLET-->
				</div>	
			<!-- END PAGE CONTENT-->
		</div>
</div>
</div>	
