<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
     <h4 class="modal-title">Message Logs</h4>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
            <div class="Scheduled-cont">
                <ul class="Scheduled-cont-data">
                    <?php if (!empty($sent_msgs)) { ?>
                        <?php foreach ($sent_msgs as $sms) { ?>
                            <li>
                                <p><?php echo $sms->message ?></p>
                                 <i class="fa fa-clock-o"></i><?php echo date('m/d/y',  strtotime($sms->date_created)); ?>
                               <i class="fa fa-user"></i><?php echo $sms->first_name." ".$sms->last_name; ?>
                               <i class="fa fa-home"></i><?php echo $sms->address1." ".$sms->address2.", ".$sms->city; ?>
                            </li>
                        <?php } ?>
                    <?php }else{ ?>
                            <li>No Messages found</li>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="modal-footer">
    <button type="button" class="btn default" data-dismiss="modal">Close</button>
</div>
