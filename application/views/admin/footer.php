<!-- BEGIN FOOTER -->
<div class="page-footer">
    <div class="page-footer-inner">
        <?= $this->config->item('copyright_text') ?>
    </div>
    <div class="scroll-to-top">
        <i class="icon-arrow-up"></i>
    </div>
</div>

<!-- Large Modal start here -->
<div class="modal fade" id="large" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
        </div>
    </div>
</div>
<!-- Large Modal end here -->

<!-- Regular Modal start here -->
<div class="modal fade" id="general" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        </div>
    </div>
</div>
<!-- Regular Modal end here -->

<!-- END FOOTER -->

<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="<?= $this->config->item('admintemplateassets') ?>plugins/respond.min.js"></script>
<script src="<?= $this->config->item('admintemplateassets') ?>plugins/excanvas.min.js"></script> 
<![endif]-->
<!--
<script src="<?= $this->config->item('admintemplateassets') ?>js/jquery.min.js" type="text/javascript"></script>
-->
<script src="<?= $this->config->item('admintemplateassets') ?>js/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="<?= $this->config->item('admintemplateassets') ?>jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="<?= $this->config->item('admintemplateassets') ?>bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?= $this->config->item('admintemplateassets') ?>bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="<?= $this->config->item('admintemplateassets') ?>jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="<?= $this->config->item('admintemplateassets') ?>js/jquery.blockui.min.js" type="text/javascript"></script>
<script src="<?= $this->config->item('admintemplateassets') ?>js/jquery.cokie.min.js" type="text/javascript"></script>
<script src="<?= $this->config->item('admintemplateassets') ?>uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="<?= $this->config->item('admintemplateassets') ?>bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?= $this->config->item('admintemplateassets') ?>jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?= $this->config->item('admintemplateassets') ?>select2/select2.min.js"></script>
<script type="text/javascript" src="<?= $this->config->item('admintemplateassets') ?>datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?= $this->config->item('admintemplateassets') ?>datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?= $this->config->item('admintemplateassets') ?>js/metronic.js" type="text/javascript"></script>
<script src="<?= $this->config->item('admintemplateassets') ?>js/layout.js" type="text/javascript"></script>
<script src="<?= $this->config->item('admintemplateassets') ?>js/quick-sidebar.js" type="text/javascript"></script>
<script src="<?= $this->config->item('admintemplateassets') ?>js/demo.js" type="text/javascript"></script>
<script src="<?= $this->config->item('admintemplateassets') ?>js/index.js" type="text/javascript"></script>
<script src="<?= $this->config->item('admintemplateassets') ?>js/table-managed.js"></script>
<script src="<?= $this->config->item('admintemplateassets') ?>js/tasks.js" type="text/javascript"></script>
<script type="text/javascript" src="<?= $this->config->item('admintemplateassets') ?>bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<!--
<script type="text/javascript" src="<?= $this->config->item('admintemplateassets') ?>bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
-->
<script type="text/javascript" src="<?= $this->config->item('admintemplateassets') ?>bootstrap-fileinput/bootstrap-fileinput.js"></script>
<!--
<script type="text/javascript" src="<?= $this->config->item('admintemplateassets') ?>ckeditor/ckeditor.js"></script>
-->
<script type="text/javascript" src="<?= $this->config->item('admintemplateassets') ?>bootstrap-markdown/js/bootstrap-markdown.js"></script>
<script type="text/javascript" src="<?= $this->config->item('admintemplateassets') ?>bootstrap-markdown/lib/markdown.js"></script>
<script src="<?= $this->config->item('admintemplateassets') ?>js/form-validation.js"></script>
<script type="text/javascript" src="<?= $this->config->item('admintemplateassets'); ?>picklist/jquery.ui.widget.js"></script>
<script type="text/javascript" src="<?= $this->config->item('admintemplateassets'); ?>picklist/jquery-picklist.js"></script>


<!-- END PAGE LEVEL SCRIPTS -->
<script>
    jQuery(document).ready(function () {
        Metronic.init(); // init metronic core componets
        Layout.init(); // init layout
        QuickSidebar.init(); // init quick sidebar
        Demo.init(); // init demo features
        //~ Index.init();   
        //~ Index.initDashboardDaterange();
        //~ Tasks.initDashboardWidget();
        //TableManaged.init();
        FormValidation.init();
    });

 $('.phone_us').mask('(000) 000-0000');
    $('.check_all').click(function (event)
    {
        if (this.checked)
        {
            $(':checkbox').each(function ()
            {
                //$(this).parent().parent().addClass('checked');
                $(this).parent().addClass("checked");
                this.checked = true;
            });
        }
        else
        {
            $(':checkbox').each(function ()
            {
                //$(this).parent().parent().removeClass('checked');
                $(this).parent().removeClass("checked");
                this.checked = false;
            });
        }
    });

    $('.ids').click(function (event)
    {  // alert('sdfdsf');
        if (this.checked)
        {
            $(this).parent().addClass("checked");
            this.checked = true;
        }
        else
        {
            $('.check_all').parent().removeClass("checked");
            $(this).parent().removeClass("checked");
            this.checked = false;
            $('.check_all').checked = false;
        }
    });
    
  

    $('.multiple-action li a').click(function (event) {
        var value = $(this).attr('data-value');
    });

    $('.close').click(function (event) {
        $('div .ajax_report').hide('slow');
//$('div .ajax_report').removeClass('display-hide');
    });


    $("#multiple_active").click(function () {
        $("#multiple-operations").append('<input type="hidden" name="perform_task" value="Active">');
        bootbox.confirm("Are you sure,You want to active all selected records?", function (result) {
            if (result)
            {
                $("#multiple-operations").submit();
            }
        });
    });
    $("#multiple_inactive").click(function () {
        $("#multiple-operations").append('<input type="hidden" name="perform_task" value="Inactive">');
        bootbox.confirm("Are you sure,You want to Inactive all selected records?", function (result) {
            if (result)
            {
                $("#multiple-operations").submit();
            }
        });
    });
    $("#multiple_delete").click(function () {
        $("#multiple-operations").append('<input type="hidden" name="perform_task" value="delete">');
        bootbox.confirm("Are you sure,You want to delete all selected records?", function (result) {
            if (result)
            {
                $("#multiple-operations").submit();
            }
        });
    });

</script>

<!-- For advance search javascript starts here-->
<script>
    $(document).ready(function () {
        $("#advanceSearch").click(function () {
            $("#adv_box").toggle('slow');
            $("#adv_boxclose").toggle('slow');
            $(".filterInput").toggle('slow');
        });
        $("#advanceSearch_close").click(function () {
            $("#adv_box").toggle('slow');
            $("#adv_boxclose").toggle('slow');
            $(".filterInput").toggle('slow');
        });

        $('#search_key_ad').blur(function (key) {
            var value = $(this).val();
            changevalue(this, value);
        });
    });

    $('#sample_1').DataTable({
        paging: false,
        "columnDefs": [{
                "targets": 'no_record',
                "orderable": false,
            }],
        bFilter: false, bInfo: false
    });


</script>
<!-- For advance search javascript End here-->



<!-- Load and execute javascript code used On Index page -->
<?php
if (sizeof($this->config->item('adminjs')) > 0) {
    foreach ($this->config->item('adminjs') as $item) {
        echo '<script type="text/javascript" src="' . $this->config->item('admintemplateassets') . $item . '"></script>' . "\n";
    }
}
?>
<!-- Load and execute javascript code used On datatable  page -->

<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
