<script>
 pageckeditor=true;
 
  function goBack()
	{
		 var redirect_url = '<?=$_SERVER['HTTP_REFERER']?>';
		 if(redirect_url)
		 window.location=redirect_url;
		 else
		window.location='<?=site_url($this->config->config['adminName'].'/email-templates')?>';
	}

</script>
<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			
			   <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                    <i class="fa fa-home"></i>
                    <a href="<?=site_url('admin')?>">Home</a>
                    <i class="fa fa-angle-right"></i>
            </li>
            <li><a href="<?php if(isset($breadcrum1_url)) { echo $breadcrum1_url;} else { ?>javascript:;<?php } ?>"><?=isset($breadcrum1)?$breadcrum1:'';?></a><?php if(isset($breadcrum2)) { ?><i class="fa fa-angle-right"></i><?php } ?></li>
            <li><a href="<?php if(isset($breadcrum2_url)) { echo $breadcrum2_url;} else { ?>javascript:;<?php } ?>"><?=isset($breadcrum2)?$breadcrum2:'';;?></a><?php if(isset($breadcrum3)) { ?><i class="fa fa-angle-right"></i><?php } ?></li>
            <li><a href="<?php if(isset($breadcrum3_url)) { echo $breadcrum3_url;} else { ?>javascript:;<?php } ?>"><?=isset($breadcrum3)?$breadcrum3:'';;?></a><?php if(isset($breadcrum4)) { ?><i class="fa fa-angle-right"></i><?php } ?></li>
            <li><a href="<?php if(isset($breadcrum4_url)) { echo $breadcrum4_url;} else { ?>javascript:;<?php } ?>"><?=isset($breadcrum4)?$breadcrum4:'';;?></a><?php if(isset($breadcrum5)) { ?><i class="fa fa-angle-right"></i><?php } ?></li>
        </ul>
    </div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12 ">
					<!-- BEGIN SAMPLE FORM PORTLET-->
					<div class="portlet box blue ">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-gift"></i> <?=$subpageName?>
							</div>
							<div class="tools">
								
							</div>
						</div>
						<div class="portlet-body form">
							<?php echo form_open_multipart('',array('class'=>'ajaxForm form-horizontal','id'=>'add-amenties'))?>
								<div class="ajax_report alert display-hide" role="alert" style="margin-left:232px;width:956px;"><span class="close"></span><span class="ajax_message">Hello Message</span></div>
								<div class="form-body">
									<div class="form-group">
										<label class="col-md-2 control-label">Title <span class="required" aria-required="true"> * </span></label>
										<div class="col-md-9">
											<div class="input-group">
												<span class="input-group-addon">
												</span>
                                                                                            <input type="text" class="form-control" name="title" placeholder="Email title" value="<?=  isset($recordDetail->title)?$recordDetail->title:'';?>">
											</div>
										</div>
									</div>
									
									<div class="form-group">
										<label class="col-md-2 control-label">Subject <span class="required" aria-required="true"> * </span></label>
										<div class="col-md-9">
											<div class="input-group">
												<span class="input-group-addon">
												</span>
                                                                                            <input type="text" class="form-control" name="subject" placeholder="Email Subject" value="<?=  isset($recordDetail->title)?$recordDetail->title:''?>">
											</div>
										</div>
									</div>
									
									<div class="form-group">
										<label class="col-md-2 control-label">Description <span class="required" aria-required="true"> * </span></label>
										<div class="col-md-9">
											<div class="input-group">
                                                                                            <textarea id="ckeditor<?=isset($key)?$key:''?>" name="description" class="ckeditor"><?=  isset($recordDetail->description)?$recordDetail->description:''?></textarea>
											</div>
										</div>
									</div>
									
								
									
								</div>
								<div class="form-actions">
									<div class="row">
										<div class="col-md-offset-3 col-md-9">
											<button type="submit" class="btn green">Submit</button>
											<button type="reset" class="btn default">Reset</button>
										</div>
									</div>
								</div>
							<?=form_close();?>
						</div>
					</div>
					<!-- END SAMPLE FORM PORTLET-->
			<!-- END PAGE CONTENT-->
		</div>
	
