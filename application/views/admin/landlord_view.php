<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title"><?= $adminSubPage ?> Detail</h4>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped table-bordered table-hover" id="sample_1">
                <thead>
                    <tr>
                        <th>Field Name</th>
                        <th>Data</th>
                    </tr>
                </thead>
                <tbody>

                    <tr class="odd gradeX">
                        <td>Member ID </td>
                        <td><?= $records->mem_id ?></td>
                    </tr>
                    <tr class="odd gradeX">
                        <td>Name</td>
                        <td><?= $records->first_name ?> <?= $records->last_name ?></td>
                    </tr>
                    <tr class="odd gradeX">
                        <td>email</td>
                        <td><?= $records->email ?></td>
                    </tr>
                    <tr class="odd gradeX">
                        <td>Mobile</td>
                        <td><?= $records->mobile_no ?></td>
                    </tr>
                    <tr class="odd gradeX">
                        <td>Address</td>
                        <td><?= $records->address ?></td>
                    </tr>
                    <tr class="odd gradeX">
                        <td>Country</td>
                        <td>
                            <?= $this->common_model->getSingleFieldFromAnyTable('country_name', 'id', $records->country_id, 'tbl_country'); ?> </a>
                        </td>
                    </tr>
                    <tr class="odd gradeX">
                        <td>State </td>
                        <td>
                            <?= $this->common_model->getSingleFieldFromAnyTable('region_name', 'region_id', $records->state_id, 'tbl_region'); ?> </a>
                        </td>
                    </tr>

                <td>Add Date</td>
                <td><?= date('m/d/y', $records->add_date) ?></td>
                </tr>
                <tr class="odd gradeX">
                    <td>Status</td>
                    <td><?= $records->status ?></td>
                </tr>

                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="modal-footer">
    <button type="button" class="btn default" data-dismiss="modal">Close</button>
</div>
