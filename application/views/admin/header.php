<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.4
Version: 3.9.0
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title><?php if(isset($title) && !empty($title)) { echo $title.' | '.$this->config->item('site_title'); } else { ?>TenantTag<?php } ?></title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="" name="description"/>
<meta content="" name="author"/>
<script type="text/javascript" src="<?=$this->config->item('admintemplateassets')?>js/jquery-1.11.0.js"></script>
<script src="<?=$this->config->item('admintemplateassets')?>js/jquery.form.js"></script>
<script type="text/javascript" src="<?=$this->config->item('admintemplateassets')?>js/formClass.js"></script>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="<?=$this->config->item('admintemplateassets')?>css/fonts-googleapi.css" rel="stylesheet" type="text/css"/>
<link href="<?=$this->config->item('admintemplateassets')?>font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="<?=$this->config->item('admintemplateassets')?>simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
<link href="<?=$this->config->item('admintemplateassets')?>bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="<?=$this->config->item('admintemplateassets')?>uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
<link href="<?=$this->config->item('admintemplateassets')?>bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css"/>
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE STYLES -->
<link href="<?=$this->config->item('admintemplateassets')?>css/tasks.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" type="text/css" href="<?=$this->config->item('admintemplateassets')?>select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="<?=$this->config->item('admintemplateassets')?>datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<link rel="stylesheet" type="text/css" href="<?=$this->config->item('admintemplateassets')?>bootstrap-wysihtml5/bootstrap-wysihtml5.css"/>
<link rel="stylesheet" type="text/css" href="<?=$this->config->item('admintemplateassets')?>bootstrap-markdown/css/bootstrap-markdown.min.css">
<link rel="stylesheet" type="text/css" href="<?=$this->config->item('admintemplateassets')?>bootstrap-datepicker/css/bootstrap-datepicker3.min.css"/>
<link href="<?=$this->config->item('admintemplateassets')?>bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" type="text/css" href="<?=$this->config->item('admintemplateassets')?>css/profile-old.css"/>
<link rel="stylesheet" type="text/css" href="<?=$this->config->item('admintemplateassets')?>datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<!-- END PAGE STYLES -->
<!-- BEGIN THEME STYLES -->
<link href="<?=$this->config->item('admintemplateassets')?>css/components.css" id="style_components" rel="stylesheet" type="text/css"/>
<link href="<?=$this->config->item('admintemplateassets')?>css/plugins.css" rel="stylesheet" type="text/css"/>
<link href="<?=$this->config->item('admintemplateassets')?>css/layout.css" rel="stylesheet" type="text/css"/>
<link href="<?=$this->config->item('admintemplateassets')?>themes/darkblue.css" rel="stylesheet" type="text/css" id="style_color"/>
<link href="<?=$this->config->item('admintemplateassets')?>css/custom.css" rel="stylesheet" type="text/css"/>
<!-- END THEME STYLES -->
<script src="<?=$this->config->item('admintemplateassets')?>bootbox/bootbox.min.js"></script>
   <script type="text/javascript" src="<?= $this->config->item('templateassets') ?>js/jquery.mask.js"></script>
<link href="http://www.jqueryscript.net/css/top.css" rel="stylesheet" type="text/css">
<link type="text/css" href="<?=$this->config->item('admintemplateassets');?>picklist/jquery-picklist.css" rel="stylesheet" />
<link rel="shortcut icon" href="<?php echo base_url() ?>/favicon.ico" type="image/x-icon">
        <link rel="icon" href="<?php echo base_url() ?>/favicon.ico" type="image/x-icon">
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<!-- DOC: Apply "page-header-fixed-mobile" and "page-footer-fixed-mobile" class to body element to force fixed header or footer in mobile devices -->
<!-- DOC: Apply "page-sidebar-closed" class to the body and "page-sidebar-menu-closed" class to the sidebar menu element to hide the sidebar by default -->
<!-- DOC: Apply "page-sidebar-hide" class to the body to make the sidebar completely hidden on toggle -->
<!-- DOC: Apply "page-sidebar-closed-hide-logo" class to the body element to make the logo hidden on sidebar toggle -->
<!-- DOC: Apply "page-sidebar-hide" class to body element to completely hide the sidebar on sidebar toggle -->
<!-- DOC: Apply "page-sidebar-fixed" class to have fixed sidebar -->
<!-- DOC: Apply "page-footer-fixed" class to the body element to have fixed footer -->
<!-- DOC: Apply "page-sidebar-reversed" class to put the sidebar on the right side -->
<!-- DOC: Apply "page-full-width" class to the body element to have full width page without the sidebar menu -->
<body class="page-header-fixed page-quick-sidebar-over-content login">
<!-- BEGIN HEADER -->
<div class="page-header navbar navbar-fixed-top">
	<!-- BEGIN HEADER INNER -->
	<div class="page-header-inner">
		<!-- BEGIN LOGO -->
		<div class="page-logo">
			<a href="<?=site_url('admin')?>">
				<?php $site_logo=$this->common_model->getSingleFieldFromAnyTable('site_logo','id',1,'tbl_wesite_config');?>
				<img src="<?=$this->config->item('uploads')?>site_logo/<?=$site_logo?>" alt="logo" class="logo-default" height="30" width="150"/>
			</a>
			<div class="menu-toggler sidebar-toggler hide">
				<!-- DOC: Remove the above "hide" to enable the sidebar toggler button on header -->
			</div>
		</div>
		<!-- END LOGO -->
		<!-- BEGIN RESPONSIVE MENU TOGGLER -->
		<a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
		</a>
		<!-- END RESPONSIVE MENU TOGGLER -->
		<!-- BEGIN TOP NAVIGATION MENU -->
		<div class="top-menu">
			<ul class="nav navbar-nav pull-right">
				<!-- BEGIN NOTIFICATION DROPDOWN -->
				<!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
				
				<!-- END NOTIFICATION DROPDOWN -->
				
				<!-- BEGIN USER LOGIN DROPDOWN -->
				<!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
				<li class="dropdown dropdown-user">
					<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
					<?php $img = $this->common_model->getSingleFieldFromAnyTable('profile_pic','adm_id',$this->session->userdata('ADM_ID'),'tbl_admin'); ?>	
					<img alt="" class="img-circle" src="<?=site_url().'assets/uploads/admin_image/'.$img?>"/>
					<span class="username username-hide-on-mobile">
					<?=isset($adminDetail->username)?$adminDetail->username:''?> </span>
					<i class="fa fa-angle-down"></i>
					</a>
					<ul class="dropdown-menu dropdown-menu-default">
						<li>
							<a href="<?=site_url('admin/my-profile')?>">
							<i class="icon-user"></i> My Profile </a>
						</li>
						
					</ul>
				</li>
				<!-- END USER LOGIN DROPDOWN -->
				<!-- BEGIN QUICK SIDEBAR TOGGLER -->
				<!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
				<li class="dropdown dropdown-quick-sidebar-toggler">
					<a href="<?=site_url('admin/logout')?>" class="dropdown-toggle">
					<i class="icon-logout"></i>
					</a>
				</li>
				<!-- END QUICK SIDEBAR TOGGLER -->
			</ul>
		</div>
		<!-- END TOP NAVIGATION MENU -->
		
	</div>
	<!-- END HEADER INNER -->
</div>
<!-- END HEADER -->
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
<?php  $this->load->view('admin/left_part');?>
<script>
var siteUrl	=	'<?=site_url()?>';
var adminsiteUrl	=	'<?=site_url('admin')?>';
var pageckeditor=false;
</script>

<!--live notifications for admin -->
<script type="text/javascript">
$(document).ready(function() {
  /*  setInterval(function() {
        $.ajax({
        url: adminsiteUrl + "/get-admin-notifications",
        type: 'POST',
        success: function(html){
			$("li#header_notification_bar").html('');
			$("li#header_notification_bar").append(html);
			}
		});
    }, 10000);*/
});
</script> 
 <div id="wait-div"></div>
