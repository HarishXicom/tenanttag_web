<script>
    $(document).ready(function () {
         var date = new Date();
        date.setDate(date.getDate());
        $('.date-picker').datepicker({
            endDate: '-18y',
            
        });
        $('.due_date').datepicker({
            autoclose: true
        });

        $(".lease_start_date").datepicker({
            autoclose: true,
             //startDate: date
        }).on('changeDate', function (selected) {
            var startDate = new Date(selected.date.valueOf());
            $('.lease_end_date').datepicker('setStartDate', startDate);
            $('.due_date').datepicker('setStartDate', startDate);
        }).on('clearDate', function (selected) {
            $('.lease_end_date').datepicker('setStartDate', null);
        });

        $(".lease_end_date").datepicker({
            autoclose: true,
             startDate: date
        }).on('changeDate', function (selected) {
            var endDate = new Date(selected.date.valueOf());
            $('.lease_start_date').datepicker('setEndDate', endDate);
            $('.due_date').datepicker('setEndDate', endDate);
        }).on('clearDate', function (selected) {
            $('.lease_start_date').datepicker('setEndDate', null);
        });

    });
</script>
<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->

       <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                    <i class="fa fa-home"></i>
                    <a href="<?=site_url('admin')?>">Home</a>
                    <i class="fa fa-angle-right"></i>
            </li>
            <li><a href="<?php if(isset($breadcrum1_url)) { echo $breadcrum1_url;} else { ?>javascript:;<?php } ?>"><?=isset($breadcrum1)?$breadcrum1:'';?></a><?php if(isset($breadcrum2)) { ?><i class="fa fa-angle-right"></i><?php } ?></li>
            <li><a href="<?php if(isset($breadcrum2_url)) { echo $breadcrum2_url;} else { ?>javascript:;<?php } ?>"><?=isset($breadcrum2)?$breadcrum2:'';;?></a><?php if(isset($breadcrum3)) { ?><i class="fa fa-angle-right"></i><?php } ?></li>
            <li><a href="<?php if(isset($breadcrum3_url)) { echo $breadcrum3_url;} else { ?>javascript:;<?php } ?>"><?=isset($breadcrum3)?$breadcrum3:'';;?></a><?php if(isset($breadcrum4)) { ?><i class="fa fa-angle-right"></i><?php } ?></li>
            <li><a href="<?php if(isset($breadcrum4_url)) { echo $breadcrum4_url;} else { ?>javascript:;<?php } ?>"><?=isset($breadcrum4)?$breadcrum4:'';;?></a><?php if(isset($breadcrum5)) { ?><i class="fa fa-angle-right"></i><?php } ?></li>
        </ul>
    </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <div class="col-md-12 ">
                <!-- BEGIN SAMPLE FORM PORTLET-->
                <div class="portlet box blue ">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-gift"></i> <?= $subpageName ?>
                        </div>
                        <div class="tools">

                        </div>
                    </div>
                    <div class="portlet-body form">
                    <?php echo form_open_multipart('', array('class' => 'ajaxForm form-horizontal', 'id' => 'admin_login')) ?>
                        <div class="ajax_report alert display-hide" role="alert" style="margin-left:232px;width:956px;"><span class="close"></span><span class="ajax_message">Hello Message</span></div>
                        <div class="form-body">
                            <input type="text" name="user_type" value="tenant" hidden>
                            <div class="form-group">
                                <label class="col-md-2 control-label">First Name <span class="required" aria-required="true"> * </span></label>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                        </span>
                                        <input type="text" class="form-control" name="first_name" placeholder="First Name" value="<?= isset($memberDetail->first_name)?$memberDetail->first_name:''; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">Last Name <span class="required" aria-required="true"> * </span></label>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                        </span>
                                        <input type="text" class="form-control" name="last_name" placeholder="Last Name" value="<?= isset($memberDetail->last_name)?$memberDetail->last_name:'';?>">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">Email <span class="required" aria-required="true"> * </span></label>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-envelope"></i>
                                        </span>
                                        <input type="email" class="form-control" name="email" placeholder="Email Address" value="<?=  isset($memberDetail->email)?$memberDetail->email:''; ?>" <?php if (isset($action) && $action == 'edit') { ?> readonly<?php } ?>>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label">Mobile No. <span class="required" aria-required="true"> * </span></label>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                        </span>
                                        <input type="text" class="form-control phone_us" name="mobile" placeholder="Mobile No." value="<?= isset($memberDetail->mobile_no)?$memberDetail->mobile_no:''; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">DOB <span class="required" aria-required="true"> * </span></label>
                                <div class="col-md-9">
                                    <div class="input-group date date-picker" data-date-format="dd-mm-yyyy">
                                        <input type="text" class="form-control" readonly name="dob" value="<?php if (isset($memberDetail->dob)) { ?><?= date('d-m-Y', $memberDetail->dob) ?><?php } ?>">
                                        <span class="input-group-btn">
                                            <button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
                                        </span>
                                    </div>
                                    <!-- /input-group -->

                                </div>
                            </div>
                              <div class="form-group">
                                    <label class="col-md-2 control-label">Select Language <span class="required" aria-required="true"> * </span></label>
                                    <div class="col-md-9">
                                        <select class="form-control" name="preferred_language" >
                                              <option value="">Select Language</option>
                                            <option value="English">English</option>
                                            <option value="Spanish">Spanish</option>
                                        </select>
                                    </div>
                                </div>

                            <h3 class="form-section">Property details(tenant is going to occupie)</h3>

                            <div class="form-group">
                                <label class="col-md-2 control-label">Property Owner <span class="required" aria-required="true"> * </span></label>
                                <div class="col-md-9">
                                    <select class="form-control" name="landlord" id="landlord" onChange="getPropertiesByLandlordId(this.value)">
                                        <option value="">Select Property owner</option>
<?php foreach ($allLandlords as $value) { ?>
                                        <option value="<?= $value->mem_id ?>" <?php if (isset($ownedPropertyDetail->owner_id) && $ownedPropertyDetail->owner_id == $value->mem_id) { ?> selected<?php } ?>><?= ucfirst($value->first_name) ?> <?= ucfirst($value->last_name) ?></option>
<?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div id="properties" style="<?php if (isset($action) && $action != 'edit') { ?> display:none<?php } ?>">
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Properties <span class="required" aria-required="true"> * </span></label>
                                    <div class="col-md-9">
                                        <select class="form-control" name="property" id="property" >
                                            <option value="">Select Property</option>
                                        <?php if(isset($allProperties) && !empty($allProperties)){ foreach ($allProperties as $value) { ?>
                                                <option value="<?= $value->prop_id ?>" <?php if ($ownedPropertyDetail->property_id == $value->prop_id) { ?> selected<?php } ?>><?= $value->property_code ?></option>
                                        <?php }} ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div id="lease_deatails" style="<?php if (isset($action) && $action != 'edit') { ?> display:none<?php } ?>">
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Lease start date <span class="required" aria-required="true"> * </span></label>
                                    <div class="col-md-2">
                                        <div class="input-group date lease_start_date" data-date-format="dd-mm-yyyy">
                                            <input type="text" class="form-control" readonly name="lease_start_date" value="<?php if (isset($ownedPropertyDetail->lease_start_date)) { ?><?= date('d-m-Y', strtotime($ownedPropertyDetail->lease_start_date)) ?><?php } ?>">
                                            <span class="input-group-btn">
                                                <button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
                                            </span>
                                        </div>
                                    </div>
                                    <label class="col-md-2 control-label">Lease End date <span class="required" aria-required="true"> * </span></label>
                                    <div class="col-md-2">
                                        <div class="input-group date lease_end_date" data-date-format="dd-mm-yyyy">
                                            <input type="text" class="form-control" readonly name="lease_end_date" value="<?php if (isset($ownedPropertyDetail->lease_end_date)) { ?><?= date('d-m-Y', strtotime($ownedPropertyDetail->lease_end_date)) ?><?php } ?>">
                                            <span class="input-group-btn">
                                                <button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
                                            </span>
                                        </div>
                                    </div>
                                    <label class="col-md-1 control-label">Due date <span class="required" aria-required="true"> * </span></label>
                                    <div class="col-md-2">
                                        <div class="input-group date">
                                         <select class="form-control "  name="due_date">
                                            <option value="">Rent Due</option>
                                            <?php for ($j = 1; $j < 31; $j++) { ?>
                                                        <option  <?php
                                                        if (isset($ownedPropertyDetail->due_date) && $ownedPropertyDetail->due_date == $j) {
                                                            echo 'selected="selected"';
                                                        }
                                                        ?> value="<?php echo $j ?>"><?php echo ordinal($j); ?></option>
                                                        <?php } ?>

                                        </select>
                                        </div>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Rent Amount <span class="required" aria-required="true"> * </span></label>
                                    <div class="col-md-2">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                            </span>
                                            <input type="text" class="form-control" name="rent_amount" placeholder="Rent Amount" value="<?= isset($ownedPropertyDetail->rent_amount)?$ownedPropertyDetail->rent_amount:''; ?>">
                                        </div>
                                    </div>
                                     <label class="col-md-2 control-label">Late Fee Type <span class="required" aria-required="true"> * </span></label>
                                    <div class="col-md-2">
                                        <div class="input-group">
                                            
                                              <select class="form-control my-txt-fleid late_fee_type_cls" name="late_fee_type">
                                                  <option <?php if(isset($ownedPropertyDetail->late_fee_type) && $ownedPropertyDetail->late_fee_type==''){ echo 'selected="selected"';} ?> value="">Late Fee type</option>
                                                    <option <?php if(isset($ownedPropertyDetail->late_fee_type) && $ownedPropertyDetail->late_fee_type=='Daily charge'){ echo 'selected="selected"';} ?> value="Daily charge">Daily charge</option>
                                                    <option <?php if(isset($ownedPropertyDetail->late_fee_type) && $ownedPropertyDetail->late_fee_type=='One time'){ echo 'selected="selected"';} ?> value="One time">One time</option>
                                                </select>
                                       
                                        </div>
                                    </div>
                                    <label class="col-md-1 control-label">Late Fee <span class="required" aria-required="true"> * </span></label>
                                    <div class="col-md-2">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                            </span>
                                            <input type="text" class="form-control" name="late_fee" placeholder="Late Fee" value="<?= isset($ownedPropertyDetail->late_fee)?$ownedPropertyDetail->late_fee:''; ?>">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">

                                </div>

                            </div>


                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn green">Submit</button>
                                    <button type="reset" class="btn default">Reset</button>
                                </div>
                            </div>
                        </div>
<?= form_close(); ?>
                    </div>
                </div>
                <!-- END SAMPLE FORM PORTLET-->
                <!-- END PAGE CONTENT-->
            </div>

            <script>
                function getStateByCountryId(con_id)
                {
                    if (con_id)
                    {
                        var postUrl = siteUrl + 'getStateByCountryId/' + con_id;
                        $.ajax({
                            url: postUrl,
                            dataType: 'json',
                             beforeSend: function () {
                                $('#wait-div').show();
                            },
                            success: function (response) {
                                if (response.success)
                                {
                                    $('#state').html('');
                                    $('#regions').show('slow');
                                    $('#state').append('<option value="">Select State</option>')
                                    $.each(response.regions, function (key, value) {
                                        $('#state').append('<option value="' + value.region_id + '">' + value.region_name + ' </option>');
                                    });
                                }
                                else
                                {
                                    $('#state').html('');
                                    $('#state').append('<option value="">No state found under selected Country</option>');
                                }
                                $('#wait-div').hide();
                            },
                            error: function () {
                                $('#wait-div').hide();
                                alert('server error');
                            },
                        });
                    }
                }

                function getCityByStateId(state_id)
                {
                    if (state_id)
                    {
                        var postUrl = siteUrl + 'getCityByStateId/' + state_id;
                        $.ajax({
                            url: postUrl,
                            dataType: 'json',
                              beforeSend: function () {
                                $('#wait-div').show();
                            },
                            success: function (response) {
                                if (response.success)
                                {
                                    $('#city').html('');
                                    $('#cities').show('slow');
                                    $('#city').append('<option value="">Select City</option>')
                                    $.each(response.cities, function (key, value) {
                                        $('#city').append('<option value="' + value.city_id + '">' + value.name + ' </option>');
                                    });
                                }
                                else
                                {
                                    $('#city').html('');
                                    $('#city').append('<option value="">No city found under selected State</option>');
                                }
                                $('#wait-div').hide();
                            },
                            error: function () {
                                 $('#wait-div').hide();
                                alert('server error');
                            },
                        });
                    }
                }

                function getPropertiesByLandlordId(owner_id)
                {
                    if (owner_id)
                    {
                        var postUrl = adminsiteUrl + '/getPropertiesByLandlordId/' + owner_id;
                        $.ajax({
                            url: postUrl,
                            dataType: 'json',
                              beforeSend: function () {
                                $('#wait-div').show();
                            },
                            success: function (response) {
                                if (response.success)
                                {
                                    $('#lease_deatails').show('slow');
                                    $('#property').html('');
                                    $('#properties').show('slow');
                                    $('#property').append('<option value="">Select Property</option>')
                                    $.each(response.properties, function (key, value) {
                                        $('#property').append('<option value="' + value.prop_id + '">' + value.address1 + ' ' + value.address2 + ' ' + value.city +'</option>');
                                    });
                                }
                                else
                                {
                                    $('#lease_deatails').hide('slow');
                                    $('#properties').show('slow');
                                    $('#property').html('');
                                    $('#property').append('<option value="">No Property found under selected Landlord</option>');
                                }
                                  $('#wait-div').hide();
                            },
                            error: function () {
                                  $('#wait-div').hide();
                                alert('server error');
                            },
                        });
                    }
                }
            </script>
