<style>
    .modal-dialog.modal-lg {
        width: 80%;
    }
</style>
<div class="page-content-wrapper">
    <div class="page-content" style="min-height:1161px">
        <!-- BEGIN PAGE HEADER-->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <i class="fa fa-home"></i>
                    <a href="https://tenanttag.com/admin">Home</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li><a href="javascript:;">Member Manager</a><i class="fa fa-angle-right"></i></li>
                <li><a href="https://tenanttag.com/admin/landlords">All Landlords</a><i class="fa fa-angle-right"></i></li>
                <li><a href="https://tenanttag.com/admin/landlords/add"></a></li>
                <li><a href="javascript:;"></a></li>
            </ul>
        </div>
        <div class="row">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <!-- BEGIN PAGE CONTENT-->
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-user"></i>
                            <?= $records->first_name ?> <?= $records->last_name ?>, <?= $records->email ?>, <?= $records->mobile_no ?>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-3 col-sm-3 col-xs-3">
                                <ul class="nav nav-tabs tabs-left">
                                    <li class="active"><a data-toggle="tab" aria-expanded="true" href="#summary" class="summary ">Summary</a></li>
                                    <?php foreach ($allProperties as $prop) { ?>
                                        <li class=""><a data-toggle="tab" aria-expanded="false" href="#tabs-<?= $prop->prop_id ?>"><?= $prop->city ?>, <?= $prop->address1 ?> <?= $prop->address2 ?></a></li>
                                    <?php } ?>
                                </ul>
                            </div>
                            <div class="col-md-9 col-sm-9 col-xs-9">
                                <div class="tab-content">
                                    <div id="summary" class="tab-pane active in">
                                        <div class="tenant">
                                            <?php if(!empty($allProperties)){ ?>
                                            <ul class="tenant1">
                                                <?php
                                                foreach ($allProperties as $val) {
                                                    $tenatnts = $val->tenants;
                                                    ?>
                                                    <li>
                                                        <span><?= $val->city ?>, <?= $val->address1 ?> <?= $val->address2 ?></span>
                                                        <p class="tnt_label">Tenants</p>
                                                        <?php
                                                        $defaultWidth = 32;
                                                        if (!empty($tenatnts)) {
                                                            $width = $defaultWidth / sizeof($tenatnts);
                                                            foreach ($tenatnts as $tenant) {
                                                                ?>
                                                                <a  href="javascript:;"><?= ucfirst($tenant->first_name)." ".ucfirst($tenant->last_name); ?></a>
                                                                <?php
                                                            }
                                                        } else {
                                                            ?>
                                                            <a href="javascript:;">No Tenants</a>	
                                                            <?php 
                                                        } ?>	
                                                        <?php
                                                        if ( checkTrialTimePeriod($val->owner_id) == false) {
                                                            $subscription_info = $this->common_model->getPropertySubscriptionDetail($val->owner_id,$val->prop_id);
                                                            $get_customer_saved_card = $this->common_model->get_customer_saved_card($val->owner_id,$val->prop_id);
                                                            if(isset($subscription_info->last_subscription_date)){ ?>
                                                                <div class="row">
                                                                    <span style="background-image: none;width: 40%;" class="">
                                                                        <b>Subscription renewal date:</b> <?php  echo $subscription_info->next_subscription_date; ?>
                                                                    </span>
                                                                    <span style="background-image: none;float:right;width: 40%;" class="">  
                                                                        <b>CC: </b> XXXX - XXXX - XXXX - <?php echo $get_customer_saved_card->last_digits; ?>
                                                                    </span>
                                                                </div>
                                                                <?php 
                                                            }else{ ?>
                                                                <div class="row">
                                                                    <span style="background-image: none;" class=""><b>Subscription</b>: Not available</span>
                                                                </div>
                                                                <?php
                                                            }
                                                        } else{ ?>
                                                            <div class="row">
                                                                <span  style="background-image: none;" class="" style="float:rig1ht"><b>Subscription</b> renewal date: <?php  echo checkTrialTimePeriod($val->owner_id); ?></span>
                                                            </div>
                                                            <?php 
                                                        } ?>
                                                    </li>
                                                <?php } ?>
                                            </ul>
                                            <?php }else{ ?>
                                            <ul class="tenant1"><li>  <span>No Properties</span></li></ul>
                                            <?php } ?>
                                        </div>
                                    </div>

                                    <?php
                                    foreach ($allProperties as $val) {
                                        $tenatnts = $val->tenants;
                                        //echo '<pre>';print_r($allProperties);die;
                                        ?>
                                        <div id="tabs-<?= $val->prop_id ?>" class="tab-pane fade">
                                            <div class="tenant">
                                                <ul class="tenant1">
                                                    <li>
                                                        <div class="row">
                                                            <?php
                                                            if ( checkTrialTimePeriod($val->owner_id) == false) {
                                                                $subscription_info = $this->common_model->getPropertySubscriptionDetail($val->owner_id,$val->prop_id);
                                                                $get_customer_saved_card = $this->common_model->get_customer_saved_card($val->owner_id,$val->prop_id);
                                                                if(isset($subscription_info->last_subscription_date)){ ?>
                                                                    <div class="row">
                                                                        <span style="background-image: none;width: 40%;" class="">
                                                                            <b>Subscription renewal date:</b> <?php  echo $subscription_info->next_subscription_date; ?>
                                                                        </span>
                                                                        <span style="background-image: none;float:right;width: 40%;" class="">  
                                                                            <b>CC: </b> XXXX - XXXX - XXXX - <?php echo $get_customer_saved_card->last_digits; ?>
                                                                        </span>
                                                                    </div>
                                                                    <?php 
                                                                }else{ ?>
                                                                    <div class="row">
                                                                        <span style="background-image: none;" class=""><b>Subscription</b>: Not available</span>
                                                                    </div>
                                                                    <?php
                                                                }
                                                            } else{ ?>
                                                                <div class="row">
                                                                    <span  style="background-image: none;" class="" style="float:rig1ht"><b>Subscription</b> renewal date: <?php  echo checkTrialTimePeriod($val->owner_id); ?></span>
                                                                </div>
                                                                <?php 
                                                            } ?>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                            <?php if ($tenatnts) { ?>
                                                <?php foreach ($tenatnts as $tenant) { ?>
                                                    <div class="tenant">
                                                        <ul class="tenant1" id="edit-<?= $tenant->tenant_id ?>">
                                                            <li><span class="my-span"><?= $val->property_code ?></span>

                                                            </li>
                                                        </ul>
                                                        <ul class="my-list border0" id="show-tenant-<?= $tenant->tenant_id ?>" >
                                                            <li>
                                                                <span><label class="label1">Last Name</label><label class="label2"><?= $tenant->last_name ?></label></span>
                                                                <span> <label class="label1">First name</label><label class="label2"><?= $tenant->first_name ?></label></span>
                                                                <span><label class="label1">DOB</label><label class="label2"><?= date('m/d/y', $tenant->dob) ?></label></span>
                                                            </li>
                                                            <li>
                                                                <span><label class="label1">Language</label><label class="label2"><?= $tenant->preferred_language ?></label></span>


                                                                <span><label class="label1">Email</label><label class="label2"><?= $tenant->email ?></label></span>
                                                                <span> <label class="label1">Mobile</label><label class="label2"><?php if (isset($tenant->country_code)) { ?><?= $tenant->country_code ?>-<?php } ?><?= $tenant->mobile_no ?></label></span>
                                                            </li>
                                                            <li>
                                                                <span><label class="label1">Lease Start  </label><label class="label2"><?= date('m/d/y', strtotime($tenant->lease_start_date)) ?></label></span>
                                                                <span> <label class="label1">Lease End</label><label class="label2"><?= date('m/d/y', strtotime($tenant->lease_end_date)) ?></label></span>
                                                                <span><label class="label1">Rent</label><label class="label2"><?= $tenant->rent_amount ?></label></span>
                                                                <span><label class="label1">Due Date</label><label class="label2"><?= date('m/'.$tenant->due_date . '/y') ?></label></span>
                                                                <span><label class="label1">Late Fee</label><label class="label2"><?= $tenant->late_fee ?></label></span>
                                                            </li>
                                                        </ul>

                                                        <!--                                                <h2 class="lease-doc">Tenant Document</h2>
                                                                                                        <ul class="upload-file">
                                                                                                            <li><img alt="" src="<?//= $this->config->item('templateassets') ?>images/file-upload.png"><span><?= $tenant->lease_document ?></span></li>
                                                        
                                                                                                        </ul>-->
                                                    </div>       
                                                    <?php
                                                }
                                            } else {
                                                ?>
                                                <h3 class="tnt"> No tenant found for this property.</h3>
                                            <?php } ?>	
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>

            <div class="modal-footer">
                <button type="button" class="btn default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
