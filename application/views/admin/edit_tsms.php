<script>
    pageckeditor = true;

    function goBack()
    {
        var redirect_url = '<?= isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : FALSE ?>';
        if (redirect_url)
            window.location = redirect_url;
        else
            window.location = '<?= site_url($this->config->config['adminName'] . '/messages') ?>';
    }

</script>
<style type="text/css">
    .form-horizontal .form-group {
        margin-left: 0px;
        margin-right: 0px;
    }
    .my-table tbody tr td {  display: inline-block;  width: 100%;}
    .my-table tbody tr {  display: inline-block;  width: 100%;}

    .portlet.box.blue.my-panel {        padding: 0;        border: 0px;}
    .portlet.box.blue.my-panel .portlet-body  {        padding: 0;        border: 0px;        border-bottom: 1px solid #ccc}
    .portlet.box.blue.my-panel .portlet-body table  {      margin: 0;}
    .portlet.box.blue.my-panel .portlet-body table  td{}
    .portlet {border-bottom: 1px solid #ccc; border: 0px;}
    div.checker {
        margin-left: 3px;
        margin-right: 0;
        width: 30px;
    }
    .col-md-2.control-label {
        padding: 0;
        text-align: left;
        width: 150px;
        padding: 5px 0;
    }
    .input-group.date.date-picker {
        background-image: none;
        padding: 0;
    }
    .tablesorter-blue input.tablesorter-filter, .tablesorter-blue select.tablesorter-filter {
        background-position: 15px -16px !important;
        height: 36px !important;
        padding: 6px 0 6px 40px !important;
        font-family: open sans;
        font-weight: lighter;
    }
</style>
<?php
$yearArray = array(
    "1" => "Jan",
    "2" => "Feb",
    "3" => "Mar",
    "4" => "Apr",
    "5" => "May",
    "6" => "Jun",
    "7" => "Jul",
    "8" => "Aug",
    "9" => "Sep",
    "10" => "Oct",
    "11" => "Nov",
    "12" => "Dec",
        )
?>
<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->

        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <i class="fa fa-home"></i>
                    <a href="<?= site_url('admin') ?>">Home</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li><a href="<?php
                    if (isset($breadcrum1_url)) {
                        echo $breadcrum1_url;
                    } else {
                        ?>javascript:;<?php } ?>"><?= isset($breadcrum1) ? $breadcrum1 : ''; ?></a><?php if (isset($breadcrum2)) { ?><i class="fa fa-angle-right"></i><?php } ?></li>
                <li><a href="<?php
                    if (isset($breadcrum2_url)) {
                        echo $breadcrum2_url;
                    } else {
                        ?>javascript:;<?php } ?>"><?=
                           isset($breadcrum2) ? $breadcrum2 : '';
                           ;
                           ?></a><?php if (isset($breadcrum3)) { ?><i class="fa fa-angle-right"></i><?php } ?></li>
                <li><a href="<?php
                    if (isset($breadcrum3_url)) {
                        echo $breadcrum3_url;
                    } else {
                        ?>javascript:;<?php } ?>"><?=
                           isset($breadcrum3) ? $breadcrum3 : '';
                           ;
                           ?></a><?php if (isset($breadcrum4)) { ?><i class="fa fa-angle-right"></i><?php } ?></li>
                <li><a href="<?php
                    if (isset($breadcrum4_url)) {
                        echo $breadcrum4_url;
                    } else {
                        ?>javascript:;<?php } ?>"><?=
                           isset($breadcrum4) ? $breadcrum4 : '';
                           ;
                           ?></a><?php if (isset($breadcrum5)) { ?><i class="fa fa-angle-right"></i><?php } ?></li>
            </ul>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <div class="col-md-12 ">
                <!-- BEGIN SAMPLE FORM PORTLET-->
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-gift"></i>Schedule Admin Message
                        </div>

                    </div>
                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <?php echo form_open_multipart('', array('class' => 'ajaxForm form-horizontal', 'id' => '')) ?>
                        <div class="ajax_report alert display-hide" role="alert" style="margin-left:232px;width:956px;"><span class="close"></span><span class="ajax_message">Hello Message</span></div>

                        <div class="form-body">
                            <h3 class="form-section">Edit</h3>
                           


                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Select Amenity:</label>
                                        <?php
                                        $amenty = array(
                                            'heat' => 'Heat', 'ac' => 'AC',
                                            'fireexten' => 'Fire Extinguishers',
                                            'smoke' => 'Smoke Detectors', 'yard' => 'Yard Service'
                                        );
                                        ?>
                                        <div class="col-md-2">
                                            <?php
                                            if (!empty($amenties)) {
                                                foreach ($amenties as $amen) {
                                                    $amenty[$amen->id] = $amen->name;
                                                }
                                            }
                                            ?>
                                            <select name="amenty" required="true" class="form-control repeat_msg">
                                                <option value="">Select Amenity</option>
                                                <?php if (!empty($amenty)) { ?>
                                                    <?php foreach ($amenty as $key => $atype) { ?>
                                                        <option <?php if ($records->amenity == $key) {
                                                    echo 'selected="selected"';
                                                } ?> value="<?php echo $key ?>"><?php echo $atype ?></option>
    <?php } ?>
<?php } ?>
                                            </select>
                                        </div>
                                          <div class="col-md-2" style="">
                                    <button type="button" class="btn blue add_more_date">Add More Dates</button>
                                </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                              
                                <div class="col-md-12 apnd_more">
                                    <?php $saved_dates = $records->dates; $i = 0;
                                    foreach ($saved_dates as $dte) {
                                        ?>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">Select Date:</label>
                                            <div class="col-md-2">

                                                <select name="month[]" required="true" class="form-control">
                                                     <option value="">Month</option>
                                                   <?php foreach ($yearArray as $k => $year) { ?>
                                                        <option <?php if($dte->month==$k){ echo 'selected="selected"';}?> value="<?php echo $k ?>"><?php echo $year ?></option>
                                                     <?php } ?>
                                                </select>
                                            </div>
                                            <div class="col-md-2">
                                                <select name="day[]" required="true" class="form-control">
                                                    <option value="">Date</option>
                                             <?php for ($j = 1; $j < 31; $j++) { ?>
                                                        <option <?php if($dte->day==$j){ echo 'selected="selected"';} ?> value="<?php echo $j; ?>"><?php echo ordinal($j); ?></option>
                                        <?php } ?>
                                                </select>
                                            </div>
                                            <?php if($i>0){ ?>
                                             <div class="col-md-2">
                                                     <button type="button" class="btn blue remove_more_date">Remove</button>
                                            </div>
                                            
                                            <?php } ?>
                                        </div>
                                       <?php $i++;}?>


                                </div>

                            </div>
                             <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <textarea name="custom_message" class="form-control wrte_msg" placeholder="Write your message here..."><?php echo $records->message ?></textarea>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="form-actions">

                            <button class="btn blue snd_sms" type="submit"><i class="fa fa-floppy-o "></i> Save</button>
                        </div>
                        </form>
                        <!-- END FORM-->

                    </div>
                    <!-- END SAMPLE FORM PORTLET-->
                    <!-- END PAGE CONTENT-->
                </div>
                <div class="sel_html" style="display:none;">
                    <div class="form-group">
                        <label class="col-md-2 control-label">Select Date:</label>
                        <div class="col-md-2">
                           
                            <select name="month[]" class="form-control">
<?php foreach ($yearArray as $k => $year) { ?>
                                    <option value="<?php echo $k ?>"><?php echo $year ?></option>
<?php } ?>
                            </select>
                        </div>
                        <div class="col-md-2">
                            <select name="day[]" class="form-control repeat_msg">
<?php for ($j = 1; $j < 31; $j++) { ?>
                                    <option value="<?php echo $j; ?>"><?php echo ordinal($j); ?></option>
<?php } ?>
                            </select>
                        </div>
                        <div class="col-md-2">
                            <button type="button" class="btn blue remove_more_date">Remove</button>
                        </div>
                    </div>

                </div>
                <?php
                $weeks = array('1' => '1 week', '2' => '2 weeks', '3' => '3 weeks', '4' => '4 weeks');
                for ($j = 1; $j < 31; $j++) {
                    $days[$j] = ordinal($j);
                }
                ?>
                <script>
                    $(document).ready(function () {
                        $(".add_more_date").click(function () {
                            var content = $(".sel_html").html();
                            $(".apnd_more").append(content);
                        });
                        $(document).delegate('.remove_more_date', 'click', function () {
                            $(this).parent().parent().remove();
                        });
                    });
                </script>


