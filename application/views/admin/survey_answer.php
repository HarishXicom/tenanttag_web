<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->

        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <i class="fa fa-home"></i>
                    <a href="<?= site_url('admin') ?>">Home</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li><a href="<?php if (isset($breadcrum1_url)) {
                        echo $breadcrum1_url;
                    } else { ?>javascript:;<?php } ?>"><?= isset($breadcrum1) ? $breadcrum1 : ''; ?></a><?php if (isset($breadcrum2)) { ?><i class="fa fa-angle-right"></i><?php } ?></li>
                                    <li><a href="<?php if (isset($breadcrum2_url)) {
                        echo $breadcrum2_url;
                    } else { ?>javascript:;<?php } ?>"><?= isset($breadcrum2) ? $breadcrum2 : '';
                    ; ?></a><?php if (isset($breadcrum3)) { ?><i class="fa fa-angle-right"></i><?php } ?></li>
                                    <li><a href="<?php if (isset($breadcrum3_url)) {
                        echo $breadcrum3_url;
                    } else { ?>javascript:;<?php } ?>"><?= isset($breadcrum3) ? $breadcrum3 : '';
                    ; ?></a><?php if (isset($breadcrum4)) { ?><i class="fa fa-angle-right"></i><?php } ?></li>
                                    <li><a href="<?php if (isset($breadcrum4_url)) {
                        echo $breadcrum4_url;
                    } else { ?>javascript:;<?php } ?>"><?= isset($breadcrum4) ? $breadcrum4 : '';
                    ; ?></a><?php if (isset($breadcrum5)) { ?><i class="fa fa-angle-right"></i><?php } ?></li>
            </ul>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <div class="col-md-12 ">
                <!-- BEGIN SAMPLE FORM PORTLET-->
                <div class="portlet box blue ">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-gift"></i> <b>Question:- </b><?php echo $question->question; ?>
                        </div>
                        <div class="tools">

                        </div>
                    </div>
                    <div class="portlet-body form">
                    </div>
                </div>
                <h3>Answers</h3>
                <!-- END SAMPLE FORM PORTLET-->


                 <table class="table table-striped table-bordered table-hover" id="<?php if ($records) { ?>sample_1<?php } ?>">
                            <thead>
                                <tr role="row">
                         
                                <th class="no_record">
                                    Username
                                </th>
                                <th class="no_record">
                                    Answer
                                </th>
                                <th class="no_record">
                                    Answer 1
                                </th>
                                <th class="no_record">
                                    Answer 2
                                </th>
                                <th class="no_record">
                                    Answer 3
                                </th>
                                <th class="no_record">
                                    Answer 4
                                </th>
                                <th class="no_record">
                                    Answer 5
                                </th>
                                <th class="no_record">
                                    Modified date 
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php if ($records) { ?>    
                                    <?php foreach ($records as $key => $value) { ?>
                                        <tr class="gradeX odd" role="row">
                                            <th class="sorting_1">
                                               <?php echo $value['first_name'].' '.$value['last_name']; ?>
                                            </th>
                                            <td class="sorting_1">
                                               <?php echo $value['answer']; ?>
                                            </td>
                                            <td class="sorting_1">
                                               <?php echo $value['answer1']; ?>
                                            </td>
                                            <td class="sorting_1">
                                               <?php echo $value['answer2']; ?>
                                            </td>
                                            <td class="sorting_1">
                                               <?php echo $value['answer3']; ?>
                                            </td>
                                            <td class="sorting_1">
                                               <?php echo $value['answer4']; ?>
                                            </td>
                                            <td class="sorting_1">
                                               <?php echo $value['answer5']; ?>
                                            </td>
                                            <td class="sorting_1">
                                                    <?= $value['modified_date']; ?>
                                            </td>
                                        </tr>
                                        <?php }
                                    } else { ?>
                                        <tr class="gradeX odd" role="row"><td style="text-align:center;" colspan="3">No records found...</td></tr>
                                    <?php } ?>  
                            </tbody>
                        </table>

            </div>
            <!-- END PAGE CONTENT-->

        </div>
    </div>
</div>

