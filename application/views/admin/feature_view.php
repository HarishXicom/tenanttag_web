<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">Detail</h4>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped table-bordered table-hover" id="sample_1">
                <thead>
                    <tr>
                        <th>Field Name</th>
                        <th>Data</th>
                    </tr>
                </thead>
                <tbody>

                    <tr class="odd gradeX">
                        <td>ID </td>
                        <td><?= $records->pg_id ?></td>
                    </tr>
                    <tr class="odd gradeX">
                        <td>Feature Name</td>
                        <td><?= $records->name ?></td>
                    </tr>
                    <tr class="odd gradeX">
                        <td>Title</td>
                        <td><?= $records->title ?></td>
                    </tr>
                    <tr class="odd gradeX">
                        <td>Description</td>
                        <td><?= $records->description ?></td>
                    </tr>
                    <tr class="odd gradeX">
                        <td>Meta title</td>
                        <td><?= $records->meta_title ?></td>
                    </tr>
                    <tr class="odd gradeX">
                        <td>Meta keywords</td>
                        <td><?= $records->meta_keywords ?></td>
                    </tr>
                    <tr class="odd gradeX">
                        <td>Meta Description</td>
                        <td><?= $records->meta_description ?></td>
                    </tr>
                    <tr>
                        <td>Add Date</td>
                        <td><?= date('m/d/y', $records->add_date) ?></td>
                    </tr>
                    <tr class="odd gradeX">
                        <td>Status</td>
                        <td><?= $records->status ?></td>
                    </tr>

                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="modal-footer">
    <button type="button" class="btn default" data-dismiss="modal">Close</button>
</div>
