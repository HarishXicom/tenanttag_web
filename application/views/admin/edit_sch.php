<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title"> Detail</h4>
</div>
<form action="" method="POST" id="edit_sch_sms">
    <input type="hidden" name="id" value="<?= $records->id ?>"/>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <div role="alert" class="ajax_report alert alert-success" style="display:none">
                    <span class="close"></span>Selected record has been updated successfully</div>
                <table class="table table-striped table-bordered table-hover" id="sample_1">
                    <thead>
                        <tr>
                            <th>Field Name</th>
                            <th>Data</th>
                        </tr>
                    </thead>
                    <tbody>

                        <tr>
                            <td>Scheduled</td>
                            <td class="sorting_1">
                                <div class="input-group date date-picker" data-date-format="dd-mm-yyyy">
                                    <input type="text" class="form-control" readonly name="sch_date" value="<?= date('d-m-Y', strtotime($records->scheduled_on)); ?>">
                                    <span class="input-group-btn">
                                        <button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
                                    </span>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>Repeat Type</td>
                            <td>
                                <select  name="edit_repeat_msg"   class="form-control my-select-1 edit_repeat_msg">
                                    <option value="week" <?php if ($records->repeat_type == 'week') {
    echo 'selected="selected"';
} ?>>Weekly</option>
                                    <option value="month" <?php if ($records->repeat_type == 'month') {
    echo 'selected="selected"';
} ?>>Monthly</option>
                                    <option value="year" <?php if ($records->repeat_type == 'year') {
    echo 'selected="selected"';
} ?>>Yearly</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Repeat Every </td>
                            <td>
                                <select id="erv" name="edit_repeat_every" class="form-control my-select-1 shw_rpt_type"  <?php if ($records->repeat_type == 'year') {
    echo 'style="display:none"';
} ?>>
                                    <?php if ($records->repeat_type == 'month') { ?>
                                        <?php
                                        for ($j = 1; $j < 31; $j++) {
                                            if ($records->repeat_every == $j) {
                                                echo '<option selected="selected" value="' . $j . '">' . ordinal($j) . '</option>';
                                            } else {
                                                echo '<option value="' . $j . '">' . ordinal($j) . '</option>';
                                            }
                                        }
                                        ?>
                                    <?php } else { ?>
                                        <?php
                                        $weeks = array('1' => '1 week', '2' => '2 weeks', '3' => '3 weeks', '4' => '4 weeks');
                                        foreach ($weeks as $key => $weekday) {
                                            if ($records->repeat_every == $key) {
                                                echo '<option selected="selected" value="' . $key . '">' . $weekday . '</option>';
                                            } else {
                                                echo '<option value="' . $key . '">' . $weekday . '</option>';
                                            }
                                        }
                                        ?>
<?php } ?>
                                </select>
<?php // $records->repeat_every;   ?>
                            </td>
                        </tr>
                        <tr>
                            <td>Repeat On</td>
                            <td>
                                <select id="ero" name="edit_repeat_on" class="form-control my-select-1 shw_rpt_on" <?php if ($records->repeat_type == 'year' || $records->repeat_type == 'month') {
    echo 'style="display:none"';
} ?>>
                                    <option <?php if ($records->repeat_on == 'Monday') {
    echo 'selected="selected"';
} ?> >Monday</option>
                                    <option <?php if ($records->repeat_on == 'Tuesday') {
    echo 'selected="selected"';
} ?>>Tuesday</option>
                                    <option <?php if ($records->repeat_on == 'Wednesday') {
                                        echo 'selected="selected"';
                                    } ?>>Wednesday</option>
                                    <option <?php if ($records->repeat_on == 'Thursday') {
                                        echo 'selected="selected"';
                                    } ?>>Thursday</option>
                                    <option <?php if ($records->repeat_on == 'Friday') {
                                        echo 'selected="selected"';
                                    } ?>>Friday</option>
                                    <option <?php if ($records->repeat_on == 'Saturday') {
                                        echo 'selected="selected"';
                                    } ?>>Saturday</option>
                                    <option <?php if ($records->repeat_on == 'Sunday') {
                                        echo 'selected="selected"';
                                    } ?>>Sunday</option>
                                </select>
<?php // $records->repeat_on;  ?>
                            </td>
                        </tr>
                        <tr>
                            <td>End</td>
                            <td>
                                <select id="ert" name="repeat_occurence" class="form-control my-select-1">
<?php for ($k = 1; $k < 13; $k++) { ?>
                                        <option <?php if ($records->occurences == $k) {
        echo 'selected="selected"';
    } ?> value="<?php echo $k ?>">Repeat <?php echo $k; ?> Times</option>
<?php } ?>
                                    <option <?php if ($records->end_repeat == 'infinte') {
    echo 'selected="selected"';
} ?> value="noend">Repeat Infinite</option>
                                </select>
                            </td>
                        </tr>
                    </tbody>
                </table>

            </div>
        </div>
    </div>

    <div class="modal-footer">
        <button type="submit" class="btn green">Update</button>
        <button type="button" class="btn default" data-dismiss="modal">Close</button>
    </div>
</form>
<style>
    .input-group.date.date-picker {
        background-image: none;
        padding: 0;
    }
</style>
<?php
$weeks = array('1' => '1 week', '2' => '2 weeks', '3' => '3 weeks', '4' => '4 weeks');
for ($j = 1; $j < 31; $j++) {
    $days[$j] = ordinal($j);
}
?>
<script>
    $(document).ready(function () {
        var date = new Date();
        date.setDate(date.getDate());
        $('.date-picker').datepicker({
            startDate: date
        });

        $(document).delegate('.edit_repeat_msg', 'change', function () {

            if ($(this).val() == 'week') {
                $("#erv").html('');
                var weeks = '<?php echo json_encode($weeks); ?>';
                weeks = JSON.parse(weeks);
                $.each(weeks, function (key, value) {
                    $("#erv").append('<option value="' + key + '">' + value + '</option>');
                });
                $("#erv").show();
                $("#ero").show();
            }
            else if ($(this).val() == 'month') {
                $("#erv").html('');
                var dayz = '<?php echo json_encode($days); ?>';
                dayz = JSON.parse(dayz);
                $.each(dayz, function (key, value) {
                    $("#erv").append('<option value="' + key + '">' + value + '</option>');
                });
                $("#erv").show();
                $("#ero").hide();
            } else {
                $("#erv").html('');
                $("#erv").hide();
                $("#ero").hide();
            }
        });

        $(".green").click(function (e) {
            e.preventDefault();
            save_sch_sms()
        })

        function save_sch_sms(e)
        {
            $.ajax({
                url: '<?php echo base_url('admin/messages/update_message'); ?>',
                type: 'POST',
                data: $("#edit_sch_sms").serialize(),
                beforeSend: function () {
                    $('#wait-div').show();
                },
                success: function (data) {
                    $("#wait-div").hide();
                    $(".ajax_report").show();
                },
                error: function () {
                    $("#wait-div").hide();
                }
            });
        }

    });
</script>