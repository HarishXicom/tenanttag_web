<script type="text/javascript">
    function makeSearchUrl()
    {
        var key = $('#filterKey').val();
        if (key == '')
            key = 'NULL';
        var show_me = $('#show_me').val();
        if (show_me == '')
            show_me = 'NULL';

        var sort_by = $('#sort_by').val();
        if (sort_by == '')
            sort_by = 'NULL';
        window.location = '<?= site_url($this->config->config['adminName'] . '/features/filter') ?>/' + key + '/' + show_me + '/' + sort_by;
    }
    function resetsearch()
    {
        window.location = '<?= site_url($this->config->config['adminName'] . '/features') ?>';
    }

    function changevalue($this, current)
    {
        $('#filterKey').val(current);
    }
</script>	
<div class="page-content-wrapper">
    <div class="page-content" style="min-height:1161px">

        <!-- BEGIN PAGE HEADER-->

        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <i class="fa fa-home"></i>
                    <a href="<?= site_url('admin') ?>">Home</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li><a href="<?php if (isset($breadcrum1_url)) {
    echo $breadcrum1_url;
} else { ?>javascript:;<?php } ?>"><?= isset($breadcrum1) ? $breadcrum1 : ''; ?></a><?php if (isset($breadcrum2)) { ?><i class="fa fa-angle-right"></i><?php } ?></li>
                <li><a href="<?php if (isset($breadcrum2_url)) {
    echo $breadcrum2_url;
} else { ?>javascript:;<?php } ?>"><?= isset($breadcrum2) ? $breadcrum2 : '';
; ?></a><?php if (isset($breadcrum3)) { ?><i class="fa fa-angle-right"></i><?php } ?></li>
                <li><a href="<?php if (isset($breadcrum3_url)) {
    echo $breadcrum3_url;
} else { ?>javascript:;<?php } ?>"><?= isset($breadcrum3) ? $breadcrum3 : '';
; ?></a><?php if (isset($breadcrum4)) { ?><i class="fa fa-angle-right"></i><?php } ?></li>
                <li><a href="<?php if (isset($breadcrum4_url)) {
    echo $breadcrum4_url;
} else { ?>javascript:;<?php } ?>"><?= isset($breadcrum4) ? $breadcrum4 : '';
; ?></a><?php if (isset($breadcrum5)) { ?><i class="fa fa-angle-right"></i><?php } ?></li>
            </ul>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <!--<a href="<?php // site_url($this->config->config['adminName'] . '/pages/filter/NULL/NULL/NULL') ?>" class="widget widget-hover-effect1">-->
                    <div class="dashboard-stat blue">
                        <div class="visual">
                            <i class="fa fa-file-text"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                <strong>Total</strong><br>
                                <small><?= $total ?></small>
                            </div>
                        </div>
                    </div>
                <!--</a>-->
            </div>

            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <!--<a href="<?php // site_url($this->config->config['adminName'] . '/pages/filter/NULL/Active/NULL') ?>" class="widget widget-hover-effect1">-->
                    <div class="dashboard-stat green">
                        <div class="visual">
                            <i class="fa fa-check-square-o fa-1x"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                <strong>Active</strong><br>
                                <small><?= $totalActive ?></small>
                            </div>
                        </div>
                    </div>
                <!--</a>-->
            </div>

            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <!--<a href="<?php // site_url($this->config->config['adminName'] . '/pages/filter/NULL/Inactive/NULL') ?>" class="widget widget-hover-effect1">-->
                    <div class="dashboard-stat red">
                        <div class="visual">
                            <i class="fa fa-exclamation fa-1x"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                <strong>Inactive</strong><br>
                                <small><?= $totalInactive ?></small>
                            </div>
                        </div>
                    </div>
                <!--</a>-->
            </div>


        </div>


        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-globe"></i><?= $subpageName ?> (<?= sizeof($records) ?> records)
                        </div>
                        <div class="actions">
                            <!-- <a class="btn btn-default btn-sm" href="<?= $breadcrum3_url ?>">
                                <i class="fa fa-plus"></i> Add </a>
                            <div class="btn-group">

                                <a data-toggle="dropdown" href="javascript:;" class="btn btn-default btn-sm" aria-expanded="false">
                                    <i class="fa fa-cogs"></i> Tools <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li>
                                        <a href="javascript:;" data-value="Active" id="multiple_active">
                                            <i class="fa fa-circle"></i> Active 
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;" data-value="Inactive" id="multiple_inactive">
                                            <i class="fa fa-circle-o"></i> Inactive 
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;" data-value="delete" id="multiple_delete">
                                            <i class="fa fa-trash-o"></i> Delete 
                                        </a>
                                    </li>
                                </ul>

                            </div> -->
                        </div> 

                    </div>
                    <div class="portlet-body">

                        <table class="table table-striped table-bordered table-hover" id="<?php if ($records) { ?>sample_1<?php } ?>">
                            <thead>
                                <tr role="row">
                             
                            <th>
                                Id
                            </th>
                            <th class="no_record">
                                Feature Name
                            </th>
                            <th class="no_record">
                                Title
                            </th>
                            <th class="no_record">
                                Feature Link
                            </th>
                            <th class="no_record">
                                add date
                            </th>
                            <th class="no_record">
                                Status
                            </th>
                            <th class="no_record">
                                Action
                            </th>
                            </tr>
                            </thead>
                            <tbody>
<?php if ($records) { ?>	
    <?php foreach ($records as $value) { ?>
                                        <tr class="gradeX odd" role="row">
                                            
                                            <td class="sorting_1">
        <?= $value->pg_id ?>
                                            </td>
                                            <td class="sorting_1">
                                                    <?= $this->common_model->showLimitedText($value->name, 50); ?>
                                            </td>
                                            <td class="sorting_1">
        <?= $this->common_model->showLimitedText($value->title, 50); ?>
                                            </td>
                                            <td class="sorting_1">
                                                <a href="<?= site_url('feature') ?>/<?= $value->slug ?>" target="_blank">
                                                    <font color="#666666">
                                                <?= site_url('feature') ?>/<?= $this->common_model->showLimitedText($value->slug, 70) ?>
                                                    </font>
                                                </a>
                                            </td>
                                            <td class="center">
                                                <span class="label label-sm label-danger">
                                        <?= date('m/d/y', $value->add_date) ?>
                                                </span>
                                            </td>
                                            <td>
                                                <span class="label label-sm <?php if ($value->status == 'Active') { ?> label-success<?php } else { ?> label-warning<?php } ?>">
                                <?= $value->status ?> </span>
                                            </td>
                                            <td>

                                                <a href="<?= site_url('admin/features/view/' . $value->pg_id) ?>" data-target="#large" data-toggle="modal" class="config btn btn-sm blue" data-title="view" ><i class="fa fa-eye"></i></a>

                                                <a href="<?= site_url('admin/features/edit/' . $value->pg_id) ?>" class="config btn blue btn-sm" title="Edit"><i class="fa fa-pencil"></i></a>
                                                <?php if ($value->status == 'Inactive') { ?>
                                               <!--     <a href="<?= site_url('admin/features/doTask/Active/' . $value->pg_id) ?>" data-target="#general" data-toggle="modal" class="config btn btn-sm red" title="Active"><i class="fa fa-circle"></i></a>-->
                                                <?php } else { ?>
                                                    <!--<a href="<?= site_url('admin/features/doTask/Inactive/' . $value->pg_id) ?>" data-target="#general" data-toggle="modal" class="config btn btn-sm blue" title="Inactive"><i class="fa fa-circle-o"></i></a>-->
                                                <?php } ?>
                                                <!--<a href="<?= site_url('admin/features/doTask/delete/' . $value->pg_id) ?>" data-target="#general" data-toggle="modal" class="config btn btn-sm red" title="delete"><i class="fa fa-trash"></i></a>-->
                                            </td>
                                        </tr>
    <?php }
} else { ?>
                                    <tr class="gradeX odd" role="row"><td style="text-align:center;" colspan="10">No records found...</td></tr>
<?php } ?>	
                            </tbody>
                        </table>
<?= form_close(); ?>
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="dataTables_paginate paging_bootstrap_full_number" id="sample_1_paginate">

<?= isset($paging)?$paging:'' ?>


                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <!-- END PAGE CONTENT-->
            </div>

            <!-- END PAGE CONTENT-->
        </div>
    </div>
