<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<h3 class="page-title">
			My Profile 
			</h3>
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="<?=site_url('admin')?>">Home</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="javascript:;">My Profile</a>
					</li>
				</ul>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row profile">
				<div class="col-md-12">
					<!--BEGIN TABS-->
					<div class="tabbable-line tabbable-full-width">
						<ul class="nav nav-tabs">
							<li class="active">
								<a href="#tab_1_3" data-toggle="tab">
								Account </a>
							</li>
						</ul>
						<div class="tab-content">
							<div class="tab-pane active" id="tab_1_3">
								<div class="row profile-account">
									<div class="col-md-3">
										<ul class="ver-inline-menu tabbable margin-bottom-10">
											<li>
												<img src="<?=site_url().'assets/uploads/admin_image/'.$record->profile_pic?>" class="img-responsive" alt=""/>
												
											</li>
											<li class="active">
												<a data-toggle="tab" href="#tab_1-1">
												<i class="fa fa-cog"></i> Personal info </a>
												<span class="after">
												</span>
											</li>
											<li>
												<a data-toggle="tab" href="#tab_2-2">
												<i class="fa fa-picture-o"></i> Change Avatar </a>
											</li>
											<li>
												<a data-toggle="tab" href="#tab_3-3">
												<i class="fa fa-lock"></i> Change Password </a>
											</li>
										</ul>
									</div>
									<div class="col-md-9">
										<div class="tab-content">
											<div id="tab_1-1" class="tab-pane active">
												<?php echo form_open_multipart('',array('class'=>'ajaxForm form-horizontal','id'=>'personal-info'))?>
													<div class="ajax_report alert display-hide" role="alert" style="margin-left: -14px; width: 1004px;"><span class="close"></span><span class="ajax_message">Hello Message</span></div>
													<div class="form-group">
														<label class="">Name</label>
														<input type="text" placeholder="Admin Name" name="name" value="<?=$name?>" class="form-control"/>
													</div>
													<div class="form-group">
														<label class="">Email</label>
														<input type="email" placeholder="Email Address" name="email" value="<?=$email?>" class="form-control"/>
													</div>
													<div class="form-group">
														<label class="">Mobile Number</label>
														<input type="text" placeholder="+1 646 580 DEMO (6284)" name="mobile_no" value="<?=$mobile_no?>" class="form-control"/>
													</div>
													<div class="form-group">
														<label class="">Address</label>
														<textarea class="form-control" rows="3" placeholder="Enter your Address" name="address"><?=$address?></textarea>
													</div>
													<input type="hidden" name="form_post_for" value="personal_profile">
													<div class="margiv-top-10">
														<button type="submit" class="btn green">Submit</button>
														<!--<button type="button" class="btn default">Cancel</button>-->
													</div>
												<?php echo form_close();?>
											</div>
											<div id="tab_2-2" class="tab-pane">
												
												<?php echo form_open_multipart('',array('class'=>'ajaxForm form-horizontal','id'=>'admin-image'))?>
													<div class="ajax_report alert display-hide" role="alert" style="margin-left: -14px; width: 1004px;"><span class="close"></span><span class="ajax_message">Hello Message</span></div>
													<div class="form-group">
														<div class="fileinput fileinput-new" data-provides="fileinput">
															<div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
																<?php if($record->profile_pic=='') { ?>
																	<img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""/>
																<?php } else { ?>
																	<img src="<?=site_url().'assets/uploads/admin_image/'.$record->profile_pic?>" alt=""/>
																<?php } ?>
															</div>
															<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
															</div>
															<div>
																<span class="btn default btn-file">
																<span class="fileinput-new">
																Select image </span>
																<span class="fileinput-exists">
																Change </span>
																<input type="file" name="admin_icon_image">
																</span>
																<a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput">
																Remove </a>
															</div>
														</div>
														<div class="clearfix margin-top-10">
															<span class="label label-danger">
															NOTE! </span>
															<span>
															Attached image thumbnail is supported in Latest Firefox, Chrome, Opera, Safari and Internet Explorer 10 only </span>
														</div>
														<input type="hidden" name="form_post_for" value="admin_image">
													</div>
													<div class="margin-top-10">
														<button type="submit" class="btn green">Submit</button>
														<!--<button type="button" class="btn default">Cancel</button>-->
													</div>
												<?php echo form_close();?>
											</div>
											<div id="tab_3-3" class="tab-pane">
												<?php echo form_open_multipart('',array('class'=>'ajaxForm form-horizontal','id'=>'admin-change-password'))?>
													<div class="ajax_report alert display-hide" role="alert" style="margin-left: -14px; width: 1004px;"><span class="close"></span><span class="ajax_message">Hello Message</span></div>
													<div class="form-group">
														<label class="">Current Password</label>
														<input type="password" class="form-control" name="old_password"/>
													</div>
													<div class="form-group">
														<label class="">New Password</label>
														<input type="password" class="form-control" name="new_password"/>
													</div>
													<div class="form-group">
														<label class="">Re-type New Password</label>
														<input type="password" class="form-control" name="re_password"/>
													</div>
													<input type="hidden" name="form_post_for" value="change_password">
													<div class="margin-top-10">
														<button type="submit" class="btn green">Submit</button>
														<button type="reset" class="btn default">Reset</button>
													</div>
												<?php echo form_close();?>
											</div>
										</div>
									</div>
									<!--end col-md-9-->
								</div>
							</div>
							<!--end tab-pane-->
						</div>
					</div>
					<!--END TABS-->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	
