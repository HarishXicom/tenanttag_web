<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->

        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <i class="fa fa-home"></i>
                    <a href="<?= site_url('admin') ?>">Home</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li><a href="<?php if (isset($breadcrum1_url)) {
    echo $breadcrum1_url;
} else { ?>javascript:;<?php } ?>"><?= isset($breadcrum1) ? $breadcrum1 : ''; ?></a><?php if (isset($breadcrum2)) { ?><i class="fa fa-angle-right"></i><?php } ?></li>
                <li><a href="<?php if (isset($breadcrum2_url)) {
    echo $breadcrum2_url;
} else { ?>javascript:;<?php } ?>"><?= isset($breadcrum2) ? $breadcrum2 : '';
; ?></a><?php if (isset($breadcrum3)) { ?><i class="fa fa-angle-right"></i><?php } ?></li>
                <li><a href="<?php if (isset($breadcrum3_url)) {
    echo $breadcrum3_url;
} else { ?>javascript:;<?php } ?>"><?= isset($breadcrum3) ? $breadcrum3 : '';
; ?></a><?php if (isset($breadcrum4)) { ?><i class="fa fa-angle-right"></i><?php } ?></li>
                <li><a href="<?php if (isset($breadcrum4_url)) {
    echo $breadcrum4_url;
} else { ?>javascript:;<?php } ?>"><?= isset($breadcrum4) ? $breadcrum4 : '';
; ?></a><?php if (isset($breadcrum5)) { ?><i class="fa fa-angle-right"></i><?php } ?></li>
            </ul>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <div class="col-md-12 ">
                <!-- BEGIN SAMPLE FORM PORTLET-->
                <div class="portlet box blue ">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-gift"></i> <?= isset($subpageName) ? $subpageName : '' ?>
                        </div>
                        <div class="tools">
                                
                        </div>
                    </div>
                </div>
                    <div class="portlet-body form form-body">
                        <div class="col-md-1">
                        
                        </div>
                        <div class="col-md-11">
                            <b>Question:-</b>  <?php echo isset($question_info->question) ? $question_info->question : ''; ?>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <?php echo form_open_multipart('', array('class' => 'ajaxForm form-horizontal', 'id' => 'common_answer')) ?>
                        <div class="ajax_report alert display-hide" role="alert" style="margin-left:232px;width:956px;"><span class="close"></span><span class="ajax_message">Hello Message</span></div>
                        <div class="form-body">
                            
                            <div class="form-group">
                                <label class="col-md-2 control-label"> </label>
                                <div class="col-md-9">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">Answer </label>
                                <div class="col-md-9">
                                    <input type="text" id="answer" style="width:100%;height:35px;" value="<?php echo isset($edit_answer_info->answer) ? $edit_answer_info->answer : ''; ?>" name="answer">
                                    <p class="help-block">
                                        Common answer...
                                    </p>
                                </div>
                            </div>


                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn green">Submit</button>
                                </div>
                            </div>
                        </div>
<?= form_close(); ?>
                    </div>
                </div>
                <!-- END SAMPLE FORM PORTLET-->


                 <table class="table table-striped table-bordered table-hover" id="<?php if ($answer_records) { ?>sample_1<?php } ?>">
                            <thead>
                                <tr role="row">
                            <th>
                                Id
                            </th>
                            <th class="no_record">
                                Question
                            </th>
                            <th class="no_record">
                                Created date 
                            </th>
                            <th class="no_record">
                                Action
                            </th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php if ($answer_records) { ?>    
                                    <?php foreach ($answer_records as $key => $value) { ?>
                                        <tr class="gradeX odd" role="row">
                                            <td class="sorting_1">
                                                <?= $value->id ?>
                                            </td>
                                            <td class="sorting_1">
                                               <?php echo $value->answer; ?>
                                            </td>
                                            <td class="sorting_1">
                                                    <?= $value->created_date; ?>
                                            </td>
                                            <td>  
                                                <a href="<?= site_url('admin/common_question/edit_answer/' . $value->question_id.'/'.$value->id) ?>" class="config btn btn-sm blue" title="Edit"><i class="fa fa-pencil"></i></a>
                                                <a href="<?= site_url('admin/common_question/delete_answer/' . $value->question_id.'/'.$value->id) ?>" class="config btn btn-sm red"  title="delete"><i class="fa fa-trash"></i></a>
                                            </td>
                                        </tr>
                                        <?php }
                                    } else { ?>
                                        <tr class="gradeX odd" role="row"><td style="text-align:center;" colspan="3">No records found...</td></tr>
                                    <?php } ?>  
                            </tbody>
                        </table>

            </div>
            <!-- END PAGE CONTENT-->

        </div>
    </div>
</div>

