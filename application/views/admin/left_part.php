<!-- BEGIN SIDEBAR -->
<div class="page-sidebar-wrapper">
    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
    <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
    <div class="page-sidebar navbar-collapse collapse">
        <!-- BEGIN SIDEBAR MENU -->
        <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
        <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
        <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
        <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
        <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
        <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
        <ul class="page-sidebar-menu page-sidebar-menu-light" data-keep-expanded="true" data-auto-scroll="true" data-slide-speed="200">
            <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
            <li class="sidebar-toggler-wrapper">
                <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                <div class="sidebar-toggler">
                </div>
                <!-- END SIDEBAR TOGGLER BUTTON -->
            </li>
            <li class="start <?php if (isset($pageName) && $pageName == 'General Settings') { ?>active open<?php } ?>">
                <a href="javascript:;">
                    <i class="fa fa-cogs"></i><span class="title">General Settings</span>
                    <span class=""></span><span class="arrow "></span>
                </a>
                <ul class="sub-menu" >
                    <li class=" <?php if (isset($subpageName) && $subpageName == 'Website Settings') { ?> active<?php } ?>">
                        <a href="<?= site_url('admin/website-settings') ?>">
                            <i class="icon-docs"></i>
                            Global Page Settings</a>
                    </li>
                    <!--
                                                                    <li class=" <?php if ($subpageName == 'Email Settings') { ?> active<?php } ?>">
                                                                            <a href="<?= site_url('admin/email-settings') ?>">
                                                                            <i class="icon-bulb"></i>
                                                                            Email Settings</a>
                                                                    </li>
                    -->
                    <li class=" <?php if (isset($subpageName) && $subpageName == 'Social Settings') { ?> active<?php } ?>">
                        <a href="<?= site_url('admin/social-settings') ?>">
                            <i class="icon-graph"></i>
                            Social Settings</a>
                    </li>
                    <li class=" <?php if (isset($subpageName) && $subpageName == 'Resources') { ?> active<?php } ?>">
                        <a href="<?= site_url('admin/resources') ?>">
                            <i class="icon-graph"></i>
                            Resources</a>
                    </li>
                </ul>
            </li>

            <li class="start <?php if (isset($pageName) && $pageName == 'Contents') { ?>active open<?php } ?>">
                <a href="<?= site_url('admin/pages') ?>">
                    <i class="fa fa-sitemap"></i>
                    <span class="title">Contents</span>
                    <span class=""></span>
                    <span class="arrow "></span>
                </a>
                <ul class="sub-menu" >
                    <li class=" <?php if (isset($subpageName) && ($subpageName == 'Static Pages' || $subpageName == 'Add Static Page' || $subpageName == 'Edit Static Page')) { ?> active<?php } ?>">
                        <a href="<?= site_url('admin/pages') ?>">
                            <i class="icon-bar-chart"></i>
                            Static Pages</a>
                    </li>
                    <li class=" <?php if (isset($subpageName) && ($subpageName == 'Features' || $subpageName == 'Add Feature Page' || $subpageName == 'Edit Feature Page')) { ?> active<?php } ?>">
                        <a href="<?= site_url('admin/features') ?>">
                            <i class="icon-bar-chart"></i>
                            Features</a>
                    </li>

                </ul>
            </li>

            <li class="start <?php if (isset($pageName) && $pageName == 'Member Manager') { ?>active open<?php } ?>">
                <a href="javascript:;">
                    <i class="fa fa-group"></i>
                    <span class="title">Member Manager</span>
                    <span class=""></span>
                    <span class="arrow "></span>
                </a>
                <ul class="sub-menu" >
                    <li class=" <?php if (isset($subpageName) && $subpageName == 'All Landlords') { ?> active<?php } ?>">
                        <a href="<?= site_url('admin/landlords') ?>">
                           <i class="fa fa-users"></i>
                            All Landlords</a>
                    </li>
                    <li class=" <?php if (isset($subpageName) && $subpageName == 'Add Landlord') { ?> active<?php } ?>">
                        <a href="<?= site_url('admin/landlords/add') ?>">
                            <i class="fa fa-plus-square"></i>
                            Add Landlord</a>
                    </li>
                    <li class=" <?php if (isset($subpageName) && $subpageName == 'All Tenants') { ?> active<?php } ?>">
                        <a href="<?= site_url('admin/tenants') ?>">
                            <i class="fa fa-users"></i>
                            All Tenants</a>
                    </li>
                    <li class=" <?php if (isset($subpageName) && $subpageName == 'Add Tenant') { ?> active<?php } ?>">
                        <a href="<?= site_url('admin/tenants/add') ?>">
                            <i class="fa fa-plus-square"></i>
                            Add Tenant</a>
                    </li>
                </ul>
            </li>

            <li class="start <?php if (isset($pageName) && $pageName == 'Amenties') { ?>active open<?php } ?>">
                <a href="javascript:;">
                    <i class="fa fa-gears"></i>
                    <span class="title">Amenities</span>
                    <span class=""></span>
                    <span class="arrow "></span>
                </a>
                <ul class="sub-menu" >
                    <li class=" <?php if (isset($subpageName) && $subpageName == 'All Amenties') { ?> active<?php } ?>">
                        <a href="<?= site_url('admin/amenties') ?>">
                            <i class="fa fa-list"></i>
                            All Amenities</a>
                    </li>
                    <li class=" <?php if (isset($subpageName) && $subpageName == 'Add Amentie') { ?> active<?php } ?>">
                        <a href="<?= site_url('admin/amenties/add') ?>">
                            <i class="fa fa-plus-square"></i>
                            Add Amenities</a>
                    </li>
                    <li class=" <?php if (isset($subpageName) && $subpageName == 'All Services') { ?> active<?php } ?>">
                        <a href="<?= site_url('admin/services') ?>">
                           <i class="fa fa-list"></i>
                            All More</a>
                    </li>
                    <li class=" <?php if (isset($subpageName) && $subpageName == 'Add Service') { ?> active<?php } ?>">
                        <a href="<?= site_url('admin/services/add') ?>">
                            <i class="fa fa-plus-square"></i>
                            Add More</a>
                    </li>
                </ul>
            </li>

            <li class="start <?php if (isset($pageName) && $pageName == 'Services') { ?>active open<?php } ?>">
                <a href="javascript:;">
                    <i class="fa fa-sort-amount-desc"></i>
                    <span class="title">tMaintenance</span>
                    <span class=""></span>
                    <span class="arrow "></span>
                </a>
                <ul class="sub-menu" >

                    <li class=" <?php if (isset($subpageName) && $subpageName == 'All Tservices') { ?> active<?php } ?>">
                        <a href="<?= site_url('admin/tservices') ?>">
                           <i class="fa fa-list-alt"></i>
                            All Categories</a>
                    </li>
                    <li class=" <?php if (isset($subpageName) && $subpageName == 'Add TService') { ?> active<?php } ?>">
                        <a href="<?= site_url('admin/tservices/add') ?>">
                           <i class="fa fa-plus-square"></i>
                            Add Category</a>
                    </li>
                </ul>
            </li>
           
            <li class="start <?php if (isset($pageName) && $pageName == 'Messages') { ?>active open<?php } ?>">
                <a href="javascript:;">
                    <i class="fa fa-envelope"></i>
                    <span class="title">Messages</span>
                    <span class=""></span>
                    <span class="arrow "></span>
                </a>
                <ul class="sub-menu" >
                    <li class=" <?php if (isset($subpageName) && $subpageName == 'Sent Messages') { ?> active<?php } ?>">
                        <a href="<?= site_url('admin/messages/sent_sms') ?>">
                            <i class="fa fa-paper-plane"></i>
                            Sent Messages</a>
                    </li>
                     <li class=" <?php if (isset($subpageName) && $subpageName == 'Scheduled Messages') { ?> active<?php } ?>">
                        <a href="<?= site_url('admin/messages/scheduled_sms') ?>">
                            <i class="fa fa-clock-o"></i>
                            Scheduled Messages</a>
                    </li>
                    <li class=" <?php if (isset($subpageName) && $subpageName == 'Create Message') { ?> active<?php } ?>">
                        <a href="<?= site_url('admin/messages/create') ?>">
                            <i class="fa fa-plus-square"></i>
                            Add Message</a>
                    </li>
                    <li class=" <?php if (isset($subpageName) && $subpageName == 'Amenities Messages') { ?> active<?php } ?>">
                        <a href="<?= site_url('admin/tmessages') ?>">
                           <i class="fa fa-wrench"></i>
                            Amenities Messages</a>
                    </li>
                    <li class=" <?php if (isset($subpageName) && $subpageName == 'Create Amenity Message') { ?> active<?php } ?>">
                        <a href="<?= site_url('admin/tmessages/create') ?>">
                           <i class="fa fa-plus-square"></i>
                           Add Amenity Message</a>
                    </li>
                     <li class=" <?php if (isset($subpageName) && $subpageName == 'tMaintenance Messages') { ?> active<?php } ?>">
                        <a href="<?= site_url('admin/reports') ?>">
                            <i class="fa fa-gears"></i>
                           tMaintenance Messages</a>
                    </li>
                     <li class=" <?php if (isset($subpageName) && $subpageName == 'All Messages') { ?> active<?php } ?>">
                        <a href="<?= site_url('admin/messages/') ?>">
                            <i class="fa fa-paper-plane"></i>
                            SMS Templates</a>
                    </li>
                </ul>
            </li>

            <li class="start <?php if (isset($pageName) && $pageName == 'Email Settings') { ?>active open<?php } ?>">
                <a href="javascript:;">
                    <i class="fa fa-envelope-square"></i><span class="title">Email Settings</span>
                    <span class=""></span><span class="arrow "></span>
                </a>
                <ul class="sub-menu" >
                    <li class=" <?php if (isset($subpageName) && $subpageName == 'Email Settings') { ?> active<?php } ?>">
                        <a href="<?= site_url('admin/email-settings') ?>">
                           <i class="fa fa-gears"></i>
                            Email Settings</a>
                    </li>
                    <li class=" <?php if (isset($subpageName) && ($subpageName == 'Email Templates' || $subpageName == 'Add Email Template' || $subpageName == 'Edit Email Template')) { ?> active<?php } ?>">
                        <a href="<?= site_url('admin/email-templates') ?>">
                            <i class="fa fa-file-image-o"></i>
                            Email Templates</a>
                    </li>
                    <li class=" <?php if (isset($subpageName) && $subpageName == 'All Sent Mails') { ?> active<?php } ?>">
                        <a href="<?= site_url('admin/promotional/emails') ?>">
                             <i class="fa fa-list"></i>
                            All Sent Mails</a>
                    </li>
                    <li class=" <?php if (isset($subpageName) && $subpageName == 'Send Mail') { ?> active<?php } ?>">
                        <a href="<?= site_url('admin/promotional/emails/send') ?>">
                             <i class="fa fa-send-o"></i>
                            Send Mail</a>
                    </li>
                </ul>
            </li>

            <li class="start <?php if (isset($pageName) && $pageName == 'Enquiry') { ?>active open<?php } ?>">
                <a href="javascript:;">
                    <i class="fa fa-rocket"></i>
                    <span class="title">Inquiry</span>
                    <span class=""></span>
                    <span class="arrow "></span>
                </a>
                <ul class="sub-menu" >
                    <li class=" <?php if (isset($subpageName) && ($subpageName == 'Enquiries' || $subpageName == 'Reply')) { ?> active<?php } ?>">
                        <a href="<?= site_url('admin/enquiries') ?>">
                             <i class="fa fa-list"></i>
                            Inquiries</a>
                    </li>
                </ul>
            </li>
            
             <li class="start <?php if (isset($pageName) && $pageName == 'Checklist') { ?>active open<?php } ?>">
                <a href="javascript:;">
                    <i class="fa fa-list"></i>
                    <span class="title">Checklist</span>
                    <span class=""></span>
                    <span class="arrow "></span>
                </a>
                <ul class="sub-menu" >

                    <li class=" <?php if (isset($subpageName) && $subpageName == 'All Tservices') { ?> active<?php } ?>">
                        <a href="<?= site_url('admin/checklist') ?>">
                            <i class="fa fa-list"></i>
                            All Checklist</a>
                    </li>
                    <li class=" <?php if (isset($subpageName) && $subpageName == 'Add Tervice') { ?> active<?php } ?>">
                        <a href="<?= site_url('admin/checklist/add') ?>">
                            <i class="fa fa-plus-circle"></i>
                            Add Checklist Item</a>
                    </li>
                </ul>
            </li>
              <li class="start <?php if (isset($pageName) && $pageName == 'Subscribers') { ?>active open<?php } ?>">
                  <a href="<?= site_url('admin/subscribers') ?>">
                    <i class="fa fa-group"></i>
                    <span class="title">Subscribers</span>
              
                </a>
               
            </li>
            <li class="start <?php if (isset($pageName) && $pageName == 'Questions') { ?>active open<?php } ?>">
                  <a href="<?= site_url('admin/common_question') ?>">
                    <i class="fa fa-group"></i>
                    <span class="title">Common Question<?php $pageName; ?></span>              
                </a>   
                <ul class="sub-menu" >
                    <li class=" <?php if (isset($subpageName) && $subpageName == 'Common Question') { ?> active<?php } ?>">
                        <a href="<?= site_url('admin/common_question') ?>">
                           <i class="fa fa-gears"></i>
                            Common Question</a>
                    </li>
                    <li class=" <?php if (isset($subpageName) && $subpageName == 'View Question Category') { ?> active<?php } ?>">
                        <a href="<?= site_url('admin/view_category') ?>">
                           <i class="fa fa-gears"></i>
                            Common Question Category</a>
                    </li>  
                </ul>          
            </li>
            <li class="start <?php if (isset($pageName) && ($pageName == 'Survey Question' || $pageName == 'Survey Answer')) { ?>active open<?php } ?>">
                  <a href="<?= site_url('admin/survey_question') ?>">
                    <i class="fa fa-group"></i>
                    <span class="title">Survey Question</span>              
                </a>   
                <ul class="sub-menu" >
                    <li class=" <?php if (isset($subpageName) && ($pageName == 'Survey Question' || $subpageName == 'Edit Survey Answer' || $subpageName == 'Survey Answer' || $pageName == 'Survey Answer')) { ?> active<?php } ?>">
                        <a href="<?= site_url('admin/survey_question') ?>">
                           <i class="fa fa-gears"></i>
                            Survey Question</a>
                    </li>
                </ul>          
            </li>

        </ul>
        <!-- END SIDEBAR MENU -->
    </div>
</div>
<!-- END SIDEBAR -->
