<script type="text/javascript">
    function makeSearchUrl()
    {
        var key = $('#filterKey').val();
        if (key == '')
            key = 'NULL';
        var show_me = $('#show_me').val();
        if (show_me == '')
            show_me = 'NULL';

        var sort_by = $('#sort_by').val();
        if (sort_by == '')
            sort_by = 'NULL';
        window.location = '<?= site_url($this->config->config['adminName'] . '/reports/filter') ?>/' + key + '/' + show_me + '/' + sort_by;
    }
    function resetsearch()
    {
        window.location = '<?= site_url($this->config->config['adminName'] . '/reports') ?>';
    }

    function changevalue($this, current)
    {
        $('#filterKey').val(current);
    }
</script>	
<div class="page-content-wrapper">
    <div class="page-content" style="min-height:1161px">

        <!-- BEGIN PAGE HEADER-->

        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <i class="fa fa-home"></i>
                    <a href="<?= site_url('admin') ?>">Home</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li><a href="<?php
                    if (isset($breadcrum1_url)) {
                        echo $breadcrum1_url;
                    } else {
                        ?>javascript:;<?php } ?>"><?= isset($breadcrum1) ? $breadcrum1 : ''; ?></a><?php if (isset($breadcrum2)) { ?><i class="fa fa-angle-right"></i><?php } ?></li>
                <li><a href="<?php
                    if (isset($breadcrum2_url)) {
                        echo $breadcrum2_url;
                    } else {
                        ?>javascript:;<?php } ?>"><?=
                           isset($breadcrum2) ? $breadcrum2 : '';
                           ;
                           ?></a><?php if (isset($breadcrum3)) { ?><i class="fa fa-angle-right"></i><?php } ?></li>
                <li><a href="<?php
                    if (isset($breadcrum3_url)) {
                        echo $breadcrum3_url;
                    } else {
                        ?>javascript:;<?php } ?>"><?=
                        isset($breadcrum3) ? $breadcrum3 : '';
                        ;
                        ?></a><?php if (isset($breadcrum4)) { ?><i class="fa fa-angle-right"></i><?php } ?></li>
                <li><a href="<?php
                    if (isset($breadcrum4_url)) {
                        echo $breadcrum4_url;
                    } else {
                        ?>javascript:;<?php } ?>"><?=
                        isset($breadcrum4) ? $breadcrum4 : '';
                        ;
                    ?></a><?php if (isset($breadcrum5)) { ?><i class="fa fa-angle-right"></i><?php } ?></li>
            </ul>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->



        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-globe"></i><?= $subpageName ?> (<?= sizeof($records) ?> records)
                        </div>
                        <div class="actions">
<!--                            <a class="btn btn-default btn-sm" href="<?//= $breadcrum3_url ?>">
                                <i class="fa fa-plus"></i> Add </a>-->
                            <div class="btn-group">

                                <a data-toggle="dropdown" href="javascript:;" class="btn btn-default btn-sm" aria-expanded="false">
                                    <i class="fa fa-cogs"></i> Tools <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <!--                                    <li>
                                                                            <a href="javascript:;" data-value="Active" id="multiple_active">
                                                                                <i class="fa fa-circle"></i> Active 
                                                                            </a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="javascript:;" data-value="Inactive" id="multiple_inactive">
                                                                                <i class="fa fa-circle-o"></i> Inactive 
                                                                            </a>
                                                                        </li>-->
                                    <li>
                                        <a href="javascript:;" data-value="delete" id="multiple_delete">
                                            <i class="fa fa-trash-o"></i> Delete Selected
                                        </a>
                                    </li>
                                </ul>

                            </div>
                        </div>

                    </div>
                    <div class="portlet-body">
                        <div class="row">
                                <?php if ($this->session->userdata('SUCCESS_MESSAGE')) { ?>
                                <div class="row">
                                    <div class="ajax_report alert alert-success" role="alert" style="margin-left:30px;width:1290px;"><span class="close"></span><?= $this->session->userdata('SUCCESS_MESSAGE') ?></div>
                                <?php $this->session->unset_userdata('SUCCESS_MESSAGE'); ?>
                                </div>	
                                <?php } ?>
                                <?php if ($this->session->userdata('ERROR_MESSAGE')) { ?>
                                <div class="row">
                                    <div class="ajax_report alert alert-danger" role="alert" style="margin-left:30px;width:1290px;"><span class="close"></span><?= $this->session->userdata('ERROR_MESSAGE') ?></div>
    <?php $this->session->unset_userdata('ERROR_MESSAGE'); ?>
                                </div>	
                                <?php } ?>
                            <div class="col-md-12 col-sm-12">
                                <div id="sample_1_filter" class="filterInput" style="display:<?php
                                if (isset($advanceSearch) && $advanceSearch == 'Yes') {
                                    echo "none;";
                                } else {
                                    echo "block;";
                                }
                                ?>">
                                    <form action="" method="post">
                                    <label>Search: <input type="search" name="search" id="filterKey" class="form-control input-small input-inline" placeholder="" aria-controls="sample_1" value=""></label>
                                    <button class="btn green" type="submit">Search</button>
                                    </form>
                                </div>
                            </div>
                       
                        </div>
<?php echo form_open('', array('class' => ' ', 'id' => 'multiple-operations')) ?>
                        <table class="table table-striped table-bordered table-hover" id="<?php if ($records) { ?>sample_1<?php } ?>">
                            <thead>
                                <tr role="row">
                                    <th class="table-checkbox no_record" rowspan="1" colspan="1" style="width: 24px;">
                            <div class="checker">
                                <span>
                                    <input type="checkbox" class="check_all">
                                </span>
                            </div>
                            </th>
                            <th>
                                Id
                            </th>
                            <th>Service</th>
                            <th>
                                Property
                            </th>
                            <th>
                                Added By
                            </th>
                            <th>
                                Landlord
                            </th>
                            <th class="no_record">
                                Message
                            </th>
                            <th class="no_record">
                                Added
                            </th>
                            <th class="no_record">
                                Action
                            </th>
                            </tr>
                            </thead>
                            <tbody>
<?php if ($records) { ?>	
    <?php foreach ($records as $value) { ?>
                                        <tr class="gradeX odd" role="row">
                                            <td>
                                                <div class="checker">
                                                    <span>
                                                        <input type="checkbox" value="<?= $value->report_id ?>" id="" class="checkboxes ids" name="checkIds[]">
                                                    </span>
                                                </div>
                                            </td>
                                            
                                            <td class="sorting_1">
                                                <?= $value->report_id ?>
                                            </td>	
                                             <td class="sorting_1">
                                                <?= $value->category ?>
                                            </td>
                                            <td class="sorting_1">
                                                <?php
                                                $address = '';
                                                if ($value->address1 != '') {
                                                    $address .=$value->address1;
                                                }
                                                if ($value->address2 != '') {
                                                    $address .=" " . $value->address2;
                                                }
                                                if ($value->unit_number != 0) {
                                                    $address .=" " . $value->unit_number;
                                                }
                                                if ($value->city != '') {
                                                    $address .=" " . $value->city . ",";
                                                }
                                                if ($value->region_name != '') {
                                                    $address .=" " . $value->region_name;
                                                }
                                                if ($value->zip != '') {
                                                    $address .=" " . $value->zip;
                                                }
                                                echo $address;
                                                ?>
                                            </td>
                                            <td class="sorting_1">
                                                    <?php echo $value->tfname . " ", $value->tlname; ?>
                                            </td>
                                            <td class="center">
                                               
                                                    <?php echo $value->lfname . " ", $value->llname; ?>
                                            
                                            </td>
                                            <td>
                                               <?= $this->common_model->showLimitedText($value->report, 50); ?>
                                            </td>
                                             <td class="sorting_1">
                                                      <?= date('m/d/y', strtotime($value->date_created)); ?>
                                            </td>
                                            <td>
                                                <a href="<?= site_url('admin/reports/view/' . $value->report_id) ?>" data-target="#large" data-toggle="modal" class="config btn btn-sm blue" data-title="view" ><i class="fa fa-eye"></i></a>
                                                <a href="<?= site_url('admin/reports/doTask/delete/' . $value->report_id) ?>" data-target="#general" data-toggle="modal" class="config btn btn-sm red" title="delete"><i class="fa fa-trash"></i></a>
                                            </td>
                                        </tr>
                                    <?php
                                    }
                                } else {
                                    ?>
                                    <tr class="gradeX odd" role="row"><td style="text-align:center;" colspan="10">No records found...</td></tr>
<?php } ?>	
                            </tbody>
                        </table>
                                    <?= form_close(); ?>
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="dataTables_paginate paging_bootstrap_full_number" id="sample_1_paginate">

<?= isset($paging) ? $paging : '' ?>


                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <!-- END PAGE CONTENT-->
            </div>


            <!-- END PAGE CONTENT-->
        </div>
    </div>
