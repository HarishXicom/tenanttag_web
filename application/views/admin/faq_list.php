<script type="text/javascript">
	function makeSearchUrl()
	{
		var key =$('#filterKey').val();
		if(key=='')
			key = 'NULL';
		var show_me	=	$('#show_me').val();
		if(show_me=='')
			show_me = 'NULL';
		
		var sort_by	=	$('#sort_by').val();
		if(sort_by=='')
			sort_by = 'NULL';	
		window.location='<?=site_url($this->config->config['adminName'].'/faq/filter')?>/'+key+'/'+show_me+'/'+sort_by;
	}
	function resetsearch()
	{
			window.location='<?=site_url($this->config->config['adminName'].'/faq')?>';
	}
	
	function changevalue($this,current)
	{
		$('#filterKey').val(current);
	}
</script>	
<div class="page-content-wrapper">
		<div class="page-content" style="min-height:1161px">
			
			<!-- BEGIN PAGE HEADER-->
			
			   <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                    <i class="fa fa-home"></i>
                    <a href="<?=site_url('admin')?>">Home</a>
                    <i class="fa fa-angle-right"></i>
            </li>
            <li><a href="<?php if(isset($breadcrum1_url)) { echo $breadcrum1_url;} else { ?>javascript:;<?php } ?>"><?=isset($breadcrum1)?$breadcrum1:'';?></a><?php if(isset($breadcrum2)) { ?><i class="fa fa-angle-right"></i><?php } ?></li>
            <li><a href="<?php if(isset($breadcrum2_url)) { echo $breadcrum2_url;} else { ?>javascript:;<?php } ?>"><?=isset($breadcrum2)?$breadcrum2:'';;?></a><?php if(isset($breadcrum3)) { ?><i class="fa fa-angle-right"></i><?php } ?></li>
            <li><a href="<?php if(isset($breadcrum3_url)) { echo $breadcrum3_url;} else { ?>javascript:;<?php } ?>"><?=isset($breadcrum3)?$breadcrum3:'';;?></a><?php if(isset($breadcrum4)) { ?><i class="fa fa-angle-right"></i><?php } ?></li>
            <li><a href="<?php if(isset($breadcrum4_url)) { echo $breadcrum4_url;} else { ?>javascript:;<?php } ?>"><?=isset($breadcrum4)?$breadcrum4:'';;?></a><?php if(isset($breadcrum5)) { ?><i class="fa fa-angle-right"></i><?php } ?></li>
        </ul>
    </div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
					<a href="<?=site_url($this->config->config['adminName'].'/faq/filter/NULL/NULL/NULL')?>" class="widget widget-hover-effect1">
					<div class="dashboard-stat blue">
						<div class="visual">
							<i class="fa fa-file-text"></i>
						</div>
						<div class="details">
							<div class="number">
								 <strong>Total</strong><br>
                                 <small><?=$total?></small>
							</div>
						</div>
					</div>
					</a>
				</div>
					
				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
					<a href="<?=site_url($this->config->config['adminName'].'/faq/filter/NULL/Active/NULL')?>" class="widget widget-hover-effect1">
					<div class="dashboard-stat green">
						<div class="visual">
							<i class="fa fa-check-square-o fa-1x"></i>
						</div>
						<div class="details">
							<div class="number">
								 <strong>Active</strong><br>
                                 <small><?=$totalActive?></small>
							</div>
						</div>
					</div>
					</a>
				</div>
				
				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
					<a href="<?=site_url($this->config->config['adminName'].'/faq/filter/NULL/Inactive/NULL')?>" class="widget widget-hover-effect1">
					<div class="dashboard-stat red">
						<div class="visual">
							<i class="fa fa-exclamation fa-1x"></i>
						</div>
						<div class="details">
							<div class="number">
								 <strong>Inactive</strong><br>
                                 <small><?=$totalInactive?></small>
							</div>
						</div>
					</div>
					</a>
				</div>
				
				
			</div>
			
			
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
					<div class="portlet box blue">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-globe"></i><?=$subpageName?> (<?=sizeof($records)?> records)
							</div>
								<div class="actions">
								<a class="btn btn-default btn-sm" href="<?=$breadcrum3_url?>">
								<i class="fa fa-plus"></i> Add </a>
								<div class="btn-group">

									<a data-toggle="dropdown" href="javascript:;" class="btn btn-default btn-sm" aria-expanded="false">
										<i class="fa fa-cogs"></i> Tools <i class="fa fa-angle-down"></i>
									</a>
									<ul class="dropdown-menu pull-right">
										<li>
											<a href="javascript:;" data-value="Active" id="multiple_active">
												<i class="fa fa-circle"></i> Active 
											</a>
										</li>
										<li>
											<a href="javascript:;" data-value="Inactive" id="multiple_inactive">
												<i class="fa fa-circle-o"></i> Inactive 
											</a>
										</li>
										<li>
											<a href="javascript:;" data-value="delete" id="multiple_delete">
												<i class="fa fa-trash-o"></i> Delete 
											</a>
										</li>
									</ul>

								</div>
							</div>
						
						</div>
						<div class="portlet-body">
							<div class="row">
									<?php if($this->session->userdata('SUCCESS_MESSAGE')) { ?>
									<div class="row">
										<div class="ajax_report alert alert-success" role="alert" style="margin-left:30px;width:1290px;"><span class="close"></span><?=$this->session->userdata('SUCCESS_MESSAGE')?></div>
										<?php $this->session->unset_userdata('SUCCESS_MESSAGE');?>
									</div>	
									<?php } ?>
									<?php if($this->session->userdata('ERROR_MESSAGE')) { ?>
									<div class="row">
										<div class="ajax_report alert alert-danger" role="alert" style="margin-left:30px;width:1290px;"><span class="close"></span><?=$this->session->userdata('ERROR_MESSAGE')?></div>
										<?php $this->session->unset_userdata('ERROR_MESSAGE');?>
									</div>	
									<?php } ?>
									<div class="col-md-12 col-sm-12">
                                                                            <div id="sample_1_filter" class="filterInput" style="display:<?php if(isset($advanceSearch) && $advanceSearch=='Yes') { echo "none;"; }else { echo "block;";} ?>">
                                                                                <label>Search: <input type="search" name="filterKey" id="filterKey" class="form-control input-small input-inline" placeholder="" aria-controls="sample_1" value="<?=isset($filterKey)?$filterKey:'';?>"></label>
												<button class="btn green" type="submit" onClick="makeSearchUrl();">Search</button>
												&nbsp;<button type="button" id="advanceSearch" class="btn btn-alt btn-primary enable-tooltip" title="Advance Search"><i class="fa fa-sun-o fa-lg"></i></button>
										</div>
									</div>
									<div id='adv_box' class="col-md-12"  style="display:<?php if(isset($advanceSearch) && $advanceSearch=='Yes') { echo "block;"; }else { echo "none;";} ?>">
											<div style="display:inline;">
											<div class="block">
											<div class="block-title">
												<div class="block-options pull-right form-actions">
													<button type="button" id="advanceSearch_close" class="btn btn-alt btn-sm btn-danger btn-circle enable-tooltip" title="Advance Search close"><i class="fa fa-times"></i></button>
												<button type="reset" onClick="resetsearch();" class="btn btn-alt btn-sm btn-warning btn-circle enable-tooltip" title="Reset Search"><i class="fa fa-repeat"></i></button>
													</div>
												<h4><strong>Advance </strong>Search</h4>
													
											</div>
                                
											<div class="form-inline">
												 <div class="form-group">
													<label for="example-if-email" class=""> Search Key</label>
													<input type="text" id="search_key_ad" name="search" value="<?=isset($filterKey)?$filterKey:'';?>" class="form-control" placeholder="Search by Name/Email..">
												 </div>
												
												<div class="form-group">
													<label for="example-if-email" class=""> Show Me</label>
												    <select id="show_me" name="example-chosen" class="select-chosen form-control" data-placeholder="Choose a Type.." style="width: 250px;">
														<option></option>
                                                                                                                <option <?php if(isset($show_me) && $show_me=='Active') { ?>selected<?php } ?> value="Active">Active</option>
														<option <?php if(isset($show_me) && $show_me=='Inactive') { ?>selected<?php } ?> value="Inactive">Inactive</option>
													</select> 
												</div>
												<div class="form-group">
													<label for="example-if-email" class=""> Sort By</label>
													<select id="sort_by" name="example-chosen" class="select-chosen form-control" data-placeholder="Choose a Type.." style="width: 250px;">
														<option></option>
														<option <?php if(isset($sort_by) && $sort_by=='New') { ?>selected<?php } ?> value="New">New</option>
														<option <?php if(isset($sort_by) && $sort_by=='Old') { ?>selected<?php } ?> value="Old">Old</option>
														<option <?php if(isset($sort_by) && $sort_by=='Asc') { ?>selected<?php } ?> value="Asc">Asc</option>
														<option <?php if(isset($sort_by) && $sort_by=='Desc') { ?>selected<?php } ?> value="Desc">Desc</option>
													</select> 
												</div>
												<button type="button" style="" onClick="javascript:makeSearchUrl();" class="btn btn-primary">Search</button>
											</div>
											</div>
											</div>
									</div>
								</div>
							<?php echo form_open('',array('class'=>' ','id'=>'multiple-operations'))?>
							<table class="table table-striped table-bordered table-hover" id="<?php if($records) { ?>sample_1<?php } ?>">
							<thead>
							<tr role="row">
								<th class="table-checkbox no_record" rowspan="1" colspan="1" style="width: 24px;">
									<div class="checker">
										<span>
										<input type="checkbox" class="check_all">
										</span>
									</div>
								</th>
								<th>
									 Id
								</th>
								<th class="no_record">
									 Question
								</th>
								<th class="no_record">
									 add date
								</th>
								<th class="no_record">
									 Status
								</th>
								<th class="no_record">
									 IP
								</th>
								<th class="no_record">
									 Action
								</th>
							</tr>
							</thead>
							<tbody>
							<?php if($records) { ?>	
							<?php foreach($records as $value) { ?>
							<tr class="gradeX odd" role="row">
								<td>
									<div class="checker">
										<span>
										<input type="checkbox" value="<?=$value->f_id?>" id="" class="checkboxes ids" name="checkIds[]">
										</span>
									</div>
								</td>
								<td>
									<span class="sorting_1">
									<?=$value->f_id?> </span>
								</td>
								<td class="sorting_1">
									 <?=$this->common_model->showLimitedText($value->question,50);?>
								</td>
								<td class="center">
									<span class="label label-sm label-danger">
									 <?=date('m/d/y',$value->add_date)?>
									 </span>
								</td>
								<td>
									<span class="label label-sm <?php if($value->status=='Active') { ?> label-success<?php } else { ?> label-warning<?php } ?>">
									<?=$value->status?> </span>
								</td>
								<td>
									<span class="sorting_1">
									<?=$value->ip?> </span>
								</td>
								<td>

									<a href="<?=site_url('admin/faq/view/'.$value->f_id)?>" data-target="#large" data-toggle="modal" class="config btn btn-sm blue" data-title="view" ><i class="fa fa-eye"></i></a>

									<a href="<?=site_url('admin/faq/edit/'.$value->f_id)?>" class="config btn blue btn-sm" title="Edit"><i class="fa fa-pencil"></i></a>
									<?php if($value->status=='Inactive') { ?>
									<a href="<?=site_url('admin/faq/doTask/Active/'.$value->f_id)?>" data-target="#general" data-toggle="modal" class="config btn btn-sm red" title="Active"><i class="fa fa-circle"></i></a>
									<?php } else { ?>
									<a href="<?=site_url('admin/faq/doTask/Inactive/'.$value->f_id)?>" data-target="#general" data-toggle="modal" class="config btn btn-sm blue" title="Inactive"><i class="fa fa-circle-o"></i></a>
									<?php } ?>
									<a href="<?=site_url('admin/faq/doTask/delete/'.$value->f_id)?>" data-target="#general" data-toggle="modal" class="config btn btn-sm red" title="delete"><i class="fa fa-trash"></i></a>
								</td>
							</tr>
							<?php } } else { ?>
							<tr class="gradeX odd" role="row"><td style="text-align:center;" colspan="10">No records found...</td></tr>
							<?php } ?>
							</tbody>
							</table>
							<?=form_close();?>
								<div class="row">
								<div class="col-md-12 col-sm-12">
									<div class="dataTables_paginate paging_bootstrap_full_number" id="sample_1_paginate">
										
											<?=isset($paging)?$paging:''?>

										
									</div>
								</div>
							</div>
						
						</div>
					</div>
				
			<!-- END PAGE CONTENT-->
		</div>
			</div>
