<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->

        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <i class="fa fa-home"></i>
                    <a href="<?= site_url('admin') ?>">Home</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li><a href="<?php if (isset($breadcrum1_url)) {
    echo $breadcrum1_url;
} else { ?>javascript:;<?php } ?>"><?= isset($breadcrum1) ? $breadcrum1 : ''; ?></a><?php if (isset($breadcrum2)) { ?><i class="fa fa-angle-right"></i><?php } ?></li>
                <li><a href="<?php if (isset($breadcrum2_url)) {
    echo $breadcrum2_url;
} else { ?>javascript:;<?php } ?>"><?= isset($breadcrum2) ? $breadcrum2 : '';
; ?></a><?php if (isset($breadcrum3)) { ?><i class="fa fa-angle-right"></i><?php } ?></li>
                <li><a href="<?php if (isset($breadcrum3_url)) {
    echo $breadcrum3_url;
} else { ?>javascript:;<?php } ?>"><?= isset($breadcrum3) ? $breadcrum3 : '';
; ?></a><?php if (isset($breadcrum4)) { ?><i class="fa fa-angle-right"></i><?php } ?></li>
                <li><a href="<?php if (isset($breadcrum4_url)) {
    echo $breadcrum4_url;
} else { ?>javascript:;<?php } ?>"><?= isset($breadcrum4) ? $breadcrum4 : '';
; ?></a><?php if (isset($breadcrum5)) { ?><i class="fa fa-angle-right"></i><?php } ?></li>
            </ul>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <div class="col-md-12 ">
                <!-- BEGIN SAMPLE FORM PORTLET-->
                <div class="portlet box blue ">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-gift"></i> <?= isset($subpageName) ? $subpageName : '' ?>
                        </div>
                        <div class="tools">

                        </div>
                    </div>
                    <div class="portlet-body form">
                        <?php echo form_open_multipart('', array('class' => 'ajaxForm form-horizontal', 'id' => 'common_question')) ?>
                        <div class="ajax_report alert display-hide" role="alert" style="margin-left:232px;width:956px;"><span class="close"></span><span class="ajax_message">Hello Message</span></div>
                        <div class="form-body">
                            
                            <div class="form-group">
                                <label class="col-md-2 control-label"> </label>
                                <div class="col-md-9">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">Question </label>
                                <div class="col-md-9"> <?php //print_R($edit_record); ?>
                                    <input type="text" id="question" value="<?php echo isset($edit_record->question) ? $edit_record->question : ''; ?>" style="width:100%;height:35px;" name="question">
                                    <p class="help-block">
                                        Select Common Question Category...
                                    </p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">Question </label>
                                <div class="col-md-9">
                                    <select name="category">
                                        <?php foreach ($question_category as $key => $value) { ?>
                                                    <?php if(isset($edit_record->category_id) && $edit_record->category_id == $value->id ){ ?>
                                                            <option selected value="<?php echo $value->id; ?>" > <?php echo $value->category_name; ?></option>
                                                    <?php } else{ ?>
                                                            <option value="<?php echo $value->id; ?>" > <?php echo $value->category_name; ?></option>
                                                    <?php } ?>
                                        <?php } ?>
                                    </select>
                                    <p class="help-block">
                                        Common Question...
                                    </p>
                                </div>
                            </div>


                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn green">Submit</button>
                                </div>
                            </div>
                        </div>
<?= form_close(); ?>
                    </div>
                </div>
                <!-- END SAMPLE FORM PORTLET-->


                 <table class="table table-striped table-bordered table-hover" id="<?php if ($records) { ?>sample_1<?php } ?>">
                            <thead>
                                <tr role="row">
                            <th>
                                Id
                            </th>
                            <th class="no_record">
                                Question
                            </th>
                            <th class="no_record">
                                Category
                            </th>
                            <th class="no_record">
                                Created date 
                            </th>
                            <th class="no_record">
                                Action
                            </th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php if ($records) { ?>    
                                    <?php foreach ($records as $key => $value) { ?>
                                        <tr class="gradeX odd" role="row">
                                            <td class="sorting_1">
                                                <?= $value['id'] ?>
                                            </td>
                                            <td class="sorting_1">
                                               <?php echo $value['question']; ?>
                                            </td>
                                            <td class="sorting_1">
                                               <?php echo $value['category_name']; ?>
                                            </td>
                                            <td class="sorting_1">
                                                    <?= $value['created_date']; ?>
                                            </td>
                                            <td>                                                
                                                <a href="<?= site_url('admin/common_question/edit_question/' . $value['id']) ?>" class="config btn btn-sm blue" title="Edit"><i class="fa fa-pencil"></i></a>
                                                <a href="<?= site_url('admin/common_question/view_answer/' . $value['id']) ?>" title="View Anwser" class="config btn btn-sm blue" ><i class="fa fa-eye"></i></a>
                                                <a href="<?= site_url('admin/common_question/delete_question/' . $value['id']) ?>" class="config btn btn-sm red" title="delete"><i class="fa fa-trash"></i></a>
                                            </td>
                                        </tr>
                                        <?php }
                                    } else { ?>
                                        <tr class="gradeX odd" role="row"><td style="text-align:center;" colspan="3">No records found...</td></tr>
                                    <?php } ?>  
                            </tbody>
                        </table>

            </div>
            <!-- END PAGE CONTENT-->

        </div>
    </div>
</div>

