<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title"> Detail</h4>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-12">

            <table class="table table-striped table-bordered table-hover" id="sample_1">
                <thead>
                    <tr>
                        <th>Field Name</th>
                        <th>Data</th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="odd gradeX">
                        <td>ID </td>
                        <td><?= $records->id ?></td>
                    </tr>
                    <tr class="odd gradeX">
                        <td>User</td>
                        <td class="sorting_1">
                           <?php echo $records->lfname . " ", $records->llname; ?>
                        </td>
                    </tr>
                    <tr class="odd gradeX">
                        <td>Type</td>
                        <td class="sorting_1">
                            <?= $records->user_type; ?>
                        </td>
                    </tr>
                    <tr class="odd gradeX">
                        <td>Mobile</td>
                        <td class="center">

                            <?php echo $records->number; ?>

                        </td>
                    </tr>
                  
                    <tr>
                        <td>Add Date</td>
                        <td class="sorting_1">
                            <?= date('m/d/y', strtotime($records->date_created)); ?>
                        </td>
                    </tr>
                </tbody>
            </table>
            <h4>Message</h4>
            <div class="note note-info">
               
                <p>
                   <?= $records->message; ?>
                </p>
            </div>
        </div>
    </div>
</div>

<div class="modal-footer">
    <button type="button" class="btn default" data-dismiss="modal">Close</button>
</div>
