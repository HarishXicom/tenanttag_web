<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title"> Detail</h4>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-12">

            <table class="table table-striped table-bordered table-hover" id="sample_1">
                <thead>
                    <tr>
                        <th>Field Name</th>
                        <th>Data</th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="odd gradeX">
                        <td>ID </td>
                        <td><?= $records->id ?></td>
                    </tr>
                    <tr class="odd gradeX">
                        <td>User</td>
                        <td class="sorting_1">
                           <?php echo $records->lfname . " ", $records->llname; ?>
                        </td>
                    </tr>
                    <tr class="odd gradeX">
                        <td>Type</td>
                        <td class="sorting_1">
                            <?= $records->user_type; ?>
                        </td>
                    </tr>
                    <tr class="odd gradeX">
                        <td>Mobile</td>
                        <td class="center">

                            <?php echo $records->number; ?>

                        </td>
                    </tr>
                 
                    <tr>
                        <td>Scheduled</td>
                        <td class="sorting_1">
                            <?= date('m/d/y', strtotime($records->date_created)); ?>
                        </td>
                    </tr>
                    
                    
                </tbody>
            </table>
             <h4>Message</h4>
            <div class="note note-info">
                 
                <p>
                   <?= $records->message; ?>
                </p>
                 <ul style="margin: 0px; padding: 0px 0px 0px 10px;">
                        <li>Repeat Type: 
                           <?php if($records->repeat_type=='week'){ echo 'Weekly';}elseif($records->repeat_type=='month'){ echo 'Monthly';}else{echo 'Yearly';}; ?>
                        </li>
                   
                        <li>Repeat Every: 
                            <?php if($records->repeat_type=='week'){ echo $records->repeat_every." week(s)";}elseif($records->repeat_type=='month'){ echo ordinal($records->repeat_every).' of every month';}else{echo 'Year';}; ?>
                            
                        </li>
                    <?php if($records->repeat_type=='week'){ ?>
                        <li>Repeat On: 
                            <?= $records->repeat_on; ?>
                        </li>
                    <?php }?>
                        <?php if($records->occurences!=0){ ?>
                        <li>Occurences: 
                            <?= $records->occurences." time(s)"; ?>
                        </li>
                        <?php } ?>
                    </ul>
            </div>
        </div>
    </div>
</div>

<div class="modal-footer">
    <button type="button" class="btn default" data-dismiss="modal">Close</button>
</div>
