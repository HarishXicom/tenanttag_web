<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->

        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <i class="fa fa-home"></i>
                    <a href="<?= site_url('admin') ?>">Home</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li><a href="<?php if (isset($breadcrum1_url)) {
                        echo $breadcrum1_url;
                    } else { ?>javascript:;<?php } ?>"><?= isset($breadcrum1) ? $breadcrum1 : ''; ?></a><?php if (isset($breadcrum2)) { ?><i class="fa fa-angle-right"></i><?php } ?></li>
                                    <li><a href="<?php if (isset($breadcrum2_url)) {
                        echo $breadcrum2_url;
                    } else { ?>javascript:;<?php } ?>"><?= isset($breadcrum2) ? $breadcrum2 : '';
                    ; ?></a><?php if (isset($breadcrum3)) { ?><i class="fa fa-angle-right"></i><?php } ?></li>
                                    <li><a href="<?php if (isset($breadcrum3_url)) {
                        echo $breadcrum3_url;
                    } else { ?>javascript:;<?php } ?>"><?= isset($breadcrum3) ? $breadcrum3 : '';
                    ; ?></a><?php if (isset($breadcrum4)) { ?><i class="fa fa-angle-right"></i><?php } ?></li>
                                    <li><a href="<?php if (isset($breadcrum4_url)) {
                        echo $breadcrum4_url;
                    } else { ?>javascript:;<?php } ?>"><?= isset($breadcrum4) ? $breadcrum4 : '';
                    ; ?></a><?php if (isset($breadcrum5)) { ?><i class="fa fa-angle-right"></i><?php } ?></li>
            </ul>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <div class="col-md-12 ">
                <!-- BEGIN SAMPLE FORM PORTLET-->
                <div class="portlet box blue ">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-gift"></i> <?= isset($subpageName) ? $subpageName : '' ?>
                        </div>
                        <div class="tools">

                        </div>
                    </div>
                    <div class="portlet-body form">
                        <?php echo form_open_multipart('', array('class' => 'ajaxForm form-horizontal', 'id' => 'survey_question')) ?>
                        <div class="ajax_report alert display-hide" role="alert" style="margin-left:232px;width:956px;"><span class="close"></span><span class="ajax_message">Hello Message</span></div>
                        <div class="form-body">
                            
                            <div class="form-group">
                                <label class="col-md-2 control-label"> </label>
                                <div class="col-md-9">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">Question </label>
                                <div class="col-md-9"> <?php //print_R($edit_record); ?>
                                    <input type="text" id="question" value="<?php echo isset($edit_record->question) ? $edit_record->question : ''; ?>" style="width:100%;height:35px;" name="question">
                                    <p class="help-block">
                                        Add Survey Question ...
                                    </p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">Answer Type </label>
                                <div class="col-md-9"> <?php //print_R($edit_record); ?>
                                    <label style="width:25%">
                                        <input type="radio" class="answer_type" value="1" <?php echo !isset($edit_record->answer_type) ? "checked" : ''; ?> <?php echo (isset($edit_record->answer_type) && $edit_record->answer_type == 1) ? "checked" : ''; ?> name="answer_type">
                                        Yes/No
                                    </label>
                                    <label style="width:72%">
                                        <input type="radio" class="answer_type" value="2" <?php echo (isset($edit_record->answer_type) && $edit_record->answer_type == 2) ? "checked" : ''; ?> name="answer_type">
                                        Multiple Choice
                                    </label>
                                    <p class="help-block">
                                    <br>
                                        Choose answer Type
                                    </p>
                                </div>
                            </div>
                            <div class="multiple_answer_conainer" style="display:<?php echo (isset($edit_record->answer_type) && $edit_record->answer_type == 2) ? 'block' : 'none'; ?>">
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Answer 1 </label>
                                    <div class="col-md-9"> <?php //print_R($edit_record); ?>
                                        <input type="text" id="answer1" value="<?php echo isset($edit_record->answer1) ? $edit_record->answer1 : ''; ?>" style="width:100%;height:35px;" name="answer1">
                                        <p class="help-block">
                                            Answer 1
                                        </p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Answer 2 </label>
                                    <div class="col-md-9"> <?php //print_R($edit_record); ?>
                                        <input type="text" id="answer2" value="<?php echo isset($edit_record->answer2) ? $edit_record->answer2 : ''; ?>" style="width:100%;height:35px;" name="answer2">
                                        <p class="help-block">
                                            Answer 2
                                        </p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Answer 3 </label>
                                    <div class="col-md-9"> <?php //print_R($edit_record); ?>
                                        <input type="text" id="answer3" value="<?php echo isset($edit_record->answer3) ? $edit_record->answer3 : ''; ?>" style="width:100%;height:35px;" name="answer3">
                                        <p class="help-block">
                                            Answer 3
                                        </p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Answer 4 </label>
                                    <div class="col-md-9"> <?php //print_R($edit_record); ?>
                                        <input type="text" id="answe4" value="<?php echo isset($edit_record->answer4) ? $edit_record->answer4 : ''; ?>" style="width:100%;height:35px;" name="answer4">
                                        <p class="help-block">
                                            Answer 4
                                        </p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Answer 5 </label>
                                    <div class="col-md-9"> <?php //print_R($edit_record); ?>
                                        <input type="text" id="answer5" value="<?php echo isset($edit_record->answer5) ? $edit_record->answer5 : ''; ?>" style="width:100%;height:35px;" name="answer5">
                                        <p class="help-block">
                                            Answer 5
                                        </p>
                                    </div>
                                </div>
                            </div>


                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn green">Submit</button>
                                </div>
                            </div>
                        </div>
<?= form_close(); ?>
                    </div>
                </div>
                <!-- END SAMPLE FORM PORTLET-->


                 <table class="table table-striped table-bordered table-hover" id="<?php if ($records) { ?>sample_1<?php } ?>">
                            <thead>
                                <tr role="row">
                                <th style="display:none"></th>
                            <th class="no_record">
                                Id
                            </th>
                            <th class="no_record" style="width:10%">
                                Question
                            </th>
                            <th class="no_record">
                                Answer Type
                            </th>
                            <th class="no_record">
                                Answer 1
                            </th>
                            <th class="no_record">
                                Answer 2
                            </th>
                            <th class="no_record">
                                Answer 3
                            </th>
                            <th class="no_record">
                                Answer 4
                            </th>
                            <th class="no_record">
                                Answer 5
                            </th>
                            <th class="no_record">
                                Created date 
                            </th>
                            <th class="no_record" style="width:15%">
                                Action
                            </th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php if ($records) { ?>    
                                    <?php foreach ($records as $key => $value) { ?>
                                    <?php

                                    ?>
                                        <tr class="sorting_1">
                                            <td  style="display:none"></td>
                                            <td class="sorting_1">
                                                <?= $value['id'] ?>
                                            </td>
                                            <td class="sorting_1">
                                               <?php echo $value['question']; ?>
                                            </td>
                                            <td class="sorting_1">
                                               <?php echo ($value['answer_type'] == 1) ? '<span class="label label-sm  label-success">Single</span>' : '<span class="label label-sm  label-warning">Multiple</span>'; ?>
                                            </td>
                                            <td class="sorting_1">
                                               <?php echo $value['answer1']; ?>
                                            </td>
                                            <td class="sorting_1">
                                               <?php echo $value['answer2']; ?>
                                            </td>
                                            <td class="sorting_1">
                                               <?php echo $value['answer3']; ?>
                                            </td>
                                            <td class="sorting_1">
                                               <?php echo $value['answer4']; ?>
                                            </td>
                                            <td class="sorting_1">
                                               <?php echo $value['answer5']; ?>
                                            </td>
                                            <td class="sorting_1">
                                                    <?= $value['created_date']; ?>
                                            </td>
                                            <td>                                                
                                                <a href="<?= site_url('admin/edit_survey_question/' . $value['id']) ?>" class="config btn btn-sm blue" title="Edit"><i class="fa fa-pencil"></i></a>
                                                <a href="<?= site_url('admin/survey_question_answer/' . $value['id']) ?>" class="config btn btn-sm blue" title="View answers"><i class="fa fa-eye"></i></a>
                                                <a href="<?= site_url('admin/survey_question/delete_survey_question/' . $value['id']) ?>" onclick="return confirm('Do you really want to delete this question?')" class="config btn btn-sm red" title="delete"><i class="fa fa-trash"></i></a>
                                            </td>
                                        </tr>
                                        <?php }
                                    } else { ?>
                                        <tr class="gradeX odd" role="row"><td style="text-align:center;" colspan="3">No records found...</td></tr>
                                    <?php } ?>  
                            </tbody>
                        </table>

            </div>
            <!-- END PAGE CONTENT-->

        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $(".answer_type").change(function(){
            if($(this).val() == 1){
                $(".multiple_answer_conainer").css("display","none");
            }else{
                $(".multiple_answer_conainer").css("display","block");
            }
            //alert($(this).val())
        });
    });
</script>

