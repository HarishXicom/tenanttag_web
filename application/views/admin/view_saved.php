<?php
$amenty = array(
    'heat' => 'Heat', 'ac' => 'AC',
    'fireexten' => 'Fire Extinguishers',
    'smoke' => 'Smoke Detectors', 'yard' => 'Yard Service'
);
?>
<?php
if (!empty($amenties)) {
    foreach ($amenties as $amen) {
        $amenty[$amen->id] = $amen->name;
    }
}
?>
<?php
$yearArray = array(
    "1" => "Jan",
    "2" => "Feb",
    "3" => "Mar",
    "4" => "Apr",
    "5" => "May",
    "6" => "Jun",
    "7" => "Jul",
    "8" => "Aug",
    "9" => "Sep",
    "10" => "Oct",
    "11" => "Nov",
    "12" => "Dec",
        )
?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title"> Detail</h4>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
<?php //echo '<pre>';print_r($records);die; ?>
          
            <h4>Amenity</h4>
            <div class="note note-info">
               
                <p>
                <?php echo $amenty[$records->amenity]; ?>
                </p>
            </div>
             <h4>Message</h4>
            <div class="note note-info">
               
                <p>
                   <?= $records->message; ?>
                </p>
            </div>
               <h4>Scheduled</h4>
            <div class="note note-info">
               
                <p>
                   <?php
                            $dts = $records->dates;
                            $str = '';
                            if (!empty($dts)) {
                                foreach ($dts as $dt) {
                                    $str .=ordinal($dt->day) . " " . $yearArray[$dt->month] . ", ";
                                }
                            }
                            echo $str;
                            ?>
                </p>
            </div>
        </div>
    </div>
</div>

<div class="modal-footer">
    <button type="button" class="btn default" data-dismiss="modal">Close</button>
</div>
