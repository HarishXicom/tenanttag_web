<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
<i class="icon-bell"></i>
<span class="badge badge-default">
<?=sizeof($notifications)?> </span>
</a>
<ul class="dropdown-menu">
	<li class="external">
		<h3><span class="bold"><?=sizeof($notifications)?></span> notifications</h3>
<!--
		<a href="extra_profile.html">view all</a>
-->
	</li>
	<li>
		<ul class="dropdown-menu-list scroller" style="height: 250px;" data-handle-color="#637283">
			<?php foreach($notifications as $value) { ?>
			<li>
				<a href="javascript:;">
				<span class="time"><?=$this->common_model->Myago($value->add_date)?></span>
				<span class="details">
				<span class="label label-sm label-icon label-success">
				<i class="fa fa-plus"></i>
				</span>
				<?=$value->notification?> </span>
				</a>
			</li>
			<?php } ?>
		</ul>
	</li>
</ul>
