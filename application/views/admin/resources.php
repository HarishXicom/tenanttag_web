<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->

        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <i class="fa fa-home"></i>
                    <a href="<?= site_url('admin') ?>">Home</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li><a href="<?php if (isset($breadcrum1_url)) {
    echo $breadcrum1_url;
} else { ?>javascript:;<?php } ?>"><?= isset($breadcrum1) ? $breadcrum1 : ''; ?></a><?php if (isset($breadcrum2)) { ?><i class="fa fa-angle-right"></i><?php } ?></li>
                <li><a href="<?php if (isset($breadcrum2_url)) {
    echo $breadcrum2_url;
} else { ?>javascript:;<?php } ?>"><?= isset($breadcrum2) ? $breadcrum2 : '';
; ?></a><?php if (isset($breadcrum3)) { ?><i class="fa fa-angle-right"></i><?php } ?></li>
                <li><a href="<?php if (isset($breadcrum3_url)) {
    echo $breadcrum3_url;
} else { ?>javascript:;<?php } ?>"><?= isset($breadcrum3) ? $breadcrum3 : '';
; ?></a><?php if (isset($breadcrum4)) { ?><i class="fa fa-angle-right"></i><?php } ?></li>
                <li><a href="<?php if (isset($breadcrum4_url)) {
    echo $breadcrum4_url;
} else { ?>javascript:;<?php } ?>"><?= isset($breadcrum4) ? $breadcrum4 : '';
; ?></a><?php if (isset($breadcrum5)) { ?><i class="fa fa-angle-right"></i><?php } ?></li>
            </ul>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <div class="col-md-12 ">
                <!-- BEGIN SAMPLE FORM PORTLET-->
                <div class="portlet box blue ">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-gift"></i> <?= isset($subpageName) ? $subpageName : '' ?>
                        </div>
                        <div class="tools">

                        </div>
                    </div>
                    <div class="portlet-body form">
                        <?php echo form_open_multipart('', array('class' => 'ajaxForm form-horizontal', 'id' => 'resources')) ?>
                        <div class="ajax_report alert display-hide" role="alert" style="margin-left:232px;width:956px;"><span class="close"></span><span class="ajax_message">Hello Message</span></div>
                        <div class="form-body">
                            
                            <div class="form-group">
                                <label class="col-md-2 control-label"> </label>
                                <div class="col-md-9">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">Document Name </label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" id="doc_name" name="doc_name">
                                    <p class="help-block">
                                        Document name
                                    </p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">Document </label>
                                <div class="col-md-9">
                                    <input type="file" id="file" name="file">
                                    <p class="help-block">
                                        Select resource document for site...
                                    </p>
                                </div>
                            </div>


                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn green">Submit</button>
                                </div>
                            </div>
                        </div>
                        <?= form_close(); ?>
                    </div>
                </div>
                <!-- END SAMPLE FORM PORTLET-->


                 <table class="table table-striped table-bordered table-hover" id="<?php if ($records) { ?>sample_1<?php } ?>">
                            <thead>
                                <tr role="row">
                            <th>
                                Id
                            </th>
                            <th class="no_record">
                                Document
                            </th>
                            <th class="no_record">
                                Created date
                            </th>
                            <th class="no_record">
                                Action
                            </th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php if ($records) { ?>    
                                    <?php foreach ($records as $key => $value) { ?>
                                        <tr class="gradeX odd" role="row">
                                            <td class="sorting_1">
                                                <?= $value['id'] ?>
                                            </td>
                                            <td class="sorting_1">
                                              <a href="<?= $this->config->item('uploads') ?>resources/<?= $value['resources_docs'] ?>"> <?php echo $value['doc_name'] ?></a>
                                            </td>
                                            <td class="sorting_1">
                                                    <?= $this->common_model->showLimitedText($value['created_date'], 50); ?>
                                            </td>
                                            <td>                                                
                                                <a href="<?= site_url('admin/web_config/delete_resources/' . $value['id']) ?>" title="delete"><i class="fa fa-trash"></i></a>
                                            </td>
                                        </tr>
                                        <?php }
                                    } else { ?>
                                        <tr class="gradeX odd" role="row"><td style="text-align:center;" colspan="3">No records found...</td></tr>
                                    <?php } ?>  
                            </tbody>
                        </table>

            </div>
            <!-- END PAGE CONTENT-->

        </div>
    </div>
</div>

