
<link href="<?= $this->config->item('templateassets') ?>css/theme.blue.css" rel="stylesheet">
<script type="text/javascript" src="<?= $this->config->item('templateassets') ?>js/jquery.validate.min.js"></script>   
<script type="text/javascript" src="<?= $this->config->item('templateassets') ?>js/jquery.tablesorter.js"></script>   
<script type="text/javascript" src="<?= $this->config->item('templateassets') ?>js/jquery.tablesorter.widgets.js"></script>   

<script id="js">// *** Filter search type function arguments ***
// data.filter = filter input value for a column;
// data.iFilter = same as filter, except lowercase (if wo.filter_ignoreCase is true)
// data.exact = table cell text (or parsed data, if  save_sch_smscolumn parser enabled)
// data.iExact = same as exact, except lowercase (if wo.filter_ignoreCase is true)

// search for a match from the beginning of a string
// "^l" matches "lion" but not "koala"
    $.tablesorter.filter.types.start = function (config, data) {
        if (/^\^/.test(data.iFilter)) {
            return data.iExact.indexOf(data.iFilter.substring(1)) === 0;
        }
        return null;
    };

// search for a match at the end of a string
// "a$" matches "Llama" but not "aardvark"
    $.tablesorter.filter.types.end = function (config, data) {
        if (/\$$/.test(data.iFilter)) {
            var filter = data.iFilter,
                    filterLength = filter.length - 1,
                    removedSymbol = filter.substring(0, filterLength),
                    exactLength = data.iExact.length;
            return data.iExact.lastIndexOf(removedSymbol) + filterLength === exactLength;
        }
        return null;
    };

    $(function () {
        //   $("#filters").data("sorter", false);
        $('#filters')
                .on('filterEnd filterReset', function (e, table) {
                    var c = this.config,
                            fr = c.filteredRows;
                    if (fr === 0) {
                        c.$table.append([
                            '<tr class="noData remove-me" role="alert" aria-live="assertive">',
                            '<td colspan="' + c.columns + '">No Data Found</td>',
                            '</tr>'
                        ].join(''));
                    } else {
                        c.$table.find('.noData').remove();
                    }
                })
                .tablesorter({
                    theme: 'blue',
                    widthFixed: false,
                    widgets: ["zebra", "filter"],
                    widgetOptions: {
                        filter_reset: '.reset'
                    }
                });
        $('#filters2')
                .on('filterEnd filterReset', function (e, table) {
                    var c = this.config,
                            fr = c.filteredRows;
                    if (fr === 0) {
                        c.$table.append([
                            '<tr class="noData remove-me" role="alert" aria-live="assertive">',
                            '<td colspan="' + c.columns + '">No Data Found</td>',
                            '</tr>'
                        ].join(''));
                    } else {
                        c.$table.find('.noData').remove();
                    }
                })
                .tablesorter({
                    theme: 'blue',
                    widthFixed: false,
                    widgets: ["zebra", "filter"],
                    widgetOptions: {
                        filter_reset: '.reset'
                    }
                });

        $("input[name='sch_msg']").click(function () {
            var value = $(this).val();
            if (value == 'Yes') {
                $('.show_for_sch').show();
                $(".snd_sms").html("<i class='fa fa-floppy-o'></i> Save");
            }
            if (value == 'No') {
                $('.show_for_sch').hide();
                $(".snd_sms").html("<i class='fa fa-paper-plane'></i> Send");
            }
        });
    });

</script>	
<script>
    pageckeditor = true;

    function goBack()
    {
        var redirect_url = '<?= isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : FALSE ?>';
        if (redirect_url)
            window.location = redirect_url;
        else
            window.location = '<?= site_url($this->config->config['adminName'] . '/messages') ?>';
    }

</script>
<style type="text/css">
    .form-horizontal .form-group {
        margin-left: 0px;
        margin-right: 0px;
    }
    .my-table tbody tr td {  display: inline-block;  width: 100%;}
    .my-table tbody tr {  display: inline-block;  width: 100%;}

    .portlet.box.blue.my-panel {        padding: 0;        border: 0px;}
    .portlet.box.blue.my-panel .portlet-body  {        padding: 0;        border: 0px;        border-bottom: 1px solid #ccc}
    .portlet.box.blue.my-panel .portlet-body table  {      margin: 0;}
    .portlet.box.blue.my-panel .portlet-body table  td{}
    .portlet {border-bottom: 1px solid #ccc; border: 0px;}
    div.checker {
        margin-left: 3px;
        margin-right: 0;
        width: 30px;
    }
    .col-md-2.control-label {
        padding: 0;
        text-align: left;
        width: 150px;
        padding: 5px 0;
    }
    .input-group.date.date-picker2 {
        background-image: none;
        padding: 0;
        width: 100%;
    }
    .tablesorter-blue input.tablesorter-filter, .tablesorter-blue select.tablesorter-filter {
        background-position: 15px -16px !important;
        height: 36px !important;
        padding: 6px 0 6px 40px !important;
        font-family: open sans;
        font-weight: lighter;
    }
</style>
<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->

        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <i class="fa fa-home"></i>
                    <a href="<?= site_url('admin') ?>">Home</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li><a href="<?php
                    if (isset($breadcrum1_url)) {
                        echo $breadcrum1_url;
                    } else {
                        ?>javascript:;<?php } ?>"><?= isset($breadcrum1) ? $breadcrum1 : ''; ?></a><?php if (isset($breadcrum2)) { ?><i class="fa fa-angle-right"></i><?php } ?></li>
                <li><a href="<?php
                    if (isset($breadcrum2_url)) {
                        echo $breadcrum2_url;
                    } else {
                        ?>javascript:;<?php } ?>"><?=
                           isset($breadcrum2) ? $breadcrum2 : '';
                           ;
                           ?></a><?php if (isset($breadcrum3)) { ?><i class="fa fa-angle-right"></i><?php } ?></li>
                <li><a href="<?php
                    if (isset($breadcrum3_url)) {
                        echo $breadcrum3_url;
                    } else {
                        ?>javascript:;<?php } ?>"><?=
                           isset($breadcrum3) ? $breadcrum3 : '';
                           ;
                           ?></a><?php if (isset($breadcrum4)) { ?><i class="fa fa-angle-right"></i><?php } ?></li>
                <li><a href="<?php
                    if (isset($breadcrum4_url)) {
                        echo $breadcrum4_url;
                    } else {
                        ?>javascript:;<?php } ?>"><?=
                           isset($breadcrum4) ? $breadcrum4 : '';
                           ;
                           ?></a><?php if (isset($breadcrum5)) { ?><i class="fa fa-angle-right"></i><?php } ?></li>
            </ul>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <div class="col-md-12 ">
                <!-- BEGIN SAMPLE FORM PORTLET-->
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-gift"></i>Schedule Message
                        </div>

                    </div>
                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <?php echo form_open_multipart('', array('class' => 'ajaxForm form-horizontal', 'id' => '')) ?>
                        <div class="ajax_report alert display-hide" role="alert" style="margin-left:232px;width:956px;"><span class="close"></span><span class="ajax_message">Hello Message</span></div>

                        <div class="form-body">


                            <!--<h3 class="form-section">Select Users</h3>-->
                            <div class="row">
                                <div class="col-md-6 col-sm-12">
                                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                    <div class="portlet box blue my-panel">
                                        <!-- <div class="portlet-title">
                                            <div class="caption">
                                                <i class="fa fa-user"></i>Landlords
                                            </div>

                                        </div> -->
                                        <div class="portlet-body ">
                                            <table class="table table-striped table-bordered table-hover my-table" id="filters">
                                                <thead>
                                                    <tr>

                                                        <th colspan='2' class="sorter-false" style="padding:10px">
                                                            Landlords
                                                        </th>

                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php if (!empty($landlords)) { ?>
                                                        <tr class="odd gradeX">
                                                            <td>
                                                                <input type="checkbox" class="sel_all" value=""/>


                                                                <b>Select All Landlords</b>
                                                            </td>
                                                        </tr>   
                                                        <?php foreach ($landlords as $landlord) { ?>
                                                            <tr class="odd gradeX">
                                                                <td>
                                                                    <input type="checkbox" class="checkboxes" name="users[]" value="<?php echo $landlord->mem_id ?>"/>

                                                                    <?php echo $landlord->first_name . " " . $landlord->last_name; ?>
                                                                </td>
                                                            </tr>     
                                                        <?php } ?>
                                                    <?php } else { ?>
                                                        <tr>
                                                            <td >No landlord found</td>
                                                        </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <!-- END EXAMPLE TABLE PORTLET-->
                                </div>

                                <div class="col-md-6 col-sm-12">
                                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                    <div class="portlet box blue my-panel">
                                        <!--  <div class="portlet-title">
                                             <div class="caption">
                                                 <i class="fa fa-user"></i>Tenants
                                             </div>

                                         </div> -->
                                        <div class="portlet-body ">
                                            <table class="table table-striped table-bordered table-hover my-table" id="filters2">
                                                <thead>
                                                    <tr>

                                                        <th colspan='2' class="sorter-false" style="padding:10px">
                                                            Tenants
                                                        </th>

                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php if (!empty($tenants)) { ?>
                                                        <tr class="odd gradeX">
                                                            <td>
                                                                <input type="checkbox" class="sel_all_tent" value=""/>

                                                                <b>Select All Tenants</b>
                                                            </td>
                                                        </tr>    
                                                        <?php foreach ($tenants as $tenant) { ?>
                                                            <tr class="odd gradeX">
                                                                <td>
                                                                    <input type="checkbox" class="tcheckboxes" name="users[]" value="<?php echo $tenant->mem_id ?>"/>

                                                                    <?php echo $tenant->first_name . " " . $tenant->last_name; ?>
                                                                </td>
                                                            </tr>     
                                                        <?php } ?>
                                                    <?php } else { ?>
                                                        <tr>
                                                            <td >No landlord found</td>
                                                        </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <!-- END EXAMPLE TABLE PORTLET-->
                                </div>


                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <textarea name="custom_message" class="form-control wrte_msg" placeholder="Write your message here..."></textarea>
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="my-label">Schedule Message</label>
                                        <div class="sched">
                                            <label>
                                                <input type="radio" name="sch_msg" class='sch_msg' id="optionsRadios4" value="Yes" > Yes </label>
                                            <label>
                                                <input type="radio" name="sch_msg" class='sch_msg' id="optionsRadios5" value="No" checked> No </label>

                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="row show_for_sch">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Start Date</label>
                                        <div class="col-md-2">
                                            <div class="input-group date date-picker2" data-date-format="dd-mm-yyyy">
                                                <input type="text" class="form-control" readonly name="sch_date" value="">
                                                <span class="input-group-btn">
                                                    <button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
                                                </span>
                                            </div>
                                            <!-- /input-group -->

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row show_for_sch">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Repeat:</label>
                                        <div class="col-md-2">
                                            <select  id="country" name="repeat_msg" class="form-control repeat_msg">
                                                <option value="year" selected="selected">Yearly</option>
                                                <option value="month">Monthly</option>
                                                <option value="week">Weekly</option>
                                            </select>
                                        </div>
                                        <label class="col-md-2 control-label repeat-every" style="display:none;">Repeat Every :</label>
                                        <div class="col-md-2">
                                            <select  id="repeat_every" name="repeat_every" class="form-control repeat-every" style="display:none;">

                                            </select>
                                        </div>
                                        <label class="col-md-2 control-label repeat_on" style="display:none;">Repeat On :</label>
                                        <div class="col-md-2">
                                            <select name="repeat_on" class="form-control repeat_on"  style="display:none;">
                                                <option>Monday</option>
                                                <option>Tuesday</option>
                                                <option>Wednesday</option>
                                                <option>Thursday</option>
                                                <option>Friday</option>
                                                <option>Saturday</option>
                                                <option>Sunday</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row show_for_sch">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="my-label">End</label>

                                        <div class="sched">
                                            <label>
                                                <input type="radio" name="end_msg" id="radio008" value="infinte" checked="true"> Never </label>
                                            <label>
                                                <input type="radio" name="end_msg" id="radio009" value="finite" > After </label>
                                        </div>
                                        <div class="sched1">
                                            <label class="col-md-2 control-label repeat-everyoc" style="display: none">Occurences</label>
                                            <div class="col-md-2">
                                                <select name="msg_occurence" class="form-control repeat-everyoc" style="display: none">
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <!--<button class="btn default" type="button">Cancel</button>-->
                            <button class="btn blue snd_sms" type="submit"><i class="fa fa-paper-plane "></i> Send</button>
                        </div>
                        </form>
                        <!-- END FORM-->

                    </div>
                    <!-- END SAMPLE FORM PORTLET-->
                    <!-- END PAGE CONTENT-->
                </div>
                <?php
                $weeks = array('1' => '1 week', '2' => '2 weeks', '3' => '3 weeks', '4' => '4 weeks');
                for ($j = 1; $j < 31; $j++) {
                    $days[$j] = ordinal($j);
                }
                ?>
                <script>
                    $(document).delegate('.repeat_msg', 'change', function () {
                        if ($(this).val() == 'week') {
                            $("#repeat_every").html('');
                            var weeks = '<?php echo json_encode($weeks); ?>';
                            weeks = JSON.parse(weeks);
                            $.each(weeks, function (key, value) {
                                $("#repeat_every").append('<option value="' + key + '">' + value + '</option>');
                            });
                            $(".repeat-every").show();
                            $(".repeat_on").show();
                        }
                        else if ($(this).val() == 'month') {
                            $("#repeat_every").html('');
                            var dayz = '<?php echo json_encode($days); ?>';
                            dayz = JSON.parse(dayz);
                            $.each(dayz, function (key, value) {
                                $("#repeat_every").append('<option value="' + key + '">' + value + '</option>');
                            });
                            $(".repeat-every").show();
                            $(".repeat_on").hide();
                        } else {
                            $("#repeat_every").html('');
                            $(".repeat-every").hide();
                            $(".repeat_on").hide();
                        }
                    });
                    $(document).ready(function () {
                        $("#radio008").click(function () {
                            $(".repeat-everyoc").hide();
                        });
                        $("#radio009").click(function () {
                            $(".repeat-everyoc").show();
                        });


                        $('.sel_all').click(function (event)
                        {
                            if (this.checked)
                            {
                                $('.checkboxes').each(function ()
                                {
                                    //$(this).parent().parent().addClass('checked');
                                    $(this).parent().addClass("checked");
                                    this.checked = true;
                                });
                            }
                            else
                            {
                                $('.checkboxes').each(function ()
                                {
                                    //$(this).parent().parent().removeClass('checked');
                                    $(this).parent().removeClass("checked");
                                    this.checked = false;
                                });
                            }
                        });


                        $('.sel_all_tent').click(function (event)
                        {
                            if (this.checked)
                            {
                                $('.tcheckboxes').each(function ()
                                {
                                    //$(this).parent().parent().addClass('checked');
                                    $(this).parent().addClass("checked");
                                    this.checked = true;
                                });
                            }
                            else
                            {
                                $('.tcheckboxes').each(function ()
                                {
                                    //$(this).parent().parent().removeClass('checked');
                                    $(this).parent().removeClass("checked");
                                    this.checked = false;
                                });
                            }
                        });
                        var date = new Date();
                        //alert(date.getDate());
                        date.setDate(date.getDate());
                        $('.date-picker2').datepicker({
                            startDate: date
                        });
                    });
                </script>


