<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
define('FOPEN_READ', 'rb');
define('FOPEN_READ_WRITE', 'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE', 'ab');
define('FOPEN_READ_WRITE_CREATE', 'a+b');
define('FOPEN_WRITE_CREATE_STRICT', 'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
define('EXIT_SUCCESS', 0); // no errors
define('EXIT_ERROR', 1); // generic error
define('EXIT_CONFIG', 3); // configuration error
define('EXIT_UNKNOWN_FILE', 4); // file not found
define('EXIT_UNKNOWN_CLASS', 5); // unknown class
define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
define('EXIT_USER_INPUT', 7); // invalid user input
define('EXIT_DATABASE', 8); // database error
define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code

define('TWILIO_SID','AC8865d8728f5d185518c7bb15ff844531');
define('TWILIO_TOKEN', 'dca452618ab61f02c9023e1b3556bf18');
define('API_TOKEN','5d8728f5d2618ab61f02c9023185518c7bb');
define('PROPERTY_PHOTOS', 'assets/uploads/properties_photos');
define('USER_DOCUMENT', 'assets/uploads/user_document');
define('GOOGLE_KEY','AIzaSyBDwwVzZxzze990TyZTmfH0YG-n2KB1y1I');
define('SIGNATURE','TenantTag');
define('PLAYSTORE','https://goo.gl/oGEsUf');
define('APPSTORE','https://goo.gl/f8MMXW');

define('PER_PROPERTY_AMOUNT',6.95);
define('PER_PROPERTY_AMOUNT_TEST',1.95);
//define('TMAINTENANCE_TTEXT_PRICE',5.00);
/*define('TMAINTENANCE_TTEXT_PAYMENT_CODE',1);
define('TPAY_PAYMENT_CODE',2);*/


/*define('STRIPE_SECRET_KEY','sk_test_Q7WTvSnfKPazDdcDCeljsAG9');
define('STRIPE_PULBLISHABLE_KEY','pk_test_mo7Ze4S2ZLdYooQx7vl1fC9a');*/
define('STRIPE_PULBLISHABLE_KEY','pk_live_Yab9kdtLJnzhPsjHekdVs7xZ');
define('STRIPE_SECRET_KEY','sk_live_D7r2m36ZINZAe00FW1VUYfNW');

/**************** Synapse Info **************************************/
//define('SYNAPSE_CLIENT_ID','id-c83d6205-785a-4776-b4de-8584aebc045d');
//define('SYNAPSE_CLIENT_SECRET','secret-076f2412-69cf-46b1-a304-ad27e62f38f3');
/*******************Live Synapse_detail*************************************/
define('SYNAPSE_IP_ADDRESS','54.152.53.196');
define('SYNAPSE_FINGERPRINT','suasusau21324redakufejfjsf');

define('SYNAPSE_API_BASE_URL','https://synapsepay.com/api/3/');
define('SYNAPSE_CLIENT_ID','client_id_0UM8SnNfuXFca7gZh62PQ9lV3LW4YxGdiEKsezqp');
define('SYNAPSE_CLIENT_SECRET','client_secret_KhArjpdwz4eI08xfJBL3ZGmvHNDqVQoX0MTOtYun');
/*define('SYNAPSE_API_BASE_URL','https://sandbox.synapsepay.com/api/3/');
define('SYNAPSE_CLIENT_ID','id-7f2fcccd-c941-4214-82bb-2e5b7b2770b1');
define('SYNAPSE_CLIENT_SECRET','secret-26dc3d14-b623-441a-800e-6c2d8dad84e5');*/
