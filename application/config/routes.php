<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/*
  | -------------------------------------------------------------------------
  | URI ROUTING
  | -------------------------------------------------------------------------
  | This file lets you re-map URI requests to specific controller functions.
  |
  | Typically there is a one-to-one relationship between a URL string
  | and its corresponding controller class/method. The segments in a
  | URL normally follow this pattern:
  |
  |	example.com/class/method/id/
  |
  | In some instances, however, you may want to remap this relationship
  | so that a different class/function is called than the one
  | corresponding to the URL.
  |
  | Please see the user guide for complete details:
  |
  |	http://codeigniter.com/user_guide/general/routing.html
  |
  | -------------------------------------------------------------------------
  | RESERVED ROUTES
  | -------------------------------------------------------------------------
  |
  | There are three reserved routes:
  |
  |	$route['default_controller'] = 'welcome';
  |
  | This route indicates which controller class should be loaded if the
  | URI contains no data. In the above example, the "welcome" class
  | would be loaded.
  |
  |	$route['404_override'] = 'errors/page_missing';
  |
  | This route will tell the Router which controller/method to use if those
  | provided in the URL cannot be matched to a valid route.
  |
  |	$route['translate_uri_dashes'] = FALSE;
  |
  | This is not exactly a route, but allows you to automatically route
  | controller and method names that contain dashes. '-' isn't a valid
  | class or method name character, so it requires translation.
  | When you set this option to TRUE, it will replace ALL dashes in the
  | controller and method URI segments.
  |
  | Examples:	my-controller/index	-> my_controller/index
  |		my-controller/my-method	-> my_controller/my_method
 */
$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['uploads/(:any)/(:any)'] = "image/resize/";
$route['uploads/(:any)/(:any)/(:any)'] = "image/resize/";
$route['uploads/(:any)/(:any)/(:any)/(:any)'] = "image/resize/";

$route['getStateByCountryId/(:num)'] = 'welcome/getStateByCountryId/$1';
$route['getCityByStateId/(:num)'] = 'welcome/getCityByStateId/$1';
$route['terms-and-conditions'] = 'welcome/terms';

/* * ************** Front Route Starts here **************************************** */

$route['dashboard'] = "account/index";
$route['update_survey_question'] = "welcome/update_survey_question";
$route['dashboard/(:any)'] = "account/index/$1";
$route['login'] = "register/login";
$route['forget_password'] = "register/forgot_password";
$route['reset-password/(:any)'] = "register/reset_password/$1";
$route['logout'] = "register/logout";
$route['register'] = "register/index";
$route['register/validate/(:any)'] = "register/validateAccount/$1";

/* * *************** Property routes ******************************************* */

$route['add-property'] = "property/add";
$route['add-property-step2'] = "property/add_step2";
$route['add-property-step3'] = "property/add_step3";
$route['account_setup'] = "property/account_setup";
$route['checkout_process'] = "property/checkout_process";
$route['checkout'] = "property/checkout";
$route['change-tservices-status/(:num)/(:any)/(:any)'] = "property/changeTservicesStatus/$1/$2/$3";
$route['update-property'] = "property/update_property";
$route['update-address'] = "property/update_address";
$route['update-property-step1'] = "property/updateStep1";
$route['update-property-step2'] = "property/updateStep2";
/* * *************** Vendor routes ******************************************* */

$route['add-vendor'] = "vendors/add_vendor";
$route['update-vendor/(:num)'] = "vendors/edit_vendor/$1";
$route['delete-vendor/(:num)'] = "vendors/delete_vendor/$1";

/* * ************** Front Route Ends here **************************************** */

/* * *************** Tenants routes ******************************************* */

$route['tenants'] = "tenant/index";
$route['add-tenant'] = "tenant/add";
$route['update-tenant/(:num)'] = "tenant/edit/$1";

/* * ************** Front Route Ends here **************************************** */

/* * *************** My properties routes ******************************************* */

$route['my-properties'] = "account/my_properties";

/* * ************** Front Route Ends here **************************************** */

/* * ************** Admin Route Start here **************************************** */

$route['admin/getPropertiesByLandlordId/(:num)'] = 'admin/landlords/getPropertiesByLandlordId/$1';

$route['admin/website-settings'] = "admin/web_config/update_wesite_settings";
$route['admin/social-settings'] = "admin/web_config/update_social_settings";
$route['admin/email-settings'] = "admin/web_config/update_email_settings";
$route['admin/resources'] = "admin/web_config/resources";

$route['admin/common_question'] = "admin/common_question/index";
$route['admin/view_category'] = "admin/common_question/view_category";
$route['admin/survey_question'] = "admin/common_question/survey_question";
$route['admin/survey_question_answer/(:num)'] = "admin/common_question/survey_question_answer/$1"; 
$route['admin/edit_survey_question/(:num)'] = "admin/common_question/edit_survey_question/$1"; 
$route['admin/survey_question/delete_survey_question/(:num)'] = "admin/common_question/delete_survey_question/$1";
$route['admin'] = "admin/admin";
$route['admin/my-profile'] = "admin/admin/admin_profile";
$route['admin/dashboard'] = "admin/admin/index";
$route['admin/logout'] = "admin/admin/logout";
$route['admin/forgetPassword'] = "admin/admin/forgetPassword";
$route['admin/test'] = "admin/admin/test";

$route['admin/login'] = "account/index";
$route['admin'] = "account/index";
$route['coastal-investment-admin/login'] = "admin/admin/login";
$route['coastal-investment-admin'] = "admin/admin/login";


/* * ******************* Admin Landlords Route starts here **************************** */

$route['admin/landlords'] = "admin/landlords/index";
$route['admin/landlords/(:num)'] = "admin/landlords/index/$1";
$route['admin/landlords/add'] = "admin/landlords/add";
$route['admin/landlords/view/(:num)'] = "admin/landlords/view/$1";
$route['admin/landlords/edit/(:num)'] = "admin/landlords/edit/$1";
$route['admin/landlords/doTask/(:any)/(:num)'] = "admin/landlords/doTask/$1/$2";
$route['admin/landlords/doMultipleTask/(:any)'] = "admin/landlords/doMultipleTask/$1";
$route['admin/landlords/filter/(:any)'] = "admin/landlords/filter/$1";
/* * ******************* Admin Landlords Route End here **************************** */

/* * ******************* Admin Tenants Route starts here **************************** */

$route['admin/tenants'] = "admin/tenants/index";
$route['admin/tenants/(:num)'] = "admin/tenants/index/$1";
$route['admin/tenants/add'] = "admin/tenants/add";
$route['admin/tenants/view/(:num)'] = "admin/tenants/view/$1";
$route['admin/tenants/edit/(:num)'] = "admin/tenants/edit/$1";
$route['admin/tenants/doTask/(:any)/(:num)'] = "admin/tenants/doTask/$1/$2";
$route['admin/tenants/doMultipleTask/(:any)'] = "admin/tenants/doMultipleTask/$1";
$route['admin/tenants/filter/(:any)'] = "admin/tenants/filter/$1";
/* * ******************* Admin Tenants Route starts here **************************** */

/* * ******************* Admin Amenties Route starts here **************************** */

$route['admin/amenties'] = "admin/amenties/index";
$route['admin/amenties/(:num)'] = "admin/amenties/index/$1";
$route['admin/amenties/add'] = "admin/amenties/add";
$route['admin/amenties/view/(:num)'] = "admin/amenties/view/$1";
$route['admin/amenties/edit/(:num)'] = "admin/amenties/edit/$1";
$route['admin/amenties/doTask/(:any)/(:num)'] = "admin/amenties/doTask/$1/$2";
$route['admin/amenties/doMultipleTask/(:any)'] = "admin/amenties/doMultipleTask/$1";
$route['admin/amenties/filter/(:any)'] = "admin/amenties/filter/$1";
/* * ******************* Admin Amenties Route End here **************************** */

/* * ******************* Admin Services Route starts here **************************** */

$route['admin/services'] = "admin/services/index";
$route['admin/services/(:num)'] = "admin/services/index/$1";
$route['admin/services/add'] = "admin/services/add";
$route['admin/services/view/(:num)'] = "admin/services/view/$1";
$route['admin/services/edit/(:num)'] = "admin/services/edit/$1";
$route['admin/services/doTask/(:any)/(:num)'] = "admin/services/doTask/$1/$2";
$route['admin/services/doMultipleTask/(:any)'] = "admin/services/doMultipleTask/$1";
$route['admin/services/filter/(:any)'] = "admin/services/filter/$1";
/* * ******************* Admin Services Route End here **************************** */

/* * ******************* Admin Tservices Route starts here **************************** */

$route['admin/tservices'] = "admin/tservices/index";
$route['admin/tservices/(:num)'] = "admin/tservices/index/$1";
$route['admin/tservices/add'] = "admin/tservices/add";
$route['admin/tservices/view/(:num)'] = "admin/tservices/view/$1";
$route['admin/tservices/edit/(:num)'] = "admin/tservices/edit/$1";
$route['admin/tservices/doTask/(:any)/(:num)'] = "admin/tservices/doTask/$1/$2";
$route['admin/tservices/doMultipleTask/(:any)'] = "admin/tservices/doMultipleTask/$1";
$route['admin/tservices/filter/(:any)'] = "admin/tservices/filter/$1";
/* * ******************* Admin Tservices Route End here **************************** */

/* * ******************* Admin Messages Route starts here **************************** */

$route['admin/messages'] = "admin/messages/index";
$route['admin/messages/(:num)'] = "admin/messages/index/$1";
$route['admin/messages/add'] = "admin/messages/add";
$route['admin/messages/view/(:num)'] = "admin/messages/view/$1";
$route['admin/messages/edit/(:num)'] = "admin/messages/edit/$1";
$route['admin/messages/doTask/(:any)/(:num)'] = "admin/messages/doTask/$1/$2";
$route['admin/messages/doMultipleTask/(:any)'] = "admin/messages/doMultipleTask/$1";
$route['admin/messages/filter/(:any)'] = "admin/messages/filter/$1";
/* * ******************* Admin Messages Route End here **************************** */

/* * ******************* Admin Sent Mails History and Sent New Mail Route starts here **************************** */

$route['admin/promotional/emails'] = "admin/send_email/index";
$route['admin/promotional/emails/(:num)'] = "admin/send_email/index/$1";
$route['admin/promotional/emails/send'] = "admin/send_email/sendEmail";
$route['admin/promotional/emails/view/(:num)'] = "admin/send_email/view/$1";
$route['admin/promotional/emails/doTask/(:any)/(:num)'] = "admin/send_email/doTask/$1/$2";
$route['admin/promotional/emails/doMultipleTask/(:any)'] = "admin/send_email/doMultipleTask/$1";
$route['admin/promotional/emails/filter/(:any)'] = "admin/send_email/filter/$1";
/* * ******************* Admin Sent Mails History and Sent New Mail Route Ends here **************************** */

/* * ******************* Admin Email Templates Route starts here **************************** */

$route['admin/email-templates'] = "admin/email_template/index";
$route['admin/email-templates/(:num)'] = "admin/email_template/index/$1";
$route['admin/email-templates/add'] = "admin/email_template/add";
$route['admin/email-templates/view/(:num)'] = "admin/email_template/view/$1";
$route['admin/email-templates/edit/(:num)'] = "admin/email_template/edit/$1";
$route['admin/email-templates/doTask/(:any)/(:num)'] = "admin/email_template/doTask/$1/$2";
$route['admin/email-templates/doMultipleTask/(:any)'] = "admin/email_template/doMultipleTask/$1";
$route['admin/email_template/filter/(:any)'] = "admin/email_template/filter/$1";
/* * ******************* Admin Email Templates Route End here **************************** */

/* * ******************* Admin Static Pages Route starts here **************************** */

$route['admin/pages'] = "admin/pages/index";
$route['admin/pages/(:num)'] = "admin/pages/index/$1";
$route['admin/pages/add'] = "admin/pages/add";
$route['admin/pages/view/(:num)'] = "admin/pages/view/$1";
$route['admin/pages/edit/(:num)'] = "admin/pages/edit/$1";
$route['admin/pages/doTask/(:any)/(:num)'] = "admin/pages/doTask/$1/$2";
$route['admin/pages/doMultipleTask/(:any)'] = "admin/pages/doMultipleTask/$1";
$route['admin/pages/filter/(:any)'] = "admin/pages/filter/$1";
/* * ******************* Admin Static Pages Route End here **************************** */

/* * ******************* Admin Fetures Route starts here **************************** */

$route['admin/features'] = "admin/features/index";
$route['admin/features/(:num)'] = "admin/features/index/$1";
$route['admin/features/add'] = "admin/features/add";
$route['admin/features/view/(:num)'] = "admin/features/view/$1";
$route['admin/features/edit/(:num)'] = "admin/features/edit/$1";
$route['admin/features/doTask/(:any)/(:num)'] = "admin/features/doTask/$1/$2";
$route['admin/features/doMultipleTask/(:any)'] = "admin/features/doMultipleTask/$1";
$route['admin/features/filter/(:any)'] = "admin/features/filter/$1";
/* * ******************* Admin Feturess Route End here **************************** */

/* * ******************* Admin Contact us enquiries Route starts here **************************** */

$route['admin/enquiries'] = "admin/enquiries/index";
$route['admin/enquiries/(:num)'] = "admin/enquiries/index/$1";
$route['admin/enquiries/reply/(:num)'] = "admin/enquiries/reply/$1";
$route['admin/enquiries/view/(:num)'] = "admin/enquiries/view/$1";
$route['admin/enquiries/doTask/(:any)/(:num)'] = "admin/enquiries/doTask/$1/$2";
$route['admin/enquiries/doMultipleTask/(:any)'] = "admin/enquiries/doMultipleTask/$1";
$route['admin/enquiries/filter/(:any)'] = "admin/enquiries/filter/$1";
/* * ******************* Admin Contact us enquiries Route Ends here **************************** */

/* * ******************* Admin Messages Route starts here **************************** */

$route['admin/faq'] = "admin/faq/index";
$route['admin/faq/(:num)'] = "admin/faq/index/$1";
$route['admin/faq/add'] = "admin/faq/add";
$route['admin/faq/view/(:num)'] = "admin/faq/view/$1";
$route['admin/faq/edit/(:num)'] = "admin/faq/edit/$1";
$route['admin/faq/doTask/(:any)/(:num)'] = "admin/faq/doTask/$1/$2";
$route['admin/faq/doMultipleTask/(:any)'] = "admin/faq/doMultipleTask/$1";
$route['admin/faq/filter/(:any)'] = "admin/faq/filter/$1";
$route['page/(:any)'] = "page/index/$1";
$route['page/app/(:any)'] = "page/app/$1";
$route['feature/(:any)'] = "feature/index/$1";
/* * ******************* Admin Messages Route End here **************************** */

/* * ******************* For admin notifications ********************************** */

$route['admin/get-admin-notifications'] = "admin/more_notifications/index";
