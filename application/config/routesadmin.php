<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions. 
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a 
| URL normally follow this pattern: 
|
|	example.com/class/method/id/ 
|
| In some instances, however, you may want to remap this relationship   
| so that a different class/function is called than the one 
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/
$route['default_controller'] = "admin/admin";
$route['404_override'] = 'error_not_found/index';
$route['scaffolding_trigger'] = "";
$route['admin/(\w{2})/(.*)'] = '$2';
$route['admin/(\w{2})'] = $route['default_controller']; 


$route[config_item('adminName').'/underconstruction'] 					="admin/admin/error404";
$route[config_item('adminName').'/admin-logout'] 						="admin/admin/adminLogout";
$route[config_item('adminName').'/login'] 								="admin/admin/login";
$route[config_item('adminName').'/adminarea'] 							="admin/admin/adminarea";
$route[config_item('adminName').'/index'] 								="admin/admin/index";
$route[config_item('adminName').'/update-profile'] 						="admin/admin/update_admin";
$route[config_item('adminName').'/admin-profile'] 						="admin/admin/admin_profile";
$route[config_item('adminName').'/change-password'] 					="admin/admin/changePassword";

/**************** Admin Route Start here *****************************************/

$route[config_item('adminName').'/website-settings']		=	"admin/web_config/update_wesite_settings";
$route[config_item('adminName').'/social-settings']			=	"admin/web_config/update_social_settings";

$route[config_item('adminName')]							=	"admin/admin";
$route[config_item('adminName').'/dashboard']				=	"admin/admin/index";
$route[config_item('adminName').'/logout']					=	"admin/admin/logout";
$route[config_item('adminName').'/login']					=	"admin/admin/login";
$route[config_item('adminName').'/forget_password']			=	"admin/admin/forget_password";

/********************* Admin Lanlords Route starts here *****************************/
	
$route[config_item('adminName').'/landlords']						=	"admin/landlords/index";
$route[config_item('adminName').'/landlords/(:num)']				=	"admin/landlords/index/$1";
$route[config_item('adminName').'/landlords/add']					=	"admin/landlords/add";
$route[config_item('adminName').'/landlords/view/(:num)']			=	"admin/landlords/view/$1";
$route[config_item('adminName').'/landlords/edit/(:num)']			=	"admin/landlords/edit/$1";
$route[config_item('adminName').'/landlords/doTask/(:any)/(:num)']	=	"admin/landlords/doTask/$1/$2";
$route[config_item('adminName').'/landlords/doMultipleTask/(:any)']	=	"admin/landlords/doMultipleTask/$1";
$route[config_item('adminName').'/landlords/filter']				=	"admin/landlords/search";
/********************* Admin Lanlords Route End here *****************************/

/********************* Admin Amenties Route starts here *****************************/
	
$route[config_item('adminName').'/amenties']						=	"admin/amenties/index";
$route[config_item('adminName').'/amenties/(:num)']					=	"admin/amenties/index/$1";
$route[config_item('adminName').'/amenties/add']					=	"admin/amenties/add";
$route[config_item('adminName').'/amenties/view/(:num)']			=	"admin/amenties/view/$1";
$route[config_item('adminName').'/amenties/edit/(:num)']			=	"admin/amenties/edit/$1";
$route[config_item('adminName').'/amenties/doTask/(:any)/(:num)']	=	"admin/amenties/doTask/$1/$2";
$route[config_item('adminName').'/amenties/doMultipleTask/(:any)']	=	"admin/amenties/doMultipleTask/$1";
$route[config_item('adminName').'/amenties/filter']					=	"admin/amenties/search";
/********************* Admin Amenties Route End here *****************************/

/********************* Admin Services Route starts here *****************************/
	
$route[config_item('adminName').'/services']						=	"admin/services/index";
$route[config_item('adminName').'/services/(:num)']					=	"admin/services/index/$1";
$route[config_item('adminName').'/services/add']					=	"admin/services/add";
$route[config_item('adminName').'/services/view/(:num)']			=	"admin/services/view/$1";
$route[config_item('adminName').'/services/edit/(:num)']			=	"admin/services/edit/$1";
$route[config_item('adminName').'/services/doTask/(:any)/(:num)']	=	"admin/services/doTask/$1/$2";
$route[config_item('adminName').'/services/doMultipleTask/(:any)']	=	"admin/services/doMultipleTask/$1";
$route[config_item('adminName').'/services/filter']					=	"admin/services/search";
/********************* Admin Amenties Route End here *****************************/
