<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$autoload['packages'] = array();

$autoload['libraries'] = array('session','session','database','form_validation','user_agent','email','xmlrpc','autoacl');

$autoload['drivers'] = array();

$autoload['helper'] = array('url','file','form','captcha','cookie','image','function');

$autoload['config'] = array();

$autoload['language'] = array();

$autoload['model'] = array('common_model');
