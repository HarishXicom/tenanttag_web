<?php


require('init.php');

use SynapsePayRest\Client;






function createSynapsepayTransaction($user_id,$node_id,$fingerprint,$client_id,$client_secret,$ip_address,$note,$amount,$currency){

    $options = array(
        'fingerprint'=> $fingerprint,
        'client_id'=> $client_id,
        'client_secret'=> $client_secret,
        'development_mode'=> true, # true will ping sandbox.synapsepay.com
        'ip_address'=> $ip_address
    );

    $client = new Client($options, $user_id); // $user_id optional
    /**************** Create user ***************************************/
    $create_response = $client->user->create($options);
    $user = $client->user->get($user_id);

    /**************** Create oauth_key by refresh_token ***************************************/
    $refresh_payload = array('refresh_token' => $user['refresh_token']); 
    $refresh_response = $client->user->refresh($refresh_payload);

    /**************** Create transaction  ***************************************/
    $oauth_key = $refresh_response['oauth_key'];
    $options = array(
        'oauth_key'=> $oauth_key, # Optional,
        'fingerprint'=> $fingerprint,
        'client_id'=> $client_id,
        'client_secret'=> $client_secret,
        'development_mode'=> true, # true will ping sandbox.synapsepay.com
        'ip_address'=> $ip_address
    );
    $client_auth = new Client($options, $user_id); // $user_id optional
    $trans_payload = array(
        "to" => array(
            "type" => "ACH-US",
            "id" => $node_id
        ),
        "amount" => array(
            "amount" => $amount,
            "currency" => $currency
        ),
        "extra" => array(
            "note" => $note,
            "process_on" => 1,
            "ip" => $ip_address
        )
    );

    $create_response = $client_auth->trans->create($node_id,$trans_payload);
    return $create_response;
}





$user_id = '58a146e184f2d0001e02964b'; # optionals
$node_id = '58a149037e0887001fd6a5e8';
$fingerprint = 'HTC-HTC One X-e7921e54c8e05686-GOOG';
$client_id = 'id-c83d6205-785a-4776-b4de-8584aebc045d';
$client_secret = 'secret-076f2412-69cf-46b1-a304-ad27e62f38f3';
$ip_address = '192.1.1.1';
$note = "Deposit to bank account";
$amount = 10.10;
$currency = 'USD';

echo "<pre>";
$data = createSynapsepayTransaction($user_id,$node_id,$fingerprint,$client_id,$client_secret,$ip_address,$note,$amount,$currency);
print_r($data);
exit;



?>