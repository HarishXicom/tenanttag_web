$('document').ready(function () {
    //$('document').on('submit','.ajaxForm',function(){
    $('.ajaxForm').submit(function () {
        if (pageckeditor)
        {
            CKupdate();
        }
        var formId = $(this).attr('id');
      //     console.log(formId);
        var formClass = '';
        if (formId)
            formClass = '#' + formId;
        else
            formClass = '.ajaxForm';
        var postUrl = $(this).attr('action');

        $(this).ajaxSubmit({
            url: postUrl,
            dataType: 'json',
            beforeSend: function () {
                  $('#wait-div').show();
            },
            success: function (response) {
                $(formClass).find('.ajax_report').removeClass('alert-success').removeClass('alert-danger').fadeIn(200);
                 $('#wait-div').hide();
                if (response.success)
                    $(formClass).find('.ajax_report').addClass('alert-success').children('.ajax_message').html(response.success_message);
                else
                    $(formClass).find('.ajax_report').addClass('alert-danger').children('.ajax_message').html(response.error_message);
                if (response.url)
                {
                    setTimeout(function () {
                        window.location.href = response.url;
                    }, 700);
                }
                if (response.resetForm)
                    $(formClass).resetForm();
                if (response.selfReload)
                {
                    //location.reload();
                    setTimeout(function () {
                        location.reload();
                    }, 700);
                }
                if (response.scrollToElement)
                    scrollToElement(formClass, 1000);
                if (response.ajaxCallBackFunction)
                    ajaxCallBackFunction(response);
                setTimeout(function () {
                    $(formClass).find('.ajax_report').fadeOut(1000);
                }, 5000);
            },
            error: function () {
                alert('server error');
                  $('#wait-div').hide();
            }


        });
        return false;
    });

});

function CKupdate()
{
    for (instance in CKEDITOR.instances)
        CKEDITOR.instances[instance].updateElement();
}

function scrollToElement(element, speed)
{
    $('html, body').animate({scrollTop: $(element).position().top - 70}, speed);
}
