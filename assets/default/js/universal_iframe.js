function setupSynapseiFrame(data){
	var button = document.getElementById('synapsepay');
	var button_initial = '';
	try{
		button_initial = button.value;
		button.value = "Loading...";
	}catch(exc){

	}
	iFrame = document.getElementById('synapse_iframe');
	// iFrame.src = 'http://localhost:8090/src/www';
	iFrame.src = 'https://cm.synapsepay.com/universal';

	console.log(data);

	iFrame.onload = function() {
		setTimeout(function(){
			iFrame.contentWindow.postMessage(JSON.stringify(data),'*');
			// document.body.style.overflow = 'hidden';
			try{
				button.value = button_initial;
			}catch(exc){

			}
		}, 100);
	};
}