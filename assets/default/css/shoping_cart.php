
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script>
    $(document).ready(function(){
        $("#link-btn").click(function(){
            $("#linked-acc").slideDown("slow");
        });
    });
</script>	
<div class="level">
    <div class="container">
        <div class="level-indicator">
            <img src="<?= $this->config->item('templateassets') ?>images/full-level.png" alt="" />
            <ul>
                <li> Property Info</li>
                <li>Tenants</li>
                <li  class="blue-col1">tServices</li>
            </ul>
        </div>
    </div>
</div>
<section class="signup-section2">
    <div class="container">
        <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
        <script type="text/javascript">  
          Stripe.setPublishableKey('pk_test_mo7Ze4S2ZLdYooQx7vl1fC9a');    
          function onSubmitDo () {      
            Stripe.card.createToken( document.getElementById('payment-form'), myStripeResponseHandler );          
            return false;      
          };

          function myStripeResponseHandler ( status, response ) {      
            console.log( status );
            console.log( response );    
            if ( response.error ) {
              document.getElementById('payment-error').innerHTML = response.error.message;
            } else {
              var tokenInput = document.createElement("input");
              tokenInput.type = "hidden";
              tokenInput.name = "stripeToken";
              tokenInput.value = response.id;
              var paymentForm = document.getElementById('payment-form');
              paymentForm.appendChild(tokenInput);
              paymentForm.submit();
            }      
         };      
        </script>
        <form action="<?php echo base_url('property/charge_by_stripe'); ?>" method="POST" id="payment-form" onsubmit="return onSubmitDo()">   
            <div class="ajax_report alert display-hide" role="alert" style="margin-bottom: 10px; margin-left: 0px; width: 500px; position: relative; top: 100px;">
              <span class="close-message"></span>
              <div class="ajax_message">Hello Message</div>
            </div>
            <div class="tpay-acc-setup">
                 <div class="shopping-cart-sec">
                  <h2>Shopping Cart</h2>
                  <div class="shopping-cart-sec-in">
                      <h4>Summary of Services</h4>
                      <table border="1" cellpadding="0" cellspacing="0">
                          <thead>
                              <tr>
                                  <th>Services</th>
                                  <th>Properties</th>
                                  <th>Status</th>
                                  <th>Price</th>
                                  <th>Total</th>
                              </tr>
                          </thead>
                          <tbody>
                              <tr>
                                  <td>tMaintenance / tText</td>
                                  <td>1-5</td>
                                  <td>
                                    <div class="togle tpayment stop " id="ttext" data-type="ttext" data-value="yes" data-property="No">
                                        <span class="check"></span>
                                    </div>
                                  </td>
                                  <td>$5/mth</td>
                                  <td>$5</td>
                              </tr>
                              <tr>
                                  <td>tRent</td>
                                  <td>3</td>
                                  <td>
                                    <div class="togle tpayment " id="trent" data-type="trent" data-value="yes" data-property="No">
                                        <span class="check"></span>
                                    </div>
                                  </td>
                                  <td>$4.95/mth/prop</td>
                                  <td>$14.85</td>
                              </tr>
                              <tr>
                                  <td>Total</td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td>$19.85</td>
                              </tr>
                          </tbody>
                      </table>
                  </div>  
                  <div class="cart-form">                      
                      <span style='color: red' id='payment-error'></span>                      
                          <div class="cart-form-divide cart-form-section">
                              <input type="hidden" name="amount"/>
                              <input type="text" data-stripe="name" name="first_name" placeholder="First Name" />
                              <input type="text" name="last_name" placeholder="Last Name" />
                          </div>
                          
                          <div class="clearfix"></div>
                          <div class="cart-form-section cart-form-sectionfour">
                              <input type="text" name="" style="width:49%;" data-stripe="number" placeholder="CC"  />
                               <input type="text" data-stripe="cvc" placeholder="CVV">
                              <input type="text"   class="reset" name="" style="" data-stripe="number" placeholder="Exp. Date"  />
                             
                          </div>
                          <div class="clearfix"></div>
                          <div class="cart-form-section cart-form-sectionfour cart-form-sectionfour2">
                              <input type="text" name="" data-stripe="billingAddress" placeholder="Billing Address" />
                               <input type="text" name="BillingAddressCity" style="width:21%;" data-stripe="BillingAddressCity" placeholder="City" />
                              <select data-stripe="BillingAddressState" name="BillingAddressState">
                                  <option>State</option>
                                  <option name="option1">option1</option>
                                  <option name="option2">option2</option>
                                  <option name="option3">option3</option>
                              </select>
                              <input type="text" class="reset" data-stripe="address_zip" placeholder="Zip" />
                          </div>
                          <div class="clearfix"></div>
                          
                          <div class="cart-btns">
                              <a href="#" data-dismiss="modal" class="cancel-btn">Cancel</a>
                              <input type="hidden" name="emailAddress">
                              <input type="submit" name="" value="Confirm" />
                          </div>
                  </div>    
              </div>      
            </div>          
        </form>
    </div>
</section>