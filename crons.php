<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* Project    : Real Estate Iny
 * Author     : Tarun Kaushik
 * Created    : 17 july 2014
 * Descrption : All the blog related functionality 
 * */


class Crons extends MY_Controller
{
     // constructor of class property   
    public function __construct() {
        parent::__construct();
    }

    /* List of all blog of current month
     * 
     * created : 16-3-2015
     * author  : sumit Kohli
     * access  : Public
     * */ 
    public function move_claims_to_archieve()
    {
        $conds = $params = array();
        $conds['status'] = '1';
        $conds['updated <= '] = date('Y-m-d H:i:s', strtotime('- 2 days'));
        $conds['archieve'] = '0';
        $params['fields'] = array('id', 'updated');

        $total_approved_claims = $this->common->get_all('property_claims', $conds, $params);

        if (!empty($total_approved_claims))
        {
            foreach ($total_approved_claims as $val) {
                $this->common->updaterecord('property_claims', array('archieve' => '1'), array('id' => $val->id));
            }
        }
    }

    public function index() {
        //$this->for_sale_properties();
        //$this->sidex_schedule_update();
        $this->send_campaign_cron_emails();
        $this->getAllSoldOutPropertiesAndChangeStatusIfSixMonthCompleted();
        exit('done');
    }

    private function sidex_schedule_update()
    {
        /*Changing for sale properties first*/

        $conds = $params = array();
        $conds['sidex_schedules.status'] = '0';
        $conds['sidex_schedules.scheduled_date <='] = date('Y-m-d');
        $params['join'] = array(
            array('table' => 'property', 'conditions' => 'property.id = sidex_schedules.property_id')
        );
        $sidex_schedules = $this->common->get_all('sidex_schedules', $conds, $params);
       
        if (!empty($sidex_schedules))
        {
            $property_ids = array();
            
            foreach ($sidex_schedules as $val) {
                $property_ids[] = $val->property_id;
                $update_query = "update property SET last_si_dex_estimate = si_dex_estimate, last_sdex_modified = '" . date('Y-m-d 00:00:00') . "', si_dex_estimate = FLOOR ( ( ( si_dex_estimate * '" . $val->sidex_change . "' ) / 100 ) + (si_dex_estimate) ) WHERE id = " . $val->property_id;
                $this->db->query($update_query);
                
                $this->common->updaterecord('sidex_schedules', array('status' => '1', 'updated_on' => sql_now()), array('id' => $val->id));
            }
            
            $this->change_property_history($property_ids);
        }
    }

    private function change_property_history($ids) {
        $property_id = $ids;

        if (!empty($property_id)) {
            $query = "INSERT INTO `property_history` (`property_id`, `event`, `price`, `source`, `status`, `notes`) VALUES";

            foreach ($property_id as $val) {
                $sidex = $this->common->get_single_field_table('property', array('id' => $val), 'si_dex_estimate');

                $sidex = $sidex[0]['si_dex_estimate'];
                if (!empty($sidex) || $sidex == 0) {
                    $query .= "('$val', 'SI-DEX Change', '$sidex', 'System Update', '1', 'System Update'),";
                }
            }
        }

        $query = rtrim($query, ',');
        $this->db->query($query);

        /* To maintain Analytics */
        $this->common->save('analytics', array('section' => SIDEX_CHANGE, 'count' => count($property_id)));
    }

    private function for_sale_properties() {
        $conds = $params = array();
        $conds['sold_expiry_date'] = date('Y-m-d');
        $conds['status'] = '2';
        $params['fields'] = array('id');
        $for_sale_properties = $this->common->get_all('property', $conds, $params);

        if (!empty($for_sale_properties)) {
            foreach ($for_sale_properties as $val) {
                $table_data = array();
                $table_data['id'] = $val->id;
                $table_data['sold_expiry_date'] = NULL;
                $table_data['for_sale'] = '0';
                $table_data['price'] = NULL;
                $table_data['status'] = ACTIVE;
                $this->common->save_data('property', $table_data);
            }
        }
    }

    private function send_campaign_cron_emails(){
        $conds = $params = array();
        $conds['num_dates'] = date('Y-m-d');
        $all_emailsToday = $this->common->get_all('campaign_email_template', $conds, $params);
        print_r($all_emailsToday);
        exit;

        if (!empty($for_sale_properties)) {
            foreach ($for_sale_properties as $val) {
                $table_data = array();
                $table_data['id'] = $val->id;
                $table_data['sold_expiry_date'] = NULL;
                $table_data['for_sale'] = '0';
                $table_data['price'] = NULL;
                $table_data['status'] = ACTIVE;
                $this->common->save_data('property', $table_data);
            }
        }
    }
    
    /*******Function to change status of sold out properties after six months from sold out date Added by Rakesh kumar on 08-10-2015 *****/
    
    function getAllSoldOutPropertiesAndChangeStatusIfSixMonthCompleted()
    {
		$this->db->where('sold_date !=','');
		$this->db->where('sold_date !=', '0000-00-00 00:00:00');
		$this->db->where('sold_price !=', '0');
		$query = $this->db->get('property');
		$allSoldPRoperties	=	$query->result();
		
		foreach($allSoldPRoperties as $value)
		{
			$newDate = date('Y-m-d', strtotime('+6 month', strtotime($value->sold_date)));
			if(strtotime($newDate) == strtotime(date('Y-m-d',time())))
			{
				//**** changing status in property table ******//
				$this->db->set('sold_date','0000-00-00 00:00:00');
				$this->db->set('sold_status',0);
				$this->db->set('sold_price',0);
				$this->db->set('for_sale','1');
				$this->db->where('id',$value->id);
				$this->db->update('property');
				
				//**** creating an entry in property history table ******//
				$this->db->set('property_id',$value->id);
				$this->db->set('event','Changed sold status to for sale');
				$this->db->set('price',$value->sold_price);
				$this->db->set('source','Syaytem update');
				$this->db->set('notes','Changed sold status to for sale');
				$this->db->set('status','1');
				$this->db->set('created',date('Y-m-d H:i:s'));
				$this->db->insert('property_history');
			}
		}
 	}
 	
 	function getAllProperties()
 	{
		$this->db->where('rating',0);
		$query = $this->db->get('property');
		$allPRoperties	=	$query->result();
		foreach($allPRoperties as $value)
		{
			if($value->si_dex_estimate == 0)
			{
				$this->db->set('rating',1);
			}
			if($value->si_dex_estimate != 0)
			{
				$this->db->set('rating',3);
			}
			$this->db->where('id',$value->id);
			$this->db->update('property');
		}
	}
}
/* End of file */
