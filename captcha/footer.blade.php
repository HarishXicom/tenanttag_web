<div class="footer-nav">
    <ul>
        <li><a href="#">About     </a></li>
        <li><a href="#">Terms Privacy  </a></li>
    </ul>
    <p> <?php echo empty(\App\Option::getOptionByOptionKey('footer_note')) ? 'Patent Pending © 2016 <span>WirelessTrek.</span> All Rights Reserved. ' : \App\Option::getOptionByOptionKey('footer_note')->option_value; ?></p>
</div>